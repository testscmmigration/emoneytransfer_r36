package emgshared.model;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import emgshared.model.Transaction;
import emgshared.property.EMTSharedContainerProperties;
import emgshared.property.PropertyLoader;
import emgshared.services.TransactionService;
import emgshared.services.TransactionServiceImpl;
import emgshared.util.Constants;

import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.services.ServiceFactory;
import emgadm.services.TransactionServiceCyberSource;
import emgadm.services.TransactionServiceGlobalCollect;

import static org.mockito.Mockito.*;


/**
 * This class tests, if according the USE_GC_for_proccesing 
 * flag and pymt_srvc_prvr_code of the transaction,
 * returns the Transaction service instance 
 * appropiate for the transaction
 * 
 * @author vl82
 *
 */
public class PaymentServiceProviderTest {
	/**
	 * Transaction test
	 */
	private Transaction transaction;
	private PropertyLoader pv;
	
	@Before
	public void setUp() throws Exception {
		transaction = new Transaction();
		
		transaction.setPartnerSiteId(Constants.MGOUK_PARTNER_SITE_ID);
		transaction.setTCProviderCode(Constants.GLOBAL_COLLECT_PROVIDER_CODE);

		//Mocks the environment variables used internally in the method
		pv = mock(PropertyLoader.class);
		when(pv.getString("USE_GC_For_Processing")).thenReturn("false");
		EMTSharedContainerProperties.initialize(pv);
	}

	@Test
	public void testReturnGlobalCollectTransactionService(){
		
		when(pv.getString("USE_GC_For_Processing")).thenReturn("true");
		EMTSharedContainerProperties.initialize(pv);
		
		boolean isGlobalCollectForProcessing = EMTSharedContainerProperties.getUseGCForProcessing();
		if(isGlobalCollectForProcessing){
			if(transaction.getTCProviderCode() == EMoneyGramAdmApplicationConstants.PYMT_PROVIDER_CODE_GLOBAL_COLLECT){
				TransactionService tranService= ServiceFactory.getInstance().getTransactionServicePymtProvider(EMoneyGramAdmApplicationConstants.PYMT_PROVIDER_CODE_GLOBAL_COLLECT);
				if(tranService instanceof TransactionServiceGlobalCollect)
				//TODO finish test
			} else if (transaction.getTCProviderCode() == EMoneyGramAdmApplicationConstants.PYMT_PROVIDER_CODE_CYBER_SOURCE){
				TransactionService tranService= ServiceFactory.getInstance().getTransactionServicePymtProvider(EMoneyGramAdmApplicationConstants.PYMT_PROVIDER_CODE_GLOBAL_COLLECT);
				if(tranService instanceof TransactionServiceCyberSource)
				//TODO finish test
			}
		}
	}
	
	@Test
	public void testReturnCyberSourceTransactionService(){
		
		when(pv.getString("USE_GC_For_Processing")).thenReturn("false");
		EMTSharedContainerProperties.initialize(pv);
		
		boolean isGlobalCollectForProcessing = EMTSharedContainerProperties.getUseGCForProcessing();
		if(!isGlobalCollectForProcessing){
			if(transaction.getTCProviderCode() == EMoneyGramAdmApplicationConstants.PYMT_PROVIDER_CODE_GLOBAL_COLLECT){
				TransactionService tranService= ServiceFactory.getInstance().getTransactionServicePymtProvider(EMoneyGramAdmApplicationConstants.PYMT_PROVIDER_CODE_GLOBAL_COLLECT);
				if(tranService instanceof TransactionServiceGlobalCollect)
				//TODO finish test
			} else if (transaction.getTCProviderCode() == EMoneyGramAdmApplicationConstants.PYMT_PROVIDER_CODE_CYBER_SOURCE){
				TransactionService tranService= ServiceFactory.getInstance().getTransactionServicePymtProvider(EMoneyGramAdmApplicationConstants.PYMT_PROVIDER_CODE_GLOBAL_COLLECT);
				if(tranService instanceof TransactionServiceCyberSource)
				//TODO finish test
			}
		}
	}
	
	
}
