<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/datePicker.js" language="JavaScript"></script>
<script src="../../Display/jscript/setOtherRangeValue.js" language="JavaScript"></script>

<center>
<span class="error">
	<html:errors property='<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>' />
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

<html:form action="/viewMessageRecipients">

<html:hidden property="msgId" />
<html:hidden property="poster" />
<html:hidden property="postTime" />
<html:hidden property="typeCode" />
<html:hidden property="prtyCode" />
<html:hidden property="msgText" />
<html:hidden property="beginTime" />
<html:hidden property="endTime" />
<html:hidden property="saveAction" />
<html:hidden property="saveAction1" />

</center>
<h3><fmt:message key="label.view.admin.message.recipients"/></h3>

<br />
<input type='submit' name='submitExit' value='Return to Admin Message viewer' />
<br /><br />

<center>

<fieldset>
	<legend class="td-label"><font color='red'><b>
		<fmt:message key="label.message" />
		<bean:write name="viewMessageRecipientsForm" property="msgId" />
		<fmt:message key="label.left.blk" /><fmt:message key="label.poster" /><fmt:message key="label.suffix"/>
		<bean:write name="viewMessageRecipientsForm" property="poster" /> 
		<fmt:message key="label.and" />
		<fmt:message key="label.posted.at" />
		<bean:write name="viewMessageRecipientsForm" property="postTime" /><fmt:message key="label.right.blk" />
		</b></font></legend>

<table border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tr>
		<td	nowrap="nowrap" class="td-label-small">
			<fmt:message key="label.search.message.type"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td	nowrap="nowrap" class="td-small">
			<bean:write name="viewMessageRecipientsForm" property="typeCode" /></td>
		<td nowrap="nowrap" class="td-small"> &nbsp; </td>
		<td	nowrap="nowrap" class="td-label-small">
			<fmt:message key="label.search.message.priority"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td	nowrap="nowrap" class="td-small">
			<bean:write name="viewMessageRecipientsForm" property="prtyCode" /></td>
	</tr>

	<tr>
		<td	nowrap="nowrap" class="td-label-small">
			<fmt:message key="label.post.begin.time"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td	nowrap="nowrap" class="td-small">
			<bean:write name="viewMessageRecipientsForm" property="beginTime" /></td>
		<td nowrap="nowrap" class="td-small"> &nbsp; </td>
		<td	nowrap="nowrap" class="td-label-small">
			<fmt:message key="label.post.end.time"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td	nowrap="nowrap" class="td-small">
			<bean:write name="viewMessageRecipientsForm" property="endTime" /></td>
	</tr>

	<tr>
		<td	nowrap="nowrap" class="td-label-small" valign="top">
			<fmt:message key="label.message.text"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td	class="td-small" colspan="4">
			<bean:write name="viewMessageRecipientsForm" property="msgText" filter="false" /></td>
	</tr>

</table>
</fieldset>

<logic:notEmpty name="rcvList">
<br />
<table border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tr>
		<td nowrap='nowrap' class='td-header-small'>
			<fmt:message key="label.recipient.id"/></td>
		<td nowrap='nowrap' class='td-header-small'>
			<fmt:message key="label.view.timestamp"/></td>
		<td nowrap='nowrap' class='td-header-small'>
			<fmt:message key="label.first.name"/></td>
		<td nowrap='nowrap' class='td-header-small'>
			<fmt:message key="label.last.name"/></td>
		<td nowrap='nowrap' class='td-header-small'>
			<fmt:message key="label.status"/></td>
		<td nowrap='nowrap' class='td-header-small'>
			<fmt:message key="label.role.name"/></td>
		<td nowrap='nowrap' class='td-header-small'>
			<fmt:message key="label.email.address"/></td>
		<td nowrap='nowrap' class='td-header-small'>
			<fmt:message key="label.last.access.timestamp"/></td>
	</tr>

	<% String dispClass = "td-small"; %>

	<logic:iterate id="rcv" name="rcvList">

	<tr>
		<td nowrap='nowrap' class='<%=dispClass%>'>
			<bean:write name="rcv" property="recipientId" /></td>
		<logic:equal name="rcv" property="read" value="true">
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="rcv" property="msgRcvDateText" /></td>
		</logic:equal>
		<logic:equal name="rcv" property="unread" value="true">
			<td nowrap='nowrap' class='<%=dispClass%>'> &nbsp; </td>
		</logic:equal>
		<td nowrap='nowrap' class='<%=dispClass%>'>
			<bean:write name="rcv" property="firstName" /></td>
		<td nowrap='nowrap' class='<%=dispClass%>'>
			<bean:write name="rcv" property="lastName" /></td>
		<td nowrap='nowrap' class='<%=dispClass%>'>
			<bean:write name="rcv" property="status" /></td>
		<td nowrap='nowrap' class='<%=dispClass%>'>
			<bean:write name="rcv" property="roleName" /></td>
		<td nowrap='nowrap' class='<%=dispClass%>'>
			<bean:write name="rcv" property="email" /></td>
		<td nowrap='nowrap' class='<%=dispClass%>'>
			<bean:write name="rcv" property="lastAccessTimeText" /></td>
	</tr>

	<% dispClass = dispClass.equals("td-shaded-small") ? "td-small" : "td-shaded-small"; %>

	</logic:iterate>

</table>
</logic:notEmpty>

</html:form>

</center>
