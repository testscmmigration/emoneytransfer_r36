<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
<head>
	<title>Transaction Status Temporary Fix</title>
	<html:base />
</head>

<link href="../../theme/eMGAdm.css" rel="stylesheet" type="text/css" />

<body MARGINWIDTH="0" MARGINHEIGHT="0" rightMargin="0" topMargin="0" leftMargin="0" bottomMargin="0" style="MARGIN-TOP: 0px; MARGIN-LEFT: 0px">
<table border="0" width='100%"' height='100%' cellspacing="0" cellpadding="0" >

	<tr height='36' valign='top' >
		<td width='6%' bgcolor='#000000' valign='top' align='left' colspan='2'>
			<img border='0' src='../../images/emoneygramheader.jpg'  />
		</td>
		<td width='*' bgcolor='#000000' colspan='3'>
			
		</td>
	</tr>
	<tr height='40'>
		<td colspan='3' valign='top'>
			<table border="0" bgcolor="#FFFFFF"  width="100%" cellPadding="0" cellSpacing="0" >
				<tr>
					<td width="3%" bgcolor="#FFFFFF" valign="top" align="left"></td>
					<td bgcolor="#ffffff" align='left' valign='top' nowrap='nowrap' class="GREY-BOLD" colspan='1'>
				</tr>
				<tr>
					<td></td>
					<td colspan='2' align='left' class='TITLE' bgcolor='#FFFFFF'><br/>
						&nbsp;&nbsp;&nbsp;Fix Transaction Status<br/>
						<img src='../../images/doubleGrayLine.gif'  />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="6%" bgcolor="#ffffff" align="left" valign="top" ></td>
		<td width="84%" valign="top" align="left">

<center>
<span class="error"><bean:write name="bean" property="msg" /></span>

<form name="fixTranStatus" method="post" action="../../fixTranStatus.emt">

<html:hidden name="bean" property="state" />

<logic:notEqual name="bean" property="state" value="go">
	<html:hidden name="bean" property="tranId" />
	<html:hidden name="bean" property="tranCreateDate" />
	<html:hidden name="bean" property="tranSender" />
	<html:hidden name="bean" property="tranCustId" />
	<html:hidden name="bean" property="tranSenderName" />
	<html:hidden name="bean" property="tranReceiver" />
	<html:hidden name="bean" property="tranCntryCd" />
	<html:hidden name="bean" property="tranStatus" />
	<html:hidden name="bean" property="tranSubStatus" />
	<html:hidden name="bean" property="tranType" />
	<html:hidden name="bean" property="tranAmount" />
	<html:hidden name="bean" property="tranStatusDate" />
</logic:notEqual>

<br />

<logic:equal name="bean" property="state" value="go">
	<span class="td-label-small">Transaction Id: &nbsp;
	<input type="text" name="tranId" size="8" /> &nbsp; &nbsp;
	<input type="submit" name="submitGo" value="go" />
	</span>
</logic:equal>

<logic:notEqual name="bean" property="state" value="go">
<table border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tr>
		<td nowrap='nowrap' class='td-label-small'><fmt:message key="label.transaction.id"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td nowrap='nowrap' class='td-small'><bean:write name="bean" property="tranId" /></td>
		<td> &nbsp; </td>
		<td nowrap='nowrap' class='td-label-small'><fmt:message key="label.transaction.status"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td nowrap='nowrap' class='td-small'><bean:write name="bean" property="tranStatus" /></td>
	</tr>

	<tr>
 		<td nowrap='nowrap' class='td-label-small'><fmt:message key="label.create.date"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td nowrap='nowrap' class='td-small'><bean:write name="bean" property="tranCreateDate" /></td>
		<td> &nbsp; </td>
 		<td nowrap='nowrap' class='td-label-small'><fmt:message key="label.transaction.funding.status"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td nowrap='nowrap' class='td-small'><bean:write name="bean" property="tranSubStatus" /></td>
	</tr>
	
	<tr>
 		<td nowrap='nowrap' class='td-label-small'><fmt:message key="label.amount"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td nowrap='nowrap' class='td-small'><bean:write name="bean" property="tranAmount" /></td>
		<td> &nbsp; </td>
 		<td nowrap='nowrap' class='td-label-small'><fmt:message key="label.status.date"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td nowrap='nowrap' class='td-small'><bean:write name="bean" property="tranStatusDate" /></td>
	</tr>
	
	<tr>
 		<td nowrap='nowrap' class='td-label-small'><fmt:message key="label.sender"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td nowrap='nowrap' class='td-small'><bean:write name="bean" property="tranSender" /></td>
		<td> &nbsp; </td>
 		<td nowrap='nowrap' class='td-label-small'><fmt:message key="label.cust.id"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td nowrap='nowrap' class='td-small'><bean:write name="bean" property="tranCustId" /></td>
	</tr>

	<tr>
 		<td nowrap='nowrap' class='td-label-small'><fmt:message key="label.sender.name"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td nowrap='nowrap' class='td-small'><bean:write name="bean" property="tranSenderName" /></td>
		<td> &nbsp; </td>
 		<td nowrap='nowrap' class='td-label-small'><fmt:message key="label.sender.name"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td nowrap='nowrap' class='td-small'><bean:write name="bean" property="tranReceiver" /></td>
	</tr>

	<tr>
 		<td nowrap='nowrap' class='td-label-small'><fmt:message key="label.country.code"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td nowrap='nowrap' class='td-small'><bean:write name="bean" property="tranCntryCd" /></td>
		<td> &nbsp; </td>
 		<td nowrap='nowrap' class='td-label-small'><fmt:message key="label.transaction.type"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td nowrap='nowrap' class='td-small'><bean:write name="bean" property="tranType" /></td>
	</tr>

</table>

<br />

<!--       ===========  tran actions  =======================       -->

<bean:define id="tranActions" name="bean" property="tranActionList" />

<logic:notEmpty name="tranActions">
	<h3><fmt:message key="label.transaction.action.history" /></h3>
	<table border='1' cellpadding='1' cellspacing='1'>
	<% String dispClass = "TD-SMALL";%>
		<tr>
			<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.date" /></td>
			<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.userid" /></td>
			<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.desc" /></td>
			<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.from.status" /></td>
			<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.from.sub.status" /></td>
			<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.to.status" /></td>
			<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.to.sub.status" /></td>
			<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.cc.auth.id" /></td>
			<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.post.amt" /></td>
			<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.dr.acct.desc" /></td>
			<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.cr.acct.desc" /></td>				
		</tr>
		<logic:iterate id="tranAction" name="tranActions">
		<tr>
			<td nowrap='nowrap' class='<%=dispClass%>' valign='top'>
				<bean:write name="tranAction" property="tranActionDate"/></td>
			<td nowrap='nowrap' class='<%=dispClass%>' valign='top'>
				<bean:write name="tranAction" property="tranCreateUserid"/></td>
			<td nowrap='nowrap' class='<%=dispClass%>' valign='top'>
				<bean:write name="tranAction" property="tranReasDesc"/></td>
			<td nowrap='nowrap' class='<%=dispClass%>' valign='top' align='center'>
				<bean:write name="tranAction" property="tranOldStatCode"/></td>
			<td nowrap='nowrap' class='<%=dispClass%>' valign='top' align='center'>
				<bean:write name="tranAction" property="tranOldSubStatCode"/></td>
			<td nowrap='nowrap' class='<%=dispClass%>' valign='top' align='center'>
				<bean:write name="tranAction" property="tranStatCode"/></td>
			<td nowrap='nowrap' class='<%=dispClass%>' valign='top' align='center'>
				<bean:write name="tranAction" property="tranSubStatCode"/></td>
			<td nowrap='nowrap' class='<%=dispClass%>' valign='top'>
				<bean:write name="tranAction" property="tranConfId"/> &nbsp; </td>
			<td nowrap='nowrap' class='<%=dispClass%>' valign='top' align='right'>
				<bean:write name="tranAction" property="tranPostAmt"/> &nbsp; </td>
			<td nowrap='nowrap' class='<%=dispClass%>' valign='top'> &nbsp;
				<bean:write name="tranAction" property="drGLAcctDesc"/></td>
			<td nowrap='nowrap' class='<%=dispClass%>' valign='top'> &nbsp;
				<bean:write name="tranAction" property="crGLAcctDesc"/></td>
		<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
			else dispClass = "TD-SHADED-SMALL"; %>
		</logic:iterate>
	</table>

</logic:notEmpty>

<br />

<table>	
	<logic:equal name="bean" property="state" value="update">
	
	<tr>
 		<td  nowrap="nowrap" class="td-label-small">New Status<fmt:message key="label.suffix"/> &nbsp; </td>
		<td> &nbsp; </td>
		<td  nowrap='nowrap' class='td-small'>
			<bean:define id="list" name="bean" property="substatusList" />
			<html:select name="bean" property="newSubstatus" size="1">
				<html:options collection="list" property="value" labelProperty="label"/>
			</html:select></td>
	</tr>
	
	<tr><td colspan="3"> &nbsp; </td></tr>
	
	<tr>
		<td nowrap='nowrap' class="td-small" colspan="3">
			<input type="submit" name="submitUpdate" value="Update Status" /></td>
	</tr>
	</logic:equal>

	<logic:equal name="bean" property="state" value="new">
	<tr>
		<td nowrap='nowrap' class="td-small" colspan="3">
		<input type="submit" name="submitNew" value="Fix Another Transaction" /></td>
	</tr>
	</logic:equal>

</table>
</logic:notEqual>

</form>
</center>


		</td>
	</tr>
</table>
</body>
</html>
