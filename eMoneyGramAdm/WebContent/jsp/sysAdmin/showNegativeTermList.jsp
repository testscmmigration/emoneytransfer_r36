<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<center>
	<span class="error">
		<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
	</span>
<span class="message">
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</span>
</center>

<html:form action="/showNegativeTermList.do">

<h3><fmt:message key="label.negativetermlist.title"/></h3>
<div id="formButtonsAdd">

	<table border="0" width="100%">
		<tr>
		<td  valign="top">
					<logic:equal name="auth" property="addNegativeTermList" value="true">
					<html:submit property="submitAdd" value="Add to Negative List"
						onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';" />
				</logic:equal>
			</td>
		<td valign="top">&nbsp;</td>
			
		</tr>
		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>

	</table>
	</div>
	<fieldset>
		<legend class="td-label-small"><fmt:message key="label.negativetermlist.search.legend" /></legend>

	<table border="0" width="100%">
		<tr>
			<td width="100" valign="top"><fmt:message key="label.negativetermlist.type" /></td>
			<td>
				<html:select property="negativeType" size="1">
					<html:option value="">- Select Type -</html:option>
<!-- <html:optionsCollection name="negativeTermListForm" property="purposeTypes"/>   -->
					<html:options collection="types" property="value" labelProperty="label" />
				</html:select> &nbsp; &nbsp; &nbsp;
			</td>
		</tr>
	
		<tr>
			<td width="100" valign="top"><fmt:message key="label.negativetermlist.string" /></td>
			<td>
				<html:text name="negativeTermListForm" property="negativeTerm" size="20" />
					<html:submit property="submitSearch" value="Search" 
						onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';"/>
			</td>
		</tr>
	
		<tr>
			<td colspan="2">
				<div id="formButtonsSearch">
					<html:submit property="submitShowAll" value="Show All"
						onclick="formButtonsSearch.style.display='none';processing.style.visibility='visible';" />
				</div>
			</td>
		</tr>
	</table>
	</fieldset>

	<div id="processing" style="visibility: hidden"><span
		class="PROCESSING">Your request is in process ....</span></div>
		
	<logic:notEmpty name="negativeTermList">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
					<logic:equal name="auth" property="addNegativeTermList" value="true">
						<TD nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
							<fmt:message key="label.action" /></TD>
					</logic:equal>
	 				<TD nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
						<fmt:message key="label.negativetermlist.type" /></TD>		
					<TD nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
						<fmt:message key="label.negativetermlist.string" /></TD>		
					<TD nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
						Last Update Date</TD>		
					<TD nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
						User ID</TD>		
				</tr>
				<logic:iterate id="negativeTerm" name="negativeTermList">
				<tr>
					<logic:equal name="auth" property="addNegativeTermList" value="true">
            			<td nowrap='nowrap' class='<%=dispClass%>'>
							<a href='../../showNegativeTermList.do?submitShowAll=showAll&negativeType=
								<bean:write filter="false" name="negativeTerm" property="purposeTypeCode"/>&negativeTerm=
								<bean:write filter="false" name="negativeTerm" property="negativeString"/>&negativeNum=
								<bean:write filter="false" name="negativeTerm" property="idNumber"/>&action=delete'>Delete</a>
						</td>
					</logic:equal>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="negativeTerm" property="purposeTypeCode" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="negativeTerm" property="negativeString" /></td>				
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="negativeTerm" property="createDate" /></td>				
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="negativeTerm" property="createUserId" /></td>				
				</tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>
	
</html:form>

