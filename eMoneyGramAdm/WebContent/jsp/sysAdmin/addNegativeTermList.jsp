<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/textCounter.js" language="JavaScript"></script>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>
<span class="message">
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</span>
</center>

<html:form action="/addNegativeTermList.do" focus="negativeTerm">

<h3><fmt:message key="label.negativetermlist.add.title"/></h3>

	<table border="0" width="100%">

	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>

	<tr>
		<td width="100" valign="top"><fmt:message key="label.negativetermlist.type" /></td>
		<td>
			<html:select property="negativeType" size="1">
				<html:options collection="types" property="value" labelProperty="label" />
			</html:select> &nbsp; &nbsp; &nbsp;
		</td>
	</tr>
	
	<tr>
		<td width="100" valign="top"><fmt:message key="label.negativetermlist.string" /></td>
		<td width="200">
			<html:text name="negativeTermListForm" property="negativeTerm" size="20" />
		</td>
	    <td nowrap="nowrap">
	    	<span class="error"><html:errors property="negativeTerm"/></span>
		</td>
 	</tr>
	<tr>
		<td colspan="3">
			<div id="formButtonsAdd">
				<html:submit property="submitProcess" value=" Process " 
					onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';"/>
				<html:submit property="submitCancel" value=" Cancel " 
					onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';"/>			
			</div>
		</td>
	</tr>
	
	</table>
	

	<div id="processing" style="visibility:hidden" >
		<span class="PROCESSING"><fmt:message key="message.in.process" />
		</span>
	</div>

</html:form>
