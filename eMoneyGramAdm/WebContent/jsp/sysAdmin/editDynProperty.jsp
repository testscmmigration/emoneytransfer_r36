<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<script language="JavaScript" type="text/javascript">

function validate(form)
{
	fldVal = form.propVal.value.toLowerCase();
	fldType = form.dataTypeCode.value;
	form.propVal.focus();
	
	if (fldVal.length == 0)
	{
		alert("Please enter Property Value");
		return false;
	}

	if (fldType == "boolean" && !(fldVal == "true" || fldVal == "false"))
	{
		alert ("Property Value must be 'true' or 'false'");
		return false;
	} 

	if (fldType == "char" && fldVal.length > 50)
	{
		alert ("Max length for a Property Value is 50");
		return false;
	} 
	
	if (fldType == "int" && fldVal.length > 8)
	{
		alert ("Max length for a Property Value is 8");
		return false;
	} 
	
	if (fldType == "int")
	{
		var charpos = fldVal.search("[^0-9]"); 
		if (charpos >= 0)
		{
			alert ("Property Value must be a numeric value");
			return false;
		}
	} 
	
	if (fldType == "float" || fldType == "double")
	{
		if (fldVal.length > 20)
		{
			alert ("Max length for a Property Value is 20");
			return false;
		}
		
		var pos = fldVal.indexOf(".");
		var str1 = fldVal;
		var str2 = "";
		
		if (pos > 0)
		{
			str1 = fldVal.substr(0, pos);
			str2 = fldVal.substr(pos + 1);
			var charpos = str2.search("[^0-9]"); 
			if (charpos >= 0)
			{
				alert ("Property Value must be a decimal value");
				return false;
			}
		}
		
		var charpos = str1.search("[^0-9]"); 
		if (charpos >= 0)
		{
			alert ("Property Value must be a decimal value");
			return false;
		}
	} 
	
	return true;
}
</script>

<html:messages id="message" property="headerMessage">
	<c:out value="${message}"/>
</html:messages>
	
<h3>
	<fmt:message key="label.edit.system.properties" />
</h3>

<fieldset class="fieldset1">
	<legend class="legend">
		<fmt:message key="label.valid.property.list" />: 
	</legend>

	<logic:empty name="goodList">
		<fmt:message key="msg.no.valid.property.found" />: 
	</logic:empty>
	
	<logic:notEmpty name="goodList">

	<% java.lang.String dispClass = "td-small"; %>

	<table border='0' width='100%' cellpadding='1' cellspacing='1'>
		<thead>
			<tr>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.prop.name" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.prop.type" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.prop.value" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.prop.desc" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.admin.flag" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.multivalue.flag" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.data.type.code" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.create.date" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.create.userid" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.last.update.date" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.last.update.userid" /></td>
				<td class='TD-HEADER-SMALL'> &nbsp; </td>
			</tr>
		</thead>
		<tbody>
			<logic:iterate id="goodBean" name="goodList">
			<bean:define id="key" name="goodBean" property="propKey" />
				<tr>
					<html:form action="/updateDynProperty.do" onsubmit="return validate(this)">
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="key" property="propName" />
						<html:hidden name="key" property="propName"/></td>
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="key" property="propType" />
						<html:hidden name="key" property="propType"/></td>
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<logic:equal name="goodBean" property="editable" value="true">
						<logic:notEqual name="key" property="propVal" value="xAdd a New Value Here">
							<html:text name="key" property="propVal" /> &nbsp;
							<input type="submit" class="transbtn2" name="submitUpdate" value="Update" /> &nbsp;
						</logic:notEqual>
						<logic:equal name="key" property="propVal" value="xAdd a New Value Here">
							<input type="text" name="propVal" /> &nbsp;
							<input type="submit" class="transbtn2" name="submitAdd" value="Add" /> &nbsp;
						</logic:equal>						
						</logic:equal>
						<logic:notEqual name="goodBean" property="editable" value="true">
							<bean:write name="key" property="propVal" />
							<html:hidden name="key" property="propVal"/>
						</logic:notEqual>
						<span class="error"><html:errors property='property'/></span></td>
					<td class="<%=dispClass%>" valign="top">
						<bean:write name="goodBean" property="propDesc" /></td>
						<html:hidden name="goodBean" property="propDesc"/>
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="goodBean" property="adminUseFlag" /></td>
						<html:hidden name="goodBean" property="adminUseFlag"/>
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="goodBean" property="multivalueFlag" />
						<html:hidden name="goodBean" property="multivalueFlag"/>
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="goodBean" property="dataTypeCode" />
						<html:hidden name="goodBean" property="dataTypeCode"/>
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="goodBean" property="createDate" />
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="goodBean" property="createUserid" />
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="goodBean" property="lastUpdateDate" />
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="goodBean" property="lastUpdateUserid" />
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<span class="error">
							<html:errors property="property"/>
						</span></td>
					</html:form>
				</tr>

			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small"; 
				} else {
					dispClass = "td-shaded-small";
				}  %>
				
			</logic:iterate>
		</tbody>
	</table>
	</logic:notEmpty>
</fieldset>
<br />

<logic:notEmpty name="inDbList">

<fieldset class="fieldset1">
	<legend class="legend">
		<fmt:message key="label.in.db.property.list" />: 
	</legend>


	<% java.lang.String dispClass = "td-small"; %>

	<table border='0' width='100%' cellpadding='1' cellspacing='1'>
		<thead>
			<tr>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.prop.name" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.prop.type" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.prop.value" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.prop.desc" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.admin.flag" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.multivalue.flag" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.data.type.code" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.create.date" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.create.userid" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.last.update.date" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.last.update.userid" /></td>
				<td class='TD-HEADER-SMALL'> &nbsp; </td>
			</tr>
		</thead>
		<tbody>
			<logic:iterate id="inDbBean" name="inDbList">
			<bean:define id="key" name="inDbBean" property="propKey" />
				<tr>
					<html:form action="/updateDynProperty.do">
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="key" property="propName" />
						<html:hidden name="key" property="propName"/></td>
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="key" property="propType" />
						<html:hidden name="key" property="propType"/></td>
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="key" property="propVal" />
						<html:hidden name="key" property="propVal"/></td>
					<td class="<%=dispClass%>" valign="top">
						<bean:write name="inDbBean" property="propDesc" /></td>
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="inDbBean" property="adminUseFlag" /></td>
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="inDbBean" property="multivalueFlag" />
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="inDbBean" property="dataTypeCode" />
						<html:hidden name="inDbBean" property="dataTypeCode"/>
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="inDbBean" property="createDate" />
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="inDbBean" property="createUserid" />
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="inDbBean" property="lastUpdateDate" />
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="inDbBean" property="lastUpdateUserid" />
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<span class="error">
							<html:errors property="property"/>
						</span></td>
					</html:form>
				</tr>

			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small"; 
				} else {
					dispClass = "td-shaded-small";
				}  %>
				
			</logic:iterate>
		</tbody>
	</table>
</fieldset>
<br />
</logic:notEmpty>

<logic:notEmpty name="inClassList">
<fieldset class="fieldset1">
	<legend class="legend">
		<fmt:message key="label.in.class.property.list" />: 
	</legend>

	<% java.lang.String dispClass = "td-small"; %>

	<table border='0' width='100%' cellpadding='1' cellspacing='1'>
		<thead>
			<tr>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.prop.name" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.prop.type" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.prop.value" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.prop.desc" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.admin.flag" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.multivalue.flag" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.data.type.code" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.create.date" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.create.userid" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.last.update.date" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.last.update.userid" /></td>
				<td class='TD-HEADER-SMALL'> &nbsp; </td>
			</tr>
		</thead>
		<tbody>
			<logic:iterate id="inClassBean" name="inClassList">
			<bean:define id="key" name="inClassBean" property="propKey" />
				<tr>
					<html:form action="/updateDynProperty.do">
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="key" property="propName" />
						<html:hidden name="key" property="propName"/></td>
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="key" property="propType" />
						<html:hidden name="key" property="propType"/></td>
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="key" property="propVal" />
						<html:hidden name="key" property="propVal"/></td>
					<td class="<%=dispClass%>" valign="top">
						<bean:write name="inClassBean" property="propDesc" /></td>
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="inClassBean" property="adminUseFlag" /></td>
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="inClassBean" property="multivalueFlag" />
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="inClassBean" property="dataTypeCode" />
						<html:hidden name="inClassBean" property="dataTypeCode"/>
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="inClassBean" property="createDate" />
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="inClassBean" property="createUserid" />
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="inClassBean" property="lastUpdateDate" />
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<bean:write name="inClassBean" property="lastUpdateUserid" />
					<td nowrap="nowrap" class="<%=dispClass%>" valign="top">
						<span class="error">
							<html:errors property="property"/>
						</span></td>
					</html:form>
				</tr>

			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small"; 
				} else {
					dispClass = "td-shaded-small";
				}  %>
				
			</logic:iterate>
		</tbody>
	</table>
</fieldset>
</logic:notEmpty>
