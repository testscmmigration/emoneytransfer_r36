<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page import="org.owasp.esapi.ESAPI"%>

<CENTER>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

</CENTER><br>

<%
 String selectedCountry = ESAPI.encoder().encodeForHTMLAttribute((String)request.getSession().getAttribute("selectedCountry"));
%>

<h3><fmt:message key="label.country.selection.list"/></h3><br>
<html:form action="/addCountryException.do">
<logic:notEmpty name="excps">
<p><b>Country:</b> <%= selectedCountry%></p>
	<table border='1' cellpadding='1' cellspacing='1'>
		<tbody>
			<tr>
			<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.country.code" /></TD>		
			<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.country.legacy.code" /></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.country.name" /></TD>
			</tr>
			<logic:iterate id="countryInfo" name="excps">
			<tr>
						<td nowrap='nowrap' class='TD-SMALL' align='center' valign='top'><a
							href='../../editCountryException.do?cntryId=<bean:write 
					filter="false" name="countryInfo" property="countryCode"/>&selectedCountry=<%= selectedCountry %>'>
								<bean:write filter="false" name="countryInfo"
									property="countryCode" />
						</a>
						</td>
						<td nowrap='nowrap' class='TD-SMALL' align='center' valign='top'>
					<bean:write filter="false" name="countryInfo" property="countryLegacyCode"/></td>
				<td class='TD-SMALL' valign='top'>
					<bean:write filter="false" name="countryInfo" property="countryName" /></td>
			<tr>
			</logic:iterate>
		</tbody>			      
	</table>
</logic:notEmpty>

</html:form>
