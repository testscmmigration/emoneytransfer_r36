<%@ page import="emgadm.background.BackgroundProcessAction" %>
<%@ page import="emgadm.background.BackgroundProcessMaster" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String runState = BackgroundProcessMaster.isRunning()?
"is running":"is not running";

String lastTransactionProcessed 
	= "" ;
	//+ BackgroundProcessAction.getLastTranProcessed();
int totalProcessed = 0;//BackgroundProcessAction.getTotalProcessedSinceStartup();
int totalExceptions = 0;//BackgroundProcessAction.getMasterProcessExceptionCount();

%>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

<h3>Administer Auto Processing</h3>
</center>

<div style="margin:auto; position: relative; border: 1px solid #303030; font-family: verdana, arial, sans; background-color: #8080a0; width: 75%; padding: 1em;">
		<img style="float:right;" src="../../images/
		<% 
		if ( BackgroundProcessMaster.isRunning()) { 
			%>scope_on.gif<% 
		} else { 
			%>scope_off.gif<%  
		} 
		%>">
	<form action="<%=basePath%>backgroundProcessingAdmin.do" method="get">		
		<div id="partnerSiteSelect_div">Select Program: 
			<html:select name="backgroundProcessingForm" property="partnerSiteId" size="1" styleId="partnerSiteId" >
				<html:option value="ALL"><fmt:message key="option.all"/></html:option>
				<html:option value="MGO">MGO</html:option>
				<html:option value="MGOUK">MGO UK</html:option>
		    </html:select>
		</div>		
		<input style="margin:3px;" name="act" type="submit" value="Start"><br/>
		<input style="margin:3px;" name="act" type="submit" value="Stop"><br/>
		<input style="margin:3px;" name="act" type="submit" value="Refresh"><br/>
	<p/>
	Processor Run State: <%=runState%><br/>
	Last Transaction Processed:  <%=lastTransactionProcessed%><br/>
	Total Processed since Server Startup:   <%=totalProcessed%><br/>
	Total Exceptions since Server Startup:   <%=totalExceptions%><br/>
	
	</form>
</div>
