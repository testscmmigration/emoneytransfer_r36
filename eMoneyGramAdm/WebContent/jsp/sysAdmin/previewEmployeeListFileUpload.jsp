<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/showUser.js" language="JavaScript"></script>

<html:form action="/previewEmployeeListFileUpload.do" >
<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>
</center>

<h3><fmt:message key="label.file.employee.upload.title" /></h3>
<br>

	<table class="TD-SMALL" border="0" width="100%">
	<tr>
		<td width="32%" class="TD-HEADER-SMALL">
			<b>&nbsp;<fmt:message key="label.file.Information" /></b>
		</td>
		<td>
			&nbsp;&nbsp;&nbsp;
		</td>
		<td width="32%" class="TD-HEADER-SMALL">
			<b>&nbsp;<fmt:message key="label.file.preview.counts" /></b>
		</td>
		<td>
			&nbsp;&nbsp;&nbsp;
		</td>
		<td width="32%" class="TD-HEADER-SMALL">
			<b>&nbsp;<fmt:message key="label.file.upload.counts" /></b>
		</td>		
	</tr>
	<tr>
		<td valign="top">
			<table class="TD-SHADED-SMALL" border="0" width="100%" >
		
			<tr>
				<td width="80">
					<b><fmt:message key="label.file.name" />&nbsp;:</b>
				</td>
				<td>
					<bean:write filter="false" name="uploadFile" property="theFile.fileName"/>
				</td>
			</tr>
			<tr>
				<td width="80">
					<b><fmt:message key="label.file.size" />&nbsp;:</b>
				</td>
				<td>
					<bean:write filter="false" name="uploadFile" property="theFile.fileSize"/>&nbsp;bytes
				</td>		
			</tr>
			<tr>
				<td colspan="2">
					&nbsp;
				</td>		
			</tr>
			
			</table>
		</td>
		<td>&nbsp;&nbsp;&nbsp;</td>		
		<td>
			<table class="TD-SHADED-SMALL" border="0" width="100%">
				<tr>
					<td width="150">
						<b><fmt:message key="label.file.total.rows" />&nbsp;:</b>
					</td>
					<td>
						<bean:write filter="false" name="uploadFile" property="totalRows"/>
					</td>		
				</tr>
			
				<tr>
					<td width="150">
						<b><fmt:message key="label.file.total.good.rows" />&nbsp;:</b>
					</td>
					<td>
						<bean:write filter="false" name="uploadFile" property="totalGoodRows"/>
					</td>		
				</tr>		
				<tr>
					<td width="150">
						<b><fmt:message key="label.file.total.failed.rows" />&nbsp;:</b>
					</td>
					<td>
						<bean:write filter="false" name="uploadFile" property="totalFaultyRows"/>
					</td>		
				</tr>		

			</table>
		</td>
		<td>&nbsp;&nbsp;&nbsp;</td>		
		<td valign="top">
			<table class="TD-SHADED-SMALL" border="0" width="100%">
				<tr>
					<td width="170">
						<b><fmt:message key="label.file.total.upload.rows" />&nbsp;:</b>
					</td>
					<td>
						<bean:write filter="false" name="uploadFile" property="totalUploadRows"/>
					</td>		
				</tr>
			
				<tr>
					<td width="170">
						<b><fmt:message key="label.file.total.upload.good.rows" />&nbsp;:</b>
					</td>
					<td>
						<bean:write filter="false" name="uploadFile" property="totalUploadGoodRows"/>
					</td>		
				</tr>		
				<tr>
					<td width="170">
						<b><fmt:message key="label.file.total.upload.failed.rows" />&nbsp;:</b>
					</td>
					<td>
						<bean:write filter="false" name="uploadFile" property="totalUploadFaultyRows"/>
					</td>		
				</tr>		

			</table>
		</td>
		
	</tr>
	</table>
	
	
	<table class="TD-SMALL" border="0" width="100%">
	<tr>
		<td colspan="2">
			<logic:equal name="uploadclick" value="0"> 		
				<span id="formSubmitUpload">
					<html:submit property="submitUpload" value="Upload" 
						onclick="formSubmitUpload.style.display='none';processing.style.visibility='visible';"/>&nbsp;
				</span>					
			</logic:equal>
			<span id="formSubmitDone">
				<html:submit property="submitCancel" value="Done" />
			</span>					
				
		</td>
	</tr>
	</table>
	
	<div id="processing" style="visibility:hidden" >
		<span class="PROCESSING"><fmt:message key="message.in.process" />
		</span>
	</div>
	
	<table class="TD-SMALL" border="0" width="100%">

	<tr>
		<td colspan="2">
			&nbsp;
		</td>
	</tr>

	<tr>
		<td colspan="2">
			<b>File Contents</b>
		</td>
	</tr>

	<tr>
		<td nowrap='nowrap' colspan="2">
			<table border="0" width="100%">
				<tr>
					<td class='TD-HEADER-SMALL'>
						&nbsp;<fmt:message key="label.file.data" />
					</td>
					<td class='TD-HEADER-SMALL'>
						&nbsp;<fmt:message key="label.file.error" />
					</td>
				</tr>
				<%String dispClass = "TD-SHADED-SMALL";%>
				
				<logic:iterate id="uploadFileRow" property="fileContent" name="uploadFile">
					<tr>
						<td nowrap='nowrap' class='<%=dispClass%>'>
							<bean:write filter="false" name="uploadFileRow" property="row"/>
						</td>
						<td class='<%=dispClass%>'>
							<font color="FF0000">
								<bean:write filter="false" name="uploadFileRow" property="error"/>&nbsp;
							</font>
						</td>
					</tr>
					<%  
						if (dispClass.equals("TD-SHADED-SMALL")) {
							dispClass = "TD-SMALL"; 
						} else {
							dispClass = "TD-SHADED-SMALL";
						}  
					%>					
				</logic:iterate>
			</table>
		</td>
	</tr>
	
	</table>


	

</html:form>