<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<meta http-equiv="refresh" content="5" />

<h3>Oracle Data Source Monitor</h3>

<html:form action="/showODSInfo">

	<%String dispClass = "TD-SMALL";%>
	<table border='0' cellpadding='1' cellspacing='1' width='100%'>
		<tr>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'>DB Name</TD>
			<logic:equal name="noCache" value="N">
			<td nowrap='nowrap' class='TD-HEADER-SMALL'>Connection Cache Name</TD>
			</logic:equal>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'>Attribute</TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'>Value</TD>
		</tr>
			
		<logic:iterate id="info" name="infoList">
		<tr>
			<td nowrap='nowrap' class='<%=dispClass%>'><bean:write name="info" property="dbName" /></td>
			<logic:equal name="noCache" value="N">
			<td nowrap='nowrap' class='<%=dispClass%>'><bean:write name="info" property="cacheName" /></td>
			</logic:equal>
			<td nowrap='nowrap' class='<%=dispClass%>'><bean:write name="info" property="attr" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<logic:greaterEqual name="info" property="seq" value="40">
				<logic:lessEqual name="info" property="seq" value="50">
					<font color="red">
				</logic:lessEqual>
				</logic:greaterEqual>
				<bean:write name="info" property="value" /></font>
				</td>

			<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
				else dispClass = "TD-SHADED-SMALL"; %>	
		</logic:iterate>

	</table>

</html:form>
