<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/datePicker.js" language="JavaScript"></script>
<script src="../../Display/jscript/setOtherRangeValue.js" language="JavaScript"></script>

<center>
<span class="error">
	<html:errors property='<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>' />
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

</center>

<html:form action="/eventMonQuery">

<h3>System Event Monitor Data Query</h3>

<table border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tr>
		<td	nowrap="nowrap" class="td-label-small"><fmt:message key="label.event.name" />: </td>
		<td	nowrap="nowrap" class="td-small">
			<html:select name="eventMonQueryForm" property="event" size="1">
				<html:options collection="events" property="value" labelProperty="label"/>
			</html:select></td>
		<td nowrap="nowrap" class="td-small"> &nbsp; </td>
		<td	nowrap="nowrap" class="td-label-small"><fmt:message key="label.begin.date" />: </td>	
		<td	nowrap="nowrap" class="td-small">
			<html:text size="10" name="eventMonQueryForm" property="beginDate" 
				onchange="setOtherRangeValue(this, eventMonQueryForm.beginDate);" />
				<img border="0" src="../../images/calendar.gif" 
				onclick="show_calendar('eventMonQueryForm.beginDate');" 
				onmouseover="window.status='Pick a Date';return true;" 
				onmouseout="window.status='';return true;" />
			<span class="error"><html:errors property='beginDate'/></span></td>
		<td nowrap="nowrap" class="td-small"> &nbsp; </td>
		<td	nowrap="nowrap" class="td-label-small"><fmt:message key="label.end.date" />: </td>	
		<td	nowrap="nowrap" class="td-small">
			<html:text size="10" name="eventMonQueryForm" property="endDate" 
				onchange="setOtherRangeValue(this, eventMonQueryForm.endDate);" />
				<img border="0" src="../../images/calendar.gif" 
				onclick="show_calendar('eventMonQueryForm.endDate');" 
				onmouseover="window.status='Pick a Date';return true;" 
				onmouseout="window.status='';return true;" />
			<span class="error"><html:errors property='endDate'/></span></td>
		<td nowrap="nowrap" class="td-small"> &nbsp; </td>
		<td nowrap="nowrap" class="td-small" valign="middle" rowspan="2">
			<html:submit property="submitGo" value="Refresh Data" /></td>
	</tr>
	<tr>
		<td nowrap="nowrap" class="td-small" colspan="9"><b>For each selected day:</b> &nbsp; &nbsp; &nbsp;
			<html:radio name="eventMonQueryForm" property="timeType" value="A">Use 24 hours total</html:radio> &nbsp; &nbsp;
			<html:radio name="eventMonQueryForm" property="timeType" value="R">
				Only hours between
				<html:select name="eventMonQueryForm" property="startTime" size="1">
					<html:options collection="hours" property="value" labelProperty="label"/>
				</html:select>
				&nbsp; and &nbsp;
				<html:select name="eventMonQueryForm" property="endTime" size="1">
					<html:options collection="hours" property="value" labelProperty="label"/>
				</html:select>
			</html:radio>
			<span class="error"><html:errors property='timeRange'/></span>
		</td>
	</tr>
</table>

<br />

<center>
<logic:present name="resultList">
<logic:notEmpty name="resultList">

<table border='0' cellpadding='2' cellspacing='2'>
	<tr>
		<td nowrap="nowrap" class="td-label-small">Selected Event: </td>
		<td nowrap="nowrap" class="td-small">
			<bean:write name="eventMonQueryForm" property="selEvent" />
			<html:hidden property="selEvent" /></td>
	</tr>
	<tr>
		<td nowrap="nowrap" class="td-label-small">Selected Date Range: </td>
		<td nowrap="nowrap" class="td-small">
			<bean:write name="eventMonQueryForm" property="selDateRange" />
			<html:hidden property="selDateRange" /></td>
	</tr>
	<tr>
		<td nowrap="nowrap" class="td-label-small">Selected Time Range: </td>
		<td nowrap="nowrap" class="td-small">
			<bean:write name="eventMonQueryForm" property="selTimeRange" />
			<html:hidden property="selTimeRange" /></td>
	</tr>
</table>

<br />

<table border='0' cellpadding='0' cellspacing='0' width='80%'>
	<tr>
		<td  nowrap='nowrap' class='td-small' colspan='6' align='right'><a 
			href="../../ExcelReport.emt?id=resultList" target="excel">
			<fmt:message key="msg.view.or.download.excel" /></a></td>
	</tr>
	<tr>
		<td nowrap="nowrap" class="td-header-small" align="center"><b>Date</b></td>
		<td nowrap="nowrap" class="td-header-small" align="center" align="center"><b>Time Period</b></td>
		<td nowrap="nowrap" class="td-header-small" align="center"><b>Count</b></td>
		<td nowrap="nowrap" class="td-header-small" align="center"><b>High</b></td>
		<td nowrap="nowrap" class="td-header-small" align="center"><b>Average</b></td>
		<td nowrap="nowrap" class="td-header-small" align="center"><b>Low</b></td>
	</tr>
	<%String dispClass = "td-shaded-small"; %>
	<logic:iterate id="result" name="resultList">
	<tr>
		<td nowrap="nowrap" class="<%=dispClass%>" align="center">
			<bean:write name="result" property="date" /></td>
		<td nowrap="nowrap" class="<%=dispClass%>" align="center">
			<bean:write name="result" property="timePeriod" /></td>
		<td nowrap="nowrap" class="<%=dispClass%>" align="center">
			<bean:write name="result" property="count" /></td>
		<td nowrap="nowrap" class="<%=dispClass%>" align="center">
			<bean:write name="result" property="high" /></td>
		<td nowrap="nowrap" class="<%=dispClass%>" align="center">
			<bean:write name="result" property="average" /></td>
		<td nowrap="nowrap" class="<%=dispClass%>" align="center">
			<bean:write name="result" property="low" /></td>
	</tr>
			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small"; 
				} else {
					dispClass = "td-shaded-small";
				}  %>
	</logic:iterate>
</table>

</logic:notEmpty>
</logic:present>
</center>	
</html:form>
