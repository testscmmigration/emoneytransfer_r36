<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 


<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

<html:form action="/login" focus="userID">

	<br/>
	<table border='0'>
	<tr>
		<td  class="TD-LABEL">User ID:</td>
		<td colspan="2"><html:text property="userID" maxlength="50" size="25\" autocomplete=\"off" /><span class="error"> <html:errors property="userID"/></span></td>
	</tr>
	<tr>
		<td  class="TD-LABEL">Password:</td>
		<td colspan="2"><html:password property="userPassword" maxlength="30" size="25\" autocomplete=\"off"></html:password>
			<span class="error"><html:errors property="userPassword"/></span>
		</td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td></td>
		<td colspan="2">	<input type="submit" name="submit" value="Login"/>
		</td>
	</tr>
	</table>

	<p/>

</html:form>
