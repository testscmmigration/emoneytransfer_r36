<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>

</center>

<html:form action="/showBillerLimits.do" focus="agentNameSearch">

<h3><fmt:message key="label.emg.biller.limits.list" /></h3>

	<table border="0" width="400">

	<tr>
		<td width="100"><b><fmt:message key="label.agent.name.search.key" />&nbsp;: </b> </td>
		<td width="10">&nbsp;</td>
		<td>
			<html:text name="showBillerLimitsForm" property="agentNameSearch" size="12" maxlength="12" />
			<html:submit property="submitSearchKey" value="Search" 
			onclick="formButtons.style.display='none';processing.style.visibility='visible';" /> 
		</td>
	</tr>	

	<tr>
		<td width="100"><b><fmt:message key="label.agent.name.search.code" />&nbsp;:</b> </td>
		<td width="10">&nbsp;</td>
		<td>
			<html:text name="showBillerLimitsForm" property="billerCodeSearch" size="4" maxlength="4" />
			<html:submit property="submitSearchCode" value="Search" 
			onclick="formButtons.style.display='none';processing.style.visibility='visible';" /> 
		</td>
	</tr>	

	</table>
	
<div id="formButtons">	
	 <html:submit property="submitShowAll" value="Show All" 
 		onclick="formButtons.style.display='none';processing.style.visibility='visible';" /> &nbsp; &nbsp;
	<logic:equal name="auth" property="editAgentLimit" value="true">
		<html:submit property="submitAdd" value="Add a Biller" 
			onclick="formButtons.style.display='none';processing.style.visibility='visible';" /> 
	</logic:equal>
</div >
<div id="processing"  style="visibility: hidden">
	<span class="PROCESSING"><fmt:message key="message.in.process" /></span>
</div >					
	
<logic:notEmpty name="billers">
	<%String dispClass = "TD-SMALL";%>	
	<table>
		<tbody>
			<tr>
			<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.id" /></TD>		
			<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.name" /></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.city" /></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.state" /></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.limit.amount" /></TD>		
			<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.payment.profile.id" /></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.receive.code" /></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.status" /></TD>
			</tr>
			<logic:iterate id="biller" name="billers">
			<tr>
				<logic:equal name="auth" property="editAgentLimit" value="true">
				    <td nowrap='nowrap' class='<%=dispClass%>'><a href='../../changeLimit.do?id=<bean:write filter="false" name="biller" property="id"/>'
				    	><bean:write filter="false" name="biller" property="id" /></a></td>
				</logic:equal>
				<logic:notEqual name="auth" property="editAgentLimit" value="true">
					<td nowrap='nowrap' class='<%=dispClass%>'>
				    	<bean:write filter="false" name="biller" property="id" /></td>
				</logic:notEqual>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write filter="false" name="biller" property="name"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write filter="false" name="biller" property="city" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write filter="false" name="biller" property="state" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
					<bean:write filter="false" name="biller" property="limit"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write filter="false" name="biller" property="paymentProfileId"/></td>

				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write filter="false" name="biller" property="receiveCode"/></td>
				
				<% String act = "De-Activate"; %>
				<logic:equal name="biller" property="status" value="NAT">
					<% act = "Activate"; %>
				</logic:equal>
				
				<logic:equal name="auth" property="editAgentLimit" value="true">
					<td nowrap='nowrap' class='<%=dispClass%>'><bean:write filter="false" name="biller" property="status"/> - <a href='../../showBillerLimits.do?id=<bean:write 
						filter="false" name="biller" property="id" />&action=<%=act%>'>(<%=act%>)</a></td>
				</logic:equal>
				<logic:equal name="auth" property="editAgentLimit" value="false">
					<td nowrap='nowrap' class='<%=dispClass%>'> <bean:write filter="false" name="biller" property="status"/> </TD>		
				</logic:equal>
				
				
				
			<tr>
			<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
				else dispClass = "TD-SHADED-SMALL"; %>	
			</logic:iterate>
		</tbody>			      
	</table>
</logic:notEmpty>

</html:form>
