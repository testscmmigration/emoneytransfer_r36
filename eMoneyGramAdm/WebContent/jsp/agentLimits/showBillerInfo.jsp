<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<CENTER>
<span class="error">
	<html:errors />
</span>


</CENTER>

<h3><fmt:message key="label.Biller.Info"/></h3>

<html:form action="/showBillerInfo.do" focus="submitReturn">
<html:hidden name="showBillerInfoForm" property="id" />
<html:hidden name="showBillerInfoForm" property="detail" />
<html:hidden name="showBillerInfoForm" property="receiveCode" />
<html:hidden name="showBillerInfoForm" property="agentId" />
<html:hidden name="showBillerInfoForm" property="agentName" />
<html:hidden name="showBillerInfoForm" property="agentCity" />
<html:hidden name="showBillerInfoForm" property="limitAmt" />

	<br />
	<table border="0">
	    <tbody>
	        <tr>
	            <td class="TD-LABEL"><fmt:message key="label.biller.location.receive.code"/>: </td>
	            <td><bean:write name="showBillerInfoForm" property="receiveCode" /></td>
	        </tr>
	        <tr>
	            <td class="TD-LABEL"><fmt:message key="label.biller.location.id"/>: </td>
	            <td><bean:write name="showBillerInfoForm" property="agentId" /></td>
	        </tr>
	        <tr>
	            <td class="TD-LABEL"><fmt:message key="label.biller.location.name"/>: </td>
	            <td><bean:write name="showBillerInfoForm" property="agentName" /></td>
	        </tr>
	        <tr>
	            <td class="TD-LABEL"><fmt:message key="label.biller.location.city"/>: </td>
	            <td><bean:write name="showBillerInfoForm" property="agentCity" /></td>
	        </tr>
	        <tr>
	            <td class="TD-LABEL"><fmt:message key="label.dollar.amount.limit"/>: </td>
	            <td><bean:write name="showBillerInfoForm" property="limitAmt" /></td>
			</tr>
	        <tr>
	            <td class="TD-LABEL"><fmt:message key="label.payment.profile.id"/>: </td>
	            <td><bean:write name="showBillerInfoForm" property="paymentProfileId" /></td>
			</tr>
	        <tr>
	            <td colspan="2"> &nbsp; </td>
	        </tr>
	        <tr>
			    <td colspan="2" align="center">
			    <logic:notEqual name="showBillerInfoForm" property="detail" value="Y">
					<html:submit property="submitReturn" value="Return to Transaction Queue" />
				</logic:notEqual>	
			    <logic:equal name="showBillerInfoForm" property="detail" value="Y">
					<html:submit property="submitReturn" value="Return to Transaction Detail" />
				</logic:equal>	
			    </td>
			</tr>
	    </tbody>
	</table>

</html:form>
