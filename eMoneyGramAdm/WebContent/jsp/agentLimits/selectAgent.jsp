<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<CENTER>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>

</CENTER>

<h3><fmt:message key="label.agent.selection.list"/></h3>

<html:form action="/agentSelect.do" focus="agentName" >
<html:hidden property="corpSelection" />

	<table border="0">
	    <tbody>
	        <tr>
	            <td class="TD-LABEL"><fmt:message key="label.biller.main.office.id"/>: </td>
	            <td><bean:write name="corp" property="agentId" scope="session" /></td>
	        </tr>
	        <tr>
	            <td class="TD-LABEL"><fmt:message key="label.biller.main.office.name"/>: </td>
	            <td><bean:write name="corp" property="agentName" scope="session" /></td>
	        </tr>
	        <tr>
	            <td  class="TD-LABEL"><fmt:message key="label.biller.main.office.city"/>: </td>
	            <td><bean:write name="corp" property="agentCity" scope="session" /></td>
	        </tr>
	    </tbody>
	</table><br />
	<table border="0">
		<tbody>
			<tr>
				<td>
					<html:select property="agentSelection" size="12">
						<bean:define id="agents" name="agentSelectForm" property="agents" />
					    <html:options collection="agents" labelProperty="displayName" property="agentId"/>
	                </html:select>
				</td>

				<td>
					<input type="submit" name="submitAgent" value="Process Selection" />
				</td>
			</tr>
		</tbody>
	</table>
	<br/>
	<table border="0">
		<tbody>
			<tr>
				<td class="TD-LABEL">
					<fmt:message key="label.narrow.list1"/>:
				</td>
				
				<td class="TD-INPUT">
					<html:text property="agentName" size="20"	maxlength="35" />
				</td>
				
				<td>
					<html:submit property="submitAgentFilter" value="Narrow the List" /> &nbsp; &nbsp;
					<html:submit property="submitClearFilter" value="Show All" /><br />
				</td>
			</tr>
			<tr>
			    <td colspan="3" align="center">
			    	<html:submit property="submitReturn" value="Back to Biller Main Offices Select" />
			    	<html:submit property="submitExit" value="Cancel Adding Biller Limit" />
			    </td>
			</tr>
		</tbody>
	</table>

</html:form>
