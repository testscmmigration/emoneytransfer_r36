<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ page import="java.util.List"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script src="../../Display/jscript/textCounter.js" language="JavaScript"></script>
<script src="../../Display/jscript/showPopup.js" language="JavaScript"></script>
<script src="../../Display/jscript/consumerStatus.js"
	language="JavaScript"></script>
	
<fmt:setTimeZone value="${timeZone}" />

<html:form action="/viewDocumentDetails" >

<%
			String dispClass = "td-small";
			String dispClass2 = "TD-SHADED3-SMALL";
		%>
<img height="20" src="../../images/trans.gif" width="10">

	<c:if test="${not empty documentList}">
		<fieldset class="fieldset1">
			<legend class="legend">
				<fmt:message key="label.view.documents" />
				:
			</legend>
			<table width="100%">
					<thead>

						<tr>
							<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message
									key="label.document.date" />
							</td>
							<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message
									key="label.document.type" />
							</td>
							<td nowrap="nowrap" class='TD-HEADER-SMALL'>
							</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="acty" items="${documentList}">
							<tr>
								<td nowrap="nowrap" class="<%=dispClass%>"><fmt:formatDate
								value="${acty.docCreatedTime}" pattern="dd/MMM/yyyy HH:mm:ss z" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${acty.docTypeCode}" /></td>
								<td nowrap="nowrap" class="<%=dispClass%>"><a
							href='../../viewDocumentDetails.do?imgId=<c:out value="${acty.docImgId}"/>&fileName=<c:out value="${acty.docImgFileName}"/>&action=ViewIdImage'
							onclick="openWindow('<c:out value="${pageContext.request.contextPath}" />/viewDocumentDetails.do?imgId=<c:out value="${acty.docImgId}"/>&fileName=<c:out value="${acty.docImgFileName}"/>&action=ViewIdImage');return false;">
								View Document </a></td>		
							</tr>
							<%
								if (dispClass.equals("td-shaded-small")) {
												dispClass = "td-small";
											} else {
												dispClass = "td-shaded-small";
											}
							%>

						</c:forEach>
					</tbody>
			</table>
		</fieldset>
		<br />
	</c:if>

</html:form>