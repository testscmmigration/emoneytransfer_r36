<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/datePicker.js" language="JavaScript"></script>
<script src="../../Display/jscript/setOtherRangeValue.js" language="JavaScript"></script>

<center>
<span class="error">
	<html:errors property='<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>' />
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

<fmt:setTimeZone value="${timeZone}" />

<html:form action="/viewAddressHistory" >

</center>
<h3>Customer Address History - <c:out value="${paddressCount}" ></c:out> address record(s)</h3>
<br />
<a href='../../showCustomerProfile.do?custId=<c:out value="${custId}"/>&basicFlag=N'><fmt:message key="link.back.to.customer" /></a>
<logic:notEmpty name="addresses">

<table border='0' cellpadding='0' cellspacing='0' width='100%'>
			<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.create.status" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.createdby" /></td>				
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.addressline1" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.addr2" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.addressline3" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.town.city" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.province" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.country" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.postalcode" /></td>
			</tr>
			
			<% String dispClass = "td-small"; %>
			<logic:iterate id="address" name="addresses">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<fmt:formatDate value="${address.created}" pattern="dd/MMM/yyyy h:mm a z" /> - 
						<bean:write name="address" property="statusCode" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write name="address" property="createdBy" />
					</td>					
					<td  class='<%=dispClass%>' align='left'>
						<bean:write name="address" property="addressLine1" />
					</td>
					<td  class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="addressLine2" />
					</td>
					<td  class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="addressLine3" />
					</td>
					<td  class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="city" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="state" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="isoCountryCode" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="postalCode" /><bean:write name="address" property="zip4" />
					
					</td>
				</tr>
				<% dispClass = dispClass.equals("td-shaded-small") ? "td-small" : "td-shaded-small"; %>
			</logic:iterate>
		</table>

<br />

</logic:notEmpty>
<h3>Additional Non-Profile Addresses - <c:out value="${npaddressCount}" ></c:out> address record(s)</h3>
<br />
<logic:notEmpty name="nonaddresses">

<table border='0' cellpadding='0' cellspacing='0' width='100%'>
			<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.create.status" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.createdby" /></td>				
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.addressline1" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.addr2" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.addressline3" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.town.city" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.province" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.country" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.postalcode" /></td>
			</tr>
			
			<% String dispClass = "td-small"; %>
			<logic:iterate id="address" name="nonaddresses">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<fmt:formatDate value="${address.created}" pattern="MMM dd yy h:mm a z" /> - 
						<bean:write name="address" property="statusCode" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write name="address" property="createdBy" />
					</td>					
					<td  class='<%=dispClass%>' align='left'>
						<bean:write name="address" property="addressLine1" />
					</td>
					<td  class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="addressLine2" />
					</td>
					<td  class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="addressLine3" />
					</td>
					<td  class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="city" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="state" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="isoCountryCode" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="postalCode" /><bean:write name="address" property="zip4" />
					
					</td>
				</tr>
				<% dispClass = dispClass.equals("td-shaded-small") ? "td-small" : "td-shaded-small"; %>
			</logic:iterate>
		</table>

<br />

</logic:notEmpty>


</html:form>

</center>
