<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ page import="java.util.List" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html><title>Person Search Detail </title><head></head>
<style>
#wrapper {
 /*  display:table-cell; */
  width:100%;
margin-left:10px;
margin-right:10px;
text-align:center;
}
#row {
  display:table-row;
}
#first {
  display:table-cell;  
  width:25%;
  background-color:rgb(246, 246, 246);
  vertical-align:top;
}
#gap{
display:table-cell;
width:5px;
}
#second {
  display:table-cell; 
  width:55%;
  vertical-align:top;
}
#third {
  display:table-cell; 
  width:20%;
  background-color:rgb(246, 246, 246);
  vertical-align:top;
}
#tdInside{
 font:9pt Arial,Helvetica;
 background-color:rgb(246, 246, 246) !important;
 white-space:nowrap;
}
#tdgreyed{
 font:8pt Arial,Helvetica;
 background-color:rgb(246, 246, 246) !important;
 white-space:nowrap;
 color:#707070;
}

#bodyUnique{
overflow:scroll !important;
position:relative !important;
}
</style>
<body id="bodyUnique">
<h3>
<fmt:message key="label.BasicPersonSearchDetails"/></h3>
<div>
<% int count = 0; %>
<c:forEach var="ssnEntry" items="${showBasicPersonSearchForm.ssnLinkedMap}">
	
<div id="wrapper">
  
    <div id="first">
    	<table id="showBasicPersonSearchDetails" style="background-color:#F6F6F6 !important" class="miscdataTable" height="100%" width="100%" cellspacing="3" cellpadding="0" border="0">
    			<% if(count < 1){ %>
    			<tr><td style="background-color:#8FB8E6" class='TD-HEADER-SMALL'><fmt:message key="label.BasicPersonSearchDetails.Name"/></td></tr>
    			<%}%>
    			
    			<tr><td><table>
    				<c:forEach var="fullName" items="${ssnEntry.value.personNameSet}">
    					<tr><td id="tdInside" class="td-small">
    					<c:out value="${fullName}" escapeXml="${fullName}" ></c:out>
    				</c:forEach>
    				<tr><td id="tdInside" class="td-small">&nbsp;</td></tr>			
    				<c:forEach var="accDet" items="${ssnEntry.value.accessoryDetailsSet}">
    					<c:if test="${not empty accDet.dob}">
    					<tr><td id="tdgreyed" class="td-small">
    						(<fmt:message key="label.BasicPersonSearchDetails.DOB"/>&nbsp;:&nbsp;
    							<c:out value="${accDet.dob}" escapeXml="${accDet.dob}" ></c:out>  							   							
    							)</td>
    					</tr></c:if>  
    					
    					<!-- brkcets added since lex nex site has them pls dont remove -->
    					<c:if test="${not empty accDet.age}">
	    					<tr><td id="tdgreyed" class="td-small">
	    						(<fmt:message key="label.BasicPersonSearchDetails.Age"/>&nbsp;:&nbsp;
	    					<c:out value="${accDet.age}" escapeXml="${accDet.age}" ></c:out>
	    					 )</td>	 				
	    					
	    					</tr></c:if>
    					
    					
    					<tr><td id="tdInside" class="td-small"><c:if test="${not empty accDet.gender}">
    						Gender&nbsp;:&nbsp;
    							<c:out value="${accDet.gender}" escapeXml="${accDet.gender}" ></c:out></c:if>
    							
    							
    							</td>
    					</tr>
    					
    					<tr><td>&nbsp;</td></tr>
    				</c:forEach>
    				
				</table></td></tr>
		</table>
	</div>
	<div id="gap"></div>
    <div id="second">
   		<table id="showBasicPersonSearchDetails" class="miscdataTable" height="100%" width="100%" cellspacing="3" cellpadding="0" border="0">
		    <% if(count < 1){ %>		    
		    	<tr><td style="background-color:#8FB8E6" class='TD-HEADER-SMALL'><fmt:message key="label.BasicPersonSearchDetails.Address"/></td>
		    	<td style="background-color:#8FB8E6" nowrap="nowrap" class='TD-HEADER-SMALL' width="50%"><fmt:message key="label.BasicPersonSearchDetails.Phone"/></td></tr>	    
		    <%}%>
		    <c:forEach var="address" items="${ssnEntry.value.addressList}">
		    <tr>
		    	<td style="background-color:#F6F6F6 !important">
		    		<table>
			    		<tr><td id="tdInside" class="td-small"><c:out value="${address.addressLine1}" escapeXml="${address.addressLine1}" /></td></tr>
			    		<tr><td id="tdInside" class="td-small"><c:out value="${address.addressLine2}" escapeXml="${address.addressLine2}" /></td></tr>
			    		<tr><td id="tdInside" class="td-small">
			    			<c:if test="${empty address.subCity}">
			    				<c:out value="${address.city}" escapeXml="${address.city}" ></c:out></c:if>
			    			<c:if test="${not empty address.subCity}"><c:out value="${address.subCity}" escapeXml="${address.subCity}"></c:out>,&nbsp;
			    				<c:out value="${address.city}" escapeXml="${address.city}" ></c:out>
			    			</c:if></td></tr>
			    		<tr><td id="tdInside" class="td-small"><c:out value="${address.state}" escapeXml="${address.state}" ></c:out></td></tr>
			    		<tr><td id="tdInside" class="td-small"><c:out value="${address.postalCode}" escapeXml="${address.postalCode}" /></td></tr>
			    		<tr><td id="tdInside" class="td-small">			    				
			    					<c:if test="${not empty address.firstSeenDate and not empty address.lastSeenDate}">
				    					(<c:out value="${address.firstSeenDate}" escapeXml="${address.firstSeenDate}" />&nbsp;<span>-</span>&nbsp;
				    					<c:out value="${address.lastSeenDate}" escapeXml="${address.lastSeenDate}" />)
			    					</c:if>
			    			</td></tr>
			    			<tr><td></td></tr><!-- added for space under addres -->
			    			<tr><td></td></tr>
			    				
		    		</table>
				</td>
				<td width="50%" style="background-color:#F6F6F6 !important">
					<table>
						<c:forEach var="phone" items="${address.bpsPhoneDetails}">
								<tr>
									<c:if test="${not empty phone.number}"><c:if test="${not empty phone.type}"><td id="tdInside" class="td-small">
									<c:out value="${phone.type}" escapeXml="${phone.type}" />:&nbsp;</td></c:if>
										<td id="tdInside" class="td-small"><c:out value="${phone.number}" escapeXml="${phone.number}" /></td></c:if>
									
								</tr>						
					</c:forEach>
					</table>
				</td>
			</tr>
			</c:forEach>
		</table></div>
	<div id="gap"></div>
    <div id="third"> 	
	    <table id="showBasicPersonSearchDetails" class="miscdataTable" height="100%" width="100%" cellspacing="3" cellpadding="0" border="0">
		  <% if(count < 1){ %> <tr><td style="background-color:#8FB8E6" nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message key="label.BasicPersonSearchDetails.SSN"/></td></tr>
		   <%} %>
		    <tr>
		    	<td id="tdInside" class="td-small"><c:out value="${ssnEntry.value.ssnInfo.ssnNumber}" escapeXml="${ssnEntry.value.ssnInfo.ssnNumber}" ></c:out></td>
		    </tr>
		    <tr><td></td></tr><tr><td></td></tr>
		    <tr><td id="tdInside" class="td-small"><c:if test="${not empty ssnEntry.value.ssnInfo.issuedLocation}">
		    							<c:out value="${ssnEntry.value.ssnInfo.issuedLocation}" escapeXml="${ssnEntry.value.ssnInfo.issuedLocation}" ></c:out>:&nbsp;</c:if>
		    		<c:out value="${ssnEntry.value.ssnInfo.issuedStartDate}" escapeXml="${ssnEntry.value.ssnInfo.issuedStartDate}" ></c:out>&nbsp;-&nbsp;
		    		<c:out value="${ssnEntry.value.ssnInfo.issuedEndDate}" escapeXml="${ssnEntry.value.ssnInfo.issuedEndDate}" ></c:out>
		    	</td>
		    </tr>
		    <tr><td></td></tr><tr><td></td></tr>
		    <tr><td id="tdInside" class="td-small"><fmt:message key="label.BasicPersonSearchDetails.lexId"/>&nbsp;:&nbsp;<c:out value="${ssnEntry.value.uniqueId}" escapeXml="${ssnEntry.value.uniqueId}" ></c:out> 
		    </td></tr>
		    </table>
	</div><div id="gap"></div>
 </div>
<div id="wrapper" style="height:3px;background-color:#8FB8E6">&nbsp;</div>
 
  <% count++; %>    
</c:forEach>
</div>
</body></html>
