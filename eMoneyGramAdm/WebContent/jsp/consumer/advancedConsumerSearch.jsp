<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 


<CENTER>


<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>


</CENTER>

<h3><fmt:message key="label.advance.consumer.profile.search" /></h3>
<P class="error">
<html:messages id="message" property='nbrFound' message="true">
	<c:out value="${message}"/>
</html:messages>
</P>

<html:form action="/advancedConsumerSearch.do" focus="searchFirstName" styleId="advancedConsumerSearchForm">
<html:hidden property="firstTime1" />


<table>
	<tbody>
		<tr>
			<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.first.name"/><fmt:message key="label.suffix"/>&nbsp;</td>
			<td class="TD-SMALL"><html:text property="searchFirstName" size="25" maxlength="25" /></td>							
			<td> &nbsp; </td>
			<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.middle.name"/><fmt:message key="label.suffix"/>&nbsp;</td>
			<td class="TD-SMALL"><html:text property="searchMiddleName" size="30" maxlength="20" /></td>							
		</tr>
		<tr>
			<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.last.name"/><fmt:message key="label.suffix"/>&nbsp;</td>
			<td class="TD-SMALL"><html:text property="searchLastName" size="25" maxlength="25" /></td>							
			<td> &nbsp; </td>
			<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.second.last.name"/><fmt:message key="label.suffix"/>&nbsp;</td>
			<td class="TD-SMALL"><html:text property="searchSecondLastName" size="30" maxlength="30" />
				</td>
		</tr>
			<tr>
				<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.phone"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td class="TD-SMALL"><html:text property="searchPhone" size="20" maxlength="20" />
				<span class="error"><html:errors property="searchPhone"/></span></td>
				<td></td>
				<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.email.id"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td class="TD-SMALL"><html:text property="searchLoginId" size="30" maxlength="30" /></td>
			<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="TD_SMALL" colspan="2"><fmt:message key="label.10.digits.phone.number"/></td>
				<td></td>
				<td align="right" nowrap="nowrap" class="td-label-small"><fmt:message key="label.cust.premier.code"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">					
					<html:select property="searchPrmrCode" size="1">
						<html:option value=""><fmt:message key="option.select.an.option"/></html:option>												
						<html:options collection="custPrmrCodes"  property="custPrmrCode" labelProperty="custPrmrDesc" />
					</html:select>				
				</td>
			</tr>
			<tr>
			
		</tr>
		<tr>
			<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.credit.card"/><fmt:message key="label.suffix"/>&nbsp;</td>
			<td class="TD-SMALL"><html:text property="searchCC4" size="4" maxlength="4" /> (last 4) 
				<span class="error"><html:errors property="searchCC4"/></span></td>
			<td> &nbsp; </td>
			<!-- <td nowrap="nowrap" class="td-label-small"><fmt:message key="label.ssn.mask"/><fmt:message key="label.suffix"/>&nbsp;</td> 
			<td class="TD-SMALL"><html:text property="searchSSN" size="4" maxlength="4" /> (last 4) 
				<span class="error"><html:errors property="searchSSN"/></span></td> -->
		</tr>
		<tr>
			<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.bank.acct" /><fmt:message key="label.suffix"/>&nbsp;</td>
			<td class="TD-SMALL"><html:text property="searchBankAcct" size="4" maxlength="4" /> (last 4)							
				<span class="error"><html:errors property="searchBankAcct"/></span></td>
			<td> &nbsp; </td>
			<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.profile.create.date"/><fmt:message key="label.suffix"/>&nbsp;</td>
			<td class="TD-SMALL"><html:text property="searchCreateDate" size="11" maxlength="11" />  (DD/MMM/YYYY)
				<span class="error"><html:errors property="searchCreateDate"/></span></td>
		</tr>
		<tr>
			<td/>
			<td/>
			<td/>
			<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.profile.create.date.to"/><fmt:message key="label.suffix"/>&nbsp;</td>
			<td class="TD-SMALL"><html:text property="searchCreateDateTo" size="11" maxlength="11" />  (DD/MMM/YYYY)
				<span class="error"><html:errors property="searchCreateDateTo"/></span>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.street.address"/><fmt:message key="label.suffix"/>&nbsp;</td>
			<td class="TD-SMALL"><html:text property="searchAddr" size="12" maxlength="30" /> 
				<span class="error"><html:errors property="searchAddr"/></span></td>
			<td> &nbsp; </td>
			<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.city"/><fmt:message key="label.suffix"/>&nbsp;</td>
			<td class="TD-SMALL"><html:text property="searchCity" size="12" maxlength="20" />
				<span class="error"><html:errors property="searchCity"/></span></td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.state"/><fmt:message key="label.suffix"/>&nbsp;</td>
			<td class="TD-SMALL">
				<html:select property="searchState" >
				<html:option value=""><fmt:message key="option.select.an.option"/></html:option>
				<html:optionsCollection name="states" value="stateProvinceCode" label="stateProvinceName"/>
				</html:select>
				<span class="error"><html:errors property="searchState"/></span></td>
			<td> &nbsp; </td>
			<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.zip.code"/><fmt:message key="label.suffix"/>&nbsp;</td>
			<td class="TD-SMALL"><html:text property="searchZip" size="14" maxlength="14" />
				<span class="error"><html:errors property="searchZip"/></span></td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.profile.status"/><fmt:message key="label.suffix"/>&nbsp;</td>
			<td class="TD-SMALL">
				<html:select property="searchProfileStatus">
					<html:option value=""><fmt:message key="option.select.an.option"/></html:option>
					<html:optionsCollection name="custStatusOptions" />
				</html:select>
				<span class="error"><html:errors property="searchprofileStatus"/></span></td>
			<td> &nbsp; </td>
			<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.create.ipaddress"/><fmt:message key="label.suffix"/>&nbsp;</td>
			<td class="TD-SMALL"><html:text property="searchCreateIpAddrId" size="15" maxlength="15" />
				<span class="error"><html:errors property="searchCreateIpAddrId"/></span></td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="td-label-small">Program<fmt:message key="label.suffix"/>&nbsp;</td>
			<logic:equal name="advancedConsumerSearchForm" property="roleZAF" value="false" >
			<td class="TD-SMALL">
				<html:select property="partnerSiteId" styleClass="partnerSiteId" styleId="partnerSiteId"
						size="1">
						<html:option value="All">
							<fmt:message key="option.all" />
						</html:option>
							<html:options collection="partnerSiteIds" property="partnerSiteId"
							labelProperty="partnerSiteId" />
						</html:select>
		 	<span class="error"></span></td>
		 	</logic:equal>
		 	<logic:notEqual name="advancedConsumerSearchForm" property="roleZAF" value="false" >
			<td class="TD-SMALL">
				<html:select property="partnerSiteId" styleClass="partnerSiteId"
						size="1">
							<html:optionsCollection name="advancedConsumerSearchForm"
								property="truncPartnerList" label="partnerSiteId"
								value="partnerSiteId" />
						</html:select>
		 	<span class="error"></span></td>
		 	</logic:notEqual>
			<td> &nbsp; </td>
			<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.sort.report.by"/><fmt:message key="label.suffix"/>&nbsp;</td>
			<td nowrap="nowrap">
				<html:select property="sortBy" size="1">
					<html:option value="custId"><fmt:message key="label.consumer.id"/></html:option>												
					<html:option value="custLogonId"><fmt:message key="label.email.id"/></html:option>												
					<html:option value="custStatCode"><fmt:message key="label.status"/></html:option>												
					<html:option value="custSubStatCode"><fmt:message key="label.sub.status"/></html:option>												
					<html:option value="custFrstName"><fmt:message key="label.first.name"/></html:option>												
					<html:option value="custLastName"><fmt:message key="label.last.name"/></html:option>												
					<html:option value="custPhoneNbr"><fmt:message key="label.phone"/></html:option>												
					<!-- <html:option value="custSSNMask"><fmt:message key="label.ssn.mask"/></html:option>	-->										
					<html:option value="custBrthDate"><fmt:message key="label.cust.birth.date"/></html:option>												
					<html:option value="custCreateDate"><fmt:message key="label.create.date"/></html:option>												
					<html:option value="custCreateIpAddrId"><fmt:message key="label.create.ipaddress"/></html:option>												
					<html:option value="custPrmrCode"><fmt:message key="label.cust.premier.code"/></html:option>
					<html:option value="custCreatSrcWebSite">Program</html:option>	
				</html:select>
			</td>				
		</tr>
		
		<%-- Added by Ankit Bhatt for MBO-128 --%>
		
		<tr>
			<td nowrap="nowrap" class="td-label-small">Profile Type<fmt:message key="label.suffix"/>&nbsp;</td>
			<td class="TD-SMALL">
				<html:select property="profileType" styleClass="profileType" size="1">
					<html:option value="ALL"><fmt:message key="option.all"/></html:option>
					<html:option value="GUEST">Guest</html:option>
					<html:option value="REGISTERED">Registered</html:option>					
				</html:select>
				<span class="error"></span></td>
			<td> &nbsp; </td>
			<td nowrap="nowrap"></td>
			<td nowrap="nowrap"> </td>
		</tr>
		
		
		<%-- Ended --%>
		<tr>
			
			<td nowrap="nowrap" class="td-label-small">
				<div class="europeanIDrow">
				ID Type
				<fmt:message key="label.suffix"/>&nbsp;
				</div>
				<div class="deIDrow">
				Document Status
				<fmt:message key="label.suffix"/>&nbsp;
				</div>
				
			</td>
			
			<td class="TD-SMALL">
				<div class="europeanIDrow">			
					<html:select styleClass="additionalIdType" property="additionalIdType" size="1">
						<html:option value="">All</html:option>												
						<html:optionsCollection name="additionalIdTypes" />
					</html:select>
				</div>
				<div class="deIDrow">			
					<html:select styleClass="additionalDocStatus" property="additionalDocStatus" size="1">
						<html:option value="">All</html:option>												
						<html:optionsCollection name="additionalDocStatuses"/>
					</html:select>
				</div>
			</td>
				
			<td> &nbsp; </td>
			<td nowrap="nowrap" class="td-label-small">
			<div class="europeanIDrow">ID Number<fmt:message key="label.suffix"/>&nbsp;
			</div></td>
			<td class="TD-SMALL">
				<div class="europeanIDrow searchId">
						<input type="text" class="searchId" value="" name="searchId" maxlength="15"/>
				</div>
			</td>
		
		</tr>
		
		<logic:present name="advancedConsumerSearchForm" property="searchPasswordHash">
		<logic:notEqual name="advancedConsumerSearchForm" property="searchPasswordHash" value="">
		<tr>
			<td nowrap="nowrap" colspan="5"> &nbsp; &nbsp; &nbsp;
					<html:checkbox name="advancedConsumerSearchForm" property="removePasswordSearch">
					Remove password as search criteria
					</html:checkbox>
			</td>
		</tr>
		</logic:notEqual>
		</logic:present>

	</tbody>
</table>


<div id="formButtons">

		<html:submit property="submitSearch" value="Search"  
		onclick="formButtons.style.display='none';processing.style.visibility='visible';" />

</div>
	
<div id="processing" style="visibility: hidden">
	<table>
		<tr>
			<td class="PROCESSING"><fmt:message key="message.in.process" /></td>
		</tr>
	</table>
</div >

<logic:present name="profileList">
<%String dispClass = "TD-SMALL";%>
<fieldset>
	<legend class="td-label-small"><fmt:message key="label.advance.consumer.profile.search"/> - 
		
	<html:messages id="message" property='nbrFound' message="true">
		<c:out value="${message}"/>
	</html:messages>
     
	</legend>

<table>
	<tbody>
		<tr><th colspan="14" align="right"><a href="../../ExcelReport.emt?id=profileList" target="excel">
			<fmt:message key="msg.view.or.download.excel" /></a>
			</th></tr>
		<tr>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'> &nbsp; </th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.profile.type"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL' colspan='3'><fmt:message key="label.consumer.id"/></th>		
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.email.id"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.status"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sub.status"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.first.name"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.last.name"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.phone"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.consumer.address"/></th>	
			<!-- <th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.ssn.mask"/></th> -->
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.cust.birth.date"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.create.date"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.create.ipaddress"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.cust.premier.code"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.trans.program"/></th>
		</tr>
		<logic:iterate id="profile" name="profileList">
		<tr>
			<td nowrap='nowrap' class='<%=dispClass%>'><font color="red"><b>
				<bean:write name="profile" property="taintedText" /></b></font></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="profileType" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custId" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
			    <a href='../../showCustomerProfile.do?custId=<bean:write 
			    name="profile" property="custId" />&basicFlag=N'>Detail</a></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
			    <a href='../../showCustomerProfile.do?custId=<bean:write 
			    name="profile" property="custId" />&basicFlag=Y'>Basic</a></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custLogonId" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custStatDesc" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custSubStatDesc" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custFrstName" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custLastName" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custPhoneNbr" /></td>
				<td class='<%=dispClass%>'>
				<bean:write name="profile" property="custAddr"/></td>
			<!-- <td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custSSNMask" /></td> -->
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custBrthDate" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custCreateDate" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custCreateIpAddrId" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custPrmrCode" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custCreatSrcWebSite" /></td>
		</tr>
		<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
			else dispClass = "TD-SHADED-SMALL"; %>	
		</logic:iterate>
	</tbody>		      
</table>
</fieldset>
</logic:present>

</html:form>

