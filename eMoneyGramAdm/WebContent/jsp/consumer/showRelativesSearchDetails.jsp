<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html><title>Person Search Detail </title>
<style>
#bodyUnique {
	overflow: scroll !important;
	position: relative !important;
}

#wrapper { /*  display:table-cell; */
	width: 70%;
	text-align: center !important;
	margin: 0 auto;
}

#tdInside {
	font: 9pt Arial, Helvetica;
	background-color: #F6F6F6 !important;
}

#tdFirstDepth {
	font: 9pt Arial, Helvetica;
	background-color: #dddddd !important;
}
#tdGreyed{
 font:8pt Arial,Helvetica;
 background-color:inherit;
 white-space:nowrap;
 color:#707070;
}
#tdIcon {
	vertical-align: middle;
	display: table-cell;
	margin-right: 0em;
}
</style>
<head></head>
<body id="bodyUnique">
	<form>

		<div id="wrapper">
			<h3 align="left">
				<fmt:message key="label.relativessearchdetails.header" />
			</h3>
			<br />
			<c:choose>
			<c:when
				test="${not empty showRelativesSearchForm.relativesSearchResponse}">
	<table class="miscdataTable" height="100%" width="100%"
						cellspacing="1" cellpadding="0" border="0">
				<c:choose>
				
				<c:when
					test="${not empty showRelativesSearchForm.relativesSearchResponse.relatives.relativesList}">
					
						<tr>

							<td style="background-color: #8FB8E6; width: 35% !important"
								class='TD-HEADER-SMALL'><fmt:message
									key="label.relativessearchdetails.relatives" /></td>
							<td style="background-color: #8FB8E6; width: 35% !important"
								class='TD-HEADER-SMALL'><fmt:message
									key="label.relativessearchdetails.addresses" /></td>
							<td style="background-color: #8FB8E6; width: 30% !important"
								class='TD-HEADER-SMALL'><fmt:message
									key="label.relativessearchdetails.depth" /></td>
						</tr>

						<c:set var="relativesList"
							value="${showRelativesSearchForm.relativesSearchResponse.relatives.relativesList}"
							scope="request"></c:set>
						<jsp:include page="relativesDetails.jsp" />
					<!-- </table> -->
				</c:when>
				 <c:otherwise>
				<%-- <tr><td colspan="3" nowrap='nowrap' class="td-small">
				<fmt:message key="label.relativessearchdetails.relativesnotfound"></fmt:message></td></tr> --%>
					
					<h3>
						<fmt:message key="label.relativessearchdetails.relativesnotfound"></fmt:message>
					</h3>
					<br/>
				</c:otherwise> 
				</c:choose>
				<c:choose>
				<c:when
					test="${not empty showRelativesSearchForm.relativesSearchResponse.associates.associateList}">

					<!-- <table class="miscdataTable" height="100%" width="100%"
						cellspacing="1" cellpadding="0" border="0"> -->
						<tr>
							<td style="background-color: #8FB8E6; width: 35% !important"
								class='TD-HEADER-SMALL'><fmt:message
									key="label.relativessearchdetails.associates" /></td>
							<td style="background-color: #8FB8E6; width: 35% !important"
								class='TD-HEADER-SMALL'><fmt:message
									key="label.relativessearchdetails.addresses" /></td>
							<td style="background-color: #8FB8E6; width: 30% !important"
								class='TD-HEADER-SMALL'><fmt:message
									key="label.relativessearchdetails.depth" /></td>
						</tr>
						<c:forEach var="associate"
							items="${showRelativesSearchForm.relativesSearchResponse.associates.associateList}">
							<tr>
								<td id="tdFirstDepth" nowrap='nowrap' class="td-small"><table>
										<tr>
											<td id="tdFirstDepth" nowrap='nowrap' class="td-small"><b>
													<c:if
														test="${not empty associate.associateIdentity.prefix}">
														<c:out value="${associate.associateIdentity.prefix}"></c:out>
													</c:if> <c:if
														test="${not empty associate.associateIdentity.firstName}">
														<c:out value="${associate.associateIdentity.firstName}"></c:out>
													</c:if> <c:if
														test="${not empty associate.associateIdentity.middleName}">
														<c:out value="${associate.associateIdentity.middleName}"></c:out>
													</c:if> <c:if
														test="${not empty associate.associateIdentity.lastName}">
														<c:out value="${associate.associateIdentity.lastName}"></c:out>
													</c:if> </b>
											</td>
										</tr>
										<c:if test="${not empty associate.associateIdentity.dob}">
											<tr>
												<td id="tdFirstDepth" nowrap='nowrap' class="td-small">Date
													of birth&nbsp;:&nbsp; <c:out
														value="${associate.associateIdentity.dob}"></c:out>
												</td>
											</tr>
										</c:if>
										<c:if test="${not empty associate.associateIdentity.dod}">
											<tr>
												<td id="tdFirstDepth" nowrap='nowrap' class="td-small">Date
													of death&nbsp;:&nbsp; <c:out
														value="${associate.associateIdentity.dod}"></c:out>
												</td>
											</tr>
										</c:if>
										
										<c:if test="${not empty associate.associateIdentity.gender}">
											<tr>
												<td id="tdFirstDepth" nowrap='nowrap' class="td-small">Gender&nbsp;:&nbsp;
													<c:out value="${associate.associateIdentity.gender}"></c:out>
												</td>
											</tr>
										</c:if>
										<c:if
											test="${not empty associate.associateIdentity.ssnInfo.ssnNumber }">
											<tr>
												<td id="tdFirstDepth" nowrap='nowrap' class="td-small">SSN
													Number&nbsp;:&nbsp;<c:out
														value="${associate.associateIdentity.ssnInfo.ssnNumber}"></c:out>
												</td>
											</tr>
										</c:if>
										<c:if test="${not empty associate.uniqueId}">
											<tr>
												<td id="tdFirstDepth" nowrap='nowrap' class="td-small">LexId&nbsp;:&nbsp;<c:out
														value="${associate.uniqueId}"></c:out>
												</td>
											</tr>
										</c:if>
										<c:if test="${not empty associate.associateIdentity.age}">
											<tr>
												<td id="tdGreyed" nowrap='nowrap' class="td-small">(Age&nbsp;:&nbsp;
													<c:out value="${associate.associateIdentity.age}"></c:out>)
												</td>
											</tr>
										</c:if>
										<c:if test="${associate.associateIdentity.deceased eq 'Y'}">
											<tr>
												<td class="td-small" id="tdFirstDepth" valign="bottom"><div
														style="display: table !important">
														<span style="vertical-align: middle; display: table-cell">
															<img id="tdIcon" "width="18px" height="18px"
															align="right" src="../../images/deceased.png">
														</span> <span style="vertical-align: middle; display: table-cell">
														<fmt:message key="label.relativessearchdetails.deceased"></fmt:message></span>
													</div>
												</td>
											</tr>
										</c:if>
										
									</table>
								</td>
								<td id="tdFirstDepth" nowrap='nowrap' class="td-small">
									<table class="miscdataTable" height="100%" width="100%"
										cellspacing="3" cellpadding="0" border="0">
										<c:forEach var="baseAddress"
											items="${associate.associateAddresses.associateAddressList}">
											<tr>
												<td id="tdFirstDepth" class="td-small"><c:out
														value="${baseAddress.address.formattedAddress}"></c:out></td>
											</tr>
										</c:forEach>

									</table>
								</td>
								<td id="tdFirstDepth" nowrap='nowrap' class="td-small">&nbsp;</td>
							</tr>

						</c:forEach>
					</c:when>
					<c:otherwise>
				<%-- <tr><td colspan="3" nowrap='nowrap' class="td-small">
				<fmt:message key="label.relativessearchdetails.relativesnotfound"></fmt:message></td></tr> --%>
					
					<h3>
						<fmt:message key="label.relativessearchdetails.associatesnotfound"></fmt:message>
					</h3>
					<br/>
				</c:otherwise> 
				</c:choose>
				</table>
			</c:when>			
			<c:otherwise><h3><fmt:message key="label.relativessearchdetails.noresultsfound"></fmt:message></h3>
			</c:otherwise>
			</c:choose>
			
		</div>
	</form>

</body>
</html>