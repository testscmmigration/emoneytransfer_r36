<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<head>
<script>
	$(document).ready(function () {
	 $("#addressList").hide();
	$('.address, .city, .state, .zip').change(function () {
      if($('.address').attr('checked')||$('.city').attr('checked')||$('.state').attr('checked')||$('.zip').attr('checked')) {
    $("#addressList").show();
} else {
    $("#addressList").hide();
}
    });
});
</script></head>
<CENTER>

	<span class="error"> <html:errors
			property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
	</span>

	<h3>
		<html:messages id="message"
			property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>'
			message="true">
			<c:out value="${message}" />
		</html:messages>
	</h3>

</CENTER>

<h3>
	<fmt:message key="label.consumer.profile.search" />
	Selection
</h3>
<html:form action="/consumerSearchSelection.do">
	<html:hidden property="findProfiles" />
	<html:hidden property="includePrmrCode" />

	<fmt:message key="label.find.similar.consumer.profiles" />:
<br />
	<br />

	<table>
		<tr>
			<td nowrap="nowrap" class="td-label-small"><fmt:message
					key="label.consumer.id" />:</td>
			<td>&nbsp; &nbsp; &nbsp;</td>
			<td><bean:write name="consumerSearchSelectionForm"
					property="custId" />
					<html:hidden property="custId"/>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="td-label-small"><fmt:message
					key="label.action.userid" />:</td>
			<td>&nbsp; &nbsp; &nbsp;</td>
			<td><bean:write name="consumerSearchSelectionForm"
					property="custLogonId" />
					<html:hidden property="custLogonId"/>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="td-label-small"><fmt:message
					key="label.first.name" />:</td>
			<td>&nbsp; &nbsp; &nbsp;</td>
			<td><bean:write name="consumerSearchSelectionForm"
					property="custFirstName" />
					<html:hidden property="custFirstName"/>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="td-label-small"><fmt:message
					key="label.last.name" />:</td>
			<td>&nbsp; &nbsp; &nbsp;</td>
			<td><bean:write name="consumerSearchSelectionForm"
					property="custLastName" />
					<html:hidden property="custLastName"/>
			</td>
		</tr>
	</table>
	<br />

	<h4>
		<fmt:message key="label.check.fields" />
		.
	</h4>
	<br />

			<table>
				<tbody>
					<tr>
						<td class="TD-SMALL"><html:checkbox
								property="includeFirstName">
								<b><fmt:message key="label.first.name" />
								</b>
							</html:checkbox>
						</td>
						<td>&nbsp; &nbsp;</td>
						<td class="TD-SMALL"><html:checkbox property="includeLoginId">
								<b><fmt:message key="label.email.id" />
								</b>
							</html:checkbox>
						</td>
					</tr>
					<tr>
						<td class="TD-SMALL"><html:checkbox
								property="includeLastName">
								<b><fmt:message key="label.last.name" />
								</b>
							</html:checkbox>
						</td>
						<td>&nbsp; &nbsp;</td>
						<td class="TD-SMALL"><html:checkbox property="includePhone">
								<b><fmt:message key="label.phone" />
								</b>
							</html:checkbox>
						</td>
					</tr>
					<tr>
						<td class="TD-SMALL"><html:checkbox property="includeAddr" styleClass = "address">
								<b><fmt:message key="label.street.address" />
								</b>
							</html:checkbox>
						</td>
						<td>&nbsp; &nbsp;</td>
						<td class="TD-SMALL"><html:checkbox
								property="includeCreateDate">
								<b><fmt:message key="label.profile.create.date" />
								</b>
							</html:checkbox>
						</td>
					</tr>
					<tr>
						<td class="TD-SMALL"><html:checkbox property="includeCity" styleClass = "city">
								<b><fmt:message key="label.city" />
								</b>
							</html:checkbox>
						</td>
						<td>&nbsp; &nbsp;</td>
						<td class="TD-SMALL"><html:checkbox
								property="includeProfileStatus">
								<b><fmt:message key="label.profile.status" />
								</b>
							</html:checkbox>
						</td>
					</tr>
					<tr>
						<td class="TD-SMALL"><html:checkbox property="includeState" styleClass = "state">
								<b><fmt:message key="label.state" />
								</b>
							</html:checkbox>
						</td>
						<td>&nbsp; &nbsp;</td>
						<%--added by Ankit Bhatt for 314 --%>
						<c:choose>
						<c:when test="${fn:toLowerCase(findProfiles.profileType) eq 'guest'}">
						<td class="TD-SMALL"><html:checkbox
								property="includePasswordHash"  disabled="true">
								<b><fmt:message key="label.password" />
								</b>
							</html:checkbox>
						</td>
						</c:when>
						<c:otherwise>
						<td class="TD-SMALL"><html:checkbox
								property="includePasswordHash">
								<b><fmt:message key="label.password" />
								</b>
							</html:checkbox>
						</c:otherwise>
						</c:choose>
					</tr>
					<tr>
						<td class="TD-SMALL"><html:checkbox property="includeZip" styleClass = "zip">
						<b> <fmt:message key="label.post.code" /> </b>
					</html:checkbox>
						</td>
						<td>&nbsp; &nbsp;</td>
						<td class="TD-SMALL"><html:checkbox
								property="includeCreateIpAddrId">
								<b><fmt:message key="label.create.ipaddress" />
								</b>
							</html:checkbox></td>
					</tr>

					<!-- 
		<tr>
			<td class="TD-SMALL">
				<html:checkbox property="includePrmrCode">
				<b><fmt:message key="label.cust.premier.code"/></b>
				</html:checkbox></td>
			<td> &nbsp; &nbsp; </td>
			<td class="TD-SMALL"> &nbsp; </td>
		</tr>
		 -->

				</tbody>
			</table>
						<table><tbody>
			<tr>
						<td class="TD-SMALL addressList" id = "addressList"><b>
						<logic:greaterThan name="consumerSearchSelectionForm" property="addressListSize" value="1">
						 <fmt:message
									key="label.address.list" /> :</b> <html:select
								name="consumerSearchSelectionForm" property="selectedAddress">
								<html:optionsCollection name="consumerSearchSelectionForm" property="fullAddress" />
							</html:select>
							</logic:greaterThan></td>

					</tr>
			
			</tbody></table>
			
	<br />
	<br />

	<div id="formButtons">

		<html:submit property="submitSelAll" value="Select All"
			onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
		&nbsp; &nbsp; &nbsp;
		<html:submit property="submitDeselAll" value="Deselect All"
			onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
		&nbsp; &nbsp; &nbsp;
		<html:submit property="submitGo" value="Go"
			onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
	</div>

	<div id="processing" style="visibility: hidden">
		<h3>
			<fmt:message key="message.in.process" />
		</h3>
	</div>

</html:form>

