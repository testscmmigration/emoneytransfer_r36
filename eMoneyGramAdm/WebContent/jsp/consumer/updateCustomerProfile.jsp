<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page import="emgshared.util.Constants" %>
<jsp:useBean id="cons" class="emgshared.util.Constants" scope="session"/>

<style>
.consumerProfile {
	font-family: Arial;
	font-size: 11px;
	color: #000;
	width: 95%
}

.consumerProfile td {
	padding: 8px 0;
}

.consumerProfile label {
	font-weight: bold;
}

.consumerProfile b,.consumerProfile label {
	width: 100px;
	float: left;
	display: block;
	text-align: right;
	margin-right: 10px;
}

.consumerProfile input,.consumerProfile select {
	width: 100px;
	border: 1px solid #000;
}

.consumerProfile .stateSelect {
	width: 190px;
}

.consumerProfile .zip input {
	width: 70px;
}

.consumerProfile .check {
	width: 20px;
	float: left;
	border: none;
}

.consumerProfile .miniLabel {
	width: auto;
	margin-right: 15px;
}

.consumerProfile .buttons input {
	width: auto;
	border-top: 1px solid #ffffff;
	border-left: 1px solid #ffffff;
	border-right: 1px solid #333333;
	border-bottom: 1px solid #333333;
}
</style>

<h3>
	<center>
		<span class="error"> <html:errors
				property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
		</span>
	</center>
</h3>

<h3>Update Consumer Profile</h3>

<html:form action="updateCustomerProfile">
	<html:hidden property="partnerSiteId" />
	<logic:equal name="duplicateProfileOverride" value="Y">
		<logic:equal name="allowDuplicate" value="Y">
			<td nowrap="nowrap" class="td-small" align="right"><b>Allow
					Duplicate:</b> <html:checkbox property="allowOverride" /></td>
			<td><span class="error"> <html:messages id="message"
						property="duplicateProfileOverrideErr">
						<c:out value="${message}" />
					</html:messages> </span>
			</td>
			</tr>
		</logic:equal>
	</logic:equal>

	<logic:empty name="duplicateProfileOverride">
		<logic:equal name="allowDuplicate" value="Y">
			<td nowrap="nowrap" class="td-small" align="right">&nbsp;<html:hidden
					value="false" property="allowOverride" /></td>
			<td><span class="error"> <html:messages id="message"
						property="duplicateProfileOverrideErr">
						<c:out value="${message}" />
					</html:messages> </span>
			</td>
			</tr>
		</logic:equal>
	</logic:empty>

	<TABLE border="0" width="1100px" class="consumerProfile">
		<TBODY>
			<tr>
				<td width="245px"><b><fmt:message key="label.first.name" />
						<fmt:message key="label.suffix" /> </b> <html:text
						name="updateCustomerProfileForm" property="custFirstName"
						size="30" /> <span class="error"> <html:messages
							id="message" property="firstName">
							<c:out value="${message}" />
						</html:messages> </span>
				</td>
				<td width="350px">
					<table width="100%" border="0" class="consumerProfile"
						cellspacing="0" cellpadding="0">
						<tr>
							<td width="13%" nowrap="nowrap"><b style=""><fmt:message
										key="label.middle.name" /> <fmt:message key="label.suffix" />
							</b><br> <span style="margin-left: 55px"><fmt:message
										key="label.optional"></fmt:message> </span></td>
							<td><html:text name="updateCustomerProfileForm"
									property="middleName" size="30" />
							</td>
						</tr>
					</table> <span class="error"> <html:messages id="message"
							property="middleName">
							<c:out value="${message}" />
						</html:messages> </span>
				</td>
				<td></td>
			</tr>
			<tr>
				<td><b><fmt:message key="label.last.name" /> <fmt:message
							key="label.suffix" /> </b> <html:text
						name="updateCustomerProfileForm" property="custLastName" size="40" />
					<span class="error"> <html:messages id="message"
							property="lastName">
							<c:out value="${message}" />
						</html:messages> </span>
				</td>
				<td>
					<table width="100%" border="0" class="consumerProfile"
						cellspacing="0" cellpadding="0">
						<tr>
							<td width="13%"><b style=""><fmt:message
										key="label.second.last.name" /> <fmt:message
										key="label.suffix" /> </b><br> 
							<c:if test="${updateCustomerProfileForm.partnerSiteId != 'MGOESP'}">
								<span
									style="margin-left: 55px"><fmt:message
											key="label.optional"></fmt:message> </span>
							</c:if>
							</td>
							<td><html:text name="updateCustomerProfileForm"
									property="custSecondLastName" size="40" /></td>
						</tr>
					</table> <span class="error"> <html:messages id="message"
							property="secondLastName">
							<c:out value="${message}" />
						</html:messages> </span>
				</td>
				<td></td>
			</tr>
			<tr>
				<td><b><fmt:message key="label.street.addressline1" /> <fmt:message
							key="label.suffix" /> </b> <html:text
						name="updateCustomerProfileForm" property="addressLine1" size="30"/>
						 <span class="error"> <html:messages
							id="message" property="addressLine1">
							<c:out value="${message}" />
						</html:messages> </span>
				</td>
				<td><b><fmt:message key="label.town" /> <fmt:message
							key="label.suffix" /> </b> <html:text
						name="updateCustomerProfileForm" property="city" size="20" /> <span
					class="error"> <html:messages id="message" property="city">
							<c:out value="${message}" />
						</html:messages> </span>
				</td>
				
            <c:if test="${updateCustomerProfileForm.hasStatesProvinces == 'Y'}">
					<td width="300px"><b><fmt:message key="label.state" /> <fmt:message
								key="label.suffix" /> </b> <html:select size="1"
							name="updateCustomerProfileForm" property="state">
							<html:option value="">
							Select a State
						</html:option>
							<html:optionsCollection  name="updateCustomerProfileForm" property="statesProvincesList" />
							</html:select>
						<span class="error"> <html:messages id="message"
								property="state">
								<c:out value="${message}" />
							</html:messages> </span>
					</td>
			</c:if>  
				
			</tr>

			<tr>
				<td><b><fmt:message key="label.street.addressline2" /> <fmt:message
							key="label.suffix" /> </b> <html:text
						name="updateCustomerProfileForm" property="addressLine2" size="30"/>
				</td>
				<td class="zip"><b><fmt:message key="label.postal.code" />
						<fmt:message key="label.suffix" /> </b> <html:text
						name="updateCustomerProfileForm" property="postalCode" size="10" />
					<span class="error"> <html:messages id="message"
							property="postalCode">
							<c:out value="${message}" />
						</html:messages> </span>
				</td>
				<td></td>
			</tr>

			<tr>
				<td><b><fmt:message key="label.street.addressline3" /> <fmt:message
							key="label.suffix" /> </b> <html:text
						name="updateCustomerProfileForm" property="addressLine3" size="30"/>
				</td>

			</tr>
			<c:choose>

				<c:when
					test="${updateCustomerProfileForm.partnerSiteId != 'MGOZAF'}">
					<tr>
					<!-- MBO-7310 -->
						<td><b><fmt:message key="label.country.code" /> <fmt:message
									key="label.suffix" /> </b> <html:select
								property="phoneCountryCode">
								<html:option value="">Select one</html:option>
								<html:optionsCollection name="updateCustomerProfileForm"
									property="countryCodeList" />
							</html:select></td>

						<td><b><fmt:message key="label.phone.number" /> <fmt:message
									key="label.suffix" /> </b> <html:text
								name="updateCustomerProfileForm" property="phoneNumber" /> <html:hidden
								name="updateCustomerProfileForm" property="savePhoneNumber" />
							<span class="error"> <html:messages id="message"
									property="phoneNumber">
									<c:out value="${message}" />
								</html:messages> </span>
						</td>
						<td><html:radio property="phoneNumberType" value="{H}"
								styleClass="check" /><label class="miniLabel">Home</label> <html:radio
								property="phoneNumberType" value="{M}" styleClass="check" /><label
							class="miniLabel">Mobile</label></td>
					</tr>
				</c:when>
				<c:otherwise>
					<html:hidden name="updateCustomerProfileForm"
						property="phoneNumber" />
					<html:hidden name="updateCustomerProfileForm"
						property="savePhoneNumber" />
						<!-- MBO-7310 -->
						<td><b><fmt:message key="label.country.code" /> <fmt:message
									key="label.suffix" /> </b> 
									<html:text	name="updateCustomerProfileForm" property="phoneCountryCode" disabled="true" />
									
								
							</td>
						
						
				</c:otherwise>
			</c:choose>

			<tr>
				<td><b><fmt:message key="label.birth.date" /> <fmt:message
							key="label.suffix" /> (dd/MMM/yyyy)</b> <html:text size="10"
						property="birthDateText" /> <span class="error"> <html:messages
							id="message" property="birthDateText">
							<c:out value="${message}" />
						</html:messages> </span>
				</td>
				
				   <!-- FIXME - Have generic logic to set readoOnly attribute based on any dynamic nameValue pair, 'plusEnabled' from 
				                getSourceSiteInfo response after data is fixed in table for all source sites -->
				
					<td><b><fmt:message key="label.loyaltyId" /> <fmt:message
								key="label.suffix" /> </b> 
						<c:choose>

						<c:when test="${updateCustomerProfileForm.partnerSiteId != 'MGOUK' && updateCustomerProfileForm.partnerSiteId != 'MGOZAF' && updateCustomerProfileForm.partnerSiteId != 'MGOCAN' && !EUSites.fromValue(updateCustomerProfileForm.partnerSiteId) && updateCustomerProfileForm.partnerSiteId != 'POSTCAN'}">
							<html:text
							name="updateCustomerProfileForm"
							property="loyaltyPgmMembershipId" readonly="false"/> 
						</c:when>
						<c:otherwise> 
							<html:text
							name="updateCustomerProfileForm"
							property="loyaltyPgmMembershipId" readonly="true"/>
						</c:otherwise>
						</c:choose>
							
							<span
						class="error"> <html:messages id="message"
								property="loyaltyId" >
								<c:out value="${message}" />
							</html:messages> </span>
					</td>
				
				<td></td>
			</tr>
			<tr>
				<td align="center" colspan="3" class="buttons"><html:submit
						property="updateSubmit" value="Update Profile" /> &nbsp;&nbsp; <!-- MBO-3527 commented below line for suppressing the dvs service call -->
					<!-- html:submit property="dvsValidate" value="DVS Validation" /-->
					&nbsp;&nbsp; <html:submit property="updateCancel" value="Cancel" />
					<html:hidden name="updateCustomerProfileForm" property="custId" />
					<html:hidden name="updateCustomerProfileForm" property="plusNumber" />
				</td>
			</tr>
		</TBODY>
	</TABLE>
</html:form>