<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:if
	test="${not empty relativesList}">
	<c:forEach var="relative"
		items="${relativesList}">
		<tr>
			<td id="tdFirstDepth" nowrap='nowrap' class="td-small"><table>
					<tr>
						<td id="tdFirstDepth" nowrap='nowrap' class="td-small"><b>
								<c:if test="${not empty relative.relativeIdentity.prefix}">
									<c:out value="${relative.relativeIdentity.prefix}"></c:out>
								</c:if> <c:if test="${not empty relative.relativeIdentity.firstName}">
									<c:out value="${relative.relativeIdentity.firstName}"></c:out>
								</c:if> <c:if test="${not empty relative.relativeIdentity.middleName}">
									<c:out value="${relative.relativeIdentity.middleName}"></c:out>
								</c:if> <c:if test="${not empty relative.relativeIdentity.lastName}">
									<c:out value="${relative.relativeIdentity.lastName}"></c:out>
								</c:if> </b></td>
					</tr>
					<c:if test="${not empty relative.relativeIdentity.dob}">
						<tr>
							<td id="tdFirstDepth" nowrap='nowrap' class="td-small">Date
								of birth&nbsp;:&nbsp; <c:out
									value="${relative.relativeIdentity.dob}"></c:out></td>
						</tr>
					</c:if>
					<c:if test="${not empty relative.relativeIdentity.dod}">
						<tr>
							<td id="tdFirstDepth" nowrap='nowrap' class="td-small">Date
								of death&nbsp;:&nbsp; <c:out
									value="${relative.relativeIdentity.dod}"></c:out></td>
						</tr>
					</c:if>
					
					<c:if test="${not empty relative.relativeIdentity.gender}">
						<tr>
							<td id="tdFirstDepth" nowrap='nowrap' class="td-small">Gender&nbsp;:&nbsp;
								<c:out value="${associate.associateIdentity.gender}"></c:out></td>
						</tr>
					</c:if>
					<c:if
						test="${not empty relative.relativeIdentity.ssnInfo.ssnNumber }">
						<tr>
							<td id="tdFirstDepth" nowrap='nowrap' class="td-small">SSN
								Number&nbsp;:&nbsp;<c:out
									value="${relative.relativeIdentity.ssnInfo.ssnNumber}"></c:out>
							</td>
						</tr>
					</c:if>
					<c:if test="${not empty relative.uniqueId}">
						<tr>
							<td id="tdFirstDepth" nowrap='nowrap' class="td-small">LexId&nbsp;:&nbsp;<c:out
									value="${relative.uniqueId}"></c:out></td>
						</tr>
					</c:if>
					<c:if test="${not empty relative.relativeIdentity.age}">
						<tr>
							<td id="tdGreyed" nowrap='nowrap' class="td-small">(Age&nbsp;:&nbsp;
								<c:out value="${relative.relativeIdentity.age}"></c:out>)</td>
						</tr>
					</c:if>
					<c:if test="${relative.relativeIdentity.deceased eq 'Y'}">
						<tr>
							<td class="td-small" id="tdGreyed" valign="bottom"><div
									style="display: table !important">
									<span style="vertical-align: middle; display: table-cell">
										<img id="tdIcon" "width="18px" height="18px" align="right"
										src="../../images/warning.png"> </span> <span
										style="vertical-align: middle; display: table-cell">
											<fmt:message key="label.relativessearchdetails.deceased"></fmt:message></span>
								</div></td>
						</tr>
					</c:if>
					
				</table></td>
			<td id="tdFirstDepth" nowrap='nowrap' class="td-small">
				<table class="miscdataTable" height="100%" width="100%"
					cellspacing="3" cellpadding="0" border="0">
					<c:forEach var="baseAddress"
						items="${relative.relativeAddresses.relativeAddressList}">
						<tr>
							<td id="tdFirstDepth" class="td-small"><c:out
									value="${baseAddress.address.formattedAddress}"></c:out>
							</td>
						</tr>
					</c:forEach>

				</table></td>
			<td id="tdFirstDepth" nowrap='nowrap' class="td-small"><c:out value="${relative.depth}"></c:out></td>
		</tr>
		<c:if test="${not empty relative.relatives.relativesList}">

			<c:forEach var="relative" items="${relative.relatives.relativesList}">
				<tr>
					<td id="tdInside" nowrap='nowrap' class="td-small"><table>
							<tr>
								<td id="tdInside" nowrap='nowrap' class="td-small"><b>
										<c:if test="${not empty relative.relativeIdentity.prefix}">
											<c:out value="${relative.relativeIdentity.prefix}"></c:out>
										</c:if> <c:if test="${not empty relative.relativeIdentity.firstName}">
											<c:out value="${relative.relativeIdentity.firstName}"></c:out>
										</c:if> <c:if
											test="${not empty relative.relativeIdentity.middleName}">
											<c:out value="${relative.relativeIdentity.middleName}"></c:out>
										</c:if> <c:if test="${not empty relative.relativeIdentity.lastName}">
											<c:out value="${relative.relativeIdentity.lastName}"></c:out>
										</c:if> </b></td>
							</tr>
							<c:if test="${not empty relative.relativeIdentity.dob}">
								<tr>
									<td id="tdInside" nowrap='nowrap' class="td-small">Date of
										birth&nbsp;:&nbsp; <c:out
											value="${relative.relativeIdentity.dob}"></c:out></td>
								</tr>
							</c:if>
							<c:if test="${not empty relative.relativeIdentity.dod}">
								<tr>
									<td id="tdInside" nowrap='nowrap' class="td-small">Date of
										death&nbsp;:&nbsp; <c:out
											value="${relative.relativeIdentity.dod}"></c:out></td>
								</tr>
							</c:if>
							
							<c:if test="${not empty relative.relativeIdentity.gender}">
								<tr>
									<td id="tdInside" nowrap='nowrap' class="td-small">Gender&nbsp;:&nbsp;
										<c:out value="${associate.associateIdentity.gender}"></c:out>
									</td>
								</tr>
							</c:if>
							<c:if
								test="${not empty relative.relativeIdentity.ssnInfo.ssnNumber }">
								<tr>
									<td id="tdInside" nowrap='nowrap' class="td-small">SSN
										Number&nbsp;:&nbsp;<c:out
											value="${relative.relativeIdentity.ssnInfo.ssnNumber}"></c:out>
									</td>
								</tr>
							</c:if>
							<c:if test="${not empty relative.uniqueId}">
								<tr>
									<td id="tdInside" nowrap='nowrap' class="td-small">LexId&nbsp;:&nbsp;<c:out
											value="${relative.uniqueId}"></c:out></td>
								</tr>
							</c:if>
							<c:if test="${not empty relative.relativeIdentity.age}">
								<tr>
									<td id="tdGreyed" nowrap='nowrap' class="td-small">(Age&nbsp;:&nbsp;
										<c:out value="${relative.relativeIdentity.age}"></c:out>)</td>
								</tr>
							</c:if>
							<c:if test="${relative.relativeIdentity.deceased eq 'Y'}">
								<tr>
									<td class="td-small" id="tdGreyed" valign="bottom"><div
											style="display: table !important">
											<span style="vertical-align: middle; display: table-cell">
												<img id="tdIcon" "width="18px" height="18px" align="right"
												src="../../images/warning.png"> </span> <span
												style="vertical-align: middle; display: table-cell"><fmt:message key="label.relativessearchdetails.deceased"></fmt:message></span>
										</div></td>
								</tr>
							</c:if>
					
						</table></td>
					<td id="tdInside" nowrap='nowrap' class="td-small">
						<table class="miscdataTable" height="100%" width="100%"
							cellspacing="3" cellpadding="0" border="0">
							<c:forEach var="baseAddress"
								items="${relative.relativeAddresses.relativeAddressList}">
								<tr>
									<td id="tdInside" class="td-small"><c:out
											value="${baseAddress.address.formattedAddress}"></c:out>
									</td>
								</tr>
							</c:forEach>

						</table></td>
					<td id="tdInside" nowrap='nowrap' class="td-small"><c:out value="${relative.depth}"></c:out></td>
				</tr>
			</c:forEach>
		</c:if>

	</c:forEach>

</c:if>
