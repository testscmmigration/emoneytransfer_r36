<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<CENTER>

<br/>
	<span class="error"> <html:errors
			property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
	</span>
	<br/>
</CENTER>

<h3>KBA Questionnaire</h3>




<html:form type="emgadm.consumer.SubmitKBAQuestionsForm"
	name="submitKBAQuestionsForm" action="/submitKBAQuestions">


	<div class="tVQuestions">
		<table>

			<tr class="">
				<td valign="top"><div></div></td>
			</tr>
			<logic:iterate id="item" name="getKBAQuestionsForm"
				property="KBAQuestions" indexId="counter">
         <html:hidden name = "submitKBAQuestionsForm" property="questions" value="${getKBAQuestionsForm.KBAQuestions}"/>
				
				<tr class="tVQuestion">
					<td valign="top">
						<div class="exPoint exQuestion"></div> <span
						style="font-weight: bold">Question<%=counter+1%></span> <bean:write name="item"
							property="KBAQuestionDescription" />
					</td>
				</tr>	
				<logic:iterate id="answer" name="item" property="kbaAnswers">

					<tr class="tVAnswer question"<%=counter %>>
						<td valign="top">
							<div class="fakeRadioButton">
								<a href="#"></a>

								<html:radio name="submitKBAQuestionsForm"
									property="answerCode[${counter}]"
									value="${answer.KBAAnswerCode}"  />


								<bean:write name="answer" property="KBAAnswerDescription" />
								<br />

							</div>
							
						</td>
					</tr>
					

				</logic:iterate>

			</logic:iterate>


		</table>

		<br/>
		<html:submit  styleClass="transbtn4" value="Submit Answers"></html:submit>
	  	
		
		<html:button  styleClass="transbtn4" value="Back" property="button" onclick="parent.location='/eMoneyGramAdm/backToCustProfile.do'"></html:button>
		
		
	</div>
<br/>

</html:form>

