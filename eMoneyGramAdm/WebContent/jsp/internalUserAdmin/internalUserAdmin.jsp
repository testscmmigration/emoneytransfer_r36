<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

<%String focus = "newUserID";%>
<c:if test="${internalUserAdminForm.userId!=''}">
	<%focus = "firstName";%>
</c:if>
<html:form action="/internalUserAdmin" focus="<%=focus%>">
<html:hidden property="userId" />
	
	<TABLE border="0">
		<TBODY>
			<TR>
				<c:if test="${internalUserAdminForm.userId!=''}">
					<TD nowrap="nowrap" class="TD-LABEL">User ID:</TD>
					<TD><bean:write name="internalUserAdminForm" property="userId"/></TD>
				</c:if>
			</TR>
			<TR>
				<TD nowrap="nowrap" class="TD-LABEL">
					First Name:
				</TD>
				<TD nowrap="nowrap" >
					<c:out value="${internalUserAdminForm.user.firstName}"/>
				</TD>
			</TR>
			<TR>
				<TD nowrap="nowrap" class="TD-LABEL">
					Last Name:
				</TD>
				<TD nowrap="nowrap" >
					<c:out value="${internalUserAdminForm.user.lastName}"/>
				</TD>
			</TR>
			<TR>
				<TD nowrap="nowrap" class="TD-LABEL">
					Email Address:
				</TD>
				<TD class="nowrap">
 				   <c:out value="${internalUserAdminForm.user.emailAddress}"/>
 				</TD>
			</TR>
			<TR>
				<TD nowrap="nowrap" class="TD-LABEL">
					Application Role:
				</TD>
				<TD class="nowrap">
					<c:out value="${internalUserAdminForm.commaSeparatedRoleIds}"/>
				</TD>
			</TR>
			</TBODY>
	</TABLE>

	<BR/>
	<TABLE border="0">
		<TBODY>
			<TR>
				<TD>
					<div id="formButtons">
						<c:if test="${internalUserAdminForm.userId!=''}">
								<input type="button" name="cancel" value="  Ok  " onclick="parent.location='<html:rewrite page="/internalUserAdmin.do"/>?cancel=edit'"></input>
						</c:if>
					</div >
					
					<div id="processing" style="visibility: hidden">
						<table>
							<tr>
								<td class="PROCESSING"><fmt:message key="message.in.process" /></td>
							</tr>
						</table>
					</div >					
				</TD>
				<TD>
				</TD>
			</TR>			
		</TBODY>
	</TABLE>

</html:form>
