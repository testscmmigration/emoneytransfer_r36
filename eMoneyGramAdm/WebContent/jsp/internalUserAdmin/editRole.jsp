<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>
</center>

<h3>Application Role Maintenance</h3>

<html:form action="/editRole.do">
<html:hidden property="action" />
<html:hidden property="saveAction" />
<html:hidden property="roleId" />
<html:hidden property="dblClickAction" />

<center>
<table>
	<tr>
		<td nowrap='nowrap' class='td-small' align='right'><b>
			<fmt:message key="label.role.name" />: </b></td>
		<td nowrap='nowrap' class='td-small'>
			<logic:notEqual name="editRoleForm" property="saveAction" value="add">
				<bean:write name="editRoleForm" property="roleName" />
				<html:hidden property="roleName" />
			</logic:notEqual>
			<logic:equal name="editRoleForm" property="saveAction" value="add">
				<html:text name="editRoleForm" property="roleName" />
			</logic:equal>
		</td>
		<td nowrap="nowrap"><span class="error">
			<html:errors property="roleName"/></span></td>
	</tr>
</table>
<br />
<table>
	<tr>
		<td nowrap='nowrap' class='td-header-small' align="center">
			Available Commands:
		</td>
		<td nowrap='nowrap' class='td-small'>
			&nbsp;
		</td>
		<td nowrap='nowrap' class='td-header-small' align="center">
			Assigned Commands:
		</td>
	</tr>
	<tr>
		<td nowrap='nowrap' class='td-small'>
			<table border='1'>
				<tr><td>
					<html:select property="unauthCmdId" size="20" multiple="true" style="width:400px;"
						ondblclick="document.editRoleForm.dblClickAction.value='Auth';document.editRoleForm.submit();">
						<html:options collection="available" 
							property="cmdId" labelProperty="cmdDesc" />
					</html:select>
				</td></tr>
			</table>
		</td>
		<td nowrap='nowrap' class='td-small'>
			<table border='0'>
				<tr><td> &nbsp; </td></tr>
				<tr><td><input type="image" name="submitAuth" src="../../images/forward_arrow.gif"  
						border="0" alt="Authorize Commands" 
						onMouseOver="this.src='../../images/forward_arrow1.gif';"
						onMouseOut="this.src='../../images/forward_arrow.gif';"	/></td></tr>
				<tr><td> &nbsp; </td></tr>
				<tr><td><input type="image" name="submitUnauth" src="../../images/backward_arrow.gif"  
						border="0" alt="Unauthorize Commands"
						onMouseOver="this.src='../../images/backward_arrow1.gif';"
						onMouseOut="this.src='../../images/backward_arrow.gif';" /></td></tr>
				<tr><td> &nbsp; </td></tr>
			</table>
		</td>
		<td nowrap='nowrap' class='td-small'>
			<table border='1'>
				<tr><td>
					<html:select property="authCmdId" size="20" multiple="true" style="width:400px;"
						ondblclick="document.editRoleForm.dblClickAction.value='Unauth';document.editRoleForm.submit();">
						<html:options collection="assigned" 
							property="cmdId" labelProperty="cmdDesc" />
					</html:select>
				</td></tr>
			</table>
		</td>	
	</tr>
</table>

<br /><br />
<div id="formButtons">
	<input type="image" name="submitSave" src="../../images/save_button.gif"  
						border="0" alt="Save Role Changes" 
		onclick="formButtons.style.display='none';processing.style.visibility='visible';"
		onMouseOver="this.src='../../images/save_button1.gif';"
		onMouseOut="this.src='../../images/save_button.gif';" />
 		&nbsp; &nbsp; &nbsp;
	<input type="image" name="submitExit" src="../../images/exit_button.gif"  
						border="0" alt="Exit to Application Role List" 
		onMouseOver="this.src='../../images/exit_button1.gif';"
		onMouseOut="this.src='../../images/exit_button.gif';" />
</div >
<div id="processing"  style="visibility: hidden">
	<table>
		<tr>
			<td class="PROCESSING"><fmt:message key="message.in.process" /></td>
		</tr>
	</table>
</div >					



</center>

</html:form>	    	

