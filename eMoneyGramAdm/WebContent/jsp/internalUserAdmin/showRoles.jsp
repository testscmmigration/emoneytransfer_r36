<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page import="java.util.List" %>

<script src="../../Display/jscript/showUser.js" language="JavaScript"></script>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>
</center>

<h3>Application Role List</h3>

<html:form action="/showRoles.do">
<!-- 
	<logic:equal name="auth" value="Y">
		<a href='../../editRole.do?action=add'>Add a new application role</a><br />	
	</logic:equal>
 -->
	<a href='../../internalUserSearch.do?search=Search'>						
		View All Users</a><br />

<br />

	<logic:notEmpty name="roles">
		<%String dispClass = "TD-SMALL";%>		
		<table>
			<tbody>
				<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.role.name" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.role.desc" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					Options</TD>
				<td> &nbsp; </td>
				</tr>

				<logic:iterate id="role" name="roles">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<logic:equal name="auth" value="Y">
							<a href='../../editRole.do?roleId=<bean:write 
							name="role" property="id"/>&action=edit'><bean:write 
							name="role" property="name"/></a>
						</logic:equal>
						<logic:notEqual name="auth" value="Y">
							<bean:write name="role" property="name" />
						</logic:notEqual>
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write name="role" property="description" /></td>
					<logic:equal name="auth" value="Y">
	               	<logic:empty name="role" property="users">
						<td nowrap='nowrap' class='<%=dispClass%>'><a 
							href='../../showRoles.do?delete=<bean:write 
							name="role" property="id"/>&roleName=<bean:write
							name="role" property="name"/>'>Delete</a></td> 
					</logic:empty>
	               	<logic:notEmpty name="role" property="users">
	               		<td>
<!--	               		
	               			<a href='/eMoneyGramAdm/showRoleUsers.do?roleId=<bean:write 
							name="role" property="id"/>' onClick="this.href='javascript:showUserDetail(\'/eMoneyGramAdm/showRoleUsers.do?roleId=<bean:write 
							name="role" property="id"/>\')'">
-->	
							<a href='../../internalUserSearch.do?search=Search&roleID=<bean:write
							name="role" property="id" />'>						

							View Role Users</a></td>
					</logic:notEmpty>
					</logic:equal>
					<logic:equal name="auth" value="N">
						<td> &nbsp; </td>
					</logic:equal>
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>
	
</html:form>	    	

