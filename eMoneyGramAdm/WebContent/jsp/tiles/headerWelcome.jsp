<%@page import="emgadm.dataaccessors.MenuManager"%>
<script src="../../Display/jscript/jquery-1.12.4.min.js" language="JavaScript"></script>
<script src="../../Display/jscript/jquery-migrate-1.4.1.min.js" language="JavaScript"></script>
<script src="../../Display/jscript/jquery-ui-1.12.4.min.js" language="JavaScript"></script>
<script src="../../Display/jscript/jquery.custom.autocomplete.js" language="JavaScript"></script>
<script src="../../Display/jscript/viewRoles.js" language="JavaScript"></script>
<script src="../../Display/jscript/eMGAdm.js" language="JavaScript"></script>
<script src="../../Display/jscript/menu.js" language="JavaScript"></script>
<script src="../../Display/jscript/menuOptionsWelcome.js" language="JavaScript"></script>
<script language="JavaScript">
	<%
	  	emgadm.model.UserProfile user = null;
	  	user=(emgadm.model.UserProfile) session.getAttribute("userProfile");
	  	out.println(MenuManager.getMenu(user)); 
  		out.println(MenuManager.getEndMenu());

	%>
</script>
<div id="main_nav" class="nav">
	<!-- Main menu UL starts here -->
</div>