<%@page import="org.owasp.esapi.StringUtilities"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<%
	boolean showVal=false;
	boolean showTranVal=false;
	String errMsg = "";
	emgadm.model.UserProfile up = (emgadm.model.UserProfile) request
	.getSession()
	.getAttribute(
			emgadm.constants.EMoneyGramAdmSessionConstants.USER_PROFILE);

	//  check if the session is timeout or not
	if (up == null) {
		response.sendRedirect("../../login.do");
		return;
	}

	//  to check if there are admin messages
	String msgCnt = "0";
	String color = "";
	java.util.Map msgMap = emgshared.model.MessageCacheBean
	.getInstance().getMessageMap();
	if (msgMap != null) {
		emgshared.model.UserMessages um = (emgshared.model.UserMessages) msgMap
		.get(up.getUID());
		msgCnt = (um == null ? "0" : String
		.valueOf(um.getUnreadCount()));
		if (msgCnt.equals("")) {
	msgCnt = "0";
		}
	}
	request.setAttribute("msgCnt", msgCnt);
	color = msgCnt.equals("0") ? "black" : "red";

	String tranOk = up.hasPermission("transactionDetail") ? "Y" : "N";
	String custOk = up.hasPermission("showCustomerProfile") ? "Y" : "N";
	request.setAttribute("tranOk", tranOk);
	request.setAttribute("custOk", custOk);

	emgshared.util.DateFormatter df = new emgshared.util.DateFormatter(
	"dd/MMM/yyyy hh:mm a", true);
	String timestamp = df.format(new java.util.Date(System
	.currentTimeMillis()));

	request.setAttribute("tranColl",
	emgadm.actions.EMoneyGramAdmBaseAction.getCollection(
			request, "tranList"));
	request.setAttribute("custColl",
	emgadm.actions.EMoneyGramAdmBaseAction.getCollection(
			request, "custList"));

	if (!emgadm.util.StringHelper.isNullOrEmpty(request
	.getParameter("quickSearch"))) {
		request.getSession().removeAttribute("errMsg");
		request.getSession().removeAttribute("errTran");
		request.getSession().removeAttribute("errTran");

		String tran = request.getParameter("tranId");
		String cust = request.getParameter("custId");

		//	 due security reason removing all the html codes
		if (!emgadm.util.StringHelper.isNullOrEmpty(tran))
	tran = tran.replaceAll("\\<.*?>", "").trim();

		if (!emgadm.util.StringHelper.isNullOrEmpty(cust))
	cust = cust.replaceAll("\\<.*?>", "").trim();

		if (emgadm.util.StringHelper.isNullOrEmpty(tran)
		&& emgadm.util.StringHelper.isNullOrEmpty(cust)) {
	errMsg = "Please enter either a tran id or a cust id.";
		} else if (!emgadm.util.StringHelper.isNullOrEmpty(tran)
		&& !emgadm.util.StringHelper.isNullOrEmpty(cust)) {
	errMsg = "Please enter either a tran id or a cust id, but not both";
		} else if (!emgadm.util.StringHelper.isNullOrEmpty(tran)
		&& tran.length() > 9) {
	errMsg = "Tran id cannot be more than 9 digits long";
		} else if (!emgadm.util.StringHelper.isNullOrEmpty(cust)
		&& cust.length() > 24) {
	errMsg = "Cust ID/Guid cannot be more than 24 characters long";
		} else {
	//  a tran is entered
	if (!emgadm.util.StringHelper.isNullOrEmpty(tran)) {
		if (!org.apache.commons.lang.StringUtils
				.isNumeric(tran)) {
			errMsg = "Tran Id must numeric";
		} else {
			showTranVal=true;
			emgadm.dataaccessors.TransactionManager tm = emgadm.dataaccessors.ManagerFactory
					.createTransactionManager();
			emgshared.model.Transaction tranBean = tm
					.getTransaction(Integer.parseInt(tran));
			if (tranBean == null) {
				errMsg = "Specified transaction does not exist, try again.";
			} else {
				//tran = org.apache.commons.lang.StringEscapeUtils
					//	.escapeJava(StringUtilities.stripControls(tran));
				ESAPI.httpUtilities().sendRedirect(response,"../../transactionDetail.do?tranId="
						+ ESAPI.encoder().encodeForHTML(tran));	
				//response.sendRedirect("../../transactionDetail.do?tranId="
				//		+ ESAPI.encoder().encodeForHTML(tran));
				return;
			}
		}
	}
	//  a cust is entered
	if (!emgadm.util.StringHelper.isNullOrEmpty(cust)) {
		// search by guid
		if (!org.apache.commons.lang.StringUtils
				.isNumeric(cust)) {
			if (cust.length() < 24) {
				errMsg = "Please enter a numeric Cust Id or a valid GUID value.";
			} else {
				emgshared.services.ServiceFactory sf = emgshared.services.ServiceFactory
						.getInstance();
				emgshared.services.ConsumerProfileService cps = sf
						.getConsumerProfileService();
				int customerId = cps.getCustIdByEdirGuid(cust,
						up.getUID());
				if (customerId < 1) {
					errMsg = "Specified customer does not exist, try again.";
				} else {
					response.sendRedirect("../../showCustomerProfile.do?custId="
							+ URLEncoder.encode(String.valueOf(customerId),"UTF-8") + "&basicFlag=" + URLEncoder.encode("N","UTF-8"));
					return;
				}
			}
			// search by cust Id
		} else {
			if (cust.length() > 9)
				errMsg = "Cust Id Numbers must be 9 digits or less";
			else {
				showVal=true;
				int consumerId = Integer.parseInt(cust);
				emgshared.services.ServiceFactory sf = emgshared.services.ServiceFactory
						.getInstance();
				emgshared.services.ConsumerProfileService cps = sf
						.getConsumerProfileService();
				if (!cps.isCustomerExist(consumerId,
						up.getUID())) {
					errMsg = "Specified customer does not exist, try again.";
				} else {
					String showCustomerProfileUrl = "../../showCustomerProfile.do?custId="
							+ URLEncoder.encode(cust,"UTF-8") + "&basicFlag=" + URLEncoder.encode("N","UTF-8");
				//MBO-11074
				java.util.regex.Matcher matcher = null;
				java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("[a-zA-Z0-9-?./_&=$]*");
				if (null != showCustomerProfileUrl) {
					matcher = pattern.matcher(showCustomerProfileUrl);
				}
				if (matcher.matches()) {
						ESAPI.httpUtilities().sendRedirect(response,showCustomerProfileUrl);
						//response.sendRedirect(StringUtilities.stripControls(showCustomerProfileUrl));
				}
					return;
				}
			}
		}
	}
		}
		if (!"".equals(errMsg)) {
			java.util.Map<String, String> transferMap = new java.util.HashMap();
			transferMap.put("errMsg", errMsg);
			transferMap.put("errTran", tran);
			transferMap.put("errCust", cust);
			request.getSession().setAttribute("errDetail", transferMap);
			response.sendRedirect("../../quickSearchError.do");
			return;
		}
	}
%>
<form name="quickSearchForm" method="post" action="quickSearch.jsp">
	<table border='0' cellspacing='1' cellpadding='1'>
		<tr>
			<td nowrap='nowrap' class='td-small'><b><%=timestamp%></b>
			</td>
			<logic:equal name="tranOk" value="Y">
				<td nowrap='nowrap' class='td-label-small'>&nbsp;
					<div id="quickProcessing" style="visibility: hidden">
						<span class="ERROR"> <fmt:message
								key="message.in.process.short" />
						</span>
					</div></td>
				<td nowrap='nowrap' class='td-small'>
					<div id="TranLinks">
						<logic:present name="tranColl">
							<logic:notEmpty name="tranColl">
								<logic:iterate id="tranId" name="tranColl">
									<a
										href='../../transactionDetail.do?tranId=<bean:write
						name="tranId" />'
										onclick="goButton.style.display='none';TranLinks.style.display='none';CustLinks.style.display='none';quickProcessing.style.visibility='visible';"><bean:write
											name="tranId" />
									</a>
								</logic:iterate>
							</logic:notEmpty>
						</logic:present>
					</div></td>

				<td nowrap='nowrap' class='td-label-small'><fmt:message
						key="label.tran.id" />
					<fmt:message key="label.suffix" /> &nbsp;</td>
				<td nowrap='nowrap' class='td-small'><input type='text'
					name='tranId' tabindex='1' size='8'
					<%java.util.Map<String, String> transferMap = (java.util.Map<String, String>) request
						.getSession().getAttribute("errDetail");
				String errTran = null;
				if (transferMap != null)
					if (transferMap.containsKey("errTran"))
						errTran = transferMap.get("errTran");
				if (!emgadm.util.StringHelper.isNullOrEmpty(errTran) && showTranVal==true) {%>
					out.print(${fn:escapeXml("value='" +errTran+ "'")});
				<% }%> />
				</td>
			</logic:equal>

			<logic:equal name="tranOk" value="N">
				<td colspan='5'></td>
			</logic:equal>

			
		</tr>
		<tr>
			<td nowrap='nowrap' class='td-small'><b> <bean:write
						scope="session" name="userProfile" property="firstName" /> <bean:write
						scope="session" name="userProfile" property="lastName" /> - <bean:write
						scope="session" name="userProfile" property="UID" /> &nbsp;
					&nbsp;<a href="../../welcome.do"><fmt:message
							key="label.home.link" />
				</a> &nbsp; &nbsp; &nbsp; &nbsp;</b>
			</td>

			<logic:equal name="custOk" value="Y">
				<td nowrap='nowrap' class='td-label-small'>&nbsp;</td>
				<td nowrap='nowrap' class='td-small'>
					<div id="CustLinks">
						<logic:present name="custColl">
							<logic:notEmpty name="custColl">
								<logic:iterate id="custId" name="custColl">
									<a
										href='../../showCustomerProfile.do?custId=<bean:write
						name="custId" />&basicFlag=N'
										onclick="goButton.style.display='none';TranLinks.style.display='none';CustLinks.style.display='none';quickProcessing.style.visibility='visible';">
										<bean:write name="custId" />
									</a>
								</logic:iterate>
							</logic:notEmpty>
						</logic:present>
					</div></td>
				<td nowrap='nowrap' class='td-label-small'><fmt:message
						key="label.cust.guid.id" />
					<fmt:message key="label.suffix" /> &nbsp;</td>
				<td nowrap='nowrap' class='td-small' colspan='2'><input
					type='text' name='custId' tabindex='2' size='24'
					
					<%java.util.Map<String, String> transferMap = (java.util.Map<String, String>) request
						.getSession().getAttribute("errDetail");
				String errCust = null;
				if (transferMap != null)
					if (transferMap.containsKey("errCust"))
						errCust = transferMap.get("errCust");
				if (!emgadm.util.StringHelper.isNullOrEmpty(errCust) && showVal==true) {%>
					out.print(${fn:escapeXml("value='" +errCust+ "'")});
				<%}%>
			    />
				</td>
			</logic:equal>

			<logic:equal name="custOk" value="N">
				<td nowrap='nowrap' colspan='5'></td>
			</logic:equal>

			<td nowrap='nowrap' class='td-small'>
				<div id="goButton">
					<input type="submit" name="quickSearch" tabindex="3" value="go"
						onclick="goButton.style.display='none';TranLinks.style.display='none';CustLinks.style.display='none';quickProcessing.style.visibility='visible';" />
					&nbsp; &nbsp;
				</div></td>

		</tr>
	</table>

</form>
