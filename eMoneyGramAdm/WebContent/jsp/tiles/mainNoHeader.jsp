<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=10" />
	<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
	<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %> 
	<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
	<title><tiles:getAsString name="title"/></title>
	<html:base/>
	<link href="../../theme/eMGAdm.css" rel="stylesheet"	type="text/css" />
</head>



<body MARGINWIDTH="0" MARGINHEIGHT="0" rightMargin="0" topMargin="0" leftMargin="0" bottomMargin="0" style="MARGIN-TOP: 0px; MARGIN-LEFT: 0px">
<table border="0" width='100%"' height='100%' cellspacing="0" cellpadding="0" >

	<tr height='36' valign='top' >
		<td width='6%' bgcolor='#000000' valign='top' align='left' colspan='2'>
			<img border='0' src='../../images/emoneygramheader.jpg'  />
		</td>
		<td width='*' bgcolor='#000000' colspan='3'>
			
		</td>
	</tr>
	<tr height='40'>
		<td colspan='3' align='top'>
			<table border="0" bgcolor="#FFFFFF"  width="100%" cellPadding="0" cellSpacing="0" >
				<tr>
					<td width="3%" bgcolor="#FFFFFF" valign="top" align="left"></td>
					<td bgcolor="#ffffff" align='left' valign='top' nowrap='true' class="GREY-BOLD" colspan='1'>
				</tr>
				<tr>
					<td></td>
					<td colspan='2' align='left' class='TITLE' bgcolor='#FFFFFF'><br/>
						&nbsp;&nbsp;&nbsp;<tiles:getAsString name="title"/><br/>	<img src='../../images/doubleGrayLine.gif'  />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="6%" bgcolor="#ffffff" align="left" valign="top" ></td>
		<td width="84%" valign="top" align="left"><tiles:insert attribute='body' /></td>
		<td width='10%' colspan='2'></td>
	</tr>
	<tr>
		<td colspan="4" valign="bottom">
			<tiles:insert attribute="footer" />
		</td>
	</tr>
</table>

</body>
</html>
