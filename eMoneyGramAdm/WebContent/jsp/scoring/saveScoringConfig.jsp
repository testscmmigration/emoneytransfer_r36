<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<CENTER>


<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

<script src="../../Display/jscript/showUser.js" language="JavaScript"></script>

<script language="javascript">
	function DoAction(formAction, configId) { 

		if (formAction=="delete"){
			question = confirm("Are you sure you want to delete?");
			if (question =="0"){
				return;
			}
		}

		if (formAction=="activate"){
			question = confirm("Are you sure you want to activate?");
			if (question =="0"){
				return;
			}
		}
	
		saveScoringConfigForm.hiddenSaveAction.value = formAction;
		saveScoringConfigForm.hiddenConfigId.value = configId;
		saveScoringConfigForm.submit();	
	} 
</script>

</CENTER>

<% int count = 0; %>
<% String view = "View"; %>
<% String edit = "Edit"; %>
<% String delete = "Delete"; %>
<% String activate = "Activate"; %>

<html:form action="/saveScoringConfig.do">

<html:hidden name="saveScoringConfigForm" property="hiddenSaveAction"/>
<html:hidden name="saveScoringConfigForm" property="hiddenConfigId"/>

<h3><fmt:message key="label.transaction.scoring.configuration" /> (${fn:escapeXml(partnerSiteId)})  </h3>
<br>
<table>
	<tbody>
		<tr>
			<td class="TD-SMALL"><b>Selected Product :</b>&nbsp;</td>
			<td class="TD-SMALL">
				<bean:write filter="false" name="currentTranType" property="emgTranTypeDescLabel"/>
			</td> 
		</tr>
	</tbody>
</table>
<br>

<logic:notEmpty name="allVersions">
<%String dispClass = "TD-SMALL";%>	
<table>
	<tbody>
		<tr>
			<td class="TD-HEADER-SMALL" align="center"><b>Version</b></td>
			<td class="TD-HEADER-SMALL" align="center"><b>Status</b></td>
			<td class="TD-HEADER-SMALL" align="center"><b>Accept Threshold</b></td>
			<td class="TD-HEADER-SMALL" align="center"><b>Reject Threshold</b></td>
			<td class="TD-HEADER-SMALL" colspan="5" align="center"><b>Available Actions</b></td>
			<td class="TD-HEADER-SMALL" align="center"><b>Create Date</b></td>
			<td class="TD-HEADER-SMALL" align="center"><b>Created By</b></td>
			<td class="TD-HEADER-SMALL" align="center"><b>Modified Date</b></td>
			<td class="TD-HEADER-SMALL" align="center"><b>Modified By</b></td>
		</tr>

		<logic:iterate id="scoreConfiguration" name="allVersions">
		<tr>
			<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
				<bean:write filter="false" name="scoreConfiguration" property="configurationVersionNumber"/></td>
			<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
				<bean:write filter="false" name="scoreConfiguration" property="status" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
				<bean:write filter="false" name="scoreConfiguration" property="acceptThreshold" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
				<bean:write filter="false" name="scoreConfiguration" property="rejectThreshold"/></td>
				
			<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
					&nbsp;			
					<a href='javascript:DoAction("view", "<c:out value='${scoreConfiguration.configurationId}'/>")'>
						View
					</a>
					&nbsp;						
			</td>

   			<logic:equal name="scoreConfiguration" property="editable" value="true">
			<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
					&nbsp;			
					<a href='javascript:DoAction("edit", "<c:out value='${scoreConfiguration.configurationId}'/>")'>
						Edit
					</a>
					&nbsp;					
			</td>
   			</logic:equal>

   			<logic:notEqual name="scoreConfiguration" property="editable" value="true">
			<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
					&nbsp;			
			</td>
   			</logic:notEqual>

			
   			<logic:equal name="scoreConfiguration" property="status" value="Inactive">
			<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
					&nbsp;
					<a href='javascript:DoAction("activate", "<c:out value='${scoreConfiguration.configurationId}'/>")'>
						Activate
					</a>
					&nbsp;
			</td>
   			<logic:equal name="scoreConfiguration" property="editable" value="true">
				<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
					&nbsp;
					<a href='javascript:DoAction("delete", "<c:out value='${scoreConfiguration.configurationId}'/>")'>
						Delete
					</a>
					&nbsp;					
				</td>
   			</logic:equal>
   			
   			<logic:notEqual name="scoreConfiguration" property="editable" value="true">
				<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
						&nbsp;			
				</td>
   			</logic:notEqual>
   			
   			
   			</logic:equal>

   			<logic:notEqual name="scoreConfiguration" property="status" value="Inactive">
			<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
					&nbsp;
			</td>
			<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
					&nbsp;
			</td>
   			</logic:notEqual>

			<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
					&nbsp;
					<a href='javascript:DoAction("test", "<c:out value='${scoreConfiguration.configurationId}'/>")'>
						Test
					</a>
					&nbsp;					
			</td>
				
			<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
				<bean:write filter="false" name="scoreConfiguration" property="createDate"/></td>
			<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
           		<logic:notEqual name="scoreConfiguration" property="createUserid" value="">
					<a href='../../showUser.do?userId=<c:out 
					value="${scoreConfiguration.createUserid}"/>' 
					onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out 
					value="${scoreConfiguration.createUserid}"/>\')'">
					<b><bean:write name="scoreConfiguration" property="createUserid" /></b></a>
				</logic:notEqual>
     			<logic:equal name="scoreConfiguration" property="createUserid" value="">
    				<bean:write name="scoreConfiguration" property="createUserid" />
    			</logic:equal>
			</td>
			<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
				<bean:write filter="false" name="scoreConfiguration" property="lastUpdateDate"/></td>
			<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
           		<logic:notEqual name="scoreConfiguration" property="lastUpdateUserid" value="">
					<a href='../../showUser.do?userId=<c:out 
					value="${scoreConfiguration.lastUpdateUserid}"/>' 
					onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out 
					value="${scoreConfiguration.lastUpdateUserid}"/>\')'">
					<b><bean:write name="scoreConfiguration" property="lastUpdateUserid" /></b></a>
				</logic:notEqual>
     			<logic:equal name="scoreConfiguration" property="lastUpdateUserid" value="">
    				<bean:write name="scoreConfiguration" property="lastUpdateUserid" />
    			</logic:equal>
			</td>
			
		<tr>
		<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
			else dispClass = "TD-SHADED-SMALL"; %>	
			</logic:iterate>
		</tbody>			      
	</table>
</logic:notEmpty>

&nbsp;&nbsp;

<div id="formButtons">
		<html:submit property="submitDone" value=" Done  "  
		onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
</div>

	
<div id="processing" style="visibility: hidden">
	<table>
		<tr>
			<td class="PROCESSING"><fmt:message key="message.in.process" /></td>
		</tr>
	</table>
</div >

</html:form>


