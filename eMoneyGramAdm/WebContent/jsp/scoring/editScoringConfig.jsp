<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<CENTER>

<script language="javascript">
	function DoAction(formAction, categoryName, valueIndex) { 
		editScoringConfigForm.hiddenEditAction.value = formAction;
		editScoringConfigForm.hiddenCategory.value = categoryName;
		editScoringConfigForm.hiddenValue.value = valueIndex;
		editScoringConfigForm.submit();	
	} 
</script>

<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

</CENTER>

<% int count = 0; %>
<%String dispClass = "TD-SMALL";%>	

<html:form action="/editScoringConfig.do">
<html:hidden name="editScoringConfigForm" property="hiddenEditAction"/>
<html:hidden name="editScoringConfigForm" property="hiddenCategory"/>
<html:hidden name="editScoringConfigForm" property="hiddenValue"/>

<h3><fmt:message key="label.transaction.scoring.configuration" />(${fn:escapeXml(partnerSiteId)}) </h3>


<div id="topButtons">
	<html:submit property="submitSave" value=" Save  "  
		onclick="topButtons.style.display='none';topProcessing.style.visibility='visible';" />
	&nbsp; &nbsp;
	<html:submit property="submitCancel" value=" Cancel  "  
		onclick="topButtons1.style.display='none';topProcessing.style.visibility='visible';" />
</div>
   
<div id="topProcessing" style="visibility: hidden">
	<font color='red'><b><fmt:message key="message.in.process" /></b></font>
</div >

<table>
	<tr>
		<td>
		<table>
			<tbody>
				<tr>
					<td class="td-label-small">Selected Product : </td>
					<td class="td-small">
						<bean:write name="currentTranType" property="emgTranTypeDescLabel" />
					</td> 
				</tr>
				
				<tr>
					<td class="td-label-small">Accept Threshold : </td>
					<td class="td-small">
						<html:text name="editScoringConfigForm" property="acceptThreshold" size="5" maxlength="3" />
						<span class="error"><html:errors property='acceptThreshold'/></span>
					</td>
				</tr>
				<tr>
					<td class="td-label-small">Reject Threshold : </td>
					<td class="td-small">
						<html:text name="editScoringConfigForm" property="rejectThreshold" size="5" maxlength="3" />
						<span class="error"><html:errors property='rejectThreshold'/></span>
					</td>
				<tr>					
			</tbody>
		</table>
		</td>
		<td width="150">&nbsp;</td>
	</tr>
</table>

<br>

<a name="cat1"></a>
<table width="600" border="1" cellspacing="0">
	<tbody valign="top">
		<tr>
			<td class="TD-HEADER-SMALL">
			
				<!-- For Password reset in 24 hrs -->
			    <bean:define id="catName1" name="scoreCategory1" property="scoreCategoryName" type="java.lang.String" />				
				<html:hidden name="editScoringConfigForm" property="hiddenCategoryName1" value="<%=catName1 %>" />
				<bean:write name="scoreCategory1" property="scoreCategoryName"/> : 

			</td>
		</tr>
		<tr>
			<td>					
					<TABLE>
				        <TBODY>
				        <TR class="TD-LABEL">
				          <TD class="td-label-small">Disabled: </TD>
				          <TD class="td-small">
							<html:checkbox name="editScoringConfigForm" property="disable1" /></TD>
				          <TD>&nbsp;</TD>
				          <TD class="td-label-small">Weight: </TD>
				          <TD class="td-small">
				          	<html:text name="editScoringConfigForm" property="weightText1" size="5" maxlength="2" />
							<span class="error"><html:errors property='weightText1' /></span>
				          </TD>
				        </TR>
						</TBODY>
					</TABLE>      
					<br>
					<span class="error">
						<html:errors property="Category1Error"/>
					</span>
					<TABLE>
						<TBODY>
						
							<TR>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Values</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Points</b>&nbsp;</TD>
							</TR>
					        <TR  class=TD-SMALL>
					          <TD>
									<html:select name="editScoringConfigForm" property="value1" size="1">
										<html:option value=""><fmt:message key="option.select.an.option"/></html:option>												
										<html:options collection="booleanList" property="code" labelProperty="description" />
									</html:select>		
							  </TD>
					          <TD class=TD-SMALL>
					            <html:text name="editScoringConfigForm" property="pointsText1" size="6" maxlength="6" />
							  </TD>
					          <TD>
								<html:submit property="submitAdd1" value=" Add " />
					          </TD>
					        </TR>
		
							<logic:present name="scoreCategory1">
								<%dispClass = "TD-SMALL";%>	
								<logic:iterate id="scoreValue" name="scoreCategory1" property="scoreValues">
							        <TR>
							        	<TD class='<%=dispClass%>'><bean:write name="scoreValue" property="value"/></TD>
							          	<TD class='<%=dispClass%>'><bean:write name="scoreValue" property="points"/></TD>
								        <TD class='<%=dispClass%>'>
											<a href='javascript:DoAction("delete", "<bean:write name='scoreCategory1' property='scoreCategoryName'/>", <%=count++%>)'>
												delete
											</a>
										</TD>
							        </TR>
									<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
										else dispClass = "TD-SHADED-SMALL"; %>	
						        </logic:iterate>
							</logic:present>
						</TBODY>
					</TABLE>
			</td>
		</tr>
	</tbody>
</table>

<br>	

<a name="cat2"></a>
<table width="600" border="1" cellspacing="0">
	<tbody valign="top">
		<tr>
			<td class="TD-HEADER-SMALL">
				<!-- For Profile Status -->
			    <bean:define id="catName2" name="scoreCategory2" property="scoreCategoryName" type="java.lang.String" />				
				<html:hidden name="editScoringConfigForm" property="hiddenCategoryName2" value="<%=catName2 %>" />
				
				<bean:write filter="false" name="scoreCategory2" property="scoreCategoryName"/> : 
			</td>
		</tr>
		<tr>
			<td>
					<TABLE>
				        <TBODY>
				        <TR class="TD-LABEL">
				          <TD class="td-label-small">Disabled: </TD>
				          <TD class="td-small">
							<html:checkbox name="editScoringConfigForm" property="disable2" /></TD>
				          <TD>&nbsp;</TD>
				          <TD class="td-label-small">Weight: </TD>
				          <TD class="td-small">
				          	<html:text name="editScoringConfigForm" property="weightText2" size="5" maxlength="2" />
							<span class="error"><html:errors property='weightText2' /></span>
				          </TD>
				        </TR>
						</TBODY>
					</TABLE>      
					<BR>	
					<span class="error">
						<html:errors property="Category2Error"/>
					</span>
					<TABLE>
						<TBODY>
						
							<TR>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Values</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Points</b>&nbsp;</TD>
							</TR>
					        <TR  class=TD-SMALL>
					          <TD>
								<html:select name="editScoringConfigForm" property="value2" size="1">
									<html:option value=""><fmt:message key="option.select.an.option"/></html:option>
									<html:options collection="statusList" property="code" labelProperty="code" />
								</html:select>									
							  </TD>
					          <TD class=TD-SMALL>
					            <html:text name="editScoringConfigForm" property="pointsText2" size="6" maxlength="6" />
							  </TD>
					          <TD>
								<html:submit property="submitAdd2" value=" Add " />
					          </TD>
					        </TR>
		
							<%count=0;%>
							<logic:present name="scoreCategory2">
								<%dispClass = "TD-SMALL";%>	
								<logic:iterate id="scoreValue" name="scoreCategory2" property="scoreValues">
							        <TR>
							        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="value"/></TD>
							          	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="points"/></TD>
								        <TD class='<%=dispClass%>'>
											<a href='javascript:DoAction("delete", "<bean:write filter="false" name="scoreCategory2" property="scoreCategoryName"/>", <%=count++%>)'>
												delete
											</a>
										</TD>
							        </TR>
									<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
										else dispClass = "TD-SHADED-SMALL"; %>	
						        </logic:iterate>
							</logic:present>

						</TBODY>
					</TABLE>
			</td>			
		</tr>
	</tbody>
</table>

<br>

<a name="cat3"></a>
<table width="600" border="1" cellspacing="0">
	<tbody valign="top">
		<tr>
			<td class="TD-HEADER-SMALL">
	
				<!-- For ChargeBack Count -->
			    <bean:define id="catName3" name="scoreCategory3" property="scoreCategoryName" type="java.lang.String" />				
				<html:hidden name="editScoringConfigForm" property="hiddenCategoryName3" value="<%=catName3 %>" />
				
				<bean:write filter="false" name="scoreCategory3" property="scoreCategoryName"/> : 
			</td>
		</tr>
		<tr>
			<td>			
					<TABLE>
				        <TBODY>
				        <TR class="TD-LABEL">
				          <TD class="td-label-small">Disabled: </TD>
				          <TD class="td-small">
							<html:checkbox name="editScoringConfigForm" property="disable3" /></TD>
				          <TD>&nbsp;</TD>
				          <TD class="td-label-small">Weight: </TD>
				          <TD class="td-small">
				          	<html:text name="editScoringConfigForm" property="weightText3" size="5" maxlength="2" />
							<span class="error"><html:errors property='weightText3' /></span>
				          </TD>
				        </TR>
						</TBODY>
					</TABLE>      
					<BR>	
					<span class="error">
						<html:errors property="Category3Error"/>
					</span>
					<TABLE>
						<TBODY>
						
							<TR>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Values</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Points</b>&nbsp;</TD>
							</TR>
					        <TR  class=TD-SMALL>
					          <TD>
									<html:text name="editScoringConfigForm" property="value3" size="6" maxlength="6" />					          
							  </TD>
					          <TD class=TD-SMALL>
					            <html:text name="editScoringConfigForm" property="pointsText3" size="6" maxlength="6" />
							  </TD>
					          <TD>
								<html:submit property="submitAdd3" value=" Add " />
					          </TD>
					        </TR>
		
							<%count=0;%>
							<logic:present name="scoreCategory3">
								<%dispClass = "TD-SMALL";%>	
								<logic:iterate id="scoreValue" name="scoreCategory3" property="scoreValues">
							        <TR>
							        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="value"/></TD>
							          	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="points"/></TD>
								        <TD class='<%=dispClass%>'>
											<a href='javascript:DoAction("delete", "<bean:write filter="false" name="scoreCategory3" property="scoreCategoryName"/>", <%=count++%>)'>
												delete
											</a>
										</TD>
							        </TR>
									<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
										else dispClass = "TD-SHADED-SMALL"; %>	
						        </logic:iterate>
							</logic:present>
							
						</TBODY>
					</TABLE>
			</td>
		</tr>
	</tbody>
</table>	

<br>			
			
<a name="cat4"></a>
<table width="600" border="1" cellspacing="0">
	<tbody valign="top">
		<tr>
			<td class="TD-HEADER-SMALL">
				<!-- For Days since last Transaction -->
			    <bean:define id="catName4" name="scoreCategory4" property="scoreCategoryName" type="java.lang.String" />				
				<html:hidden name="editScoringConfigForm" property="hiddenCategoryName4" value="<%=catName4 %>" />

				<bean:write filter="false" name="scoreCategory4" property="scoreCategoryName"/> : 
			</td>
		</tr>
		<tr>
			<td>
					<TABLE>
				        <TBODY>
				        <TR class="TD-LABEL">
				          <TD class="td-label-small">Disabled: </TD>
				          <TD class="td-small">
							<html:checkbox name="editScoringConfigForm" property="disable4" /></TD>
				          <TD>&nbsp;</TD>
				          <TD class="td-label-small">Weight: </TD>
				          <TD class="td-small">
				          	<html:text name="editScoringConfigForm" property="weightText4" size="5" maxlength="2" />
							<span class="error"><html:errors property='weightText4' /></span>
				          </TD>
				        </TR>
						</TBODY>
					</TABLE>      
					<BR>	
					<span class="error">
						<html:errors property="Category4Error"/>
					</span>
					<TABLE>
						<TBODY>
						
							<TR>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Min Value</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Max Value</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Points</b>&nbsp;</TD>
							</TR>
					        <TR  class=TD-SMALL>
					          <TD>
					            <html:text name="editScoringConfigForm" property="minimumValueText4" size="4" maxlength="15" />
							  </TD>
					          <TD>
					            <html:text name="editScoringConfigForm" property="maximumValueText4" size="4" maxlength="15" />
							  </TD>
					          <TD class=TD-SMALL>
					            <html:text name="editScoringConfigForm" property="pointsText4" size="4" maxlength="15" />
							  </TD>
					          <TD>
								<html:submit property="submitAdd4" value=" Add " />
					          </TD>
					        </TR>
		
							<%count=0;%>
							<logic:present name="scoreCategory4">
								<%dispClass = "TD-SMALL";%>	
								<logic:iterate id="scoreValue" name="scoreCategory4" property="scoreValues">
							        <TR>
										<logic:empty name="scoreValue" property="value">							        
								        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="minimumValue"/></TD>
								        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="maximumValue"/></TD>
										</logic:empty>
							          	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="points"/></TD>
								        <TD class='<%=dispClass%>'>
											<a href='javascript:DoAction("delete", "<bean:write filter="false" name="scoreCategory4" property="scoreCategoryName"/>", <%=count++%>)'>
												delete
											</a>
										</TD>
							        </TR>
									<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
										else dispClass = "TD-SHADED-SMALL"; %>	
						        </logic:iterate>
							</logic:present>

						</TBODY>
					</TABLE>
				</td>
			</tr>
	</tbody>
</table>
	
<br>

<a name="cat5"></a>
<table width="600" border="1" cellspacing="0">
	<tbody valign="top">
		<tr>
			<td class="TD-HEADER-SMALL">
				<!-- Email Address in Blocked List -->
			    <bean:define id="catName5" name="scoreCategory5" property="scoreCategoryName" type="java.lang.String" />				
				<html:hidden name="editScoringConfigForm" property="hiddenCategoryName5" value="<%=catName5 %>" />

				<bean:write filter="false" name="scoreCategory5" property="scoreCategoryName"/> : 

			</td>
		</tr>
		<tr>
			<td>					
					<TABLE>
				        <TBODY>
				        <TR class="TD-LABEL">
				          <TD class="td-label-small">Disabled: </TD>
				          <TD class="td-small">
							<html:checkbox name="editScoringConfigForm" property="disable5" /></TD>
				          <TD>&nbsp;</TD>
				          <TD class="td-label-small">Weight: </TD>
				          <TD class="td-small">
				          	<html:text name="editScoringConfigForm" property="weightText5" size="5" maxlength="2" />
							<span class="error"><html:errors property='weightText5' /></span>
				          </TD>
				        </TR>
						</TBODY>
					</TABLE>      
					<br>	
					<span class="error">
						<html:errors property="Category5Error"/>
					</span>
					<TABLE>
						<TBODY>
						
							<TR>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Values</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Points</b>&nbsp;</TD>
							</TR>
					        <TR  class=TD-SMALL>
					          <TD>
									<html:select name="editScoringConfigForm" property="value5" size="1">
										<html:option value=""><fmt:message key="option.select.an.option"/></html:option>												
										<html:options collection="booleanList" property="code" labelProperty="description" />
									</html:select>		
							  </TD>
					          <TD class=TD-SMALL>
					            <html:text name="editScoringConfigForm" property="pointsText5" size="6" maxlength="6" />
							  </TD>
					          <TD>
								<html:submit property="submitAdd5" value=" Add " />
					          </TD>
					        </TR>

							<%count=0;%>
							<logic:present name="scoreCategory5">
								<%dispClass = "TD-SMALL";%>	
								<logic:iterate id="scoreValue" name="scoreCategory5" property="scoreValues">
							        <TR>
							        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="value"/></TD>
							          	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="points"/></TD>
								        <TD class='<%=dispClass%>'>
											<a href='javascript:DoAction("delete", "<bean:write filter='false' name='scoreCategory5' property='scoreCategoryName'/>", <%=count++%>)'>
												delete
											</a>
										</TD>
							        </TR>
									<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
										else dispClass = "TD-SHADED-SMALL"; %>	
						        </logic:iterate>
							</logic:present>

						</TBODY>
					</TABLE>
			</td>
		</tr>
	</tbody>
</table>

<br>	

<a name="cat6"></a>
<table width="600" border="1" cellspacing="0">
	<tbody valign="top">
		<tr>
			<td class="TD-HEADER-SMALL">
				<!-- Email Address in Blocked List -->
			    <bean:define id="catName6" name="scoreCategory6" property="scoreCategoryName" type="java.lang.String" />				
				<html:hidden name="editScoringConfigForm" property="hiddenCategoryName6" value="<%=catName6 %>" />

				<bean:write filter="false" name="scoreCategory6" property="scoreCategoryName"/> : 

			</td>
		</tr>
		<tr>
			<td>					
					<TABLE>
				        <TBODY>
				        <TR class="TD-LABEL">
				          <TD class="td-label-small">Disabled: </TD>
				          <TD class="td-small">
							<html:checkbox name="editScoringConfigForm" property="disable6" /></TD>
				          <TD>&nbsp;</TD>
				          <TD class="td-label-small">Weight: </TD>
				          <TD class="td-small">
				          	<html:text name="editScoringConfigForm" property="weightText6" size="5" maxlength="2" />
							<span class="error"><html:errors property='weightText6' /></span>
				          </TD>
				        </TR>
						</TBODY>
					</TABLE>      
					<br>	
					<span class="error">
						<html:errors property="Category6Error"/>
					</span>
					<TABLE>
						<TBODY>
						
							<TR>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Values</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Points</b>&nbsp;</TD>
							</TR>
					        <TR  class=TD-SMALL>
					          <TD>
									<html:select name="editScoringConfigForm" property="value6" size="1">
										<html:option value=""><fmt:message key="option.select.an.option"/></html:option>												
										<html:options collection="booleanList" property="code" labelProperty="description" />
									</html:select>		
							  </TD>
					          <TD class=TD-SMALL>
					            <html:text name="editScoringConfigForm" property="pointsText6" size="6" maxlength="6" />
							  </TD>
					          <TD>
								<html:submit property="submitAdd6" value=" Add " />
					          </TD>
					        </TR>
		
							<%count=0;%>
							<logic:present name="scoreCategory6">
								<%dispClass = "TD-SMALL";%>	
								<logic:iterate id="scoreValue" name="scoreCategory6" property="scoreValues">
							        <TR>
							        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="value"/></TD>
							          	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="points"/></TD>
								        <TD class='<%=dispClass%>'>
											<a href='javascript:DoAction("delete", "<bean:write filter='false' name='scoreCategory6' property='scoreCategoryName'/>", <%=count++%>)'>
												delete
											</a>
										</TD>
							        </TR>
									<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
										else dispClass = "TD-SHADED-SMALL"; %>	
						        </logic:iterate>
							</logic:present>
							
						</TBODY>
					</TABLE>
			</td>
		</tr>
	</tbody>
</table>

<br>	

<a name="cat7"></a>
<table width="600" border="1" cellspacing="0">
	<tbody valign="top">
		<tr>
			<td class="TD-HEADER-SMALL">
				<!-- Rcv state matches profile state -->
			    <bean:define id="catName7" name="scoreCategory7" property="scoreCategoryName" type="java.lang.String" />				
				<html:hidden name="editScoringConfigForm" property="hiddenCategoryName7" value="<%=catName7 %>" />

				<bean:write filter="false" name="scoreCategory7" property="scoreCategoryName"/> : 

			</td>
		</tr>
		<tr>
			<td>					
					<TABLE>
				        <TBODY>
				        <TR class="TD-LABEL">
				          <TD class="td-label-small">Disabled: </TD>
				          <TD class="td-small">
							<html:checkbox name="editScoringConfigForm" property="disable7" /></TD>
				          <TD>&nbsp;</TD>
				          <TD class="td-label-small">Weight: </TD>
				          <TD class="td-small">
				          	<html:text name="editScoringConfigForm" property="weightText7" size="5" maxlength="2" />
							<span class="error"><html:errors property='weightText7' /></span>
				          </TD>
				        </TR>
						</TBODY>
					</TABLE>      
					<br>	
					<span class="error">
						<html:errors property="Category7Error"/>
					</span>
					<TABLE>
						<TBODY>
						
							<TR>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Values</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Points</b>&nbsp;</TD>
							</TR>
					        <TR  class=TD-SMALL>
					          <TD>
									<html:select name="editScoringConfigForm" property="value7" size="1">
										<html:option value=""><fmt:message key="option.select.an.option"/></html:option>												
										<html:options collection="booleanList" property="code" labelProperty="description" />
									</html:select>		
							  </TD>
					          <TD class=TD-SMALL>
					            <html:text name="editScoringConfigForm" property="pointsText7" size="6" maxlength="6" />
							  </TD>
					          <TD>
								<html:submit property="submitAdd7" value=" Add " />
					          </TD>
					        </TR>
		
							<%count=0;%>
							<logic:present name="scoreCategory7">
								<%dispClass = "TD-SMALL";%>	
								<logic:iterate id="scoreValue" name="scoreCategory7" property="scoreValues">
							        <TR>
							        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="value"/></TD>
							          	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="points"/></TD>
								        <TD class='<%=dispClass%>'>
											<a href='javascript:DoAction("delete", "<bean:write filter='false' name='scoreCategory7' property='scoreCategoryName'/>", <%=count++%>)'>
												delete
											</a>
										</TD>
							        </TR>
									<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
										else dispClass = "TD-SHADED-SMALL"; %>	
						        </logic:iterate>
							</logic:present>
							
						</TBODY>
					</TABLE>
			</td>
		</tr>
	</tbody>
</table>

<br>

<a name="cat8"></a>
<table width="600" border="1" cellspacing="0">
	<tbody valign="top">
		<tr>
			<td class="TD-HEADER-SMALL">
				<!-- Age Threshold -->
			    <bean:define id="catName8" name="scoreCategory8" property="scoreCategoryName" type="java.lang.String" />				
				<html:hidden name="editScoringConfigForm" property="hiddenCategoryName8" value="<%=catName8 %>" />

				<bean:write filter="false" name="scoreCategory8" property="scoreCategoryName"/> : 

			</td>
		</tr>
		<tr>
			<td>					
					<TABLE>
				        <TBODY>
				        <TR class="TD-LABEL">
				          <TD class="td-label-small">Disabled: </TD>
				          <TD class="td-small">
							<html:checkbox name="editScoringConfigForm" property="disable8" /></TD>
				          <TD>&nbsp;</TD>
				          <TD class="td-label-small">Weight: </TD>
				          <TD class="td-small">
				          	<html:text name="editScoringConfigForm" property="weightText8" size="5" maxlength="2" />
							<span class="error"><html:errors property='weightText8' /></span>
				          </TD>
				        </TR>
						</TBODY>
					</TABLE>      
					<br>	
					<span class="error">
						<html:errors property="Category8Error"/>
					</span>
					<TABLE>
						<TBODY>
							<TR>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Values</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Points</b>&nbsp;</TD>
							</TR>
					        <TR  class=TD-SMALL>
					          <TD>
									<html:select name="editScoringConfigForm" property="value8" size="1">
										<html:option value=""><fmt:message key="option.select.an.option"/></html:option>												
										<html:options collection="booleanList" property="code" labelProperty="description" />
									</html:select>		
							  </TD>
					          <TD class=TD-SMALL>
					            <html:text name="editScoringConfigForm" property="pointsText8" size="6" maxlength="6" />
							  </TD>
					          <TD>
								<html:submit property="submitAdd8" value=" Add " />
					          </TD>
					        </TR>
		
							<%count=0;%>
							<logic:present name="scoreCategory8">
								<%dispClass = "TD-SMALL";%>	
								<logic:iterate id="scoreValue" name="scoreCategory8" property="scoreValues">
							        <TR>
							        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="value"/></TD>
							          	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="points"/></TD>
								        <TD class='<%=dispClass%>'>
											<a href='javascript:DoAction("delete", "<bean:write filter='false' name='scoreCategory8' property='scoreCategoryName'/>", <%=count++%>)'>
												delete
											</a>
										</TD>
							        </TR>
									<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
										else dispClass = "TD-SHADED-SMALL"; %>	
						        </logic:iterate>
							</logic:present>
						</TBODY>
					</TABLE>
			</td>
		</tr>
	</tbody>
</table>

<br>

<a name="cat9"></a>
<table width="600" border="1" cellspacing="0">
	<tbody valign="top">
		<tr>
			<td class="TD-HEADER-SMALL">
				<!-- Zip To State -->
			    <bean:define id="catName9" name="scoreCategory9" property="scoreCategoryName" type="java.lang.String" />				
				<html:hidden name="editScoringConfigForm" property="hiddenCategoryName9" value="<%=catName9 %>" />

				<bean:write filter="false" name="scoreCategory9" property="scoreCategoryName"/> : 

			</td>
		</tr>
		<tr>
			<td>					
					<TABLE>
				        <TBODY>
				        <TR class="TD-LABEL">
				          <TD class="td-label-small">Disabled: </TD>
				          <TD class="td-small">
							<html:checkbox name="editScoringConfigForm" property="disable9" /></TD>
				          <TD>&nbsp;</TD>
				          <TD class="td-label-small">Weight: </TD>
				          <TD class="td-small">
				          	<html:text name="editScoringConfigForm" property="weightText9" size="5" maxlength="2" />
							<span class="error"><html:errors property='weightText9' /></span>
				          </TD>
				        </TR>
						</TBODY>
					</TABLE>      
					<br>	
					<span class="error">
						<html:errors property="Category9Error"/>
					</span>
					<TABLE>
						<TBODY>
							<TR>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Values</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Points</b>&nbsp;</TD>
							</TR>
					        <TR  class=TD-SMALL>
					          <TD>
									<html:select name="editScoringConfigForm" property="value9" size="1">
										<html:option value=""><fmt:message key="option.select.an.option"/></html:option>												
										<html:options collection="booleanList" property="code" labelProperty="description" />
									</html:select>		
							  </TD>
					          <TD class=TD-SMALL>
					            <html:text name="editScoringConfigForm" property="pointsText9" size="6" maxlength="6" />
							  </TD>
					          <TD>
								<html:submit property="submitAdd9" value=" Add " />
					          </TD>
					        </TR>
		
							<%count=0;%>
							<logic:present name="scoreCategory9">
								<%dispClass = "TD-SMALL";%>	
								<logic:iterate id="scoreValue" name="scoreCategory9" property="scoreValues">
							        <TR>
							        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="value"/></TD>
							          	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="points"/></TD>
								        <TD class='<%=dispClass%>'>
											<a href='javascript:DoAction("delete", "<bean:write filter='false' name='scoreCategory9' property='scoreCategoryName'/>", <%=count++%>)'>
												delete
											</a>
										</TD>
							        </TR>
									<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
										else dispClass = "TD-SHADED-SMALL"; %>	
						        </logic:iterate>
							</logic:present>
						</TBODY>
					</TABLE>
			</td>
		</tr>
	</tbody>
</table>
<br>

<a name="cat10"></a>
<table width="600" border="1" cellspacing="0">
	<tbody valign="top">
		<tr>
			<td class="TD-HEADER-SMALL">
				<!-- Zip To State -->
			    <bean:define id="catName10" name="scoreCategory10" property="scoreCategoryName" type="java.lang.String" />				
				<html:hidden name="editScoringConfigForm" property="hiddenCategoryName10" value="<%=catName10 %>" />

				<bean:write filter="false" name="scoreCategory10" property="scoreCategoryName"/> : 

			</td>
		</tr>
		<tr>
			<td>					
					<TABLE>
				        <TBODY>
				        <TR class="TD-LABEL">
				          <TD class="td-label-small">Disabled: </TD>
				          <TD class="td-small">
							<html:checkbox name="editScoringConfigForm" property="disable10" /></TD>
				          <TD>&nbsp;</TD>
				          <TD class="td-label-small">Weight: </TD>
				          <TD class="td-small">
				          	<html:text name="editScoringConfigForm" property="weightText10" size="5" maxlength="2" />
							<span class="error"><html:errors property='weightText10' /></span>
				          </TD>
				        </TR>
						</TBODY>
					</TABLE>      
					<br>	
					<span class="error">
						<html:errors property="Category10Error"/>
					</span>
					<TABLE>
						<TBODY>
							<TR>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Values</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Points</b>&nbsp;</TD>
							</TR>
					        <TR  class=TD-SMALL>
					          <TD>
									<html:select name="editScoringConfigForm" property="value10" size="1">
										<html:option value=""><fmt:message key="option.select.an.option"/></html:option>												
										<html:options collection="booleanList" property="code" labelProperty="description" />
									</html:select>		
							  </TD>
					          <TD class=TD-SMALL>
					            <html:text name="editScoringConfigForm" property="pointsText10" size="6" maxlength="6" />
							  </TD>
					          <TD>
								<html:submit property="submitAdd10" value=" Add " />
					          </TD>
					        </TR>
		
							<%count=0;%>
							<logic:present name="scoreCategory10">
								<%dispClass = "TD-SMALL";%>	
								<logic:iterate id="scoreValue" name="scoreCategory10" property="scoreValues">
							        <TR>
							        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="value"/></TD>
							          	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="points"/></TD>
								        <TD class='<%=dispClass%>'>
											<a href='javascript:DoAction("delete", "<bean:write filter='false' name='scoreCategory10' property='scoreCategoryName'/>", <%=count++%>)'>
												delete
											</a>
										</TD>
							        </TR>
									<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
										else dispClass = "TD-SHADED-SMALL"; %>	
						        </logic:iterate>
							</logic:present>
						</TBODY>
					</TABLE>
			</td>
		</tr>
	</tbody>
</table>
<br>

<a name="cat11"></a>
<table width="600" border="1" cellspacing="0">
	<tbody valign="top">
		<tr>
			<td class="TD-HEADER-SMALL">
				<!-- Zip To State -->
			    <bean:define id="catName11" name="scoreCategory11" property="scoreCategoryName" type="java.lang.String" />				
				<html:hidden name="editScoringConfigForm" property="hiddenCategoryName11" value="<%=catName11 %>" />

				<bean:write filter="false" name="scoreCategory11" property="scoreCategoryName"/> : 

			</td>
		</tr>
		<tr>
			<td>					
					<TABLE>
				        <TBODY>
				        <TR class="TD-LABEL">
				          <TD class="td-label-small">Disabled: </TD>
				          <TD class="td-small">
							<html:checkbox name="editScoringConfigForm" property="disable11" /></TD>
				          <TD>&nbsp;</TD>
				          <TD class="td-label-small">Weight: </TD>
				          <TD class="td-small">
				          	<html:text name="editScoringConfigForm" property="weightText11" size="5" maxlength="2" />
							<span class="error"><html:errors property='weightText11' /></span>
				          </TD>
				        </TR>
						</TBODY>
					</TABLE>      
					<br>	
					<span class="error">
						<html:errors property="Category11Error"/>
					</span>
					<TABLE>
						<TBODY>
							<TR>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Values</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Points</b>&nbsp;</TD>
							</TR>
					        <TR  class=TD-SMALL>
					          <TD>
									<html:select name="editScoringConfigForm" property="value11" size="1">
										<html:option value=""><fmt:message key="option.select.an.option"/></html:option>												
										<html:options collection="booleanList" property="code" labelProperty="description" />
									</html:select>		
							  </TD>
					          <TD class=TD-SMALL>
					            <html:text name="editScoringConfigForm" property="pointsText11" size="6" maxlength="6" />
							  </TD>
					          <TD>
								<html:submit property="submitAdd11" value=" Add " />
					          </TD>
					        </TR>
		
							<%count=0;%>
							<logic:present name="scoreCategory11">
								<%dispClass = "TD-SMALL";%>	
								<logic:iterate id="scoreValue" name="scoreCategory11" property="scoreValues">
							        <TR>
							        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="value"/></TD>
							          	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="points"/></TD>
								        <TD class='<%=dispClass%>'>
											<a href='javascript:DoAction("delete", "<bean:write filter='false' name='scoreCategory11' property='scoreCategoryName'/>", <%=count++%>)'>
												delete
											</a>
										</TD>
							        </TR>
									<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
										else dispClass = "TD-SHADED-SMALL"; %>	
						        </logic:iterate>
							</logic:present>
						</TBODY>
					</TABLE>
			</td>
		</tr>
	</tbody>
</table>
<br>

<a name="cat12"></a>
<table width="600" border="1" cellspacing="0">
	<tbody valign="top">
		<tr>
			<td class="TD-HEADER-SMALL">
				<!-- Zip To State -->
			    <bean:define id="catName12" name="scoreCategory12" property="scoreCategoryName" type="java.lang.String" />				
				<html:hidden name="editScoringConfigForm" property="hiddenCategoryName12" value="<%=catName12 %>" />

				<bean:write filter="false" name="scoreCategory12" property="scoreCategoryName"/> : 

			</td>
		</tr>
		<tr>
			<td>					
					<TABLE>
				        <TBODY>
				        <TR class="TD-LABEL">
				          <TD class="td-label-small">Disabled: </TD>
				          <TD class="td-small">
							<html:checkbox name="editScoringConfigForm" property="disable12" /></TD>
				          <TD>&nbsp;</TD>
				          <TD class="td-label-small">Weight: </TD>
				          <TD class="td-small">
				          	<html:text name="editScoringConfigForm" property="weightText12" size="5" maxlength="2" />
							<span class="error"><html:errors property='weightText12' /></span>
				          </TD>
				        </TR>
						</TBODY>
					</TABLE>      
					<br>	
					<span class="error">
						<html:errors property="Category12Error"/>
					</span>
					<TABLE>
						<TBODY>
							<TR>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Values</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Points</b>&nbsp;</TD>
							</TR>
					        <TR  class=TD-SMALL>
					          <TD>
									<html:select name="editScoringConfigForm" property="value12" size="1">
										<html:option value=""><fmt:message key="option.select.an.option"/></html:option>												
										<html:options collection="booleanList" property="code" labelProperty="description" />
									</html:select>		
							  </TD>
					          <TD class=TD-SMALL>
					            <html:text name="editScoringConfigForm" property="pointsText12" size="6" maxlength="6" />
							  </TD>
					          <TD>
								<html:submit property="submitAdd12" value=" Add " />
					          </TD>
					        </TR>
		
							<%count=0;%>
							<logic:present name="scoreCategory12">
								<%dispClass = "TD-SMALL";%>	
								<logic:iterate id="scoreValue" name="scoreCategory12" property="scoreValues">
							        <TR>
							        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="value"/></TD>
							          	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="points"/></TD>
								        <TD class='<%=dispClass%>'>
											<a href='javascript:DoAction("delete", "<bean:write filter='false' name='scoreCategory12' property='scoreCategoryName'/>", <%=count++%>)'>
												delete
											</a>
										</TD>
							        </TR>
									<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
										else dispClass = "TD-SHADED-SMALL"; %>	
						        </logic:iterate>
							</logic:present>
						</TBODY>
					</TABLE>
			</td>
		</tr>
	</tbody>
</table>
<br>

























<a name="cat13"></a>
<table width="600" border="1" cellspacing="0">
	<tbody valign="top">
		<tr>
			<td class="TD-HEADER-SMALL">
				<!-- Transaction Monitor Score -->
			    	<bean:define id="catName13" name="scoreCategory13" property="scoreCategoryName" type="java.lang.String" />
			    	<html:hidden name="editScoringConfigForm" property="hiddenCategoryName13" value="<%=catName13 %>" />				
					<bean:write filter="false" name="scoreCategory13" property="scoreCategoryName"/> :
			</td>
		</tr>
		<tr>
			<td>
					<TABLE>
				        <TBODY>
				        <TR class="TD-LABEL">
				          <TD class="td-label-small">Disabled: </TD>
				          <TD class="td-small">
							<html:checkbox name="editScoringConfigForm" property="disable13" /></TD>
				          <TD>&nbsp;</TD>
				          <TD class="td-label-small">Weight: </TD>
				          <TD class="td-small">
				          	<html:text name="editScoringConfigForm" property="weightText13" size="5" maxlength="2" />
							<span class="error"><html:errors property='weightText13' /></span>
				          </TD>
				        </TR>
						</TBODY>
					</TABLE>      
					<BR>	
					<span class="error">
						<html:errors property="Category13Error"/>
					</span>
					<TABLE>
						<TBODY>
						
							<TR>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Min Value</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Max Value</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Points</b>&nbsp;</TD>
							</TR>
					        <TR  class=TD-SMALL>
					          <TD>
					            <html:text name="editScoringConfigForm" property="minimumValueText13" size="4" maxlength="15" />
							  </TD>
					          <TD>
					            <html:text name="editScoringConfigForm" property="maximumValueText13" size="4" maxlength="15" />
							  </TD>
					          <TD class=TD-SMALL>
					            <html:text name="editScoringConfigForm" property="pointsText13" size="4" maxlength="15" />
							  </TD>
					          <TD>
								<html:submit property="submitAdd13" value=" Add " />
					          </TD>
					        </TR>
		
							<%count=0;%>
							<logic:present name="scoreCategory13">
								<%dispClass = "TD-SMALL";%>	
								<logic:iterate id="scoreValue" name="scoreCategory13" property="scoreValues">
							        <TR>
										<logic:empty name="scoreValue" property="value">							        
								        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="minimumValue"/></TD>
								        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="maximumValue"/></TD>
										</logic:empty>
							          	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="points"/></TD>
								        <TD class='<%=dispClass%>'>
											<a href='javascript:DoAction("delete", "<bean:write filter="false" name="scoreCategory13" property="scoreCategoryName"/>", <%=count++%>)'>
												delete
											</a>
										</TD>
							        </TR>
									<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
										else dispClass = "TD-SHADED-SMALL"; %>	
						        </logic:iterate>
							</logic:present>

						</TBODY>
					</TABLE>
				</td>
			</tr>
	</tbody>
</table>

<a name="cat15"></a>
<table width="600" border="1" cellspacing="0">
	<tbody valign="top">
		<tr>
			<td class="TD-HEADER-SMALL">
				<!-- For Transaction Amount -->
			    <bean:define id="catName15" name="scoreCategory15" property="scoreCategoryName" type="java.lang.String" />				
				<html:hidden name="editScoringConfigForm" property="hiddenCategoryName15" value="<%=catName15 %>" />

				<bean:write filter="false" name="scoreCategory15" property="scoreCategoryName"/> : 
			</td>
		</tr>
		<tr>
			<td>
					<TABLE>
				        <TBODY>
				        <TR class="TD-LABEL">
				          <TD class="td-label-small">Disabled: </TD>
				          <TD class="td-small">
							<html:checkbox name="editScoringConfigForm" property="disable15" /></TD>
				          <TD>&nbsp;</TD>
				          <TD class="td-label-small">Weight: </TD>
				          <TD class="td-small">
				          	<html:text name="editScoringConfigForm" property="weightText15" size="5" maxlength="2" />
							<span class="error"><html:errors property='weightText15' /></span>
				          </TD>
				        </TR>
						</TBODY>
					</TABLE>      
					<BR>	
					<span class="error">
						<html:errors property="Category15Error"/>
					</span>
					<TABLE>
						<TBODY>
						
							<TR>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Min Value</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Max Value</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Points</b>&nbsp;</TD>
							</TR>
					        <TR  class=TD-SMALL>
					          <TD>
					            <html:text name="editScoringConfigForm" property="minimumValueText15" size="4" maxlength="15" />
							  </TD>
					          <TD>
					            <html:text name="editScoringConfigForm" property="maximumValueText15" size="4" maxlength="15" />
							  </TD>
					          <TD class=TD-SMALL>
					            <html:text name="editScoringConfigForm" property="pointsText15" size="4" maxlength="15" />
							  </TD>
					          <TD>
								<html:submit property="submitAdd15" value=" Add " />
					          </TD>
					        </TR>
		
							<%count=0;%>
							<logic:present name="scoreCategory15">
								<%dispClass = "TD-SMALL";%>	
								<logic:iterate id="scoreValue" name="scoreCategory15" property="scoreValues">
							        <TR>
										<logic:empty name="scoreValue" property="value">							        
								        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="minimumValue"/></TD>
								        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="maximumValue"/></TD>
										</logic:empty>
							          	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="points"/></TD>
								        <TD class='<%=dispClass%>'>
											<a href='javascript:DoAction("delete", "<bean:write filter="false" name="scoreCategory15" property="scoreCategoryName"/>", <%=count++%>)'>
												delete
											</a>
										</TD>
							        </TR>
									<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
										else dispClass = "TD-SHADED-SMALL"; %>	
						        </logic:iterate>
							</logic:present>

						</TBODY>
					</TABLE>
				</td>
			</tr>
	</tbody>
</table>










<div id="buttons">
	<html:submit property="submitSave" value=" Save  "  
		onclick="buttons.style.display='none';processing.style.visibility='visible';" />
	&nbsp; &nbsp;
	<html:submit property="submitCancel" value=" Cancel  "  
		onclick="buttons1.style.display='none';processing.style.visibility='visible';" />
</div>
   
<div id="processing" style="visibility: hidden">
	<h3><fmt:message key="message.in.process" /></h3>
</div >

</html:form>


