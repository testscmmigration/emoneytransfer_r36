<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<CENTER>


<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

<script language="javascript">
	function DoAction(formAction, configId, tranType, tranTypeDesc) { 
		selectProductForm[1].hiddenProductAction.value = formAction;
		selectProductForm[1].hiddenProductConfigId.value = configId;
		selectProductForm[1].hiddenProductTranTypeCode.value = tranType;
		selectProductForm[1].hiddenProductTranTypeDesc.value = tranTypeDesc;
		selectProductForm[1].submit();	
	} 
</script>

<h3>Transaction Scoring Configuration</h3>
<br>

<span class="error">
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/><br/>
	</html:messages>
</span>


</CENTER>

<html:form action="/selectProduct.do" styleId="selectProductForm">

<fieldset style="width:810;"> 
<legend class="TD-LABEL">Automation Velocity Filter</legend>
<div id="partnerSiteSelect_div">Select Program: 
    <html:select name="SelectProductForm" property="partnerSiteId" size="1" styleId="partnerSiteId" 
    	onchange="javascript:document.getElementById('selectProductForm').submit()">
        <html:option value="MGO">MGO</html:option>
        <html:option value="MGOUK">MGO UK</html:option>
        <html:option value="MGODE">MGO DE</html:option>
        <html:option value="WAP">WAP</html:option>
    </html:select>
</div>
<table width="800">
	<tbody>
		<tr>
			<td class="TD-LABEL" ><b>Max Transactions Automated per Period : </b></td>
			<td><html:text name="SelectProductForm" property="maximumNumberAutomatedPerPeriod" size="5" maxlength="5"/>
			</td>			
			<td class="TD-SMALL">
				<div id="formButtons">
					<html:submit property="submitSaveAutomationParms" value="Update Automation Filter"
					 onclick="formButtons.style.display='none';velocityProcessing.style.visibility='visible';" />
				</div>
				<div id="velocityProcessing" style="visibility: hidden" class="ERROR">
						<fmt:message key="message.in.process" />
				</div >
			</td>
			
		</tr>
		<tr>
			<td class="TD-LABEL" ><b>Automation Period</b>(in hours): </td>
			<td><html:text name="SelectProductForm"  property="automationPeriodInHrs" size="5" maxlength="5"/>
		</tr>
		
		<tr>
		</tr>

	</tbody>

</table>
</fieldset>	

</html:form>

<html:form action="/selectProduct.do">
<fieldset style="width:810;"> 
<legend class="TD-LABEL">Scoring Parameters by Product</legend>

<table>
	<tbody>
		<tr>
			<td class="TD-SMALL" colspan="2"><b>Product Type : </b> &nbsp;&nbsp;
				<html:select name="selectProductForm" property="tranType" size="1"> 
					<html:options collection="tranTypes" property="emgTranTypeCodeLabel" labelProperty="emgTranTypeDescLabel" />
				</html:select>		
			</td>
		</tr>
		<tr>
			<td class="TD-SMALL" colspan="2">&nbsp;</td>
		</tr>
		
		<tr>
			<td class="TD-SMALL">
				<div id="formButtons">
					<html:submit property="submitAutomation" value="Automation"
					 onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
				</div>
			</td>
			<td class="TD-SMALL">
				<div id="formButtons1">
					<html:submit property="submitScoringConfig" value="Scoring Configuration"
					 onclick="formButtons1.style.display='none';processing.style.visibility='visible';" />
				</div>
			</td>
		</tr>
	</tbody>
</table>     
</fieldset> 
&nbsp;&nbsp;
<div id="processing" style="visibility: hidden">
	<table>
		<tr>
			<td class="PROCESSING"><fmt:message key="message.in.process" /></td>
		</tr>
	</table>
</div >

<h3>Active Configurations </h3>

<html:hidden name="selectProductForm" property="hiddenProductAction"/>
<html:hidden name="selectProductForm" property="hiddenProductConfigId"/>
<html:hidden name="selectProductForm" property="hiddenProductTranTypeCode"/>
<html:hidden name="selectProductForm" property="hiddenProductTranTypeDesc"/>

<table>
	<tr>
<logic:iterate id="scoreConfiguration" name="scoreConfigurations">
	<td>
	<table border="1">
		<tbody>
			<tr>
				<td class="TD-HEADER-SMALL" colspan="2" align="center">
					<bean:write filter='false' name='scoreConfiguration' property='transactionType'/>
				</td> 
			</tr>
			<tr>
				<td class="TD-SMALL" align="right"><b>Allow Automation : </b>
				</td> 
				<td class="TD-SMALL">
					<bean:write filter='false' name='scoreConfiguration' property='autoAprvFlag'/>
				</td> 
			</tr>
			<tr>
				<td class="TD-SMALL" align="right"><b>Allow Scoring : </b>
				</td> 
				<td class="TD-SMALL">
					<bean:write filter='false' name='scoreConfiguration' property='scoreFlag'/>
				</td> 
			</tr>
	
			<tr>
				<td class="TD-SMALL" align="right"><b>Configuration Id : </b>
				</td> 
				<td class="TD-SMALL">

						<bean:write filter='false' name='scoreConfiguration' property='configurationId'/>
				</td> 
			</tr>
	
			<tr>
				<td class="TD-SMALL" align="right"><b>Version Number : </b>
				</td> 
				<td class="TD-SMALL">
					<bean:write filter='false' name='scoreConfiguration' property='configurationVersionNumber'/>
				</td> 
			</tr>
	
			<tr>
				<td class="TD-SMALL" align="right"><b>Accept Threshold : </b>
				</td> 
				<td class="TD-SMALL">
					<bean:write filter='false' name='scoreConfiguration' property='acceptThreshold'/>
				</td> 
			</tr>
	
			<tr>
				<td class="TD-SMALL" align="right"><b>Reject Threshold : </b>
				</td> 
				<td class="TD-SMALL">
					<bean:write filter='false' name='scoreConfiguration' property='rejectThreshold'/>
				</td> 
			</tr>
		</tbody>
	</table>    
	</td>  
	<td width="20">&nbsp;</td>
</logic:iterate>
	</tr>
</table>

</html:form>


