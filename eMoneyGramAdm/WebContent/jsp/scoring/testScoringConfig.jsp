<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<CENTER>

<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

</CENTER>

<% int count = 0; %>

<html:form action="/testScoringConfig.do">

<h3>Transaction Scoring - Test</h3>
<br>
<table class="TD-SMALL">
<tr valign="top"><td>
	<table class="TD-SMALL" border="1">
		<tbody>
		
			<tr class="TD-HEADER-SMALL">
				<td><b>Category</b></td>
				<td><b>Value</b></td>
			</tr>
			<tr>
				<td>Password Reset</td>
				<td>				
					<html:select name="testScoringConfigForm" property="result1" size="1">
						<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
						<html:options collection="booleanList" property="code" labelProperty="description" />
					</html:select>
				</td>
			</tr>
			<tr>
				<td>Customer Profile Status</td>
				<td>				
					<html:select name="testScoringConfigForm" property="result2" size="1">
						<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
						<html:options collection="statusList" property="code" labelProperty="code" />
					</html:select>	
				</td>
			</tr>
	
			<tr>
				<td>Negative Activities</td>
				<td>				
					<html:text name="testScoringConfigForm" property="result3" size="4" maxlength="15" />
				</td>
			</tr>
	
			<tr>
				<td>Days since last transaction</td>
				<td>				
					<html:text name="testScoringConfigForm" property="result4" size="4" maxlength="15" />
					<br> Enter "-1" for no transaction history
				</td>
			</tr>
			<tr>
				<td>Email Address in Blocked List</td>
				<td>				
					<html:select name="testScoringConfigForm" property="result5" size="1">
						<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
						<html:options collection="booleanList" property="code" labelProperty="description" />
					</html:select>
				</td>
			</tr>
			<tr>
				<td>Country Threshold Exceeded</td>
				<td>				
					<html:select name="testScoringConfigForm" property="result6" size="1">
						<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
						<html:options collection="booleanList" property="code" labelProperty="description" />
					</html:select>
				</td>
			</tr>
			<tr>
				<td>Receive state matches profile state</td>
				<td>				
					<html:select name="testScoringConfigForm" property="result7" size="1">
						<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
						<html:options collection="booleanList" property="code" labelProperty="description" />
					</html:select>
				</td>
			</tr>
			<tr>
				<td>Age Threshold</td>
				<td>				
					<html:select name="testScoringConfigForm" property="result8" size="1">
						<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
						<html:options collection="booleanList" property="code" labelProperty="description" />
					</html:select>
				</td>
			</tr>
			<tr>
				<td>Zip To State</td>
				<td>				
					<html:select name="testScoringConfigForm" property="result9" size="1">
						<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
						<html:options collection="booleanList" property="code" labelProperty="description" />
					</html:select>
				</td>
			</tr>
			<tr>
				<td>Area To State</td>
				<td>				
					<html:select name="testScoringConfigForm" property="result10" size="1">
						<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
						<html:options collection="booleanList" property="code" labelProperty="description" />
					</html:select>
				</td>
			</tr>
			<tr>
				<td>Address in Negative File</td>
				<td>				
					<html:select name="testScoringConfigForm" property="result11" size="1">
						<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
						<html:options collection="booleanList" property="code" labelProperty="description" />
					</html:select>
				</td>
			</tr>
			<tr>
				<td>Keywords in Email</td>
				<td>				
					<html:select name="testScoringConfigForm" property="result12" size="1">
						<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
						<html:options collection="booleanList" property="code" labelProperty="description" />
					</html:select>
				</td>
			</tr>
			<tr>
				<td>Transaction Monitoring Score</td>
				<td>				
					<html:text name="testScoringConfigForm" property="result13" size="4" maxlength="15" />
					<br> Enter "-1" for no transaction monitoring score
				</td>
			</tr>
			<tr>
				<td>Send Amount</td>
				<td>				
					<html:text name="testScoringConfigForm" property="result14" size="4" maxlength="15" />
				</td>
			</tr><tr>
				<td>Transaction Amount</td>
				<td>				
					<html:text name="testScoringConfigForm" property="result15" size="4" maxlength="15" />
				</td>
			</tr>
		</tbody>
	</table>
</td>
<td width="30">&nbsp;</td>
<td valign="top">
	<table class="TD-SMALL" border="1">
		<tbody>
			<tr>
				<td colspan="6"><b>Result </b></td>
			</tr>
		
			<tr class="TD-HEADER-SMALL">
				<td><b>Category</b></td>
				<td><b>Raw Score</b></td>
				<td><b>Weighted Score</b></td>
			</tr>
	
			<logic:present name="PasswordCategory">
			<tr>
		    	<TD class="TD-SMALL"><bean:write filter="false" name="PasswordCategory" property="scoreCatName"/></TD>
		    	<TD class="TD-SMALL" align="right"><bean:write filter="false" name="PasswordCategory" property="rawScoreNbr"/></TD>
	  			<TD class="TD-SMALL" align="right"><bean:write filter="false" name="PasswordCategory" property="wgtScoreNbr"/></TD>
			</tr>
			</logic:present>
	
			<logic:present name="ProfileCategory">
			<tr>
		    	<TD class="TD-SMALL"><bean:write filter="false" name="ProfileCategory" property="scoreCatName"/></TD>
		    	<TD class="TD-SMALL" align="right"><bean:write filter="false" name="ProfileCategory" property="rawScoreNbr"/></TD>
	  			<TD class="TD-SMALL" align="right"><bean:write filter="false" name="ProfileCategory" property="wgtScoreNbr"/></TD>
			</tr>
			</logic:present>
	
			<logic:present name="ChargeBackCategory">
			<tr>
		    	<TD class="TD-SMALL"><bean:write filter="false" name="ChargeBackCategory" property="scoreCatName"/></TD>
		    	<TD class="TD-SMALL" align="right"><bean:write filter="false" name="ChargeBackCategory" property="rawScoreNbr"/></TD>
	  			<TD class="TD-SMALL" align="right"><bean:write filter="false" name="ChargeBackCategory" property="wgtScoreNbr"/></TD>
			</tr>
			</logic:present>
	
			<logic:present name="DaysSinceCategory">
			<tr>
		    	<TD class="TD-SMALL"><bean:write filter="false" name="DaysSinceCategory" property="scoreCatName"/></TD>
		    	<TD class="TD-SMALL" align="right"><bean:write filter="false" name="DaysSinceCategory" property="rawScoreNbr"/></TD>
	  			<TD class="TD-SMALL" align="right"><bean:write filter="false" name="DaysSinceCategory" property="wgtScoreNbr"/></TD>
			</tr>
			</logic:present>

			<logic:present name="EmailCategory">
			<tr>
		    	<TD class="TD-SMALL"><bean:write filter="false" name="EmailCategory" property="scoreCatName"/></TD>
		    	<TD class="TD-SMALL" align="right"><bean:write filter="false" name="EmailCategory" property="rawScoreNbr"/></TD>
	  			<TD class="TD-SMALL" align="right"><bean:write filter="false" name="EmailCategory" property="wgtScoreNbr"/></TD>
			</tr>
			</logic:present>

			<logic:present name="CountryThresholdCategory">
			<tr>
		    	<TD class="TD-SMALL"><bean:write filter="false" name="CountryThresholdCategory" property="scoreCatName"/></TD>
		    	<TD class="TD-SMALL" align="right"><bean:write filter="false" name="CountryThresholdCategory" property="rawScoreNbr"/></TD>
	  			<TD class="TD-SMALL" align="right"><bean:write filter="false" name="CountryThresholdCategory" property="wgtScoreNbr"/></TD>
			</tr>
			</logic:present>

			<logic:present name="ReceiveStateCategory">
			<tr>
		    	<TD class="TD-SMALL"><bean:write filter="false" name="ReceiveStateCategory" property="scoreCatName"/></TD>
		    	<TD class="TD-SMALL" align="right"><bean:write filter="false" name="ReceiveStateCategory" property="rawScoreNbr"/></TD>
	  			<TD class="TD-SMALL" align="right"><bean:write filter="false" name="ReceiveStateCategory" property="wgtScoreNbr"/></TD>
			</tr>
			</logic:present>

			<logic:present name="SeniorIdCategory">
			<tr>
		    	<TD class="TD-SMALL"><bean:write filter="false" name="SeniorIdCategory" property="scoreCatName"/></TD>
		    	<TD class="TD-SMALL" align="right"><bean:write filter="false" name="SeniorIdCategory" property="rawScoreNbr"/></TD>
	  			<TD class="TD-SMALL" align="right"><bean:write filter="false" name="SeniorIdCategory" property="wgtScoreNbr"/></TD>
			</tr>
			</logic:present>
			<logic:present name="ZipStateCategory">
			<tr>
		    	<TD class="TD-SMALL"><bean:write filter="false" name="ZipStateCategory" property="scoreCatName"/></TD>
		    	<TD class="TD-SMALL" align="right"><bean:write filter="false" name="ZipStateCategory" property="rawScoreNbr"/></TD>
	  			<TD class="TD-SMALL" align="right"><bean:write filter="false" name="ZipStateCategory" property="wgtScoreNbr"/></TD>
			</tr>
			</logic:present>
			<logic:present name="AreaStateCategory">
			<tr>
		    	<TD class="TD-SMALL"><bean:write filter="false" name="AreaStateCategory" property="scoreCatName"/></TD>
		    	<TD class="TD-SMALL" align="right"><bean:write filter="false" name="AreaStateCategory" property="rawScoreNbr"/></TD>
	  			<TD class="TD-SMALL" align="right"><bean:write filter="false" name="AreaStateCategory" property="wgtScoreNbr"/></TD>
			</tr>
			</logic:present>
			<logic:present name="NegativeAddressCategory">
			<tr>
		    	<TD class="TD-SMALL"><bean:write filter="false" name="NegativeAddressCategory" property="scoreCatName"/></TD>
		    	<TD class="TD-SMALL" align="right"><bean:write filter="false" name="NegativeAddressCategory" property="rawScoreNbr"/></TD>
	  			<TD class="TD-SMALL" align="right"><bean:write filter="false" name="NegativeAddressCategory" property="wgtScoreNbr"/></TD>
			</tr>
			</logic:present>
			<logic:present name="EmailKeywordCategory">
			<tr>
		    	<TD class="TD-SMALL"><bean:write filter="false" name="EmailKeywordCategory" property="scoreCatName"/></TD>
		    	<TD class="TD-SMALL" align="right"><bean:write filter="false" name="EmailKeywordCategory" property="rawScoreNbr"/></TD>
	  			<TD class="TD-SMALL" align="right"><bean:write filter="false" name="EmailKeywordCategory" property="wgtScoreNbr"/></TD>
			</tr>
			</logic:present>
			<logic:present name="TransMonitorCategory">
			<tr>
		    	<TD class="TD-SMALL"><bean:write filter="false" name="TransMonitorCategory" property="scoreCatName"/></TD>
		    	<TD class="TD-SMALL" align="right"><bean:write filter="false" name="TransMonitorCategory" property="rawScoreNbr"/></TD>
	  			<TD class="TD-SMALL" align="right"><bean:write filter="false" name="TransMonitorCategory" property="wgtScoreNbr"/></TD>
			</tr>
			</logic:present>
			<logic:present name="SendAmountCategory">
			<tr>
		    	<TD class="TD-SMALL"><bean:write filter="false" name="SendAmountCategory" property="scoreCatName"/></TD>
		    	<TD class="TD-SMALL" align="right"><bean:write filter="false" name="SendAmountCategory" property="rawScoreNbr"/></TD>
	  			<TD class="TD-SMALL" align="right"><bean:write filter="false" name="SendAmountCategory" property="wgtScoreNbr"/></TD>
			</tr>
			</logic:present>
			<logic:present name="TransactionAmountCategory">
			<tr>
		    	<TD class="TD-SMALL"><bean:write filter="false" name="TransactionAmountCategory" property="scoreCatName"/></TD>
		    	<TD class="TD-SMALL" align="right"><bean:write filter="false" name="TransactionAmountCategory" property="rawScoreNbr"/></TD>
	  			<TD class="TD-SMALL" align="right"><bean:write filter="false" name="TransactionAmountCategory" property="wgtScoreNbr"/></TD>
			</tr>
			</logic:present>
		</tbody>
	</table>		
	<br>
	<table class="TD-SMALL" border="1">
		<tbody>
			<tr class="TD-HEADER-SMALL">
				<td><b>Final Score</b></td>
				<td><b>System Recommendation</b></td>
			</tr>
			<logic:present name="TransactionScoreResult">
			<tr>
		    	<TD class="TD-SMALL" align="right">
		    		<bean:write filter="false" name="TransactionScoreResult" property="score"/>
		    	</TD>
					<logic:equal  name="TransactionScoreResult" property="resultCode" value="REVW">
			  			<TD class="TD-SMALL" align="center" >
							REVIEW
			  			</TD>
					</logic:equal>	  			
					<logic:equal  name="TransactionScoreResult" property="resultCode" value="REJ">
			  			<TD align="center" bgcolor="#FF2020">
							REJECT
			  			</TD>
					</logic:equal>	  			
					<logic:equal  name="TransactionScoreResult" property="resultCode" value="APRV">
			  			<TD align="center" bgcolor="#20FF20">
							APPROVE
			  			</TD>
					</logic:equal>	  				
			</tr>
			</logic:present>
		</tbody>
	</table>		
</td></tr>
</table>
<br>
<div id="formButtons">
		<html:submit property="submitTestScore" value="  Score  "  
		onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
		<html:submit property="submitTestDone" value="  Done  "  
		onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
</div>
<br>
<div id="processing" style="visibility: hidden">
	<table>
		<tr>
			<td class="PROCESSING"><fmt:message key="message.in.process" /></td>
		</tr>
	</table>
</div >
<br>
<h3>Chosen Score Configuration</h3>
<table>
	<tbody>
		<tr>
			<td class="TD-SMALL"><b>Configuration ID : </b></td>
			<td class="TD-SMALL">
				<bean:write filter="false" name="scoreConfiguration" property="configurationId"/> 
			</td> 
		</tr>

		<tr>
			<td class="TD-SMALL"><b>Product Type : </b></td>
			<td class="TD-SMALL">
				<bean:write filter="false" name="currentTranType" property="emgTranTypeDesc"/> 
			</td> 
		</tr>
		
		<tr>
			<td class="TD-SMALL"><b>Accept Threshold : </b></td>
			<td class="TD-SMALL">
				<bean:write filter="false" name="scoreConfiguration" property="acceptThreshold"/>
			</td>
		</tr>
		<tr>
			<td class="TD-SMALL"><b>Reject Threshold : </b></td>
			<td class="TD-SMALL">
				<bean:write filter="false" name="scoreConfiguration" property="rejectThreshold"/>
			</td>
		<tr>					
	</tbody>
</table>
<br>
<table width="100%">
	<tbody valign="top">

	<logic:iterate id="scoreCategory" name="scoreCategories">
	
			<TABLE width="600" border="1" cellspacing="0">
			  <TBODY>
			  <TR class="TD-HEADER-SMALL">
			  	<TD>
				<bean:write filter="false" name="scoreCategory" property="scoreCategoryName"/>
				</TD>
			  </TR>
			  <TR><TD>
				<TABLE>
			        <TBODY>
			        <TR class="TD-LABEL-REQUIRED">
						<logic:equal name="scoreCategory" property="included" value="false">
				          <TD align="left" colspan="2"><b> DISABLED </b></TD>
						</logic:equal>
						<logic:notEqual name="scoreCategory" property="included" value="false">
				          <TD colspan="2">&nbsp;</TD>
						</logic:notEqual>
			        </TR>		        
			        <TR  class="TD-SMALL">
			          <TD class="TD-SMALL" align="right"><b>Weight : </b></TD>
			          <TD align="left">
			          	<bean:write filter="false" name="scoreCategory" property="weight"/> &nbsp;
						<logic:equal name="scoreCategory" property="included" value="true">
				          	(<bean:write filter="false" name="scoreCategory" property="weightPercentage"/>%)
						</logic:equal>
			          </TD>
			        </TR>
					</TBODY>
				</TABLE>      
				<BR>	
				<TABLE>
					<TBODY>
						<logic:present name="scoreCategory" property="scoreValues">
						<logic:equal name="scoreCategory" property="listTypeCode" value="range">
							<TR>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Minimum Value</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Maximum Value</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Points</b>&nbsp;</TD>
							</TR>
						</logic:equal>
						<logic:notEqual name="scoreCategory" property="listTypeCode" value="range">
							<TR>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Value</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Points</b>&nbsp;</TD>
							</TR>
						</logic:notEqual>		
							<%String dispClass = "TD-SMALL";%>	
							<logic:iterate id="scoreValue" name="scoreCategory" property="scoreValues">
						        <TR>
									<logic:equal name="scoreCategory" property="listTypeCode" value="range">
							        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="minimumValue"/></TD>
							        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="maximumValue"/></TD>
							          	<TD class='<%=dispClass%>' align="right"><bean:write filter="false" name="scoreValue" property="points"/></TD>
									</logic:equal>

									<logic:notEqual name="scoreCategory" property="listTypeCode" value="range">
							        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="value"/></TD>
							          	<TD class='<%=dispClass%>' align="right"><bean:write filter="false" name="scoreValue" property="points"/></TD>
									</logic:notEqual>
						        </TR>
								<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
									else dispClass = "TD-SHADED-SMALL"; %>	
					        </logic:iterate>
						</logic:present>        
						<TR>
						  <TD colspan="2">&nbsp;</TD>
						</TR>				
					</TBODY>
				</TABLE>
				</TD>
				</TR>
			</TBODY>
		</TABLE>
		<BR>
	</logic:iterate>  	
	</tbody>
</table>
</html:form>