<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<CENTER>


<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

</CENTER>

<html:form action="/showScoringConfig.do">

<h3><fmt:message key="label.transaction.scoring.configuration" /> (${fn:escapeXml(partnerSiteId)})</h3>
<br>
<table>
	<tbody>
		<tr>
			<td class="TD-SMALL" align="right"><b>Selected Product Type : </b></td>
			<td class="TD-SMALL">
				<bean:write filter="false" name="currentTranType" property="emgTranTypeDescLabel"/> 
			</td> 
		</tr>
		
		<tr>
			<td class="TD-SMALL" align="right"><b>Accept Threshold : </b></td>
			<td class="TD-SMALL">
				<bean:write filter="false" name="scoreConfiguration" property="acceptThreshold"/>
			</td>
		</tr>
		<tr>
			<td class="TD-SMALL" align="right"><b>Reject Threshold : </b></td>
			<td class="TD-SMALL">
				<bean:write filter="false" name="scoreConfiguration" property="rejectThreshold"/>
			</td>
		<tr>					
	</tbody>
</table>
   
<br>

<table width="100%">
	<tbody valign="top">
	<tr><td>
	<logic:iterate id="scoreCategory" name="scoreCategories">
	
			<TABLE width="600" border="1" cellspacing="0">
			  <TBODY>
			  <TR class="TD-HEADER-SMALL">
			  	<TD>
				<bean:write filter="false" name="scoreCategory" property="scoreCategoryName"/>
				</TD>
			  </TR>
			  <TR><TD>
				<TABLE>
			        <TBODY>
			        <TR class="TD-LABEL-REQUIRED">
						<logic:equal name="scoreCategory" property="included" value="false">
				          <TD align="left" colspan="2"><b> DISABLED </b></TD>
						</logic:equal>
						<logic:notEqual name="scoreCategory" property="included" value="false">
				          <TD colspan="2">&nbsp;</TD>
						</logic:notEqual>
			        </TR>		        
			        <TR  class=TD-SMALL>
			          <TD class=TD-SMALL align="right"><b>Weight : </b></TD>
			          <TD align="left">
			          	<bean:write filter="false" name="scoreCategory" property="weight"/> &nbsp;
						<logic:equal name="scoreCategory" property="included" value="true">
				          	(<bean:write filter="false" name="scoreCategory" property="weightPercentage"/>%)
						</logic:equal>
			          </TD>
			        </TR>
					</TBODY>
				</TABLE>      
				<BR>	
				<TABLE>
					<TBODY>

						<logic:present name="scoreCategory" property="scoreValues">

						<logic:equal name="scoreCategory" property="listTypeCode" value="range">
							<TR>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Minimum Value</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Maximum Value</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Points</b>&nbsp;</TD>
							</TR>
						</logic:equal>

						<logic:notEqual name="scoreCategory" property="listTypeCode" value="range">
							<TR>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Value</b>&nbsp;</TD>
							  <TD class="TD-HEADER-SMALL">&nbsp;<b>Points</b>&nbsp;</TD>
							</TR>
						</logic:notEqual>
		
							<%String dispClass = "TD-SMALL";%>	
							<logic:iterate id="scoreValue" name="scoreCategory" property="scoreValues">
						        <TR>
									<logic:equal name="scoreCategory" property="listTypeCode" value="range">
							        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="minimumValue"/></TD>
							        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="maximumValue"/></TD>
							          	<TD class='<%=dispClass%>' align="right"><bean:write filter="false" name="scoreValue" property="points"/></TD>
									</logic:equal>

									<logic:notEqual name="scoreCategory" property="listTypeCode" value="range">
							        	<TD class='<%=dispClass%>'><bean:write filter="false" name="scoreValue" property="value"/></TD>
							          	<TD class='<%=dispClass%>' align="right"><bean:write filter="false" name="scoreValue" property="points"/></TD>
									</logic:notEqual>
						        </TR>
								<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
									else dispClass = "TD-SHADED-SMALL"; %>	
					        </logic:iterate>
						</logic:present>        
						<TR>
						  <TD colspan="2">&nbsp;</TD>
						</TR>
						
					</TBODY>
				</TABLE>

				</TD>
				</TR>
			</TBODY>
		</TABLE>
		<BR>
	</logic:iterate>
    </td></tr>	
	</tbody>
</table>
	
<table>
	<tbody>
    <tr>
		<td align="left">
			<div id="formButtons">
						<html:submit property="submitClone" value=" Clone  "  
						onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
			</div>
		</td>

		<td align="left">
			<div id="formButtons1">
						<html:submit property="submitShowDone" value=" Done  "  
						onclick="formButtons1.style.display='none';processing.style.visibility='visible';" />
			</div>
		</td>
    </tr>		        
	</tbody>
</table>      

<br>
	
<div id="processing" style="visibility: hidden">
	<table>
		<tr>
			<td class="PROCESSING"><fmt:message key="message.in.process" /></td>
		</tr>
	</table>
</div >

</html:form>


