<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

<center>

<h3>
		Record Credit Card Chargeback
</h3>

<table>
<% if (request.getAttribute("msgText") != null) {%>
	<tr><td valign='top'><font color='#0000FF'><b>Action Description: </b></font></td><td class='td-small'>
	<% out.println((String)request.getAttribute("msgText")); %>
	</td></tr>
<%}%>
</table><br />

<html:form action="/recordCcChargeback">
	<table border='0' width='100%' cellpadding='1' cellspacing='1'>
		<tbody>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.transaction.id"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<html:hidden property="tranId" />
					<c:out value="${manualAdjustmentForm.tranId}" />
				</td>
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'><fmt:message key="label.face.amount"/></td>
				<td nowrap='nowrap' class='TD-INPUT'>:
					<html:hidden property="tranFaceAmt" />
					<c:out value="${manualAdjustmentForm.tranFaceAmt}" />
				</td>
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'><fmt:message key="label.send.amount"/> </td>
				<td nowrap='nowrap' class='TD-INPUT'>:
					<html:hidden property="tranSendAmt" />
					<c:out value="${manualAdjustmentForm.tranSendAmt}" />					
				</td>				
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.amount"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<html:text property="amount" />
				</td>
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.chargeback.type"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
				<html:hidden property="subReasonRequired" value="Y" />
				<html:select name="manualAdjustmentForm" property="subReasonCode" size="1">
						<html:option value=""><fmt:message key="option.select.one"/></html:option>												
						<html:options collection="TranSubReasons" property="value" labelProperty="label" />
				</html:select>						
				</td>
			</tr>
		</tbody>
	</table>
	
	<div id="formButtons">
	<br/>
		<html:submit property="submitRecord" value="Record" 
			onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
			&nbsp; &nbsp; &nbsp;
		<html:submit property="submitCancel" value="Cancel" />
	</div>
	
	<div id="processing" style="visibility: hidden">
		<table>
			<tr>
				<td class="PROCESSING"><fmt:message key="message.in.process" /></td>
			</tr>
		</table>
	</div >

</html:form>
</center>
