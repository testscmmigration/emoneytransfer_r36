<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

<center>

<h3>
		Confirmation of Transaction Action
</h3>

<table>
<% if (request.getAttribute("msgText") != null) {%>
	<tr><td valign='top'><font color='#0000FF'><b>Action Description: </b></font></td><td class='td-small'>
	<% out.println((String)request.getAttribute("msgText")); %>
	</td></tr>
<%}%>
</table><br />

<html:form action="/tranCancel">
	<table border='0' width='100%' cellpadding='1' cellspacing='1'>
		<tbody>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.transaction.id"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<html:hidden property="tranId" />
					<c:out value="${confirmSubReasonForm.tranId}" />						
				</td>
			</tr>
			<html:hidden property="tranSubStatusCode" value="${confirmSubReasonForm.tranSubStatusCode}"/>
			<html:hidden property="submitAutoCancelBankRqstRefund" value="${confirmSubReasonForm.submitAutoCancelBankRqstRefund}"/>
			<html:hidden property="submitAutoChargebackAndWriteOffLoss" value="${confirmSubReasonForm.submitAutoChargebackAndWriteOffLoss}"/>
			<html:hidden property="submitCancelBankRqstRefund" value="${confirmSubReasonForm.submitCancelBankRqstRefund}"/>
			<html:hidden property="submitRecCcAutoRef" value="${confirmSubReasonForm.submitRecCcAutoRef}"/>
			<html:hidden property="submitRecCcCxlRef" value="${confirmSubReasonForm.submitRecCcCxlRef}"/>
			<html:hidden property="submitCancelTransaction" value="${confirmSubReasonForm.submitCancelTransaction}"/>
			<html:hidden property="submitManCancelTran" value="${confirmSubReasonForm.submitManCancelTran}"/>
			
			
			<%--
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.denial.type"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
				<html:hidden property="subReasonRequired" value="Y" />
				<html:hidden property="subReasonDescription"/>
				<html:select name="confirmSubReasonForm" property="subReasonCode" size="1">
						<html:option value=""><fmt:message key="option.select.one"/></html:option>												
						<html:options collection="TranSubReasons" property="value" labelProperty="label" />
				</html:select>						
				</td>
			</tr>
			 --%>
		</tbody>
	</table>
	
	<div id="formButtons">
	<br/>
		<html:submit property="submitRecord" value="Record" 
			onclick="formButtons.style.display='none';processing.style.visibility='visible';document.confirmSubReasonForm.subReasonDescription.value = document.confirmSubReasonForm.subReasonCode.options[document.confirmSubReasonForm.subReasonCode.selectedIndex].text;" />
			&nbsp; &nbsp; &nbsp;
		<html:submit property="submitCancel" value="Cancel" />
	</div>
	
	<div id="processing" style="visibility: hidden">
		<table>
			<tr>
				<td class="PROCESSING"><fmt:message key="message.in.process" /></td>
			</tr>
		</table>
	</div >

</html:form>
</center>
