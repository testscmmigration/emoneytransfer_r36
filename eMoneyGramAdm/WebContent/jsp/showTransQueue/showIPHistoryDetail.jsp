<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

<html:form action="/showIPHistoryDetail">

<c:out value="${showIPHistoryDetailForm.logonId}" /> &nbsp; &nbsp; (Id:
	<b><c:out value="${showIPHistoryDetailForm.custId}" /></b>) &nbsp; &nbsp; &nbsp;
	<html:button property="close" value="Close Window" onclick="window.close()"/>
<br /><br />
<logic:notEmpty name="ipHistoryDetailList">
	
<table border='0' width='100%' cellpadding='1' cellspacing='1'>
	<thead>
		<tr>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.consumer.ip.address" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.web.server" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.logon.date" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.logon.result" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.logon.resultDetail" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.partner.site.id" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.logon.sourceDevice" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.logon.sourceApp" />
					</td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="ipDetail" items="${ipHistoryDetailList}">
			<tr>
				<td class='TD'><c:out value="${ipDetail.ipAddress}"/></td>
				<td class='TD'><c:out value="${ipDetail.webServerName}" /></td>
				<td class='TD'><c:out value="${ipDetail.loginDate}" /></td>
				<td class='TD'><c:out value="${ipDetail.result}" /></td>
				<c:if test="${ipDetail.tainted}">
					<td class='TD'><font color="red"><b><c:out value="(Tainted)" /></b></font></td>
				</c:if>
				<c:if test="${!ipDetail.tainted}">
					<td class='TD'><c:out value="" /></td>
				</c:if>
				<td class='TD'><c:out value="${ipDetail.partnerSiteId}" /></td>				
				<td class='TD'><c:out value="${ipDetail.sourceDevice}" /></td>
				<td class='TD'><c:out value="${ipDetail.sourceApp}" /></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<br />
</logic:notEmpty>
<center>
<html:button property="close" value="Close Window" onclick="window.close()"/>

</center>
</html:form>
