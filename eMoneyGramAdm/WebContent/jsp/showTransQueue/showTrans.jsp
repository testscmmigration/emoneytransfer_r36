<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>

<!-- <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/blitzer/jquery-ui.css" /> -->


<div id="transactionFormDiv">
<b id="transactionFormLabel"><fmt:message key="label.partner.site" />: </b>
	<html:select name="showTransQueueForm" styleId="partnerSiteId" property="partnerSiteId" size="1" onchange="this.form.submit();">
	<html:optionsCollection name="programNames" />
</html:select>


<b><fmt:message key="label.profile.type" />: </b>
	<html:select name="showTransQueueForm" property="profileType" size="1"  onchange="this.form.submit();">
		<html:option value="All">
				<fmt:message key="option.all" />
	</html:option>
	<html:option value="Guest"> Guest </html:option>
	<html:option value="Registered"> Registered </html:option></html:select>
</div>
