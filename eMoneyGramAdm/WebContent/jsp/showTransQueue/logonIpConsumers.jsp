<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<Script>
function closeAndRedirect(url) {
 opener.window.location.href=url;
 window.close();
}

</Script>
<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

<html:form action="/logonIpConsumers">

<fmt:message key="label.consumer.ip.address"/>: &nbsp;
<b><c:out value="${logonIpConsumersForm.ipAddress}" /></b> &nbsp; &nbsp; &nbsp;
	<html:button property="close" value="Close Window" onclick="window.close()"/>
<br /><br />
<logic:notEmpty name="ipConsumerList">
	
<table border='0' width='100%' cellpadding='1' cellspacing='1'>
	<thead>
		<tr>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.login.id"/></td>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.consumer.id"/></td>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.block.status"/></td>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.status"/></td>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.first.name"/></td>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.last.name"/></td>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.phone"/></td>		
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.cust.birth.date" /></td>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.profile.eff.date" /></td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="ipConsumer" items="${ipConsumerList}">
			<tr>
				<td nowrap='nowrap' class='TD-SMALL'><c:out value="${ipConsumer.logonId}"/></td>
				<td nowrap='nowrap' class='TD-SMALL'><A href="javascript:closeAndRedirect('<c:out value="${ipConsumer.custIdLink}"/>')"><c:out value="${ipConsumer.custId}"/></A></td>
				<td nowrap='nowrap' class='TD-SMALL'><c:out value="${ipConsumer.custBlockedCode}"/></td>
				<td nowrap='nowrap' class='TD-SMALL'><c:out value="${ipConsumer.statCode}" />/<c:out value="${ipConsumer.subStatCode}" /></td>
				<td nowrap='nowrap' class='TD-SMALL'><c:out value="${ipConsumer.frstName}" /></td>
				<td nowrap='nowrap' class='TD-SMALL'><c:out value="${ipConsumer.lastName}"/></td>
				<td nowrap='nowrap' class='TD-SMALL'><c:out value="${ipConsumer.phoneNbr}" /></td>
				<td nowrap='nowrap' class='TD-SMALL'><c:out value="${ipConsumer.brthDate}" /></td>
				<td nowrap='nowrap' class='TD-SMALL'><c:out value="${ipConsumer.beginDate}" /> -
					<c:out value="${ipConsumer.endDate}" /> </td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<br />
</logic:notEmpty>
<center>
<html:button property="close" value="Close Window" onclick="window.close()"/>

</center>
</html:form>
