<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ page import="java.util.List"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script src="../../Display/jscript/textCounter.js" language="JavaScript"></script>
<script src="../../Display/jscript/showPopup.js" language="JavaScript"></script>
<script src="../../Display/jscript/consumerStatus.js"
	language="JavaScript"></script>
	<%
 String partnerSiteId = ESAPI.encoder().encodeForHTMLAttribute((String)request.getAttribute("partnerSiteId"));
 String customerId = ESAPI.encoder().encodeForHTMLAttribute((String)request.getAttribute("custId"));
 
%>
<SCRIPT LANGUAGE="JavaScript">
// Added for MBO-3458 to show a customized popup to display error message
var alert = new CustomAlert();
 function CustomAlert(){
    this.render = function(dialog){
        var winW = window.innerWidth;
        var winH = window.innerHeight;
        var dialogoverlay = document.getElementById('dialogoverlay');
        var dialogbox = document.getElementById('dialogbox');
        dialogoverlay.style.display = "block";
        dialogoverlay.style.height = winH+"px";
        dialogbox.style.left = (winW/2) - (450 * .5)+"px";
        dialogbox.style.top = "100px";
        dialogbox.style.width = "300px";
        dialogbox.style.display = "block";
        document.getElementById('dialogboxbody').innerHTML = dialog;
        document.getElementById('dialogboxfoot').innerHTML = '<button onclick="alert.ok()">OK</button>';
    }
	this.ok = function(){
		document.getElementById('dialogbox').style.display = "none";
		document.getElementById('dialogoverlay').style.display = "none";
	}
}
window.onload = pop;
function pop() {
/*	var kbaErrMsg = document.getElementById("kbaErrMsg").value;
	if(null!= kbaErrMsg){
		alert.render('The request has timed out');
	}
	return true;*/
} 
//Modified function for MBO-2932

function ClipBoardEdir(testval) 
{
	// Copy textarea, pre, div, etc.
	if (document.body.createTextRange) {
        // IE 
        var textRange = document.body.createTextRange();
        textRange.moveToElementText(testval);
        textRange.select();
        textRange.execCommand("Copy");     
    }
	else if (window.getSelection && document.createRange) {
        // non-IE        
        var range = document.createRange();
        range.selectNodeContents(testval);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range); 
     try {  
		    var successful = document.execCommand('copy');  
		    var msg = successful ? 'successful' : 'unsuccessful';  
		    console.log('Copy command was ' + msg);  
		} catch(err) {  
		    console.log('Oops, unable to copy');  
		}         
	} 
}

function hideit()
{
	guidOuter.style.display='inline';
	guidCopy.style.display='none';	
}

</SCRIPT>
<style>
#dialogoverlay {
	display: none;
	opacity: .8;
	position: fixed;
	top: 0px;
	left: 0px;
	background: #FFF;
	width: 100%;
	z-index: 10;
}

#dialogbox {
	display: none;
	position: fixed;
	background: #000;
	border-radius: 7px;
	width: 550px;
	z-index: 10;
}

#dialogbox>div {
	background: #FFF;
	margin: 4px;
}

#dialogbox>div>#dialogboxbody {
	background: #eeeeee;
	padding: 20px;
	color: #333;
	font-weight: bold;
	text-align: center;
}

#dialogbox>div>#dialogboxfoot {
	background: #eeeeee;
	padding: 10px;
	text-align: center;
}
</style>
<div id="dialogoverlay"></div>
<div id="dialogbox">
	<div>
		<div id="dialogboxbody"></div>
		<div id="dialogboxfoot"></div>
	</div>
</div>
<fmt:setTimeZone value="${userTimeZone}" />

<!-- Code modified for removing country code appended to customer Id as part of MBO-5150 -->
		<c:set var="custIdMarket" value="${showCustomerProfileForm.custId}" />


<h4>
	<html:messages id="message" property="headerMessage">
		<c:if test="${message != null}">
			<c:out value="${message}" />
		</c:if>
	</html:messages>
</h4>
<div>
	<html:messages id="message" property="kbaMessage">
		<input type="hidden" value="${message}" name="kbaFlag" id="kbaErrMsg" />
	</html:messages>
</div>
<h3>
	<fmt:message key="label.customer.profile" />
</h3>

<div class="cProfile cProfile_${market}">



	<fieldset class="fieldset1">
		<html:form type="emgadm.consumer.UpdateCustomerProfileForm"
			name="superTaintForm" target="showBlockedList"
			action="showBlockedList" styleId="superTaintForm" method="GET">
			<input type="hidden" value="<%=customerId%>"
				name="custId" />
			<button type="submit" value="Super Taint">Super Taint</button>
		</html:form>
		<legend class="legend">
			<fmt:message key="label.dashboard" />
			:
		</legend>
		<c:out value="${showCustomerProfileForm.custLogonId}" />
		&nbsp; &nbsp; (Id: <b><c:out value="${custIdMarket}" />
		</b>)

		<center>
			<logic:notEmpty name="dashboard">

				<table border='1' cellpadding='1' cellspacing='1' bgcolor='#e6e6fa'>
					<tr>
						<td class="td-small"><b>First Login Date:</b> <fmt:formatDate
								value="${dashboard.lastLogin}" pattern="dd/MMM/yyyy h:mm a z" />
							&nbsp; &nbsp; &nbsp; <b>Last Login Date:</b> <fmt:formatDate
								value="${dashboard.lastLogin}" pattern="dd/MMM/yyyy h:mm a z" />
						</td>
					</tr>

					<tr>
						<td class="td-small">
							<table>
								<tr>
									<td class="td-label-small">Good Trans In last 30 days:</td>

									<logic:equal name="dashboard" property="recentTranCnt"
										value="0">
										<td class="td-small"><b>Tran Count:</b> 0</td>
									</logic:equal>

									<logic:notEqual name="dashboard" property="recentTranCnt"
										value="0">
										<td class="td-small"><b>Tran Count:</b> <logic:equal
												name="dashDetail" value="Y">
												<a
													href='../../showDashboardDetail.do?from=dash&id=recentTran'>
													<bean:write name="dashboard" property="recentTranCnt" />
												</a>
											</logic:equal> <logic:notEqual name="dashDetail" value="Y">
												<bean:write name="dashboard" property="recentTranCnt" />
											</logic:notEqual> &nbsp; &nbsp; &nbsp; <b>Total Amount:</b> <bean:write
												name="dashboard" property="recentAmt" /><br /> <bean:define
												id="rcvs" name="dashboard" property="receiverList" /></td>
								</tr>
								<tr>
									<td class="td-small">&nbsp;</td>
									<td class="td-small"><b>Receiver:</b> <logic:iterate
											id="rcv" name="rcvs">
											<bean:write name="rcv" property="frstName" />
											<bean:write name="rcv" property="lastName" />
											<logic:equal name="dashDetail" value="Y">
												<a
													href='../../showDashboardDetail.do?from=dash&id=recentRcv&frst=<bean:write
							name="rcv" property="frstName" />&last=<bean:write
							name="rcv" property="lastName" />'>
													<bean:write name="rcv" property="tranCnt" />
												</a>
											</logic:equal>
											<logic:notEqual name="dashDetail" value="Y">
												<bean:write name="rcv" property="tranCnt" />
											</logic:notEqual>
						&nbsp; &nbsp;
					</logic:iterate></td>
									</logic:notEqual>
								</tr>
							</table></td>
					</tr>
					<tr>
						<td class="td-small"><b>Total Tran:</b> <logic:equal
								name="dashboard" property="totalTranCnt" value="0">
			0
			</logic:equal> <logic:notEqual name="dashboard" property="totalTranCnt" value="0">
								<logic:equal name="dashDetail" value="Y">
									<a href='../../showDashboardDetail.do?from=dash&id=totalTran'>
										<bean:write name="dashboard" property="totalTranCnt" />
									</a>
								</logic:equal>
								<logic:notEqual name="dashDetail" value="Y">
									<bean:write name="dashboard" property="totalTranCnt" />
								</logic:notEqual>
							</logic:notEqual> &nbsp; &nbsp; <logic:notEqual name="dashboard"
								property="denyTranCnt" value="0">
								<b>Deny:</b>
								<logic:equal name="dashDetail" value="Y">
									<a href='../../showDashboardDetail.do?from=dash&id=denyTran'>
										<bean:write name="dashboard" property="denyTranCnt" />
									</a>
								</logic:equal>
								<logic:notEqual name="dashDetail" value="Y">
									<bean:write name="dashboard" property="denyTranCnt" />
								</logic:notEqual>
				&nbsp; &nbsp;
			</logic:notEqual> <logic:notEqual name="dashboard" property="sendTranCnt" value="0">
								<b>Send:</b>
								<logic:equal name="dashDetail" value="Y">
									<a href='../../showDashboardDetail.do?from=dash&id=sendTran'>
										<bean:write name="dashboard" property="sendTranCnt" />
									</a>
								</logic:equal>
								<logic:notEqual name="dashDetail" value="Y">
									<bean:write name="dashboard" property="sendTranCnt" />
								</logic:notEqual>
				&nbsp; &nbsp;
			</logic:notEqual> <logic:notEqual name="dashboard" property="cancelTranCnt" value="0">
								<b>Cancel:</b>
								<logic:equal name="dashDetail" value="Y">
									<a href='../../showDashboardDetail.do?from=dash&id=cancelTran'>
										<bean:write name="dashboard" property="cancelTranCnt" />
									</a>
								</logic:equal>
								<logic:notEqual name="dashDetail" value="Y">
									<bean:write name="dashboard" property="cancelTranCnt" />
								</logic:notEqual>
				&nbsp; &nbsp;
			</logic:notEqual> <logic:notEqual name="dashboard" property="fundedTranCnt" value="0">
								<b>Funded:</b>
								<logic:equal name="dashDetail" value="Y">
									<a href='../../showDashboardDetail.do?from=dash&id=fundedTran'>
										<bean:write name="dashboard" property="fundedTranCnt" />
									</a>
								</logic:equal>
								<logic:notEqual name="dashDetail" value="Y">
									<bean:write name="dashboard" property="fundedTranCnt" />
								</logic:notEqual>
				&nbsp; &nbsp;
			</logic:notEqual> <b>Average Amount:</b> <bean:write name="dashboard"
								property="tranAvgAmt" /></td>
					</tr>
					<tr>
						<td class="td-small"><logic:notEqual name="dashboard"
								property="bankAccountCnt" value="0">
								<b>Bank Account:</b>
								<logic:equal name="dashDetail" value="Y">
									<a href='../../showDashboardDetail.do?from=dash&id=bankAccts'>
										<bean:write name="dashboard" property="bankAccountCnt" />
									</a>
								</logic:equal>
								<logic:notEqual name="dashDetail" value="Y">
									<bean:write name="dashboard" property="bankAccountCnt" />
								</logic:notEqual>
				&nbsp; &nbsp;
			</logic:notEqual> <logic:notEqual name="dashboard" property="creditCardAccountCnt"
								value="0">
								<b>Credit Card Account:</b>
								<logic:equal name="dashDetail" value="Y">
									<a
										href='../../showDashboardDetail.do?from=dash&id=creditCardAccts'>
										<bean:write name="dashboard" property="creditCardAccountCnt" />
									</a>
								</logic:equal>
								<logic:notEqual name="dashDetail" value="Y">
									<bean:write name="dashboard" property="creditCardAccountCnt" />
								</logic:notEqual>
				&nbsp; &nbsp;
			</logic:notEqual> <logic:notEqual name="dashboard" property="debitCardAccountCnt"
								value="0">
								<b>Debit Card Account:</b> 
								<logic:equal name="dashDetail" value="Y">
									<a
										href='../../showDashboardDetail.do?from=dash&id=debitCardAccts'>
										<bean:write name="dashboard" property="debitCardAccountCnt" />
									</a>
								</logic:equal>
								<logic:notEqual name="dashDetail" value="Y">
									<bean:write name="dashboard" property="debitCardAccountCnt" />
								</logic:notEqual>
							</logic:notEqual></td>
					</tr>
				</table>
			</logic:notEmpty>
		</center>

	</fieldset>

	<br />

	<fieldset class="fieldset1">
		<legend class="legend">
			<fmt:message key="label.profile.info" />
			:
		</legend>
		<table border='0' width='100%' cellpadding='1' cellspacing='1'>
			<tbody>
				<logic:equal name="isTainted" value="Y">
					<tr>
						<td colspan='2'>
							<h3>
								<fmt:message key="label.profile.tainted" />
							</h3></td>
						<logic:equal name="isSSNTainted" value="Y">
							<html:form type="emgadm.consumer.UpdateCustomerProfileForm"
								name="clearSSNTaintForm" action="clearSSNTaint">
								<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
											key="label.ssnDetaint" />: </b>
								</td>
								<td nowrap="nowrap" class="td-small"><html:hidden
										name="showCustomerProfileForm" property="custId" /> <logic:equal
										name="clearSSNTaint" value="Y">
										<input type="submit" class="transbtn2" value="Clear SSN Taint" />
									</logic:equal> <logic:notEqual name="clearSSNTaint" value="Y">
							See Manager for removal
						</logic:notEqual></td>
							</html:form>
						</logic:equal>
					</tr>
				</logic:equal>
				<logic:notEqual name="isTainted" value="Y">
					<logic:equal name="profileMasterOnlyTaint" value="Y">
						<tr>
							<td colspan='3'>
								<h3>
									<fmt:message key="label.profile.tainted.master.only" />
								</h3></td>
							<html:form type="emgadm.consumer.UpdateCustomerProfileForm"
								name="clearMasterTaintForm" action="clearMasterTaint">
								<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
											key="label.masterDetaint" />: </b>
								</td>
								<td nowrap="nowrap" class="td-small"><html:hidden
										name="showCustomerProfileForm" property="custId" /> <logic:equal
										name="clearMasterTaint" value="Y">
										<input type="submit" class="transbtn2"
											value="Clear Master Taint" />
									</logic:equal> <logic:notEqual name="clearMasterTaint" value="Y">
							See Manager for removal
						</logic:notEqual></td>
							</html:form>
						</tr>
					</logic:equal>
				</logic:notEqual>
				<tr>
					<td nowrap="nowrap" class="td-small" colspan="3"><logic:equal
							name="searchAuth" value="Y">
							<a href="../../consumerSearchSelection.do"><fmt:message
									key="label.search.similar.profile" />
							</a>
						</logic:equal></td>
					<td></td>
					<td></td>
				</tr>


				<tr>
					<%-- Added by Ankit Bhatt for MBO-222 --%>

					<c:if
						test="${fn:toLowerCase(showCustomerProfileForm.profileType) ne 'guest'}">
						<%--ended by Ankit Bhatt for MBO-222 --%>
						<td nowrap="nowrap" class="td-small" colspan="1"><logic:equal
								name="searchAuth" value="Y">

								<logic:notEqual name="showCustomerProfileForm"
									property="createdIPAddress" value="">
									<b><fmt:message key="label.create.ipaddress" />:</b>
						</td>
						<td nowrap="nowrap" class="td-small" align="left"><c:out
								value="${showCustomerProfileForm.createdIPAddress}" />
						</td>
						<td></td>
						</logic:notEqual>
						<logic:equal name="showCustomerProfileForm"
							property="createdIPAddress" value="">
							</td>
							<td nowrap="nowrap" class="td-small" align="left"></td>
							<td></td>
						</logic:equal>
						</logic:equal>
						</td>
						<td nowrap="nowrap" class="td-small" align="right"></td>
						<td nowrap="nowrap" class="td-small" align="left"></td>
						</td>
						<%-- Added for MBO-222 --%>
					</c:if>
					<%--ended MBO-222 --%>
				</tr>

				<!-- Code modified to disable the button -->
				<logic:equal name="profileDeletedFromLdap" value="Y"> 
					<tr>
						<%-- Added by Ankit Bhatt for MBO-222 --%>
						<c:if
							test="${fn:toLowerCase(showCustomerProfileForm.profileType) ne 'guest'}">
							<%--ended by Ankit Bhatt for MBO-222 --%>
							<td nowrap="nowrap" class="td-small" align="left" colspan='1'><b>User
									is Deleted from eDirectory LDAP</b>
							</td>
							
							<html:form type="emgadm.consumer.UpdateCustomerProfileForm"
								name="receateLDAPEntryForm" action="recreateLDAPEntry">
								<td nowrap="nowrap" class="td-small"><html:hidden
										name="showCustomerProfileForm" property="custId" /> 
										
	
										<logic:equal
										name="recreateLDAPEntry" value="Y">
										<input type="submit" class="transbtn2"
											value="Recreate LDAP Entry" id="submittbutton"
											onclick="this.disabled=disabled;
						document.getElementById('submittbutton').disabled='disable';
						this.form.submit(); " />

									</logic:equal> <logic:notEqual name="recreateLDAPEntry" value="Y">
							See Manager for LDAP recreate functionality.
						</logic:notEqual></td>
							</html:form>
							<%-- Added by Ankit Bhatt for MBO-222 --%>
						</c:if>
						<%--ended by Ankit Bhatt for MBO-222 --%>
					</tr>
				 </logic:equal> 
				<%-- Added by Ankit Bhatt for MBO-222 --%>

				<!-- Added for MBO-5465 -->

				<c:if
					test="${showCustomerProfileForm.custStatusCode == 'NAT' && showCustomerProfileForm.EDirectoryGuid != null}">							

					<logic:notEqual name="profileDeletedFromLdap" value="Y">

						<html:form type="emgadm.consumer.UpdateCustomerProfileForm"
							name="removeLDAPEntryForm" action="removeLDAPEntry">
							<td nowrap="nowrap" class="td-small"><html:hidden
									name="showCustomerProfileForm" property="custId" /> <%-- <logic:equal
									name="removeLDAPEntry" value="Y"> --%>
									<input type="submit" class="transbtn2"
										value="Remove LDAP Entry" id="submittbutton"
										onclick="this.disabled=disabled;
										document.getElementById('submittbutton').disabled='disable';
										this.form.submit(); " />

								<%-- </logic:equal> --%></td>
						</html:form>
					</logic:notEqual>

				</c:if>
				<c:if
					test="${fn:toLowerCase(showCustomerProfileForm.profileType) ne 'guest'}">
					<%--ended by Ankit Bhatt for MBO-222 --%>
					<logic:notEqual name="profileDeletedFromLdap" value="Y">
						<tr>
							<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
										key="label.invalid.logon.try" />:</b>
							</td>
							<td nowrap="nowrap" class="td-small" align="left"><c:out
									value="${showCustomerProfileForm.invlLoginTryCnt}" />
							</td>
							<td>&nbsp;</td>
							<td nowrap="nowrap" class="td-small" align="right"><b>Profile
									Locked: </b>
							</td>

							<logic:equal name="profileLocked" value="Y">
								<html:form type="emgadm.consumer.UnlockCustomerProfileForm"
									name="showCustomerProfileForm" action="unlockCustomerProfile">
									<html:hidden name="showCustomerProfileForm" property="custId" />
									<td nowrap="nowrap" class="td-small" align="left">Yes&nbsp;<input
										type="submit" class="transbtn2" value="Unlock Profile" />
									</td>
								</html:form>
							</logic:equal>
							<logic:notEqual name="profileLocked" value="Y">
								<td nowrap="nowrap" class="td-small" align="left">No&nbsp;</td>
							</logic:notEqual>
						</tr>
					</logic:notEqual>
					<%-- Added for MBO-222 --%>
				</c:if>
				<%-- change ended --%>
				<tr>
					<html:form type="emgadm.consumer.UpdateCustomerPasswordForm"
						name="showCustomerProfileForm" action="updateCustomerPassword">
						<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
									key="label.consumer.logon.id" />: </b>
						</td>
						<td nowrap="nowrap" class="td-small"><c:out
								value="${showCustomerProfileForm.custLogonId}" /> &nbsp; &nbsp;
							(Id: <b><c:out value="${custIdMarket}" />
						</b>)</td>
						<td nowrap="nowrap" class="td-small">&nbsp;</td>
						<logic:equal name="updatePw" value="Y">
							<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
										key="label.new.password" />: </b>
							</td>
							<td nowrap="nowrap" class="td-small"><html:hidden
									name="showCustomerProfileForm" property="custId" /> <%-- Added by Ankit Bhatt for MBO-222 --%>
								<c:choose>
									<c:when
										test="${fn:toLowerCase(showCustomerProfileForm.profileType) ne 'guest'}">
										<html:password property="newValue" value="" maxlength="20"
											size="20\" autocomplete=\"off" />
										<input type="submit" class="transbtn2" value="Set Password" />
									</c:when>
									<c:otherwise>
										<input type="text" disabled="disabled" />
										<input type="submit" class="transbtn2" value="Set Password"
											disabled="disabled" />
									</c:otherwise>
								</c:choose> <%--ended by Ankit Bhatt for MBO-222 --%> <font class="error">
									<html:messages id="message" property="newValue">
										<c:out value="${message}" />
									</html:messages> </font></td>
						</logic:equal>
						<logic:notEqual name="updatePw" value="Y">
							<td nowrap="nowrap" colspan="2">&nbsp;</td>
						</logic:notEqual>
					</html:form>
				</tr>
				<!-- MBO - 2158 CHANGES  -->
				<tr>
					<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
								key="label.consumer.emailAgeScore" />: </b>
					</td>
					<td nowrap="nowrap" class="td-small">
						<div id="guidOuter" style="visibility: visible">
							<c:out value="${showCustomerProfileForm.emailScore}" />
						</div></td>
				</tr>
				<!-- MBO-2587 Email first seen date -->
				<tr>
					<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
								key="label.consumer.emailFirstSeen" />: </b>
					</td>
					
					
					<td nowrap="nowrap" class="td-small">
						<div id="guidOuter" style="visibility: visible">
							<c:out value="${showCustomerProfileForm.emailFirstSeen}" />
						</div></td>
					
					<td>&nbsp;</td>
					<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
					           key="label.source.device" />:</b></td>
					           
					 <td nowrap="nowrap" class="td-small">
						<div id="guidOuter" style="visibility: visible">
							<c:out value="${showCustomerProfileForm.sourceDevice}" />
						</div></td>

				</tr>
				
				<!-- MBO-3819 Email age name match -->
				<tr>
					<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
								key="label.consumer.emailNameMatch" />: </b>
					</td>
					
					<td nowrap="nowrap" class="td-small">
						<div id="guidOuter" style="visibility: visible">
							<c:out value="${showCustomerProfileForm.emailAgeNameMatch}" />
						</div></td>
						
						<td>&nbsp;</td>
					<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
					            key="label.source.app" />:</b></td>
									
					<td nowrap="nowrap" class="td-small">
						<div id="guidOuter" style="visibility: visible">
							<c:out value="${showCustomerProfileForm.sourceApp}" />
						</div></td>
						
					
					
				</tr>
				
				<tr>
					<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
								key="label.edir.guid" />: </b>
					</td>
					<td nowrap="nowrap" class="td-small">
						<div id="guidOuter" style="visibility: visible">
							<c:if test="${showCustomerProfileForm.EDirectoryGuid == null}">Unknown</c:if>
							<c:if test="${showCustomerProfileForm.EDirectoryGuid == ''}">Unknown</c:if>
							<c:if test="${showCustomerProfileForm.EDirectoryGuid != ''}">

								<div id='markUp' style="visibility: visible">
									<c:out value="${showCustomerProfileForm.EDirectoryGuid}" />
								</div>

								<a
									href="javascript:ClipBoardEdir(document.getElementById('markUp'));">
									(Copy)</a>
						</div>

						<div id="guidCopy" style="display: none">
							<span class="error">E-Directory Guid is Copied to your
								clipboard</span>
						</div> </c:if></td>
					  
					<td>&nbsp;</td>          
					<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
								key="label.rsa.enrollment.date" />: </b>
					</td>
					<td nowrap="nowrap" class="td-small"><c:if
							test="${showCustomerProfileForm.adaptiveAuthProfileCompleteDate == null}">
							<fmt:message key="msg.not.enrolled" />
						</c:if> <c:if
							test="${showCustomerProfileForm.adaptiveAuthProfileCompleteDate != null}">
							<fmt:formatDate
								value="${showCustomerProfileForm.adaptiveAuthProfileCompleteDate}"
								pattern="dd/MMM/yyyy h:mm a z" />
						</c:if></td>

				</tr>

				<tr>
					<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
								key="label.consumer.name" />: </b>
					</td>
					<td nowrap="nowrap" class="td-small cpName"><span
						class="contain"> <c:out
								value="${showCustomerProfileForm.custFirstName}" /> <c:if
								test="${showCustomerProfileForm.custMiddleName != null}">
								<c:out value="${showCustomerProfileForm.custMiddleName}" />
							</c:if> <c:out value="${showCustomerProfileForm.custLastName}" /> <c:if
								test="${showCustomerProfileForm.custSecondLastName != null}">
								<c:out value="${showCustomerProfileForm.custSecondLastName}" />
							</c:if> &nbsp;&nbsp; <c:set var="custId"
								value="${showCustomerProfileForm.custId}" /> <logic:equal
								name="phoneTainted" value="N">
								<logic:equal name="updateProfile" value="Y">
									<c:url value="updateCustomerProfile.do" var="editProfileUrl">
										<c:param name="custId" value="${custId}" />
										<c:if test="${showCustomerProfileForm.loyaltyPgmMembershipId != null}">
											<c:param name="plusNumber" value="${showCustomerProfileForm.loyaltyPgmMembershipId}" />
										</c:if>	
									</c:url>
									<html:link action="${editProfileUrl}">
										<fmt:message key="label.edit.consumer.profile" />
									</html:link>
								</logic:equal>
							</logic:equal> </span> <a
						href="http://www.facebook.com/search.php?q=<c:out value="${showCustomerProfileForm.custFirstName} ${showCustomerProfileForm.custMiddleName} ${showCustomerProfileForm.custLastName}" />"
						class="cpFacebook name contain" target="_blank"
						title="<fmt:message key="label.search.facebook" />"> <fmt:message
								key="label.search.facebook" /> </a></td>
					<td nowrap="nowrap" class="td-small">&nbsp;</td>
					<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
								key="label.created.partner.site.id" />:</b>
					</td>
					<!-- MBO-100: Show Profile Type on EMT Admin Consumer profile Page-->
					<td nowrap="nowrap" class="td-small"><c:out
							value="${showCustomerProfileForm.partnerSiteId}" /> &nbsp;
						&nbsp; &nbsp; &nbsp; <b><fmt:message key="label.profile.type" />:</b>
						<c:out value="${showCustomerProfileForm.profileType}" /></td>

				</tr>
				
				<!-- Changes for 5646-->
				<tr>
					<td nowrap="nowrap" class="td-small" align="right"><b>Gender:
					</b>
					</td>
					<td nowrap="nowrap" class="td-small">
							<c:out value="${showCustomerProfileForm.gender}" />
					</td>		
					<c:if test="${showCustomerProfileForm.partnerSiteId == 'MGOESP'}"> 			
						<td>&nbsp;</td>
						<td nowrap="nowrap" class="td-small" align="right"><b>NIF(Tax ID Number):
						</b>
						</td>
						<td nowrap="nowrap" class="td-small">
								*****<c:out value="${showCustomerProfileForm.idNumber}" />
						</td>
					</c:if>
					
					<!-- <td>
						<!-- SSN Display MBO-6746 --> <!-- <c:if
							test="${showCustomerProfileForm.isoCountryCode == 'USA'}">
							<td class="td-small" nowrap="nowrap" align="right"><b><fmt:message
																key="label.consumer.ssn" /> </b> :</td>
							<td>
								<div class="content_toggler">
									<div class="content_toggler_content">
										<table border='0' cellpadding='0' cellspacing='0'
											id='consumer_ssn' class='miscdatatablessn ssnToggle' >
											<tbody>
												<tr>
													
													<td nowrap="nowrap" class="td-small"><c:out
															value="${showCustomerProfileForm.custSsn}" />
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit"
														class="transbtn2 content_toggler_button" value="Change" />
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<html:messages id="sssnerror" property='custSsn'>
										<div class="content_toggler_content content_toggler_default">
									</html:messages>
									<c:if test="${sssnerror==null || ssnerror ==''}">
										<div class="content_toggler_content">
									</c:if>
									<html:form action="updateCustomerSsn">
										<logic:equal name="updateSSN" value="Y">
											<c:if
												test="${showCustomerProfileForm.custSsnTaintText != 'Blocked'}">
												<table 
											id='consumer_ssn'>
													<tbody>
														<tr>
															<td nowrap="nowrap" class="td-small"><b><fmt:message
																		key="label.new.ssn" /> 
															</b>
															</td>
															<td><html:hidden name="showCustomerProfileForm"
																	property="custId" /> <html:hidden
																	name="showCustomerProfileForm"
																	property="custSsnTaintText" /> <html:hidden
																	name="showCustomerProfileForm" property="saveCustSsn" />
																<html:text property="custSsn"
																	value="${showCustomerProfileForm.custSsn}" size="10" /> <span
																class="error"> <html:messages id="message"
																		property='custSsn'>
																		<c:out value="${message}" />
																	</html:messages> </span> <span nowrap="nowrap" class="td-small"> <br> <input
																	type="submit" class="transbtn2" value="Change SSN" />
															</span></td>
														</tr>
													</tbody>
												</table>
											</c:if>
										</logic:equal>
										<logic:equal name="duplicateProfileOverride" value="Y">
											<logic:equal name="duplicateOverrideSSN" value="Y">
						Allow Duplicate:</b>
												<html:checkbox property="allowOverride" />
							</td>
							<span class="error"> <html:messages id="message"
									property="custSSNUpdateStatus">
									<c:out value="${message}" />
								</html:messages> </span>
							</logic:equal>
							</logic:equal>
							<logic:empty name="duplicateProfileOverride">
								<logic:equal name="duplicateOverrideSSN" value="Y">
									<html:hidden value="false" property="allowOverride" />
									<span class="error"> <html:messages id="message"
											property="custSSNUpdateStatus">
											<c:out value="${message}" />
										</html:messages> </span>
								</logic:equal>
							</logic:empty>

							<logic:notEqual name="updateSSN" value="Y">
								<td nowrap="nowrap" colspan="2">&nbsp;</td>
							</logic:notEqual>
							</html:form>
							</div>
							</div>
						</c:if></td> -->
				</tr>
				<!-- SSN end -->

				<!-- end -->
				 <tr>
				 	<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
										key="label.consumer.address" />: </b>
							</td>
							<td class="td-small address_cell">
								<table border='0' width='100%' cellpadding='0' cellspacing='0'
									id='consumer_address' class='miscdatatable'>
									<thead></thead>
									<tbody>
										<tr class="oddrow">
										<td class="highlight">Country</td>
											<td><c:out
													value="${showCustomerProfileForm.isoCountryCode}" />
											</td>
											<td class="highlight">Postal Code</td>
											 <td><c:out value="${showCustomerProfileForm.postalCode}" /><c:if test="${showCustomerProfileForm.zip4 != null}">
											 	<c:out value="${showCustomerProfileForm.zip4}" /></c:if></td> 		
										</tr>
										
										<tr>
											<td class="highlight">Address Line 1/Street</td>
											<td><c:out
													value="${showCustomerProfileForm.addressLine1}" />
											</td>
											<td class="highlight">City/Town</td>
											<td><c:if
													test="${showCustomerProfileForm.city != null}">
											<c:out value="${showCustomerProfileForm.city}" />
											</c:if>
											</td>
										</tr>
										<tr class="oddrow">
											<td class="highlight">Address Line 2</td>
											<td><c:if
													test="${showCustomerProfileForm.addressLine2 != null}">
													<c:out value="${showCustomerProfileForm.addressLine2}" />
												</c:if></td>
											<td class="highlight">Province/State</td>
											<td><c:if
													test="${showCustomerProfileForm.state != null}">
											<c:out value="${showCustomerProfileForm.state}" />
											</c:if>
											</td>	
											</tr>
											<tr>
											<td class="highlight"><fmt:message
								key="label.address.line3" /></td>
											<td><c:if
													test="${showCustomerProfileForm.addressLine3 != null}">
													<c:out value="${showCustomerProfileForm.addressLine3}" />
												</c:if></td>
											</tr>
										
									</tbody>
								</table> 
								<a
								href='../../viewAddressHistory.do?custId=<c:out value="${showCustomerProfileForm.custId}"/>
				&country=<c:out value="${showCustomerProfileForm.isoCountryCode}" />&partnerSiteId=<c:out value="${showCustomerProfileForm.partnerSiteId }"/>'><fmt:message
										key="link.view.address.history" />
							</a>
								</td>

				</tr>
				<tr>
					<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
								key="label.date.of.birth" />: </b>
					</td>
					<td nowrap="nowrap" class="td-small"><c:out
							value="${showCustomerProfileForm.brthDate}" /> - (<c:out
							value="${showCustomerProfileForm.custAge}" />)</td>
					<td nowrap="nowrap" class="td-small">&nbsp;</td>
				</tr>
				<!-- Changes for 5464. New field created for country of birth -->
				<tr>
					<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
								key="label.country.of.birth" />: </b>
					</td>
					<td nowrap="nowrap" class="td-small"><c:out
													value="${showCustomerProfileForm.countryOfBirth}" /></td>
				</tr>
				<!-- Changes for 5464. New field created for country of birth -->
				<tr class="rewards_row">
					<td nowrap="nowrap" class="td-small" align="right"><b>Plus
							#:&nbsp;</b></td>
					<td nowrap="nowrap" class="td-small"><c:if
							test="${showCustomerProfileForm.loyaltyPgmMembershipId == null}">
							<fmt:message key="msg.not.enrolled" />
						</c:if> <c:if
							test="${showCustomerProfileForm.loyaltyPgmMembershipId != null}">
							<c:out value="${showCustomerProfileForm.loyaltyPgmMembershipId}" /> &nbsp;
					</c:if></td>
					<td nowrap="nowrap" class="td-small">&nbsp;</td>
					<td nowrap="nowrap" class="td-small" align="right"><b>Plus
							Auto Enroll Flag: </b>
					</td>
					<td nowrap="nowrap" class="td-small">
						<%--Defect 711 Changes Starts
					 <c:out value="${showCustomerProfileForm.custAutoEnrollFlag}" /> --%>
						<c:if
							test="${showCustomerProfileForm.loyaltyPgmMembershipId == null}">
							<c:out value="N" />
						</c:if> <c:if
							test="${showCustomerProfileForm.loyaltyPgmMembershipId != null}">
							<c:out value="Y" />
						</c:if> <%--Defect 711 Changes Ends --%></td>
				</tr>
				<tr>
					<html:form type="emgadm.consumer.UpdateCustomerStatusForm"
						name="UpdateCustomerStatusForm" action="updateCustomerStatus">
						<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
									key="label.primary.phone" />: </b>
						</td>
						<!--  MBO-7310-->
						    
						<td nowrap="nowrap" class="td-small"><c:out
								value="${showCustomerProfileForm.phoneCountryCode}" />&nbsp;
						<c:out	value="${showCustomerProfileForm.phoneNumber}" />
							<c:if
								test="${showCustomerProfileForm.phoneNumberTaintText != 'Not Blocked'}">
								<font color="red"><b> &nbsp; (<c:out
											value="${showCustomerProfileForm.phoneNumberTaintText}" />)</b>
								</font>
							</c:if> 
								<c:out value="${findProfiles.phoneNumberType}" />
							 <logic:equal name="showCustomerProfileForm"
								property="phoneNumberTaintText" value="Not Blocked">
								<logic:equal name="addBlockedPhone" value="Y">
									<logic:notEmpty name="showCustomerProfileForm"
										property="phoneNumber">
						&nbsp; &nbsp; <a
											href='../../showPhoneBlockedList.do?phNbr=<c:out value="${showCustomerProfileForm.phoneNumber}" />&submitNew=add'>Block
											this Phone</a>
									</logic:notEmpty>
								</logic:equal>
							</logic:equal></td>
						<td nowrap="nowrap" class="td-small">&nbsp;</td>
						<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
									key="label.consumer.status" />: </b>
						</td>
						<td nowrap="nowrap" class="td-small"><html:hidden
								name="showCustomerProfileForm" property="custId" /> <html:select
								name="showCustomerProfileForm" property="custStatus">
								<html:optionsCollection name="showCustomerProfileForm"
									property="custStatusOptions" />
							</html:select> <input type="hidden" name="currentCustStatus"
							value="<bean:write name="showCustomerProfileForm" property="custStatus" />" />
							<logic:equal name="updateStatus" value="Y">
								<input type="submit" class="transbtn2" value="Update Status" />
								<span class="error"> <html:messages id="message"
										property="custStatus">
										<c:out value="${message}" />
									</html:messages> </span>
							</logic:equal> <logic:equal name="updateStatus" value="N">
								<logic:equal name="duplicateOverride" value="N">
									<html:hidden name="UpdateCustomerStatusForm"
										property="allowOverride" />
									<span class="error"> <html:messages id="message"
											property="custStatus">
											<c:out value="${message}" />
										</html:messages> </span>
								</logic:equal>
							</logic:equal></td>
				</tr>
				<logic:equal name="duplicateProfileOverride" value="Y">
					<logic:equal name="duplicateOverride" value="Y">
						<tr>
							<td colspan='3'></td>
							<td nowrap="nowrap" class="td-small" align="right"><b>
									Allow Duplicate:</b>
							<html:checkbox name="UpdateCustomerStatusForm"
									property="allowOverride" />
							</td>
							<td><span class="error"> <html:messages id="message"
										property="custStatus">
										<c:out value="${message}" />
									</html:messages> </span>
							</td>
						</tr>
					</logic:equal>
				</logic:equal>
				<logic:empty name="duplicateProfileOverride">
					<logic:equal name="duplicateOverride" value="Y">
						<tr>
							<td colspan='3'></td>
							<td nowrap="nowrap" class="td-small" align="right">&nbsp; <html:hidden
									name="UpdateCustomerStatusForm" value="false"
									property="allowOverride" />
							</td>
							<td><span class="error"> <html:messages id="message"
										property="custStatus">
										<c:out value="${message}" />
									</html:messages> </span>
							</td>
						</tr>
					</logic:equal>
				</logic:empty>
				<tr>
					</html:form>
					<html:form type="emgadm.consumer.UpdateCustomerEmailForm"
						name="UpdateCustomerEmailForm" action="updateCustomerEmail">
						<!-- <td nowrap="nowrap" class="td-small">&nbsp;</td> -->
						<td nowrap="nowrap" class="td-small" style="text-align: right"><b><fmt:message
									key="label.consumer.email.address" />: </b>
						</td>
						<td nowrap="nowrap" class="td-small cpEmail"><span
							class="contain"> <html:hidden
									name="showCustomerProfileForm" property="custId" /> <c:out
									value="${showCustomerProfileForm.emailAddress}" /> <c:if
									test="${showCustomerProfileForm.emailAddressTaintText != 'Not Blocked'}">
									<font color="red"> <b> &nbsp; (<c:out
												value="${showCustomerProfileForm.emailAddressTaintText}" />)
									</b> </font>
								</c:if> <c:if
									test="${showCustomerProfileForm.emailDomainTaintText != 'Not Blocked'}">
									<font color="red"> <b> &nbsp; (Domain <c:out
												value="${showCustomerProfileForm.emailDomainTaintText}" />)
									</b> </font>
								</c:if> <c:if
									test="${showCustomerProfileForm.emailAddressTaintText != 'Blocked'}">
									<c:choose>
										<c:when
											test="${showCustomerProfileForm.isEmailActive == false}">
											<logic:equal name="updateEmail" value="Y">
												<input type="submit" class="transbtn2"
													value="Activate Email" />
											</logic:equal>
										</c:when>
									</c:choose>
									<span class="error"> <html:messages id="message"
											property="emailStatus">
											<c:out value="${message}" />
										</html:messages> </span>
								</c:if> <logic:equal name="showCustomerProfileForm"
									property="emailAddressTaintText" value="Not Blocked">
									<logic:equal name="addBlockedEmail" value="Y">
										<logic:notEmpty name="showCustomerProfileForm"
											property="emailAddress">
						&nbsp; &nbsp; <a
												href='../../showEmailBlockedList.do?emailAddress=<c:out value="${showCustomerProfileForm.emailAddress}" />&emailDomain=%20&submitAdd=add'>Block
												this Email</a>
										</logic:notEmpty>
									</logic:equal>
								</logic:equal> </span> <a
							href="http://www.facebook.com/search.php?q=<c:out value="${showCustomerProfileForm.emailAddress}" />"
							class="cpFacebook email contain" target="_blank"
							title="<fmt:message key="label.search.facebook" />"> <fmt:message
									key="label.search.facebook" /> </a></td>
					</html:form>
				
					<!-- Changes for 5464 -->		
							
						 <c:if test="${showCustomerProfileForm.partnerSiteId == 'MGOZAF'}"> 
							<td nowrap="nowrap" class="td-small">&nbsp;</td>
							<html:form type="emgadm.consumer.UpdateResidencyStatusForm"
						name="UpdateResidencyStatusForm" action="updateResidencyStatus">
						<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
									key="label.resident.status" />: </b>
						</td>
						<td nowrap="nowrap" class="td-small"><html:hidden
								name="showCustomerProfileForm" property="custId" /> <html:select
								name="showCustomerProfileForm" property="residentStatus">
								<html:optionsCollection name="showCustomerProfileForm"
									property="residentStatusOptions" label="label" value="value" />
							</html:select>
								<input width="29" type="submit" class="transbtn2" value="Update" />
								<span class="error"> <html:messages
									id="message" property="resdStatus">
									<c:out value="${message}" />
								</html:messages> </span></td>
					</html:form>
				 </c:if>  
					
				</tr>
				<tr>
					<html:form type="emgadm.consumer.UpdateCustPremierCodeForm"
						name="UpdateCustPremierCodeForm" action="updateCustPremierCode">
						<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
									key="label.cust.premier.code" />: </b>
						</td>
						<td nowrap="nowrap" class="td-small"><html:hidden
								name="showCustomerProfileForm" property="custId" /> <html:select
								name="showCustomerProfileForm" property="prmrCode">
								<html:optionsCollection name="showCustomerProfileForm"
									property="custPrmrCodes" />
							</html:select> <logic:equal name="updatePrmrCode" value="Y">
								<input width="29" type="submit" class="transbtn2" value="Update" />
							</logic:equal> <fmt:formatDate value="${showCustomerProfileForm.prmrCodeDate}"
								pattern="dd/MMM/yyyy h:mm a z" /> <span class="error"> <html:messages
									id="message" property="prmrCode">
									<c:out value="${message}" />
								</html:messages> </span></td>
					</html:form>
				
					<!-- Changes for 5464 -->		
							
					<c:if test="${showCustomerProfileForm.partnerSiteId == 'MGOZAF'}"> 
							<td nowrap="nowrap" class="td-small">&nbsp;</td>
							<html:form type="emgadm.consumer.UpdateSendLimitForm"
						name="UpdateSendLimitForm" action="updateSendLimit">
						<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
											key="label.allowed.send.amount" />: </b>
							</td>
							<td nowrap="nowrap" class="td-small"><html:hidden
								name="showCustomerProfileForm" property="custId" /> <html:select
								name="showCustomerProfileForm" property="sendLimit">
								<html:optionsCollection name="showCustomerProfileForm"
									property="sendLimitOptions" label="label" value="value" />
							</html:select>
						 <input width="29" type="submit" class="transbtn2" value="Update" /> 
						 <span class="error"> <html:messages
									id="message" property="sndLimit">
									<c:out value="${message}" />
								</html:messages> </span>
							</td>
							</html:form>
				 </c:if>

					<!-- <td nowrap="nowrap" class="td-small">&nbsp;</td> -->
					</tr>
					<!-- Changes for 5464 -->	
					<c:if test="${showCustomerProfileForm.partnerSiteId == 'MGOZAF'}">
			 <tr>
						
							<td nowrap="nowrap" class="td-small" align="right"></td>
							<td nowrap="nowrap" class="td-small"></td>
							<td nowrap="nowrap" class="td-small">&nbsp;</td>
							<html:form type="emgadm.consumer.UpdateReceiveLimitForm"
						name="UpdateReceiveLimitForm" action="updateReceiveLimit">
							<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
											key="label.receives.allowed" />:</b>
							</td>
							<td nowrap="nowrap" class="td-small"><html:hidden
								name="showCustomerProfileForm" property="custId" />
								<html:checkbox name="showCustomerProfileForm"
									property="receiveLimit" />
								<input width="29" type="submit" class="transbtn2" value="Update" /> 
								<span class="error"> <html:messages
									id="message" property="rcvLimit">
									<c:out value="${message}" />
								</html:messages> </span>
							</td>
							</html:form>
					</tr> 
					</c:if>
				<c:choose>
				<c:when test="${showCustomerProfileForm.partnerSiteId == 'MGO'}">
				<tr></tr>
				</c:when> 
				<c:when test="${showCustomerProfileForm.siteType =='STAGING'}"> 
						<tr>
							<td nowrap="nowrap" class="td-small">&nbsp;</td>
							<td nowrap="nowrap" class="td-small">&nbsp;</td>
							<td nowrap="nowrap" class="td-small">&nbsp;</td>
							<td nowrap="nowrap" class="td-small" align="left"><a
								href='../../viewDocumentDetails.do?custId=<c:out value="${showCustomerProfileForm.custId}"/>&action=ViewAllImage&partnerSiteId=<c:out value="${showCustomerProfileForm.partnerSiteId }"/>'
								target="_blank">View Documents</a></tr>

					</c:when>
				<c:otherwise>
				<c:if
					test="${showCustomerProfileForm.isIdImageAvailable == true}">
					<tr>
						<td nowrap="nowrap" class="td-small">&nbsp;</td>
						<td nowrap="nowrap" class="td-small">&nbsp;</td>
						<td nowrap="nowrap" class="td-small">&nbsp;</td>
						<td nowrap="nowrap" class="td-small" align="left"><a
							href='../../showCustomerProfile.do?custId=<c:out value="${showCustomerProfileForm.custId}"/>&basicFlag=N&action=ViewIdImage'
							onclick="openWindow('<c:out value="${pageContext.request.contextPath}" />/showCustomerProfile.do?custId=<c:out value="${showCustomerProfileForm.custId}"/>&basicFlag=N&action=ViewIdImage');return false;">
								View Scanned Document </a>
					</tr>
				</c:if>
				</c:otherwise>
				</c:choose>
				<logic:equal name="addCustCmt" value="Y">
					<html:form type="emgadm.consumer.AddCustomerCommentForm"
						name="AddCustomerCommentForm" action="addCustomerComment">
						<html:hidden name="showCustomerProfileForm" property="custId" />
						<tr>
							<td nowrap="nowrap" colspan='5' class='TD-HEADER-SMALL'>Customer
								Profile Comments <a
								href="../../addCustomerComment.do?custId=<c:out value="${showCustomerProfileForm.custId}" />&commentReasonCode=IP&commentText=<c:out value="${showCustomerProfileForm.createdIPAddress}" />">Add
									IP Lookup comment</a>
							</td>
						</tr>
						<tr>
							<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message
										key="label.comment.reason" />: </b>
							</td>
							<td colspan="4" nowrap="nowrap" class="td-small" valign="top">
								<html:select property="commentReasonCode" value="">
									<html:option value="">Select One </html:option>
									<html:option value=" ">------------------- </html:option>
									<html:optionsCollection name="showCustomerProfileForm"
										property="customerCommentReasons" />
								</html:select> <span class="error"> <html:messages id="message"
										property="commentReason">
										<c:out value="${message}" />
									</html:messages> </span>
							</td>
						</tr>

						<tr>
							<td nowrap="nowrap" class="td-small" align="right" valign="top"><b><fmt:message
										key="label.consumer.comment" />: </b>
							</td>
							<td colspan="3" nowrap="nowrap" class="td-small"
								style='width: 775px\9  !important;' valign="top"><html:textarea
									property="commentText" rows="4" cols="80"
									onkeypress="textCounter(this, 255);" /> <input type="submit"
								class="transbtn2" value="Add Comment" /></td>


							<c:if
								test="${showCustomerProfileForm.partnerSiteId == 'WAP' || showCustomerProfileForm.partnerSiteId == 'MGO'}">
								<td colspan="3" nowrap="nowrap" class="td-small" id='button_lex'
									valign="baseline">
									<div style="margin-bottom: 5px;">
										<html:form type="emgadm.consumer.GetPersonSearchForm"
											name="getPersonSearchForm" action="getPersonSearch">
											<logic:equal name="getPersonSearch" value="Y">
												<input type="button" class="transbtn2" id="personSrchBtn"
													value="BPS Search"
													onclick="callPersonSearch('<c:out value="${showCustomerProfileForm.custId}"/>');" />
											</logic:equal>
										</html:form>
									</div>
									<div style="margin-bottom: 5px;">
										<logic:equal name="getRelativeSearch" value="Y">
											<input type="button" class="transbtn2" id="relativeSrchBtn"
												value="Relative Search"
												onclick="callRelativeSearch('<c:out value="${showCustomerProfileForm.custId}"/>');" />
										</logic:equal>

									</div>
									<div>
										<html:form type="emgadm.consumer.GetKBAQuestionsForm"
											name="getKBAQuestionsForm" action="getKBA">
											<logic:equal name="getKBA" value="Y">
												<input type="button" class="transbtn3" value="KBA"
													onclick="parent.location='/eMoneyGramAdm/getKBA.do?custId=<c:out value="${showCustomerProfileForm.custId}"/>'" />
											</logic:equal>
										</html:form>
									</div></td>
							</c:if>



						</tr>
					</html:form>
				</logic:equal>
			</tbody>
		</table>

		<%
			String dispClass = "td-small";
			String dispClass2 = "TD-SHADED3-SMALL";
		%>

		<logic:notEqual name="noComments" value="Y">
			<table width="100%">
				<thead>
					<tr>
						<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message
								key="label.create.date" />
						</td>
						<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message
								key="label.comment" />
						</td>
						<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message
								key="label.create.user" />
						</td>
						<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message
								key="label.comment.reason" />
						</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="comment"
						items="${showCustomerProfileForm.comments}">
						<tr>
							<td nowrap="nowrap" class="<%=dispClass%>"><fmt:formatDate
									value="${comment.createDate}" pattern="dd/MMM/yyyy h:mm a z" />
							</td>
							<td class="<%=dispClass%>"><c:out value="${comment.text}" />
							</td>
							<td nowrap="nowrap" class="<%=dispClass%>"><c:out
									value="${comment.createdBy}" />
							</td>
							<td nowrap="nowrap" class="<%=dispClass%>"><c:out
									value="${comment.reasonDescription}" />
							</td>
						</tr>
						<%
							if (dispClass.equals("td-shaded-small")) {
										dispClass = "td-small";
									} else {
										dispClass = "td-shaded-small";
									}
						%>

					</c:forEach>
				</tbody>
			</table>
				
		</logic:notEqual>
	

		<logic:notEqual name="noActivities" value="Y">
		<hr />
			<table width="100%">
				<thead>
					<tr>
						<td nowrap="nowrap" colspan='4' class='TD-HEADER-SMALL'>Customer
							Profile Activity Log</td>
					</tr>
					<tr>
						<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message
								key="label.activity.date" />
						</td>
						<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message
								key="label.activity.description" />
						</td>
						<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message
								key="label.activity.category" />
						</td>
						<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message
								key="label.create.user" />
						</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="acty"
						items="${showCustomerProfileForm.consumerProfileActivity}">
						<tr>
							<td nowrap="nowrap" class="<%=dispClass%>"><fmt:formatDate
									value="${acty.activityDate}" pattern="dd/MMM/yyyy h:mm a z" />
							</td>
							<td class="<%=dispClass%>"><c:out
									value="${acty.activityBusinessDescription}" />
							</td>
							<td nowrap="nowrap" class="<%=dispClass%>"><c:out
									value="${acty.activityBusinessCategory}" />
							</td>
							<td nowrap="nowrap" class="<%=dispClass%>"><c:out
									value="${acty.createdBy}" />
							</td>
						</tr>
						<%
							if (dispClass.equals("td-shaded-small")) {
										dispClass = "td-small";
									} else {
										dispClass = "td-shaded-small";
									}
						%>

					</c:forEach>
				</tbody>
			</table>
			<hr />
		</logic:notEqual>
		
		<input type="hidden" id="isBPSEnable"
			value="${showCustomerProfileForm.isBPSEnable}" /> <input
			type="hidden" id="isRLSEnable"
			value="${showCustomerProfileForm.isRLSEnable}" />


		<c:if
			test="${showCustomerProfileForm.partnerSiteId == 'WAP' || showCustomerProfileForm.partnerSiteId == 'MGO'}">
			<table id="lexNexActivity" width="100%">
				<thead>
					<tr>
						<td nowrap="nowrap" colspan='6' class='TD-HEADER-SMALL'><fmt:message
								key="label.lexisNexis.activityLog" /></td>
					</tr>
					<tr>
						<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message
								key="label.lexisNexis.activity.date" />
						</td>
						<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message
								key="label.lexisNexis.response" />
						</td>
						<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message
								key="label.lexisNexis.highRiskIndicator" />
						</td>
						<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message
								key="label.lexisNexis.ssnValidityIndicator" />
						</td>
						<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message
								key="label.lexisNexis.ssnIndicator" />
						</td>
						<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message
								key="label.lexisNexis.comments" /></td>
					</tr>
				</thead>

				<tbody>

					<c:forEach var="lnacty"
						items="${showCustomerProfileForm.lexNexActivtyArr}">
						<tr>
							<td id="activityDate" nowrap="nowrap" class="<%=dispClass%>"><fmt:formatDate
									value="${lnacty.activityDate}" pattern="dd/MMM/yyyy h:mm a z" />
							</td>

							<td id="searchLink" class="<%=dispClass%>"><c:if
									test="${2 != lnacty.searchType}">
									<c:if
										test="${!empty lnacty.uniqueId && !empty lnacty.activityDate}">
										<a href="javascript:showPersonSearch('<c:out value="${lnacty.id}"/>');"><fmt:message
												key="label.lexisNexis.bpsSearchlink" /> </a>
									</c:if>
									<c:if
										test="${empty lnacty.uniqueId && !empty lnacty.activityDate}">
										<fmt:message key="label.lexisNexis.personNotFnd" />
									</c:if>
								</c:if> <c:if test="${2 == lnacty.searchType}">
									<c:if
										test="${!empty lnacty.uniqueId && !empty lnacty.activityDate}">
										<a href="javascript:showRelativeSearch('<c:out value="${lnacty.id}"/>');" ><fmt:message
												key="label.lexisNexis.rlsSearchlink" /> </a>
									</c:if>
									<c:if
										test="${empty lnacty.uniqueId && !empty lnacty.activityDate}">
										<fmt:message key="label.lexisNexis.relativeNotFnd" />
									</c:if>
								</c:if></td>

							<td id="indicator" nowrap="nowrap" class="<%=dispClass%>"><c:forEach
									var="indicator" items="${lnacty.highRiskIndicator}">
									<c:out value="${indicator.code} , ${indicator.description}" />
									<br />
								</c:forEach></td>
							<td id="ssnValidityInd" nowrap="nowrap" class="<%=dispClass%>">
								<c:if test="${!empty lnacty.ssnValidDisp}">
									<c:out value="${lnacty.ssnValidDisp}" />
								</c:if>
							</td>
							<td id="ssnIndicator" nowrap="nowrap" class="<%=dispClass%>">

								<c:out value="${lnacty.ssnDisp}" /></td>


							<td id="comments" nowrap="nowrap" class="<%=dispClass%>"><c:if
									test="${2 != lnacty.searchType}">
									<c:if
										test="${!empty lnacty.uniqueId && !empty lnacty.activityDate}">
										<fmt:message key="label.lexisNexis.comments.notNullCase" />
									</c:if>
									<c:if
										test="${empty lnacty.uniqueId && !empty lnacty.activityDate}">
										<fmt:message key="label.lexisNexis.comments.nullCase" />
									</c:if>
								</c:if> <c:if test="${2 == lnacty.searchType}">
									<c:if
										test="${!empty lnacty.uniqueId && !empty lnacty.activityDate}">
										<fmt:message key="label.lexisNexis.comments.notNullRLSCase" />
									</c:if>
									<c:if
										test="${empty lnacty.uniqueId && !empty lnacty.activityDate}">
										<fmt:message key="label.lexisNexis.comments.nullRLSCase" />
									</c:if>
								</c:if></td>
						</tr>
						<%
							if (dispClass.equals("td-shaded-small")) {
										dispClass = "td-small";
									} else {
										dispClass = "td-shaded-small";
									}
						%>
					</c:forEach>
			</table>
		</c:if>

	</fieldset>

	<img height="20" src="../../images/trans.gif" width="10">

	<c:if test="${not empty gbGroupLog}">
		<fieldset class="fieldset1">
			<legend class="legend">
				<fmt:message key="label.gbgroup" />
				:
			</legend>
			<table width="100%">
			<%-- 	<c:if test="${not empty gbGroupLog}"> --%>
					<thead>

						<tr>
							<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message
									key="label.activity.date" />
							</td>
							<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message
									key="label.activity.desc" />
							</td>

						</tr>
					</thead>
					<tbody>
						<c:forEach var="acty" items="${gbGroupLog}">
							<tr>
								<td nowrap="nowrap" class="<%=dispClass%>">${acty.authDateDisplay}
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>">Auth Id =<c:out
										value="${acty.authnId}" />, Score = <c:out
										value="${acty.scoreNbr}" />, Decision =<c:out
										value="${acty.deciBandText}" /></td>
							</tr>
							<%
								if (dispClass.equals("td-shaded-small")) {
												dispClass = "td-small";
											} else {
												dispClass = "td-shaded-small";
											}
							%>

						</c:forEach>
					</tbody>
				<%-- </c:if> --%>
				<%-- <c:if test="${empty gbGroupLog}">
					<td class="td-small"><fmt:message key="label.gbgroup.noinfo"></fmt:message>
					</td>
				</c:if> --%>
				<%-- <tr>
				<c:if test="${not empty gbGroupLog}">
				<td nowrap="nowrap" class="td-small">
					<!-- <a name="gbGroupDetailLink"
					href='../../showCustomerProfile.do'> -->
						<c:out value="${showCustomerProfileForm.viewGBGroupLogLinkString}" />
				</a></td>
				</c:if>

				</tr>--%>
			</table>
		</fieldset>
		<br />
	</c:if>


	<!-- --------------------- ************************ ---------------------- START of MITEK **************************** ------------------------->

	<c:if test="${showCustomerProfileForm.partnerSiteId != 'MGOZAF'}">
		<table width="100%" class="td-small">
			<tbody>
			<tr>
			<td nowrap="nowrap" class="td-small">
				<%int i=0;  %>
				<c:forEach var="consumerIdUpload" items="${aListJsp}">
					
						<fieldset class="fieldset1">
							<legend class="legend">
								<%i=i+1;%>
								<%if(i==1) {%>
								<fmt:message key="label.mitek" />
								:
								<%}%>
							</legend>
							<b>ID Type: </b>
							<c:out value="${consumerIdUpload.idType}" />
							&nbsp; &nbsp;<b> ID Number: </b>
							<c:out value="${consumerIdUpload.idNumber}" />
							&nbsp; &nbsp;<b> Country of Issue: </b>
							<c:out value="${consumerIdUpload.countryOfIssue}" />
							&nbsp; &nbsp;<b> State of Issue: </b>
							<c:out value="${consumerIdUpload.stateOfIssue}" />
							&nbsp; &nbsp;<b> ID Status: </b>
							<c:out value="${consumerIdUpload.idStatus}" />
							<b> Upload Reason Code: </b>
							<c:out value="${consumerIdUpload.uploadReasonCode}" />
							&nbsp; &nbsp;<b> ID Last Update Date: </b>
							<fmt:formatDate value="${consumerIdUpload.idLastUpdateDate}"
								pattern="dd/MMM/yyyy h:mm a z" />
							&nbsp; &nbsp;<b> ID Upload Date: </b>
							<fmt:formatDate value="${consumerIdUpload.idUploadDate}"
								pattern="dd/MMM/yyyy h:mm a z" />
							&nbsp; &nbsp;<b> Captify Reason Code: </b>
							<c:out value="${consumerIdUpload.captifyReasonCode}" />
							<br /> <b> Mitek Reason Codes: </b>
							<c:out value="${consumerIdUpload.mitekReasoncodes}" />
							&nbsp; &nbsp;<b> Session ID: </b>
							<c:out value="${consumerIdUpload.sessionID}" />		
							
							&nbsp; &nbsp;<c:if test="${consumerIdUpload.manualReviewRefId != null}">
							<b> Manual Verify Ref ID: </b>
							<c:out value="${consumerIdUpload.manualReviewRefId}" /> 
							</c:if>					
							
							<br /> <b> Truth Data Scores: FName - </b>
							<c:out value="${consumerIdUpload.fName}" />
							&nbsp; &nbsp;<b> LName - </b>
							<c:out value="${consumerIdUpload.lName}" />
							&nbsp; &nbsp;<b> DOB - </b>
							<c:out value="${consumerIdUpload.dob}" />
							&nbsp; &nbsp;<b> Address - </b>
							<c:out value="${consumerIdUpload.address}" />
						</fieldset>
					
				</c:forEach>
				</td>
				</tr>
			</tbody>
		</table>
	</c:if>
	<br>

	<!-- --------------------- ************************ ---------------------- END of MITEK **************************** ------------------------->
	<logic:equal name="showCustomerProfileForm" property="basicFlag"
		value="N">


		<fieldset class="fieldset1">
			<legend class="legend">
				<fmt:message key="label.bank.accounts" />
				:
			</legend>
			<c:out value="${showCustomerProfileForm.custLogonId}" />
			&nbsp; &nbsp; (Id: <b><c:out value="${custIdMarket}" />
			</b>)

			<logic:equal name="noBankAccts" value="Y">
	&nbsp; &nbsp; <font color="red"><b>(None)</b>
				</font>
			</logic:equal>

			<logic:notEqual name="noBankAccts" value="Y">

				<%
					dispClass = "td-small";
				%>

				<table border='0' width='100%' cellpadding='1' cellspacing='1'>
					<thead>
						<tr>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.account.number" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.routing.number" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.bank" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.account.type" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.status" />
							</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="bankAccount"
							items="${showCustomerProfileForm.bankAccounts}">
							<tr>
								<html:form type="emgadm.account.UpdateAccountStatusForm"
									name="UpdateAccountStatusForm" action="updateAccountStatus">
									<td nowrap="nowrap" class="<%=dispClass%>"><a
										href='../../viewBankAccountDetail.do?accountId=<c:out value="${bankAccount.accountId}"/>&custId=<c:out value="${showCustomerProfileForm.custId}"/>'>
											<c:out value="*****${bankAccount.accountNumberMask}" /> </a>
										&nbsp; <c:if
											test="${bankAccount.accountTaintText != 'Not Blocked'}">
											<font color="red"><b> &nbsp; (ACCT: <c:out
														value="${bankAccount.accountTaintText}" />)</b>
											</font>
										</c:if></td>
									<td nowrap="nowrap" class="<%=dispClass%>"><c:out
											value="${bankAccount.routingNumber}" /> <c:if
											test="${bankAccount.bankAbaTaintText != 'Not Blocked'}">
											<font color="red"><b> &nbsp; (ABA: <c:out
														value="${bankAccount.bankAbaTaintText}" />)</b>
											</font>
										</c:if></td>
									<td nowrap="nowrap" class="<%=dispClass%>"><c:out
											value="${bankAccount.financialInstitutionName}" />
									</td>
									<td nowrap="nowrap" class="<%=dispClass%>"><c:out
											value="${bankAccount.accountTypeDesc}" />
									</td>
									<td nowrap="nowrap" class="<%=dispClass%>"><html:hidden
											name="showCustomerProfileForm" property="custId" /> <html:hidden
											name="bankAccount" property="accountId" /> <html:hidden
											name="bankAccount" property="accountTypeCode" /> <html:select
											name="bankAccount" property="combinedStatusCode">
											<html:optionsCollection name="showCustomerProfileForm"
												property="accountStatusOptions" />
										</html:select> <logic:equal name="updateAcct" value="Y">
											<input type="submit" class="transbtn2" value="Update Status" />
										</logic:equal></td>
									<logic:present name="bankAccount" property="microDeposit">
										<td nowrap="nowrap" class="<%=dispClass%>"><b><fmt:message
													key="label.md.status" />: </b> <c:out
												value="${bankAccount.microDeposit.status.mdStatus}" /> (<c:out
												value="${bankAccount.microDeposit.status.mdStatusDesc}" />)
											<logic:equal name="bankAccount" property="statusCode"
												value="ACT">
												<logic:equal name="validateMD" value="Y">
													<c:if
														test="${bankAccount.microDeposit.status.mdStatus == 'ACS'}">
							&nbsp; <a
															href='../../validateMicroDeposit.do?accountId=<c:out value="${bankAccount.accountId}"/>'>Validate
															Micro Deposits</a>
													</c:if>
												</logic:equal>
											</logic:equal></td>
									</logic:present>
									<logic:notPresent name="bankAccount" property="microDeposit">
										<td nowrap="nowrap" class="<%=dispClass%>">&nbsp;</td>
									</logic:notPresent>
								</html:form>
							</tr>

							<logic:notEmpty name="bankAccount" property="cmntList">
								<bean:define id="cmnts" name="bankAccount" property="cmntList" />
								<tr>
									<td colspan="5">
										<table border='0' cellpadding='1' cellspacing='1'>
											<%
												dispClass2 = "TD-SHADED3-SMALL";
											%>
											<logic:iterate id="cmnt" name="cmnts">
												<%
													if (dispClass2.equals("TD-SHADED2-SMALL")) {
																			dispClass2 = "TD-SHADED3-SMALL";
																		} else {
																			dispClass2 = "TD-SHADED2-SMALL";
																		}
												%>


												<tr>
													<td nowrap="nowrap" class="<%=dispClass2%>" valign="top">
														<font color="#008b8b"><b>&nbsp; &nbsp; &nbsp;
																&nbsp; <fmt:formatDate value="${cmnt.createDate}"
																	pattern="dd/MMM/yyyy hh:mm:ss a z" /> - <bean:write
																	name="cmnt" property="createdBy" /> - <bean:write
																	name="cmnt" property="reasonDescription" /> </b>
													</font>
													</td>
													<td class="<%=dispClass2%>"><font color="#2f4f4f"><b><bean:write
																	name="cmnt" property="text" />
														</b>
													</font>
													</td>
												</tr>
											</logic:iterate>
										</table></td>
								</tr>
							</logic:notEmpty>

							<%
								if (dispClass.equals("td-shaded-small")) {
												dispClass = "td-small";
											} else {
												dispClass = "td-shaded-small";
											}
							%>

						</c:forEach>
					</tbody>
				</table>
			</logic:notEqual>
		</fieldset>

		<img height="20" src="../../images/trans.gif" width="10">

		<fieldset class="fieldset1">
			<legend class="legend">
				<fmt:message key="label.credit.accounts" />
				:
			</legend>
			<c:out value="${showCustomerProfileForm.custLogonId}" />
			&nbsp; &nbsp; (Id: <b><c:out value="${custIdMarket}" />
			</b>)

			<logic:equal name="noCreditAccts" value="Y">
	&nbsp; &nbsp; <font color="red"><b>(None)</b>
				</font>
			</logic:equal>

			<logic:notEqual name="noCreditAccts" value="Y">

				<%
					dispClass = "td-small";
				%>

				<table border='0' width='100%' cellpadding='1' cellspacing='1'>
					<thead>
						<tr>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.credit.card.number" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.credit.card.expiration.date" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.credit.card.issuer" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.credit.card.type" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.status" />
							</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="creditCardAccount"
							items="${showCustomerProfileForm.creditCardAccounts}"
							varStatus="status">
							<tr>
								<html:form type="emgadm.account.UpdateAccountStatusForm"
									name="UpdateAccountStatusForm" action="updateAccountStatus">
									<td nowrap="nowrap" class="<%=dispClass%>"><a
										href='../../viewCreditCardAccountDetail.do?accountId=<c:out value="${creditCardAccount.accountId}"/>&custId=<c:out value="${showCustomerProfileForm.custId}"/>'>
											<c:out value="${creditCardAccount.accountNumberMask}" /> </a> <c:if
											test="${creditCardAccount.accountTaintText != 'Not Blocked'}">
											<font color="red"><b> &nbsp; (ACCT: <c:out
														value="${creditCardAccount.accountTaintText}" />)</b>
											</font>
										</c:if> <c:if
											test="${creditCardAccount.creditCardBinTaintText != 'Not Blocked'}">
											<font color="red"><b> &nbsp; (BIN: <c:out
														value="${creditCardAccount.creditCardBinTaintText}" />)</b>
											</font>
										</c:if></td>
									<td nowrap="nowrap" class="<%=dispClass%>"><c:out
											value="${creditCardAccount.expirationMonth}/${creditCardAccount.expirationYear}" />
									</td>
									<td nowrap="nowrap" class="<%=dispClass%>"><c:out
											value="${creditCardAccount.issuer}" /></td>
									<td nowrap="nowrap" class="<%=dispClass%>"><c:out
											value="${creditCardAccount.creditOrDebitCode}" /></td>
									<td nowrap="nowrap" class="<%=dispClass%>"><html:hidden
											name="showCustomerProfileForm" property="custId" /> <html:hidden
											name="creditCardAccount" property="accountId" /> <html:hidden
											name="creditCardAccount" property="accountTypeCode" /> <html:select
											name="creditCardAccount" property="combinedStatusCode">
											<html:optionsCollection name="showCustomerProfileForm"
												property="accountStatusOptions" />
										</html:select> <logic:equal name="updateAcct" value="Y">
											<input type="submit" class="transbtn2" value="Update Status" />
										</logic:equal></td>
								</html:form>
							</tr>

							<logic:notEmpty name="creditCardAccount" property="cmntList">
								<bean:define id="cmnts" name="creditCardAccount"
									property="cmntList" />
								<tr>
									<td colspan="5">
										<table border='0' cellpadding='1' cellspacing='1'>
											<%
												dispClass2 = "TD-SHADED3-SMALL";
											%>
											<logic:iterate id="cmnt" name="cmnts">
												<%
													if (dispClass2.equals("TD-SHADED2-SMALL")) {
																			dispClass2 = "TD-SHADED3-SMALL";
																		} else {
																			dispClass2 = "TD-SHADED2-SMALL";
																		}
												%>

												<tr>
													<td width='10'>&nbsp;</td>
													<td nowrap="nowrap" class="<%=dispClass2%>" valign="top">
														<font color="#008b8b"><b>&nbsp; &nbsp; &nbsp;
																&nbsp; <fmt:formatDate value="${cmnt.createDate}"
																	pattern="dd/MMM/yyyy hh:mm:ss a z" /> - <bean:write
																	name="cmnt" property="createdBy" /> - <bean:write
																	name="cmnt" property="reasonDescription" /> </b>
													</font>
													</td>
													<td class="<%=dispClass2%>">
														<div class="commentDivNew">
															<b class="wrappedTxt"><bean:write name="cmnt"
																	property="text" />
															</b>
														</div>
													</td>
												</tr>
											</logic:iterate>
										</table></td>
								</tr>
							</logic:notEmpty>

							<%
								if (dispClass.equals("td-shaded-small")) {
												dispClass = "td-small";
											} else {
												dispClass = "td-shaded-small";
											}
							%>

						</c:forEach>
					</tbody>
				</table>
			</logic:notEqual>
		</fieldset>

		<img height="20" src="../../images/trans.gif" width="10">

		<fieldset class="fieldset1">
			<legend class="legend">
				<fmt:message key="label.debit.accounts" />
				:
			</legend>
			<c:out value="${showCustomerProfileForm.custLogonId}" />
			&nbsp; &nbsp; (Id: <b><c:out value="${custIdMarket}" />
			</b>)

			<logic:equal name="noDebitAccts" value="Y">
	&nbsp; &nbsp; <font color="red"><b>(None)</b>
				</font>
			</logic:equal>

			<logic:notEqual name="noDebitAccts" value="Y">

				<%
					dispClass = "td-small";
				%>

				<table border='0' width='100%' cellpadding='1' cellspacing='1'>
					<thead>
						<tr>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.debit.card.number" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.credit.card.expiration.date" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.credit.card.issuer" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.credit.card.type" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.status" />
							</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="debitCardAccount"
							items="${showCustomerProfileForm.debitCardAccounts}"
							varStatus="status">
							<tr>
								<html:form type="emgadm.account.UpdateAccountStatusForm"
									name="UpdateAccountStatusForm" action="updateAccountStatus">
									<td nowrap="nowrap" class="<%=dispClass%>"><a
										href='../../viewCreditCardAccountDetail.do?accountId=<c:out value="${debitCardAccount.accountId}"/>&custId=<c:out value="${showCustomerProfileForm.custId}"/>'>
											<c:out value="*****${debitCardAccount.accountNumberMask}" />
									</a> <c:if
											test="${debitCardAccount.accountTaintText != 'Not Blocked'}">
											<font color="red"><b> &nbsp; (ACCT: <c:out
														value="${debitCardAccount.accountTaintText}" />)</b>
											</font>
										</c:if> <c:if
											test="${debitCardAccount.creditCardBinTaintText != 'Not Blocked'}">
											<font color="red"><b> &nbsp; (BIN: <c:out
														value="${debitCardAccount.creditCardBinTaintText}" />)</b>
											</font>
										</c:if></td>
									<td nowrap="nowrap" class="<%=dispClass%>"><c:out
											value="${debitCardAccount.expirationMonth}/${debitCardAccount.expirationYear}" />
									</td>
									<td nowrap="nowrap" class="<%=dispClass%>"><c:out
											value="${debitCardAccount.issuer}" /></td>
									<td nowrap="nowrap" class="<%=dispClass%>"><c:out
											value="${debitCardAccount.creditOrDebitCode}" /></td>
									<td nowrap="nowrap" class="<%=dispClass%>"><html:hidden
											name="showCustomerProfileForm" property="custId" /> <html:hidden
											name="debitCardAccount" property="accountId" /> <html:hidden
											name="debitCardAccount" property="accountTypeCode" /> <html:select
											name="debitCardAccount" property="combinedStatusCode">
											<html:optionsCollection name="showCustomerProfileForm"
												property="accountStatusOptions" />
										</html:select> <logic:equal name="updateAcct" value="Y">
											<input type="submit" class="transbtn2" value="Update Status" />
										</logic:equal></td>
								</html:form>
							</tr>

							<logic:notEmpty name="debitCardAccount" property="cmntList">
								<bean:define id="cmnts" name="debitCardAccount"
									property="cmntList" />
								<tr>
									<td colspan="5">
										<table border='0' cellpadding='1' cellspacing='1'>
											<%
												dispClass2 = "TD-SHADED3-SMALL";
											%>
											<logic:iterate id="cmnt" name="cmnts">
												<%
													if (dispClass2.equals("TD-SHADED2-SMALL")) {
																			dispClass2 = "TD-SHADED3-SMALL";
																		} else {
																			dispClass2 = "TD-SHADED2-SMALL";
																		}
												%>

												<tr>
													<td nowrap="nowrap" class="<%=dispClass2%>" valign="top">
														<font color="#008b8b"><b>&nbsp; &nbsp; &nbsp;
																&nbsp; <fmt:formatDate value="${cmnt.createDate}"
																	pattern="dd/MMM/yyyy hh:mm:ss a z" /> - <bean:write
																	name="cmnt" property="createdBy" /> - <bean:write
																	name="cmnt" property="reasonDescription" /> </b>
													</font>
													</td>
													<td class="<%=dispClass2%>">
														<div class="commentDivNew">
															<b class="wrappedTxt"><bean:write name="cmnt"
																	property="text" />
															</b>
														</div>
													</td>
												</tr>
											</logic:iterate>
										</table></td>
								</tr>
							</logic:notEmpty>

							<%
								if (dispClass.equals("td-shaded-small")) {
												dispClass = "td-small";
											} else {
												dispClass = "td-shaded-small";
											}
							%>

						</c:forEach>
					</tbody>
				</table>
			</logic:notEqual>
		</fieldset>
		<c:choose>
			<c:when test="${findProfiles.partnerSiteId == 'MGODE'}">
				<br />
				<fieldset class="fieldset1">
					<legend class="legend">
						<fmt:message key="label.maestro.accounts" />
						:
					</legend>
					<c:out value="${showCustomerProfileForm.custLogonId}" />
					&nbsp; &nbsp; (Id: <b><c:out value="${custIdMarket}" />
					</b>)

					<logic:equal name="noMaestroAccts" value="Y">
	&nbsp; &nbsp; <font color="red"><b>(None)</b>
						</font>
					</logic:equal>

					<logic:notEqual name="noMaestroAccts" value="Y">

						<%
							dispClass = "td-small";
						%>

						<table border='0' width='100%' cellpadding='1' cellspacing='1'>
							<thead>
								<tr>
									<td class='TD-HEADER-SMALL'><fmt:message
											key="label.credit.card.number" />
									</td>
									<td class='TD-HEADER-SMALL'><fmt:message
											key="label.credit.card.expiration.date" />
									</td>
									<td class='TD-HEADER-SMALL'><fmt:message
											key="label.credit.card.type" />
									</td>
									<td class='TD-HEADER-SMALL'><fmt:message
											key="label.status" />
									</td>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="maestroAccount"
									items="${showCustomerProfileForm.maestroAccounts}"
									varStatus="status">
									<tr>
										<html:form type="emgadm.account.UpdateAccountStatusForm"
											name="UpdateAccountStatusForm" action="updateAccountStatus">
											<td nowrap="nowrap" class="<%=dispClass%>"><a
												href='../../viewCreditCardAccountDetail.do?accountId=<c:out value="${maestroAccount.accountId}"/>&custId=<c:out value="${showCustomerProfileForm.custId}"/>'>
													<c:out value="${maestroAccount.accountNumberMask}" /> </a> <c:if
													test="${maestroAccount.accountTaintText != 'Not Blocked'}">
													<font color="red"><b> &nbsp; (ACCT: <c:out
																value="${maestroAccount.accountTaintText}" />)</b>
													</font>
												</c:if> <c:if
													test="${maestroAccount.creditCardBinTaintText != 'Not Blocked'}">
													<font color="red"><b> &nbsp; (BIN: <c:out
																value="${maestroAccount.creditCardBinTaintText}" />)</b>
													</font>
												</c:if></td>
											<td nowrap="nowrap" class="<%=dispClass%>"><c:out
													value="${maestroAccount.expirationMonth}/${maestroAccount.expirationYear}" />
											</td>
											<td nowrap="nowrap" class="<%=dispClass%>"><html:hidden
													name="showCustomerProfileForm" property="custId" /> <html:hidden
													name="maestroAccount" property="accountId" /> <html:hidden
													name="maestroAccount" property="accountTypeCode" /> <html:select
													name="maestroAccount" property="combinedStatusCode">
													<html:optionsCollection name="showCustomerProfileForm"
														property="accountStatusOptions" />
												</html:select> <logic:equal name="updateAcct" value="Y">
													<input type="submit" class="transbtn2"
														value="Update Status" />
												</logic:equal></td>
										</html:form>
									</tr>

									<logic:notEmpty name="maestroAccount" property="cmntList">
										<bean:define id="cmnts" name="maestroAccount"
											property="cmntList" />
										<tr>
											<td colspan="5">
												<table border='0' cellpadding='1' cellspacing='1'>
													<%
														dispClass2 = "TD-SHADED3-SMALL";
													%>
													<logic:iterate id="cmnt" name="cmnts">
														<%
															if (dispClass2
																								.equals("TD-SHADED2-SMALL")) {
																							dispClass2 = "TD-SHADED3-SMALL";
																						} else {
																							dispClass2 = "TD-SHADED2-SMALL";
																						}
														%>

														<tr>
															<td width='10'>&nbsp;</td>
															<td nowrap="nowrap" class="<%=dispClass2%>" valign="top">
																<font color="#008b8b"><b>&nbsp; &nbsp; &nbsp;
																		&nbsp; <fmt:formatDate value="${cmnt.createDate}"
																			pattern="dd/MMM/yyyy hh:mm:ss a z" /> - <bean:write
																			name="cmnt" property="createdBy" /> - <bean:write
																			name="cmnt" property="reasonDescription" /> </b>
															</font>
															</td>
															<td class="<%=dispClass2%>">
																<div class="commentDivNew">
																	<b class="wrappedTxt"><bean:write name="cmnt"
																			property="text" />
																	</b>
																</div>
															</td>
														</tr>
													</logic:iterate>
												</table></td>
										</tr>
									</logic:notEmpty>

									<%
										if (dispClass.equals("td-shaded-small")) {
																dispClass = "td-small";
															} else {
																dispClass = "td-shaded-small";
															}
									%>

								</c:forEach>
							</tbody>
						</table>
					</logic:notEqual>
				</fieldset>
			</c:when>
		</c:choose>

		<img height="20" src="../../images/trans.gif" width="10">

		<fieldset class="fieldset1">
			<legend class="legend">
				<fmt:message key="label.person.to.person.transactions" />
				:
			</legend>
			<c:out value="${showCustomerProfileForm.custLogonId}" />
			&nbsp; &nbsp; (Id: <b><c:out value="${custIdMarket}" />
			</b>)

			<logic:equal name="noMGTrans" value="Y">
	&nbsp; &nbsp; <font color="red"><b>(None)</b>
				</font>
			</logic:equal>



			<logic:notEqual name="noMGTrans" value="Y">
				<%
					dispClass = "td-small";
				%>
				<table border='0' width='100%' cellpadding='1' cellspacing='1'>
					<thead>
						<tr>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.owned.by" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.trans.program" />
							</td>
							<!-- MBO-100: Show Profile Type on EMT Admin Consumer profile Page-->
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.profile.type" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.trans.id" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.sender" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.sender.address" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.receiver" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.country.cd" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.amount" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.primary.funding.source" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.status" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.transaction.funding.status" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.date.time" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.trans.scores" />
							</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="transaction"
							items="${showCustomerProfileForm.mgTransactions}">
							<tr>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.ownerId}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.partnerSiteId}" />
								</td>
								<!-- MBO-100: Show Profile Type on EMT Admin Consumer profile Page-->
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.profileType}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><html:link
										action="/transactionDetail.do" paramId="tranId"
										paramName="transaction" paramProperty="tranId">
										<c:out value="${transaction.tranId}" />
									</html:link></td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.senderName}" />
								</td>
								<td class="<%=dispClass%>"><c:out
										value="${transaction.formatAddress}" />
								</td>
								<!-- 3168 -->
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.receiver}" />
								</td>
								
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.countryCode}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.totalAmount}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.primaryFundSource}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.statusDesc}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.subStatusDesc}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><fmt:formatDate
										value="${transaction.statusDate}"
										pattern="dd/MMM/yyyy h:mm a z" /></td>
								<logic:equal name="transaction" property="sysAutoRsltCode"
									value="">
									<td nowrap='nowrap' class='<%=dispClass%>' align='right'>None</td>
								</logic:equal>
								<logic:notEqual name="transaction" property="sysAutoRsltCode"
									value="">
									<td nowrap='nowrap' class='<%=dispClass%>'><a
										href='../../showTranScoreDetail.do?type=q
						&tranId=<bean:write filter="false" name="transaction" property="tranId"/>
						&partnerSiteId=<%=partnerSiteId%>'>
											<bean:write filter="false" name="transaction"
												property="transScores" />-<bean:write filter="false"
												name="transaction" property="sysAutoRsltCode" />
									</a>
									</td>
								</logic:notEqual>
							</tr>

							<logic:notEmpty name="transaction" property="tranScore">
								<bean:define id="score" name="transaction" property="tranScore" />
								<logic:notEmpty name="score" property="scoreRvwCode">
									<tr>
										<td colspan="2">&nbsp;</td>
										<td colspan="8">
											<table border='0' cellpadding='1' cellspacing='1'
												width="100%">
												<tr>
													<td nowrap="nowrap" class="td-small" valign="top"><font
														color="#008b8b"><b>Review Code: <bean:write
																	name="score" property="scoreRvwCode" />
														</b>
													</font>
													</td>
													<td class="td-small"><font color="#2f4f4f"><b>(<bean:write
																	name="score" property="tranScoreCmntText" />)</b>
													</font>
													</td>
												</tr>
											</table></td>
									</tr>
								</logic:notEmpty>
							</logic:notEmpty>
							<!-- MBO-3920: show Receiver Email Address-->
							<logic:notEqual name="transaction"
								property="receiverEmailAddress" value="">
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>


									<td class='TD-HEADER-SMALL'><fmt:message
											key="label.receiver.email.address" /></td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td class="<%=dispClass%>"><c:out
											value="${transaction.receiverEmailAddress}" /></td>
								</tr>
							</logic:notEqual>
							<!-- MBO-3920: Changes Ended-->
							<logic:notEmpty name="transaction" property="cmntList">
								<bean:define id="cmnts" name="transaction" property="cmntList" />
								<tr>
									<td colspan="2">&nbsp;</td>
									<td colspan="8">
										<table border='0' cellpadding='1' cellspacing='1' width="100%">
											<%
												dispClass2 = "TD-SHADED3-SMALL";
											%>
											<logic:iterate id="cmnt" name="cmnts">
												<%
													if (dispClass2.equals("TD-SHADED2-SMALL")) {
																			dispClass2 = "TD-SHADED3-SMALL";
																		} else {
																			dispClass2 = "TD-SHADED2-SMALL";
																		}
												%>
												<tr>
													<td nowrap="nowrap" class="<%=dispClass2%>" valign="top">
														<font color="#008b8b"><b> <fmt:formatDate
																	value="${cmnt.date}" pattern="dd/MMM/yyyy hh:mm a z" />
																- <bean:write name="cmnt" property="createUserId" /> -
																<bean:write name="cmnt" property="cmntReasDesc" /> </b>
													</font>
													</td>
													<td class="<%=dispClass2%>">
														<div class="commentDivNew">
															<b class="wrappedTxt"><bean:write name="cmnt"
																	property="cmntText" />
															</b>
														</div>
													</td>
												</tr>
											</logic:iterate>
										</table></td>
								</tr>
							</logic:notEmpty>

							<%
								if (dispClass.equals("td-shaded-small")) {
												dispClass = "td-small";
											} else {
												dispClass = "td-shaded-small";
											}
							%>

						</c:forEach>
					</tbody>
				</table>
			</logic:notEqual>
		</fieldset>

		<br />
		<fieldset class="fieldset1">
			<legend class="legend">
				<fmt:message key="label.wap.delay.send.transactions" />
				:
			</legend>
			<c:out value="${showCustomerProfileForm.custLogonId}" />
			&nbsp; &nbsp; (Id: <b><c:out value="${custIdMarket}" />
			</b>)

			<logic:equal name="noDSTrans" value="Y">
	&nbsp; &nbsp; <font color="red"><b>(None)</b>
				</font>
			</logic:equal>

			<logic:notEqual name="noDSTrans" value="Y">
				<%
					dispClass = "td-small";
				%>
				<table border='0' width='100%' cellpadding='1' cellspacing='1'>
					<thead>
						<tr>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.owned.by" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.trans.program" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.profile.type" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.trans.id" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.sender" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.sender.address" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.receiver" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.country.cd" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.amount" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.primary.funding.source" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.status" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.transaction.funding.status" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.date.time" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.trans.scores" />
							</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="transaction"
							items="${showCustomerProfileForm.dsTransactions}">
							<tr>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.ownerId}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.partnerSiteId}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.profileType}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><html:link
										action="/transactionDetail.do" paramId="tranId"
										paramName="transaction" paramProperty="tranId">
										<c:out value="${transaction.tranId}" />
									</html:link></td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.senderName}" />
								</td>
								<td class="<%=dispClass%>"><c:out
										value="${transaction.formatAddress}" />
								</td>
								<!-- 3168 -->
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.receiver}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.countryCode}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.totalAmount}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.primaryFundSource}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.statusDesc}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.subStatusDesc}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><fmt:formatDate
										value="${transaction.statusDate}"
										pattern="dd/MMM/yyyy h:mm a z" /></td>
								<logic:equal name="transaction" property="sysAutoRsltCode"
									value="">
									<td nowrap='nowrap' class='<%=dispClass%>' align='right'>None</td>
								</logic:equal>
								<logic:notEqual name="transaction" property="sysAutoRsltCode"
									value="">
									<td nowrap='nowrap' class='<%=dispClass%>'><a
										href='../../showTranScoreDetail.do?type=q&tranId=<bean:write filter="false" name="transaction" property="tranId"/>'><bean:write
												filter="false" name="transaction" property="transScores" />-<bean:write
												filter="false" name="transaction" property="sysAutoRsltCode" />
									</a>
									</td>
								</logic:notEqual>
							</tr>

							<logic:notEmpty name="transaction" property="tranScore">
								<bean:define id="score" name="transaction" property="tranScore" />
								<logic:notEmpty name="score" property="scoreRvwCode">
									<tr>
										<td colspan="2">&nbsp;</td>
										<td colspan="8">
											<table border='0' cellpadding='1' cellspacing='1'
												width="100%">
												<tr>
													<td nowrap="nowrap" class="td-small" valign="top"><font
														color="#008b8b"><b>Review Code: <bean:write
																	name="score" property="scoreRvwCode" />
														</b>
													</font>
													</td>
													<td class="td-small">
														<div class="commentDivNew">
															<b class="wrappedTxt">(<bean:write name="score"
																	property="tranScoreCmntText" />)</b>
														</div>
													</td>
												</tr>
											</table></td>
									</tr>
								</logic:notEmpty>
							</logic:notEmpty>
							<!-- MBO-3920: show Receiver Email Address-->
							<logic:notEqual name="transaction"
								property="receiverEmailAddress" value="">
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>


									<td class='TD-HEADER-SMALL'><fmt:message
											key="label.receiver.email.address" /></td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td class="<%=dispClass%>"><c:out
											value="${transaction.receiverEmailAddress}" /></td>
								</tr>
							</logic:notEqual>
							<!-- MBO-3920: Changes Ended-->

							<logic:notEmpty name="transaction" property="cmntList">
								<bean:define id="cmnts" name="transaction" property="cmntList" />
								<tr>
									<td colspan="2">&nbsp;</td>
									<td colspan="8">
										<table border='0' cellpadding='1' cellspacing='1' width="100%">
											<%
												dispClass2 = "TD-SHADED3-SMALL";
											%>
											<logic:iterate id="cmnt" name="cmnts">

												<%
													if (dispClass2.equals("TD-SHADED2-SMALL")) {
																			dispClass2 = "TD-SHADED3-SMALL";
																		} else {
																			dispClass2 = "TD-SHADED2-SMALL";
																		}
												%>

												<tr>
													<td nowrap="nowrap" class="<%=dispClass2%>" valign="top">
														<font color="#008b8b"><b><fmt:formatDate
																	value="${cmnt.date}" pattern="dd/MMM/yyyy hh:mm a z" /> - <bean:write
																	name="cmnt" property="createUserId" /> - <bean:write
																	name="cmnt" property="cmntReasDesc" /> </b>
													</font>
													</td>
													<td class="<%=dispClass2%>">
														<div class="commentDivNew">
															<b class="wrappedTxt"><bean:write name="cmnt"
																	property="cmntText" />
															</b>
														</div>
													</td>
												</tr>
											</logic:iterate>
										</table></td>
								</tr>
							</logic:notEmpty>

							<%
								if (dispClass.equals("td-shaded-small")) {
												dispClass = "td-small";
											} else {
												dispClass = "td-shaded-small";
											}
							%>
						</c:forEach>
					</tbody>
				</table>
			</logic:notEqual>
		</fieldset>
		<br />

<!--MBO-12057 Added for MGCASH  -->
		<fieldset class="fieldset1">
			<legend class="legend">
				<fmt:message key="label.cash.service.transactions" />
				:
			</legend>
			<c:out value="${showCustomerProfileForm.custLogonId}" />
			&nbsp; &nbsp; (Id: <b><c:out value="${custIdMarket}" />
			</b>)

			<logic:equal name="noCashTrans" value="Y">
	&nbsp; &nbsp; <font color="red"><b>(None)</b>
				</font>
			</logic:equal>



			<logic:notEqual name="noCashTrans" value="Y">
				<%
					dispClass = "td-small";
				%>
				<table border='0' width='100%' cellpadding='1' cellspacing='1'>
					<thead>
						<tr>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.owned.by" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.trans.program" />
							</td>
							<!-- MBO-100: Show Profile Type on EMT Admin Consumer profile Page-->
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.profile.type" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.trans.id" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.sender" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.sender.address" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.receiver" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.country.cd" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.amount" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.primary.funding.source" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.status" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.transaction.funding.status" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.date.time" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.trans.scores" />
							</td>
						</tr>
					</thead>
					<tbody>
					<tr>
					<td><c:out value="${transaction.ownerId}" /> </td>
					</tr>
						<c:forEach var="transaction"
							items="${showCustomerProfileForm.cashTransactions}">
							<tr>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.ownerId}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.partnerSiteId}" />
								</td>
								<!-- MBO-100: Show Profile Type on EMT Admin Consumer profile Page-->
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.profileType}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><html:link
										action="/transactionDetail.do" paramId="tranId"
										paramName="transaction" paramProperty="tranId">
										<c:out value="${transaction.tranId}" />
									</html:link></td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.senderName}" />
								</td>
								<td class="<%=dispClass%>"><c:out
										value="${transaction.formatAddress}" />
								</td>
								<!-- 3168 -->
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.receiver}" />
								</td>
								
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.countryCode}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.totalAmount}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"> CASH
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.statusDesc}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"> CASH
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><fmt:formatDate
										value="${transaction.statusDate}"
										pattern="dd/MMM/yyyy h:mm a z" /></td>
								<logic:equal name="transaction" property="sysAutoRsltCode"
									value="">
									<td nowrap='nowrap' class='<%=dispClass%>' align='right'>None</td>
								</logic:equal>
								<logic:notEqual name="transaction" property="sysAutoRsltCode"
									value="">
									<td nowrap='nowrap' class='<%=dispClass%>'><a
										href='../../showTranScoreDetail.do?type=q
						&tranId=<bean:write filter="false" name="transaction" property="tranId"/>
						&partnerSiteId=<%=partnerSiteId%>'>
											<bean:write filter="false" name="transaction"
												property="transScores" />-<bean:write filter="false"
												name="transaction" property="sysAutoRsltCode" />
									</a>
									</td>
								</logic:notEqual>
							</tr>

							<logic:notEmpty name="transaction" property="tranScore">
								<bean:define id="score" name="transaction" property="tranScore" />
								<logic:notEmpty name="score" property="scoreRvwCode">
									<tr>
										<td colspan="2">&nbsp;</td>
										<td colspan="8">
											<table border='0' cellpadding='1' cellspacing='1'
												width="100%">
												<tr>
													<td nowrap="nowrap" class="td-small" valign="top"><font
														color="#008b8b"><b>Review Code: <bean:write
																	name="score" property="scoreRvwCode" />
														</b>
													</font>
													</td>
													<td class="td-small"><font color="#2f4f4f"><b>(<bean:write
																	name="score" property="tranScoreCmntText" />)</b>
													</font>
													</td>
												</tr>
											</table></td>
									</tr>
								</logic:notEmpty>
							</logic:notEmpty>
						 
							<logic:notEqual name="transaction"
								property="receiverEmailAddress" value="">
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>


									<td class='TD-HEADER-SMALL'><fmt:message
											key="label.receiver.email.address" /></td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td class="<%=dispClass%>"><c:out
											value="${transaction.receiverEmailAddress}" /></td>
								</tr>
							</logic:notEqual>
							<!-- MBO-3920: Changes Ended-->
							<logic:notEmpty name="transaction" property="cmntList">
								<bean:define id="cmnts" name="transaction" property="cmntList" />
								<tr>
									<td colspan="2">&nbsp;</td>
									<td colspan="8">
										<table border='0' cellpadding='1' cellspacing='1' width="100%">
											<%
												dispClass2 = "TD-SHADED3-SMALL";
											%>
											<logic:iterate id="cmnt" name="cmnts">
												<%
													if (dispClass2.equals("TD-SHADED2-SMALL")) {
																			dispClass2 = "TD-SHADED3-SMALL";
																		} else {
																			dispClass2 = "TD-SHADED2-SMALL";
																		}
												%>
												<tr>
													<td nowrap="nowrap" class="<%=dispClass2%>" valign="top">
														<font color="#008b8b"><b> <fmt:formatDate
																	value="${cmnt.date}" pattern="dd/MMM/yyyy hh:mm a z" />
																- <bean:write name="cmnt" property="createUserId" /> -
																<bean:write name="cmnt" property="cmntReasDesc" /> </b>
													</font>
													</td>
													<td class="<%=dispClass2%>">
														<div class="commentDivNew">
															<b class="wrappedTxt"><bean:write name="cmnt"
																	property="cmntText" />
															</b>
														</div>
													</td>
												</tr>
											</logic:iterate>
										</table></td>
								</tr>
							</logic:notEmpty>

							<%
								if (dispClass.equals("td-shaded-small")) {
												dispClass = "td-small";
											} else {
												dispClass = "td-shaded-small";
											}
							%>

						</c:forEach>
					</tbody>
				</table>
			</logic:notEqual>
		</fieldset>
		<br />
		<!-- End of MBO-12057 changes -->
		
		<fieldset class="fieldset1">
			<legend class="legend">
				<fmt:message key="label.economy.service.transactions" />
				:
			</legend>
			<c:out value="${showCustomerProfileForm.custLogonId}" />
			&nbsp; &nbsp; (Id: <b><c:out value="${custIdMarket}" />
			</b>)

			<logic:equal name="noESTrans" value="Y">
	&nbsp; &nbsp; <font color="red"><b>(None)</b>
				</font>
			</logic:equal>

			<logic:notEqual name="noESTrans" value="Y">
				<%
					dispClass = "td-small";
				%>
				<table border='0' width='100%' cellpadding='1' cellspacing='1'>
					<thead>
						<tr>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.owned.by" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.trans.id" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.sender" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.sender.address" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.receiver" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.amount" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.primary.funding.source" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.status" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.transaction.funding.status" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.date.time" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.trans.scores" />
							</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="transaction"
							items="${showCustomerProfileForm.esTransactions}">
							<tr>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.ownerId}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><html:link
										action="/transactionDetail.do" paramId="tranId"
										paramName="transaction" paramProperty="tranId">
										<c:out value="${transaction.tranId}" />
									</html:link></td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.senderName}" />
								</td>
								<td class="<%=dispClass%>"><c:out
										value="${transaction.formatAddress}" />
								</td>
								<!-- 3168 -->
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.receiver}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.totalAmount}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.primaryFundSource}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.statusDesc}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.subStatusDesc}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><fmt:formatDate
										value="${transaction.statusDate}"
										pattern="dd/MMM/yyyy h:mm a z" /></td>
								<logic:equal name="transaction" property="sysAutoRsltCode"
									value="">
									<td nowrap='nowrap' class='<%=dispClass%>' align='right'>None</td>
								</logic:equal>
								<logic:notEqual name="transaction" property="sysAutoRsltCode"
									value="">
									<td nowrap='nowrap' class='<%=dispClass%>'><a
										href='../../showTranScoreDetail.do?type=q&tranId=<bean:write filter="false" name="transaction" property="tranId"/>'><bean:write
												filter="false" name="transaction" property="transScores" />-<bean:write
												filter="false" name="transaction" property="sysAutoRsltCode" />
									</a>
									</td>
								</logic:notEqual>
							</tr>

							<logic:notEmpty name="transaction" property="tranScore">
								<bean:define id="score" name="transaction" property="tranScore" />
								<logic:notEmpty name="score" property="scoreRvwCode">
									<tr>
										<td colspan="2">&nbsp;</td>
										<td colspan="8">
											<table border='0' cellpadding='1' cellspacing='1'
												width="100%">
												<tr>
													<td nowrap="nowrap" class="td-small" valign="top"><font
														color="#008b8b"><b>Review Code: <bean:write
																	name="score" property="scoreRvwCode" />
														</b>
													</font>
													</td>
													<td class="td-small"><font color="#2f4f4f"><b>(<bean:write
																	name="score" property="tranScoreCmntText" />)</b>
													</font>
													</td>
												</tr>
											</table></td>
									</tr>
								</logic:notEmpty>
							</logic:notEmpty>
							<!-- MBO-3920: show Receiver Email Address-->
							<logic:notEqual name="transaction"
								property="receiverEmailAddress" value="">
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>


									<td class='TD-HEADER-SMALL'><fmt:message
											key="label.receiver.email.address" /></td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td class="<%=dispClass%>"><c:out
											value="${transaction.receiverEmailAddress}" /></td>
								</tr>
							</logic:notEqual>
							<!-- MBO-3920: Changes Ended-->

							<logic:notEmpty name="transaction" property="cmntList">
								<bean:define id="cmnts" name="transaction" property="cmntList" />
								<tr>
									<td colspan="2">&nbsp;</td>
									<td colspan="8">
										<table border='0' cellpadding='1' cellspacing='1' width="100%">
											<%
												dispClass2 = "TD-SHADED3-SMALL";
											%>
											<logic:iterate id="cmnt" name="cmnts">

												<%
													if (dispClass2.equals("TD-SHADED2-SMALL")) {
																			dispClass2 = "TD-SHADED3-SMALL";
																		} else {
																			dispClass2 = "TD-SHADED2-SMALL";
																		}
												%>

												<tr>
													<td nowrap="nowrap" class="<%=dispClass2%>" valign="top">
														<font color="#008b8b"><b><fmt:formatDate
																	value="${cmnt.date}" pattern="dd/MMM/yyyy hh:mm a z" /> - <bean:write
																	name="cmnt" property="createUserId" /> - <bean:write
																	name="cmnt" property="cmntReasDesc" /> </b>
													</font>
													</td>
													<td class="<%=dispClass2%>"><font color="#2f4f4f"><b><bean:write
																	name="cmnt" property="cmntText" />
														</b>
													</font>
													</td>
												</tr>
											</logic:iterate>
										</table></td>
								</tr>
							</logic:notEmpty>

							<%
								if (dispClass.equals("td-shaded-small")) {
												dispClass = "td-small";
											} else {
												dispClass = "td-shaded-small";
											}
							%>
						</c:forEach>
					</tbody>
				</table>
			</logic:notEqual>
		</fieldset>

		<br />

		<fieldset class="fieldset1">
			<legend class="legend">
				<fmt:message key="label.express.payment.transactions" />
				:
			</legend>
			<c:out value="${showCustomerProfileForm.custLogonId}" />
			&nbsp; &nbsp; (Id: <b><c:out value="${custIdMarket}" />
			</b>)

			<logic:equal name="noEPTrans" value="Y">
	&nbsp; &nbsp; <font color="red"><b>(None)</b>
				</font>
			</logic:equal>

			<logic:notEqual name="noEPTrans" value="Y">
				<%
					dispClass = "td-small";
				%>
				<table border='0' width='100%' cellpadding='1' cellspacing='1'>
					<thead>
						<tr>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.owned.by" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.profile.type" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.trans.id" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.sender" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.sender.address" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.receiver" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.amount" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.primary.funding.source" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.status" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.transaction.funding.status" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.date.time" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.trans.scores" />
							</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="transaction"
							items="${showCustomerProfileForm.epTransactions}">
							<tr>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.ownerId}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.profileType}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><html:link
										action="/transactionDetail.do" paramId="tranId"
										paramName="transaction" paramProperty="tranId">
										<c:out value="${transaction.tranId}" />
									</html:link></td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.senderName}" />
								</td>
								<td class="<%=dispClass%>"><c:out
										value="${transaction.formatAddress}" />
								</td>
								<!-- 3168 -->
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.receiver}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.totalAmount}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.primaryFundSource}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.statusDesc}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.subStatusDesc}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><fmt:formatDate
										value="${transaction.statusDate}"
										pattern="dd/MMM/yyyy h:mm a z" /></td>
								<logic:equal name="transaction" property="sysAutoRsltCode"
									value="">
									<td nowrap='nowrap' class='<%=dispClass%>' align='right'>None</td>
								</logic:equal>
								<logic:notEqual name="transaction" property="sysAutoRsltCode"
									value="">
									<td nowrap='nowrap' class='<%=dispClass%>'><a
										href='../../showTranScoreDetail.do?type=q&tranId=<bean:write filter="false" name="transaction" property="tranId"/>'><bean:write
												filter="false" name="transaction" property="transScores" />-<bean:write
												filter="false" name="transaction" property="sysAutoRsltCode" />
									</a>
									</td>
								</logic:notEqual>
							</tr>

							<logic:notEmpty name="transaction" property="tranScore">
								<bean:define id="score" name="transaction" property="tranScore" />
								<logic:notEmpty name="score" property="scoreRvwCode">
									<tr>
										<td colspan="2">&nbsp;</td>
										<td colspan="8">
											<table border='0' cellpadding='1' cellspacing='1'
												width="100%">
												<tr>
													<td nowrap="nowrap" class="td-small" valign="top"><font
														color="#008b8b"><b>Review Code: <bean:write
																	name="score" property="scoreRvwCode" />
														</b>
													</font>
													</td>
													<td class="td-small"><font color="#2f4f4f"><b>(<bean:write
																	name="score" property="tranScoreCmntText" />)</b>
													</font>
													</td>
												</tr>
											</table></td>
									</tr>
								</logic:notEmpty>
							</logic:notEmpty>
							<!-- MBO-3920: show Receiver Email Address-->
							<logic:notEqual name="transaction"
								property="receiverEmailAddress" value="">
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>


									<td class='TD-HEADER-SMALL'><fmt:message
											key="label.receiver.email.address" /></td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td class="<%=dispClass%>"><c:out
											value="${transaction.receiverEmailAddress}" /></td>
								</tr>
							</logic:notEqual>
							<!-- MBO-3920: Changes Ended-->

							<logic:notEmpty name="transaction" property="cmntList">
								<bean:define id="cmnts" name="transaction" property="cmntList" />
								<tr>
									<td colspan="2">&nbsp;</td>
									<td colspan="8">
										<table border='0' cellpadding='1' cellspacing='1' width="100%">
											<%
												dispClass2 = "TD-SHADED3-SMALL";
											%>
											<logic:iterate id="cmnt" name="cmnts">
												<%
													if (dispClass2.equals("TD-SHADED2-SMALL")) {
																			dispClass2 = "TD-SHADED3-SMALL";
																		} else {
																			dispClass2 = "TD-SHADED2-SMALL";
																		}
												%>

												<tr>
													<td nowrap="nowrap" class="<%=dispClass2%>" valign="top">
														<font color="#008b8b"><b><fmt:formatDate
																	value="${cmnt.date}" pattern="dd/MMM/yyyy hh:mm a z" /> - <bean:write
																	name="cmnt" property="createUserId" /> - <bean:write
																	name="cmnt" property="cmntReasDesc" /> </b>
													</font>
													</td>
													<td class="<%=dispClass2%>">
														<div class="commentDivNew">
															<b class="wrappedTxt"><bean:write name="cmnt"
																	property="cmntText" />
															</b>
														</div>
													</td>
												</tr>
											</logic:iterate>
										</table></td>
								</tr>
							</logic:notEmpty>

							<%
								if (dispClass.equals("td-shaded-small")) {
												dispClass = "td-small";
											} else {
												dispClass = "td-shaded-small";
											}
							%>
						</c:forEach>
					</tbody>
				</table>
			</logic:notEqual>
		</fieldset>
	
	<br />
<fieldset class="fieldset1">
			<legend class="legend">
				<fmt:message key="label.epcash.service.transactions" />
				:
			</legend>
			<c:out value="${showCustomerProfileForm.custLogonId}" />
			&nbsp; &nbsp; (Id: <b><c:out value="${custIdMarket}" />
			</b>)

			<logic:equal name="noEPCashTrans" value="Y">
	&nbsp; &nbsp; <font color="red"><b>(None)</b>
				</font>
			</logic:equal>

			<logic:notEqual name="noEPCashTrans" value="Y">
				<%
					dispClass = "td-small";
				%>
				<table border='0' width='100%' cellpadding='1' cellspacing='1'>
					<thead>
						<tr>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.owned.by" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.profile.type" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.trans.id" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.sender" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.sender.address" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.receiver" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.amount" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.primary.funding.source" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.status" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.transaction.funding.status" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.date.time" />
							</td>
							<td class='TD-HEADER-SMALL'><fmt:message
									key="label.trans.scores" />
							</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="transaction"
							items="${showCustomerProfileForm.epCashTransactions}">
							<tr>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.ownerId}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.profileType}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><html:link
										action="/transactionDetail.do" paramId="tranId"
										paramName="transaction" paramProperty="tranId">
										<c:out value="${transaction.tranId}" />
									</html:link></td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.senderName}" />
								</td>
								<td class="<%=dispClass%>"><c:out
										value="${transaction.formatAddress}" />
								</td>
								<!-- 3168 -->
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.receiver}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.totalAmount}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="CASH" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="${transaction.statusDesc}" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><c:out
										value="CASH" />
								</td>
								<td nowrap="nowrap" class="<%=dispClass%>"><fmt:formatDate
										value="${transaction.statusDate}"
										pattern="dd/MMM/yyyy h:mm a z" /></td>
								<logic:equal name="transaction" property="sysAutoRsltCode"
									value="">
									<td nowrap='nowrap' class='<%=dispClass%>' align='right'>None</td>
								</logic:equal>
								<logic:notEqual name="transaction" property="sysAutoRsltCode"
									value="">
									<td nowrap='nowrap' class='<%=dispClass%>'><a
										href='../../showTranScoreDetail.do?type=q&tranId=<bean:write filter="false" name="transaction" property="tranId"/>'><bean:write
												filter="false" name="transaction" property="transScores" />-<bean:write
												filter="false" name="transaction" property="sysAutoRsltCode" />
									</a>
									</td>
								</logic:notEqual>
							</tr>

							<logic:notEmpty name="transaction" property="tranScore">
								<bean:define id="score" name="transaction" property="tranScore" />
								<logic:notEmpty name="score" property="scoreRvwCode">
									<tr>
										<td colspan="2">&nbsp;</td>
										<td colspan="8">
											<table border='0' cellpadding='1' cellspacing='1'
												width="100%">
												<tr>
													<td nowrap="nowrap" class="td-small" valign="top"><font
														color="#008b8b"><b>Review Code: <bean:write
																	name="score" property="scoreRvwCode" />
														</b>
													</font>
													</td>
													<td class="td-small"><font color="#2f4f4f"><b>(<bean:write
																	name="score" property="tranScoreCmntText" />)</b>
													</font>
													</td>
												</tr>
											</table></td>
									</tr>
								</logic:notEmpty>
							</logic:notEmpty>
							<!-- MBO-3920: show Receiver Email Address-->
							<logic:notEqual name="transaction"
								property="receiverEmailAddress" value="">
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>


									<td class='TD-HEADER-SMALL'><fmt:message
											key="label.receiver.email.address" /></td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td class="<%=dispClass%>"><c:out
											value="${transaction.receiverEmailAddress}" /></td>
								</tr>
							</logic:notEqual>
							<!-- MBO-3920: Changes Ended-->

							<logic:notEmpty name="transaction" property="cmntList">
								<bean:define id="cmnts" name="transaction" property="cmntList" />
								<tr>
									<td colspan="2">&nbsp;</td>
									<td colspan="8">
										<table border='0' cellpadding='1' cellspacing='1' width="100%">
											<%
												dispClass2 = "TD-SHADED3-SMALL";
											%>
											<logic:iterate id="cmnt" name="cmnts">
												<%
													if (dispClass2.equals("TD-SHADED2-SMALL")) {
																			dispClass2 = "TD-SHADED3-SMALL";
																		} else {
																			dispClass2 = "TD-SHADED2-SMALL";
																		}
												%>

												<tr>
													<td nowrap="nowrap" class="<%=dispClass2%>" valign="top">
														<font color="#008b8b"><b><fmt:formatDate
																	value="${cmnt.date}" pattern="dd/MMM/yyyy hh:mm a z" /> - <bean:write
																	name="cmnt" property="createUserId" /> - <bean:write
																	name="cmnt" property="cmntReasDesc" /> </b>
													</font>
													</td>
													<td class="<%=dispClass2%>">
														<div class="commentDivNew">
															<b class="wrappedTxt"><bean:write name="cmnt"
																	property="cmntText" />
															</b>
														</div>
													</td>
												</tr>
											</logic:iterate>
										</table></td>
								</tr>
							</logic:notEmpty>

							<%
								if (dispClass.equals("td-shaded-small")) {
												dispClass = "td-small";
											} else {
												dispClass = "td-shaded-small";
											}
							%>
						</c:forEach>
					</tbody>
				</table>
			</logic:notEqual>
		</fieldset>
		</logic:equal>
		<br/>

	<fieldset class="fieldset1">
		<legend class="legend">
			<fmt:message key="label.ip.history.summary" />
			:
		</legend>
		<c:out value="${showCustomerProfileForm.custLogonId}" />
		&nbsp; &nbsp; (Id: <b><c:out value="${custIdMarket}" />
		</b>)

		<logic:empty name="ipHistorySummaryList">
&nbsp; &nbsp; <font color="red"><b>(None)</b>
			</font>
		</logic:empty>

		<logic:notEmpty name="ipHistorySummaryList">
			<% dispClass = "td-small"; %>
			<table border='0' width='100%' cellpadding='1' cellspacing='1'>
				<thead>
					<tr>
						<td class='TD-HEADER-SMALL'><fmt:message
								key="label.consumer.ip.address" />
						</td>
						<td class='TD-HEADER-SMALL'><fmt:message
								key="label.all.users" />
						</td>
						<td class='TD-HEADER-SMALL'><fmt:message
								key="label.all.users" />
						</td>
						<td class='TD-HEADER-SMALL'><fmt:message
								key="label.this.user" />
						</td>
						<td class='TD-HEADER-SMALL'><fmt:message
								key="label.logon.count" />
						</td>
						<td class='TD-HEADER-SMALL'><fmt:message
								key="label.first.logon.date" />
						</td>
						<td class='TD-HEADER-SMALL'><fmt:message
								key="label.last.logon.date" />
						</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="ipSummary" items="${ipHistorySummaryList}">
						<tr>
							<td nowrap="nowrap" class="<%=dispClass%>">
								<div>
									<a href="#" class="toolTipCTA"> <c:out
											value="${ipSummary.ipAddress}" />
									</a>
									<div class="toolTip addClose">
										<p>
											Country :
											<c:out value="${ipSummary.country}" />
										</p>
										<p>
											State :
											<c:out value="${ipSummary.state}" />
										</p>
										<p>
											City :
											<c:out value="${ipSummary.city}" />
										</p>
										<p>
											Postal Code :
											<c:out value="${ipSummary.zipCode}" />
										</p>
										<p>
											ISP :
											<c:out value="${ipSummary.ISP}" />
										</p>
									</div>
									<a href="#" class="toolTipCTA"><span>MD</span>
									</a>
									<div class="toolTip addClose">
										<p>
											Country :
											<c:out value="${ipSummary.country}" />
										</p>
										<p>
											State :
											<c:out value="${ipSummary.state}" />
										</p>
										<p>
											City :
											<c:out value="${ipSummary.city}" />
										</p>
										<p>
											Postal Code :
											<c:out value="${ipSummary.zipCode}" />
										</p>
										<p>
											ISP :
											<c:out value="${ipSummary.ISP}" />
										</p>
									</div>
									<c:if test="${ipSummary.ipAddressTaintText == 'Not Blocked'}">
										<logic:equal name="addBlockedIp" value="Y">
							&nbsp; &nbsp; <a
												href='../../showIPBlockedList.do?ipAddress=<c:out value="${ipSummary.ipAddress}" />&submitAdd=add'>Block
												this IP</a>
										</logic:equal>
									</c:if>
									<c:if test="${ipSummary.ipAddressTaintText != 'Not Blocked'}">
										<font color="red"><b> &nbsp; (<c:out
													value="${ipSummary.ipAddressTaintText}" />)</b>
										</font>
									</c:if>
									<div>
							</td>
							<td nowrap="nowrap" class="<%=dispClass%>"><a
								href='../../adhocReport.do?senderIP=<c:out
					value="${ipSummary.ipAddress}"/>&beginDateText=01/Jan/2003&submitSearch=Search'>
									<b>Trans for IP</b>
							</a>
							</td>
							<td nowrap="nowrap" class="<%=dispClass%>"><a
								href='../../logonIpConsumers.do?ipAddress=<c:out
					value="${ipSummary.ipAddress}"/>'
								onClick="this.href='javascript:showIPHistory(\'<%=request.getContextPath()%>/logonIpConsumers.do?ipAddress=<c:out
					value="${ipSummary.ipAddress}"/>\')'">
									<b>Logins for IP</b>
							</a>
							</td>
							<td nowrap="nowrap" class="<%=dispClass%>"><a
								href='../../adhocReport.do?profileId=<c:out
					value="${showCustomerProfileForm.custLogonId}" />&senderIP=<c:out
					value="${ipSummary.ipAddress}"/>&beginDateText=01/Jan/2003&submitSearch=Search'>
									<b>Trans for IP/Login</b>
							</a>
							</td>
							<td nowrap="nowrap" class="<%=dispClass%>"><a
								href='<c:out value="${url}" />'
								onClick="this.href='javascript:showIPHistory(\'<c:out value="${url}" />&ipAddress=<c:out
						value="${ipSummary.ipAddress}"/>\')'">
									<b><c:out value="${ipSummary.loginCount}" />
								</b>
							</a>
							</td>
							<td nowrap="nowrap" class="<%=dispClass%>"><c:out
									value="${ipSummary.firstLoginDate}" />
							</td>
							<td nowrap="nowrap" class="<%=dispClass%>"><c:out
									value="${ipSummary.lastLoginDate}" />
							</td>
						</tr>
						<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small";
				} else {
					dispClass = "td-shaded-small";
				}  %>
					</c:forEach>
					<tr>
						<td nowrap="nowrap" class="td-small" colspan='4' align='center'>
							Click <a href='<c:out value="${url}" />'
							onClick="this.href='javascript:showIPHistory(\'<c:out value="${url}" />\')'">
								<b>here</b>
						</a> to see all IPs history details.</td>
					</tr>
				</tbody>
			</table>

		</logic:notEmpty>
	</fieldset>
	<br />
	<%-- <logic:notEmpty name="gbGroupLog">
	<div class="gbGroupdiv">
		<html:form action="showCustomerProfile" styleClass="view">
		<fieldset>
			<table>
			  <tr>
			    <td class="td-small">
			    <b>
			    	<fmt:message key="label.decision">
			    	</fmt:message>:</b>
			    	<c:out value="${gbGroupLog.deciBandText}"></c:out> 
			    </td>
			  </tr>
			  <tr>
			  	<td class="td-small">
			  		<b>
			  			<fmt:message key="label.score">
			  			</fmt:message>:
			  		</b>
			    	<c:out value="${gbGroupLog.scoreNbr}"></c:out> 
			  	</td>
			  </tr>
			  <tr>
			  	<td>
			  	<c:if test="${not empty gbGroupLog && not empty gbGroupLogDetails}">
			  		<fieldset>
			  			<legend class="legend"><c:out value="${gbGroupLog.chkTypeDesc}"></c:out></legend>
			  				<table align="center" width="100%">
								<logic:iterate name="gbGroupLogDetails" id="detail">
								  <tr>
									<c:choose>
										<c:when test="${detail.inCommentRange}">
											<td>
												<img src="../../images/info.png" height="28px" width="28px"/>
											</td>
										</c:when>
										<c:when test="${detail.inMatchRange}">
											<td>
												<img src="../../images/ok.png" height="28px" width="28px"/>
											</td>
										</c:when>
										<c:when test="${detail.inWarningRange}">
											<td>
												<img src="../../images/warning.png" height="28px" width="28px"/>
											</td>
										</c:when>
										<c:when test="${detail.inMismatchRange}">
											<td>
												<img src="../../images/wrong.png" height="28px" width="28px"/>
											</td>
										</c:when>
										<c:otherwise>
											<td>
												<c:out value="${detail.rsltSvrtyText}"></c:out>	
											</td>
										</c:otherwise>
									</c:choose>
								    <td class="td-small" style="font-size:x-small;">&nbsp;&nbsp;<bean:write name="detail" property="rsltCode"/></td>
								    <td class="td-small" style="font-size:x-small;">&nbsp;&nbsp;<bean:write name="detail" property="rsltDesc"/></td>
								  </tr>
								</logic:iterate>							  
							</table>
			  		</fieldset>
			  		</c:if>
			  	</td>
			  </tr>
			</table>
		</fieldset>
		<a href="#" class="viewGBGroupLogDetailsExit">Cancel/Exit</a>
		</html:form>
	</div>

	<script type="text/javascript" language="JavaScript">
		$(document).ready(function(){
		
		$('.viewGBGroupLogDetailsTrigger').click(function(e){
			e.preventDefault();
			$.EMTbox(".gbGroupdiv");
			$('html, body').css('overflow','auto');
			$('.viewGBGroupLogDetailsExit').click(function(e){
				e.preventDefault();
				$.EMTbox.close();
				$('html, body').css('overflow','hidden');
				});
			});
		});

	</script>

</logic:notEmpty></div>
 --%>
	<img height="20" src="../../images/trans.gif" width="10">
	<script>

    /*Ajax call to BPS Search functionality*/
	function callPersonSearch(custId) {
	  var url="${pageContext.request.contextPath}/getPersonSearch.do?custId="+custId;
	  document.getElementById("personSrchBtn").disabled = true;
      if(window.XMLHttpRequest){
           req =new XMLHttpRequest();
           req.onreadystatechange = onComplete;
           try {
               req.open("POST", url, true);
           } catch (e){}
           req.send(null);
      }
     }
   var personSrchId = "";
   var relativeSrchId = "";
   /*Method to set the values from the ajax httpresponse to the lex nex avtivity log table*/
   function onComplete(){
      if(req.readyState ==4){// Complete
		if(req.status ==200){// OK response
			addRow("lexNexActivity","BPS");
		}else{
	       alert.render("Error occurred during BPS Search");
	       document.getElementById("personSrchBtn").disabled = false;
	  }
     }
   }
   /* Method to open a popup to diaplay the address details*/
   function showPersonSearch(primaryId){
   
    var url = "${pageContext.request.contextPath}/showBasicPersonSearchDetails.do?primaryId="+primaryId;
   	detailWindow = window.open(url ,"","status,scrollbars,resizable,width=1000,height=600");
	detailWindow.focus();
   }
   /*Method to enable or disable BPS Search button on load*/
    function enableBPSButton(){
		var bpsEnable = document.getElementById("isBPSEnable").value;
		if(bpsEnable=="true"){
			document.getElementById("personSrchBtn").disabled = false;//enablein button
		}else{
			document.getElementById("personSrchBtn").disabled = true;//disabling button
		}
	} 
	enableBPSButton();
	
	/*Ajax call to Relative Search functionality*/
	function callRelativeSearch(custId) {
	  var url="${pageContext.request.contextPath}/getRelativeSearch.do?custId="+custId;
	  document.getElementById("relativeSrchBtn").disabled = true;
      if(window.XMLHttpRequest){
           req =new XMLHttpRequest();
           req.onreadystatechange = onSuccess;
           try {
               req.open("POST", url, true);
           } catch (e){}
           req.send(null);
      }
     }
     
     /*Method to set the values from the ajax httpresponse to the lex nex avtivity log table*/
   function onSuccess(){
      if(req.readyState ==4){// Complete
		if(req.status ==200){// OK response
			addRow("lexNexActivity","RLS");
	  	}else{
	       alert.render("The Request has time out");
	       document.getElementById("relativeSrchBtn").disabled = false;
	  	}
     }
   }
   
    /* Method to open a popup to diaplay the relative details*/
   function showRelativeSearch(relativeId){
    var url = "${pageContext.request.contextPath}/showRelativesSearchDetails.do?primaryId="+relativeId;
   	detailWindow = window.open(url ,"","status,scrollbars,resizable,width=1000,height=600");
	detailWindow.focus();
   }
   
  function addRow(tableID,btnName) {
    var table = document.getElementById(tableID);
    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);
    var cell1 = row.insertCell(0);
	var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
	var cell4 = row.insertCell(3);
	var cell5 = row.insertCell(4);
	var cell6 = row.insertCell(5);
			
    if("BPS" == btnName){
        row.className = "td-small";
       	var rootElem=req.responseXML.getElementsByTagName('Activity')[0];
		var riskelement = rootElem.getElementsByTagName('highRiskIndicator')[0];
		cell6.innerHTML = "BPS Search - Person Not Found";
		cell2.innerHTML = "Person Not Found";
		for(x =0;x<rootElem.childNodes.length;x++)
  		{
			var xmlelem=rootElem.childNodes[x].firstChild;
			if(xmlelem != null){
				var elename=rootElem.childNodes[x].nodeName;
				var elevalue=xmlelem.nodeValue;
				if("id" == elename)
				    personSrchId = elevalue;
				if("ssnDisp" == elename)
				    cell5.innerHTML = elevalue; 
				if("ssnValidDisp" == elename)
				    cell4.innerHTML = elevalue;
				if("activityDateDisp" == elename)
				    cell1.innerHTML = elevalue;
				if("uniqueId" == elename){
				   if(null != elevalue.trim()){
				    	cell6.innerHTML = "BPS Search Initiated";
				    	cell2.innerHTML = "<a href='javascript:showPersonSearch("+personSrchId+");'>Link to the response which includes addresses if any</a>";
				    	document.getElementById("relativeSrchBtn").disabled = false;
				    }    
				}    
			}
		}
		for(i =0;i<riskelement.childNodes.length;i++)
  		{
  		    var riskelem = riskelement.getElementsByTagName('RiskIndicator')[i];
		    for(j =0;j<riskelem.childNodes.length;j++)
  			{
				var xmlelement=riskelem.childNodes[j].firstChild;
				if(riskelement != null){
				   var elementname=riskelem.childNodes[j].nodeName;
				   var elementvalue=riskelement.nodeValue;
				   if("code" == elementname){
					   cell3.innerHTML += xmlelement.nodeValue;
					   cell3.innerHTML += " , "
				   }if("description" == elementname){
				       cell3.innerHTML += xmlelement.nodeValue;
				       cell3.innerHTML += "<br />";
				   }    
			    }
			}
		}
    }else{
         row.className = "td-shaded-small";
		 var rootElem=req.responseXML.getElementsByTagName('list')[0];
		 var actelem = rootElem.getElementsByTagName('Activity')[0];
		 cell6.innerHTML = "Relative Search - Relative Not Found";
		 cell2.innerHTML = "Relative Not Found";
		 for(i =0;i<actelem.childNodes.length;i++){
        	var xmlelement=actelem.childNodes[i].firstChild;
			if(xmlelement != null){
			 var elename=actelem.childNodes[i].nodeName;
			 var elevalue=xmlelement.nodeValue;
        	if("id" == elename)
				relativeSrchId = elevalue;
		    if("activityDateDisp" == elename){
				cell1.innerHTML = elevalue;
				
			}
			if("uniqueId" == elename){
				if(null != elevalue.trim()){
				 cell6.innerHTML = "Relative Search Initiated";
				 cell2.innerHTML = "<a href='javascript:showRelativeSearch("+relativeSrchId+");'>Link to see the relative search details</a>";
				}    
			}	
			}
		}
	}
}

/*Method to enable or disable Relative Search button on load*/
    function enableRLSButton(){
		var rlsEnable = document.getElementById("isRLSEnable").value;
		if(rlsEnable=="true"){
			document.getElementById("relativeSrchBtn").disabled = false;//enablein button
		}else{
			document.getElementById("relativeSrchBtn").disabled = true;//disabling button
		}
	} 
	enableRLSButton();
   </script>