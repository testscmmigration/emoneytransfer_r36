<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/datePicker.js" language="JavaScript"></script>
<script src="../../Display/jscript/setOtherRangeValue.js" language="JavaScript"></script>
<script src="../../Display/jscript/showUser.js" language="JavaScript"></script>

<CENTER>
<span class="error">
	<html:errors />
</span>

</CENTER>

<html:form action="/showACHReturnQueue.do" focus="submitRefresh">

<h3><fmt:message key="label.achreturn.queue.list"/></h3>


<table>
	<tr>
	
		<td nowrap='nowrap' class='TD-SMALL'>
			<b><fmt:message key="label.date.begin"/><fmt:message key="label.suffix"/></b>&nbsp;
			<html:text size="10" property="beginDateText" onchange="setOtherRangeValue(this, showACHReturnQueueForm.endDateText);" />
			<img border="0" src="../../images/calendar.gif" onclick="show_calendar('showACHReturnQueueForm.beginDateText');" onmouseover="window.status='Pick a Date';return true;" onmouseout="window.status='';return true;"/>
		</td>			
	
		<td width="20">
			&nbsp;
		</td>

		<td  nowrap='nowrap' class='TD-SMALL'>
			<b><fmt:message key="label.date.end"/><fmt:message key="label.suffix"/></b>&nbsp;
			<html:text size="10" property="endDateText" onchange="setOtherRangeValue(this, showACHReturnQueueForm.beginDateText);" />
		 	<img border="0" src="../../images/calendar.gif" onclick="show_calendar('showACHReturnQueueForm.endDateText');" onmouseover="window.status='Pick a Date';return true;" onmouseout="window.status='';return true;"/>
		</td>	

		<td width="20">
			&nbsp;
		</td>
	
		<td  nowrap='nowrap' class='TD-SMALL'>
			<b><fmt:message key="label.achreturn.status" />: </b>
			<html:select name="showACHReturnQueueForm" property="status" size="1">
				<html:option value="ALL"><fmt:message key="option.all"/></html:option>												
				<html:options collection="achTranStatuses" property="achReturnStatCode" labelProperty="achReturnStatDesc" />
			</html:select>
		</td>
	</tr>

	<tr>
		<td nowrap='nowrap' class='TD-SMALL'>
			<b><fmt:message key="label.achreturn.type" />: </b>
			<html:select name="showACHReturnQueueForm" property="type" size="1">
				<html:option value="ALL"><fmt:message key="option.all"/></html:option>												
				<html:options collection="achTranTypes" property="statusCode" labelProperty="statusDescription" />
			</html:select> &nbsp;
		</td>

		<td width="20">
			&nbsp;
		</td>
		
		<td  nowrap='nowrap' class='TD-SMALL'>	
			<b><fmt:message key="label.sort.by" />: </b>
			<html:select name="showACHReturnQueueForm" property="sortBy" size="1">
				<html:option value="achReturnTranId"><fmt:message key="label.achreturn.hrdtranid"/></html:option>
				<html:option value="returnStatDesc"><fmt:message key="label.achreturn.hrdstatus"/></html:option>
				<html:option value="tranId"><fmt:message key="label.achreturn.hrdmgitranid"/></html:option>
				<html:option value="microTranId"><fmt:message key="label.md.vldn.id"/></html:option>
				<html:option value="returnTypeDesc"><fmt:message key="label.achreturn.hdrrettype"/></html:option>
				<html:option value="returnReasonDesc"><fmt:message key="option.achreturn.retreas"/></html:option>
				<html:option value="creditAmount"><fmt:message key="label.achreturn.hdrcramount"/></html:option>
				<html:option value="debitAmount"><fmt:message key="label.achreturn.hdrdramount"/></html:option>
				<html:option value="effectiveDate"><fmt:message key="option.achreturn.date"/></html:option>
			</html:select> 
		</td>

		<td width="20">
			&nbsp;
		</td>

		<td  nowrap='nowrap' class='TD-SMALL'>
			<html:select name="showACHReturnQueueForm" property="sortSeq" size="1">
				<html:option value="A"><fmt:message key="option.ascending"/></html:option>
				<html:option value="D"><fmt:message key="option.descending"/></html:option>
			</html:select>
		</td>
	</tr>
	</table>

	<%String dispClass = "TD-SHADED-SMALL"; %>

	<table>
	<tr><td nowrap='nowrap' class='TD-SMALL'>
		<div id="formButtons">
			<b><font color="#CF002F"><fmt:message key="label.total.records"
			/> : <bean:write name="showACHReturnQueueForm" property="recordCount" /></font></b>
			&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
			<input type='submit' class='transbtn2' name='submitRefresh' value='Refresh Queue' 
				onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
		</div>
	
		<div id="processing" style="visibility: hidden">
			<span class="PROCESSING"><fmt:message key="message.in.process" /></span>
		</div >
	
	</td></tr>
	</table>



	<logic:notEmpty name="achReturnQueue">
	<table>
		<tbody>
			<tr>
			<td  nowrap='nowrap' class='TD-SMALL' colspan='11' align='right'><a href="../../ExcelReport.emt?id=achReturnQueue" target="excel">
				<fmt:message key="msg.view.or.download.excel" /></a></td>
			</tr>
			<tr>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'>&nbsp;&nbsp;&nbsp;</TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.achreturn.hrdtranid"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.achreturn.hrdstatus"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.achreturn.hrdmgitranid"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.md.vldn.id"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.achreturn.hdrrettype"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.achreturn.hdrretreas"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.achreturn.hdrcramount"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.achreturn.hdrdramount"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.achreturn.hdrindiv"/></TD>			
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.achreturn.hdrdate"/></TD>		
			</tr>

	
			<logic:iterate id="achReturn" name="achReturnQueue">

			<tr>
				<td nowrap='nowrap' class='<%=dispClass%>'>
				
					<logic:notEqual name="achReturn" property="returnStatCode" value="CLS" >
						<input type="checkbox" name="selectedItems"	value="<bean:write filter="false" name="achReturn" property="achReturnTranId"/>" />		
					</logic:notEqual> 

					<logic:equal name="achReturn" property="returnStatCode" value="CLS" >
						<input type="checkbox" name="selectedItems"	value="<bean:write filter="false" name="achReturn" property="achReturnTranId"/>" disabled />		
					</logic:equal> 
					
				</td>

				<td nowrap='nowrap' class='<%=dispClass%>'>
					<a href='../../showACHReturnDetail.do?achTranReturnId=<bean:write filter="false" name="achReturn" property="achReturnTranId"/>'>
					<bean:write filter="false" name="achReturn" property="achReturnTranId"/>
					</a>
				</td>
					
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write filter="false" name="achReturn" property="returnStatDesc"/></td>

				<td nowrap='nowrap' class='<%=dispClass%>'>
					<a href='../../transactionDetail.do?tranId=<bean:write filter="false" name="achReturn" property="tranId"/>'>
					 <bean:write filter="false" name="achReturn" property="tranId"/>
					</a>
				</td>
				
				<td nowrap='nowrap' class='<%=dispClass%>'>
					
					<a href='../../recordMDACHReturn.do?micVldnId=<bean:write filter="false" name="achReturn" property="microTranId"/>'>
					 <bean:write filter="false" name="achReturn" property="microTranId"/>
					</a>
					
				</td>

				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write filter="false" name="achReturn" property="returnTypeDesc"/>
				</td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write filter="false" name="achReturn" property="returnReasonDesc"/>
				</td>
				<td nowrap='nowrap' class='<%=dispClass%>' align="right">
					<bean:write filter="false" name="achReturn" property="creditAmount"/>
				</td>
				<td nowrap='nowrap' class='<%=dispClass%>' align="right">
					<bean:write filter="false" name="achReturn" property="debitAmount"/>
				</td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write filter="false" name="achReturn" property="individualName"/>
				</td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write filter="false" name="achReturn" property="effectiveDate"/>
				</td>
				
				</tr>
			<%  if (dispClass.equals("TD-SHADED-SMALL")) {
					dispClass = "TD-SMALL"; 
				} else {
					dispClass = "TD-SHADED-SMALL";
				}  %>
			</logic:iterate>
		</tbody>
	</table>
	</logic:notEmpty>
	
	<table>
	<tr><td nowrap='nowrap' class='TD-SMALL'>
		<div id="formButtons1">
			<input type='submit' name='submitCloseSelected' value='Close Checked Items' 
				onclick="formButtons1.style.display='none';processing1.style.visibility='visible';" />
		</div>
	
		<div id="processing1" style="visibility: hidden">
			<span class="PROCESSING"><fmt:message key="message.in.process" /></span>
		</div >
	
	</td></tr>
	</table>
	

</html:form>
