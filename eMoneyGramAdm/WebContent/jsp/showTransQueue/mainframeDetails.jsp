<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<center>
 
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

<h3>Mainframe Details</h3>
</center>
<center>
<html:form action="/showMainframeDetails.do">

<html:hidden property="id" />
	<input type="button" name="back" value="Return to Transaction Details" onclick="history.back()"></input>
	<table border='0' cellpadding='2' cellspacing='1' width='100%'>
	
	 <logic:notEmpty name="showMainframeDetailsForm" property="receiptTextInfoEng"> 
	
	   	    
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>RecieptTextInfo: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="receiptTextInfoEng" /></td>
	   </tr>
	   </logic:notEmpty>

	    
	
	
		<tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>TransactionStatus: </b> 
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="transactionStatus" /></td></tr>
		
	

	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>DateTimeSent: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="dateTimeSent" />
			</td>
		</tr>
			    
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>FeeAmount: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="feeAmount" />
			  </td>
		</tr>
			    
		<tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>SendAmount: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="sendAmount" />
			</td>
		</tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>SendCurrency: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="sendCurrency" />
			</td>
	   </tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>ReceiveAmount: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="receiveAmount" />
			 </td>
		</tr>
	  	<tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>ReceiveCurrency: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="receiveCurrency" />
			 </td>
		</tr>
	   	<tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>ReceiveCountry: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="receiveCountry" />
			</td>
		</tr>
	   	<tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>DeliveryOption: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="deliveryOption" />
			 </td>
		</tr>
	    <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>SenderFirstName: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="senderFirstName" />
			</td>
		</tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>SenderMiddleName: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="senderMiddleInitial" />
			</td>
		</tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>SenderLastName: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="senderLastName" />
			 </td>
		</tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>SenderAddress: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="senderAddress" />
			 </td>
		</tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>SenderCity: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="senderCity" />
			</td>
		</tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>SenderState: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="senderState" />
			 </td>
		</tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>SenderZipCode: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="senderZipCode" />
			</td>
		</tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>SenderCountry: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="senderCountry" />
			 </td>
		</tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>SenderHomePhone: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="senderHomePhone" />
			</td>
		</tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>ReceiverFirstName: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="receiverFirstName" />
			 </td>
		</tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>ReceiverMiddleName: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="receiverMiddleInitial" />
			 </td>
		</tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>ReceiverLastName: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="receiverLastName" />
			 </td>
		</tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>ReceiverLastName2: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="receiverLastName2" />
			</td>
		</tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>ReceiverAddress: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="receiverAddress" />
			 </td>
		</tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>ReceiverCity: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="receiverCity" />
			</td>
		</tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>ReceiverState: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="receiverState" />
			</td>
		</tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>ReceiverZipCode: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="receiverZipCode" />
			</td>
		</tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>ReceiverCountry: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="receiverCountry" />
			</td>
		</tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>ReceiverPhone: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="receiverPhone" /></td></tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>Direction1: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="direction1" /></td></tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>Direction2: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="direction2" /></td></tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>Direction3: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="direction3" /></td></tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>MessageField1: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="messageField1" /></td></tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>MessageField2: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="messageField2" /></td></tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>SenderBirthCity: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="senderBirthCity" /></td></tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>SenderBirthCountry: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="senderBirthCountry" /></td></tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>ReceiverColonia: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="receiverColonia" /></td></tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>ReceiverMunicipio: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="receiverMunicipio" /></td></tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>OperatorName: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="operatorName" /></td></tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>ValidIndicator: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="validIndicator" /></td></tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>AgentUseSendData: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="agentUseSendData" /></td></tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>AgentUseReceiveData: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="agentUseReceiveData" /></td>
	   </tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>Expected Delivery Date: </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
		 		<fmt:formatDate value="${showMainframeDetailsForm.expectedDateOfDelivery}" pattern="MM/d/yyyy" />
		 	</td>
	   </tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>MoneyGramCustomerReceiveNumber (RRN): </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="mgCustomerReceiveNumber" /></td>
	   </tr>
	   <tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'>
				<b>PartnerCustomerReceiveNumber (RRN): </b>
			</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="showMainframeDetailsForm" property="partnerCustomerReceiveNumber" /></td>
	   </tr>

	</table>
	<input type="button" name="back" value="Return to Transaction Details" onclick="history.back()"></input>

</html:form>
