<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script src="../../Display/jscript/showUser.js" language="JavaScript"></script>
<script src="../../Display/jscript/textCounter.js" language="JavaScript"></script>

<center>

<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>
</center>
<fmt:setTimeZone value="${userTimeZone}" />
<h3><fmt:message key="label.transaction.detail"/></h3>

<center>

<html:form action="/transactionDetail.do" focus="submitExit">
	<html:hidden property="custId" />
	<html:hidden property="sendCustAcctId" />
	<html:hidden property="sendCustBkupAcctId" />
	<html:hidden property="esMGSendFlag" />

	<html:hidden property="rcvCustMatrnlName" />
	<html:hidden property="rcvCustMidName" />
	<html:hidden property="rcvCustAddrStateName" />
	<html:hidden property="rcvAgentRefNbr" />
	<html:hidden property="rcvCustAddrLine1Text" />
	<html:hidden property="rcvCustAddrLine2Text" />
	<html:hidden property="rcvCustAddrLine3Text" />
	<html:hidden property="rcvCustAddrCityName" />
	<html:hidden property="rcvCustAddrPostalCode" />
	<html:hidden property="rcvCustDlvrInstr1Text" />
	<html:hidden property="rcvCustDlvrInstr2Text" />
	<html:hidden property="rcvCustDlvrInstr3Text" />
	<html:hidden property="rcvCustPhNbr" />
	<html:hidden property="tranSendCity" />
	<html:hidden property="tranSendState" />
	<html:hidden property="partnerSiteId" />

	

	<logic:present name="ARRDate">
		<h3>Warning: The original ACH Request is still in process.<br /> Please wait to process the refund request until the original ACH has cleared.<br />
			<logic:equal name="partialMessage" value="show">
		    	Calculated ACH clearance at <bean:write name="ARRDate" />.
		    </logic:equal>
		</h3>
	</logic:present>

		<c:choose>
			<c:when
				test="${(transactionDetailForm.transPartnerSiteId)=='WAP'|| (transactionDetailForm.transPartnerSiteId)=='MGOUK'||(transactionDetailForm.transPartnerSiteId)=='MGODE'}">
				<jsp:include
					page="/jsp/showTransQueue/transactionDetailColumn_${fn:toLowerCase(transactionDetailForm.transPartnerSiteId)}.jsp"
					flush="true" />
			</c:when>
			<c:otherwise>
				<jsp:include
					page="/jsp/showTransQueue/transactionDetailColumn_mgo.jsp"
					flush="true" />
			</c:otherwise>
		</c:choose>


	<!--       ===========  Ownership  =======================       -->

	<logic:equal name="tranAuth" property="canOwn" value="true">
		<logic:equal name="tranAuth" property="isOwner" value="true">
			You are the owner of this transaction, you may
				<a href='../../transOwnership.do?action=Release&id=<bean:write name="transactionDetailForm" property="tranId" />&detail=y'
		    	>Release</a> this transaction.<br />
		</logic:equal>
		<logic:equal name="tranAuth" property="isOwner" value="false">
			<logic:equal name="tranAuth" property="ownerId" value="">
			<c:choose>
			<c:when
				test="${(transactionDetailForm.tranType)=='MGCASH'|| (transactionDetailForm.tranType)=='EPCASH'}">
				You are not the owner of this transaction, you may Take Ownership of this transaction.<br />
			</c:when>
			<c:otherwise>
				You are not the owner of this transaction, you may
					<a href='../../transOwnership.do?action=TakeOwnership&id=<bean:write name="transactionDetailForm" property="tranId" />&detail=y'
		    		>Take Ownership</a> of this transaction.<br />
			</c:otherwise>
		</c:choose>
			</logic:equal>
			
			<logic:notEqual name="tranAuth" property="ownerId" value="">
				This transaction is owned by

					<a href='../../showUser.do?userId=<c:out
					value="${tranAuth.ownerId}"/>'
					onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out
					value="${tranAuth.ownerId}"/>\')'">
					<b><bean:write name="tranAuth" property="ownerId" /></b></a>
					, you may
					<a href='../../transOwnership.do?action=TakeOver&id=<bean:write
					name="transactionDetailForm" property="tranId"/>&oldUserId=<bean:write
					name="tranAuth" property="ownerId" />&detail=Y'>Takeover</a> this transaction.<br />
			</logic:notEqual>
		</logic:equal>
	</logic:equal>
<p class='TD-SMALL'>
	<input type='submit' class='transbtn1' name='submitExit' value='Return To Transaction Queue List' />
</p>
</center>
<hr />

<!--       ===========  tran actions  =======================       -->

<logic:notEmpty name="tranActions">
<h3><fmt:message key="label.transaction.action.history" /></h3>
 
 	<%
 		emgadm.security.TranSecurityMatrix matrix = null;
		matrix=(emgadm.security.TranSecurityMatrix) request.getSession().getAttribute("tranAuth");
	%>
	
	<logic:equal name="tranAuth" property="isOwner" value="true">
	 	<c:if test='<%=matrix.isCcPartialRefund()%>'>
		  <div class='TD-SMALL' align='right' >
		  	<%=matrix.getPartialRefundButton()%>
		  </div>
		  <p/>
	  	</c:if>
	</logic:equal>
	
	<table border='1' cellpadding='1' cellspacing='1'>
		<tbody>
		<% String dispClass = "TD-SMALL";%>
			<tr>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.date" /></td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.userid" /></td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.desc" /></td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.from.status" /></td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.to.status" /></td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.sub.reason" /></td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.cc.auth.id" /></td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.post.amt" /></td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.dr.acct.desc" /></td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.cr.acct.desc" /></td>
			</tr>
			<logic:iterate id="tranAction" name="tranActions">
			<tr>
				<td nowrap='nowrap' class='<%=dispClass%>' valign='top'>
					<fmt:formatDate value="${tranAction.date}" pattern="dd/MMM/yyyy hh:mm:ss a z" /></td>

				<logic:equal  name="tranAction" property="tranCreateUserid" value="emgwww">
					<td nowrap='nowrap' class='<%=dispClass%>' valign='top'>
						<bean:write name="tranAction" property="tranCreateUserid"/> &nbsp; </td>
				</logic:equal>
				<logic:notEqual  name="tranAction" property="tranCreateUserid" value="emgwww">
					<td nowrap='nowrap' class='<%=dispClass%>' valign='top'>
						<a href='../../showUser.do?userId=<c:out
						value="${tranAction.tranCreateUserid}"/>'
						onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out
						value="${tranAction.tranCreateUserid}"/>\')'">
						<bean:write name="tranAction" property="tranCreateUserid"/> &nbsp; </a></td>
				</logic:notEqual>

				<td nowrap='nowrap' class='<%=dispClass%>' valign='top'>
					<bean:write name="tranAction" property="tranReasDesc"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>' valign='top' align='center'>
					<bean:write name="tranAction" property="tranOldStatCode"/>/
					<bean:write name="tranAction" property="tranOldSubStatCode"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>' valign='top' align='center'>
					<bean:write name="tranAction" property="tranStatCode"/>/
					<bean:write name="tranAction" property="tranSubStatCode"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>' valign='top' align='left'>
				<logic:notEmpty name="tranAction" property="subReasonCode">
						<bean:write name="tranAction" property="tranSubReasDesc"/> -
						<bean:write name="tranAction" property="subReasonCode"/>
				</logic:notEmpty>
					&nbsp;
				</td>
				<td nowrap='nowrap' class='<%=dispClass%>' valign='top'>
					<bean:write name="tranAction" property="tranConfId"/> &nbsp; </td>
				<td nowrap='nowrap' class='<%=dispClass%>' valign='top' align='right'> &nbsp;
					<bean:write name="tranAction" property="tranPostAmt"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>' valign='top'> &nbsp;
					<bean:write name="tranAction" property="tranDrGlAcctDesc"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>' valign='top'> &nbsp;
					<bean:write name="tranAction" property="tranCrGlAcctDesc"/></td>
			<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL";
				else dispClass = "TD-SHADED-SMALL"; %>
			</logic:iterate>
		</tbody>
	</table>


	<logic:equal name="tranAuth" property="manualCorrectingEntry" value="true">
		To manually enter a correcting journal entry, click
		<a href='../../manualCorrectingEntry.do?tranId=<bean:write
		name="transactionDetailForm" property="tranId" />&first=y'>here</a>.
	</logic:equal>


</logic:notEmpty>

<!-- ===============  tran comments  ===================================-->

<h3><fmt:message key="label.Transaction.comments"/></h3>

	<table border='0' align='center' cellpadding='2' cellspacing='1'>
<logic:iterate id="comment" name="comments">
		<tr>
			<td nowrap='nowrap' class='TD-SMALL' valign='top'>
				<logic:equal name="comment" property="createUserId" value="emgwww">
					<b><bean:write name="comment" property="createUserId" /></b>
				</logic:equal>
				<logic:notEqual name="comment" property="createUserId" value="emgwww">
				<a href='../../showUser.do?userId=<c:out
						value="${comment.createUserId}"/>'
						onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out
						value="${comment.createUserId}"/>\')'">
						<b><bean:write name="comment" property="createUserId" /></b></a>
				</logic:notEqual>
			</td>
			<td nowrap='nowrap' class='TD-SMALL' valign='top'><b><fmt:formatDate value="${comment.date}" pattern="dd/MMM/yyyy hh:mm a z" /></b></td>
			<td nowrap='nowrap' class='TD-SMALL' valign='top'><bean:write name="comment" property="tranCmntReasCode"
				/> (<bean:write name="comment" property="cmntReasDesc" />)</td>
			<td class='TD-SMALL' valign='top' ><c:out value="${comment.cmntFormattedText}" escapeXml="false" ></c:out>
			</td>
		</tr>
</logic:iterate>
	</table>
	<br />
<center>
	<table border='0' align='center' cellpadding='2' cellspacing='1' width='600'>

		<tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.comment.reason"/>:</b></td>
			<td colspan='2' align='left'>
				<html:select property="tranCmntReasCode" size="1">
					<html:options collection="reasons"
						property="emgTranCmntReasCode" labelProperty="cmntReasDesc" />
				</html:select>
			</td>
		</tr>
		<tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.transaction"/><br/><fmt:message key="label.comment"/>:</b></td>
			<td nowrap='nowrap' class='TD-SMALL'><html:textarea property="cmntText" rows="4" cols="80"
				onkeypress="textCounter(this, 255);" /></td>
			<td nowrap='nowrap' class='TD-SMALL'><html:submit property="submitAddComment" value="Add Comment" /></td>
		</tr>
		<tr>
			<td> &nbsp; </td>
			<td colspan="2"><b><font color="red"><fmt:message key="label.add.comment.note"/></font></b></td>
		</tr>
	</table>
</center>
</html:form>



	<div class="writeOffLossDiv">
		<html:form action="/transactionDetail.do" styleClass="recoveryLossForm">
			<div class="recoveryLossFormError">
				Please use a valid number.
			</div>
			<input type="hidden" name="tranId" value="<bean:write name="transactionDetailForm" property="tranId" />" />
			<fieldset>
				<label for="amount">Amount recovered:</label><br/>
				<input type="text" name="amount" class="recoveryLossAmount" />
			</fieldset>
			<fieldset>
				<label for="fee">Recovery Fee:</label> <br/>
				<input type="text" name="fee" class="recoveryLossFee" />
			</fieldset>
			<fieldset>
				<a href="#" class="writeOffLossCancel">Cancel</a>&nbsp;
				<input type="submit" class='' name='telecheckAction' value='Submit' />
			</fieldset>
		</html:form>
	</div>

	<div class="partialRefundDiv">
		<html:form action="/transactionDetail.do" styleClass="partialRefundForm">
			<div class="partialRefundAmountFormError">
				Please use a valid number.
			</div>
			<div class="partialRefundSelectionFormError">
				Please choose a valid Debit Account.
			</div>
			<input type="hidden" name="tranId" value="<bean:write name="transactionDetailForm" property="tranId" />" />
			<fieldset>
			<legend class="legend"><fmt:message key="label.partial.refund"></fmt:message> </legend>
				<table border='0' cellpadding='1' cellspacing='1'>
					<tbody>
						<tr>
							<td class='td-header' align='center'><fmt:message
								key="label.debit.account" /></td>
							<td>&nbsp; &nbsp;</td>
							<td class='td-header' align='center' width="100px">&nbsp;<fmt:message
								key="label.credit.account" />&nbsp;</td>
							<td>&nbsp; &nbsp;</td>
							<td class='td-header' align='center'><fmt:message
								key="label.amount" /></td>
						</tr>
						<tr>
							<td nowrap='nowrap' class='td-input'>
								<html:select styleClass="partialRefundDebitAccount" name="transactionDetailForm" property="debitAcct" size="1">
									<html:option value="0">
										<fmt:message key="label.select.a.debit.account" />
									</html:option>	
									<html:options collection="glAccounts" property="glAccountId" labelProperty="glAccountDesc" />
								</html:select>
							</td>
							<td>&nbsp; &nbsp;</td>
							<td nowrap='nowrap' class='td-label-normal' align="center">
								Cash Only
							</td>
							<td>&nbsp; &nbsp;</td>
							<td nowrap='nowrap' class='td-input'>
								<input type="text" name="refundAmount" class="partialRefundAmount" />
							</td>
						</tr>
					</tbody>
				</table>
			</fieldset>
			<fieldset>
				<input type="submit" class='' name='submitCcPartialRefund' value='Submit Partial Refund' /> &nbsp;
				<a href="#" class="partialRefundCancel">Cancel/Exit</a>
			</fieldset>
		</html:form>
	</div>
	
	<script type="text/javascript" language="JavaScript">
		$(document).ready(function(){
			$('.recoveryLossForm .recoveryLossFormError').hide();


			$('.writeOffLossTrigger').click(function(e){
				e.preventDefault();
				$.EMTbox(".writeOffLossDiv")
				$('.recoveryLossForm').submit(function(e){
					var regExp=/^\d+(\.\d{1,2})?$/;
					if (!regExp.test($('.recoveryLossForm .recoveryLossAmount').val()) || !regExp.test($('.recoveryLossForm .recoveryLossFee').val())){
						$('.recoveryLossForm .recoveryLossFormError').show();
						$.EMTbox.EMTboxResize()
						return false;
					}else{
						$('.recoveryLossForm .recoveryLossFormError').hide();
						$.EMTbox.EMTboxResize()
						return true;
					}
				});
				$('.writeOffLossCancel').click(function(e){
					e.preventDefault();
					$.EMTbox.close()
				});

		});
		
		$('.partialRefundForm .partialRefundAmountFormError').hide();
		$('.partialRefundForm .partialRefundSelectionFormError').hide();

		
		$('.partialRefundTrigger').click(function(e){
			e.preventDefault();
			$.EMTbox(".partialRefundDiv")
			$('.partialRefundForm').submit(function(e){
				var regExp=/^\d+(\.\d{1,2})?$/;
				if (!regExp.test($('.partialRefundForm .partialRefundAmount').val())){
				    $('.partialRefundForm .partialRefundSelectionFormError').hide();
					$('.partialRefundForm .partialRefundAmountFormError').show();
					$.EMTbox.EMTboxResize()
					return false;
				}
				else if ($('.partialRefundForm .partialRefundDebitAccount').val() == 0){
				    $('.partialRefundForm .partialRefundAmountFormError').hide();
					$('.partialRefundForm .partialRefundSelectionFormError').show();
					$.EMTbox.EMTboxResize()
					return false;
				}
				else{
					$('.partialRefundForm .partialRefundAmountFormError').hide();
					$('.partialRefundForm .partialRefundSelectionFormError').hide();
					$.EMTbox.EMTboxResize()
					return true;
				}
			});
			$('.partialRefundCancel').click(function(e){
				e.preventDefault();
				$.EMTbox.close()
				});
			});
		});

	</script>



