<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/datePicker.js" language="JavaScript"></script>
<script src="../../Display/jscript/setOtherRangeValue.js" language="JavaScript"></script>
<script src="../../Display/jscript/showUser.js" language="JavaScript"></script>

<CENTER>
<span class="error">
	<html:errors />
</span>

</CENTER>

<html:form action="/showChargebackQueue.do" focus="submitRefresh">

<h3><fmt:message key="label.chargeback.queue.list"/></h3>


<table>
	<tr>
		<td nowrap='nowrap' class='TD-SMALL'>
			<b><fmt:message key="label.date.begin"/><fmt:message key="label.suffix"/></b>&nbsp;
			<html:text size="10" property="beginDateText" onchange="setOtherRangeValue(this, showChargebackQueueForm.endDateText);" />
			<img border="0" src="../../images/calendar.gif" onclick="show_calendar('showChargebackQueueForm.beginDateText');" onmouseover="window.status='Pick a Date';return true;" onmouseout="window.status='';return true;"/>
		</td>	
		<td width="20">
			&nbsp;
		</td>

		<td nowrap='nowrap' class='TD-SMALL'>
			<b><fmt:message key="label.date.end"/><fmt:message key="label.suffix"/></b>&nbsp;
			<html:text size="10" property="endDateText" onchange="setOtherRangeValue(this, showChargebackQueueForm.beginDateText);" />
		 	<img border="0" src="../../images/calendar.gif" onclick="show_calendar('showChargebackQueueForm.endDateText');" onmouseover="window.status='Pick a Date';return true;" onmouseout="window.status='';return true;"/>
		</td>		 	
		<td width="20">
			&nbsp;
		</td>
	
		<td nowrap='nowrap' class='TD-SMALL'>
	 		<b><fmt:message key="label.chargeback.status" />: </b>
			<html:select name="showChargebackQueueForm" property="status" size="1">
					<html:option value="ALL"><fmt:message key="option.all"/></html:option>												
					<html:option value="OPN"><fmt:message key="option.open.all"/></html:option>												
					<html:options collection="chargebackStatuses" property="chargebackStatusCode" labelProperty="chargebackStatusDesc" />
			</html:select>
		</td>
	</tr>
	
	<tr>
		<td nowrap='nowrap' class='TD-SMALL'>
			<b><fmt:message key="label.sort.by" />: </b>
			<html:select name="showChargebackQueueForm" property="sortBy" size="1">
				<html:option value="tranId"><fmt:message key="label.tran.id"/></html:option>
				<html:option value="chargebackStatusDesc"><fmt:message key="label.chargeback.status"/></html:option>
				<html:option value="chargebackStatusDate"><fmt:message key="label.chargeback.statusdate"/></html:option>		
				<html:option value="chargebackTrackingDate"><fmt:message key="label.chargeback.trackingdate"/></html:option>
				<html:option value="createDate"><fmt:message key="label.chargeback.createdate"/></html:option>
				<html:option value="createUserId"><fmt:message key="label.chargeback.createid"/></html:option>
			</html:select> 
		</td>		
		<td width="20">
			&nbsp;
		</td>
		<td>
			<html:select name="showChargebackQueueForm" property="sortSeq" size="1">
				<html:option value="A"><fmt:message key="option.ascending"/></html:option>
				<html:option value="D"><fmt:message key="option.descending"/></html:option>
			</html:select>
		</td>
		<td width="20">
			&nbsp;
		</td>
		<td>
			&nbsp;
		</td>
	</tr>
	</table>

	<%String dispClass = "TD-SHADED-SMALL"; %>
	<table>
	<tr><td nowrap='nowrap' class='TD-SMALL'>
		<div id="formButtons">
			<b><font color="#CF002F">
			<fmt:message key="label.total.records" /> : 
			<bean:write name="showChargebackQueueForm" property="recordCount" /></font><b>
			&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
			<input type='submit' class='transbtn2' name='submitRefresh' value='Refresh Queue' 
				onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
		</div>
	
		<div id="processing" style="visibility: hidden">
			<span class="PROCESSING"><fmt:message key="message.in.process" /></span>
		</div >
	
	</td></tr>
	</table>


	<logic:notEmpty name="chargebackQueue">
	<table>
		<tbody>
			<tr>
			<td  nowrap='nowrap' class='TD-SMALL' colspan='11' align='right'><a href="../../ExcelReport.emt?id=chargebackQueue" target="excel">
				<fmt:message key="msg.view.or.download.excel" /></a></td>
			</tr>
			<tr>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.chargeback.status"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.chargeback.tranid"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.chargeback.statusdate"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.chargeback.statusdate.age"/></TD>			
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.chargeback.trackingdate"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.chargeback.createdate"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.chargeback.createid"/></TD>

			</tr>

			<logic:iterate id="chargebackHeader" name="chargebackQueue">

			<tr>
				<td nowrap='nowrap' class='<%=dispClass%>'><bean:write filter="false" name="chargebackHeader" property="chargebackStatusDesc"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<a href='../../transactionDetail.do?tranId=<bean:write filter="false" name="chargebackHeader" property="tranId"/>'>
					<bean:write filter="false" name="chargebackHeader" property="tranId"/>
					</a>&nbsp;-&nbsp;
					<a href='../../ccChargebackTracking.do?tranId=<bean:write filter="false" name="chargebackHeader" property="tranId"/>'>
					<fmt:message key="label.chargeback.detail"/>
					</a>
				
				</td>


				<td nowrap='nowrap' class='<%=dispClass%>'><bean:write filter="false" name="chargebackHeader" property="chargebackStatusDate"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>'><bean:write filter="false" name="chargebackHeader" property="statusAgeInDays"/> - Day(s)
				<td nowrap='nowrap' class='<%=dispClass%>'><bean:write filter="false" name="chargebackHeader" property="chargebackTrackingDate"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>'><bean:write filter="false" name="chargebackHeader" property="createDate"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>'><bean:write filter="false" name="chargebackHeader" property="createUserId"/></td>
				
				</tr>
			<%  if (dispClass.equals("TD-SHADED-SMALL")) {
					dispClass = "TD-SMALL"; 
				} else {
					dispClass = "TD-SHADED-SMALL";
				}  %>
			</logic:iterate>


		</tbody>
	</table>
	</logic:notEmpty>

</html:form>
