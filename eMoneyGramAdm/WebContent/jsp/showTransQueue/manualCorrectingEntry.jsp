<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/textCounter.js" language="JavaScript"></script>
<center>

<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

</center>

<h3>
<fmt:message key="label.manual.correcting.entry" />
</h3>
<br/>

<html:form action="/manualCorrectingEntry">

	<fmt:message key="label.transaction.id"/><fmt:message key="label.suffix"/>
	&nbsp; &nbsp;
	<html:hidden property="tranId" /><b>
	<c:out value="${manualCorrectingEntryForm.tranId}" /></b>

<!--       ===========  begin of tran actions  =======================       -->

<logic:notEmpty name="tranActions">
<h3><fmt:message key="label.transaction.action.history" /></h3>
	<table border='1' width='100%' cellpadding='1' cellspacing='1'>
		<tbody>
		<% String dispClass = "TD-SMALL";%>
			<tr>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.date" /></td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.userid" /></td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.desc" /></td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.from.status" /></td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.from.sub.status" /></td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.to.status" /></td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.to.sub.status" /></td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.cc.auth.id" /></td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.post.amt" /></td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.dr.acct.desc" /></td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.cr.acct.desc" /></td>				
			</tr>
			<logic:iterate id="tranAction" name="tranActions">
			<tr>
				<td nowrap='nowrap' class='<%=dispClass%>' valign='top'>
					<bean:write name="tranAction" property="tranActnDate"/></td>

				<logic:equal  name="tranAction" property="tranCreateUserid" value="emgwww">
					<td nowrap='nowrap' class='<%=dispClass%>' valign='top'>
						<bean:write name="tranAction" property="tranCreateUserid"/> &nbsp; </td>
				</logic:equal>
				<logic:notEqual  name="tranAction" property="tranCreateUserid" value="emgwww">
					<td nowrap='nowrap' class='<%=dispClass%>' valign='top'>
						<a href='../../showUser.do?userId=<c:out 
						value="${tranAction.tranCreateUserid}"/>' 
						onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out 
						value="${tranAction.tranCreateUserid}"/>\')'">
						<bean:write name="tranAction" property="tranCreateUserid"/> &nbsp; </a></td>
				</logic:notEqual>

				<td nowrap='nowrap' class='<%=dispClass%>' valign='top'>
					<bean:write name="tranAction" property="tranReasDesc"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>' valign='top' align='center'>
					<bean:write name="tranAction" property="tranOldStatCode"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>' valign='top' align='center'>
					<bean:write name="tranAction" property="tranOldSubStatCode"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>' valign='top' align='center'>
					<bean:write name="tranAction" property="tranStatCode"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>' valign='top' align='center'>
					<bean:write name="tranAction" property="tranSubStatCode"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>' valign='top'>
					<bean:write name="tranAction" property="tranConfId"/> &nbsp; </td>
				<td nowrap='nowrap' class='<%=dispClass%>' valign='top' align='right'> &nbsp;
					<bean:write name="tranAction" property="tranPostAmt"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>' valign='top'> &nbsp;
					<bean:write name="tranAction" property="tranDrGlAcctDesc"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>' valign='top'> &nbsp;
					<bean:write name="tranAction" property="tranCrGlAcctDesc"/></td>
			<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
				else dispClass = "TD-SHADED-SMALL"; %>
			</logic:iterate>
		</tbody>			      
	</table>
<br />

</logic:notEmpty>

	<table border='0' cellpadding='1' cellspacing='1'>
		<tbody>
			<tr>
				<td nowrap='nowrap' class='td-header' align='center'>
					<fmt:message key="label.debit.account" /></td>
				<td> &nbsp; &nbsp; </td>
				<td nowrap='nowrap' class='td-header' align='center'>
					<fmt:message key="label.credit.account" /></td>
				<td> &nbsp; &nbsp; </td>
				<td nowrap='nowrap' class='td-header' align='center'>
					<fmt:message key="label.amount" /></td>
			</tr>
			<tr>
				<td nowrap='nowrap' class='td-input'>	
					<html:select name="manualCorrectingEntryForm" property="debitAcct" size="1">
						<html:option value="0">
							<fmt:message key="label.select.a.debit.account" />
							</html:option>												
						<html:options collection="glAccounts" property="glAccountId" labelProperty="glAccountDesc" />
					</html:select></td>
				<td> &nbsp; &nbsp; </td>
				<td nowrap='nowrap' class='td-input'>	
					<html:select name="manualCorrectingEntryForm" property="creditAcct" size="1">
						<html:option value="0">
							<fmt:message key="label.select.a.credit.account" />
							</html:option>												
						<html:options collection="glAccounts" property="glAccountId" labelProperty="glAccountDesc" />
					</html:select></td>
				<td> &nbsp; &nbsp; </td>
				<td nowrap='nowrap' class='td-input'>
					<html:text property="amount" /></td>
			</tr>
		</tbody>
	</table>

	<br />

	<table border='0' cellpadding='2' cellspacing='1'>
		<tr>
			<td nowrap='nowrap' class='td-header' align='center'>
				<fmt:message key="label.comment"/>
			</td>
		</tr>
	
		<tr>
			<td nowrap='nowrap' class='TD-SMALL'><html:textarea property="cmntText" rows="4" cols="80" 
				onkeypress="textCounter(this, 255);" /></td>			
		</tr>
	</table>		
	
	<div id="formButtons">
		<html:submit property="submitCorrection" value="Add Correcting Entry" 
			onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
			&nbsp; &nbsp; &nbsp;
		<html:submit property="submitCancel" value="Cancel/Exit" />
	</div>
	
	<div id="processing" style="visibility: hidden">
		<table>
			<tr>
				<td class="PROCESSING"><fmt:message key="message.in.process" /></td>
			</tr>
		</table>
	</div >

</html:form>

