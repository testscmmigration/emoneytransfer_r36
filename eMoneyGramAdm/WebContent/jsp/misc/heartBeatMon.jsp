<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html:form action="/heartBeat.do">

<h3><fmt:message key="label.build.date" />: &nbsp; &nbsp; 
	<bean:write name="heartBeatMonForm" property="buildDate"/></h3>

	<table border='0' cellpadding='1' cellspacing='1'>
		<tbody>
			
		<%String dispClass = "TD-SMALL";%>
		<logic:notEmpty name="systems">
			<tr>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.system.key" /></TD>		
				<td> &nbsp; </td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.system.status" /></TD>
				<td> &nbsp; </td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.exception.name" /></TD>
				<td> &nbsp; </td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.exception.message" /></TD>
			</tr>
			<logic:iterate id="system" name="systems">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>' valign='top'>
						<b><bean:write filter="false" name="system" property="systemKey" /></b></td>
					<td> &nbsp; </td>
					<logic:equal name="system" property="systemStatus" value="Success">
						<td nowrap='nowrap' class='<%=dispClass%>' valign='top'>
							<bean:write filter="false" name="system" property="systemStatus"/></td>
					</logic:equal>
					<logic:equal name="system" property="systemStatus" value="Fail">
						<td nowrap='nowrap' style="background-color: #00FFFF" valign='top'>
							<bean:write filter="false" name="system" property="systemStatus"/></td>
					</logic:equal>
					<td> &nbsp; </td>
					<td class='<%=dispClass%>' valign='top'>
						<bean:write filter="false" name="system" property="exceptionClassName"/></td>
					<td> &nbsp; </td>
					<td class='<%=dispClass%>' valign='top'>
						<bean:write filter="false" name="system" property="exceptionMsg"/></td>
				<tr>
			<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
				else dispClass = "TD-SHADED-SMALL"; %>
			</logic:iterate>
		</logic:notEmpty>
		</tbody>
	</table>
	
<br />
	
	<table border='0' cellpadding='1' cellspacing='1'>
		<tbody>

		<logic:notEmpty name="props">
		
		<% dispClass = "TD-SMALL";%>
			<tr>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.property.key" /></TD>		
				<td> &nbsp; </td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.property.type" /></TD>
				<td> &nbsp; </td>
				<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.property.value" /></TD>
			</tr>
			<logic:iterate id="prop" name="props">
				<logic:notEqual name="prop" property="propValue" value="">
					<tr>
						<td nowrap='nowrap' class='<%=dispClass%>' align='right' valign='top'>
							<b><bean:write filter="false" name="prop" property="propKey" /></b></td>
					<td> &nbsp; </td>
					<td nowrap='nowrap' class='<%=dispClass%>' valign='top'>
						<bean:write filter="false" name="prop" property="propType"/></td>
					<td> &nbsp; </td>
					<td nowrap='nowrap' class='<%=dispClass%>' valign='top'>
						<bean:write filter="false" name="prop" property="propValue"/></td>
					<tr>
				</logic:notEqual>
				<logic:equal name="prop" property="propValue" value="">
					<tr>
						<td nowrap='nowrap' class='<%=dispClass%>' align='right' valign='top'>
							<b><bean:write filter="false" name="prop" property="propKey" /></b></td>
					<td> &nbsp; </td>
					<td nowrap='nowrap' class='<%=dispClass%>' valign='top'>
						<bean:write filter="false" name="prop" property="propType"/></td>
						<td> &nbsp; </td>
						<td style="background-color: #FF1493">****  Not Define  ****</td>
					<tr>
				</logic:equal>
			<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
				else dispClass = "TD-SHADED-SMALL"; %>
			</logic:iterate>
		</logic:notEmpty>
		</tbody>			      
	</table>

</html:form>
