<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<h3>Quick Search Error</h3>
<br />
<br />

<h3>
	<%
		java.util.Map<String, String> transferMap = (java.util.Map<String, String>) request
				.getSession().getAttribute("errDetail");
				
				String errMsg=null;
		
		if (transferMap != null)
			if (transferMap.containsKey("errMsg"))
				errMsg = transferMap.get("errMsg");
				pageContext.setAttribute("errMsg",errMsg);
	%>
		
	<c:out value="${fn:escapeXml(errMsg)}"/></h3>

<%
	request.getSession().removeAttribute("errDetail");
	request.getSession().removeAttribute("errCust");
%>