<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<h3>
	<fmt:message key="exception.unexpected"/>
<br />
</h3>

<br />
<b>

<%-- Please print this screen and submit it to the system support team.--%>
We are unable to process the request at this time.
</b>
<br /><br />
<a href="javascript:self.close();" >Return to the application.</a>
<br /><br />
<%-- Commented the below code in order to avoid displaying the Stack trace information to the user --%>
<%-- <logic:notEmpty name="exceptionMessages">
	<table>
		<logic:iterate id="exceptionMessage" name="exceptionMessages">
		<tr>
			<td nowrap='nowrap' class='ERROR' valign='top' align='right'>
				<bean:write name="exceptionMessage" property="msgKey" /></td>
			<td> &nbsp; </td>
			<td class='ERROR' valign='top'>
				<bean:write name="exceptionMessage" property="msgText" /></td>
		</tr>
		</logic:iterate>
	</table>
</logic:notEmpty> --%>
<br />



