<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html:form action="/viewRoles">
	<TABLE border="0">
		<TBODY>
			<TR>
				<TD nowrap="nowrap" class="TD-LABEL">
				Select Role:
					<html:select property="roleId" size="1">
						<c:if test="${true}" scope="request" var="userProfile.internalUser" >
  						   <bean:define id="roles" name="viewRolesForm" property="roles" scope="request" />
						    <html:options collection="roles" labelProperty="displayName" property="id"/>
					    </c:if>
						<c:if test="${false}" scope="request" var="userProfile.internalUser" >
							<bean:define id="externalRoles" name="viewRolesForm" property="externalRoles" scope="request" />
						    <html:options collection="externalRoles" labelProperty="description" property="id"/>
					    </c:if>
	
	                </html:select>
	                <html:submit property="Go" value="Go"/>
				</TD>

			</TR>
			</TBODY>
	</TABLE>

			<ul>
	    	</ul>
	    	
	    	<CENTER>
	    	

<P>
<c:if test="${viewRolesForm.role != null}">
	<%String dispClass = "TD-SMALL";%>
	<table border='0' cellpadding='2' cellspacing='1' width='100%'>
		<TR>
		<td nowrap='nowrap' class='TD-HEADER-SMALL'>
			Allowed Commands
		</TD>
		</TR>
		<logic:iterate id="desc" name="descList">
		<tr>
			<td nowrap='nowrap' class='<%=dispClass%>'><bean:write name="desc" /></td>
		</tr>
		<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
			else dispClass = "TD-SHADED-SMALL"; %>	
		</logic:iterate>
	</table>
</c:if>
<br>
<html:button property="close" value="Close Window" onclick="window.close()"/>
</CENTER>
</html:form>	    	

