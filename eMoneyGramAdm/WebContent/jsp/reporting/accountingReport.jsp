<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/datePicker.js" language="JavaScript"></script>
<script src="../../Display/jscript/setOtherRangeValue.js" language="JavaScript"></script>

<CENTER>
<span class="error">
	<html:errors />
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>


</CENTER>

<h3><fmt:message key="label.report.accounting"/></h3>

<center>
<html:form action="/accountingReport.do" focus="emgTranId">
	<fieldset>
		<legend class="TD-LABEL"><fmt:message key="label.accounting.search"/></legend>

		<table border='0' width='80%' cellpadding='1' cellspacing='1'>

				<tr>
					<td align='right' nowrap="nowrap" class="TD-LABEL"><fmt:message key="label.trans.id"/>: 
					</td>
					<td colspan='4'><html:text size="9" maxlength="9" property="emgTranId" />
						<b><fmt:message key="label.accounting.search.override.msg"/></b>			
					</td>		
				</tr>
			 	<tr>
					<td align="center" colspan='5'>
						<b>or</b>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap" class="TD-LABEL">
						<fmt:message key="label.date.begin"/>
						<fmt:message key="label.suffix"/> &nbsp;
					</td>
					<td nowrap="nowrap">
						<html:text 
							size="10" 
							property="beginDateText" 
							onchange="setOtherRangeValue(this, journalEntryForm.endDateText);" 
						/>
						<img border="0" src="../../images/calendar.gif" onclick="show_calendar('journalEntryForm.beginDateText');" onmouseover="window.status='Pick a Date';return true;" onmouseout="window.status='';return true;"/>
						<span class="error"><html:errors property='beginDateText'/></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					
					<td nowrap="nowrap" class="TD-LABEL">
						<fmt:message key="label.date.end"/>
						<fmt:message key="label.suffix"/>&nbsp;
					</td>
					<td nowrap="nowrap">
						<html:text 
							size="10" 
							property="endDateText" 
							onchange="setOtherRangeValue(this, journalEntryForm.endDateText);" 
						/>
						<img border="0" src="../../images/calendar.gif" onclick="show_calendar('journalEntryForm.endDateText');" onmouseover="window.status='Pick a Date';return true;" onmouseout="window.status='';return true;"/>
						<span class="error"><html:errors property='endDateText'/></span>
					</td>
			</tr>
			<tr>
				<td align="left" colspan='5'>
					&nbsp;
				</td>
			</tr>
	
			<tr>
				<td nowrap="nowrap" class="TD-LABEL" colspan='2' align='right'>
					<fmt:message key="label.sort.report.by"/>: &nbsp;
				</td>
				<td nowrap="nowrap">
					<html:select name="journalEntryForm" property="sortBy" size="1">
						<html:option value="entryDate">Date</html:option>		
						<html:option value="userId">Profile Id</html:option>												
						<html:option value="senderFirstName">Sender Name</html:option>												
						<html:option value="tranId">Transaction Id</html:option>												
						<html:option value="tranReasonDescription">Action Description</html:option>												
						<html:option value="postAmount">Debit/Credit Amount</html:option>												
					</html:select>				
				</td>
				<td colspan='2'> &nbsp; </td>
			</tr>
			<tr>
				<td align="left" colspan='5'>
					&nbsp;
				</td>
			</tr>
			
		</table>
			
	<div id="formButtons">
		<html:submit property="submitSearch" value="Search" 
			onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
	</div >
			
	<div id="processing"  style="visibility: hidden">
		<span class="PROCESSING"><fmt:message key="message.in.process" /></span>
	</div >				
		
	</fieldset>

<logic:notEmpty name="JournalEntries">
	<%String dispClass = "TD-SMALL";%>	

	&nbsp;
	
	<fieldset>
		<legend class="TD-LABEL"><fmt:message key="label.accounting.report"/></legend>


		<table>
			<tbody>
				<tr><td colspan="11" align="right"><a href="../../ExcelReport.emt?id=JournalEntries" target="excel">
					<fmt:message key="msg.view.or.download.excel" /></a>
					</td></tr>
				<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.date.time"/></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.report.adhoc.profileid"/></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.accounting.actiondescription"/></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.trans.id"/></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sender.name"/></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.accounting.debit.desc"/></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.accounting.credit.desc"/></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.post.amount"/></TD>		
				</tr>
				
				<logic:iterate id="journalEntry" name="JournalEntries">				
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<fmt:formatDate value="${journalEntry.entryDate}" pattern="dd/MMM/yyyy HH:mm:ss" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="journalEntry" property="userId"/>
					</td>

					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="journalEntry" property="tranReasonDescription"/>
					</td>

					<td nowrap='nowrap' class='<%=dispClass%>'>
						<a href='../../transactionDetail.do?tranId=<bean:write filter="false" name="journalEntry" property="tranId"/>'>
						<bean:write filter="false" name="journalEntry" property="tranId"/>
						</a>
					</td>

					<td nowrap='nowrap' class='<%=dispClass%>'>
							<bean:write name="journalEntry" property="senderFirstName"/>
							&nbsp;
							<bean:write name="journalEntry" property="senderLastName"/>						
					</td>
					
					<td nowrap='nowrap' class='<%=dispClass%>' align='Right'>
					    <logic:equal name="journalEntry" property="debitAccountId" value="null">
							&nbsp;
					    </logic:equal>
					    <logic:notEqual name="journalEntry" property="debitAccountId" value="null">
							<bean:write filter="false" name="journalEntry" property="debitAccountDesc"/>
					    </logic:notEqual>
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='Right'>
					    <logic:equal name="journalEntry" property="creditAccountId" value="null">
							&nbsp;
					    </logic:equal>
					    <logic:notEqual name="journalEntry" property="creditAccountId" value="null">
							<bean:write filter="false" name="journalEntry" property="creditAccountDesc"/>
					    </logic:notEqual>
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='Right'>
						<bean:write format="#,##0.00" filter="false" name="journalEntry" property="postAmount"/>
					</td>
					
				</tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>
				</logic:iterate>
				
			</tbody>			      
		</table>


	</fieldset>
</logic:notEmpty>		
</html:form>	
