<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/textCounter.js" language="JavaScript"></script>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>

<script language="javascript">
	function setVisibility(id, visible) { 
    	var o = getElemById(o); 
	    if (o) { 
    	    if (typeof o.style != 'undefined') { 
        	    o.style.visibility = (visible ? 'visible' : 'hidden'); 
            	return (o.style.visibility == (visible ? 'visible' : 'hidden')); 
	        } else { 
    	        o.visibility = (visible ? 'show' : 'hide'); 
        	    return (o.visibility == (visible ? 'show' : 'hide')); 
	        } 
    	} 
	    return false; 
} 
</script>

</center>

<html:form action="/addIPBlockedList.do" focus="ipAddress1">

<h3><fmt:message key="label.blockedlist.ipaddress.title" /></h3>

	<html:radio  name="addIPBlockedListForm" property="singleIPSelect" value="0"
		onclick="ipRange.style.visibility='hidden';"><fmt:message key="label.blockedlist.ipaddress.singleip" /></html:radio>&nbsp; &nbsp;
	<html:radio  name="addIPBlockedListForm" property="singleIPSelect" value="1"
		onclick="ipRange.style.visibility='visible';"><fmt:message key="label.blockedlist.ipaddress.rangeip" /></html:radio>&nbsp; &nbsp;
	
	<BR><BR>
	<table border="0" width="100%">
	<tr>
		<td width="100" valign="top"><fmt:message key="label.blockedlist.ipaddress.ipaddress" /> </td>
		<td>
			<html:text name="addIPBlockedListForm" property="ipAddress1" size="15" maxlength="15" />

			<logic:equal name="addIPBlockedListForm" property="singleIPSelect" value="1">
				<div id="ipRange"  style="visibility: visible">
			</logic:equal>
				
			<logic:equal name="addIPBlockedListForm" property="singleIPSelect" value="0">
				<div id="ipRange"  style="visibility: hidden">
			</logic:equal>

				to<br>
				<html:text name="addIPBlockedListForm" property="ipAddress2" size="15" maxlength="15" />
			</div >			
			
		</td>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td width="100"><fmt:message key="label.blockedlist.ipaddress.risklevel" /></td>
		<td colspan="2">
			<html:text name="addIPBlockedListForm" property="riskLevel" size="3" maxlength="3" />
		</td>
	</tr>
	<tr>
		<td width="100"><fmt:message key="label.blockedlist.ipaddress.action" /></td>
		<td colspan="2">
			<html:select name="addIPBlockedListForm" property="blockingAction" size="1">
				<html:option value="None"><fmt:message key="option.select.one"/></html:option>												
				<html:options collection="blockingActions" property="blockedStatusCode" labelProperty="blockedStatusDesc" />
			</html:select>		
		</td>
	</tr>
	<tr>
		<td width="100"><fmt:message key="label.blockedlist.ipaddress.reason" /></td>
		<td colspan="2">
			<html:select name="addIPBlockedListForm" property="blockingReason" size="1">
				<html:option value="None"><fmt:message key="option.select.one"/></html:option>												
				<html:options collection="blockingReasons" property="reasonCode" labelProperty="reasonDesc" />
			</html:select>		
		</td>
	</tr>
	<tr>
		<td width="100" valign="top"><fmt:message key="label.blockedlist.ipaddress.comments" /></td>
		<td colspan="2">
			<html:textarea name="addIPBlockedListForm" property="blockingComment"  rows="4" cols="40" 
					onkeypress="textCounter(this, 128);" />					
		</td>
	</tr>
	<tr>
		<td width="100" valign="top">&nbsp;</td>
		<td colspan="2">
			<div id="formButtonsAdd">
				<html:submit property="submitSave" value="Process" 
					onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';"/>
				<html:submit property="submitCancel" value="Cancel" 
					onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';"/>			
			</div>
		</td>
	</tr>
	
	</table>
	

	<div id="processing" style="visibility:hidden" >
		<span class="PROCESSING"><fmt:message key="message.in.process" />
		</span>
	</div>

</html:form>
