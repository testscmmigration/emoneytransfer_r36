<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/showUser.js" language="JavaScript"></script>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>
</center>

<html:form action="/showPhoneBlockedList.do" focus="phNbr">

<h3><fmt:message key="label.blocked.phone.search.title" /></h3>
<br>
	<table border="0" width="100%">
	<tr>
		<td nowrap='nowrap' width="100%"><fmt:message key="label.phone.number" />
	        <fmt:message key="label.suffix" /> &nbsp;
			<html:text name="showPhoneBlockedListForm" property="phNbr" size="14" maxlength="14" />
			&nbsp; <fmt:message key="label.enter.1.to.14.digits" />
		</td>
	</tr>
	
	<tr>
		<td nowrap='nowrap'>
			<div id="formButtonsAdd">
				<html:submit property="submitSearch" value="Search" 
					onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';"/> &nbsp; &nbsp;
				<html:submit property="submitShowAll" value="Show All" 
					onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';"/>
			
				<logic:equal name="phoneAuth" value="Y">
					&nbsp; &nbsp;
					<html:submit property="submitNew" value="Add a New Blocked Phone"
							 onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';" />
				</logic:equal>
			</div>
		</td>
	</tr>
	
	</table>

	<div id="processing" style="visibility: hidden"><span
		class="PROCESSING"><fmt:message key="message.in.process" /></span></div>

	
	<logic:notEmpty name="blockedPhones">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.phone.number" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.block.reason" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.block.comment" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.block.status" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.fraud.risk.level" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.create.date" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.create.user" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.last.update.date" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.last.update.user" /></TD>
				<logic:equal name="phoneAuth" value="Y">
					<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
						<fmt:message key="label.action" /></TD>
				</logic:equal>
				</tr>

				<logic:iterate id="blockedPhone" name="blockedPhones">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedPhone" property="clearText"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedPhone" property="reasonDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedPhone" property="comment" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedPhone" property="statusCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedPhone" property="fraudRiskLevelCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedPhone" property="createDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
	               		<logic:notEqual name="blockedPhone" property="createUserId" value="">
							<a href='../../showUser.do?userId=<c:out 
							value="${blockedPhone.createUserId}"/>' 
							onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out 
							value="${blockedPhone.createUserId}"/>\')'">
							<b><bean:write name="blockedPhone" property="createUserId" /></b></a>
						</logic:notEqual>
             			<logic:equal name="blockedPhone" property="createUserId" value="">
            				<bean:write name="blockedPhone" property="createUserId" />
            			</logic:equal>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedPhone" property="statusUpdateDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
	               		<logic:notEqual name="blockedPhone" property="updateUserId" value="">
							<a href='../../showUser.do?userId=<c:out 
							value="${blockedPhone.updateUserId}"/>' 
							onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out 
							value="${blockedPhone.updateUserId}"/>\')'">
							<b><bean:write name="blockedPhone" property="updateUserId" /></b></a>
						</logic:notEqual>
             			<logic:equal name="blockedPhone" property="updateUserId" value="">
            				<bean:write name="blockedPhone" property="updateUserId" />
            			</logic:equal>
            		<logic:equal name="phoneAuth" value="Y">
            			<td nowrap='nowrap' class='<%=dispClass%>'>
						 	<a href='../../editPhoneBlockedList.do?phoneNumber=
							<bean:write filter="false" name="blockedPhone" 
							property="clearText"/>&action=edit'>Edit</a></td>
					</logic:equal>
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>

</html:form>
