<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page import="emgadm.blockedList.BlockedListCustomer" %>
<%@ page import="java.util.List" %>
<%@ page import="emgadm.blockedList.BlockedListCustomer" %>
<%@taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@page import="org.owasp.esapi.ESAPI"%>

<script src="../../Display/jscript/textCounter.js" language="JavaScript"></script>

<center>

<%List<BlockedListCustomer> affectedCustomers = (List<BlockedListCustomer>) request.getAttribute("blockedCustomerList"); 
 int customerListSize = affectedCustomers.size();
 String blockingComment = ESAPI.encoder().encodeForHTMLAttribute((String)request.getAttribute("blockingComment"));
%>



<html:form action="/saveBankBlockedList.do">
<fieldset class="fieldset1">
<c:if test="${affectedCustomers!=null}">
<%-- <fmt:message key="label.blockedlist.bank.consumer.list"/> <%= affectedCustomers.size(); %> --%>
</c:if>
<!-- <td> -->
<h3 style="margin: 50px 565px 3px 0;"><fmt:message key="label.blockedlist.bank.consumer.list"/>: <%=customerListSize %></h3> 
<!-- </td> -->
<table style="width: 66%; float: left; margin-left: 70px;">
		<tbody>
	       
	        <tr>
	            <td><fmt:message key="label.block.status" /></td>
				
	            <td><%= request.getAttribute("blockedStatusDescription") %></td>
	        </tr>
	        <tr>
	            <td><fmt:message key="label.block.reason"/></td>
	            <td><%= request.getAttribute("blockedReasonDescription") %></td>
	        </tr>
	        <tr>
	            <td width="120px"><fmt:message key="label.block.comment" /></td>
	            <td width="600px" style="word-wrap:break-word; display: inline-block;"><%= blockingComment %></td>
	        </tr>
	       
	        <tr>
	            <td colspan="2">&nbsp;</td>
			</tr>
 
		</tbody>
	</table>
	
<logic:notEmpty name="blockedCustomerList">
<%String dispClass = "TD-SMALL";%>
<br />
	<table style="margin-right:238px;">
	<tbody>
		<tr>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.consumer.id"/></th>		
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.login.id"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sub.status"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.first.name"/></th> 
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.last.name"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'>User Data Blocked or UnBlocked Data</th>
		</tr>
		<logic:iterate id="customer" name="blockedCustomerList">
		<tr>
				<td nowrap='nowrap' class='<%=dispClass%>'>
			    <a href='../../showCustomerProfile.do?custId=<bean:write 
			    name="customer" property="custId" />'
			    ><bean:write name="customer" property="custId" />
				</a></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="customer" property="loginId" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="customer" property="profileSubstatus" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="customer" property="firstName" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="customer" property="lastName" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="customer" property="displayData" /></td>
				
		</tr>
		<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
			else dispClass = "TD-SHADED-SMALL"; %>	
		</logic:iterate>
	</tbody>		      
</table>
</logic:notEmpty>	
	
	
	<input style="margin-right: 160px; margin-top: 30px;" type="button" value="OK" onclick="javascript:window.close();"/>
 </fieldset>
</html:form>

</center>
