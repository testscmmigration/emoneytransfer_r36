<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/textCounter.js" language="JavaScript"></script>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>

<script language="javascript">
	function setVisibility(id, visible) { 
    	var o = getElemById(o); 
	    if (o) { 
    	    if (typeof o.style != 'undefined') { 
        	    o.style.visibility = (visible ? 'visible' : 'hidden'); 
            	return (o.style.visibility == (visible ? 'visible' : 'hidden')); 
	        } else { 
    	        o.visibility = (visible ? 'show' : 'hide'); 
        	    return (o.visibility == (visible ? 'show' : 'hide')); 
	        } 
    	} 
	    return false; 
} 
</script>

</center>

<html:form action="/showBlockedList.do">

<h3><fmt:message key="label.blocked.list.title" /></h3>

<table border="0">
<tr>
	<th></th>
	<th></th>
	<th></th>
	<th><fmt:message key="label.block.reason" /></th>
	<th><fmt:message key="label.block.status" /></th>
</tr>

<tr>
	<th><html:checkbox name="addBlockedListForm" property="blockEmail"></html:checkbox></th>
    <td><fmt:message key="label.blockedlist.email.email" /></td>
    <th><c:out value="${blockEmailAddress}" /></th>
    <td width="140">
	    <html:select name="addBlockedListForm" property="blockingActionEML" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingActionEML" property="blockedStatusCode" labelProperty="blockedStatusDesc" />
		</html:select>
    </td>
    <td width="140">
    	<html:select name="addBlockedListForm" property="blockingReasonEML" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingReasonEML" property="reasonCode" labelProperty="reasonDesc" />
		</html:select>
    </td>
  </tr>
 
  <tr>
	<th><html:checkbox name="addBlockedListForm" property="blockPhone"></html:checkbox></th>
    <td><fmt:message key="label.phone.number" /></td>
    <th><c:out value="${blockPhoneNumber}" /></th>
    <td width="140">
	    <html:select name="addBlockedListForm" property="blockingActionPHON" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingActionPHON" property="blockedStatusCode" labelProperty="blockedStatusDesc" />
		</html:select>
    </td>
    <td width="140">
    	<html:select name="addBlockedListForm" property="blockingReasonPHON" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingReasonPHON" property="reasonCode" labelProperty="reasonDesc" />
		</html:select>
    </td>
  </tr>
  
  <tr>
	<th><html:checkbox name="addBlockedListForm" property="blockCC"></html:checkbox></th>
    <td><fmt:message key="label.blockedlist.cc.cc" /></td>
    <th><c:out value="${blockCC.accountNumberBin}******${blockCC.accountNumberMask}" /></th>
    <td width="140">
	    <html:select name="addBlockedListForm" property="blockingActionCC" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingActionCC" property="blockedStatusCode" labelProperty="blockedStatusDesc" />
		</html:select>
    </td>
    <td width="140">
    	<html:select name="addBlockedListForm" property="blockingReasonCC" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingReasonCC" property="reasonCode" labelProperty="reasonDesc" />
		</html:select>
    </td>
  </tr>

  <tr>
	<th><html:checkbox name="addBlockedListForm" property="blockBin"></html:checkbox></th>
    <td><fmt:message key="label.blockedlist.bin.bin" /></td>
    <th><c:out value="${blockCC.accountNumberBin}" /></th>
    <td width="140">
	    <html:select name="addBlockedListForm" property="blockingActionBIN" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingActionBIN" property="blockedStatusCode" labelProperty="blockedStatusDesc" />
		</html:select>
    </td>
    <td width="140">
    	<html:select name="addBlockedListForm" property="blockingReasonBIN" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingReasonBIN" property="reasonCode" labelProperty="reasonDesc" />
		</html:select>
    </td>
  </tr>

  <tr>
	<th><html:checkbox name="addBlockedListForm" property="blockIP"></html:checkbox></th>
    <td><fmt:message key="label.blockedlist.ipaddress.ipaddress" /></td>
    <th><c:out value="${blockIP}" /></th>
    <td width="140">
	    <html:select name="addBlockedListForm" property="blockingActionIP" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingActionIP" property="blockedStatusCode" labelProperty="blockedStatusDesc" />
		</html:select>
    </td>
    <td width="140">
    	<html:select name="addBlockedListForm" property="blockingReasonIP" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingReasonIP" property="reasonCode" labelProperty="reasonDesc" />
		</html:select>
    </td>
  </tr>
</table>

	<table border="0">

	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>

	<tr>
		<td width="140"><fmt:message key="label.fraud.risk.level" /></td>
		<td colspan="2">
			<html:text name="addBlockedListForm" property="riskLevel" size="3" maxlength="3" />
		</td>
	</tr>
	<tr>
		<td width="140" valign="top"><fmt:message key="label.block.comment" /></td>
		<td colspan="2">
			<html:textarea name="addBlockedListForm" property="blockingComment"  rows="4" cols="40" 
					onkeypress="textCounter(this, 128);" />			
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td width="140" valign="top">&nbsp;</td>
		<td colspan="2">
			<div id="formButtonsAdd">
				<html:submit property="submitProcess" value=" Process " 
					onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';"/>
				<input type="button" value=" Cancel "
					onclick="javascript:window.close();"/>			
			</div>
		</td>
	</tr>
	
	</table>
	
	<div id="processing" style="visibility:hidden" >
		<span class="PROCESSING"><fmt:message key="message.in.process" />
		</span>
	</div>

</html:form>

<h3><fmt:message key="label.blockedlist.email.existing.list" /></h3>	
	<logic:empty name="blockedEmails">
		<fmt:message key="label.blockedlist.email.norecord" />
	</logic:empty>
	
	<logic:notEmpty name="blockedEmails">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				
				<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.email.address" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.email.type" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.block.reason" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.block.comment" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.block.status" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.fraud.risk.level" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.create.date" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.create.user" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.last.update.date" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.last.update.user" /></TD>
				</tr>
								

				<logic:iterate id="blockedBean" name="blockedEmails">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="clearText"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="elecAddrTypeCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="reasonDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="comment" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="statusDesc"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="fraudRiskLevelCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createUserId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="statusUpdateDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="updateUserId"/></td>
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>

	<BR>
	
	<h3><fmt:message key="label.blockedlist.email.consumer.list" /></h3>	

	<logic:empty name="consumerProfiles">
		<fmt:message key="label.blockedlist.email.consumer.norecord" />
	</logic:empty>
	
	<logic:notEmpty name="consumerProfiles">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.consumer.id"/></th>		
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.login.id"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.first.name"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.last.name"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.status"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sub.status"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.ssn.mask"/></th>						
				</tr>

				<logic:iterate id="consumerProfile" name="consumerProfiles">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custLogonId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custFrstName" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custLastName" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custStatDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custSubStatDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custSSNMask"/></td>

				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>
	
	<hr width="50%" align="left">

<logic:notEmpty name="phoneList">
<%String dispClass = "TD-SMALL";%>

<h3><fmt:message key="label.affected.consumer.profile.list"/></h3>
<br />
<table>
	<tbody>
		<tr>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.consumer.id"/></th>		
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.login.id"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.status"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sub.status"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.first.name"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.last.name"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.cust.birth.date"/></th>		
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.ssn.mask"/></th>		
		</tr>
		<logic:iterate id="profile" name="phoneList">
		<tr>
			<td nowrap='nowrap' class='<%=dispClass%>'>
			    <a href='../../showCustomerProfile.do?custId=<bean:write 
			    name="profile" property="custId" />'
			    ><bean:write name="profile" property="custId" />
				</a></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custLogonId" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custStatDesc" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custSubStatDesc" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custFrstName" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custLastName" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custBrthDate" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custSSNMask" /></td>
		</tr>
		<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
			else dispClass = "TD-SHADED-SMALL"; %>	
		</logic:iterate>
	</tbody>
</table>
</logic:notEmpty>
<logic:empty name="phoneList">
<h3><fmt:message key="label.no.affected.consumer.profile.found"/></h3>
</logic:empty>

<hr width="50%" align="left">

	<h3><fmt:message key="label.blockedlist.cc.existing.list" /></h3>	
	<logic:empty name="blockedCCs">
		<fmt:message key="label.blockedlist.cc.norecord" />
	</logic:empty>
	
	<logic:notEmpty name="blockedCCs">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.mask" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.reason" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.comments" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.status" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.risklevel" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.createddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.createdby" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.modifieddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.modifiedby" /> </TD>
				</tr>

				<logic:iterate id="blockedBean" name="blockedCCs">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="binText"/>******<bean:write filter="false" name="blockedBean" property="maskText"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="reasonDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="comment" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="statusDesc"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="fraudRiskLevelCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createUserId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="statusUpdateDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="updateUserId"/></td>
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>

	<BR>
	
	<h3><fmt:message key="label.blockedlist.cc.consumer.list" /></h3>	

	<logic:empty name="customerProfileAccounts">
		<fmt:message key="label.blockedlist.cc.consumer.norecord" />
	</logic:empty>
	
	<logic:notEmpty name="customerProfileAccounts">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.consumer.id"/></th>		
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.login.id"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.first.name"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.last.name"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.status"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sub.status"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.ssn.mask"/></th>						
				
				</tr>

				<logic:iterate id="customerProfileAccount" name="customerProfileAccounts">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custLogonId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custFrstName" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custLastName" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custStatDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custSubStatDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custSsnMaskNbr"/></td>

				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>
	
	<hr width="50%" align="left">

	<h3>Existing records that will be affected by this change</h3>	
	<logic:empty name="blockedIPs">
		<fmt:message key="label.blockedlist.ipaddress.noexisting" />
	</logic:empty>
	
	<logic:notEmpty name="blockedIPs">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.ipaddress" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.reason" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.comments" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.status" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.risklevel" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.createddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.createdby" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.modifieddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.modifiedby" /> </TD>
				
				</tr>

				<logic:iterate id="blockedIP" name="blockedIPs">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="ipAddress1"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="reasonDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="comment" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedIP" property="statusDesc"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedIP" property="fraudRiskLevelCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="createDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="createUserId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="statusUpdateDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="updateUserId"/></td>
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>

	