<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>

</center>

<html:form action="/saveCCBlockedList.do">

<h3><fmt:message key="label.blockedlist.cc.title" /></h3>
	<table border="0">
		<tbody>
	        <tr>
	            <td><fmt:message key="label.blockedlist.cc.cc" /> </td>
	            <td><bean:write name="blockedCC" property="binText" />******<bean:write name="blockedCC" property="maskText" /></td>
	        </tr>
	        <tr>
	            <td><fmt:message key="label.blockedlist.cc.reason" /></td>
	            <td><bean:write name="blockedCC" property="reasonDesc" /></td>
	        </tr>
	        <tr>
	            <td><fmt:message key="label.blockedlist.cc.action" /></td>
	            <td><bean:write name="blockedCC" property="statusDesc" /></td>
	        </tr>
	        <tr>
	            <td><fmt:message key="label.blockedlist.cc.comments" /></td>
	            <td><bean:write name="blockedCC" property="comment" /></td>
	        </tr>
	        <tr>
	            <td><fmt:message key="label.blockedlist.cc.risklevel" /></td>
	            <td><bean:write name="blockedCC" property="fraudRiskLevelCode" /></td>
	        </tr>
	        <tr>
	            <td colspan="2">&nbsp;</td>
			</tr>
	        
	        <tr>
	            <td>&nbsp;</td>
			    <td>
					<div id="formButtonsAdd">
					<html:submit property="submitDBSave" value=" Save "
							 onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';" />
					<html:submit property="submitDBCancel" value="Cancel"
							 onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';" />
					</div>
			    </td>
			</tr>
		</tbody>
	</table>

	<div id="processing" style="visibility:hidden" >
		<span class="PROCESSING"><fmt:message key="message.in.process" />
		</span>
	</div>
	
	<h3><fmt:message key="label.blockedlist.cc.existing.list" /></h3>	
	<logic:empty name="blockedCCs">
		<fmt:message key="label.blockedlist.cc.norecord" />
	</logic:empty>
	
	<logic:notEmpty name="blockedCCs">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.mask" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.reason" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.comments" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.status" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.risklevel" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.createddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.createdby" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.modifieddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.modifiedby" /> </TD>
				</tr>

				<logic:iterate id="blockedBean" name="blockedCCs">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="binText"/>******<bean:write filter="false" name="blockedBean" property="maskText"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="reasonDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="comment" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="statusDesc"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="fraudRiskLevelCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createUserId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="statusUpdateDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="updateUserId"/></td>
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>

	<BR>
	
	<h3><fmt:message key="label.blockedlist.cc.consumer.list" /></h3>	

	<logic:empty name="customerProfileAccounts">
		<fmt:message key="label.blockedlist.cc.consumer.norecord" />
	</logic:empty>
	
	<logic:notEmpty name="customerProfileAccounts">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.consumer.id"/></th>		
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.login.id"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.first.name"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.last.name"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.status"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sub.status"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.ssn.mask"/></th>						
				
				</tr>

				<logic:iterate id="customerProfileAccount" name="customerProfileAccounts">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custLogonId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custFrstName" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custLastName" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custStatDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custSubStatDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custSsnMaskNbr"/></td>

				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>

</html:form>
