<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>

</center>

<html:form action="/saveIPBlockedList.do">

<h3><fmt:message key="label.blockedlist.ipaddress.title" /></h3>
	<table border="0">
		<tbody>
	        <tr>
	            <td width="120"><fmt:message key="label.blockedlist.ipaddress.ipaddress" /> 
	            <td><bean:write name="blockedIP" property="ipAddress1" /></td>
	        </tr>
	        <tr>
	            <td><fmt:message key="label.blockedlist.ipaddress.ipaddress" /> </td>
	            <td><bean:write name="blockedIP" property="ipAddress2" /></td>
	        </tr>
	        <tr>
	            <td><fmt:message key="label.blockedlist.ipaddress.risklevel" /> </td>
	            <td><bean:write name="blockedIP" property="fraudRiskLevelCode" /></td>
	        </tr>
	        <tr>
	            <td><fmt:message key="label.blockedlist.ipaddress.status" /> </td>
	            <td><bean:write name="blockedIP" property="statusDesc" /></td>
	        </tr>
	        <tr>
	            <td><fmt:message key="label.blockedlist.ipaddress.reason" /> </td>
	            <td><bean:write name="blockedIP" property="reasonDesc" /></td>
	        </tr>
	        <tr>
	            <td><fmt:message key="label.blockedlist.ipaddress.comments" /> </td>
	            <td><bean:write name="blockedIP" property="comment" /></td>
	        </tr>
	        <tr>
	            <td colspan="2">&nbsp;</td>
			</tr>
	        
	        <tr>
	            <td>&nbsp;</td>
			    <td>
					<div id="formButtonsAdd">
					<html:submit property="submitDBSave" value="Save"
							 onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';" />
					<html:submit property="submitDBCancel" value="Cancel"
							 onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';" />
					</div>
			    </td>
			</tr>
		</tbody>
	</table>

	<div id="processing" style="visibility:hidden" >
		<span class="PROCESSING"><fmt:message key="message.in.process" />
		</span>
	</div>
	
	<h3>Existing records that will be affected by this change</h3>	
	<logic:empty name="blockedIPs">
		<fmt:message key="label.blockedlist.ipaddress.noexisting" />
	</logic:empty>
	
	<logic:notEmpty name="blockedIPs">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.ipaddress" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.reason" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.comments" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.status" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.risklevel" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.createddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.createdby" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.modifieddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.modifiedby" /> </TD>
				
				</tr>

				<logic:iterate id="blockedIP" name="blockedIPs">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="ipAddress1"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="reasonDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="comment" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedIP" property="statusDesc"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedIP" property="fraudRiskLevelCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="createDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="createUserId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="statusUpdateDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="updateUserId"/></td>
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>


</html:form>
