<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/showUser.js" language="JavaScript"></script>

<html:form action="/showEmailBlockedList.do" focus="emailAddress">
<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>
</center>

<h3><fmt:message key="label.blockedlist.email.title" /></h3>
<br>
	<table border="0" width="100%">
	<tr>
		<td width="100" valign="top"><fmt:message key="label.blockedlist.email.email" /></td>
		<td>
			<html:text name="showEmailBlockedListForm" property="emailAddress" size="20" />
			<html:submit property="submitEmailSearch" value="Search" 
				onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';"/>
		</td>
	</tr>
	
	<tr>
		<td width="100" valign="top"><fmt:message key="label.blockedlist.email.domain" /></td>
		<td>
			<html:text name="showEmailBlockedListForm" property="emailDomain" size="20" />
			<html:submit property="submitDomainSearch" value="Search" 
				onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';"/>
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<div id="formButtonsAdd">
				<html:submit property="submitShowAll" value="Show All"
					 onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';" />
				<logic:equal name="auth" property="addEmailBlockedList" value="true">
					<html:submit property="submitAdd" value="Add to Blocked List"
							 onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';" />
				</logic:equal>
			</div>
		</td>
	</tr>
	</table>

	<div id="processing" style="visibility: hidden"><span
		class="PROCESSING">Your request is in process ....</span></div>


	<logic:notEmpty name="emails">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
				<logic:equal name="auth" property="addEmailBlockedList" value="true">
					<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
						<fmt:message key="label.action" /></TD>
				</logic:equal>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.email.address" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.email.type" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.block.reason" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.block.comment" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.block.status" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.fraud.risk.level" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.create.date" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.create.user" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.last.update.date" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.last.update.user" /></TD>
				</tr>

				<logic:iterate id="email" name="emails">
				<tr>
					<logic:equal name="auth" property="addEmailBlockedList" value="true">
            			<td nowrap='nowrap' class='<%=dispClass%>'>
							<logic:equal name="email" property="addrFmtCode" value="A">
							 	<a href='../../addEmailBlockedList.do?paramemail=
								<bean:write filter="false" name="email" 
								property="clearText"/>&action=edit'>Edit</a>
	            			</logic:equal>								
							<logic:equal name="email" property="addrFmtCode" value="D">
							 	<a href='../../addEmailBlockedList.do?paramdomain=
								<bean:write filter="false" name="email" 
								property="clearText"/>&action=edit'>Edit</a>
	            			</logic:equal>								
						</td>
					</logic:equal>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="email" property="clearText"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="email" property="elecAddrTypeCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="email" property="reasonDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="email" property="comment" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="email" property="statusCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="email" property="fraudRiskLevelCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="email" property="createDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
	               		<logic:notEqual name="email" property="createUserId" value="">
							<a href='../../showUser.do?userId=<c:out 
							value="${email.createUserId}"/>' 
							onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out 
							value="${email.createUserId}"/>\')'">
							<b><bean:write name="email" property="createUserId" /></b></a>
						</logic:notEqual>
             			<logic:equal name="email" property="createUserId" value="">
            				<bean:write name="email" property="createUserId" />
            			</logic:equal>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="email" property="statusUpdateDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
	               		<logic:notEqual name="email" property="updateUserId" value="">
							<a href='../../showUser.do?userId=<c:out 
							value="${email.updateUserId}"/>' 
							onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out 
							value="${email.updateUserId}"/>\')'">
							<b><bean:write name="email" property="updateUserId" /></b></a>
						</logic:notEqual>
             			<logic:equal name="email" property="updateUserId" value="">
            				<bean:write name="email" property="updateUserId" />
            			</logic:equal>
            		</td>
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>
	

</html:form>