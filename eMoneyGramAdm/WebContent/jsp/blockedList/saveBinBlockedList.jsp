<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>

</center>

<html:form action="/saveBinBlockedList.do">

<h3><fmt:message key="label.blockedlist.bin.title" /></h3>
	<table border="0">
		<tbody>
	        <tr>
	            <td><fmt:message key="label.blockedlist.bin.bin" /> </td>
	            <td><bean:write name="blockedBin" property="clearText" scope="session" /></td>
	        </tr>
	        <tr>
	            <td><fmt:message key="label.blockedlist.bin.reason" /></td>
	            <td><bean:write name="blockedBin" property="reasonDesc" scope="session" /></td>
	        </tr>
	        <tr>
	            <td><fmt:message key="label.blockedlist.bin.action" /></td>
	            <td><bean:write name="blockedBin" property="statusDesc" scope="session" /></td>
	        </tr>
	        <tr>
	            <td><fmt:message key="label.blockedlist.bin.comments" /></td>
	            <td><bean:write name="blockedBin" property="comment" scope="session" /></td>
	        </tr>
	        <tr>
	            <td><fmt:message key="label.blockedlist.bin.risklevel" /></td>
	            <td><bean:write name="blockedBin" property="fraudRiskLevelCode" scope="session" /></td>
	        </tr>
	        <tr>
	            <td colspan="2">&nbsp;</td>
			</tr>
	        
	        <tr>
	            <td>&nbsp;</td>
			    <td>
					<div id="formButtonsAdd">
					<html:submit property="submitDBSave" value=" Save "
							 onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';" />
					<html:submit property="submitDBCancel" value="Cancel"
							 onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';" />
					</div>
			    </td>
			</tr>
		</tbody>
	</table>

	<div id="processing" style="visibility:hidden" >
		<span class="PROCESSING"><fmt:message key="message.in.process" />
		</span>
	</div>
	
	<h3><fmt:message key="label.blockedlist.bin.existing.list" /></h3>	
	<logic:empty name="blockedBins">
		<fmt:message key="label.blockedlist.bin.norecord" />
	</logic:empty>
	
	<logic:notEmpty name="blockedBins">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.bin" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.reason" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.comments" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.status" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.risklevel" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.createddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.createdby" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.modifieddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.modifiedby" /> </TD>
				
				</tr>

				<logic:iterate id="blockedBean" name="blockedBins">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="clearText"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="reasonDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="comment" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="statusDesc"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="fraudRiskLevelCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createUserId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="statusUpdateDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="updateUserId"/></td>
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>

	<BR>
	
	<h3><fmt:message key="label.blockedlist.bin.consumer.list" /></h3>	

	<logic:empty name="customerProfileAccounts">
		<fmt:message key="label.blockedlist.bin.consumer.norecord" />
	</logic:empty>
	
	<logic:notEmpty name="customerProfileAccounts">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.consumer.id"/></th>		
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.login.id"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.first.name"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.last.name"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.status"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sub.status"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.ssn.mask"/></th>						
				</tr>

				<logic:iterate id="customerProfileAccount" name="customerProfileAccounts">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custLogonId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custFrstName" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custLastName" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custStatDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custSubStatDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custSsnMaskNbr"/></td>

				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>

</html:form>
