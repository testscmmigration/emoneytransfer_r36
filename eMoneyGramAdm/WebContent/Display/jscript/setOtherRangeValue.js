function setOtherRangeValue(filledField, otherField)
{
	if (filledField != null)  
	{
		if (otherField != null && (otherField.value == null || otherField.value == ""))
		{
			otherField.value = filledField.value;
		}
	}
}
