function updateContentDims() {
	$('#contentWrapper').css('height', $(window).height() - $('#headerTable').height());
}

$(window).bind('resize', function() {
	updateContentDims();
});

updateContentDims();