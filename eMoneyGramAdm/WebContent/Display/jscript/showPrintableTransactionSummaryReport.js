/************************************************************
open's the transaction detail window
*************************************************************/
var reportWindow = null;
function showPrintableTransactionSummaryReport()
{
	 var random_num;
	 random_num = (Math.round((Math.random()*999)+1));
     var openURL = "viewPrintableTransactionSummaryReport.do?unique=" + random_num;
	 if (  reportWindow==null || reportWindow.closed )  
	 {   
       reportWindow = window.open(openURL,"Detail",'status,scrollbars,resizable,width=750,height=600'); 
     }
     else
     {
       reportWindow.location.href=openURL;
     }   
     reportWindow.focus();   
}
