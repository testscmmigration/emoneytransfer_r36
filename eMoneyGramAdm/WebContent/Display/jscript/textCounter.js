
function textCounter(field, maxlimit) 
{
  if (field.value.length >= maxlimit)
  {
    field.value = field.value.substring(0, maxlimit - 1);
    alert('Textarea value can only be ' + maxlimit + ' characters in length.');
    return false;
  }
}
var checkboxes = [];
var radios = [];
function selectDefaultInputs(){ 
	var inputs = document.getElementsByTagName("input");	
	for (var i = 0; i < inputs.length; i++) {  
	  if (inputs[i].type == "radio") {  
		  radios.push(inputs[i]); 
	  }
	  if (inputs[i].type == "checkbox") {  
		    checkboxes.push(inputs[i]); 
	  }
	} 	 
	for (var i = 0; i < radios.length; i++) {  
		if(radios[i].value == "BLK" || radios[i].value == "NBK" ){
			  radios[i].checked = true;
		  }
	  
	}
	for (var i = 0; i < checkboxes.length; i++) {  
	  checkboxes[i].checked = true;
	}  
}
function setHiddenParams(){
	var selectedCCVal = "";
	var selectedBankAccVal = "";
	for (var i = 0; i < checkboxes.length; i++) {  
	  if ((checkboxes[i].name == "blockCC") && (checkboxes[i].checked == true)){
		selectedCCVal = selectedCCVal + checkboxes[i].id + "|"; 
	  }
	  if((checkboxes[i].name == "blockAcct") && (checkboxes[i].checked == true)){
		selectedBankAccVal = selectedBankAccVal + checkboxes[i].id + "|";
	  }
	}
	if (selectedCCVal != ""){
		selectedCCVal = selectedCCVal.substring(0,selectedCCVal.length-1);
		document.getElementById("blockCCVal").value = selectedCCVal;
	}
	if (selectedBankAccVal != ""){
		selectedBankAccVal = selectedBankAccVal.substring(0,selectedBankAccVal.length-1);
		document.getElementById("blockBnkAccVal").value = selectedBankAccVal;
	}
}
/* trim() support for IE */
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
/*Function to check if any checkbox is checked*/
function isChceked(){	 
	for (var i = 0; i < checkboxes.length; i++) {  
	  if(checkboxes[i].checked){
	  	return true;
	  }
	}  
	return false;		
}

/*Function to validate the form */
function validateBlockingForm(blkForm){
	 var reason = document.getElementById('selectReason').value;
	 var commentEle = document.getElementById('blkComment');
	 var comment = commentEle.value.trim();
	 if (!isChceked()){
		 document.getElementById("jsErrCheckbox").style.display = "block";
	 } else {
		 document.getElementById("jsErrCheckbox").style.display = "none";
	 }
	 if (reason == "None"){
		 document.getElementById("jsErrReason").style.display = "block";
	 } else {
		 document.getElementById("jsErrReason").style.display = "none";
	 }
	 if (comment == ''){
		 document.getElementById("jsErrComment").style.display = "block";
	 } else {
		 document.getElementById("jsErrComment").style.display = "none";
	 }
	 if (!isChceked()|| (reason == "None") || (comment == '')){
	 	return false;
	 }
	 if (comment.length > 128){
	 	commentEle.value = commentEle.value.substring(0, 127);
	 }	
	setHiddenParams();	 
	blkForm.submit();	 
}

function deselectDefaultAmt(field){
	field.value = 'N';
}

function clearSndMaxAmt(field){
	field.value="0.000";
}

