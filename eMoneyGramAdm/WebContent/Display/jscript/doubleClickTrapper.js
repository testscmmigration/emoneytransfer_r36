<!-- Please keep copyright lines in place without changes.
//   Copyright 2000 William and Mari Bontrager
//   Copyright 2003 Bontrager Connection, LLC
//
// For more information and instructions, please 
//    see the "Double Click Trapper" article at 
//    http://willmaster.com/possibilities/archives


// When the form's submit button is clicked a second 
//    time, the click trap is set. Here, specify how 
//    many seconds the trap exist before it's released.

TrapClicksForHowManySeconds = 10;



// If the user clicks the submit button and causes a 
//    trap, or clicks when a trip is set, you may spawn 
//    an alert box with a message. If you do want to 
//    spawn the alert box, specify the message between 
//    the quotation marks. Otherwise, leave it blank.
//(Blank is two consecutive quotation mark characters, 
//    with nothing between them.)
// Note: If you use any quotation marks in the message 
//       itself, those must be preceeded with a back-slash 
//       character. Example: "My name is \"tall\", okay?"

AlertBoxMessage = "";



// If the user clicks the submit button and causes a trap, 
//    or clicks when a trip is set, you may spawn a popup 
//    box. If you want to spawn the popup box, specify 
//    the URL of the web page to insert into the popup. 
//    Otherwise, leave it blank. You may also specify 
//    the height of the popup and the width of the popup.

PopUpBoxURL = "";
PopUpBoxHeight = 300;
PopUpBoxWidth = 500;



// If you want the submit button to change its text when 
//    it is clicked, specify the name you assigned to the 
//    form, the name you assigned to the submit button 
//    form field, and the new text for the submit button. 
//    Otherwise, leave at least one of these blank.

NameOfForm = "";
NameOfSubmitButton = "submitter";
NewTextForSubmitButton = "Now Processing . . .";



// ////////////////////////////////////////// //
// NO CUSTOMIZATION REQUIRED BELOW THIS POINT //
// ////////////////////////////////////////// //


TrapTime = 0;
DoubleClickTrapperCounter = 0;


function DoubleClickTrapperAction() {
DoubleClickTrapperCounter++;
DoubleClickTrapperButton();
var trapTheClick = false;
if(DoubleClickTrapperCounter > 1 ) { trapTheClick = true; }
if(trapTheClick == true) {
	if(TrapTime == 0) {
		var tDate = new Date;
		TrapTime = tDate.valueOf();
		}
	else {
		var tDate = new Date;
		var localTrapTime = tDate.valueOf();
		if((localTrapTime - TrapTime) > (TrapClicksForHowManySeconds * 1000)) {
			TrapTime = 0;
			trapTheClick = false;
			DoubleClickTrapperCounter = 0;
			}
		}
	}
var valueToReturn = true;
if(trapTheClick == true) {
	valueToReturn = false;
	DoubleClickTrapperPopUp();
	DoubleClickTrapperAlert();
	}
return valueToReturn;
} // end of function DoubleClickTrapperAction()


function DoubleClickTrapperButton() {
var formname   = StripSpaces(NameOfForm);
var submitname = StripSpaces(NameOfSubmitButton);
var newtext    = StripSpaces(NewTextForSubmitButton);
if(formname.length > 0 && submitname.length > 0 && newtext.length > 0) {
	var s = 'document.' + NameOfForm + '.' + NameOfSubmitButton + ".value = '" + NewTextForSubmitButton + "'";
	eval(s);
	}
} // end of function DoubleClickTrapperButton()


function DoubleClickTrapperPopUp() {
var url = StripSpaces(PopUpBoxURL);
if(url.length > 0) {
	if(PopUpBoxHeight < 1) { PopUpBoxHeight = 200; }
	if(PopUpBoxWidth < 1) { PopUpBoxWidth = 300; }
	window.open(url,'',('height=' + PopUpBoxHeight + ',width=' + PopUpBoxWidth + ',resizable=yes,scrollbars=yes'));
	}
} // end of function DoubleClickTrapperPopUp()


function DoubleClickTrapperAlert() {
var message = StripSpaces(AlertBoxMessage);
if(message.length > 0) { alert(AlertBoxMessage); }
} // end of function DoubleClickTrapperAlert()


function StripSpaces(s) {
while(s.indexOf(' ') == 0) { s = s.substr(1); }
return s;
} // end of function StripSpaces()

-->