/*!
	EMTbox v1 - Lightbox clone for jQuery for EMT Admin
	based in slimbox2
	uw45/cgomez@moneygram.com

*/

(function($) {


	var win = $(window), options, images, activeImage = -1, activeURL, prevImage, nextImage, compatibleOverlay, middle, centerWidth, centerHeight,
		ie6 = !window.XMLHttpRequest, hiddenElements = [], documentElement = document.documentElement,


	// DOM elements
	overlay, center, image, sizer, prevLink, nextLink, bottomContainer, bottom, caption, number;

	/*
		Initialization
	*/

	$(function() {
		// Append the  HTML code EMTadmin specific
		$("#contentWrapper").before(
			$([
				overlay = $('<div id="lbOverlay" />')[0],
				center = $('<div id="lbCenter" />')[0],
				bottomContainer = $('<div id="lbBottomContainer" />')[0]
			]).css("display", "none")
		);

		image = $('<div id="lbImage" />').appendTo(center)

		bottom = $('<div id="lbBottom" />').appendTo(bottomContainer).append([
			$('<a id="lbCloseLink" href="#" />').add(overlay).click(close)[0],
			caption = $('<div id="lbCaption" />')[0],
			number = $('<div id="lbNumber" />')[0],
			$('<div style="clear: both;" />')[0]
		])[0];


	});


	/*
		API
	*/

	// Open EMTbox with the specified parameters
	$.EMTbox = function(_DOMElement, _options) {
		options = $.extend({

			overlayOpacity: 0.6,			// 1 is opaque, 0 is completely transparent (change the color in the CSS file)
			overlayFadeDuration: 0,		// Duration of the overlay fade-in and fade-out animations (in milliseconds)
			closeKeys: [27, 88, 67],		// Array of keycodes to close Slimbox, default: Esc (27), 'x' (88), 'c' (67)
			initialWidth: 250,			// Initial width of the box (in pixels)
			initialHeight: 250		// Initial height of the box (in pixels)
		}, _options);



		middle = win.scrollTop() + (win.height() / 2);
		centerWidth = options.initialWidth;
		centerHeight = options.initialHeight;
		$(center).css({top: Math.max(0, middle - (centerHeight / 2)), width: centerWidth, height: centerHeight, marginLeft: -centerWidth/2}).show();
		compatibleOverlay = ie6 || (overlay.currentStyle && (overlay.currentStyle.position != "fixed"));
		if (compatibleOverlay) overlay.style.position = "absolute";
		$(overlay).css("opacity", options.overlayOpacity).fadeIn(options.overlayFadeDuration);
		position();
		setup(1);

		DOMElement = _DOMElement;
		options.loop = options.loop && (images.length > 1);
		return changeContent(DOMElement);
	};



	/*
		Internal functions
	*/

	function position() {
		var l = win.scrollLeft(), w = win.width();
		$([center, bottomContainer]).css("left", l + (w / 2));
		if (compatibleOverlay) $(overlay).css({left: l, top: win.scrollTop(), width: w, height: win.height()});
	}

	function setup(open) {
		if (open) {
			$("object").add(ie6 ? "select" : "embed").each(function(index, el) {
				hiddenElements[index] = [el, el.style.visibility];
				el.style.visibility = "hidden";
			});
		} else {
			$.each(hiddenElements, function(index, el) {
				el[0].style.visibility = el[1];
			});
			hiddenElements = [];
		}
		var fn = open ? "bind" : "unbind";
		win[fn]("scroll resize", position);
		$(document)[fn]("keydown", keyDown);
	}

	function keyDown(event) {
		var code = event.keyCode, fn = $.inArray;
		// Prevent default keyboard action (like navigating inside the page)
		return (fn(code, options.closeKeys) >= 0) ? close()
			: true;
	}



	function changeContent(element) {


		image.html($(element).clone())
		image.find(element).show().addClass('EMTBoxClonedElement');

		$(center).css({top: Math.max(0, middle - (image.find(element).outerHeight() / 2)), width: image.find(element).outerWidth()+20, height: image.find(element).outerHeight()+20, marginLeft: -image.find(element).outerWidth()/2}).show();
		return false;
	}



	function stop() {

		$(center, image, bottom).hide();
		image.html('')
	}

	function close() {

			stop();

			$(overlay).stop().fadeOut(options.overlayFadeDuration, setup);


		return false;
	}

	function EMTboxResize() {

		$(center).css({top: Math.max(0, middle - (image.find('.EMTBoxClonedElement').outerHeight() / 2)), width: image.find('.EMTBoxClonedElement').outerWidth()+20, height: image.find('.EMTBoxClonedElement').outerHeight()+20, marginLeft: -image.find('.EMTBoxClonedElement').outerWidth()/2});
		return false;

	}

	$.EMTbox.close = function(){
		close();
	}

	$.EMTbox.EMTboxResize = function(){
		EMTboxResize();
	}

})(jQuery);
