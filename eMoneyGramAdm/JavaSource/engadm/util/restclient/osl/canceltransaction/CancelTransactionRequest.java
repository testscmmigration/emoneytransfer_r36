package engadm.util.restclient.osl.canceltransaction;

import com.fasterxml.jackson.annotation.JsonInclude;
 
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CancelTransactionRequest {
	
	public enum CancelReasonCode {
	    MONEY_NOT_NEEDED("RNW"), // Receiver no longer needs the money
	    SEND_AMOUNT_CHANGE("TCA"), // Need to change send amount
	    RECEIVER_CHANGE("RCR"), // Need to change receiver
	    TRANSACTION_TOO_SLOW("TTL"), // Transaction taking too long
	    SUSPECTED_FRAUD("FRD"), // Suspect fraud
	    SERVICE_OPT_OUT("DOS"), // Decided to use another service
	    OTHER("OTH"); // Other
	    private final String value;
	    CancelReasonCode(String value) {
	        this.value = value;
	    }
	    public String getValue() {
	        return value;
	    }
	}
	private String clientRequestId;
	private String clientSessionId;


    private CancelReasonCode reasonCode;
    private String cancellationDescription;

	private String sourceApplication;
	private Long transactionId;
	private Boolean skipACReversal;
	private String csrUserid;
	
	/**
	 * @return the clientRequestId
	 */
	public String getClientRequestId() {
		return clientRequestId;
	}
	/**
	 * @param clientRequestId the clientRequestId to set
	 */
	public void setClientRequestId(String clientRequestId) {
		this.clientRequestId = clientRequestId;
	}
	/**
	 * @return the clientSessionId
	 */
	public String getClientSessionId() {
		return clientSessionId;
	}
	/**
	 * @param clientSessionId the clientSessionId to set
	 */
	public void setClientSessionId(String clientSessionId) {
		this.clientSessionId = clientSessionId;
	}
	/**
	 * @return the reasonCode
	 */
	public CancelReasonCode getReasonCode() {
		return reasonCode;
	}
	/**
	 * @param reasonCode the reasonCode to set
	 */
	public void setReasonCode(CancelReasonCode reasonCode) {
		this.reasonCode = reasonCode;
	}
	/**
	 * @return the cancellationDescription
	 */
	public String getCancellationDescription() {
		return cancellationDescription;
	}
	/**
	 * @param cancellationDescription the cancellationDescription to set
	 */
	public void setCancellationDescription(String cancellationDescription) {
		this.cancellationDescription = cancellationDescription;
	}
	/**
	 * @return the sourceApplication
	 */
	public String getSourceApplication() {
		return sourceApplication;
	}
	/**
	 * @param sourceApplication the sourceApplication to set
	 */
	public void setSourceApplication(String sourceApplication) {
		this.sourceApplication = sourceApplication;
	}
	/**
	 * @return the transactionId
	 */
	public Long getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @return the skipACReversal
	 */
	public Boolean getSkipACReversal() {
		return skipACReversal;
	}
	/**
	 * @param skipACReversal the skipACReversal to set
	 */
	public void setSkipACReversal(Boolean skipACReversal) {
		this.skipACReversal = skipACReversal;
	}
	/**
	 * @return the csrUserid
	 */
	public String getCsrUserid() {
		return csrUserid;
	}
	/**
	 * @param csrUserid the csrUserid to set
	 */
	public void setCsrUserid(String csrUserid) {
		this.csrUserid = csrUserid;
	}
			
}