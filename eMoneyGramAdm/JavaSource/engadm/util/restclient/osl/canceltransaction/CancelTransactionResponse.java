package engadm.util.restclient.osl.canceltransaction;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import emgadm.services.oslupdate.ServiceErrorVO;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CancelTransactionResponse {
 
    private Integer status;
    @JsonProperty("error")
    private ServiceErrorVO serviceError;
    @JsonIgnore
    private Error error;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
 
 
    public Error getError() {
        return error;
    }
 
 
    public void setError(Error error) {
        this.error = error;
    }
 
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }
 
    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
 
    /**
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }
 
    /**
     * @param status the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }
 
    /**
     * @return the serviceError
     */
    public ServiceErrorVO getServiceError() {
        return serviceError;
    }
 
    /**
     * @param serviceError the serviceError to set
     */
    public void setServiceError(ServiceErrorVO serviceError) {
        this.serviceError = serviceError;
    }
    
    
 

  
	

}