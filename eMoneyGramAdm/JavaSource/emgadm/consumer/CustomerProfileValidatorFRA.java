package emgadm.consumer;

import emgadm.model.UserProfile;

public class CustomerProfileValidatorFRA extends BaseCustomerProfileValidator {

	public CustomerProfileValidatorFRA(UpdateCustomerProfileForm form,
			UserProfile userProfile,  CustomerProfileRegexPattern customerProfileRegex) {
		super(form, userProfile, customerProfileRegex);
		fields.add(new GenericPostalCode(form.getPostalCode(), ZIP_CODE, customerProfileRegex.getPostalCodeRegex())
				.setMinLength(5).setMaxLength(5));
		fields.add(new AlphaField(form.getState(), STATE).isRequired(false));
		fields.add(new AddressField(form.getPhoneCountryCode(), COUNTRY_CODE, customerProfileRegex.getAddressLine1Regex() )
				.isRequired(false));
		fields.add(new StrictNumericField(form.getLoyaltyPgmMembershipId(),
				PLUS).setMinLength(9).setMaxLength(12).isRequired(false));
		fields.add(new UntaintedPhoneNumber(form.getPhoneNumber(), PHONE_NUMBER)
				.setMinLength(7).setMaxLength(14).isRequired(false)
				.setUID(userProfile.getUID())
				.setCountryDialingCode(form.getPhoneCountryCode()));
	}

}
