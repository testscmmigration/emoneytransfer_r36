package emgadm.consumer;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class UpdateReceiveLimitForm extends EMoneyGramAdmBaseValidatorForm {
	private String custId;
	private boolean receiveLimit;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		receiveLimit = false;
	}
	
	public String getCustId() {
		return custId;
	}

	public void setCustId(String string) {
		custId = string;
	}

	public void setReceiveLimit(boolean receiveLimit) {
		this.receiveLimit = receiveLimit;
	}

	public boolean isReceiveLimit() {
		return receiveLimit;
	}

}
