/*
 * Created on April 08, 2005
 *
 */
package emgadm.consumer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.moneygram.common.service.ServiceException;
import com.moneygram.mgo.service.config_v3_4.GetSourceSiteInfoResponse;
import com.moneygram.mgo.service.config_v3_4.NameValuePairType;
import com.moneygram.mgo.service.consumer_v4_0_7_2.Consumer;
import com.moneygram.mgo.service.consumer_v4_0_7_2.Contact;
import com.moneygram.mgo.service.consumer_v4_0_7_2.LoyaltyInfo;
import com.moneygram.mgo.service.consumer_v4_0_7_2.PersonalInfo;
import com.moneygram.mgo.service.consumer_v4_0_7_2.UpdateConsumerProfileRequest;
import com.moneygram.mgo.service.consumer_v4_0_7_2.UpdateConsumerProfileResponse;
import com.moneygram.mgo.service.consumer_v4_0_7_2.UpdateConsumerProfileTask;
import com.moneygram.mgo.shared_v4_0_7_2.ConsumerAddress;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.dataaccessors.ManagerFactory;
import emgadm.model.UserProfile;
import emgadm.services.ConsumerServiceProxy;
import emgadm.services.ConsumerServiceProxyImpl;
import emgadm.services.osl.OSLException;
import emgadm.services.osl.OSLProxyImpl;
import emgadm.services.oslupdate.UpdateOSLAddressResponse;
import emgshared.cache.CacheConstants;
import emgshared.cache.locator.EMTSharedSourceSiteCacheService;
import emgshared.cache.locator.EMTSharedSourceSiteCacheServiceImpl;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.DuplicateActiveProfileException;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileComment;
import emgshared.model.TaintIndicatorType;
import emgshared.model.UserActivityType;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;
import emgshared.util.DateFormatter;
import emgshared.util.StringHelper;


/**
 * @author T349
 * 
 */
public class UpdateCustomerProfileAction extends EMoneyGramAdmBaseAction {
	private static final String FORWARD_SUCCESS = "success";
	private static final String FORWARD_FAILURE = "failure";
	private static final String MOBILE_CODE = "MOBILE";
	private static final String HOME_CODE = "HOME";
	private static final String DATE_FORMAT = "dd/MMM/yyyy";
	private static final String MGOZAF = "MGOZAF";
	private static final String UPDATE_CONSUMER_PROFILE_SOURCE_APP = "EMT";
	private static final String PLUS_NUMBER_INVALID = "20130";
	private static final String PLUS_NUMBER_NO_MATCH = "20134";
	private static final String PHONE_NUMBER_ALREADY_EXIST = "1103";
	
	private static final ServiceFactory sharedServiceFactory = ServiceFactory
			.getInstance();
	private static final ConsumerProfileService profileService = sharedServiceFactory
			.getConsumerProfileService();
	private static final Logger LOGGER = EMGSharedLogger
			.getLogger(UpdateCustomerProfileAction.class);
	private static final emgadm.services.osl.OSLProxy oslProxy = emgadm.services.osl.OSLProxyImpl
			.getInstance();
	
	@SuppressWarnings("unused")
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws NumberFormatException, DataSourceException {

		ActionForward forward = mapping.findForward(FORWARD_FAILURE);
		UpdateCustomerProfileForm inputForm = (UpdateCustomerProfileForm) form;
		ConsumerProfile consumerProfile = null;
		HttpSession session = request.getSession();
		ActionErrors errors = new ActionErrors();

		ArrayList<String> commentList = new ArrayList<String>();
		boolean addressSaved = false;

		try {
			/* Upon click of cancel button */
			if (inputForm.getUpdateCancel() != null) {
				forward = mapping.findForward(FORWARD_SUCCESS);
				return forward;
			}
		
			String userId = getUserProfile(request).getUID();
			/* Validating Profile */
			consumerProfile = profileService.getConsumerProfile(
					Integer.valueOf(inputForm.getCustId()), userId, null);
			String parentSiteId = consumerProfile.getPartnerSiteId();
			inputForm.setPartnerSiteId(parentSiteId);
			consumerProfile.setLoyaltyPgmMembershipId(inputForm.getPlusNumber());
			// MBO-7310
			if (!StringUtils.equalsIgnoreCase(parentSiteId, MGOZAF)) {
				List<String> dialingCode = ManagerFactory
						.createEMTDailingCodeInfoCacheManager()
						.dailingCodeInfo("dailingCode");
				consumerProfile.setCountryCodeList(dialingCode);
				inputForm.setCountryCodeList(consumerProfile
						.getCountryCodeList());
			} else if (StringUtils.equalsIgnoreCase(parentSiteId, MGOZAF)) {
				StringBuilder dialingCode = new StringBuilder();
				dialingCode.append(null != consumerProfile
						.getPhoneCountryCode() ? consumerProfile
						.getPhoneCountryCode() + "-" : null);
				dialingCode.append(consumerProfile.getPhoneDialingCode());
				inputForm.setPhoneCountryCode(dialingCode.toString());
			} else
				LOGGER.info("PartnerSiteID: "+consumerProfile.getPartnerSiteId());
			inputForm = hasStatesProvincesDet(inputForm);
			inputForm.setIsoCountryCode(consumerProfile.getAddress()
					.getIsoCountryCode());
			Map stateProvincesMap = profileService
					.getStatesProvinces(consumerProfile.getAddress()
							.getIsoCountryCode());
			LOGGER.info("stateProvincesMap" + stateProvincesMap);
			List<String> statesProvincesList = convertMapToLabelValueListSorted(profileService
					.getStatesProvinces(consumerProfile.getAddress()
							.getIsoCountryCode()));
			LOGGER.info("statesProvincesList" + statesProvincesList);
			inputForm.setStatesProvincesList(statesProvincesList);
			
			/* Upon click of Update button */
			if (inputForm.getUpdateSubmit() != null) {
				UserProfile userProfile = getUserProfile(request);
				if (userProfile.hasPermission("duplicateProfileOverride")) {
					request.setAttribute("duplicateProfileOverride", "Y");
				}
				// Address Field Validation for respective country using CustomerProfileValidatorFactory
				CustomerProfileValidator validator = CustomerProfileValidatorFactory
						.create(parentSiteId, inputForm, userProfile);
				errors = validator.validate(errors);

				request.setAttribute("allowDuplicate", "N");
				if (errors.isEmpty()) {
					LOGGER.info("errors object is empty");
					if (consumerProfile != null) {
						// Add profile change comment
						// UCP:553
						commentList = buildCommentText(request,
								consumerProfile, inputForm, errors);
						LOGGER.info("Is CommentList empty: "
								+ commentList.isEmpty());
						LOGGER.info("Is Errors empty: " + errors.isEmpty());
						if (!commentList.isEmpty() && errors.isEmpty()) {
							// MBO-3371:Fix EMTAdmin updates to only call, UCP924
							process(request, session, errors, inputForm,
									consumerProfile, commentList, addressSaved,
									userId);
							
							if(errors.isEmpty())
								forward = mapping.findForward(FORWARD_SUCCESS);
							else
								forward = mapping.findForward(FORWARD_FAILURE);
						} else if (!errors.isEmpty()) {
							forward = mapping.findForward(FORWARD_FAILURE);
						} else {
							errors.add("headerMessage", new ActionMessage(
									"msg.profile.not.changed"));
							forward = mapping.findForward(FORWARD_SUCCESS);
						}
						session.setAttribute("consumerProfile", null);
					} else {
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
								"error.update.consumer.profile.failed"));
					}
				}
				else
				{
					LOGGER.info("errors object has error");
				}
			} else {
				inputForm.setState(consumerProfile.getAddress().getState());
				if (consumerProfile != null) {
					inputForm = initializeInputForm(consumerProfile, inputForm);
					
				}
				if (inputForm != null) {
					session.setAttribute("consumerProfile", consumerProfile);
					request.setAttribute("updateCustomerProfileForm", inputForm);
					request.setAttribute("partnerSiteId",
							inputForm.getPartnerSiteId());
					
					
				} else {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"error.update.consumer.profile.failed"));
				}
				
			}
			
			
		} catch (DuplicateActiveProfileException dup) {
			request.setAttribute("commentList", commentList);
			request.setAttribute("allowDuplicate", "Y");
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.error("Update consumer profile failed", dup);
			errors.add("duplicateProfileOverrideErr", new ActionError(
					"errors.duplicate.profile.update"));
			consumerProfile = (ConsumerProfile) session
					.getAttribute("consumerProfile");
			inputForm = initializeInputForm(consumerProfile, inputForm);
		} catch (Exception exception) {
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
			.error("Update consumer profile failed", exception);
		    if(exception instanceof ServiceException && PHONE_NUMBER_ALREADY_EXIST
							.equalsIgnoreCase(((ServiceException) exception).getErrorCode())) {
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.phoneNumber.already.exist"));
			} else {
		    		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
					"error.update.consumer.profile.failed"));
			}
		}
		if (!errors.isEmpty()) {
			saveErrors(request, errors);
		}
		
		
		return forward;
	}

	private boolean isContactEdited(UpdateCustomerProfileForm inputForm,
			ConsumerProfile consumerProfile) {
		// phoneCountryCode, phoneNumber, phoneNumberType
		String enteredPhoneCountryCode = null != inputForm
				.getPhoneCountryCode() ? inputForm.getPhoneCountryCode()
				.substring(0, inputForm.getPhoneCountryCode().indexOf("-"))
				: null;

		if(isFieldEdited(enteredPhoneCountryCode, consumerProfile.getPhoneCountryCode())
				|| isFieldEdited(inputForm.getPhoneNumber(), consumerProfile.getPhoneNumber())
				|| isFieldEdited(inputForm.getPhoneNumberType(), consumerProfile.getPhoneNumberType()))
			return true;
		else
			return false;
	}

	private boolean isAddressEdited(UpdateCustomerProfileForm inputForm,
			ConsumerProfile consumerProfile) {
		// AddressLine1, AddressLine2, AddressLine3, AddressBuildingName, City,
		// state, postalCode, Country
		if (isFieldEdited(inputForm.getAddressLine1(),
				consumerProfile.getAddressLine1())
				|| isFieldEdited(inputForm.getAddressLine2(),
						consumerProfile.getAddressLine2())
				|| isFieldEdited(inputForm.getAddressLine3(),
						consumerProfile.getAddressLine3())
				|| isFieldEdited(inputForm.getCity(), consumerProfile.getCity())
				|| isFieldEdited(inputForm.getState(),
						consumerProfile.getState())
				|| isFieldEdited(inputForm.getPostalCode(),
						consumerProfile.getPostalCode())
				|| isFieldEdited(inputForm.getCounty(),
						consumerProfile.getCountyName())) {
			return true;
		}
		return false;
	}

	private boolean isAnyNameEdited(UpdateCustomerProfileForm inputForm,
			ConsumerProfile consumerProfile) {
		// firstName, middleName, lastName, SecondLastName
		boolean isFirstNameEdited = isFieldEdited(inputForm.getCustFirstName(), consumerProfile.getFirstName());
		boolean isMiddleNameEdited = isFieldEdited(inputForm.getMiddleName(), consumerProfile.getMiddleName());
		boolean isLastNameEdited = isFieldEdited(inputForm.getCustLastName(), consumerProfile.getLastName());
		boolean isScondLastNameEdited = isFieldEdited(inputForm.getCustSecondLastName(), consumerProfile.getSecondLastName());

		LOGGER.info("isAnyNameEdited: isFirstNameEdited: " + isFirstNameEdited
				+ " isMiddleNameEdited: " + isMiddleNameEdited
				+ " isLastNameEdited: " + isLastNameEdited
				+ " isScondLastNameEdited: " + isScondLastNameEdited);
		if (isFirstNameEdited || isMiddleNameEdited	|| isLastNameEdited || isScondLastNameEdited) 
			return true;
		else
			return false;
	}	

	private boolean isCRSEdited(UpdateCustomerProfileForm inputForm,
			ConsumerProfile consumerProfile) {
		// PhoneNumber, PhoneNumberType, AddressLine1, AddressLine2, 
		// AddressLine3, City, State, PostalCode
		if (isFieldEdited(inputForm.getAddressLine1(),
				consumerProfile.getAddressLine1())
				|| isFieldEdited(inputForm.getPhoneNumber(),
						consumerProfile.getPhoneNumber())
				|| isFieldEdited(inputForm.getPhoneNumberType(),
						consumerProfile.getPhoneNumberType())
				|| isFieldEdited(inputForm.getAddressLine1(),
						consumerProfile.getAddressLine1())
				|| isFieldEdited(inputForm.getAddressLine2(),
						consumerProfile.getAddressLine2())
				|| isFieldEdited(inputForm.getAddressLine3(),
						consumerProfile.getAddressLine3())
				|| isFieldEdited(inputForm.getCity(), consumerProfile.getCity())
				|| isFieldEdited(inputForm.getState(),
						consumerProfile.getState())
				|| isFieldEdited(inputForm.getPostalCode(),
						consumerProfile.getPostalCode())) {
			return true;
		}
		return false;
	}
	
	private boolean isFieldEdited(String inputData, String existingData) {
		if (!StringUtils.equalsIgnoreCase(StringUtils.stripToEmpty(inputData),
				StringUtils.stripToEmpty(existingData))) {
			return true;
		}
		return false;
	}

	private void process(HttpServletRequest request, HttpSession session,
			ActionErrors errors, UpdateCustomerProfileForm inputForm,
			ConsumerProfile consumerProfile, ArrayList<String> commentList,
			boolean addressSaved, String userId) throws Exception {
		
		//taskToUpdateAnyNameOrContact(inputForm, consumerProfile);
		if(isAddressEdited(inputForm, consumerProfile)){
		taskToUpdateAddress(inputForm, consumerProfile, commentList,
				addressSaved);}//direct call to OSL updateAddress
		//taskToUpdateOtherFields(inputForm, consumerProfile);
		//taskToUpdatePlusNumber(inputForm, consumerProfile, commentList, errors,
				//userId, addressSaved);
		//taskToUpdateDOB(inputForm, consumerProfile, commentList, errors, userId);		
		
		UpdateConsumerProfileResponse updateConsumerProfileResponse = callMGOConsumerService(inputForm, consumerProfile);
		if(updateConsumerProfileResponse.getError()!= null && updateConsumerProfileResponse.getError().getCode()!= null){
			if(updateConsumerProfileResponse.getError().getCode().equalsIgnoreCase(PLUS_NUMBER_INVALID)){			
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.invalid.plusnumber.mdm"));
			}
			else if(updateConsumerProfileResponse.getError().getCode().equalsIgnoreCase(PLUS_NUMBER_NO_MATCH)){
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.nomatch.plusnumber.mdm"));
			}
		}
	}

	private void taskToUpdateAddress(UpdateCustomerProfileForm inputForm,
			ConsumerProfile consumerProfile, ArrayList<String> commentList,
			boolean addressSaved) {
		// Update Address Implementation through OSL Update
		
		LOGGER.info("Address is edited");
		Consumer consumer = new Consumer();
		consumer.setConsumerId((long) inputForm.getCustId());
		consumer.setSourceSite(inputForm.getPartnerSiteId());
		ConsumerAddress consumerAddress = new ConsumerAddress();
		consumerAddress.setLine1(inputForm.getAddressLine1());
		consumerAddress.setLine2(inputForm.getAddressLine2());
		consumerAddress.setLine3(inputForm.getAddressLine3());		
		consumerAddress.setCity(inputForm.getCity());
		consumerAddress.setState(inputForm.getState());
		consumerAddress.setZipCode(inputForm.getPostalCode());
		consumerAddress.setCounty(inputForm.getCounty());
		consumerAddress.setAddressId((long) consumerProfile.getAddress()
				.getAddressId());
		consumerAddress.setCountry(consumerProfile.getIsoCountryCode());
		consumer.setProfileAddress(consumerAddress);

		if (inputForm.getAllowOverride()) {
			commentList.add("Duplicate Override - Profile Edit");
		}
		Long status = updateAddressThroughOSL(consumer);
		if (null != status && (status.equals(200L) || status.equals(202L))) {
			addressSaved = true;
		}
	
	}

	private UpdateConsumerProfileResponse callMGOConsumerService(
			UpdateCustomerProfileForm inputForm,
			ConsumerProfile consumerProfile) throws Exception {
		boolean isNameEdited = isAnyNameEdited(inputForm, consumerProfile);
		boolean isContactEdited = isContactEdited(inputForm, consumerProfile);
		boolean isLoyaltyEdited = isFieldEdited(
				inputForm.getLoyaltyPgmMembershipId(),
				consumerProfile.getLoyaltyPgmMembershipId());
		boolean isCRSEdited = isCRSEdited(inputForm, consumerProfile);
		DateFormatter formatter = new DateFormatter(DATE_FORMAT, true);
		boolean isDOBEdited = isFieldEdited(getDateOfBirth(inputForm.getBirthDateText()),
				formatter.format(consumerProfile.getBirthdate()));
		// MGO-9024, MBO-4907
		UpdateConsumerProfileRequest updateConsumerProfileRequest = new UpdateConsumerProfileRequest();
		PersonalInfo personalInfo = new PersonalInfo();
		Consumer consumer = new Consumer();
		consumer.setLoginId(consumerProfile.getUserId());
		consumer.setSourceSite(inputForm.getPartnerSiteId());
		consumer.setConsumerId(Long.valueOf(inputForm.getCustId()));

		// if plus number edited
		if (isLoyaltyEdited) {
			LoyaltyInfo loyalty = new LoyaltyInfo();
			loyalty.setPlusNumber(null != inputForm.getLoyaltyPgmMembershipId() ? inputForm
					.getLoyaltyPgmMembershipId() : null);
			consumer.setLoyalty(loyalty);
		}

		// if any name edited
		if (isNameEdited || isContactEdited) {
			personalInfo.setFirstName(inputForm.getCustFirstName());
			personalInfo.setMiddleName(inputForm.getMiddleName());
			personalInfo.setLastName(inputForm.getCustLastName());
			personalInfo.setSecondLastName(inputForm.getCustSecondLastName());
			consumer.setPersonal(personalInfo);
		}

		// if any contact edited, MBO-4734
		if (isContactEdited) {
			Contact contact = new Contact();
			contact.setPrimaryPhone(inputForm.getPhoneNumber());
			if (ConsumerProfile.MOBILE_PHONE_TYPE.equalsIgnoreCase(inputForm
					.getPhoneNumberType())) {
				contact.setPrimaryPhoneType(MOBILE_CODE);
			} else {
				contact.setPrimaryPhoneType(HOME_CODE);
			}
			String phoneCtyCode = inputForm.getPhoneCountryCode();
			if (null != phoneCtyCode && phoneCtyCode.contains("-")) {
				String[] countryCodeArr = phoneCtyCode.split("-");
				contact.setPhoneCountryCode(countryCodeArr[0]);
				if (null != countryCodeArr[1])
					contact.setPhoneCountryDialingCode(Integer
							.valueOf(countryCodeArr[1]));
			}
			consumer.setContact(contact);
		}

		// if address is edited MBO-4734, PAN-213
		if (isCRSEdited) {
			ConsumerAddress consumerAddress = new ConsumerAddress();
			consumerAddress.setLine1(inputForm.getAddressLine1());
			consumerAddress.setLine2(inputForm.getAddressLine2());
			consumerAddress.setLine3(inputForm.getAddressLine3());
			consumerAddress.setState(inputForm.getState());
			consumerAddress.setCity(inputForm.getCity());
			consumerAddress.setZipCode(inputForm.getPostalCode());
			consumerAddress.setCountry(inputForm.getIsoCountryCode());
			consumer.setProfileAddress(consumerAddress);
			
			//MBO-9647 fix- As the updateCRSProfile Command in services require the plusNumber in input 
			LoyaltyInfo loyalty = new LoyaltyInfo();
			loyalty.setPlusNumber(null != inputForm.getLoyaltyPgmMembershipId() ? inputForm
					.getLoyaltyPgmMembershipId() : null);
			consumer.setLoyalty(loyalty);
		}

		// if DOB Edited
		if (isDOBEdited) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(formatter.parse(getDateOfBirth(inputForm.getBirthDateText())));
			personalInfo.setDateOfBirth(calendar);
			consumer.setPersonal(personalInfo);
		}

		UpdateConsumerProfileTask[] updateConsumerProfileTaskArray = getTaskArray(
				isNameEdited, isContactEdited, isLoyaltyEdited, isCRSEdited,
				isDOBEdited);
		updateConsumerProfileRequest
				.setUpdateTasks(updateConsumerProfileTaskArray);
		updateConsumerProfileRequest.setConsumer(consumer);
		updateConsumerProfileRequest
				.setSourceApplication(UPDATE_CONSUMER_PROFILE_SOURCE_APP);
		
		ConsumerServiceProxy consumerServiceProxy = ConsumerServiceProxyImpl
				.getInstance();
		
		UpdateConsumerProfileResponse updateConsumerProfileResponse = null;
		updateConsumerProfileResponse = consumerServiceProxy.updateConsumerProfile(
				updateConsumerProfileRequest,
				consumerProfile.getPartnerSiteId());
		return updateConsumerProfileResponse;
	}
	
	private UpdateConsumerProfileTask[] getTaskArray(boolean isNameEdited,
			boolean isContactEdited, boolean isLoyaltyEdited,
			boolean isCRSEdited, boolean isDOBEdited) {
		UpdateConsumerProfileTask[] updateConsumerProfileTaskArray = new UpdateConsumerProfileTask[5];

		// set the task acc to what got edited
		for (int i = 0; i < updateConsumerProfileTaskArray.length; i++) {
			if (isNameEdited) {
				updateConsumerProfileTaskArray[i] = UpdateConsumerProfileTask
						.fromValue(UpdateConsumerProfileTask._UpdateName);
				isNameEdited = false;
				LOGGER.info("updateConsumerProfileTaskArray[" + i + "]: "
						+ updateConsumerProfileTaskArray[i]);
			} else if (isContactEdited) {
				updateConsumerProfileTaskArray[i] = UpdateConsumerProfileTask
						.fromValue(UpdateConsumerProfileTask._UpdateContact);
				isContactEdited = false;
				LOGGER.info("updateConsumerProfileTaskArray[" + i + "]: "
						+ updateConsumerProfileTaskArray[i]);
			} else if (isLoyaltyEdited) {
				updateConsumerProfileTaskArray[i] = UpdateConsumerProfileTask
						.fromValue(UpdateConsumerProfileTask._UpdateLoyalty);
				isLoyaltyEdited = false;
				LOGGER.info("updateConsumerProfileTaskArray[" + i + "]: "
						+ updateConsumerProfileTaskArray[i]);
			} else if (isCRSEdited) {
				updateConsumerProfileTaskArray[i] = UpdateConsumerProfileTask
						.fromValue(UpdateConsumerProfileTask._UpdateCRSProfile);
				isCRSEdited = false;
				LOGGER.info("updateConsumerProfileTaskArray[" + i + "]: "
						+ updateConsumerProfileTaskArray[i]);
			} else if (isDOBEdited) {
				updateConsumerProfileTaskArray[i] = UpdateConsumerProfileTask
						.fromValue(UpdateConsumerProfileTask._UpdateDateOfBirth);
				isDOBEdited = false;
				LOGGER.info("updateConsumerProfileTaskArray[" + i + "]: "
						+ updateConsumerProfileTaskArray[i]);
			}
		}

		return updateConsumerProfileTaskArray;
	}
	
	private UpdateCustomerProfileForm initializeInputForm(
			ConsumerProfile consumerProfile, UpdateCustomerProfileForm inputFormParam) {
			UpdateCustomerProfileForm inputForm = new UpdateCustomerProfileForm();
		try {
			
			inputForm.setCustId(consumerProfile.getId());
			inputForm.setCustFirstName(consumerProfile.getFirstName());
			inputForm.setCustLastName(consumerProfile.getLastName());
			inputForm.setMiddleName(consumerProfile.getMiddleName());
			inputForm
					.setCustSecondLastName(consumerProfile.getSecondLastName());
			DateFormatter df = new DateFormatter(DATE_FORMAT, true);
			inputForm
					.setBirthDateText(getDateOfBirth(df.format(consumerProfile.getBirthdate())));
			inputForm.setAddressLine1(consumerProfile.getAddressLine1());
			inputForm.setAddressLine2(consumerProfile.getAddressLine2());
			inputForm.setAddressLine3(consumerProfile.getAddressLine3());			
			inputForm.setCity(consumerProfile.getCity());
			inputForm.setState(consumerProfile.getState());
			inputForm.setIsoCountryCode(consumerProfile.getIsoCountryCode());
			inputForm.setCounty(consumerProfile.getCountyName());
			inputForm.setPostalCode(consumerProfile.getPostalCode());

			String postalCode = consumerProfile.getPostalCode()
					+ consumerProfile.getZip4();
			// MBO-3603
			int dashPosition = postalCode.indexOf('-');
			if (dashPosition != -1) {
				inputForm.setPostalMini1(postalCode.substring(0, dashPosition));
				inputForm
						.setPostalMini2(postalCode.substring(dashPosition + 1));
			} else {
				inputForm.setPostalMini1(postalCode.substring(0,
						postalCode.length() / 2));
				inputForm.setPostalMini2(postalCode.substring(postalCode
						.length() / 2));
			}
			// MBO-3603
			inputForm.setZip4(consumerProfile.getZip4());
			inputForm.setPhoneNumber(consumerProfile.getPhoneNumber());
			inputForm.setPlusNumber(consumerProfile
					.getLoyaltyPgmMembershipId());
			inputForm.setLoyaltyPgmMembershipId(consumerProfile
					.getLoyaltyPgmMembershipId());
			inputForm.setSavePhoneNumber(consumerProfile.getPhoneNumber());
			if (!StringHelper.isNullOrEmpty(consumerProfile.getPhoneNumber())) {
				inputForm.setPhoneNumberType(consumerProfile
						.getPhoneNumberType());
				if (consumerProfile.isPhoneBlocked()) {
					inputForm
							.setPhoneNumberTaintText(TaintIndicatorType.BLOCKED_TEXT);
				} else if (consumerProfile.isPhoneNotBlocked()) {
					inputForm
							.setPhoneNumberTaintText(TaintIndicatorType.NOT_BLOCKED_TEXT);
				} else if (consumerProfile.isPhoneOverridden()) {
					inputForm
							.setPhoneNumberTaintText(TaintIndicatorType.OVERRIDE_TEXT);
				} else {
					inputForm.setPhoneNumberTaintText("Unknown");
				}
			}

			inputForm.setPhoneNumberAlternate(consumerProfile
					.getPhoneNumberAlternate());
			inputForm.setPhoneNumberAlternateType(consumerProfile
					.getPhoneNumberAlternateType());

			inputForm.setSavePhoneNumberAlternate(consumerProfile
					.getPhoneNumberAlternate());
			if (!StringHelper.isNullOrEmpty(consumerProfile
					.getPhoneNumberAlternate())) {
				if (consumerProfile.isPhoneAlternateBlocked()) {
					inputForm
							.setPhoneNumberAlternateTaintText(TaintIndicatorType.BLOCKED_TEXT);
				} else if (consumerProfile.isPhoneAlternateNotBlocked()) {
					inputForm
							.setPhoneNumberAlternateTaintText(TaintIndicatorType.NOT_BLOCKED_TEXT);
				} else if (consumerProfile.isPhoneAlternateOverridden()) {
					inputForm
							.setPhoneNumberAlternateTaintText(TaintIndicatorType.OVERRIDE_TEXT);
				} else {
					inputForm.setPhoneNumberAlternateTaintText("Unknown");
				}
			}
			inputForm.setPartnerSiteId(consumerProfile.getPartnerSiteId());
			// MBO-7310

			inputForm.setCountryCodeList(consumerProfile.getCountryCodeList());
			inputForm.setPhoneCountryCode(consumerProfile.getPhoneCountryCode()
					+ "-" + consumerProfile.getPhoneDialingCode());
			inputForm = hasStatesProvincesDet(inputForm);
			inputForm.setStatesProvincesList(inputFormParam.getStatesProvincesList());
			inputForm.setState(inputFormParam.getState());

		} catch (Exception e) {
			return null;
		}
		return inputForm;
	}

	private StringBuilder addCommentText(StringBuilder strBuf, String comment) {
		if (strBuf != null && strBuf.length() > 0) {
			strBuf.append(", ");
		}
		return strBuf.append(comment);
	}

	private ArrayList<String> buildCommentText(HttpServletRequest request,
			ConsumerProfile profile, UpdateCustomerProfileForm form,
			ActionErrors errors) {

		StringBuilder strBuf = new StringBuilder();
		StringBuilder detailComment = new StringBuilder(255);
		StringBuilder detailText = new StringBuilder(100);
		ArrayList<String> commentList = new ArrayList<String>();
		boolean delimiterFlag = false;
		String formPhoneType;
		int custId = form.getCustId();
		ConsumerProfileComment comment = new ConsumerProfileComment();
		comment.setCustId(custId);
		comment.setReasonCode("OTH");

		if (profile.getFirstName() != null
				&& profile.getLastName() != null
				&& (!profile.getFirstName().equals(
						form.getCustFirstName().trim()) || !profile
						.getLastName().equals(form.getCustLastName().trim()))) {
			strBuf.append("Name Changed from " + profile.getFirstName() + " "
					+ profile.getLastName() + " to " + form.getCustFirstName()
					+ " " + form.getCustLastName());

			detailComment.append("Name Changed from '" + profile.getFirstName()
					+ " " + profile.getLastName() + "' to '"
					+ form.getCustFirstName() + " " + form.getCustLastName()
					+ "'");

			detailText.append("<FROM_FIRSTNAME>" + profile.getFirstName()
					+ "</FROM_FIRSTNAME>" + "<TO_FIRSTNAME>"
					+ form.getCustFirstName() + "</TO_FIRSTNAME>"
					+ "<FROM_LASTNAME>" + profile.getLastName()
					+ "</FROM_LASTNAME>" + "<TO_LASTNAME>"
					+ form.getCustLastName() + "</TO_LASTNAME>");
		}

		if (detailComment.length() > 0) {
			commentList.add(detailComment.toString());
		}
		detailComment = new StringBuilder(255);

		if (profile.getMiddleName() == null) {
			if (form.getMiddleName() != null
					&& !("").equals(form.getMiddleName())) {
				delimiterFlag = true;
				addCommentText(strBuf, "Middle Name");
				detailComment.append("Added Middle Name - '"
						+ form.getMiddleName() + "'");
				detailText.append("<FROM_MIDDLENAME>" + ""
						+ "</FROM_MIDDLENAME>" + "<TO_MIDDLENAME>"
						+ form.getMiddleName() + "</TO_MIDDLENAME>");
			}
		} else {
			if (!profile.getMiddleName().equals(form.getMiddleName().trim())) {
				delimiterFlag = true;
				addCommentText(strBuf, "Middle Name");
				detailComment.append("Middle Name Changed from '"
						+ profile.getMiddleName() + "' to '"
						+ form.getMiddleName() + "'");
				detailText.append("<FROM_MIDDLENAME>" + profile.getMiddleName()
						+ "</FROM_MIDDLENAME>" + "<TO_MIDDLENAME>"
						+ form.getMiddleName() + "</TO_MIDDLENAME>");
			}
		}

		if (detailComment.length() > 0) {
			commentList.add(detailComment.toString());
		}
		detailComment = new StringBuilder(255);

		if (profile.getSecondLastName() == null) {
			if (form.getCustSecondLastName() != null
					&& !("").equals(form.getCustSecondLastName())) {
				delimiterFlag = true;
				addCommentText(strBuf, "Second Last Name");
				detailComment.append("Added Second Last Name - '"
						+ form.getCustSecondLastName() + "'");
				detailText
						.append("<FROM_SECONDLASTNAME>" + ""
								+ "</FROM_SECONDLASTNAME>"
								+ "<TO_SECONDLASTNAME>"
								+ form.getCustSecondLastName()
								+ "</TO_SECONDLASTNAME>");
			}
		} else {
			if (!profile.getSecondLastName().equals(
					form.getCustSecondLastName().trim())) {
				delimiterFlag = true;
				addCommentText(strBuf, "Second Last Name");
				detailComment.append("Second Last Name Changed from '"
						+ profile.getSecondLastName() + "' to '"
						+ form.getCustSecondLastName() + "'");
				detailText
						.append("<FROM_SECONDLASTNAME>"
								+ profile.getSecondLastName()
								+ "</FROM_SECONDLASTNAME>"
								+ "<TO_SECONDLASTNAME>"
								+ form.getCustSecondLastName()
								+ "</TO_SECONDLASTNAME>");
			}
		}

		if (detailComment.length() > 0) {
			commentList.add(detailComment.toString());
		}
		detailComment = new StringBuilder(255);

		if (profile.getAddressLine1() == null) {
			if (form.getAddressLine1() != null
					&& !("").equals(form.getAddressLine1())) {
				delimiterFlag = true;
				addCommentText(strBuf, "Address");
				detailComment.append("Address Line1 Changed from '" + ""
						+ "' to '" + form.getAddressLine1() + "'");
				detailText.append("<FROM_ADDRESS>" + "" + "</FROM_ADDRESS>"
						+ "<TO_ADDRESS>" + form.getAddressLine1()
						+ "</TO_ADDRESS>");
			}
		} else {
			if (!profile.getAddressLine1()
					.equals(form.getAddressLine1().trim())) {
				delimiterFlag = true;
				addCommentText(strBuf, "Address");
				detailComment.append("Address Line1 Changed from '"
						+ profile.getAddressLine1() + "' to '"
						+ form.getAddressLine1() + "'");
				detailText.append("<FROM_ADDRESS>" + profile.getAddressLine1()
						+ "</FROM_ADDRESS>" + "<TO_ADDRESS>"
						+ form.getAddressLine1() + "</TO_ADDRESS>");
			}
		}

		if (delimiterFlag) {
			detailComment.append(", ");
			delimiterFlag = false;
		}

		if (profile.getAddressLine2() == null) {
			if (form.getAddressLine2() != null
					&& !("").equals(form.getAddressLine2())) {
				delimiterFlag = true;
				addCommentText(strBuf, "Address 2");
				detailComment.append("Address Line2 Changed from '" + ""
						+ "' to '" + form.getAddressLine2() + "'");
				detailText.append("<FROM_ADDRESS2>" + "" + "</FROM_ADDRESS2>"
						+ "<TO_ADDRESS2>" + form.getAddressLine2()
						+ "</TO_ADDRESS2>");
			}
		} else {
			if (!profile.getAddressLine2()
					.equals(form.getAddressLine2().trim())) {
				delimiterFlag = true;
				addCommentText(strBuf, "Address 2");
				detailComment.append("Address Line2 Changed from '"
						+ profile.getAddressLine2() + "' to '"
						+ form.getAddressLine2() + "'");
				detailText.append("<FROM_ADDRESS2>" + profile.getAddressLine2()
						+ "</FROM_ADDRESS2>" + "<TO_ADDRESS2>"
						+ form.getAddressLine2() + "</TO_ADDRESS2>");
			}
		}
		if (detailComment.length() > 0) {
			commentList.add("ADDRESS");
		}
		detailComment = new StringBuilder(255);

		/* Added for MBO-5499 */
		if (profile.getAddressLine3() == null) {
			if (form.getAddressLine3() != null
					&& !("").equals(form.getAddressLine3())) {
				delimiterFlag = true;
				addCommentText(strBuf, "Address 3");
				detailComment.append("Address Line3 Changed from '" + ""
						+ "' to '" + form.getAddressLine3() + "'");
				detailText.append("<FROM_ADDRESS3>" + "" + "</FROM_ADDRESS3>"
						+ "<TO_ADDRESS3>" + form.getAddressLine3()
						+ "</TO_ADDRESS3>");
			}
		} else {
			if (!profile.getAddressLine3()
					.equals(form.getAddressLine3().trim())) {
				delimiterFlag = true;
				addCommentText(strBuf, "Address 3");
				detailComment.append("Address Line3 Changed from '"
						+ profile.getAddressLine3() + "' to '"
						+ form.getAddressLine3() + "'");
				detailText.append("<FROM_ADDRESS3>" + profile.getAddressLine3()
						+ "</FROM_ADDRESS3>" + "<TO_ADDRESS3>"
						+ form.getAddressLine3() + "</TO_ADDRESS3>");
			}
		}
		if (detailComment.length() > 0) {
			commentList.add("ADDRESS");
		}
		
		detailComment = new StringBuilder(255);

		if (profile.getCity() == null) {
			if (form.getCity() != null && !("").equals(form.getCity())) {
				delimiterFlag = true;
				addCommentText(strBuf, "City");
				detailComment.append("City Changed from '" + "" + "' to '"
						+ form.getCity() + "'");
				detailText.append("<FROM_CITY>" + "" + "</FROM_CITY><TO_CITY>"
						+ form.getCity() + "</TO_CITY>");
			}
		} else {
			if (!profile.getCity().equals(form.getCity().trim())) {
				delimiterFlag = true;
				addCommentText(strBuf, "City");
				detailComment.append("City Changed from '" + profile.getCity()
						+ "' to '" + form.getCity() + "'");
				detailText.append("<FROM_CITY>" + profile.getCity()
						+ "</FROM_CITY>" + "<TO_CITY>" + form.getCity()
						+ "</TO_CITY>");
			}
		}
		if (delimiterFlag) {
			detailComment.append(", ");
			delimiterFlag = false;
		}
		if (profile.getState() == null) {
			if (form.getState() != null && !("").equals(form.getState())) {
				delimiterFlag = true;
				addCommentText(strBuf, "State");
				detailComment.append("State Changed from '" + "" + "' to '"
						+ form.getState() + "'");
				detailText.append("<FROM_STATE>" + "" + "</FROM_STATE>"
						+ "<TO_STATE>" + form.getState() + "</TO_STATE>");
			}
		} else {
			if (!profile.getState().equals(form.getState().trim())) {
				delimiterFlag = true;
				addCommentText(strBuf, "State");
				detailComment
						.append("State Changed from '" + profile.getState()
								+ "' to '" + form.getState() + "'");
				detailText.append("<FROM_STATE>" + profile.getState()
						+ "</FROM_STATE>" + "<TO_STATE>" + form.getState()
						+ "</TO_STATE>");
			}
		}

		if (delimiterFlag) {
			detailComment.append(", ");
			delimiterFlag = false;
		}

		if (profile.getPostalCode() == null) {
			if (form.getPostalCode() != null
					&& !("").equals(form.getPostalCode())) {
				delimiterFlag = true;
				addCommentText(strBuf, "Zip");
				detailComment.append("Zip Changed from '" + "" + "' to '"
						+ form.getPostalCode() + "'");
				detailText.append("<FROM_ZIP>" + "" + "</FROM_ZIP>"
						+ "<TO_ZIP>" + form.getPostalCode() + "</TO_ZIP>");
			}
		} else {
			if (!profile.getPostalCode().equals(form.getPostalCode().trim())) {
				delimiterFlag = true;
				addCommentText(strBuf, "Zip");
				detailComment.append("Zip Changed from '"
						+ profile.getPostalCode() + "' to '"
						+ form.getPostalCode() + "'");
				detailText.append("<FROM_ZIP>" + profile.getPostalCode()
						+ "</FROM_ZIP>" + "<TO_ZIP>" + form.getPostalCode()
						+ "</TO_ZIP>");
			}
		}
		if (delimiterFlag) {
			detailComment.append(", ");
			delimiterFlag = false;
		}
		if (profile.getZip4() == null) {
			if (form.getZip4() != null && !("").equals(form.getZip4())) {
				delimiterFlag = true;
				addCommentText(strBuf, "Zip4");
				detailComment.append("Zip4 Changed from '" + "" + "' to '"
						+ form.getZip4() + "'");
				detailText.append("<FROM_ZIP4>" + "" + "</FROM_ZIP4>"
						+ "<TO_ZIP4>" + form.getZip4() + "</TO_ZIP4>");
			}
		} else {
			if (!profile.getZip4().equals(form.getZip4().trim())) {
				delimiterFlag = true;
				addCommentText(strBuf, "Zip4");
				detailComment.append("Zip4 Changed from '" + profile.getZip4()
						+ "' to '" + form.getZip4() + "'");
				detailText.append("<FROM_ZIP4>" + profile.getZip4()
						+ "</FROM_ZIP4>" + "<TO_ZIP4>" + form.getZip4()
						+ "</TO_ZIP4>");
			}
		}
		if (detailComment.length() > 0) {
			commentList.add("ADDRESS");
		}
		detailComment = new StringBuilder(255);
		delimiterFlag = false;
		try {
			DateFormatter formatter = new DateFormatter(DATE_FORMAT, true);
			String profDateText = formatter.format(profile.getBirthdate());
			if (!profDateText.equalsIgnoreCase(getDateOfBirth(form.getBirthDateText()))) {
				delimiterFlag = true;
				addCommentText(strBuf, "BirthDate ");
				detailComment.append("BirthDate Changed from '" + profDateText
						+ "' to '" + getDateOfBirth(form.getBirthDateText()) + "'");
				detailText.append("<FROM_BIRTHDATE>" + profDateText
						+ "</FROM_BIRTHDATE>" + "<TO_BIRTHDATE>"
						+ getDateOfBirth(form.getBirthDateText()) + "</TO_BIRTHDATE>");

			}
		} catch (Exception ignore) {
		}

		if (profile.getPhoneNumber() == null) {
			if (form.getPhoneNumber() != null
					&& !("").equals(form.getPhoneNumber())) {
				delimiterFlag = true;
				addCommentText(strBuf, "Phone");
				detailComment.append("Phone Changed from '" + "" + "' to '"
						+ form.getPhoneNumber() + "'");
				detailText.append("<FROM_PHONE>" + "" + "</FROM_PHONE>"
						+ "<TO_PHONE>" + form.getPhoneNumber() + "</TO_PHONE>");
			}
		} else {
			if (!profile.getPhoneNumber().equals(form.getPhoneNumber().trim())) {
				delimiterFlag = true;
				addCommentText(strBuf, "Phone");
				detailComment.append("Phone Changed from '"
						+ profile.getPhoneNumber() + "' to '"
						+ form.getPhoneNumber() + "'");
				detailText.append("<FROM_PHONE>" + profile.getPhoneNumber()
						+ "</FROM_PHONE>" + "<TO_PHONE>"
						+ form.getPhoneNumber() + "</TO_PHONE>");
			}
		}

		// MBO-7310
		if (profile.getPhoneCountryCode() == null
				|| profile.getPhoneDialingCode() == null) {
			if (form.getPhoneCountryCode() != null
					&& !("").equals(form.getPhoneCountryCode())) {
				delimiterFlag = true;
				addCommentText(strBuf, "Country Code");
				detailComment.append("Country Code Changed from '" + ""
						+ "' to '" + form.getPhoneCountryCode() + "'");
				detailText.append("<FROM_COUNTRYCODE>" + ""
						+ "</FROM_COUNTRYCODE>" + "<TO_COUNTRYCODE>"
						+ form.getPhoneCountryCode() + "'"
						+ "</TO_COUNTRYCODE>");
			}

		} else {
			if (!(profile.getPhoneCountryCode() + "-" + profile
					.getPhoneDialingCode()).equals(form.getPhoneCountryCode()
					.trim())) {
				delimiterFlag = true;
				addCommentText(strBuf, "Country Code");
				detailComment.append("Country Code Changed from '"
						+ profile.getPhoneCountryCode() + "-"
						+ profile.getPhoneDialingCode() + "' to '"
						+ form.getPhoneCountryCode() + "'");
				detailText.append("<FROM_COUNTRYCODE>" + ""
						+ "</FROM_COUNTRYCODE>" + "<TO_COUNTRYCODE>"
						+ form.getPhoneCountryCode() + "-"
						+ "</TO_COUNTRYCODE>");
			}
		}

		if (delimiterFlag) {
			detailComment.append(", ");
			delimiterFlag = false;
		}
		// Added as part of MBO-4734
		if (!MGOZAF.equalsIgnoreCase(profile.getPartnerSiteId())) {
			if (null != form.getPhoneNumberType()) {
				if (StringUtils.isBlank(profile.getPhoneNumberType())) {
					if (!StringUtils.isBlank(form.getPhoneNumberType())
							&& (!StringUtils.isBlank(form.getPhoneNumber()))) {
						if (ConsumerProfile.HOME_PHONE_TYPE
								.equalsIgnoreCase(form.getPhoneNumberType())) {
							formPhoneType = HOME_CODE;
						} else {
							formPhoneType = MOBILE_CODE;
						}
						addCommentText(strBuf, "Phone Type");
						detailComment.append("Phone Type Changed from '" + ""
								+ "' to '" + formPhoneType + "'");
						detailText.append("<FROM_PHONE>" + "" + "</FROM_PHONE>"
								+ "<TO_PHONE>" + formPhoneType + "</TO_PHONE>");
					}
				} else {
					if (!profile.getPhoneNumberType().equals(
							form.getPhoneNumberType().trim())
							&& (!StringUtils.isBlank(form.getPhoneNumber()))) {
						String codeTypes = changePhoneTypeComment(
								form.getPhoneNumberType(),
								profile.getPhoneNumberType());
						String[] parts = codeTypes.split(",");
						String formType = parts[0];
						String profileType = parts[1];
						addCommentText(strBuf, "Phone Type");
						detailComment.append("Phone Type Changed from '"
								+ profileType + "' to '" + formType + "'");
						detailText.append("<FROM_PHONE>" + profileType
								+ "</FROM_PHONE>" + "<TO_PHONE>" + formType
								+ "</TO_PHONE>");
					}

				}
			}
		}
		boolean invalidUpdate = false;

		if (detailComment.length() > 0) {
			commentList.add(detailComment.toString());
		}
		detailComment = new StringBuilder(255);
		// UCP-333 Reverted back UpdateCRSProfile call
		if (profile.getLoyaltyPgmMembershipId() == null) {
			if (StringUtils.isNotBlank(form.getLoyaltyPgmMembershipId())) {
				if (!commentList.isEmpty()) {
					invalidUpdate = true;
				}
				delimiterFlag = true;
				addCommentText(strBuf, "Plus #");
				detailComment.append("Plus # Changed from '" + "" + "' to '"
						+ form.getLoyaltyPgmMembershipId() + "'");
				detailText.append("<FROM_REWARDS_NUM>" + ""
						+ "</FROM_REWARDS_NUM>" + "<TO_REWARDS_NUM>"
						+ form.getLoyaltyPgmMembershipId()
						+ "</TO_REWARDS_NUM>");
			}

		} else {
			if (!StringUtils.equals(profile.getLoyaltyPgmMembershipId(), 
					form.getLoyaltyPgmMembershipId().trim())) {
				if(form.getLoyaltyPgmMembershipId().equalsIgnoreCase("")){
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
					"error.invalid.plusnumber.mdm"));
				}
				else{
				if (!commentList.isEmpty()) {
					invalidUpdate = true;
				}
				delimiterFlag = true;
				addCommentText(strBuf, "Plus #");
				detailComment.append("Plus # Changed from '"
						+ profile.getLoyaltyPgmMembershipId() + "' to '"
						+ form.getLoyaltyPgmMembershipId() + "'");
				detailText.append("<FROM_REWARDS_NUM>"
						+ profile.getLoyaltyPgmMembershipId()
						+ "</FROM_REWARDS_NUM>" + "<TO_REWARDS_NUM>"
						+ form.getLoyaltyPgmMembershipId()
						+ "</TO_REWARDS_NUM>");
				}
			}
		}

		if (detailComment.length() > 0) {
			commentList.add(detailComment.toString());
		}
		detailComment = new StringBuilder(255);
		if (!invalidUpdate) {
			try {
				super.insertActivityLog(this.getClass(), request,
						detailText.toString(),
						UserActivityType.EVENT_TYPE_CONSUMERPROFILEUPDATE,
						String.valueOf(custId));
			} catch (Exception ignore) {

			}
		} else {
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
					"error.clumpedupdate.plusNumber.otherfields"));
		}

		return commentList;

	}

	private String changePhoneTypeComment(String form, String profile) {
		String formPhoneType;
		String profilePhoneType;
		if (ConsumerProfile.HOME_PHONE_TYPE.equalsIgnoreCase(form)) {
			formPhoneType = HOME_CODE;
		} else {
			formPhoneType = MOBILE_CODE;
		}
		if (ConsumerProfile.HOME_PHONE_TYPE.equalsIgnoreCase(profile)) {
			profilePhoneType = HOME_CODE;
		} else {
			profilePhoneType = MOBILE_CODE;
		}
		StringBuilder sb = new StringBuilder(formPhoneType);
		sb.append(",").append(profilePhoneType);
		return sb.toString();

	}

	private long updateAddressThroughOSL(Consumer consumer) {
		OSLProxyImpl oslProxy = new OSLProxyImpl();
		UpdateOSLAddressResponse response = null;
		try {
			response = oslProxy.updateAddressthroOSL(consumer);
		} catch (OSLException e) {
			LOGGER.error(
					"Exception occoured while trying to save address through OSL",
					e);
		}
		return response.getStatus();
	}
	
	private UpdateCustomerProfileForm hasStatesProvincesDet(UpdateCustomerProfileForm form) throws Exception{
		String partnerSite = form.getPartnerSiteId();
		EMTSharedSourceSiteCacheService proxy = EMTSharedSourceSiteCacheServiceImpl.instance();
		
			if(null != partnerSite){
			GetSourceSiteInfoResponse resp =proxy.sourceSiteDetails(partnerSite);
			NameValuePairType[] dynData  = resp.getDynamicData();
			for(NameValuePairType nameValue : dynData){
				if(nameValue.getName().equalsIgnoreCase("hasStatesProvinces")){
					form.setHasStatesProvinces(nameValue.getValue());
					break;
				}
			}
		}
		return form;
	}

	private String getDateOfBirth(String birthDateText) {
		birthDateText = birthDateText != null ? birthDateText.toUpperCase()
				: null;
		return birthDateText;
	}
}