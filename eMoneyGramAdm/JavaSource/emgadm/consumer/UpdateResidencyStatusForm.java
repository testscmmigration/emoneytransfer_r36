package emgadm.consumer;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class UpdateResidencyStatusForm extends EMoneyGramAdmBaseValidatorForm {
	private String custId;
	private String residentStatus;

	public String getCustId() {
		return custId;
	}

	public void setCustId(String string) {
		custId = string;
	}

	public String getResidentStatus() {
		return residentStatus;
	}

	public void setResidentStatus(String residentStatus) {
		this.residentStatus = residentStatus;
	}

}
