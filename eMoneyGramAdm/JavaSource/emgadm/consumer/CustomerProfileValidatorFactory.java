package emgadm.consumer;

import com.moneygram.mgo.service.config_v3_4.GetSourceSiteInfoResponse;
import com.moneygram.mgo.service.config_v3_4.NameValuePairType;

import emgadm.model.UserProfile;
import emgshared.cache.locator.EMTSharedSourceSiteCacheService;
import emgshared.cache.locator.EMTSharedSourceSiteCacheServiceImpl;
import emgshared.exceptions.EMGRuntimeException;

public class CustomerProfileValidatorFactory {

	public static CustomerProfileValidator create(String parentSiteId,
			UpdateCustomerProfileForm form, UserProfile userProfile) {
		EMTSharedSourceSiteCacheService cacheService = new EMTSharedSourceSiteCacheServiceImpl();
		GetSourceSiteInfoResponse sourceSiteResponse = new GetSourceSiteInfoResponse();
		try {
			sourceSiteResponse = cacheService.sourceSiteDetails(parentSiteId);
		} catch (Exception exception) {
			throw new EMGRuntimeException(exception);
		}
		String countryCode = null != sourceSiteResponse.getCountryInfo() ? (sourceSiteResponse
				.getCountryInfo().getIso3CharCountryCode()) : null;
		
	    NameValuePairType[] dynamicData = sourceSiteResponse.getDynamicData();
		
		return new CustomerProfileGenericValidator(form, userProfile, countryCode, parentSiteId, createRegexPattern(dynamicData));
		/*
		 * if (Constants.COUNTRY_CODE_USA.equals(countryCode)) { return new
		 * USCustomerProfileValidator(form, userProfile); } else if
		 * (Constants.COUNTRY_CODE_UK.equals(countryCode)) { return new
		 * UKCustomerProfileValidator(form, userProfile); } else if
		 * (Constants.COUNTRY_CODE_DE.equals(countryCode)) { return new
		 * DEUCustomerProfileValidator(form, userProfile); }
		 */
		/*
		 * Added for MBO-5499
		 */
		/*
		 * Added for AUS-56 Added a validation for AUS
		 */

		/*
		 * else if (Constants.COUNTRY_CODE_ZAF.equals(countryCode)) { return new
		 * ZAFCustomerProfileValidator(form, userProfile); } else if
		 * (Constants.COUNTRY_CODE_ESP.equals(countryCode)) { return new
		 * ESPCustomerProfileValidator(form, userProfile); } else if
		 * (Constants.COUNTRY_CODE_FRA.equals(countryCode)) { return new
		 * CustomerProfileValidatorFRA(form, userProfile); // CAN-55 } else if
		 * (Constants.COUNTRY_CODE_CAN.equals(countryCode)) { return new
		 * CANCustomerProfileValidator(form, userProfile); } else if
		 * (Constants.COUNTRY_CODE_AUS.equals(countryCode)) { return new
		 * CustomerProfileValidatorAUS(form, userProfile); } else { throw new
		 * IllegalArgumentException(parentSiteId +
		 * " is not a valid parent site id"); }
		 */

	}
	
	private static CustomerProfileRegexPattern createRegexPattern(NameValuePairType[] dynamicData){
		CustomerProfileRegexPattern customerProfileRegex = new CustomerProfileRegexPattern();	    
	    
	 	for(NameValuePairType nvpt : dynamicData){
			if(nvpt.getName().equals(CustomerProfileRegexPattern.FIRST_NAME_REGEX)){
				customerProfileRegex.setFirstNameRegex(nvpt.getValue());
			}
			else if(nvpt.getName().equals(CustomerProfileRegexPattern.MIDDLE_NAME_REGEX)){
				customerProfileRegex.setMiddleNameRegex(nvpt.getValue());
			}
			else if(nvpt.getName().equals(CustomerProfileRegexPattern.LAST_NAME_REGEX)){
				customerProfileRegex.setLastNameRegex(nvpt.getValue());
			}			
			else if(nvpt.getName().equals(CustomerProfileRegexPattern.SECOND_LAST_NAME_REGEX)){
				customerProfileRegex.setSecondLastNameRegex(nvpt.getValue());
			}
			else if(nvpt.getName().equals(CustomerProfileRegexPattern.ADDRESS_LINE_1_REGEX)){
				customerProfileRegex.setAddressLine1Regex(nvpt.getValue());
			}
			else if(nvpt.getName().equals(CustomerProfileRegexPattern.ADDRESS_LINE_2_REGEX)){
				customerProfileRegex.setAddressLine2Regex(nvpt.getValue());
			}
			else if(nvpt.getName().equals(CustomerProfileRegexPattern.ADDRESS_LINE_3_REGEX)){
				customerProfileRegex.setAddressLine3Regex(nvpt.getValue());
			}
			else if(nvpt.getName().equals(CustomerProfileRegexPattern.CITY_REGEX)){
				customerProfileRegex.setCityRegex(nvpt.getValue());
			}
			else if(nvpt.getName().equals(CustomerProfileRegexPattern.POSTAL_CODE_REGEX)){
				customerProfileRegex.setPostalCodeRegex(nvpt.getValue());
			}
		}
	 	return customerProfileRegex;		
	}
}
