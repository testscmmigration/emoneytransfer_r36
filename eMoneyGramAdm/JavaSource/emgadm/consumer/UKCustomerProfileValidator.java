package emgadm.consumer;

import emgadm.model.UserProfile;

public class UKCustomerProfileValidator extends BaseCustomerProfileValidator {

	public UKCustomerProfileValidator(UpdateCustomerProfileForm form,
			UserProfile userProfile, CustomerProfileRegexPattern customerProfileRegex) {
		super(form, userProfile, customerProfileRegex);
		// Commented below two line since error message is thrown twice.
		/*
		 * fields.add(new AlphaField(form.getMiddleName(),
		 * "Middle Name").setMaxLength(20).isRequired(false)); fields.add(new
		 * AlphaField(form.getCustSecondLastName(),
		 * "Second Last Name").setMaxLength(30).isRequired(false));
		 * fields.add(new AlphaNumericField(form.getPostalMini1(),
		 * "First part of Postcode").setMaxLength(4)); fields.add(new
		 * AlphaNumericField(form.getPostalMini2(),
		 * "Second part of Postcode").setMaxLength(8));
		 */// above two to be removed.
			// add postal code 13 max length
		fields.add(new UKPostalCode(form.getPostalCode(), "UKPostcode", customerProfileRegex.getPostalCodeRegex())
				.setMinLength(5).setMaxLength(8));
		// MBO-3063
		fields.add(new AlphaField(form.getCounty(), "County").setMaxLength(40)
				.isRequired(false));
		fields.add(new AddressField(form.getPhoneCountryCode(), "Country Code", customerProfileRegex.getAddressLine1Regex())
				.isRequired(true));
		fields.add(new UntaintedPhoneNumber(form.getPhoneNumber(),
				"Phone Number")
				.setUID(userProfile.getUID())
				.setCountryDialingCode(form.getPhoneCountryCode()));
		fields.add(new UntaintedPhoneNumber(form.getPhoneNumberAlternate(),
				"Phone Number Alternate").isRequired(false).setUID(userProfile.getUID())
				.setCountryDialingCode(form.getPhoneCountryCode()));

	}

}
