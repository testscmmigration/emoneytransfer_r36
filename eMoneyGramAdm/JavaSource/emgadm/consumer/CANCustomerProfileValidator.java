package emgadm.consumer;

/*
 * New Class added for CAN-55
 * 
 */
import emgadm.model.UserProfile;

public class CANCustomerProfileValidator extends BaseCustomerProfileValidator {
	
	public static final String CAN_POSTAL_CODE = "CANPostalCode";
	
	public CANCustomerProfileValidator(UpdateCustomerProfileForm form,
			UserProfile userProfile, CustomerProfileRegexPattern customerProfileRegex) {
		//Address line 2 clashes with max length as 100 hence resetting all fields
		//super(form, userProfile);
		fields.add(new AlphaFieldWithSpecialChars(form.getCustFirstName(),
				FIRST_NAME, customerProfileRegex.getFirstNameRegex()).setMaxLength(40));
		fields.add(new AlphaFieldWithSpecialChars(form.getCustLastName(),
				LAST_NAME, customerProfileRegex.getLastNameRegex()).setMaxLength(40));
		fields.add(new AlphaFieldWithSpecialChars(form.getMiddleName(), MIDDLE_NAME, customerProfileRegex.getMiddleNameRegex() )
				.setMaxLength(20).isRequired(false));
		fields.add(new AlphaFieldWithSpecialChars(form.getCustSecondLastName(),
				SECOND_LAST_NAME, customerProfileRegex.getLastNameRegex()).setMaxLength(30).isRequired(false));
		fields.add(new CityField(form.getCity(), TOWN_OR_CITY, customerProfileRegex.getCityRegex()).setMaxLength(40));
		fields.add(new BirthDateField(form.getBirthDateText(), BIRTH_DATE));
		fields.add(new AddressField(form.getAddressLine1(), ADDRESS_LINE_1, customerProfileRegex.getAddressLine1Regex())
				.setMaxLength(100));
		fields.add(new AddressField(form.getAddressLine2(), ADDRESS_LINE_2,  customerProfileRegex.getAddressLine2Regex())
				.setMaxLength(100).isRequired(false));
		fields.add(new CANPostalCode(form.getPostalCode(), CAN_POSTAL_CODE, customerProfileRegex.getPostalCodeRegex())
				.setMinLength(7).setMaxLength(7).isRequired(true));
		/*fields.add(new AlphaField(form.getState(),STATE).isRequired(true));*/
		/*fields.add(new AddressField(form.getPhoneCountryCode(), COUNTRY_CODE,  customerProfileRegex.getAddressLine1Regex()));*/
		fields.add(new StrictNumericField(form.getLoyaltyPgmMembershipId(),
				"Plus #").setMinLength(9).setMaxLength(12).isRequired(false));
		fields.add(new UntaintedPhoneNumber(form.getPhoneNumber(), PHONE_NUMBER)
				.setMinLength(7).setMaxLength(14).isRequired(true)
				.setUID(userProfile.getUID())
				.setCountryDialingCode(form.getPhoneCountryCode()));
	}

}
