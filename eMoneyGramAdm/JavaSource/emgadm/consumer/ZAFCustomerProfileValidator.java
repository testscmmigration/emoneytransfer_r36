package emgadm.consumer;

import emgadm.model.UserProfile;

public class ZAFCustomerProfileValidator  extends BaseCustomerProfileValidator{
	
	public ZAFCustomerProfileValidator(UpdateCustomerProfileForm form, UserProfile userProfile, CustomerProfileRegexPattern customerProfileRegex) {
		fields.add(new AlphaFieldWithSpecialChars(form.getCustFirstName(), "First Name", customerProfileRegex.getFirstNameRegex()).setMinLength(2).setMaxLength(40)); 
		fields.add(new AlphaFieldWithSpecialChars(form.getCustLastName(), "Last Name", customerProfileRegex.getLastNameRegex()).setMinLength(2).setMaxLength(40));
		fields.add(new AlphaField(form.getMiddleName(), "Middle Name").setMaxLength(40).isRequired(false)); 
		fields.add(new AlphaField(form.getCustSecondLastName(), "Second Last Name").setMaxLength(40).isRequired(false)); 
		fields.add(new AddressField(form.getAddressLine1(), "Address Line 1", customerProfileRegex.getAddressLine1Regex()).setMinLength(2).setMaxLength(100));
		fields.add(new AddressField(form.getAddressLine2(), "Address Line 2", customerProfileRegex.getAddressLine2Regex()).setMinLength(2).setMaxLength(100).isRequired(false));
		fields.add(new AddressField(form.getAddressLine3(), "Address Line 3", customerProfileRegex.getAddressLine3Regex()).setMinLength(2).setMaxLength(100));
		fields.add(new CityField(form.getCity(), "Town/City", customerProfileRegex.getCityRegex()).setMaxLength(40)); 
		fields.add(new StrictNumericField(form.getPostalCode(), "Postcode").setMinLength(4).setMaxLength(4));
		fields.add(new AlphaField(form.getState(), "State"));
		fields.add(new BirthDateField(form.getBirthDateText(), "Birth Date")); 
	}

}
	