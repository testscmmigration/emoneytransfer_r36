package emgadm.consumer;

import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.StringUtilities;

import com.moneygram.common.log.log4j.Log4JFactory;
import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.model.UserProfile;
import emgadm.transqueue.TransactionDetailAction;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.model.ConsumerDocument;
import emgshared.model.CustomerAuthenticationInfo;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;

/**
 * Action class called on click of 'View Document' links which returns list of
 * all documents uploaded and also individual images of documents Code regarding
 * opening image in pop up.
 * 
 * @version 1.0
 * @author wx51
 */
public class ViewDocumentsDetailsAction extends EMoneyGramAdmBaseAction {
	private static Logger logr = EMGSharedLogger.getLogger(ViewDocumentsDetailsAction.class);
	List<CustomerAuthenticationInfo> customerAuthenticationInfoList = null;
	private static final String FORWARD_SUCCESS = "success";

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DataSourceException {
		String act = null;
		// Added for MBO-6220 
		String partnerSideId = request.getParameter("partnerSiteId");
		request.setAttribute("timeZone", timeZone(partnerSideId));
		
		if (request.getParameter("action") == null) {
			act = (String) request.getAttribute("action");
		} else {
			act = (String) request.getParameter("action");
		}
		ActionForward forward = null;
		UserProfile up = getUserProfile(request);

		ConsumerProfileService consumerProfileService = ServiceFactory	
				.getInstance().getConsumerProfileService();
		if (act != null && act.equalsIgnoreCase("ViewAllImage")) {
			int customerId = Integer.parseInt(request.getParameter("custId"));
			List<ConsumerDocument> consumerDocumentsList = (List<ConsumerDocument>) consumerProfileService
					.getConsumerDocuments(customerId, up.getUID());

			request.setAttribute("documentList", consumerDocumentsList);
		}
		if (act != null && act.equalsIgnoreCase("ViewIdImage")) {
			int imgId = Integer.parseInt(request.getParameter("imgId"));
			try {
				ConsumerDocument consumerDocuments = consumerProfileService
						.getDoc(imgId);
				String name = null;
				name = (String) request.getParameter("fileName");
			
				// We NEED to verify that in fact ids are being uploaded
				// with extension
				int dotPos = name.lastIndexOf(".");
				String extension = name.substring(dotPos);

				if (extension.equals("jpg")) {
					response.setContentType("image/jpeg");
				} else {
					response.setContentType(ESAPI.encoder().encodeForHTML(extension));
				}

				// Content-Disposition is needed, otherwise IE 7+ throws a
				// fit (HTTP status code 404), and will not recognize the
				// output stream.
				ESAPI.httpUtilities().setHeader(response, "Content-Disposition", "inline; filename=id-image"+extension);
				OutputStream out = response.getOutputStream();
				out.write(consumerDocuments.getDocImgBlob());
				out.close();

				// A null must be returned, otherwise the following
				// exception is caused:
				// java.lang.IllegalStateException: OutputStream already
				// obtained
				return null;
			} catch (Exception e) {
				logr.error(
						"exception while/near calling the image to be loaded",
						e);
			}
		}

		forward = mapping.findForward(FORWARD_SUCCESS);
		return forward;
	}
	
	// Added for MBO-6220 to fetch the timezone based on the partnersiteid
	private String timeZone(String partnerSite){
		
		TransactionDetailAction transdetail = new TransactionDetailAction();
		return transdetail.timeZone(partnerSite);
	}
	
}
