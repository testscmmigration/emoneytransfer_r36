package emgadm.consumer;

import java.util.ArrayList;
import java.util.List;


import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.model.KBAQuestion;

public class GetKBAQuestionsForm extends EMoneyGramAdmBaseValidatorForm {

	private List<KBAQuestion> KBAQuestions = new ArrayList<KBAQuestion>();
	private String VendorAuthenticationEventCode;
	private String additionalInfoCode;
	private Long KBAQuestionsSetCode;
	private int custId;
	private String answerCode;

	public String getAnswerCode() {
		return answerCode;
	}

	public void setAnswerCode(String answerCode) {
		this.answerCode = answerCode;
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public List<KBAQuestion> getKBAQuestions() {
		return KBAQuestions;
	}

	public void setKBAQuestions(List<KBAQuestion> kBAQuestions) {
		KBAQuestions = kBAQuestions;
	}

	public String getVendorAuthenticationEventCode() {
		return VendorAuthenticationEventCode;
	}

	public void setVendorAuthenticationEventCode(
			String vendorAuthenticationEventCode) {
		VendorAuthenticationEventCode = vendorAuthenticationEventCode;
	}

	public String getAdditionalInfoCode() {
		return additionalInfoCode;
	}

	public void setAdditionalInfoCode(String additionalInfoCode) {
		this.additionalInfoCode = additionalInfoCode;
	}

	public Long getKBAQuestionsSetCode() {
		return KBAQuestionsSetCode;
	}

	public void setKBAQuestionsSetCode(Long kBAQuestionsSetCode) {
		KBAQuestionsSetCode = kBAQuestionsSetCode;
	}

}
