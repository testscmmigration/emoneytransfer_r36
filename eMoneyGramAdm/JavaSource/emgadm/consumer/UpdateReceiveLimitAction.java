package emgadm.consumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.DuplicateActiveProfileException;
import emgshared.model.ConsumerProfile;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;

public class UpdateReceiveLimitAction extends EMoneyGramAdmBaseAction {
	private static final String FORWARD_SUCCESS = "success";

	private static final String FORWARD_FAILURE = "failure";
	private static final String RECEIVE_DB_VALUE = "L00";
	private static final String MESSAGE_DISPLAYED = "rcvLimit";

	public ActionForward execute(ActionMapping mapping, ActionForm f,
			HttpServletRequest request, HttpServletResponse response)
			throws NumberFormatException, DataSourceException,
			DuplicateActiveProfileException {
		ActionForward forward;
		ActionErrors errors = new ActionErrors();
		UpdateReceiveLimitForm form = (UpdateReceiveLimitForm) f;

		String userId = getUserProfile(request).getUID();
		int custId = Integer.parseInt(form.getCustId());
		ConsumerProfileService profileService = ServiceFactory.getInstance()
				.getConsumerProfileService();
		ConsumerProfile cp = profileService.getConsumerProfile(
				Integer.valueOf(custId), userId, "wwwemg");

		if (null != cp.getReceiveLevel() && !form.isReceiveLimit()) {
			errors.add(MESSAGE_DISPLAYED, new ActionMessage(
					"error.receive.limit.no.change"));
			forward = mapping.findForward(FORWARD_FAILURE);
		} else if (null == cp.getReceiveLevel() && form.isReceiveLimit()) {
			errors.add(MESSAGE_DISPLAYED, new ActionMessage(
					"error.receive.limit.no.change"));
			forward = mapping.findForward(FORWARD_FAILURE);
		} else {
			cp.setReceiveLevel(form.isReceiveLimit() ? null : RECEIVE_DB_VALUE);
			profileService.updateConsumerProfile(cp, true, userId);

			errors.add(MESSAGE_DISPLAYED, new ActionMessage(
					"msg.receive.limit.changed"));

			forward = mapping.findForward(FORWARD_SUCCESS);
		}

		if (errors != null || errors.size() != 0) {
			saveErrors(request, errors);
		}

		return forward;
	}

}
