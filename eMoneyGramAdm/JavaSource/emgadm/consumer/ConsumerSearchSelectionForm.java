package emgadm.consumer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;
import emgshared.model.ConsumerAddress;
public class ConsumerSearchSelectionForm extends EMoneyGramAdmBaseValidatorForm
{
	private String custId;
	private String custLogonId;
	private String custLastName;
	private String custFirstName;
	private String partnerSiteId;
	private boolean includeFirstName;
	private boolean includeLastName;
	private boolean includeLoginId;
	private boolean includePhone;
	private boolean includeAddr;
	private boolean includeCity;
	private boolean includeState;
	private boolean includeZip;
	private boolean includeProfileStatus;
	private boolean includeCreateDate;
	private boolean includePasswordHash;
	private boolean includeCreateIpAddrId;
	private boolean includePrmrCode;
	private String findProfiles = "Y";
	private String submitSelAll;
	private String submitDeselAll;
	private String submitGo;
	//added by Ankit Bhatt for MBO-314
	private String profileType;
	private List fullAddress;
	//private  List addressList;
	private String selectedAddress;
	private List<ConsumerAddress> addressList;
	private int addressListSize;
	//ended
	public String getCustId()
	{
		return custId;
	}

	public void setCustId(String string)
	{
		custId = string;
	}

	public String getCustLogonId()
	{
		return custLogonId;
	}

	public void setCustLogonId(String string)
	{
		custLogonId = string;
	}

	
	public int getAddressListSize() {
		return addressListSize;
	}

	public void setAddressListSize(int addressListSize) {
		this.addressListSize = addressListSize;
	}

	public String getCustFirstName()
	{
		return custFirstName;
	}
	
	

	public List getFullAddress() {
		List consumerAddress = new ArrayList();
		List<String> filteredAddressList = new ArrayList<String>(); 
		String fullAddress = null;
		if(null!= addressList && addressList.size()>0){
		for(ConsumerAddress consAddress : addressList){
			if(null!= consAddress.getAddressLine2()){
				fullAddress = new StringBuilder(consAddress.getAddressLine1()).append("|").append(consAddress.getAddressLine2()).append("|")
						.append(consAddress.getCity()).append(",").append(consAddress.getState()).append(",")
						.append(consAddress.getPostalCode()).toString(); 
					
			}else{
				fullAddress = new StringBuilder(consAddress.getAddressLine1()).append("|")
						.append(consAddress.getCity()).append(",").append(consAddress.getState()).append(",")
						.append(consAddress.getPostalCode()).toString(); 
				
			}
			consAddress.setCountyName(fullAddress);
		}
		for (Iterator<ConsumerAddress> iterator = addressList.iterator(); iterator.hasNext();) {
			ConsumerAddress consAddress = iterator.next();
		    if (filteredAddressList.contains(consAddress.getCountyName())) {
		        iterator.remove();
		    }else{
		    	filteredAddressList.add(consAddress.getCountyName());
		    }
		}
		for(ConsumerAddress consAddress : addressList){
			consumerAddress.add(new LabelValueBean(consAddress.getCountyName(), String.valueOf(consAddress.getAddressId())));
		}
	}
		return consumerAddress;
	}



	
	
	
	
	public void setFullAddress(List fullAddress) {
		this.fullAddress = fullAddress;
	}

	public List<ConsumerAddress> getAddressList() {
		return addressList;
	}

	public void setAddressList(List<ConsumerAddress> addressList) {
		this.addressList = addressList;
	}



	public String getSelectedAddress() {
		return selectedAddress;
	}

	public void setSelectedAddress(String selectedAddress) {
		this.selectedAddress = selectedAddress;
	}

	public void setCustFirstName(String string)
	{
		custFirstName = string;
	}

	public String getCustLastName()
	{
		return custLastName;
	}

	public void setCustLastName(String string)
	{
		custLastName = string;
	}

	public boolean isIncludeFirstName()
	{
		return includeFirstName;
	}

	public void setIncludeFirstName(boolean b)
	{
		includeFirstName = b;
	}

	public boolean isIncludeLastName()
	{
		return includeLastName;
	}

	public void setIncludeLastName(boolean b)
	{
		includeLastName = b;
	}

	public boolean isIncludeLoginId()
	{
		return includeLoginId;
	}

	public void setIncludeLoginId(boolean b)
	{
		includeLoginId = b;
	}

	public boolean isIncludePhone()
	{
		return includePhone;
	}

	public void setIncludePhone(boolean b)
	{
		includePhone = b;
	}

	public boolean isIncludeAddr()
	{
		return includeAddr;
	}

	public void setIncludeAddr(boolean b)
	{
		includeAddr = b;
	}

	public boolean isIncludeCity()
	{
		return includeCity;
	}

	public void setIncludeCity(boolean b)
	{
		includeCity = b;
	}

	public boolean isIncludeState()
	{
		return includeState;
	}

	public void setIncludeState(boolean b)
	{
		includeState = b;
	}

	public boolean isIncludeZip()
	{
		return includeZip;
	}

	public void setIncludeZip(boolean b)
	{
		includeZip = b;
	}

	public boolean isIncludeProfileStatus()
	{
		return includeProfileStatus;
	}

	public void setIncludeProfileStatus(boolean b)
	{
		includeProfileStatus = b;
	}

	public boolean isIncludeCreateDate()
	{
		return includeCreateDate;
	}

	public void setIncludeCreateDate(boolean b)
	{
		includeCreateDate = b;
	}

	public boolean isIncludePasswordHash()
	{
		return includePasswordHash;
	}

	public void setIncludePasswordHash(boolean b)
	{
		includePasswordHash = b;
	}

	public String getFindProfiles()
	{
		return findProfiles;
	}

	public void setFindProfiles(String string)
	{
		findProfiles = string;
	}

	public String getSubmitGo()
	{
		return submitGo;
	}

	public void setSubmitGo(String s)
	{
		submitGo = s;
	}

	public boolean isIncludeCreateIpAddrId()
	{
		return includeCreateIpAddrId;
	}

	public void setIncludeCreateIpAddrId(boolean b)
	{
		includeCreateIpAddrId = b;
	}

    public boolean isIncludePrmrCode() {
        return includePrmrCode;
    }

    public void setIncludePrmrCode(boolean includePrmrCode) {
        this.includePrmrCode = includePrmrCode;
    }

	public String getSubmitSelAll()
	{
		return submitSelAll;
	}

	public void setSubmitSelAll(String string)
	{
		submitSelAll = string;
	}

	public String getSubmitDeselAll()
	{
		return submitDeselAll;
	}

	public void setSubmitDeselAll(String string)
	{
		submitDeselAll = string;
	}
	
	

	public String getProfileType() {
		return profileType;
	}

	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);
		includeFirstName = false;
		includeLastName = false;
		includeLoginId = false;
		includePhone = false;
		includeAddr = false;
		includeCity = false;
		includeState = false;
		includeZip = false;
		includeProfileStatus = false;
		includeCreateDate = false;
		includePasswordHash = false;
		includeCreateIpAddrId = false;
		includePrmrCode = false;
		findProfiles = "Y";
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();

		if (!StringHelper.isNullOrEmpty(submitGo))
		{
			errors = super.validate(mapping, request);

			if (!includeFirstName && !includeLastName && !includeLoginId
					&& !includePhone && !includeAddr && !includeCity
					&& !includeState && !includeZip && !includeProfileStatus
					&& !includeCreateDate && !includePasswordHash
					&& !includeCreateIpAddrId && !includePrmrCode) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"error.must.specify.search.criteria"));
			} else if ((includeFirstName || includeLastName || includeAddr
					|| includeCity || includeState || includeZip
					|| includeCreateDate || includeProfileStatus) && (countSearchCriteriaInputs(request) == 1)) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
				"error.invalid.similarprofile.search.criteria"));
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
				"error.specify.more.than.one.field"));
			} 
		}
		return errors;
	}
	
	public short countSearchCriteriaInputs(HttpServletRequest request) {
		short countOfSelectedInputs = 0;
		if (includeFirstName)
			countOfSelectedInputs++;
		if (includeLastName)
			countOfSelectedInputs++;
		if (includeLoginId)
			countOfSelectedInputs++;
		if (includePhone)
			countOfSelectedInputs++;
		if (includeAddr)
			countOfSelectedInputs++;
		if (includeCity)
			countOfSelectedInputs++;
		if (includeState)
			countOfSelectedInputs++;
		if (includeZip)
			countOfSelectedInputs++;
		if (includeProfileStatus)
			countOfSelectedInputs++;
		if (includeCreateDate)
			countOfSelectedInputs++;
		if (includePasswordHash)
			countOfSelectedInputs++;
		if (includeCreateIpAddrId)
			countOfSelectedInputs++;
		if (includePrmrCode)
			countOfSelectedInputs++;

		return countOfSelectedInputs;
	}
	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

}
