package emgadm.consumer;

import emgadm.model.UserProfile;

public class USCustomerProfileValidator extends BaseCustomerProfileValidator {

	public USCustomerProfileValidator(UpdateCustomerProfileForm form,
			UserProfile userProfile,  CustomerProfileRegexPattern customerProfileRegex) {
		super(form, userProfile, customerProfileRegex);
		fields.add(new GenericPostalCode(form.getPostalCode(), "Zip Code", customerProfileRegex.getPostalCodeRegex())
				.setMinLength(5).setMaxLength(5));
		fields.add(new StrictNumericField(form.getZip4(), "Zip Code")
				.setMaxLength(4).isRequired(false));
		fields.add(new AlphaField(form.getState(), "State"));
		fields.add(new StrictNumericField(form.getLoyaltyPgmMembershipId(),
				"Plus #").setMinLength(9).setMaxLength(12).isRequired(false));
		fields.add(new AddressField(form.getPhoneCountryCode(), "Country Code", customerProfileRegex.getAddressLine1Regex())
				.isRequired(true));
		fields.add(new UntaintedPhoneNumber(form.getPhoneNumber(),
				"Phone Number")
				.setUID(userProfile.getUID())
				.setCountryDialingCode(form.getPhoneCountryCode()));
		fields.add(new UntaintedPhoneNumber(form.getPhoneNumberAlternate(),
				"Phone Number Alternate").isRequired(false).setUID(userProfile.getUID())
				.setCountryDialingCode(form.getPhoneCountryCode()));
	}

}
