package emgadm.consumer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.model.ConsumerProfileDashboard;
import emgadm.model.RecentTranReceiver;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgadm.view.TransactionView;
import emgshared.exceptions.AgentConnectException;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.ConsumerProfile;
import emgshared.model.Transaction;
import emgshared.services.ConsumerAccountService;

public class ShowDashboardDetailAction extends EMoneyGramAdmBaseAction
{
	public ActionForward execute(
				ActionMapping mapping,
				ActionForm f,
				HttpServletRequest request,
				HttpServletResponse response)
			throws AgentConnectException, DataSourceException {
		ShowDashboardDetailForm form = (ShowDashboardDetailForm) f;
		UserProfile up = getUserProfile(request);
		TransactionManager tm = getTransactionManager(request);
		ConsumerAccountService accountService =
			emgshared
				.services
				.ServiceFactory
				.getInstance()
				.getConsumerAccountService();

		ConsumerProfile cp =
			(ConsumerProfile) request.getSession().getAttribute("findProfiles");
		form.setCustId(cp.getId());
		form.setCustLogonId(cp.getUserId());
		ConsumerProfileDashboard cpd =
			(ConsumerProfileDashboard) request.getSession().getAttribute(
				"dashboard");
		form.setAccountStatusOptions(
			convertMapToLabelValueListSorted(
				accountService.getAccountStatusDescriptions()));

		String action = form.getId();
		if (StringHelper.isNullOrEmpty(action)) {
			throw new EMGRuntimeException("Unknow action");
		}

		if ("recentTran".equalsIgnoreCase(action)) {
			form.setActionDesc("Consumer Last 30 Days");
			form.setTranViews(createTranViews(cpd.getRecentTranList(), tm));
		} else if ("recentRcv".equalsIgnoreCase(action)) {
			form.setActionDesc(
				"Receiver '"
					+ form.getFrst()
					+ " "
					+ form.getLast()
					+ "' Last 30 days");
			RecentTranReceiver rtr = null;
			Iterator<RecentTranReceiver> iter = cpd.getReceiverList().iterator();
			while (iter.hasNext()) {
				rtr = iter.next();
				if (rtr.getFrstName().equalsIgnoreCase(form.getFrst())
					&& rtr.getLastName().equalsIgnoreCase(form.getLast())) {
					form.setTranViews(createTranViews(rtr.getTranList(), tm));
					break;
				}
			}
		} else if ("totalTran".equalsIgnoreCase(action)) {
			form.setActionDesc("Current Consumer All");
			form.setTranViews(createTranViews(cpd.getTotalTranList(), tm));
		} else if ("denyTran".equalsIgnoreCase(action)) {
			form.setActionDesc("Consumer Denied");
			form.setTranViews(createTranViews(cpd.getDenyTranList(), tm));
		} else if ("sendTran".equalsIgnoreCase(action)) {
			form.setActionDesc("Consumer Sent");
			form.setTranViews(createTranViews(cpd.getSendTranList(), tm));
		} else if ("cancelTran".equalsIgnoreCase(action)) {
			form.setActionDesc("Consumer Canceled");
			form.setTranViews(createTranViews(cpd.getCancelTranList(), tm));
		} else if ("fundedTran".equalsIgnoreCase(action)) {
			form.setActionDesc("Consumer Funded");
			form.setTranViews(createTranViews(cpd.getFundedTranList(), tm));
		} else if ("bankAccts".equalsIgnoreCase(action)) {
			form.setActionDesc("Consumer");
			form.setBankAccounts(cpd.getBankAccounts());
		} else if ("creditCardAccts".equalsIgnoreCase(action)) {
			form.setActionDesc("Consumer");
			form.setCreditCardAccounts(cpd.getCreditCardAccounts());
		} else if ("debitCardAccts".equalsIgnoreCase(action)) {
			form.setActionDesc("Consumer");
			form.setDebitCardAccounts(cpd.getDebitCardAccounts());
		}

		if ("bankAccts".equalsIgnoreCase(action)
			|| "creditCardAccts".equalsIgnoreCase(action)
			|| "debitCardAccts".equalsIgnoreCase(action)) {
			if (up.hasPermission("updateAccountStatus")) {
				request.setAttribute("updateAcct", "Y");
			}
			if (up.hasPermission("validateMicroDeposit")) {
				request.setAttribute("validateMD", "Y");
			}
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private TransactionView[] createTranViews(
		List<Transaction> transactions,
		TransactionManager tm) {
		TransactionView[] views =
			new TransactionView[transactions == null ? 0 : transactions.size()];
		if (transactions != null) {
			Collections.sort(transactions);
			int i = 0;
			for (Iterator<Transaction> iter = transactions.iterator(); iter.hasNext(); ++i) {
				Transaction transaction = iter.next();
				try {
					transaction.setCmntList(
						(List) tm.getTransactionComments(
							transaction.getEmgTranId()));
				} catch (Exception e) {
					transaction.setCmntList(new ArrayList());
				}

				views[i] = new TransactionView(transaction);
			}
		}
		return views;
	}
}
