
package emgadm.consumer;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

/**
 * @author A119
 *
 */
public class UnlockCustomerProfileForm extends EMoneyGramAdmBaseValidatorForm
{
	private String custId;
	
	public String getCustId()
	{
		return custId;
	}

	public void setCustId(String string)
	{
		custId = string;
	}

}
