package emgadm.consumer;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.model.BPSResultUnit;

public class ShowBasicPersonSearchForm extends EMoneyGramAdmBaseValidatorForm{

	private java.util.LinkedHashMap<String,BPSResultUnit> ssnLinkedMap;

	public java.util.LinkedHashMap<String, BPSResultUnit> getSsnLinkedMap() {
		return ssnLinkedMap;
	}

	public void setSsnLinkedMap(
			java.util.LinkedHashMap<String, BPSResultUnit> ssnLinkedMap) {
		this.ssnLinkedMap = ssnLinkedMap;
	}



}
