package emgadm.consumer;

public class CustomerProfileRegexPattern {
	
	public static final String FIRST_NAME_REGEX = "firstNameRegex";
	public static final String MIDDLE_NAME_REGEX = "middleNameRegex";
	public static final String LAST_NAME_REGEX = "lastNameRegex";
	public static final String SECOND_LAST_NAME_REGEX = "secondLastNameRegex";
	public static final String ADDRESS_LINE_1_REGEX = "addressLine1Regex";
	public static final String ADDRESS_LINE_2_REGEX = "addressLine2Regex";
	public static final String ADDRESS_LINE_3_REGEX = "addressLine3Regex";
	public static final String CITY_REGEX = "cityRegex";
	public static final String POSTAL_CODE_REGEX = "postalCodeRegex";	
	
	private String firstNameRegex;
	private String middleNameRegex;
	private String lastNameRegex;
	private String secondLastNameRegex;
	private String addressLine1Regex;
	private String addressLine2Regex;
	private String addressLine3Regex;
	private String cityRegex;
	private String postalCodeRegex;
	
	public String getFirstNameRegex() {
		return firstNameRegex;
	}
	public void setFirstNameRegex(String firstNameRegex) {
		this.firstNameRegex = firstNameRegex;
	}
	public String getMiddleNameRegex() {
		return middleNameRegex;
	}
	public void setMiddleNameRegex(String middleNameRegex) {
		this.middleNameRegex = middleNameRegex;
	}
	public String getLastNameRegex() {
		return lastNameRegex;
	}
	public void setLastNameRegex(String lastNameRegex) {
		this.lastNameRegex = lastNameRegex;
	}
	public String getSecondLastNameRegex() {
		return secondLastNameRegex;
	}
	public void setSecondLastNameRegex(String secondLastNameRegex) {
		this.secondLastNameRegex = secondLastNameRegex;
	}
	public String getAddressLine1Regex() {
		return addressLine1Regex;
	}
	public void setAddressLine1Regex(String addressLine1Regex) {
		this.addressLine1Regex = addressLine1Regex;
	}
	public String getAddressLine2Regex() {
		return addressLine2Regex;
	}
	public void setAddressLine2Regex(String addressLine2Regex) {
		this.addressLine2Regex = addressLine2Regex;
	}
	public String getAddressLine3Regex() {
		return addressLine3Regex;
	}
	public void setAddressLine3Regex(String addressLine3Regex) {
		this.addressLine3Regex = addressLine3Regex;
	}
	public String getCityRegex() {
		return cityRegex;
	}
	public void setCityRegex(String cityRegex) {
		this.cityRegex = cityRegex;
	}
	public String getPostalCodeRegex() {
		return postalCodeRegex;
	}
	public void setPostalCodeRegex(String postalCodeRegex) {
		this.postalCodeRegex = postalCodeRegex;
	}	
}
