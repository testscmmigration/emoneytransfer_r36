/*
 * Created on Oct 23, 2012
 *
 */
package emgadm.consumer;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

/**
 * @author vy79
 *
 */
public class UpdateCustDocumentStatusForm extends EMoneyGramAdmBaseValidatorForm
{
	private String custId;
	private String idExtnlId;
	private String docStatus;
	private String consumerStatus;
	private String submitApproveDocument;
	private String submitDenyDocument;
	private String submitPendDocument;

	public String getCustId()
	{
		return custId;
	}

	public String getCustStatus()
	{
		return docStatus;
	}

	public void setCustId(String string)
	{
		custId = string;
	}

	public void setCustStatus(String string)
	{
		docStatus = string;
	}

	public String getIdNumber() {
		return idExtnlId;
	}

	public void setIdNumber(String idNumber) {
		this.idExtnlId = idNumber;
	}

	public String getDocStatus() {
		return docStatus;
	}

	public void setDocStatus(String docStatus) {
		this.docStatus = docStatus;
	}

	public String getSubmitApproveDocument() {
		return submitApproveDocument;
	}

	public void setSubmitApproveDocument(String submitApproveDocument) {
		this.submitApproveDocument = submitApproveDocument;
	}

	public String getSubmitDenyDocument() {
		return submitDenyDocument;
	}

	public void setSubmitDenyDocument(String submitDenyDocument) {
		this.submitDenyDocument = submitDenyDocument;
	}

	public String getSubmitPendDocument() {
		return submitPendDocument;
	}

	public void setSubmitPendDocument(String submitPendDocument) {
		this.submitPendDocument = submitPendDocument;
	}

	public String getIdExtnlId() {
		return idExtnlId;
	}

	public void setIdExtnlId(String idExtnlId) {
		this.idExtnlId = idExtnlId;
	}

	public String getConsumerStatus() {
		return consumerStatus;
	}

	public void setConsumerStatus(String consumerStatus) {
		this.consumerStatus = consumerStatus;
	}
}
