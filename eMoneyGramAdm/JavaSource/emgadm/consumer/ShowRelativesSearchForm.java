package emgadm.consumer;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class ShowRelativesSearchForm extends EMoneyGramAdmBaseValidatorForm{
	private static final long serialVersionUID = 1L;
	private emgadm.model.relativessearch.RelativeSearchResponse relativesSearchResponse;
	public emgadm.model.relativessearch.RelativeSearchResponse getRelativesSearchResponse() {
		return relativesSearchResponse;
	}
	public void setRelativesSearchResponse(
			emgadm.model.relativessearch.RelativeSearchResponse relativesSearchResponse) {
		this.relativesSearchResponse = relativesSearchResponse;
	}

	
}
