package emgadm.consumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;


import com.moneygram.security.ldap.LdapServiceImpl;
import com.moneygram.security.ldap.LdapUser;
import com.moneygram.security.ldap.exceptions.LdapUserNotFoundException;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.MaxRowsHashCryptoException;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileComment;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;


/**
 * 
 * @author xy79
 * 
 * Removing LDAP entry for South Africa users based on the conditions
 * if customer status is inactive(NAT) & customer is from South Africa (MGOZAF) & EdirGuid is not null
 * 
 * MBO-5465
 *
 */

public class RemoveLDAPEntryAction extends EMoneyGramAdmBaseAction {
	
	private static Logger logger = EMGSharedLogger.getLogger(
			RemoveLDAPEntryAction.class);
	private static final String FORWARD_SUCCESS = "success";

	private static final String FORWARD_FAILURE = "failure";
	

	private ConsumerProfileService profileService = ServiceFactory.getInstance()
			.getConsumerProfileService();

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws NumberFormatException,
			DataSourceException, MaxRowsHashCryptoException {
		
		ActionForward forward;
		ActionErrors errors = new ActionErrors();
		String userId = getUserProfile(request).getUID();
		RemoveLDAPEntryForm inputForm = (RemoveLDAPEntryForm) form;
		String custId = inputForm.getCustId();
		int consumerId = Integer.parseInt(custId);
		ConsumerProfile consumerProfile = null;
		forward = mapping.findForward(FORWARD_SUCCESS);
		String oldEDirGuid = null;
		try {
			consumerProfile = profileService.getConsumerProfile(Integer.valueOf(consumerId), userId,null);
			oldEDirGuid = consumerProfile.getEdirGuid();
			deleteLDAPEntry(errors, userId, consumerProfile, oldEDirGuid, custId);
		} catch (Exception e) {
			forward = mapping.findForward(FORWARD_FAILURE);
		}
		if (errors.size() > 0) {
			saveErrors(request, errors);
		}else{
			errors.add("headerMessage", new ActionMessage("message.ldap.entry.deleted", consumerProfile.getUserId()));
			saveErrors(request, errors);
		}
		return forward;
	}
	
	/**
	 * Method Perams
	 * @param errors
	 * @param userId
	 * @param consumerProfile
	 * @param eDirGuid
	 * @throws Exception
	 * 
	 * Deleting Ldap entry if it is not null and logging the eDirGuid in database as 'LDAP Deleted'
	 */

	private void deleteLDAPEntry(ActionErrors errors, String userId, ConsumerProfile consumerProfile, String oldEDirGuid, String custId) throws Exception {
        LdapServiceImpl ldapService = new LdapServiceImpl();
        String eDirGuid = null;
        try{
        
	        LdapUser ldapUser = new LdapUser();
	        ldapUser = ldapService.findUser(consumerProfile.getUserId());
	        ldapService.deleteUser(ldapUser);
	        if(consumerProfile.getEdirGuid() != null) {
	            eDirGuid = custId + " - LDAP Deleted";
	        	consumerProfile.setEdirGuid(eDirGuid);
	        }
	        logger.debug("~~~~~~~~~~~~~~`Going to update Consumer service after LDAP delettion");
	        profileService.updateConsumerProfile(consumerProfile, true, userId);
	        logger.debug("~~~~~~~~~~~~~~After updating consumer profile post LDAP delete");
			ConsumerProfileComment comment = new ConsumerProfileComment();
			comment.setCustId(consumerProfile.getId());
			comment.setText("LDAP ENTRY REMOVED : " + oldEDirGuid);
			comment.setReasonCode("OTH");
			 logger.debug("~~~~~~~~~~~~~~Going to update cons profile comment post LDAP delete");
			profileService.addConsumerProfileComment(comment, userId);
			 logger.debug("~~~~~~~~~~~~~~After updating cons profile comment post LDAP delete");
	    } catch (LdapUserNotFoundException unf){
	    	errors.add("headerMessage", new ActionMessage("error.edir.user.not.found",
				consumerProfile.getUserId()));
	    	logger.error("LDAP Exception " + unf.getMessage());
	    }
	}

}
