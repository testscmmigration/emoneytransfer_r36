package emgadm.consumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.transqueue.ShowCustomerProfileForm;

public class BackToCustProfileAction extends EMoneyGramAdmBaseAction {
	private static final String FORWARD_BACK = "back";
	public ActionForward execute(ActionMapping mapping, ActionForm f,
			HttpServletRequest request, HttpServletResponse response) {
	
		GetKBAQuestionsForm kbaForm;

		HttpSession session = request.getSession();
		kbaForm = (GetKBAQuestionsForm) session
				.getAttribute("getKBAQuestionsForm");
		request.setAttribute("custId", String.valueOf(kbaForm.getCustId()));

		return mapping.findForward(FORWARD_BACK);

	}
}
