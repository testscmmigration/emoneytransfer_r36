package emgadm.consumer;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

/**
 * Bean class called on click of 'View Document' links.
 * 
 * @version 1.0
 * @author wx51
 */

public class ViewDocumentsDetailsForm extends EMoneyGramAdmBaseValidatorForm {
	private String customerId;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

}
