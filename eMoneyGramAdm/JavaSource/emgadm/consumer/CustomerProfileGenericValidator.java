package emgadm.consumer;

import emgadm.model.UserProfile;
import emgshared.util.Constants;
import emgshared.util.EUSites;


public class CustomerProfileGenericValidator extends BaseCustomerProfileValidator {

	public CustomerProfileGenericValidator(UpdateCustomerProfileForm form,
			UserProfile userProfile, String countryCode, String parentSiteId, CustomerProfileRegexPattern customerProfileRegex) {

		super(form, userProfile, customerProfileRegex);		

		if (Constants.COUNTRY_CODE_USA.equals(countryCode)){
			fields.add(new GenericPostalCode(form.getPostalCode(), ZIP_CODE, customerProfileRegex.getPostalCodeRegex() )
					.setMinLength(5).setMaxLength(5));
			fields.add(new StrictNumericField(form.getZip4(), ZIP_CODE).setMaxLength(4)
					.isRequired(false));
			/*fields.add(new AlphaField(form.getState(), STATE).isRequired(true));*/
			fields.add(new StrictNumericField(form.getLoyaltyPgmMembershipId(), "PLUS")
					.setMinLength(9).setMaxLength(12).isRequired(false));
			/*fields.add(new AddressField(form.getPhoneCountryCode(), COUNTRY_CODE, customerProfileRegex.getAddressLine1Regex())
					.isRequired(true));*/
			fields.add(new UntaintedPhoneNumber(form.getPhoneNumber(), PHONE_NUMBER)
					.setUID(userProfile.getUID()).setCountryDialingCode(
							form.getPhoneCountryCode()));
			fields.add(new UntaintedPhoneNumber(form.getPhoneNumberAlternate(),
					PHONE_NUMBER_ALTERNATE).isRequired(false)
					.setUID(userProfile.getUID())
					.setCountryDialingCode(form.getPhoneCountryCode()));
		} else if (Constants.COUNTRY_CODE_UK.equals(countryCode)) {

			fields.add(new UKPostalCode(form.getPostalCode(), UK_POST_CODE, customerProfileRegex.getPostalCodeRegex() ).setMinLength(
					5).setMaxLength(8));
			// MBO-3063
			fields.add(new AlphaField(form.getCounty(), COUNTRY).setMaxLength(40)
					.isRequired(false));
			/*fields.add(new AddressField(form.getPhoneCountryCode(), COUNTRY_CODE, customerProfileRegex.getAddressLine1Regex())
					.isRequired(true));*/
			fields.add(new UntaintedPhoneNumber(form.getPhoneNumber(), PHONE_NUMBER)
					.setUID(userProfile.getUID()).setCountryDialingCode(
							form.getPhoneCountryCode()));
			fields.add(new UntaintedPhoneNumber(form.getPhoneNumberAlternate(),
					PHONE_NUMBER_ALTERNATE).isRequired(false)
					.setUID(userProfile.getUID())
					.setCountryDialingCode(form.getPhoneCountryCode()));

		} else if (Constants.COUNTRY_CODE_DE.equals(countryCode)) {

			fields.add(new DEUCityField(form.getCity(), TOWN_OR_CITY, customerProfileRegex.getCityRegex()).setMaxLength(40));
			fields.add(new GenericPostalCode(form.getPostalCode(), POSTAL_CODE, customerProfileRegex.getPostalCodeRegex())
					.setMinLength(5).setMaxLength(5));
//			fields.add(new AddressField(form.getPhoneCountryCode(), COUNTRY_CODE,customerProfileRegex.getAddressLine1Regex())
//					.isRequired(true));
			fields.add(new UntaintedPhoneNumber(form.getPhoneNumber(), PHONE_NUMBER)
					.setUID(userProfile.getUID()).setCountryDialingCode(
							form.getPhoneCountryCode()));
			fields.add(new UntaintedPhoneNumber(form.getPhoneNumberAlternate(),
					"Phone Number Alternate").isRequired(false)
					.setUID(userProfile.getUID())
					.setCountryDialingCode(form.getPhoneCountryCode()));
			fields.add(new BirthDateField(form.getBirthDateText(), BIRTH_DATE));

			// Note: Not all addresses have street names, Baltrum for example
			// does
			// not have any.
			fields.add(new DEUAddressField(form.getAddressLine1(), STREET, customerProfileRegex.getAddressLine1Regex())
					.setMaxLength(100));
			fields.add(new DEUAddressField(form.getAddressLine2(), ADDRESS_LINE_2, customerProfileRegex.getAddressLine2Regex())
					.setMaxLength(100).isRequired(false));
			fields.add(new DEUAddressField(form.getAddressLine3(), ADDRESS_LINE_3, customerProfileRegex.getAddressLine3Regex())
					.setMaxLength(100).isRequired(false));						
			fields.add(new StrictNumericField(form.getLoyaltyPgmMembershipId(), PLUS)
					.setMinLength(9).setMaxLength(12).isRequired(false));

		} else if (Constants.COUNTRY_CODE_ZAF.equals(countryCode)) {

			fields.add(new CityField(form.getCity(), TOWN_OR_CITY, customerProfileRegex.getCityRegex()).setMaxLength(40));
			fields.add(new GenericPostalCode(form.getPostalCode(), POSTAL_CODE, customerProfileRegex.getPostalCodeRegex())
					.setMinLength(4).setMaxLength(4));
			/*fields.add(new AlphaField(form.getState(), STATE).isRequired(true));*/
			fields.add(new BirthDateField(form.getBirthDateText(), BIRTH_DATE));

		} else if (Constants.COUNTRY_CODE_ESP.equals(countryCode)) {
			fields.add(new GenericPostalCode(form.getPostalCode(), ZIP_CODE,  customerProfileRegex.getPostalCodeRegex())
					.setMinLength(5).setMaxLength(5));
			/*fields.add(new AlphaField(form.getState(), STATE).isRequired(true));
			fields.add(new AddressField(form.getPhoneCountryCode(), COUNTRY_CODE, customerProfileRegex.getAddressLine1Regex())
					.isRequired(false));*/
			fields.add(new StrictNumericField(form.getLoyaltyPgmMembershipId(), PLUS)
					.setMinLength(9).setMaxLength(12).isRequired(false));
			fields.add(new UntaintedPhoneNumber(form.getPhoneNumber(), PHONE_NUMBER)
					.setMinLength(7).setMaxLength(14).isRequired(false)
					.setUID(userProfile.getUID())
					.setCountryDialingCode(form.getPhoneCountryCode()));

		} else if (Constants.COUNTRY_CODE_FRA.equals(countryCode)) {
			fields.add(new GenericPostalCode(form.getPostalCode(), ZIP_CODE, customerProfileRegex.getPostalCodeRegex() )
					.setMinLength(5).setMaxLength(5));
			/*fields.add(new AlphaField(form.getState(), STATE).isRequired(false));
			fields.add(new AddressField(form.getPhoneCountryCode(), COUNTRY_CODE, customerProfileRegex.getAddressLine1Regex())
					.isRequired(false));*/
			fields.add(new StrictNumericField(form.getLoyaltyPgmMembershipId(), PLUS)
					.setMinLength(9).setMaxLength(12).isRequired(false));
			fields.add(new UntaintedPhoneNumber(form.getPhoneNumber(), PHONE_NUMBER)
					.setMinLength(7).setMaxLength(14).isRequired(false)
					.setUID(userProfile.getUID())
					.setCountryDialingCode(form.getPhoneCountryCode()));
		} else if (Constants.COUNTRY_CODE_CAN.equals(countryCode)) {
			fields.add(new CityField(form.getCity(), TOWN_OR_CITY, customerProfileRegex.getCityRegex()).setMaxLength(40));
			fields.add(new BirthDateField(form.getBirthDateText(), BIRTH_DATE));
			fields.add(new CANPostalCode(form.getPostalCode(), CAN_POSTAL_CODE, customerProfileRegex.getPostalCodeRegex())
					.setMinLength(7).setMaxLength(7).isRequired(true));
			/*fields.add(new AlphaField(form.getState(), STATE).isRequired(true));
			fields.add(new AddressField(form.getPhoneCountryCode(), COUNTRY_CODE, customerProfileRegex.getAddressLine1Regex()));*/
			fields.add(new StrictNumericField(form.getLoyaltyPgmMembershipId(), "PLUS")
					.setMinLength(9).setMaxLength(12).isRequired(false));
			fields.add(new UntaintedPhoneNumber(form.getPhoneNumber(), PHONE_NUMBER)
					.setMinLength(7).setMaxLength(14).isRequired(true)
					.setUID(userProfile.getUID())
					.setCountryDialingCode(form.getPhoneCountryCode()));

		} else if (Constants.COUNTRY_CODE_AUS.equals(countryCode)) {
			fields.add(new GenericPostalCode(form.getPostalCode(), ZIP_CODE, customerProfileRegex.getPostalCodeRegex())
					.setMinLength(4).setMaxLength(4));
			fields.add(new StrictNumericField(form.getZip4(), ZIP_CODE).setMaxLength(4)
					.isRequired(false));
			fields.add(new StrictNumericField(form.getLoyaltyPgmMembershipId(), "PLUS")
					.setMinLength(9).setMaxLength(12).isRequired(false));
			/*fields.add(new AlphaField(form.getState(), STATE).isRequired(true));
			fields.add(new AddressField(form.getPhoneCountryCode(), COUNTRY_CODE, customerProfileRegex.getAddressLine1Regex())
					.isRequired(true));*/
			fields.add(new UntaintedPhoneNumber(form.getPhoneNumber(), PHONE_NUMBER)
					.setUID(userProfile.getUID()).setCountryDialingCode(
							form.getPhoneCountryCode()));
			fields.add(new UntaintedPhoneNumber(form.getPhoneNumberAlternate(),
					PHONE_NUMBER_ALTERNATE).isRequired(false)
					.setUID(userProfile.getUID())
					.setCountryDialingCode(form.getPhoneCountryCode()));
		} else if (EUSites.contains(countryCode)){
			/*if(form.getHasStatesProvinces().equals("Y")){
				fields.add(new AlphaNumericField(form.getState(), STATE).isRequired(true));
				}*/
			fields.add(new StrictNumericField(form.getLoyaltyPgmMembershipId(), "PLUS")
						.setMinLength(9).setMaxLength(12).isRequired(false));
			/*fields.add(new AddressField(form.getPhoneCountryCode(), COUNTRY_CODE, customerProfileRegex.getAddressLine1Regex())
						.isRequired(true));*/
			fields.add(new UntaintedPhoneNumber(form.getPhoneNumber(), PHONE_NUMBER)
						.setUID(userProfile.getUID()).setCountryDialingCode(
								form.getPhoneCountryCode()));
			fields.add(new UntaintedPhoneNumber(form.getPhoneNumberAlternate(),
						PHONE_NUMBER_ALTERNATE).isRequired(false)
						.setUID(userProfile.getUID())
						.setCountryDialingCode(form.getPhoneCountryCode()));
			fields.add(new EUPostalCode(form.getPostalCode(), EU_POSTAL_CODE,
					EUSites.fromValue(countryCode), customerProfileRegex
							.getPostalCodeRegex()));
		}
		else {
			throw new IllegalArgumentException(parentSiteId
					+ " is not a valid parent site id");
		}

	}

	private class DEUAlphaField extends AlphaField {
		DEUAlphaField(String value, String friendlyName, String patternFromDB) {
			super(value, friendlyName);
			pattern = patternFromDB;
			keyStringMessage = "errors.non.alpha";
		}
	}

	private class DEUAlphaFieldWithSpecial extends AlphaField {
		DEUAlphaFieldWithSpecial(String value, String friendlyName, String patternFromDB) {
			super(value, friendlyName);
			pattern = patternFromDB;
			keyStringMessage = "errors.non.alphaSpecial";
		}
	}

	private class DEUAddressField extends AddressField {
		DEUAddressField(String value, String friendlyName, String patternFromDB) {
			super(value, friendlyName, patternFromDB);
			pattern = patternFromDB;
			keyStringMessage = "errors.non.addressdigit";
		}
	}

	private class DEUCityField extends StringField {
		DEUCityField(String value, String friendlyName, String patternFromDB) {
			super(value, friendlyName);
			pattern = patternFromDB;
			keyStringMessage = "errors.invalid.city";
		}
	}

}
