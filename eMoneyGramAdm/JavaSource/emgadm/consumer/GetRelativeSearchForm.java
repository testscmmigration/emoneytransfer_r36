package emgadm.consumer;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class GetRelativeSearchForm extends EMoneyGramAdmBaseValidatorForm{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int custId;
	private String custLastName;	
	private String custFirstName;
	private String addressLine1;
	private String city; 
	private String state;
	private String zip;
	private String phoneNumber;
	private String custSsn;
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	public String getCustLastName() {
		return custLastName;
	}
	public void setCustLastName(String custLastName) {
		this.custLastName = custLastName;
	}
	public String getCustFirstName() {
		return custFirstName;
	}
	public void setCustFirstName(String custFirstName) {
		this.custFirstName = custFirstName;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getCustSsn() {
		return custSsn;
	}
	public void setCustSsn(String custSsn) {
		this.custSsn = custSsn;
	}
}
