package emgadm.consumer;

import org.apache.struts.action.ActionErrors;

public interface CustomerProfileValidator {

	ActionErrors validate(ActionErrors errors);

}
