package emgadm.consumer;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.moneygram.mgo.service.consumerValidationV2_1.GetKBAQuestionsResponse;
import com.moneygram.mgo.service.consumerValidationV2_1.MultipleChoicePossibleAnswer;
import com.moneygram.mgo.service.consumerValidationV2_1.MultipleChoiceQuestion;
import com.moneygram.mgo.service.consumerValidationV2_1.MultipleChoiceQuestions;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.model.KBAAnswer;
import emgadm.model.KBAQuestion;
import emgadm.services.consumervalidation.ConsumerValidationProxy;
import emgadm.services.consumervalidation.ConsumerValidationProxyImpl;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileActivity;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;

public class GetKBAQuestionsAction extends EMoneyGramAdmBaseAction {

	private static final String FORWARD_SUCCESS = "success";
	private static final String FORWARD_FAILED = "failure";
	private static final String INDIVIDUAL_NOT_FOUND = "individual_not_found";

	private static Logger logr = EMGSharedLogger.getLogger(
			GetKBAQuestionsAction.class);
	ConsumerValidationProxy cvsService = ConsumerValidationProxyImpl
			.getInstance();
	private static final ServiceFactory sharedServiceFactory = ServiceFactory
			.getInstance();
	private static final ConsumerProfileService profileService = sharedServiceFactory
			.getConsumerProfileService();

	public ActionForward execute(ActionMapping mapping, ActionForm f,
			HttpServletRequest request, HttpServletResponse response) {
		ActionForward forward = null;
		ActionErrors errors = new ActionErrors();
		GetKBAQuestionsForm form = (GetKBAQuestionsForm) f;

		int consumerid = form.getCustId();

		String userId = getUserProfile(request).getUID();

		ConsumerProfile consumerProfile = null;
		ConsumerProfileActivity cpa = new ConsumerProfileActivity();

		try {
			consumerProfile = profileService.getConsumerProfile(consumerid,
					userId, null);

			cpa.setCustId(consumerProfile.getId());
			GetKBAQuestionsResponse kbaResponse = null;

			kbaResponse = cvsService.getKBAQuestions(consumerProfile);

			String vendorAuthenticationEventCode = kbaResponse
					.getVendorEventId();

			form.setVendorAuthenticationEventCode(vendorAuthenticationEventCode);

			if ((null != kbaResponse)
					&& (null != kbaResponse.getAdditionalInformation())) {

				form.setAdditionalInfoCode(kbaResponse
						.getAdditionalInformation().getCode());
			}
			List<KBAQuestion> kbaQuestions = form.getKBAQuestions();
			kbaQuestions.clear();
			if ((null != kbaResponse) && (null != kbaResponse.getQuestions())) {
				MultipleChoiceQuestions multipleChoiceQuestions = kbaResponse
						.getQuestions();
				form.setKBAQuestionsSetCode(kbaResponse.getQuestions()
						.getQuestionsSetId());
				MultipleChoiceQuestion[] kbaIdentityQuestions = multipleChoiceQuestions
						.getItem();
				for (MultipleChoiceQuestion kbaIdentityQuestion : kbaIdentityQuestions) {
					KBAQuestion kbaQuestion = new KBAQuestion();
					kbaQuestion.setKBAQuestionCode(String
							.valueOf(kbaIdentityQuestion.getQuestionId()));
					kbaQuestion.setKBAQuestionDescription(kbaIdentityQuestion
							.getQuestionText());
					List<KBAAnswer> kbaAnswers = kbaQuestion.getKbaAnswers();
					kbaAnswers.clear();
					MultipleChoicePossibleAnswer[] kbaIdentityAnswers = kbaIdentityQuestion
							.getChoices();
					for (MultipleChoicePossibleAnswer kbaIdentityAnswer : kbaIdentityAnswers) {
						KBAAnswer kbaAnswer = new KBAAnswer();
						kbaAnswer.setKBAAnswerCode(String
								.valueOf(kbaIdentityAnswer.getChoiceId()));
						kbaAnswer.setKBAAnswerDescription(kbaIdentityAnswer
								.getChoiceText());
						kbaAnswers.add(kbaAnswer);
					}
					kbaQuestion.setKbaAnswers(kbaAnswers);

					kbaQuestions.add(kbaQuestion);
				}
				logr.info("Get KBA Questions Call was successful for the ConsumerProfile  :"
						+ consumerid);
				form.setKBAQuestions(kbaQuestions);

				cpa.setActivityLogCode(ConsumerProfileActivity.KBA_INITIATED);
				profileService.insertConsumerActivityLog(cpa, userId);

				forward = mapping.findForward(FORWARD_SUCCESS);

			} else {
				if (INDIVIDUAL_NOT_FOUND.equals(kbaResponse
						.getAdditionalInformation().getCode())) {
					cpa.setActivityLogCode(ConsumerProfileActivity.KBA_SENDER_NOT_FOUND);
					profileService.insertConsumerActivityLog(cpa, userId);
					logr.error("Get KBA Questions Call failed with INDIVIDUAL NOT FOUND error  for the ConsumerProfile  :"
							+ consumerid);
					forward = mapping.findForward(FORWARD_FAILED);

				}

			}

		} catch (DataSourceException consumerServiceException) {
			// TODO Auto-generated catch block

			logr.error("Failed to fetch the ConsumerProfile For User ID :"
					+ consumerid, consumerServiceException);
		} catch (Exception cvsException) {

			logr.error(
					"Get KBA Questions Call failed   for the ConsumerProfile  :"
							+ consumerid, cvsException);
			forward = mapping.findForward(FORWARD_FAILED);
			cpa.setActivityLogCode(ConsumerProfileActivity.KBA_ERROR_CODE);

			try {
				profileService.insertConsumerActivityLog(cpa, userId);
			} catch (DataSourceException dataSourceException) {

				logr.error(
						"Failed to Insert Consumer Activity Log for the ConsumerProfile  :"
								+ consumerid, dataSourceException);

			}
			errors.add("kbaMessage", new ActionMessage("msg.kba.timeout"));
			if (!errors.isEmpty()) {
				saveErrors(request, errors);
			}
		}
		return forward;
	}

}
