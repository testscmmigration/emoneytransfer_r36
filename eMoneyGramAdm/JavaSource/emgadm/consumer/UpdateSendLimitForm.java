package emgadm.consumer;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class UpdateSendLimitForm extends EMoneyGramAdmBaseValidatorForm{
	private String custId;
	private String sendLimit;

	public String getCustId()
	{
		return custId;
	}

	public void setCustId(String string)
	{
		custId = string;
	}

	public void setSendLimit(String sendLimit) {
		this.sendLimit = sendLimit;
	}

	public String getSendLimit() {
		return sendLimit;
	}


}
