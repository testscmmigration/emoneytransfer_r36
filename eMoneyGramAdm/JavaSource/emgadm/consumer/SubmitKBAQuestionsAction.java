package emgadm.consumer;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.log4j.Logger;


import com.moneygram.mgo.service.consumerValidationV2_1.MultipleChoiceAnswer;
import com.moneygram.mgo.service.consumerValidationV2_1.MultipleChoiceAnswers;
import com.moneygram.mgo.service.consumerValidationV2_1.MultipleChoicePossibleAnswer;
import com.moneygram.mgo.service.consumerValidationV2_1.MultipleChoiceQuestion;
import com.moneygram.mgo.service.consumerValidationV2_1.MultipleChoiceQuestions;
import com.moneygram.mgo.service.consumerValidationV2_1.ResultType;
import com.moneygram.mgo.service.consumerValidationV2_1.SubmitKBAAnswersResponse;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.model.KBAAnswer;
import emgadm.model.KBAQuestion;

import emgadm.services.consumervalidation.ConsumerValidationProxy;
import emgadm.services.consumervalidation.ConsumerValidationProxyImpl;

import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileActivity;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;
import emgshared.util.Constants;

public class SubmitKBAQuestionsAction extends EMoneyGramAdmBaseAction {

	private static final String FORWARD_SUCCESS = "success";
	private static final String FORWARD_FAILED = "failure";
	private static final String FORWARD_QUESTIONS = "questions";
	ConsumerValidationProxy cvsService = ConsumerValidationProxyImpl
			.getInstance();
	private static final ServiceFactory sharedServiceFactory = ServiceFactory
			.getInstance();
	private static final ConsumerProfileService profileService = sharedServiceFactory
			.getConsumerProfileService();
	private static Logger logr = EMGSharedLogger.getLogger(SubmitKBAQuestionsAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm f,
			HttpServletRequest request, HttpServletResponse response) {
		ActionForward forward = null;

		SubmitKBAQuestionsForm form = (SubmitKBAQuestionsForm) f;

		GetKBAQuestionsForm kbaForm;

		HttpSession session = request.getSession();
		kbaForm = (GetKBAQuestionsForm) session
				.getAttribute("getKBAQuestionsForm");
		int custId = kbaForm.getCustId();
		String custIdValue = String.valueOf(custId);
		request.setAttribute("custId", custIdValue);
		String vendorEventId = kbaForm.getVendorAuthenticationEventCode();
		Long questionSetId = kbaForm.getKBAQuestionsSetCode();

		String userId = getUserProfile(request).getUID();
		ConsumerProfile consumerProfile = null;
		ConsumerProfileActivity cpa = new ConsumerProfileActivity();

		try {
			consumerProfile = profileService.getConsumerProfile(custId, userId,
					null);

			cpa.setCustId(consumerProfile.getId());
			SubmitKBAAnswersResponse submitKBAResponse = null;
			HashMap<String, Long> questionAnswerPairs = new HashMap<String, Long>();
			int questionCount = kbaForm.getKBAQuestions().size();

			for (int i = 0; i < questionCount; i++) {

				questionAnswerPairs.put(kbaForm.getKBAQuestions().get(i)
						.getKBAQuestionCode(),
						Long.valueOf(form.getAnswerCode()[i]));
			}

			List<KBAQuestion> kbaQuestions = kbaForm.getKBAQuestions();

			submitKBAResponse = cvsService.submitKBAAnswers(consumerProfile,
					vendorEventId, questionSetId, questionAnswerPairs);
			ResultType resultType = submitKBAResponse.getResultType();

			if (ResultType.questions.equals(resultType)) {
				String vendorAuthenticationEventCode = submitKBAResponse
						.getVendorEventId();
				kbaForm.setVendorAuthenticationEventCode(vendorAuthenticationEventCode);
				kbaQuestions = kbaForm.getKBAQuestions();
				kbaQuestions.clear();
				MultipleChoiceQuestions multipleChoiceQuestions = submitKBAResponse
						.getQuestions();
				kbaForm.setKBAQuestionsSetCode(multipleChoiceQuestions
						.getQuestionsSetId());
				MultipleChoiceQuestion[] kbaIdentityQuestions = multipleChoiceQuestions
						.getItem();
				for (MultipleChoiceQuestion kbaIdentityQuestion : kbaIdentityQuestions) {
					KBAQuestion kbaQuestion = new KBAQuestion();
					kbaQuestion.setKBAQuestionCode(String
							.valueOf(kbaIdentityQuestion.getQuestionId()));
					kbaQuestion.setKBAQuestionDescription(kbaIdentityQuestion
							.getQuestionText());
					List<KBAAnswer> kbaAnswers = kbaQuestion.getKbaAnswers();
					// kbaAnswers.clear();
					MultipleChoicePossibleAnswer[] kbaIdentityAnswers = kbaIdentityQuestion
							.getChoices();
					for (MultipleChoicePossibleAnswer kbaIdentityAnswer : kbaIdentityAnswers) {
						KBAAnswer kbaAnswer = new KBAAnswer();
						kbaAnswer.setKBAAnswerCode(String
								.valueOf(kbaIdentityAnswer.getChoiceId()));
						kbaAnswer.setKBAAnswerDescription(kbaIdentityAnswer
								.getChoiceText());
						kbaAnswers.add(kbaAnswer);
					}
					kbaQuestion.setKbaAnswers(kbaAnswers);
					kbaQuestions.add(kbaQuestion);
				}
				kbaForm.setKBAQuestions(kbaQuestions);

				logr.info("Second set of KBA Questions presented for the ConsumerProfile  :"
						+ custId);
				cpa.setActivityLogCode(ConsumerProfileActivity.KBA_SECOND_QUESTION_CODE);
				profileService.insertConsumerActivityLog(cpa, userId);
				forward = mapping.findForward(FORWARD_QUESTIONS);

			} else {
				if (null != session.getAttribute("getKBAQuestionsForm")) {
					session.removeAttribute("getKBAQuestionsForm");
				}

				if (ResultType.passed.equals(resultType)) {

					logr.info("Submit KBA Answers Call was successful for the ConsumerProfile  :"
							+ custId);
					cpa.setActivityLogCode(ConsumerProfileActivity.KBA_PASSED_CODE);
					profileService.insertConsumerActivityLog(cpa, userId);
					forward = mapping.findForward(FORWARD_SUCCESS);

				} else if (ResultType.failed.equals(resultType)) {

					logr.error("Submit KBA Questions Call failed for the ConsumerProfile  :"
							+ custId);
					cpa.setActivityLogCode(ConsumerProfileActivity.KBA_FAILED_CODE);
					profileService.insertConsumerActivityLog(cpa, userId);
					forward = mapping.findForward(FORWARD_FAILED);
				}
			}

		} catch (DataSourceException consumerServiceException) {
			logr.error("Failed to fetch the ConsumerProfile For User ID :"
					+ custId, consumerServiceException);
		} catch (Exception cvsException) {

			logr.error(
					" Call to SUbmit KBA call resulted in Error  for the ConsumerProfile  :"
							+ custId, cvsException);
			cpa.setActivityLogCode(ConsumerProfileActivity.KBA_ERROR_CODE);
			try {
				profileService.insertConsumerActivityLog(cpa, userId);
			} catch (DataSourceException dataSourceException) {

			}

		}

		return forward;
	}

}
