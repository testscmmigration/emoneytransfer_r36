package emgadm.consumer;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.model.KBAQuestion;


public class SubmitKBAQuestionsForm extends EMoneyGramAdmBaseValidatorForm {

	private List<KBAQuestion> KBAQuestions = new ArrayList<KBAQuestion>();
	private String VendorAuthenticationEventCode;
	private String additionalInfoCode;
	private Long KBAQuestionsSetCode;
	private String[] answerCode = new String[6];
	private String counter;
	private String[] questions;

	public String[] getQuestions() {
		return questions;
	}

	public void setQuestions(String[] questions) {
		this.questions = questions.clone();
	}

	public String getCounter() {
		return counter;
	}

	public void setCounter(String counter) {
		this.counter = counter;
	}

	public String[] getAnswerCode() {
		return answerCode;
	}

	public void setAnswerCode(String[] answerCode) {
		this.answerCode = answerCode.clone();
	}

	public List<KBAQuestion> getKBAQuestions() {
		return KBAQuestions;
	}

	public void setKBAQuestions(List<KBAQuestion> kBAQuestions) {
		KBAQuestions = kBAQuestions;
	}

	public String getVendorAuthenticationEventCode() {
		return VendorAuthenticationEventCode;
	}

	public void setVendorAuthenticationEventCode(
			String vendorAuthenticationEventCode) {
		VendorAuthenticationEventCode = vendorAuthenticationEventCode;
	}

	public String getAdditionalInfoCode() {
		return additionalInfoCode;
	}

	public void setAdditionalInfoCode(String additionalInfoCode) {
		this.additionalInfoCode = additionalInfoCode;
	}

	public Long getKBAQuestionsSetCode() {
		return KBAQuestionsSetCode;
	}

	public void setKBAQuestionsSetCode(Long kBAQuestionsSetCode) {
		KBAQuestionsSetCode = kBAQuestionsSetCode;
	}
	
	public ActionErrors validate(
			ActionMapping mapping,
			HttpServletRequest request)
		{
		boolean isValid = true;
		int qSize = questions.length;
		for(int i=0;i<qSize;i++){
			if(null==answerCode[i]){
				isValid=false;
				break;
			}
						
		}
		ActionErrors errors = new ActionErrors();
		if(!isValid){
		
				errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError("error.must.answer.all.questions"));
		
		}
		return errors;
	
		}
	
	
	

}
