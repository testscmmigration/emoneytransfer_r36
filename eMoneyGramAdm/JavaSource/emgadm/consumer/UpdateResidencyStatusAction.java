package emgadm.consumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.DuplicateActiveProfileException;
import emgshared.model.ConsumerProfile;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;

public class UpdateResidencyStatusAction extends EMoneyGramAdmBaseAction {
	private static final String FORWARD_SUCCESS = "success";

	private static final String FORWARD_FAILURE = "failure";
	private static final String NOT_SPECIFIED = "Not Specified";
	private static final String MESSAGE_DISPLAYED = "resdStatus";

	public ActionForward execute(ActionMapping mapping, ActionForm f,
			HttpServletRequest request, HttpServletResponse response)
			throws NumberFormatException, DataSourceException,
			DuplicateActiveProfileException {
		ActionForward forward;
		ActionErrors errors = new ActionErrors();
		UpdateResidencyStatusForm form = (UpdateResidencyStatusForm) f;

		String userId = getUserProfile(request).getUID();
		int custId = Integer.parseInt(form.getCustId());
		ConsumerProfileService profileService = ServiceFactory.getInstance()
				.getConsumerProfileService();
		ConsumerProfile cp = profileService.getConsumerProfile(
				Integer.valueOf(custId), userId, "wwwemg");
		if (null == cp.getResidentStatus()) {
			cp.setResidentStatus(NOT_SPECIFIED);
		}
		if (cp.getResidentStatus().equalsIgnoreCase(form.getResidentStatus())) {
			errors.add(MESSAGE_DISPLAYED, new ActionMessage(
					"error.residency.no.change"));
			forward = mapping.findForward(FORWARD_FAILURE);
		} else {
			if (NOT_SPECIFIED.equalsIgnoreCase(form.getResidentStatus())) {
				cp.setResidentStatus(null);
			} else {
				cp.setResidentStatus(form.getResidentStatus());
			}
			profileService.updateConsumerProfile(cp, true, userId);
			errors.add(MESSAGE_DISPLAYED, new ActionMessage(
					"msg.residency.code.changed"));

			forward = mapping.findForward(FORWARD_SUCCESS);
		}

		if (errors != null || errors.size() != 0) {
			saveErrors(request, errors);
		}

		return forward;
	}

}
