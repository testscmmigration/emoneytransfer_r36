/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.consumer;


import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.apache.log4j.Logger;
import com.moneygram.mgo.service.config_v3_4.GetSourceSiteInfoResponse;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgshared.cache.locator.EMTSharedSourceSiteCacheService;
import emgshared.cache.locator.EMTSharedSourceSiteCacheServiceImpl;
import emgshared.dataaccessors.EMGSharedLogger;
import emgadm.model.UserProfile;
import emgshared.exceptions.DataSourceException;
import emgshared.model.ConsumerAddress;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;

/**
 * @author A131
 *
 */
public class ViewAddressHistoryAction extends EMoneyGramAdmBaseAction{
	private static Logger logr = EMGSharedLogger.getLogger(ViewAddressHistoryAction.class);
	private static final String FORWARD_SUCCESS = "success";
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws DataSourceException{
		
		ActionForward forward = null;
		ViewAddressHistoryForm inputForm = (ViewAddressHistoryForm) form;
		UserProfile up = getUserProfile(request);
		int customerId = Integer.parseInt(request.getParameter("custId"));
		inputForm.setCountry(request.getParameter("country"));
		String partnerSideId = request.getParameter("partnerSiteId");
		request.setAttribute("timeZone", timeZone(partnerSideId));


		ConsumerProfileService consumerProfileService =
			ServiceFactory.getInstance().getConsumerProfileService();
		TreeSet<ConsumerAddress> addresses = (TreeSet<ConsumerAddress>) consumerProfileService.getConsumerAddressHistory(customerId, up.getUID(),false);
		TreeSet<ConsumerAddress> profileAddressSet = new TreeSet<ConsumerAddress>();
		TreeSet<ConsumerAddress> nonProfileAddressSet = new TreeSet<ConsumerAddress>();
		//Commented as a part of MBO - 4234
		/*for (Iterator iterator = addresses.iterator(); iterator.hasNext();) {
			ConsumerAddress consumerAddress = (ConsumerAddress) iterator.next();			
			// Code Changed for MBO-2781 (EMT Admin - History of address changes)
			if (null != consumerAddress.getProfileAddressInfo()
					&& !consumerAddress.getProfileAddressInfo().isEmpty()
					&& consumerAddress.getProfileAddressInfo().equals("Y")) {
				profileAddressSet.add(consumerAddress);						
			} else { 
				nonProfileAddressSet.add(consumerAddress);					
			}
		}*/
		request.setAttribute("custId", customerId);
		request.setAttribute("addresses", addresses);
		request.setAttribute("nonaddresses",nonProfileAddressSet);
		request.setAttribute("paddressCount", addresses.size());
		request.setAttribute("npaddressCount", nonProfileAddressSet.size());
		forward = mapping.findForward(FORWARD_SUCCESS);
		return forward;
	}
	// Added for MBO-5942
	private String timeZone(String partnerSite){
		String timeZone = null;
		EMTSharedSourceSiteCacheService proxy = EMTSharedSourceSiteCacheServiceImpl.instance();
		try {
			GetSourceSiteInfoResponse resp =proxy.sourceSiteDetails(partnerSite);
			timeZone =resp.getCountryInfo().getTimeZone();
		} catch (Exception e) {
			logr.error("Exception in calling the Configuration service for time zone", e);
		}
		return timeZone;
		
	}
		
}
