package emgadm.consumer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;



import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.BPSAddress;
import emgadm.model.BPSPhoneDetails;
import emgadm.model.BPSResultUnit;
import emgadm.model.BasicPersonDetailsAcc;
import emgadm.model.SSNinfo;
import emgadm.model.UserProfile;
import emgadm.services.LexNexService;
import emgadm.services.LexNexServiceImpl;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.EMGRuntimeException;

public class ShowBasicPersonSearchAction extends EMoneyGramAdmBaseAction {
	private static final String FORWARD_SUCCESS = "success";
	private static Logger logger = EMGSharedLogger.getLogger(
			ShowBasicPersonSearchAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ShowBasicPersonSearchForm showPersonSearchDetailForm = (ShowBasicPersonSearchForm) form;
		ActionForward forward;
		UserProfile userProfile = getUserProfile(request);
		String userId = userProfile.getUID() == null ? "" : userProfile
				.getUID();
		String uniqueId = "";
		if (request.getParameter("primaryId") != null) {
			uniqueId = request.getParameter("primaryId");
		}
		try {
			LexNexService personSearchService = LexNexServiceImpl
					.getInstance();
			LinkedHashMap<String, BPSResultUnit> personSearchLinkedMap = personSearchService.getBPSSearchResults(
					Long.valueOf(uniqueId), userId);

			for (Entry<String, BPSResultUnit> entry : personSearchLinkedMap
					.entrySet()) {
				String ssnNumber = formatSSN(entry.getKey());
				BPSResultUnit bpsResultRecord = entry.getValue();
				if (null != bpsResultRecord) {
					SSNinfo ssnInfo = bpsResultRecord.getSsnInfo();
					ssnInfo.setSsnNumber(ssnNumber);
					bpsResultRecord.setUniqueId(formatLexId(bpsResultRecord
							.getUniqueId()));
					formatBasicDetails(bpsResultRecord);
					formatAddressList(bpsResultRecord);
					setLastSeenDate(bpsResultRecord.getAddressList());
				}
			}
			showPersonSearchDetailForm.setSsnLinkedMap(personSearchLinkedMap);
			forward = mapping.findForward(FORWARD_SUCCESS);
			return forward;
		} catch (EMGRuntimeException e) {
			logger.error("Exception in getting BPS Search Results "
					+ e.getRootCause());
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_ERROR);
		}
	}

	public static void setLastSeenDate(
			java.util.LinkedList<BPSAddress> bpsAddressList) {
		final String current = "Current";
		if (null != bpsAddressList) {
			Iterator<BPSAddress> iterator1 = bpsAddressList.iterator();
			while (iterator1.hasNext()) {
				BPSAddress bpsAddress = (BPSAddress) iterator1.next();
				SimpleDateFormat sf = new SimpleDateFormat("MM/yyyy");
				
				if (bpsAddress.getLastSeenDate() != null
						&& !"".equals(bpsAddress.getLastSeenDate())) {
					try {
						//MBO-11528 - To reformat existing date with null month (null/yyyy) to only year (yyyy) when month is null to handle parseexception
						String lastSeen = bpsAddress.getLastSeenDate();
						if(lastSeen.contains("null/"))
							lastSeen = lastSeen.substring(lastSeen.indexOf("/")+1);
						if(!lastSeen.contains("/"))
							sf = new SimpleDateFormat("yyyy");
						Date lastSeenDate = sf.parse(lastSeen);
						String formattedSystemDate = sf.format(new Date(System
								.currentTimeMillis()));
						Date formattedDate = sf.parse(formattedSystemDate);
						if (!lastSeenDate.before(formattedDate)) {
							bpsAddress.setLastSeenDate(current);
						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						throw new EMGRuntimeException(e);
					}
					
				}
			}
		}
	}

	public static String formatSSN(String ssnNumber) {
		char[] a = ssnNumber.toCharArray();
		StringBuilder ssnFormatted = new StringBuilder();
		for (int i = 0; i < a.length; i++) {
			if (i == 3 || i == 5) {
				ssnFormatted.append('-');
			}
			ssnFormatted.append(a[i]);
		}
		ssnNumber = ssnFormatted.toString();
		return ssnNumber;

	}

	public static String formatLexId(String lexId) {
		char[] lexIdInArray = lexId.toCharArray();
		StringBuilder ssnFormatted = new StringBuilder();
		for (int i = 0; i < lexIdInArray.length; i++) {
			if (i == 4 || i == 8) {
				ssnFormatted.append('-');
			}
			ssnFormatted.append(lexIdInArray[i]);
		}

		return ssnFormatted.toString();
	}

	public static void formatBasicDetails(BPSResultUnit bpsResultUnit) {

		java.util.LinkedHashSet<String> namesSet = new LinkedHashSet<String>();
		java.util.LinkedHashSet<BasicPersonDetailsAcc> bpsAccSet = new LinkedHashSet<BasicPersonDetailsAcc>();

		java.util.LinkedList<emgadm.model.BasicPersonDetails> basicDetailsList = bpsResultUnit
				.getPersonList();
		if (null != basicDetailsList) {
			Iterator<emgadm.model.BasicPersonDetails> basicDetailsIterator = basicDetailsList
					.iterator();
			while (basicDetailsIterator.hasNext()) {
				emgadm.model.BasicPersonDetails basicDetails = basicDetailsIterator
						.next();
				if (null != basicDetails) {
					String fullName = formatNames(basicDetails);
					namesSet.add(fullName);
					BasicPersonDetailsAcc bpsAcc = new BasicPersonDetailsAcc();
					bpsAcc.setAge(basicDetails.getAge());
					bpsAcc.setDob(basicDetails.getDob());
					bpsAcc.setGender(basicDetails.getGender());
					bpsAccSet.add(bpsAcc);
				}
			}
			bpsResultUnit.setPersonNameSet(namesSet);
			bpsResultUnit.setAccessoryDetailsSet(bpsAccSet);
		}
	}

	public static String formatNames(
			emgadm.model.BasicPersonDetails basicDetails) {
		final String space = " ";

		StringBuilder fullNameBuilder = new StringBuilder();
		fullNameBuilder.append(basicDetails.getPrefix().trim());
		fullNameBuilder.append(space);
		fullNameBuilder.append(basicDetails.getFirstName().trim());
		fullNameBuilder.append(space);
		fullNameBuilder.append(basicDetails.getMiddleName().trim());
		fullNameBuilder.append(space);
		fullNameBuilder.append(basicDetails.getLastName().trim());

		return fullNameBuilder.toString();
	}

	public static void formatAddressList(BPSResultUnit bpsResultUnit) {
		try {
			LinkedList<BPSAddress> bpsAddressList = new LinkedList<BPSAddress>();
			if (null != bpsResultUnit.getAddressList()) {
				Iterator<BPSAddress> addressSetIterator = bpsResultUnit
						.getAddressList().iterator();
				while (addressSetIterator.hasNext()) {
					BPSAddress bpsAddress = addressSetIterator.next();
					if (!bpsAddressList.contains(bpsAddress)) {
						bpsAddressList.add(bpsAddress);
					} else {
						Iterator<BPSAddress> addressIterator = bpsAddressList
								.iterator();
						while (addressIterator.hasNext()) {
							BPSAddress addressInNewList = addressIterator
									.next();
							if (addressInNewList.equals(bpsAddress)) {
								formatPhoneDetails(bpsAddress, addressInNewList);
							}
						}
					}

				}
				bpsResultUnit.getAddressList().clear();
				bpsResultUnit.setAddressList(bpsAddressList);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new EMGRuntimeException(e);
		}

	}

	public static void formatPhoneDetails(BPSAddress addressFromRecord,
			BPSAddress newAddress) {
		try {
			java.util.ArrayList<BPSPhoneDetails> phoneListFromRecord = null;
			if (addressFromRecord.getBpsPhoneDetails() != null) {
				phoneListFromRecord = addressFromRecord.getBpsPhoneDetails();
			}
			java.util.ArrayList<BPSPhoneDetails> phoneListFromDisplayRecord;
			if (newAddress.getBpsPhoneDetails() != null) {
				phoneListFromDisplayRecord = newAddress.getBpsPhoneDetails();
			} else {
				phoneListFromDisplayRecord = new java.util.ArrayList<BPSPhoneDetails>();
			}
			if (null != phoneListFromRecord) {
				Iterator<BPSPhoneDetails> phoneListIterator = phoneListFromRecord
						.iterator();
				while (phoneListIterator.hasNext()) {
					BPSPhoneDetails phoneDetailInRecord = (BPSPhoneDetails) phoneListIterator
							.next();
					if (!phoneListFromDisplayRecord
							.contains(phoneDetailInRecord)) {
						phoneListFromDisplayRecord.add(phoneDetailInRecord);
					}
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block

			throw new EMGRuntimeException(e);

		}

	}
}
