package emgadm.consumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;



import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.SSNinfo;
import emgadm.model.UserProfile;
import emgadm.model.relativessearch.Associates;
import emgadm.model.relativessearch.RelativeAddresses;
import emgadm.services.LexNexService;
import emgadm.services.LexNexServiceImpl;
import emgshared.dataaccessors.EMGSharedLogger;


public class ShowRelativesSearchAction extends EMoneyGramAdmBaseAction {

	private static Logger logger = EMGSharedLogger.getLogger(
			ShowRelativesSearchAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ShowRelativesSearchForm showRelativePersonSearchForm = (ShowRelativesSearchForm) form;
		ActionForward forward;
		UserProfile userProfile = getUserProfile(request);

		try {
			String userId = userProfile.getUID() == null ? "" : userProfile
					.getUID();
			String uniqueId = "";
			if (request.getParameter("primaryId") != null) {
				uniqueId = request.getParameter("primaryId");
			}
			
			LexNexService relativeSearchService = LexNexServiceImpl
					.getInstance();
			emgadm.model.relativessearch.RelativeSearchResponse relativeSearchResponse = relativeSearchService
					.getRelativesSearchResults(Long.valueOf(uniqueId), userId);
			if (relativeSearchResponse != null) {
				iterateResponse(relativeSearchResponse);
			} 
			showRelativePersonSearchForm
					.setRelativesSearchResponse(relativeSearchResponse);
				
			forward = mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		} catch (Exception e) {
			logger.error("exception in getting relatives Search Results"
					,e);
			
			forward = mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_ERROR);

		}

		return forward;
	}

	/**
	 * 
	 * to set depth in nested list of relatives reponse taken
	 * 
	 * @param relativeSearchResponse
	 */
	public static void iterateResponse(
			emgadm.model.relativessearch.RelativeSearchResponse relativeSearchResponse) {
		logger.debug("iterateResponse--->"
				+ relativeSearchResponse.getTransactionId());
		if (null != relativeSearchResponse.getRelatives()
				&& relativeSearchResponse.getRelatives().getRelativesList() != null
				&& !(relativeSearchResponse.getRelatives().getRelativesList()
						.isEmpty())) {
			java.util.LinkedList<emgadm.model.relativessearch.Relative> relativesList = relativeSearchResponse
					.getRelatives().getRelativesList();
			setDepth(relativesList, 0);
			
		}
		if (null != relativeSearchResponse.getAssociates()) {
			formatAssociatesDetails(relativeSearchResponse.getAssociates());
		}

		logger.debug("<---iterateResponse");

	}

	public static void formatAssociatesDetails(Associates associates) {
		java.util.LinkedList<emgadm.model.relativessearch.Associate> associateList = associates.getAssociateList();
		if (null != associateList) {
			java.util.Iterator<emgadm.model.relativessearch.Associate> associatesListIterator = associateList
					.iterator();
			while (associatesListIterator.hasNext()) {
				emgadm.model.relativessearch.Associate associate = associatesListIterator
						.next();
				String ssnNumber = emgadm.util.StringHelper.emptyString;
				if (associate.getAssociateIdentity() != null
						&& associate.getAssociateIdentity().getSsnInfo() != null
						&& associate.getAssociateIdentity().getSsnInfo()
								.getSsnNumber() != null) {
					int indexArray[] = { 3, 5 };
					ssnNumber = emgadm.util.StringHelper
							.formatStringWithHyphen(associate
									.getAssociateIdentity().getSsnInfo()
									.getSsnNumber(), indexArray);
					associate.getAssociateIdentity().getSsnInfo()
							.setSsnNumber(ssnNumber);
				} else if (associate.getAssociateIdentity() != null
						&& associate.getAssociateIdentity().getSsnInfo() == null) {
					emgadm.model.SSNinfo ssnInfo = new SSNinfo(ssnNumber,
							ssnNumber, ssnNumber, ssnNumber);
					associate.getAssociateIdentity().setSsnInfo(ssnInfo);
				}
				String lexId = emgadm.util.StringHelper.emptyString;
				if (associate.getUniqueId() != null) {
					int indexArray[] = { 4, 8 };
					lexId = emgadm.util.StringHelper.formatStringWithHyphen(
							associate.getUniqueId(), indexArray);
				}
				associate.setUniqueId(lexId);
				java.util.LinkedList<emgadm.model.relativessearch.BaseAddress> addressList = associate
						.getAssociateAddresses().getAssociateAddressList();
				if (null != addressList) {
					java.util.Iterator<emgadm.model.relativessearch.BaseAddress> addressListIterator = addressList
							.iterator();
					while (addressListIterator.hasNext()) {
						emgadm.model.relativessearch.BaseAddress baseAddress = addressListIterator
								.next();
						if (baseAddress.getAddress() == null) {
							baseAddress
									.setAddress(new emgadm.model.relativessearch.Address());
						}
						baseAddress.getAddress().formatAddress();
						logger.debug(baseAddress.getAddress()
								.getFormattedAddress());
					}
				}
			}
		}
	}

	/**
	 * 
	 * to set depth of nested list of relatives
	 * 
	 * @param relativesList
	 * @param depth
	 */
	public static void setDepth(
			java.util.LinkedList<emgadm.model.relativessearch.Relative> relativesList,
			int depth) {
		logger.debug("setDepth--->" + relativesList.size());
		int depthInsideList = depth + 1;
		java.util.Iterator<emgadm.model.relativessearch.Relative> relativesIterator = relativesList
				.iterator();
		while (relativesIterator.hasNext()) {
			emgadm.model.relativessearch.Relative relativeInside = relativesIterator
					.next();
			relativeInside.setDepth(depthInsideList);
			String ssnNumber = emgadm.util.StringHelper.emptyString;
			if (relativeInside.getRelativeIdentity() != null
					&& relativeInside.getRelativeIdentity().getSsnInfo() != null
					&& relativeInside.getRelativeIdentity().getSsnInfo()
							.getSsnNumber() != null) {
				int indexArray[] = {3,5};
				ssnNumber = emgadm.util.StringHelper.formatStringWithHyphen(
						relativeInside.getRelativeIdentity().getSsnInfo()
								.getSsnNumber(), indexArray);
				relativeInside.getRelativeIdentity().getSsnInfo()
						.setSsnNumber(ssnNumber);
			} else if (relativeInside.getRelativeIdentity() != null
					&& relativeInside.getRelativeIdentity().getSsnInfo() == null) {
				emgadm.model.SSNinfo ssnInfo = new SSNinfo(ssnNumber,
						ssnNumber, ssnNumber, ssnNumber);
				relativeInside.getRelativeIdentity().setSsnInfo(ssnInfo);
			}

			String lexId = emgadm.util.StringHelper.emptyString;
			if (relativeInside.getUniqueId() != null) {
				int indexArray[] = { 4, 8 };
				if (null != relativeInside.getUniqueId()) {
					lexId = emgadm.util.StringHelper.formatStringWithHyphen(
							relativeInside.getUniqueId(), indexArray);
				}
			}
			relativeInside.setUniqueId(lexId);
			if (relativeInside.getRelativeAddresses() != null) {
				formatAddress(relativeInside.getRelativeAddresses());
			}
			if (null != relativeInside.getRelatives()
					&& null != relativeInside.getRelatives().getRelativesList()
					&& !(relativeInside.getRelatives().getRelativesList()
							.isEmpty())) {
				setDepth(relativeInside.getRelatives().getRelativesList(),
						depthInsideList);

			}
		}
	}

	public static void formatAddress(RelativeAddresses relativeAddresses) {
		java.util.LinkedList<emgadm.model.relativessearch.BaseAddress> addressList = relativeAddresses
				.getRelativeAddressList();
		if (null != addressList) {
			java.util.Iterator<emgadm.model.relativessearch.BaseAddress> addressListIterator = addressList
					.iterator();
			while (addressListIterator.hasNext()) {
				emgadm.model.relativessearch.BaseAddress baseAddress = addressListIterator
						.next();
				if (null == baseAddress.getAddress()) {
					baseAddress
							.setAddress(new emgadm.model.relativessearch.Address());
				}
				baseAddress.getAddress().formatAddress();
			}
		}
	}

}
