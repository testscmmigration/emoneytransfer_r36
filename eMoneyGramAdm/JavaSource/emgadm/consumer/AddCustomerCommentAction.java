/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.consumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.json.JSONArray;
import org.json.JSONObject;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.services.iplookup.melissadata.client.IPLocatorService;
import emgadm.services.iplookup.melissadata.client.ResponseArray;
import emgadm.services.iplookup.melissadata.client.ResponseRecord;
import emgshared.exceptions.DataSourceException;
import emgshared.model.ConsumerProfileComment;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;

/**
 * @author A131
 *
 */
public class AddCustomerCommentAction extends EMoneyGramAdmBaseAction
{
	private static final String FORWARD_SUCCESS = "success";
	private static final String FORWARD_FAILURE = "failure";

    private String doIpLookup(String ip) {
    	StringBuilder message = new StringBuilder();
    	JSONArray records = null;
		JSONObject result = null;
    	try {
    		boolean isIpLookuponScoring = false;
    		/*MBO-5734 Changes Starts*/
        	JSONObject response = IPLocatorService.ipLocate(isIpLookuponScoring,ip);
        	records = response.getJSONArray("Records");
			result = records.getJSONObject(0);	
        	message.append(ip);
        	message.append(" Country: " + (result.get("CountryName")));
        	message.append(" Region: " + (result.get("Region")));
        	message.append(" City: " + (result.get("City")));
        	message.append(" ISP: " + (result.get("ISPName")));
        	message.append(" Zip Code: " + (result.get("PostalCode")));
        	/*MBO-5734 Changes Ends*/
        } catch(Exception e) {
        	message.append("IP lookup failed: " + e.getMessage());
        }
        return message.toString();
    }

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws NumberFormatException, DataSourceException
	{
		ActionForward forward;
		AddCustomerCommentForm inputForm = (AddCustomerCommentForm) form;
		String commentText = inputForm.getCommentText();
		
		if(StringUtils.isBlank(inputForm.getCommentReasonCode()))
		{
			ActionErrors errors = new ActionErrors();
			errors.add(
				"commentReason",
				new ActionMessage("errors.required", "Comment Reason"));
			saveErrors(request, errors);
			return mapping.findForward(FORWARD_FAILURE);
		}
		
		// due security reason removing all the html codes
		if (!emgadm.util.StringHelper.isNullOrEmpty(commentText))
		commentText=commentText.replaceAll("\\<.*?>","");
		
		if (StringUtils.isNotBlank(commentText))
		{
		    if (commentText.length() > 255) {
				ActionErrors errors = new ActionErrors();
				String[] parm = {"Comment text", "255"};
				errors.add("commentText",
					new ActionMessage("errors.length.too.long", parm));
				saveErrors(request, errors);
				forward = mapping.findForward(FORWARD_FAILURE);
		    } else {
				ConsumerProfileComment comment = new ConsumerProfileComment();
				if (inputForm.getCommentReasonCode().equals("IP")) {
					comment.setCustId(Integer.parseInt(inputForm.getCustId()));
					comment.setReasonCode("OTH");
					comment.setText(doIpLookup(commentText));
				} else {
					comment.setCustId(Integer.parseInt(inputForm.getCustId()));			
					comment.setReasonCode(inputForm.getCommentReasonCode());
					comment.setText(commentText);
				}
				ConsumerProfileService profileService =
					ServiceFactory.getInstance().getConsumerProfileService();
				profileService.addConsumerProfileComment(
					comment,
					getUserProfile(request).getUID());
				inputForm.setCommentText(null);
				forward = mapping.findForward(FORWARD_SUCCESS);
			}
		} else
		{
			ActionErrors errors = new ActionErrors();
			errors.add(
				"commentText",
				new ActionMessage("errors.required", "Comment text"));
			saveErrors(request, errors);
			forward = mapping.findForward(FORWARD_FAILURE);
		}
		return forward;
	}
}
