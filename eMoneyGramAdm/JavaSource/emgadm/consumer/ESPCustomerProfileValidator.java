package emgadm.consumer;

import emgadm.model.UserProfile;

public class ESPCustomerProfileValidator extends BaseCustomerProfileValidator {

	public ESPCustomerProfileValidator(UpdateCustomerProfileForm form,
			UserProfile userProfile, CustomerProfileRegexPattern customerProfileRegex) {
		super(form, userProfile, customerProfileRegex);
		//https://mgonline.atlassian.net/browse/MBO-10173
		//Making as Second Last Name/Mother's Last Name as required field
		fields.add(new AlphaFieldWithSpecialChars(form.getCustSecondLastName(),
				SECOND_LAST_NAME, customerProfileRegex.getLastNameRegex()).setMaxLength(30).isRequired(true));
		fields.add(new StrictNumericField(form.getPostalCode(), ZIP_CODE)
				.setMinLength(5).setMaxLength(5));
		fields.add(new AlphaField(form.getState(), STATE).isRequired(false));
		fields.add(new AddressField(form.getPhoneCountryCode(), COUNTRY_CODE, customerProfileRegex.getAddressLine1Regex())
				.isRequired(false));
		fields.add(new StrictNumericField(form.getLoyaltyPgmMembershipId(),
				PLUS).setMinLength(9).setMaxLength(12).isRequired(false));
		fields.add(new UntaintedPhoneNumber(form.getPhoneNumber(), PHONE_NUMBER)
				.setMinLength(7).setMaxLength(14).isRequired(false)
				.setUID(userProfile.getUID())
				.setCountryDialingCode(form.getPhoneCountryCode()));
	}
}
