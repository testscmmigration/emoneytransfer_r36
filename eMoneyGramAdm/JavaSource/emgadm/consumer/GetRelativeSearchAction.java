package emgadm.consumer;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


import com.thoughtworks.xstream.XStream;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.services.LexNexService;
import emgadm.services.LexNexServiceImpl;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.model.ConsumerProfile;
import emgshared.model.LexisNexisActivity;
import emgshared.model.SearchCriteriaDTO;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;

public class GetRelativeSearchAction extends EMoneyGramAdmBaseAction {

	private static final ServiceFactory sharedServiceFactory = ServiceFactory
			.getInstance();
	private static final ConsumerProfileService profileService = sharedServiceFactory
			.getConsumerProfileService();
	private static Logger logger = EMGSharedLogger.getLogger(
			GetPersonSearchAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws NumberFormatException, DataSourceException, Exception {
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		GetRelativeSearchForm relativeSearchForm = (GetRelativeSearchForm) form;
		int custId = relativeSearchForm.getCustId();
		ActionForward forward = null;
		String userId = getUserProfile(request).getUID();
		String ipAddress = request.getRemoteAddr();
		ConsumerProfile consumerProfile = profileService.getConsumerProfile(
				custId, userId, null);
		try {
			getRelativeSearch(consumerProfile, custId, ipAddress, request,
					response);
		} catch (Exception e) {
			logger.error("Exception Occoured while getting relative details"
					+ e);
			response.sendError(500);
		}
		return forward;
	}

	/**
	 * @author aip8
	 * 
	 *         Method to get Person from Customer Validation Service Proxy and
	 *         send back XML response to customer profile page. Used Xstream to
	 *         parse the LexisNexisActivity object to XML
	 * 
	 * @param ConsumerProfile
	 *            ,customerId,HttpServletRequest,HttpServletResponse
	 * 
	 */
	public void getRelativeSearch(ConsumerProfile consumerProfile, int custId,
			String ipAddress, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		LexNexService lexNexService = LexNexServiceImpl
		.getInstance();
		try {
			SearchCriteriaDTO searchCriteria = new SearchCriteriaDTO();
			searchCriteria.setCustId(custId);
			lexNexService.getRelativeSearch(consumerProfile, custId,
					request.getSession().getAttribute("personUniqueId")
							.toString(), ipAddress);
			List<LexisNexisActivity> activityList = profileService
					.getLexisNexisActivityLog(searchCriteria);
			request.getSession().removeAttribute("personUniqueId");
			XStream xstream = new XStream();
			xstream.alias("Activity", LexisNexisActivity.class);
			String xmlStr = xstream.toXML(activityList);
			response.getWriter().println(xmlStr);
		} catch (Exception e) {
			throw e;
		}
	}
}
