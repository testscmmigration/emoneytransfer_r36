package emgadm.consumer;

import emgadm.consumer.BaseCustomerProfileValidator.StrictNumericField;
import emgadm.model.UserProfile;

/**
 * Validator used for Germany (DEU, the ISO 3166-1 alpha-3 code )
 * 
 * @author vz70
 * 
 */
public class DEUCustomerProfileValidator extends BaseCustomerProfileValidator {

	public DEUCustomerProfileValidator(UpdateCustomerProfileForm form,
			UserProfile userProfile, CustomerProfileRegexPattern customerProfileRegex) {
		fields.add(new DEUAlphaFieldWithSpecial(form.getCustFirstName(), "First Name", customerProfileRegex.getFirstNameRegex())
				.setMaxLength(40));
		fields.add(new DEUAlphaFieldWithSpecial(form.getCustLastName(), "Last Name", customerProfileRegex.getLastNameRegex())
				.setMaxLength(40));
		fields.add(new DEUAlphaField(form.getMiddleName(), "Middle Name")
				.setMaxLength(20).isRequired(false));
		fields.add(new DEUAlphaField(form.getCustSecondLastName(),
				"Second Last Name").setMaxLength(30).isRequired(false));
		fields.add(new DEUCityField(form.getCity(), "Town/City", customerProfileRegex.getCityRegex())
				.setMaxLength(40));
		fields.add(new StrictNumericField(form.getPostalCode(), "Postcode")
				.setMinLength(5).setMaxLength(5));
		fields.add(new AddressField(form.getPhoneCountryCode(), "Country Code",  customerProfileRegex.getAddressLine1Regex())
				.isRequired(true));
		fields.add(new UntaintedPhoneNumber(form.getPhoneNumber(),
				"Phone Number")
				.setUID(userProfile.getUID())
				.setCountryDialingCode(form.getPhoneCountryCode()));
		fields.add(new UntaintedPhoneNumber(form.getPhoneNumberAlternate(),
				"Phone Number Alternate")
				.isRequired(false).setUID(userProfile.getUID())
				.setCountryDialingCode(form.getPhoneCountryCode()));
		fields.add(new BirthDateField(form.getBirthDateText(), "Birth Date"));

		// Note: Not all addresses have street names, Baltrum for example does
		// not have any.
		fields.add(new DEUAddressField(form.getAddressLine1(), "Street", customerProfileRegex.getAddressLine1Regex())
				.setMaxLength(100));
		fields.add(new DEUAddressField(form.getAddressLine2(), "Address line 2", customerProfileRegex.getAddressLine2Regex())
				.setMaxLength(100).isRequired(false));
		fields.add(new DEUAddressField(form.getAddressLine3(), "Address Line 3", customerProfileRegex.getAddressLine3Regex())
				.setMaxLength(100).isRequired(false));
		fields.add(new StrictNumericField(form.getLoyaltyPgmMembershipId(),
				"Plus #").setMinLength(9).setMaxLength(12).isRequired(false));
	}

	private class DEUAlphaField extends AlphaField {
		DEUAlphaField(String value, String friendlyName) {
			super(value, friendlyName);
			pattern = "[^a-zA-Z �������]";
			keyStringMessage = "errors.non.alpha";
		}
	}

	private class DEUAlphaFieldWithSpecial extends AlphaField {
		DEUAlphaFieldWithSpecial(String value, String friendlyName, String patternFromDB) {
			super(value, friendlyName);
			pattern = patternFromDB;
			keyStringMessage = "errors.non.alphaSpecial";
		}
	}
	
	private class DEUAddressField extends AddressField {
		DEUAddressField(String value, String friendlyName, String patternFromDB) {
			super(value, friendlyName, patternFromDB );
			pattern = patternFromDB;
			keyStringMessage = "errors.non.addressdigit";
		}
	}
	
	private class DEUCityField extends StringField {
		DEUCityField(String value, String friendlyName, String patternFromDB) {
			super(value, friendlyName);
			pattern = patternFromDB;
			keyStringMessage = "errors.invalid.city";
		}
	}

}
