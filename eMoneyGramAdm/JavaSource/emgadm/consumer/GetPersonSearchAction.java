package emgadm.consumer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


import com.moneygram.mgo.service.consumerValidationV2_1.RiskIndicator;
import com.thoughtworks.xstream.XStream;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.services.LexNexService;
import emgadm.services.LexNexServiceImpl;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.model.ConsumerProfile;
import emgshared.model.LexisNexisActivity;
import emgshared.model.SearchCriteriaDTO;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;
import emgshared.util.Constants;

public class GetPersonSearchAction extends EMoneyGramAdmBaseAction {
	
	private static final ServiceFactory sharedServiceFactory = ServiceFactory.getInstance();
	private static final ConsumerProfileService profileService = sharedServiceFactory.getConsumerProfileService();
	private static Logger logger = EMGSharedLogger.getLogger(GetPersonSearchAction.class);
		
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws NumberFormatException, DataSourceException, Exception {
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		GetPersonSearchForm personSearchForm = (GetPersonSearchForm) form;
		int custId = personSearchForm.getCustId();
		ActionForward forward = null;
		String userId = getUserProfile(request).getUID();
		String ipAddress = request.getRemoteAddr();
		ConsumerProfile consumerProfile = profileService.getConsumerProfile(
				custId, userId, null);
		try {
			logger.info("BPS Search started for cust id : "+custId);
			getPersonSearch(consumerProfile, custId, ipAddress, request, response);
			logger.info("BPS Search ended for cust id : "+consumerProfile.getId());
		} catch (Exception e) {
			logger.error("Exception Occoured while getting peron details" + e);
			response.sendError(500);
		}
		return forward;
	}
	
	/**
	 * @author aip8
	 * 
	 * Method to get Person from Customer Validation Service Proxy
	 * and send back XML response to customer profile page. Used Xstream to parse
	 * the LexisNexisActivity object to XML
	 * 
	 * @param ConsumerProfile,customerId,HttpServletResponse
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public void getPersonSearch(ConsumerProfile consumerProfile, int custId,
			String ipAddress, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		LexNexService lexNexService = LexNexServiceImpl
				.getInstance();
		try {
			lexNexService.getPersonSearch(consumerProfile, custId,
					ipAddress);
			SearchCriteriaDTO searchCriteria = new SearchCriteriaDTO();
			searchCriteria.setCustId(custId);
			List<LexisNexisActivity> activityList = profileService
					.getLexisNexisActivityLog(searchCriteria);
			XStream xstream = new XStream();
			xstream.alias("Activity", LexisNexisActivity.class);
			xstream.alias("RiskIndicator", RiskIndicator.class);
			String xmlStr = "";
			if (null != activityList && !activityList.isEmpty()) {
				Iterator iterActivity = activityList.iterator();
				while (iterActivity.hasNext()) {
					LexisNexisActivity activity = (LexisNexisActivity) iterActivity.next();
					if (Constants.BPSCode.equals(activity.getSearchType())) {
						request.getSession().setAttribute("personUniqueId",
								activity.getUniqueId());
						xmlStr = xstream.toXML(activity);
					}
				}
			}
			response.getWriter().println(xmlStr);
		} catch (Exception e) {
			throw e;
		}
	}
}
