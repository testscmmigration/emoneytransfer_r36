/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.consumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import emgadm.actions.EMoneyGramAdmBaseAction;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.MaxRowsHashCryptoException;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileActivity;
import emgshared.model.UserActivityType;
import emgshared.services.ConsumerProfileService;
import emgshared.services.FraudService;
import emgshared.services.ServiceFactory;

/**
 * @author A119
 *
 */
public class ClearMasterTaintAction extends EMoneyGramAdmBaseAction {
	private static final String FORWARD_SUCCESS = "success";
	private static final String FORWARD_FAILURE = "failure";
	private ConsumerProfileService profileService = ServiceFactory.getInstance().getConsumerProfileService();
	private static final FraudService fraudService = emgshared.services.ServiceFactory.getInstance().getFraudService();


	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws NumberFormatException, DataSourceException, MaxRowsHashCryptoException {
		ActionForward forward;
		ActionErrors errors = new ActionErrors();
		String userId = getUserProfile(request).getUID();
		ClearMasterTaintForm inputForm = (ClearMasterTaintForm) form;
		String custId = inputForm.getCustId();
        int consumerId = Integer.parseInt(custId);
        ConsumerProfile consumerProfile = null;
        forward = mapping.findForward(FORWARD_SUCCESS);
        try {
            consumerProfile = profileService.getConsumerProfile(Integer.valueOf(consumerId), userId, null);
            fraudService.clearMasterBlockIndicator(custId, userId);
			ConsumerProfileActivity cpa = new ConsumerProfileActivity();
			cpa.setCustId(consumerProfile.getId());
			cpa.setActivityLogCode(ConsumerProfileActivity.PROFILE_MASTER_TAINT_CLEAR_CODE);
			profileService.insertConsumerActivityLog(cpa, userId);
            super.insertActivityLog(this.getClass(),request,"Profile Master Taint Cleared: " + consumerProfile.getUserId(),UserActivityType.EVENT_TYPE_CONSUMERPROFILECLEARMASTERTAINT,inputForm.getCustId());
        } catch (Exception e) {
            forward = mapping.findForward(FORWARD_FAILURE);
        }
		if (errors.size() > 0) {
			saveErrors(request, errors);
		}
		return forward;
	}
}
