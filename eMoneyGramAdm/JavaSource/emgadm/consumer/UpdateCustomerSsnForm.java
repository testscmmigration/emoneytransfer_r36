/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.consumer;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

/**
 * @author A131
 *
 */
public class UpdateCustomerSsnForm extends EMoneyGramAdmBaseValidatorForm
{
	private String custId;
	private String custSsn;
	private String saveCustSsn;
	private String custSsnTaintText;
	private boolean allowOverride;

	public boolean getAllowOverride() {
		return allowOverride;
	}

	public void setAllowOverride(boolean allowOverride) {
		this.allowOverride = allowOverride;
	}


	public String getCustId()
	{
		return custId;
	}

	public String getCustSsn()
	{
		return custSsn;
	}

	public void setCustId(String string)
	{
		custId = string;
	}

	public void setCustSsn(String string)
	{
		custSsn = string;
	}

	public String getSaveCustSsn() {
		return saveCustSsn;
	}

	public void setSaveCustSsn(String string) {
		saveCustSsn = string;
	}

	public String getCustSsnTaintText() {
		return custSsnTaintText;
	}

	public void setCustSsnTaintText(String string) {
		custSsnTaintText = string;
	}
}
