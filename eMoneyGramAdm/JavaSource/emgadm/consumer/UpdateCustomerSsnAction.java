/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.consumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.DuplicateActiveProfileException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.exceptions.MaxRowsHashCryptoException;
import emgshared.model.ConsumerProfileComment;
import emgshared.model.UserActivityType;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;
import emgshared.util.StringHelper;

/**
 * @author A131
 *
 */
public class UpdateCustomerSsnAction extends EMoneyGramAdmBaseAction {
	private static final String FORWARD_SUCCESS = "success";
	private static final String FORWARD_FAILURE = "failure";

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws NumberFormatException, DataSourceException, MaxRowsHashCryptoException {
		ActionForward forward;
		ActionErrors errors = new ActionErrors();
		String userId = getUserProfile(request).getUID();
		UpdateCustomerSsnForm inputForm = (UpdateCustomerSsnForm) form;
		String cleartextSsn = inputForm.getCustSsn();
		cleartextSsn = StringHelper.removeNonDigits(cleartextSsn);
		int custId = Integer.parseInt(inputForm.getCustId());

		if (StringUtils.isBlank(cleartextSsn)) {
			errors.add("custSsn", new ActionMessage("errors.required", "SSN"));
			forward = mapping.findForward(FORWARD_FAILURE);
		} else if (cleartextSsn.length() != 4) {
			errors.add(	"custSsn", new ActionMessage("errors.invalid.ssn", cleartextSsn));
			forward = mapping.findForward(FORWARD_FAILURE);
		}  else if (inputForm.getSaveCustSsn().equals(cleartextSsn)) {
			errors.add("custSsn", new ActionMessage("error.same.key", "new consumer SSN"));
			forward = mapping.findForward(FORWARD_FAILURE);
		} else {
			try {
				ConsumerProfileService profileService =	ServiceFactory.getInstance().getConsumerProfileService();
				if (inputForm.getAllowOverride())
				{
					profileService.updateSsn(Integer.parseInt(inputForm.getCustId()), StringUtils.right(cleartextSsn, 4), getUserProfile(request).getUID(),true);
					ConsumerProfileComment comment = new ConsumerProfileComment();
					comment.setCustId(custId);
					comment.setReasonCode("OTH");
					comment.setText("Duplicate Override - SSN Edit");
			        profileService.addConsumerProfileComment(comment, userId);
				}
				else
					profileService.updateSsn(Integer.parseInt(inputForm.getCustId()), StringUtils.right(cleartextSsn, 4), getUserProfile(request).getUID(),false);
				// Add profile change comment
				ConsumerProfileComment comment = new ConsumerProfileComment();
				comment.setCustId(custId);
				comment.setReasonCode("OTH");
				comment.setText("SSN Last 4 Changed");
		        profileService.addConsumerProfileComment(comment, userId);
		        forward = mapping.findForward(FORWARD_SUCCESS);
				try {
				    super.insertActivityLog(this.getClass(),request,null,UserActivityType.EVENT_TYPE_CONSUMERPROFILECHGSSN,String.valueOf(custId));    
				} catch (Exception e) { }
			} catch (DataSourceException e) {
				throw new EMGRuntimeException(e);
			}
			catch (DuplicateActiveProfileException e)
			{
				request.setAttribute("duplicateOverrideSSN", "Y");
				errors.add("custSSNUpdateStatus", new ActionMessage("errors.duplicate.profile.ssn",cleartextSsn));
				forward = mapping.findForward(FORWARD_FAILURE);				
			}
		}
		if (errors.size() > 0) {
			saveErrors(request, errors);
		}

		return forward;
	}
}
