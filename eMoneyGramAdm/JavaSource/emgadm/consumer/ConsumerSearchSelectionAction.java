package emgadm.consumer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.util.StringHelper;
import emgshared.exceptions.AgentConnectException;
import emgshared.exceptions.DataSourceException;
import emgshared.model.ConsumerAddress;
import emgshared.model.ConsumerProfile;
import emgshared.util.Constants;

public class ConsumerSearchSelectionAction extends EMoneyGramAdmBaseAction {
	public ActionForward execute(ActionMapping mapping, ActionForm f,
			HttpServletRequest request, HttpServletResponse response)
			throws AgentConnectException, DataSourceException {

		ConsumerSearchSelectionForm form = (ConsumerSearchSelectionForm) f;
		List<ConsumerAddress> consumerAdress = new ArrayList<ConsumerAddress>();
		List<ConsumerAddress> consumerAdrList = null;

		ConsumerAddress address = null;
		ConsumerProfile cf = (ConsumerProfile) request.getSession()
				.getAttribute("findProfiles");
		/* MBO-2809 EMT Admin Changes- Search Similar Profiles Changes Starts */
		consumerAdrList = (List<ConsumerAddress>) request.getSession()
				.getAttribute("consumerAddressList");
		request.getSession().setAttribute("isSimilarProfileSearch", "yes");
		consumerAdress.addAll(consumerAdrList);
		form.setAddressListSize(consumerAdrList.size());
		if (null == form.getSelectedAddress()) {
			List<ConsumerAddress> sortedConsumerAddress = new LinkedList<ConsumerAddress>();

			if (null != consumerAdress && consumerAdress.size() > 0) {
				for (Iterator<ConsumerAddress> iterator = consumerAdress
						.iterator(); iterator.hasNext();) {
					ConsumerAddress consumerAddress = iterator.next();
					if (null != consumerAddress.getProfileAddressInfo()
							&& consumerAddress.getProfileAddressInfo().equals(
									"Y")) {
						address = consumerAddress;
						iterator.remove();
						break;
					}
				}
				sortedConsumerAddress.add(address);
				sortedConsumerAddress.addAll(consumerAdress);
				form.setAddressList(sortedConsumerAddress);
			} else {
				form.setAddressList(consumerAdress);
			}
		}

		if (null != form.getSelectedAddress()) {
			int selectedAddressId = Integer.parseInt(form.getSelectedAddress());
			for (ConsumerAddress consAddress : consumerAdress) {
				if (selectedAddressId == consAddress.getAddressId()) {
					request.getSession().setAttribute("selectedAddress",
							consAddress);
					break;
				}
			}
		}

		/* MBO-2809 EMT Admin Changes- Search Similar Profiles Changes Ends */

		if (null != cf.getProfileType())
			form.setProfileType(cf.getProfileType());
		form.setCustId(String.valueOf(cf.getId()));
		form.setCustLogonId(cf.getUserId());
		form.setCustFirstName(cf.getFirstName());
		form.setCustLastName(cf.getLastName());
		form.setPartnerSiteId(cf.getPartnerSiteId());

		if (!StringHelper.isNullOrEmpty(form.getSubmitGo())) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_ADVANCED_CONSUMER_SEARCH);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitSelAll())) {
			form.setIncludeFirstName(true);
			form.setIncludeLastName(true);
			form.setIncludeLoginId(true);
			form.setIncludePhone(true);
			form.setIncludeAddr(true);
			form.setIncludeCity(true);
			form.setIncludeState(true);
			form.setIncludeZip(true);
			form.setIncludeProfileStatus(true);
			form.setIncludeCreateDate(true);
			// added by Ankit Bhatt for 562
			if (null != cf.getProfileType()
					&& !"Guest".equalsIgnoreCase(cf.getProfileType()))
				form.setIncludePasswordHash(true);
			form.setIncludeCreateIpAddrId(true);
			form.setIncludePrmrCode(true);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitDeselAll())) {
			form.setIncludeFirstName(false);
			form.setIncludeLastName(false);
			form.setIncludeLoginId(false);
			form.setIncludePhone(false);
			form.setIncludeAddr(false);
			form.setIncludeCity(false);
			form.setIncludeState(false);
			form.setIncludeZip(false);
			form.setIncludeProfileStatus(false);
			form.setIncludeCreateDate(false);
			form.setIncludePasswordHash(false);
			form.setIncludeCreateIpAddrId(false);
			form.setIncludePrmrCode(false);
		}

		form.setFindProfiles("Y");

		return mapping
				.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
