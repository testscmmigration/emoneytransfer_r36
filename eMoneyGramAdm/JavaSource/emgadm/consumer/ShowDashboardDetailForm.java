package emgadm.consumer;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.view.ConsumerBankAccountView;
import emgadm.view.ConsumerCreditCardAccountView;
import emgadm.view.TransactionView;

public class ShowDashboardDetailForm extends EMoneyGramAdmBaseValidatorForm
{
	private String from;
	private String id;
	private String actionDesc;
	private String frst;
	private String last;
	private int custId;
	private String custLogonId;
	private TransactionView[] tranViews = null;
	private ConsumerBankAccountView[] bankAccounts = null;
	private ConsumerCreditCardAccountView[] creditCardAccounts = null;
	private ConsumerCreditCardAccountView[] debitCardAccounts = null;
	private List AccountStatusOptions = new ArrayList();

	public String getFrom()
	{
		return from;
	}

	public void setFrom(String string)
	{
		from = string;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String string)
	{
		id = string;
	}

	public String getActionDesc()
	{
		return actionDesc;
	}

	public void setActionDesc(String string)
	{
		actionDesc = string;
	}

	public String getFrst()
	{
		return frst;
	}

	public void setFrst(String string)
	{
		frst = string;
	}

	public String getLast()
	{
		return last;
	}

	public void setLast(String string)
	{
		last = string;
	}

	public int getCustId()
	{
		return custId;
	}

	public void setCustId(int i)
	{
		custId = i;
	}

	public String getCustLogonId()
	{
		return custLogonId;
	}

	public void setCustLogonId(String string)
	{
		custLogonId = string;
	}

	public TransactionView[] getTranViews()
	{
		return tranViews;
	}

	public void setTranViews(TransactionView[] views)
	{
		tranViews = views.clone();
	}

	public ConsumerBankAccountView[] getBankAccounts()
	{
		return bankAccounts;
	}

	public void setBankAccounts(ConsumerBankAccountView[] views)
	{
		bankAccounts = views.clone();
	}

	public ConsumerCreditCardAccountView[] getCreditCardAccounts()
	{
		return creditCardAccounts;
	}

	public void setCreditCardAccounts(ConsumerCreditCardAccountView[] views)
	{
		creditCardAccounts = views.clone();
	}

	public ConsumerCreditCardAccountView[] getDebitCardAccounts()
	{
		return debitCardAccounts;
	}

	public void setDebitCardAccounts(ConsumerCreditCardAccountView[] views)
	{
		debitCardAccounts = views.clone();
	}

	public List getAccountStatusOptions()
	{
		return AccountStatusOptions;
	}

	public void setAccountStatusOptions(List list)
	{
		AccountStatusOptions = list;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);
		this.from = "N";
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();

		return errors;
	}
}
