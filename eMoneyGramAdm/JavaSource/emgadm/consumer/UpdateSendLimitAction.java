package emgadm.consumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.DuplicateActiveProfileException;
import emgshared.model.ConsumerProfile;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;

public class UpdateSendLimitAction extends EMoneyGramAdmBaseAction {
	private static final String FORWARD_SUCCESS = "success";

	private static final String FORWARD_FAILURE = "failure";
	private static final String NOT_SPECIFIED = "NS";
	private static final String MESSAGE_DISPLAYED = "sndLimit";

	public ActionForward execute(ActionMapping mapping, ActionForm f,
			HttpServletRequest request, HttpServletResponse response)
			throws NumberFormatException, DataSourceException,
			DuplicateActiveProfileException {
		ActionForward forward;
		ActionErrors errors = new ActionErrors();
		UpdateSendLimitForm form = (UpdateSendLimitForm) f;

		String userId = getUserProfile(request).getUID();
		int custId = Integer.parseInt(form.getCustId());
		ConsumerProfileService profileService = ServiceFactory.getInstance()
				.getConsumerProfileService();
		ConsumerProfile cp = profileService.getConsumerProfile(
				Integer.valueOf(custId), userId, "wwwemg");
		if (null == cp.getSendLevel()) {
			cp.setSendLevel(NOT_SPECIFIED);
		}
		if (cp.getSendLevel().equalsIgnoreCase(form.getSendLimit())) {
			errors.add(MESSAGE_DISPLAYED, new ActionMessage(
					"error.send.limit.no.change"));
			forward = mapping.findForward(FORWARD_FAILURE);
		} else {
			if (NOT_SPECIFIED.equalsIgnoreCase(form.getSendLimit())) {
				cp.setSendLevel(null);
			} else {
				cp.setSendLevel(form.getSendLimit());
			}
			profileService.updateConsumerProfile(cp, true, userId);
			errors.add(MESSAGE_DISPLAYED, new ActionMessage(
					"msg.send.limit.changed"));

			forward = mapping.findForward(FORWARD_SUCCESS);
		}

		if (errors != null || errors.size() != 0) {
			saveErrors(request, errors);
		}

		return forward;
	}

}
