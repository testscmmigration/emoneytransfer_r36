package emgadm.consumer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import emgadm.model.UserProfile;
import emgshared.exceptions.DataSourceException;
import emgshared.services.FraudService;
import emgshared.services.ServiceFactory;
import emgshared.util.EUSites;
import emgshared.util.PhoneNumberUtils;

public abstract class BaseCustomerProfileValidator implements
		CustomerProfileValidator {

	public static final String FIRST_NAME = "First Name";
	public static final String LAST_NAME = "Last Name";
	public static final String MIDDLE_NAME = "Middle Name";
	public static final String SECOND_LAST_NAME = "Second Last Name";
	public static final String TOWN_OR_CITY = "Town/City";
	public static final String BIRTH_DATE = "Birth Date";
	public static final String STREET = "Street";
	public static final String ADDRESS_LINE_1 = "Address Line 1";
	public static final String ADDRESS_LINE_2 = "Address Line 2";
	public static final String ADDRESS_LINE_3 = "Address Line 3";
	public static final String ZIP_CODE = "Zip Code";
	public static final String LAST_NAME_FATHERS_OR_LAST_NAME = "Last Name/Father's last Name";
	public static final String STATE = "State";
	public static final String COUNTRY = "County";
	public static final String COUNTRY_CODE = "Country Code";
	public static final String PHONE_NUMBER = "Phone Number";
	public static final String PHONE_NUMBER_ALTERNATE = "Phone Number Alternate";
	public static final String PLUS = "Plus #";
	public static final String CAN_POSTAL_CODE = "CANPostalCode";
	public static final String POSTAL_CODE = "Postcode";
	public static final String UK_POST_CODE = "UKPostcode";
	public static final String EU_POSTAL_CODE = "EUPostalCode";
    
	
	protected List<Field> fields = new ArrayList<Field>();
	protected UserProfile userProfile;
	protected boolean addressErrorAdded = false;

	protected BaseCustomerProfileValidator() {
	}

	protected BaseCustomerProfileValidator(UpdateCustomerProfileForm form,
			UserProfile userProfile, CustomerProfileRegexPattern customerProfileRegex) {
		fields.add(new AlphaFieldWithSpecialChars(form.getCustFirstName(),
				FIRST_NAME, customerProfileRegex.getFirstNameRegex()).setMaxLength(40));
		fields.add(new AlphaFieldWithSpecialChars(form.getCustLastName(),
				LAST_NAME,customerProfileRegex.getLastNameRegex()).setMaxLength(40));
		fields.add(new AlphaFieldWithSpecialChars(form.getMiddleName(), MIDDLE_NAME, customerProfileRegex.getMiddleNameRegex())
				.setMaxLength(20).isRequired(false));
		fields.add(new AlphaFieldWithSpecialChars(form.getCustSecondLastName(),
				SECOND_LAST_NAME, customerProfileRegex.getSecondLastNameRegex()).setMaxLength(30).isRequired(false));
		fields.add(new CityField(form.getCity(), TOWN_OR_CITY, customerProfileRegex.getCityRegex()).setMaxLength(40));
		fields.add(new BirthDateField(form.getBirthDateText(), BIRTH_DATE));
		fields.add(new AddressField(form.getAddressLine1(), ADDRESS_LINE_1, customerProfileRegex.getAddressLine1Regex())
				.setMaxLength(100));
		fields.add(new AddressField(form.getAddressLine2(), ADDRESS_LINE_2, customerProfileRegex.getAddressLine2Regex())
				.setMaxLength(100).isRequired(false));
		fields.add(new AddressField(form.getAddressLine3(), ADDRESS_LINE_3,customerProfileRegex.getAddressLine3Regex())
				.setMaxLength(100).isRequired(false));
	}

	public ActionErrors validate(ActionErrors errors) {
		for (Field field : fields) {
			errors = field.validate(errors);
		}
		return errors;
	}

	abstract class Field {
		String value;
		String friendlyName;
		boolean required;
		boolean valid;

		Field(String value, String friendlyName) {
			this.value = value;
			this.friendlyName = friendlyName;
			required = true;
			valid = true;
		}

		Field isRequired(boolean required) {
			this.required = required;
			return this;
		}

		boolean isValid() {
			return value != null && value.length() > 0;
		}

		public String toString() {
			return friendlyName;
		}

		ActionErrors validate(ActionErrors errors) {
			if (!(valid = isValid())) {
				if (required) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"errors.required", friendlyName));
				}
			}
			return errors;
		}
	}

	abstract class StringField extends Field {
		int minLength;
		int maxLength;
		String pattern;
		String keyStringMessage;

		StringField(String value, String friendlyName) {
			super(value, friendlyName);
			this.minLength = 1;
			this.maxLength = Integer.MAX_VALUE;
		}

		StringField setMinLength(int length) {
			this.minLength = length;
			return this;
		}

		StringField setMaxLength(int length) {
			this.maxLength = length;
			return this;
		}

		boolean isValidExpression() {
			Pattern regex = Pattern.compile(pattern);
			Matcher matcher = regex.matcher(value);
			if (friendlyName.equalsIgnoreCase("UKPostcode")) {
				return matcher.find();
			//CAN-55
			}else if (friendlyName.equalsIgnoreCase("CANPostalCode")) {
				return matcher.find();
			}else if (friendlyName.equalsIgnoreCase("EUPostalCode")) {
				return matcher.find();
			}

			boolean match = matcher.matches();
			return match;			
		}

		boolean hasCorrectLength() {
			return value.length() >= minLength && value.length() <= maxLength;
		}

		boolean isAddressField(String friendlyName) {
			return (friendlyName.equalsIgnoreCase("Address Line 1")
					|| friendlyName.equalsIgnoreCase("Address Line 2") || friendlyName
					.equalsIgnoreCase("Address Line 3"));
		}

		ActionErrors validate(ActionErrors errors) {
			errors = super.validate(errors);
	
			if (valid){ 
				boolean validExpression = isValidExpression();
				if(validExpression == false){
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						keyStringMessage, friendlyName));
				}
			}
			if (valid && !(valid = hasCorrectLength())) {
				if (isAddressField(friendlyName) && !addressErrorAdded) {
					errors.add(ActionErrors.GLOBAL_ERROR,
							new ActionError("error.address", friendlyName,
									minLength, maxLength));
					addressErrorAdded = true;
				} else if (!isAddressField(friendlyName)) {
					if (minLength != maxLength) {
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
								"errors.range.length", friendlyName, minLength,
								maxLength));
					} else {
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
								"errors.exact.length", friendlyName, minLength));
					}
				}
			}
			return errors;
		}
	}

	class AlphaNumericField extends StringField {
		AlphaNumericField(String value, String friendlyName) {
			super(value, friendlyName);
			pattern = "[^a-zA-Z0-9]";
			keyStringMessage = "errors.non.alphadigit";
		}

		AlphaNumericField setMinLength(int length) {
			return (AlphaNumericField) super.setMinLength(length);
		}

		AlphaNumericField setMaxLength(int length) {
			return (AlphaNumericField) super.setMaxLength(length);
		}

		AlphaNumericField isRequired(boolean required) {
			return (AlphaNumericField) super.isRequired(required);
		}
	}

	class AddressField extends StringField {
		AddressField(String value, String friendlyName, String patternFromDB) {
			super(value, friendlyName);
			pattern = patternFromDB;
			keyStringMessage = "errors.non.addressdigit";
		}

		AddressField setMinLength(int length) {
			return (AddressField) super.setMinLength(length);
		}

		AddressField setMaxLength(int length) {
			return (AddressField) super.setMaxLength(length);
		}

		AddressField isRequired(boolean required) {
			return (AddressField) super.isRequired(required);
		}
	}

	class AlphaField extends StringField {
		AlphaField(String value, String friendlyName) {
			super(value, friendlyName);
			//pattern = "[^a-zA-Z ]";
			pattern = "[a-zA-Z]";
			keyStringMessage = "errors.non.alpha";
		}

		AlphaField setMinLength(int length) {
			return (AlphaField) super.setMinLength(length);
		}

		AlphaField setMaxLength(int length) {
			return (AlphaField) super.setMaxLength(length);
		}

		AlphaField isRequired(boolean required) {
			return (AlphaField) super.isRequired(required);
		}
	}

	class AlphaFieldWithSpecialChars extends StringField {
		AlphaFieldWithSpecialChars(String value, String friendlyName, String patternFromDB) {
			super(value, friendlyName);		
			pattern = patternFromDB; 
			keyStringMessage = "errors.non.alphaSpecial";
		}

		AlphaFieldWithSpecialChars setMinLength(int length) {
			return (AlphaFieldWithSpecialChars) super.setMinLength(length);
		}

		AlphaFieldWithSpecialChars setMaxLength(int length) {
			return (AlphaFieldWithSpecialChars) super.setMaxLength(length);
		}

		AlphaFieldWithSpecialChars isRequired(boolean required) {
			return (AlphaFieldWithSpecialChars) super.isRequired(required);
		}
	}

	class StrictNumericField extends StringField {
		StrictNumericField(String value, String friendlyName) {
			super(value, friendlyName);
			//pattern = "[^0-9]";
			pattern = "[0-9]+$";
			keyStringMessage = "errors.non.digit";
		}

		StrictNumericField setMinLength(int length) {
			return (StrictNumericField) super.setMinLength(length);
		}

		StrictNumericField setMaxLength(int length) {
			return (StrictNumericField) super.setMaxLength(length);
		}

		StrictNumericField isRequired(boolean required) {
			return (StrictNumericField) super.isRequired(required);
		}
	}

	class DateField extends Field {
		Date date;
		String expectedFormat;
		DateFormat sf;
		Calendar now = Calendar.getInstance();

		DateField(String value, String friendlyName) {
			super(value, friendlyName);
			expectedFormat = "dd/MMM/yyyy";
			sf = new SimpleDateFormat(expectedFormat);
		}

		DateField setExpectedFormat(String format) {
			expectedFormat = format;
			return this;
		}

		boolean hasValidFormat() {
			try {
				date = sf.parse(value);
				return true;
			} catch (ParseException e) {
				return false;
			}
		}

		boolean isValidDate() {
			String formattedDate = sf.format(date);
			return formattedDate.equalsIgnoreCase(value)
					|| (now = Calendar.getInstance()).getTime().compareTo(date) > 0;
		}

		ActionErrors validate(ActionErrors errors) {
			errors = super.validate(errors);
			if (valid && !(valid = hasValidFormat())) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"error.date.format", friendlyName, expectedFormat));
			}
			if (valid && !(valid = isValidDate())) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"errors.date.invalid", value));
			}
			return errors;
		}
	}

	class BirthDateField extends DateField {
		int minimumValidAge;
		int maximumValidAge;

		BirthDateField(String value, String name) {
			super(value, name);
			minimumValidAge = 18;
			maximumValidAge = 108;
		}

		BirthDateField setMinimumAge(int age) {
			minimumValidAge = age;
			return this;
		}

		BirthDateField setMaximumAge(int age) {
			maximumValidAge = age;
			return this;
		}

		BirthDateField setExpectedFormat(String format) {
			return (BirthDateField) super.setExpectedFormat(format);
		}

		boolean hasValidAge() {
			int age = getAge();
			return age >= minimumValidAge && age <= maximumValidAge;
		}

		protected int getAge() {
			Calendar dob = Calendar.getInstance();
			dob.setTime(date);
			int year1 = now.get(Calendar.YEAR);
			int year2 = dob.get(Calendar.YEAR);
			int age = year1 - year2;
			int month1 = now.get(Calendar.MONTH);
			int month2 = dob.get(Calendar.MONTH);
			if (month2 > month1) {
				age--;
			} else if (month1 == month2) {
				int day1 = now.get(Calendar.DAY_OF_MONTH);
				int day2 = dob.get(Calendar.DAY_OF_MONTH);
				if (day2 > day1) {
					age--;
				}
			}
			return age;
		}

		ActionErrors validate(ActionErrors errors) {
			errors = super.validate(errors);
			if (valid && !(valid = hasValidFormat() && hasValidAge())) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"errors.date.age", minimumValidAge, maximumValidAge));
			}
			return errors;
		}
	}

	class UntaintedPhoneNumber extends StrictNumericField {
		String uid;
		String countryDialingCode;

		UntaintedPhoneNumber(String value, String friendlyName) {
			super(value, friendlyName);
			uid = null;
			countryDialingCode = null;
		}

		UntaintedPhoneNumber setMinLength(int length) {
			return (UntaintedPhoneNumber) super.setMinLength(length);
		}

		UntaintedPhoneNumber setMaxLength(int length) {
			return (UntaintedPhoneNumber) super.setMaxLength(length);
		}

		UntaintedPhoneNumber isRequired(boolean required) {
			return (UntaintedPhoneNumber) super.isRequired(required);
		}

		UntaintedPhoneNumber setUID(String uid) {
			this.uid = uid;
			return this;
		}

		UntaintedPhoneNumber setCountryDialingCode(String countryDialingCode) {
			this.countryDialingCode = getCountryCode(countryDialingCode);
			return this;
		}

		private String getCountryCode(String countryDialingCode) {
			if (StringUtils.isNotEmpty(countryDialingCode)) {
				if (countryDialingCode.contains("-")) {
					String[] countryCodeArr = countryDialingCode.split("-");
					return countryCodeArr[0];
				}
			}
			return null;

		}

		boolean isNotTainted() {
			try {
				boolean tainted = checkTainted();
				return !tainted;
			} catch (DataSourceException e) {
				// Something went wrong, assume is not tainted
				return true;
			}
		}

		boolean checkTainted() throws DataSourceException {
			ServiceFactory sf = ServiceFactory.getInstance();
			FraudService fs = sf.getFraudService();
			return fs.checkBlockedPhone(uid, value);
		}

		ActionErrors validate(ActionErrors errors) {
			errors = super.validate(errors);
			if (valid && StringUtils.isNotEmpty(countryDialingCode)) {
				PhoneNumberUtils phUtil = new PhoneNumberUtils();
				if (!(phUtil.validPhoneNumber(value, countryDialingCode))) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"error.update.consumer.phone.failed", value));
				} else {
					if (!(isNotTainted())) {
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
								"error.phone.is.blocked", value));
					}
				}
			}

			return errors;
		}
	}
	
	class GenericPostalCode extends StringField {
		GenericPostalCode(String value, String friendlyName, String patternFromDB) {
			super(value, friendlyName);
			pattern = patternFromDB;
			keyStringMessage = "errors.non.digit";
		}

		GenericPostalCode setMinLength(int length) {
			return (GenericPostalCode) super.setMinLength(length);
		}

		GenericPostalCode setMaxLength(int length) {
			return (GenericPostalCode) super.setMaxLength(length);
		}

		GenericPostalCode isRequired(boolean required) {
			return (GenericPostalCode) super.isRequired(required);
		}
	}


	class UKPostalCode extends StringField {
		UKPostalCode(String value, String friendlyName, String patternFromDB) {
			super(value, friendlyName);
			pattern = patternFromDB;
			keyStringMessage = "errors.invalid.uk.postcode";
		}

		UKPostalCode setMinLength(int length) {
			return (UKPostalCode) super.setMinLength(length);
		}

		UKPostalCode setMaxLength(int length) {
			return (UKPostalCode) super.setMaxLength(length);
		}

		UKPostalCode isRequired(boolean required) {
			return (UKPostalCode) super.isRequired(required);
		}

	}

	/**
	 * This method is used to validate the city field
	 */
	class CityField extends StringField {

		CityField(String value, String friendlyName, String patternFromDB) {
			super(value, friendlyName);
			pattern = patternFromDB;
			keyStringMessage = "errors.invalid.city";
		}

		CityField setMinLength(int length) {
			return (CityField) super.setMinLength(length);
		}

		CityField setMaxLength(int length) {
			return (CityField) super.setMaxLength(length);
		}

		CityField isRequired(boolean required) {
			return (CityField) super.isRequired(required);
		}
	}
	//CAN-55: Added regex for Canada postal code validation
	class CANPostalCode extends StringField {
		CANPostalCode(String value, String friendlyName, String patternFromDB) {
			super(value, friendlyName);
			pattern = patternFromDB;
			keyStringMessage = "errors.invalid.can.postcode";
		}

		CANPostalCode setMinLength(int length) {
			return (CANPostalCode) super.setMinLength(length);
		}

		CANPostalCode setMaxLength(int length) {
			return (CANPostalCode) super.setMaxLength(length);
		}

		CANPostalCode isRequired(boolean required) {
			return (CANPostalCode) super.isRequired(required);
		}
	}
	
	//BUCKET-118: Added regex for EU Sites-Phase1 postal code validation
	class EUPostalCode extends StringField {
		EUPostalCode(String value, String friendlyName, EUSites countryCode, String patternFromDB) {
			super(value, friendlyName);
			switch(countryCode)
			{
			case MGOAUT:
			case MGOBEL:
			case MGONOR:
			case MGOBGR:
			case MGOHUN:
			case MGOLUX:
			case MGOSVN:
			case MGOCHE:
				super.setMinLength(4);
				super.setMaxLength(4);
				pattern = patternFromDB;
				keyStringMessage = "errors.non.digit";
				break;
			case MGOHRV:
			case MGOEST:
			case MGOFIN:
			case MGOITA:
				super.setMinLength(5);
				super.setMaxLength(5);
				pattern = patternFromDB;
				keyStringMessage = "errors.non.digit";
				break;
			case MGODNK:
				super.setMinLength(3);
				super.setMaxLength(4);
				pattern = patternFromDB;
				keyStringMessage = "errors.non.digit";
				break;
			case MGOGRC:
			case MGOSWE:
			case MGOCZE:
			case MGOPOL:
			case MGOSVK:
				super.setMinLength(5);
				super.setMaxLength(6);
				pattern = patternFromDB;	
				keyStringMessage = "errors.invalid.eu.postcode";
				break;
			case MGOIRL:
			case MGOMLT:
				super.setMinLength(7);
				super.setMaxLength(8);
				pattern = patternFromDB;
				keyStringMessage = "errors.invalid.eu.postcode";
				break;
			case MGOISL:
				super.setMinLength(3);
				super.setMaxLength(3);
				pattern = patternFromDB;
				keyStringMessage = "errors.non.digit";
				break;
			case MGOLVA:
			case MGONLD:
				super.setMinLength(6);
				super.setMaxLength(7);
				pattern = patternFromDB;
				keyStringMessage = "errors.invalid.eu.postcode";
				break;
			case MGOLTU:
			case MGOPRT:
				super.setMinLength(7);
				super.setMaxLength(8);
				pattern = patternFromDB;
				keyStringMessage = "errors.invalid.eu.postcode";
				break;
			case MGOROU:
				super.setMinLength(6);
				super.setMaxLength(6);
				pattern = patternFromDB;
				keyStringMessage = "errors.invalid.eu.postcode";
				break;
				
			default :
				System.out.println("No match");
			}
			
		}

		EUPostalCode isRequired(boolean required) {
			return (EUPostalCode) super.isRequired(required);
		}
	}

}
	