/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.consumer;

import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.owasp.esapi.ESAPI;



import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.model.Notification;
import emgadm.property.EMTAdmContainerProperties;
import emgadm.services.NotificationAccessImpl;
import emgadm.services.osl.OSLException;
import emgadm.services.osl.OSLProxyImpl;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.DuplicateActiveProfileException;
import emgshared.exceptions.SameStatusException;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileComment;
import emgshared.model.ConsumerStatus;
import emgshared.model.UserActivityType;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;

/**
 * @author A131
 *
 */
public class UpdateCustomerStatusAction extends EMoneyGramAdmBaseAction
{
	private static final String FORWARD_SUCCESS = "success";
	private static final String FORWARD_FAILURE = "failure";

	private static final ConsumerProfileService profileService =
		ServiceFactory.getInstance().getConsumerProfileService();
	private static final Logger log = EMGSharedLogger.getLogger(UpdateCustomerStatusAction.class);
	private static final NotificationAccessImpl notificationImpl = new NotificationAccessImpl();
	
	String smsText = null;
	String msgTemplate = null;
	private static final String NON_ACTIVE_CODE = "NAT";
	private static final String zanonActiveSMSText = "za.nonActive.SMS";
	protected static final String resourceFile = "emgadm.resources.ApplicationResources";
	ResourceBundle msgBundle = ResourceBundle.getBundle(resourceFile); 
	String generalphonenumber = EMTAdmContainerProperties.getGeneralPhoneNumber();
	
	//MBO-6411
	private static final String zaActiveSMSText = "za.active.SMS";
	

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws NumberFormatException, DataSourceException
	{
		ActionForward forward = null;
		ActionErrors errors = new ActionErrors();
		UpdateCustomerStatusForm inputForm = (UpdateCustomerStatusForm) form;
		ConsumerProfile custprofile=profileService.getConsumerProfile(Integer.valueOf(inputForm.getCustId()), "MGO", null);
		//MBO-7491
		boolean isSMSEnabled = EMTAdmContainerProperties.isSendSMS();
		/*Added for MBO-6412*/
		if((notificationImpl.SITEIDENTIFIER_ZAF).equals(custprofile.getPartnerSiteId())
				&& (!(inputForm.getCustStatus().equals(ConsumerStatus.NON_ACTIVE_VERID_FAIL.toString()))))
		{
		if((((inputForm.getCustStatus().substring(0,3)).equals(NON_ACTIVE_CODE))
				|| inputForm.getCustStatus().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_7.toString())
				|| inputForm.getCustStatus().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_10.toString())
				|| inputForm.getCustStatus().equals(ConsumerStatus.ACTIVE_NEW_ACCT_PROTECTION_FLR.toString()))
			&& isSMSEnabled)
		{
				
				try { // MBO-6789
					Notification smsNotification = notificationImpl
							.createNotificationRequest( null, NotificationAccessImpl.SMS_NOTIFY_MSG_TYPE_PROFILE_DEC, custprofile);
					OSLProxyImpl.sendNotification(smsNotification);
					log.info(" Declined SMS sent successfully !  Cust Id :: " + Integer.valueOf(inputForm.getCustId()));
				} catch (OSLException e) {
					log.error("Error while sending Profile Declined SMS", e);
				}	// Ended MBO-6789 				
		}
		} /*MBO-6412 ended*/
						
		if (inputForm.getCustStatus().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_0.toString()) 
			|| inputForm.getCustStatus().equals(ConsumerStatus.NON_ACTIVE_VERID_FAIL.toString())
			|| inputForm.getCustStatus().equals(ConsumerStatus.PENDING_INITIAL.toString())
			|| inputForm.getCustStatus().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_6.toString()))
		{
			Map m = profileService.getConsumerStatusDescriptions();
			ConsumerStatus status =	ConsumerStatus.getInstanceByCombinedCode(inputForm.getCustStatus());
			errors.add("custStatus",new ActionMessage("error.status.not.allowed", m.get(status).toString()));
			forward = mapping.findForward(FORWARD_FAILURE);
		} else
		{
			ConsumerStatus newStatus = ConsumerStatus.getInstanceByCombinedCode(inputForm.getCustStatus());
			ConsumerStatus currStatus =	ConsumerStatus.getInstanceByCombinedCode(inputForm.getCurrentCustStatus());
			ConsumerProfileService profileService =	ServiceFactory.getInstance().getConsumerProfileService();
			String userId = getUserProfile(request).getUID();
			int custId = Integer.parseInt(inputForm.getCustId());
			try
			{
				if (inputForm.getAllowOverride())
				{
					profileService.updateStatus(custId, newStatus, userId,true);
				}
				else
					profileService.updateStatus(custId, newStatus, userId,false);
				//Look up status description
				Map Descriptions = profileService.getConsumerStatusDescriptions();
				String StatusDesc = (String) Descriptions.get(newStatus);
				String origStatusDesc = (String) Descriptions.get(currStatus);
				
				//Add status change comment
				ConsumerProfileComment comment = new ConsumerProfileComment();
				comment.setCustId(custId);
				comment.setReasonCode("OTH");
				comment.setText("Status Changed  from '" + origStatusDesc 
						+ "' to '" + StatusDesc + "'");
				if (inputForm.getAllowOverride())
					comment.setText(comment.getText() + " - Duplicate Profile Override applied.");
				profileService.addConsumerProfileComment(comment, userId);
	            try {
	                StringBuilder detailText = new StringBuilder(512);
	                detailText.append("<FROM_STATUS>" + inputForm.getCurrentCustStatus() + "</FROM_STATUS>");
	                detailText.append("<TO_STATUS>" + newStatus + "</TO_STATUS>");
	                super.insertActivityLog(this.getClass(),request,detailText.toString(),UserActivityType.EVENT_TYPE_CONSUMERPROFILESTATUSCHG,String.valueOf(custId));    
				} catch (Exception ignore) { }
				request.setAttribute("duplicateOverride", "N");
				
				
				/*Added for MBO-6411*/
				if(custprofile.getPartnerSiteId().equals(NotificationAccessImpl.SITEIDENTIFIER_ZAF))
				{
					log.debug("Inside ZAF check - going to send SMS for Approved profiles");
				if((inputForm.getCustStatus().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_1.toString())
						|| inputForm.getCustStatus().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_2.toString())
						|| inputForm.getCustStatus().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_3.toString())
						|| inputForm.getCustStatus().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_4.toString())
						|| inputForm.getCustStatus().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_5.toString())
						|| inputForm.getCustStatus().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_6.toString())
						|| inputForm.getCustStatus().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_8.toString())
						|| inputForm.getCustStatus().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_9.toString()))
					&& isSMSEnabled)
				{
					log.debug("Status is one of the approved status " + 
							ESAPI.encoder().encodeForHTMLAttribute(inputForm.getCustStatus().toString()) + " for cust " + custId);
						try { //MBO-6789
							Notification smsNotification = notificationImpl
									.createNotificationRequest( null, NotificationAccessImpl.SMS_NOTIFY_MSG_TYPE_PROFILE_APP, custprofile);
							OSLProxyImpl.sendNotification(smsNotification);
							log.info(" Approved SMS sent successfully !  Cust Id :: "+ custId);
						} catch (OSLException e) {
							log.error("Error while sending Profile Approved SMS", e);
						}	// Ended MBO-6789			
				}
				} /*MBO-6411 ended*/
				
				//Add status change message next to "Update Status" button
				errors.add("custStatus", new ActionMessage("msg.status.changed"));
				forward = mapping.findForward(FORWARD_SUCCESS);
			} catch (SameStatusException e)
			{
				errors.add("headerMessage",	new ActionMessage("error.status.no.change"));
				forward = mapping.findForward(FORWARD_FAILURE);
			} catch (DuplicateActiveProfileException e)
			{
				request.setAttribute("duplicateOverride", "Y");
				errors.add("custStatus", new ActionMessage("errors.duplicate.profile"));
				forward = mapping.findForward(FORWARD_FAILURE);
			}
		}

		if (errors.size() > 0)
		{
			saveErrors(request, errors);
		}

		return forward;
	}
}
