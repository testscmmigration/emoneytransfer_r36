package emgadm.transqueue;

import java.sql.SQLException;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;


import com.moneygram.common_v1.Header;
import com.moneygram.common_v1.ProcessingInstruction;
import com.moneygram.mgo.service.config_v3_4.GetSourceSiteInfoResponse;
import com.moneygram.service.moneygramscoringservice_v3.Action;

import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.model.Notification;
import emgadm.model.UserProfile;
import emgadm.services.NotificationAccess;
import emgadm.services.NotificationAccessImpl;
import emgadm.services.ServiceFactory;
import emgadm.services.TransactionService;
import emgadm.services.mgoscoringservice.MGOScoringServiceProxyImpl;
import emgadm.services.osl.OSLException;
import emgadm.services.osl.OSLProxyImpl;
import emgadm.util.MerchantIdHelper;
import emgshared.cache.locator.EMTSharedSourceSiteCacheService;
import emgshared.cache.locator.EMTSharedSourceSiteCacheServiceImpl;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerProfile;
import emgshared.model.Transaction;
import emgshared.model.TransactionStatus;
import emgshared.model.TransactionType;
import emgshared.services.ConsumerProfileService;
import emgshared.util.Constants;

/**
 * @author A131
 * 
 */
public class DenyAction extends ConfirmSubReasonBaseAction {

//	private static final String resourceFile = "emgadm.resources.ApplicationResources";
//	private static final MailService mailService = ServiceFactory.getInstance()
//			.getMailService();
	private static final String REASON_CODE_HUNDRED = "100";
	protected static final ConsumerProfileService profileService = emgshared.services.ServiceFactory
	.getInstance().getConsumerProfileService();
	private static Logger LOGGER = EMGSharedLogger.getLogger(
			ConfirmSubReasonBaseAction.class);

	protected ActionForward processConfirmation(ActionMapping mapping,
			ConfirmSubReasonForm form, HttpServletRequest request,
			HttpServletResponse response) throws NumberFormatException,
			TransactionAlreadyInProcessException,
			TransactionOwnershipException, DataSourceException, SQLException{
		ActionErrors errors = new ActionErrors();
		UserProfile userProfile = getUserProfile(request);
		TransactionManager tm = getTransactionManager(request);
		TransactionService tranService = ServiceFactory.getInstance()
				.getTransactionService();
		EMTSharedSourceSiteCacheService cacheService = new EMTSharedSourceSiteCacheServiceImpl();
		GetSourceSiteInfoResponse sourceSiteResponse = new GetSourceSiteInfoResponse();
		int tranId = Integer.parseInt(form.getTranId());
		Transaction tran = tm.getTransaction(tranId);
		// List tas;
	/*	tranService.confirmStatusChangeWithSubReason(Integer.parseInt(form
				.getTranId()), userProfile.getUID(), String
				.valueOf(TransactionStatus.DENIED_NOT_FUNDED.getStatusCode()),
				String.valueOf(TransactionStatus.DENIED_NOT_FUNDED
						.getSubStatusCode()), form.getSubReasonCode());
		
		if (tran.getTranAppVersionNumber() != Constants.VERSION_NBR_UK_DE
				&& tran.getTranAppVersionNumber() != Constants.VERSION_NBR_US) {
			// MBO-450 changes starts
			updateCyberSourceReject(form.getSubReasonDescription(),
					userProfile, tm, tranId, tran);
			// MBO-450 changes ends
		}

		tm.setTransactionComment(tranId,
				EMoneyGramAdmApplicationConstants.DENIAL_CODE, "Subreason: "
						+ form.getSubReasonDescription(), userProfile.getUID());
		
		request.getSession().setAttribute(
				EMoneyGramAdmApplicationConstants.DENIAL_CODE,
				form.getSubReasonDescription());

		try {
			this.sendDenyEmail(tran);
		} catch (Exception e) {
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
					"error.send.mail.failed"));
			saveErrors(request, errors);
		}*/
		//1939 Collect Cancel Reason in EMTAdmin
		boolean cancelTransaction = false;
		final String CRN = "CRN";
		final String CRQ = "CRQ";
		//MBO-4770 To get countryCode from cache service
		try {
			sourceSiteResponse = cacheService.sourceSiteDetails(tran
					.getPartnerSiteId());
		} catch (Exception exception) {
			LOGGER.error(
					"exception when getting source site details from cache....",
					exception);
		}
		String countryCode = (null != sourceSiteResponse.getCountryInfo() ? sourceSiteResponse
				.getCountryInfo().getIso3CharCountryCode() : null);
		//Cancelling only if the trans is meetin following conditions
		if( Constants.COUNTRY_CODE_USA.equals(countryCode) 
				&& ("FFS".equals(tran.getTranStatCode()) 
					|| "PEN".equals(tran.getTranStatCode()))){
			cancelTransaction = true;
		}
		if((CRN.equalsIgnoreCase(form.getSubReasonCode()) 
				|| CRQ.equalsIgnoreCase(form.getSubReasonCode())) && cancelTransaction ) {
			//treat CRQ/CRN 'deny' actions as cancels instead of denials
			tranService.confirmStatusChangeWithSubReason(Integer.parseInt(form 
				.getTranId()), userProfile.getUID(), 
				String.valueOf(TransactionStatus.CANCELED_NOT_FUNDED.getStatusCode()), 
				String.valueOf(TransactionStatus.CANCELED_NOT_FUNDED.getSubStatusCode()),
				"OTH"); 
			tm.setTransactionComment(tranId, 
				EMoneyGramAdmApplicationConstants.COMMENT_CODE_OTH, "Subreason: Cancel: " 
				+ form.getSubReasonDescription(), userProfile.getUID()); 
			//MBO-7767
			NotificationAccessImpl na = new NotificationAccessImpl();
			try { 
				ConsumerProfile profile = profileService.getConsumerProfile(
						Integer.valueOf(tran.getCustId()),
						tran.getSndCustLogonId(), "");
				Notification emailNotification = na
						.createNotificationRequest( tran, NotificationAccessImpl.EMAIL_NOTIFY_MSG_TYPE_TRAN_CANCEL, profile);
				OSLProxyImpl.sendNotification(emailNotification);
				LOGGER.info("Txn Cancelled email sent successfully when the transaction is denied!  Tran Id :: "+ tran.getEmgTranId() + "Cust Id" + profile.getId());
			} catch (OSLException e) {
				LOGGER.error("Error while sending email", e);
			}
			//MBO-7767 Ends
		}else{ 
			tranService.confirmStatusChangeWithSubReason(Integer.parseInt(form
					.getTranId()), userProfile.getUID(), String
					.valueOf(TransactionStatus.DENIED_NOT_FUNDED.getStatusCode()),
					String.valueOf(TransactionStatus.DENIED_NOT_FUNDED
							.getSubStatusCode()), form.getSubReasonCode());
			
			tm.setTransactionComment(tranId,
					EMoneyGramAdmApplicationConstants.COMMENT_CODE_OTH, "Subreason: "
							+ form.getSubReasonDescription(), userProfile.getUID());
		
		/*request.getSession().setAttribute(
				EMoneyGramAdmApplicationConstants.DENIAL_CODE,
				form.getSubReasonDescription());*/

			try {
				this.sendDenyEmail(tran);
			} catch (Exception e) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"error.send.mail.failed"));
				saveErrors(request, errors);
			}
		}
		//1939 Collect Cancel Reason in EMTAdmin
		return mapping
				.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	protected void getCommandDescription(HttpServletRequest request) {
		Locale locale = Locale.getDefault();
		MessageResources msgRes = MessageResources
				.getMessageResources("emgadm.resources.TransactionConfirmationDescription");
		MessageFormat formatter = new MessageFormat(
				msgRes.getMessage("confirmation.deny"));
		formatter.setLocale(locale);
		String msgText = formatter.toPattern();
		request.setAttribute("msgText", msgText);

	}

	private void sendDenyEmail(Transaction tran) throws Exception {
		ConsumerProfile profile = emgshared.services.ServiceFactory
				.getInstance()
				.getConsumerProfileService()
				.getConsumerProfile(Integer.valueOf(tran.getCustId()),
						tran.getSndCustLogonId(), "");
		if (profile != null) {
			String preferedlanguage = profile.getPreferedLanguage();
			ConsumerEmail consumerEmail = null;
			// Loop sends out email to each address.
			NotificationAccess na = new NotificationAccessImpl();
			for (Iterator iter = profile.getEmails().iterator(); iter.hasNext();) {
				consumerEmail = (ConsumerEmail) iter.next();
				na.notifyDeniedTransaction(tran, consumerEmail,
						preferedlanguage);
			}
		}
	}

}
