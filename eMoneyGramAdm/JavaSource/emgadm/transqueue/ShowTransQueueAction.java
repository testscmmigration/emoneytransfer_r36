package emgadm.transqueue;

import java.sql.Date;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.log4j.Logger;



import java.util.HashMap;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.exceptions.TooManyResultException;
import emgadm.model.TranQueueView;
import emgadm.model.TranType;
import emgadm.model.UserProfile;
import emgadm.util.AuditLogUtils;
import emgadm.util.StringHelper;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.model.Transaction;
import emgshared.model.TransactionSearchRequest;
import emgshared.model.TransactionStatus;
import emgshared.model.TransactionType;
import emgshared.property.EMTSharedDynProperties;
import emgshared.util.Constants;
import emgshared.util.DateFormatter;

public class ShowTransQueueAction extends EMoneyGramAdmBaseAction {
	private static EMTSharedDynProperties dynProps = new EMTSharedDynProperties();

	public final class UserId {
		private String userId;

		private UserId(String s) {
			userId = s;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String string) {
			userId = string;
		}

	}

	static String TRAN_ENDDATE = "tranEndDateText";
	static String TRAN_BEGDATE = "tranBeginDateText";
	static String TRAN_STATUS = "tranStatus";
	static String TRAN_FNDSTATUS = "tranFundStatus";
	static String TRAN_TYPE = "tranType";
	static String TRAN_OWNBY = "tranOwnedBy";
	static String TRAN_SRTBY = "tranSortBy";
	static String TRAN_SRTSEQ = "tranSortSeq";
	static String TRAN_USESCR = "useScore";
	static String TRAN_LOWSCR = "lowScore";
	static String TRAN_HIGSCR = "highScore";
	private static Logger logger = EMGSharedLogger.getLogger(
			ShowTransQueueAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm f,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		ShowTransQueueForm form = (ShowTransQueueForm) f;
		UserProfile up = getUserProfile(request);
		Collection transactions = null;
		ServletContext context = request.getSession().getServletContext();

		String requestPartnerSiteId = (String) request
				.getAttribute("partnerSiteId");
		String formPartnerSiteId = form.getPartnerSiteId();
		String partnerSiteId = StringHelper.isNullOrEmpty(formPartnerSiteId) ? StringHelper
				.isNullOrEmpty(formPartnerSiteId) ? "ALL"
				: requestPartnerSiteId : formPartnerSiteId;
		form.setPartnerSiteId(partnerSiteId);
		request.setAttribute("partnerSiteId", partnerSiteId);
		String contextDocStatus = (String) context.getAttribute("docStatus");
		String formDocStatus = form.getDocStatus();
		String docStatus = StringHelper.isNullOrEmpty(formDocStatus) ? StringHelper
				.isNullOrEmpty(contextDocStatus) ? "" : contextDocStatus
				: formDocStatus;

		if (!form
				.getPartnerSiteId()
				.toString()
				.equals(EMoneyGramAdmApplicationConstants.MGODE_PARTNER_SITE_ID)
				|| !((String) request.getAttribute("partnerSiteId"))
						.equals(EMoneyGramAdmApplicationConstants.MGODE_PARTNER_SITE_ID)) {
			context.removeAttribute("docStatus");
			form.setDocStatus("");
		} else {
			form.setDocStatus(docStatus);
			context.setAttribute("docStatus", docStatus);
		}

		String path = mapping.getPath();
		if (request.getAttribute("path") == null) {
			request.setAttribute("path", path);
		}
		if (form.getPartnerSiteId().equals(
				EMoneyGramAdmApplicationConstants.MGO_PARTNER_SITE_ID)
				&& !path.equals("/"
						+ EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_TRANS_QUEUE_MGO)) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_TRANS_QUEUE_MGO);
		} else if (form.getPartnerSiteId().equals(
				EMoneyGramAdmApplicationConstants.MGOUK_PARTNER_SITE_ID)
				&& !path.equals("/"
						+ EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_TRANS_QUEUE_MGOUK)) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_TRANS_QUEUE_MGOUK);
		} else if (form.getPartnerSiteId().equals(
				EMoneyGramAdmApplicationConstants.MGODE_PARTNER_SITE_ID)
				&& !path.equals("/"
						+ EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_TRANS_QUEUE_MGODE)) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_TRANS_QUEUE_MGODE);
		} else if (form.getPartnerSiteId().equals(
				EMoneyGramAdmApplicationConstants.WAP_PARTNER_SITE_ID)
				&& !path.equals("/"
						+ EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_TRANS_QUEUE_WAP)) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_TRANS_QUEUE_WAP);
		} else if ((form.getPartnerSiteId().equals(
				EMoneyGramAdmApplicationConstants.ALL_PARTNER_SITE_ID) && !path
				.equals("/"
						+ EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_TRANS_QUEUE_ALL))) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_TRANS_QUEUE_ALL);
		}

		boolean isQueueTran = setFilters(request, form);
		request.removeAttribute("path");

		if (up.hasPermission("process")) {
			request.getSession().setAttribute("canProcess", "Y");
		}

		if (up.hasPermission("batchESProcess")) {
			request.getSession().setAttribute("batchProcess", "Y");
		}

		if (context.getAttribute("programNames") == null) {
			context.setAttribute("programNames",
					up.getProgramsWithPermissions());
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitESSend())) {
			request.setAttribute("processType", form.getSubmitESSend());
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_BATCH_ES_PROCESS);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitESDeny())) {
			request.setAttribute("processType", form.getSubmitESDeny());
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_BATCH_ES_PROCESS);
		}

		if (StringHelper.isNullOrEmpty(form.getSubmitRefresh()) && !isQueueTran
				&& request.getSession().getAttribute("transMap") != null) {
			if (request.getSession().getAttribute("transMap") instanceof Map) {
				Map<String, String> transferMap = (Map<String, String>) request
						.getSession().getAttribute("transMap");
				if (transferMap != null) {
					Iterator<String> keySetIterator = transferMap.keySet()
							.iterator();
					while (keySetIterator.hasNext()) {
						String key = keySetIterator.next();
						if (key.equals(TRAN_BEGDATE))
							form.setBeginDateText(transferMap.get(TRAN_BEGDATE));
						else if (key.equals(TRAN_ENDDATE))
							form.setEndDateText(transferMap.get(TRAN_ENDDATE));
						else if (key.equals(TRAN_STATUS))
							form.setStatus(transferMap.get(TRAN_STATUS));
						else if (key.equals(TRAN_FNDSTATUS))
							form.setFundStatus(transferMap.get(TRAN_FNDSTATUS));
						else if (key.equals(TRAN_TYPE))
							form.setType(transferMap.get(TRAN_TYPE));
						else if (key.equals(TRAN_OWNBY))
							form.setOwnedBy(transferMap.get(TRAN_OWNBY));
						else if (key.equals(TRAN_SRTBY))
							form.setSortBy(transferMap.get(TRAN_SRTBY));
						else if (key.equals(TRAN_SRTSEQ))
							form.setSortSeq(transferMap.get(TRAN_SRTSEQ));
						else if (key.equals(TRAN_USESCR))
							form.setUseScore(transferMap.get(TRAN_USESCR));
						else if (key.equals(TRAN_LOWSCR))
							form.setLowScore(transferMap.get(TRAN_LOWSCR));
						else if (key.equals(TRAN_HIGSCR))
							form.setHighScore(transferMap.get(TRAN_HIGSCR));
					}
				}
			}
			form.setSubmitRefresh("Refresh");
		}
		if (partnerSiteId
				.equals(EMoneyGramAdmApplicationConstants.MGODE_PARTNER_SITE_ID)) {
			form.setType("MGSEND");
		}
		Map<String, String> transferMap = new HashMap();
		transferMap.put(TRAN_BEGDATE, form.getBeginDateText());
		transferMap.put(TRAN_ENDDATE, form.getEndDateText());
		transferMap.put(TRAN_STATUS, form.getStatus());
		transferMap.put(TRAN_FNDSTATUS, form.getFundStatus());
		transferMap.put(TRAN_TYPE, form.getType());
		transferMap.put(TRAN_OWNBY, form.getOwnedBy());
		transferMap.put(TRAN_SRTBY, form.getSortBy());
		transferMap.put(TRAN_SRTSEQ, form.getSortSeq());
		transferMap.put(TRAN_USESCR, form.getUseScore());
		transferMap.put(TRAN_LOWSCR, form.getLowScore());
		transferMap.put(TRAN_HIGSCR, form.getHighScore());

		request.getSession().setAttribute("transMap", transferMap);

		if (!StringHelper.isNullOrEmpty(form.getSortBy())) {
			TranQueueView.sortBy = form.getSortBy();
		}

		if (form.getSortSeq().equalsIgnoreCase("A")) {
			TranQueueView.sortSeq = 1;
		} else {
			TranQueueView.sortSeq = -1;
		}

		List<TranQueueView> transQueueList = new ArrayList<TranQueueView>();
		TransactionManager tm = getTransactionManager(request);

		DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
		long now = System.currentTimeMillis();
		if (StringHelper.isNullOrEmpty(form.getBeginDateText())) {
			form.setBeginDateText(df.format(new Date(now - 24L * 60L * 60L
					* 1000L)));
			form.setEndDateText(df.format(new Date(now + 16L * 60L * 60L
					* 1000L)));
		}
		TransactionSearchRequest tsr = new TransactionSearchRequest();

		tsr.setBeginDate(form.getBeginDateText());
		tsr.setEndDate(form.getEndDateText());
		tsr.setStatusDate(DateFormatter.getAchStatusDay(
				new Date(System.currentTimeMillis()),
				(Map) request.getSession().getServletContext()
						.getAttribute("holidayMap"),
				dynProps.getEsSendWaitDays()));

		if (!"All".equalsIgnoreCase(form.getType())) {
			tsr.setTranType(form.getType());
		}
		if (!"All".equalsIgnoreCase(form.getStatus())) {
			tsr.setTranStatCode(form.getStatus());
		}
		if (!"All".equalsIgnoreCase(form.getFundStatus())) {
			tsr.setTranSubStatCode(form.getFundStatus());
		}

		tsr.setUseScore(form.getUseScore());
		if ("Y".equalsIgnoreCase(form.getUseScore())) {
			tsr.setLowScore(Integer.parseInt(form.getLowScore()));
			tsr.setHighScore(Integer.parseInt(form.getHighScore()));
		}

		tsr.setPartnerSiteId(form.getPartnerSiteId());
		tsr.setDocStatus(form.getDocStatus());
		// added by Ankit Bhatt for MBO-128
		if (null != form.getProfileType()) {
			System.out
					.println("In Dashboard action - showTransQueueAction - form.getProfileType() "
							+ form.getProfileType().toUpperCase());
			tsr.setProfileType(form.getProfileType().toUpperCase());
		}
		// ended
		boolean tooManyResultError = false;

		try {
			if (docStatus != null && !StringHelper.isNullOrEmpty(docStatus)) {
				transactions = tm.getTransactionQueueDocStatus(tsr);
			} else {
				transactions = tm.getTransactionQueue(tsr);
			}
		} catch (TooManyResultException e) {

			form.setRecordCount(String.valueOf(transQueueList.size()));
			request.getSession().setAttribute("queueList", transQueueList);

			errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("error.too.many.results", dynProps
							.getMaxDownloadTransactions() + ""));
			saveErrors(request, errors);
			tooManyResultError = true;
		}

		if (!tooManyResultError) {

			Iterator iter = transactions.iterator();

			while (iter.hasNext()) {
				Transaction transaction = (Transaction) iter.next();

				if (form.getPartnerSiteId() != null
						&& !form.getPartnerSiteId().equals(
								transaction.getPartnerSiteId())
						&& !form.getPartnerSiteId().equals("ALL")) {
					continue;
				}

				// Build a list of users
				String tmpUserId = transaction.getCsrPrcsUserid();
				if (StringHelper.isNullOrEmpty(tmpUserId)) {
					tmpUserId = "None";
				}

				if ((form.getSubmitRefresh() != null)
						&& (form.getOwnedBy().equalsIgnoreCase("All") || form
								.getOwnedBy().equalsIgnoreCase(tmpUserId))) {
					addTrans(transaction, transQueueList, up);
				} else if (form.getSubmitRefresh() == null) {
					// added by Ankit Bhatt for MBO-128
					System.out.println("Before adding Transaction in List");
					System.out.println("Transaction profile type "
							+ transaction.getProfileType());
					// ended
					addTrans(transaction, transQueueList, up);
				}
			}

			if (transQueueList.size() == 0) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"error.no.match.found"));
				saveErrors(request, errors);
			}

			if (transQueueList.size() > 1) {
				Collections.sort(transQueueList);
			}

			form.setRecordCount(String.valueOf(transQueueList.size()));
			request.getSession().setAttribute("queueList", transQueueList);

			ServletContext sc = request.getSession().getServletContext();
			if (sc.getAttribute("tranStatuses") == null) {
				sc.setAttribute("tranStatuses", tm.getTranStatusCodes());
			}
			if (sc.getAttribute("tranSubStatuses") == null) {
				sc.setAttribute("tranSubStatuses", tm.getTranSubStatusCodes());
			}

			Integer code = Constants.partnerSiteIdToCode.get(partnerSiteId);
			Collection<TranType> tranTypes = tm
					.getTranTypes(code != null ? code.intValue() : -1);
			{
				Set<String> seen = new HashSet<String>();
				for (Iterator<TranType> i = tranTypes.iterator(); i.hasNext();) {
					TranType current = i.next();
					if (seen.contains(current.getEmgTranTypeCode())) {
						i.remove();
					} else {
						seen.add(current.getEmgTranTypeCode());
					}
				}
			}

			sc.setAttribute("tranTypes", tranTypes);

			Collection col = tm.getProcessUsers();
			Collection<UserId> users = new ArrayList<UserId>();
			Iterator itr = col.iterator();
			while (itr.hasNext()) {
				UserId userId = new UserId((String) itr.next());
				users.add(userId);
			}
			col = null;
			sc.setAttribute("users", users);
		}

		setQueueCounts(tm.getQueueCounts(), form);

		return mapping
				.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private boolean showDefaultQueue(ShowTransQueueForm form,
			HttpServletRequest request) {
		return form == null
				|| (form.getPartnerSiteId() == null && request
						.getParameter("partnerSiteId") == null);
	}

	private void addTrans(Transaction transaction,
			List<TranQueueView> transQueueList, UserProfile up) {
		TranQueueView trans = new TranQueueView();

		// column 1.a Owner of the transaction at Money Gram
		if (StringHelper.isNullOrEmpty(transaction.getCsrPrcsUserid())) {
			trans.setTransUserId("");
		} else {
			trans.setTransUserId(transaction.getCsrPrcsUserid());
		}

		// column 1.b processing date for the above user
		DateFormatter sdf = new DateFormatter("dd/MMM/yyyy hh:mm a", true);

		if (transaction.getCsrPrcsDate() == null) {
			trans.setTransOwnershipDate("");
		} else {
			trans.setTransOwnershipDate(sdf.format(transaction.getCsrPrcsDate()));
		}

		// column 1.c the possible actions the current user can do to the
		// transaction
		if (!up.hasPermission("transOwnership")) {
			trans.setTransCommand("None");
		} else if (trans.getTransUserId().equals("")) {
			trans.setTransCommand("Take Ownership");
		} else if (transaction.getCsrPrcsUserid().equalsIgnoreCase(up.getUID())) {
			trans.setTransCommand("Release");
		} else {
			trans.setTransCommand("Take Over");
		}

		// column 2 transaction id
		trans.setTransId(transaction.getEmgTranId() + "");

		// column 3 the customer
		trans.setTransSender(transaction.getSndCustAcctId() + "");

		// column 3.a the customer id
		trans.setTransCustId(transaction.getCustId() + "");

		// column 3x the customer name
		String fullName = "";

		if (!StringHelper.isNullOrEmpty(transaction.getSndCustFrstName())) {
			fullName = transaction.getSndCustFrstName() + " ";
		}

		if (!StringHelper.isNullOrEmpty(transaction.getSndCustLastName())) {
			fullName += transaction.getSndCustLastName();
		}

		trans.setTransSenderName(fullName);

		// column 6 transaction type
		if (StringHelper.isNullOrEmpty(transaction.getEmgTranTypeCode())) {
			trans.setTransType("");
		} else {
			trans.setTransType(transaction.getEmgTranTypeCode());
		}

		// transReceiver; // column 4
		fullName = "";
		if (!StringHelper.isNullOrEmpty(transaction.getRcvCustFrstName())) {
			fullName = transaction.getRcvCustFrstName() + " ";
		}

		if (!StringHelper.isNullOrEmpty(transaction.getRcvCustMidName())) {
			fullName += transaction.getRcvCustMidName() + " ";
		}

		if (!StringHelper.isNullOrEmpty(transaction.getRcvCustLastName())) {
			fullName += transaction.getRcvCustLastName();
		}

		if (transaction.getEmgTranTypeCode().equalsIgnoreCase(
				TransactionType.EXPRESS_PAYMENT_SEND_CODE)) {
			trans.setTransReceiver(transaction.getRcvAgcyCode() + "");
		} else {
			trans.setTransReceiver(fullName);
		}

		// country code for receiver 4.a
		trans.setTransCntryCd(transaction.getRcvISOCntryCode());

		// transStatus; // column 5.a
		if (StringHelper.isNullOrEmpty(transaction.getTranStatDesc())) {
			trans.setTransStatus("");
		} else {
			trans.setTransStatus(transaction.getTranStatDesc());
		}

		// transSubStatus; // column 5.b
		if (StringHelper.isNullOrEmpty(transaction.getTranSubStatDesc())) {
			trans.setTransSubStatus("");
		} else {
			trans.setTransSubStatus(transaction.getTranSubStatDesc());
		}

		// transAmount; // column 7
		NumberFormat df = new DecimalFormat("#,##0.00");
		trans.setTransAmount(df.format(transaction.getSndFaceAmt()
				.doubleValue()));

		// transStatusDate // column 8
		if (transaction.getTranStatDate() == null) {
			trans.setTransStatusDate("");
		} else {
			trans.setTransStatusDate(sdf.format(transaction.getTranStatDate()));
		}

		// transScores; // column 9
		trans.setTransScores(transaction.getTransScores());
		trans.setSysAutoRsltCode(transaction.getSysAutoRsltCode());

		trans.setEsSendable(transaction.getEsSendable());

		trans.setOverThrldWarnAmt(false);
		if (transaction.getSndThrldWarnAmt().doubleValue() != 0) {
			trans.setOverThrldWarnAmt(transaction.getSndFaceAmt().doubleValue() > transaction
					.getSndThrldWarnAmt().doubleValue());
		}

		trans.setPartnerSiteId(transaction.getPartnerSiteId());
		// added by Ankit Bhatt for MBO-128
		trans.setProfileType(transaction.getProfileType());
		// ended
		transQueueList.add(trans);
	}

	private boolean setFilters(HttpServletRequest request,
			ShowTransQueueForm form) {
		String path = ((String) request.getAttribute("path")).substring(1);

		boolean isQueue = true;
		String tranQueue = "Y";
		String queueType = null;

		if (path.equalsIgnoreCase(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRAN_APPROVAL_P2P)) {
			queueType = "1";
		} else if (path
				.equalsIgnoreCase(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRAN_APPROVAL_EP)) {
			queueType = "2";
		} else if (path
				.equalsIgnoreCase(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRAN_PROCESS_P2P)) {
			queueType = "3";
		} else if (path
				.equalsIgnoreCase(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRAN_PROCESS_EP)) {
			queueType = "4";
		} else if (path
				.equalsIgnoreCase(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRAN_IN_PROGRESS)) {
			queueType = "5";
		} else if (path
				.equalsIgnoreCase(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRAN_EXCEPTION)) {
			queueType = "6";
		} else if (path
				.equalsIgnoreCase(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRAN_APPROVAL_ES)) {
			queueType = "7";
		} else if (path
				.equalsIgnoreCase(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRAN_PROCESS_ES)) {
			queueType = "8";
		} else if (path
				.equalsIgnoreCase(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRAN_SEND_ES)) {
			queueType = "9";
		} else if (path
				.equalsIgnoreCase(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRAN_APPROVAL_DS)) {
			queueType = "10";
		} else if (path
				.equalsIgnoreCase(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRAN_PROCESS_DS)) {
			queueType = "11";
		} else if (path
				.equalsIgnoreCase(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRAN_SEND_DS)) {
			queueType = "12";
		} else if (path
				.equalsIgnoreCase(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRAN_PROCESS_DOC)) {
			queueType = "13";
		} else if (path
				.equalsIgnoreCase(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRAN_PROCESS_DOC_PEN)) {
			queueType = "14";
		} else {
			if (form.getSubmitRefresh() != null) {
				isQueue = false;
			} else {
				tranQueue = (String) request.getSession().getAttribute(
						"tranQueue");
				if ("Y".equalsIgnoreCase(tranQueue)) {
					queueType = (String) request.getSession().getAttribute(
							"queueType");
				}
			}
		}

		if (!isQueue) {
			tranQueue = "N";
		} else {
			form.setSubmitRefresh("Refresh");
		}

		request.getSession().setAttribute("tranQueue", tranQueue);
		request.getSession().setAttribute("queueType", queueType);

		if (isQueue) {
			if ("1".equals(queueType)) {
				form.setStatus(TransactionStatus.FORM_FREE_SEND_CODE);
				form.setFundStatus(TransactionStatus.NOT_FUNDED_CODE);
				form.setType(TransactionType.MONEY_TRANSFER_SEND_CODE);
			} else if ("2".equals(queueType)) {
				form.setStatus(TransactionStatus.FORM_FREE_SEND_CODE);
				form.setFundStatus(TransactionStatus.NOT_FUNDED_CODE);
				form.setType(TransactionType.EXPRESS_PAYMENT_SEND_CODE);
			} else if ("3".equals(queueType)) {
				form.setStatus(TransactionStatus.APPROVED_CODE);
				form.setType(TransactionType.MONEY_TRANSFER_SEND_CODE);
			} else if ("4".equals(queueType)) {
				form.setStatus(TransactionStatus.APPROVED_CODE);
				form.setType(TransactionType.EXPRESS_PAYMENT_SEND_CODE);
			} else if ("5".equals(queueType)) {
				form.setStatus(TransactionStatus.PENDING_CODE);
			} else if ("6".equals(queueType)) {
				form.setStatus(TransactionStatus.ERROR_CODE);
			} else if ("7".equals(queueType)) {
				form.setStatus(TransactionStatus.FORM_FREE_SEND_CODE);
				form.setType(TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE);
			} else if ("8".equals(queueType)) {
				form.setStatus(TransactionStatus.APPROVED_CODE);
				form.setFundStatus(TransactionStatus.NOT_FUNDED_CODE);
				form.setType(TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE);
			} else if ("9".equals(queueType)) {
				form.setStatus(TransactionStatus.APPROVED_CODE);
				form.setFundStatus(TransactionStatus.ACH_SENT_CODE);
				form.setType(TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE);
			} else if ("10".equals(queueType)) {
				form.setStatus(TransactionStatus.FORM_FREE_SEND_CODE);
				form.setType(TransactionType.DELAY_SEND_CODE);
				form.setPartnerSiteId(Constants.SITE_IDENTIFIER_WAP);
			} else if ("11".equals(queueType)) {
				form.setStatus(TransactionStatus.APPROVED_CODE);
				form.setFundStatus(TransactionStatus.NOT_FUNDED_CODE);
				form.setType(TransactionType.DELAY_SEND_CODE);
				form.setPartnerSiteId(Constants.SITE_IDENTIFIER_WAP);
			} else if ("12".equals(queueType)) {
				form.setStatus(TransactionStatus.APPROVED_CODE);
				form.setFundStatus(TransactionStatus.BANK_SETTLED_CODE);
				form.setType(TransactionType.DELAY_SEND_CODE);
				form.setPartnerSiteId(Constants.SITE_IDENTIFIER_WAP);
			} else if ("13".equals(queueType)) { // Process: DOC
				form.setStatus(TransactionStatus.SAVE_CODE);
				form.setDocStatus(TransactionStatus.DOC_STATUS_SUBMITTING_CODE);
				form.setFundStatus(TransactionStatus.NOT_FUNDED_CODE);
				form.setPartnerSiteId(Constants.SITE_IDENTIFIER_DE);
			} else if ("14".equals(queueType)) {// Process: DOC PEN
				form.setStatus(TransactionStatus.PENDING_CODE);
				form.setDocStatus(TransactionStatus.DOC_STATUS_PENDING_CODE);
				form.setFundStatus(TransactionStatus.NOT_FUNDED_CODE);
				form.setPartnerSiteId(Constants.SITE_IDENTIFIER_DE);
			} else {
				form.setStatus(TransactionStatus.FORM_FREE_SEND_CODE);
			}
		}

		return isQueue;
	}

	private void setQueueCounts(int[] count, ShowTransQueueForm form) {
		form.setAppP2PCount(count[0] + "");
		form.setAppEPCount(count[1] + "");
		form.setProcP2PCount(count[2] + "");
		form.setProcEPCount(count[3] + "");
		form.setInProgressCount(count[4] + "");
		form.setExceptionCount(count[5] + "");
		form.setAppESCount(count[6] + "");
		form.setProcESCount(count[7] + "");
		form.setSendESCount(count[8] + "");
		form.setAppDSCount(count[9] + "");
		form.setProcDSCount(count[10] + "");
		form.setSendDSCount(count[11] + "");
		form.setProcDOCCount(count[12] + "");
		form.setProcDOCPENCount(count[13] + "");
	}
}
