/*
 * Created on Apr 12, 2005
 *
 */
package emgadm.transqueue;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgshared.exceptions.DataSourceException;

/**
 * @author A131
 *
 */
public abstract class ConfirmSubReasonBaseAction
	extends EMoneyGramAdmBaseAction
{
	protected static final String FORWARD_SUCCESS = "success";
	protected static final String FORWARD_CANCEL = "cancel";
	protected static final String FORWARD_INVALID_TOKEN = "invalidToken";
	protected static final String FORWARD_INIT = "init";

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws
			NumberFormatException,
			TransactionAlreadyInProcessException,
			TransactionOwnershipException,
			SQLException,
			DataSourceException
	{
		ActionForward forward;
		ConfirmSubReasonForm inputForm = (ConfirmSubReasonForm) form;
		if (StringUtils.isNotBlank(inputForm.getSubmitRecord()))
		{
			if (isTokenValid(request, true))
			{
				forward = processConfirmation(mapping, inputForm, request, response);
			} else
			{				
				forward = mapping.findForward(FORWARD_INVALID_TOKEN);
			}
		} else if (StringUtils.isNotBlank(inputForm.getSubmitCancel()))
		{
			//2820
			//setting Cancel action names to null
			request.setAttribute("SubmitRecCcAutoRef", "true");	
			request.setAttribute("SubmitAutoCancelBankRqstRefund", "true");
			request.setAttribute("SubmitRecCcCxlRef", "true");
			request.setAttribute("SubmitCancelBankRqstRefund", "true");	
			request.setAttribute("SubmitCancelTran", "true");
			request.setAttribute("SubmitManualCancelTran", "true");
			
			forward = mapping.findForward(FORWARD_CANCEL);
			resetToken(request);
		} else
		{
		    getCommandDescription(request);
		    forward = initialize(mapping, inputForm, request, response);
			saveToken(request);
		}
		return forward;
	}

	protected abstract ActionForward processConfirmation(
		ActionMapping mapping,
		ConfirmSubReasonForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws
			NumberFormatException,
			TransactionAlreadyInProcessException,
			TransactionOwnershipException, DataSourceException, SQLException;

	protected ActionForward initialize(
		ActionMapping mapping,
		ConfirmSubReasonForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws
			NumberFormatException,
			TransactionAlreadyInProcessException,
			TransactionOwnershipException
	{
		return mapping.findForward(FORWARD_INIT);
	}
	
	protected abstract void getCommandDescription(HttpServletRequest request);
	
}
