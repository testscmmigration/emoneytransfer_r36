package emgadm.transqueue;

import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.owasp.esapi.ESAPI;



import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.model.UserProfile;
import emgadm.services.ServiceFactory;
import emgadm.services.TransactionService;
import emgadm.sysmon.EventListConstant;
import emgadm.util.restclient.osl.OSLClientExecutor;
import emgadm.util.restclient.tps.TPSClientExecutor;
import emgadm.util.restclient.tps.cancel.TPSCancelResponse;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.AgentConnectException;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.ConsumerAccountType;
import emgshared.model.Transaction;
import emgshared.model.TransactionStatus;
import emgshared.model.TransactionType;
import emgshared.util.StringHelper;
import engadm.util.restclient.osl.canceltransaction.CancelTransactionResponse;
import eventmon.eventmonitor.EventMonitor;

/**
 * @author wx54
 */
public class CancelAction extends ConfirmSubReasonBaseAction {
	
	private static Logger logger = EMGSharedLogger.getLogger(
			ConfirmSubReasonBaseAction.class);

	protected ActionForward processConfirmation(ActionMapping mapping,
			ConfirmSubReasonForm form, HttpServletRequest request,
			HttpServletResponse response) throws NumberFormatException,
			TransactionAlreadyInProcessException,
			TransactionOwnershipException, DataSourceException, SQLException{

		ActionErrors errors = new ActionErrors();
		UserProfile up = getUserProfile(request);
		TransactionManager tm = getTransactionManager(request);
		TransactionService tranService = ServiceFactory.getInstance()
				.getTransactionService();

		int tranId = Integer.parseInt(form.getTranId());
		Transaction tran = tm.getTransaction(tranId);
		
		String tranStatus = null;
		String fundStatus = null;
		boolean changeStatus=false;
		if (!StringHelper.isNullOrEmpty(form
				.getSubmitRecCcAutoRef())) {
			logger.info("Inside Auto Cancel - Cancel Action");
			doAutoRefund(form, up, tm, errors, request, true);
			/*try {
				ConsumerAccountType primaryAccountType = ConsumerAccountType
				.getInstance(tran.getSndAcctTypeCode());
				tranService.refundPersonToPersonSuccessEmail(tran,
						primaryAccountType);
			} catch (Exception e) {
				logger.error("Exception occurs while sending cancel email", e);
			}*/
			/** This parameter will be checked in TranscationDetailAction
			 * to render Transaction detail page after canceling transaction.
			 */
			request.setAttribute("SubmitRecCcAutoRef", "true");	
		} else if (!StringHelper.isNullOrEmpty(form
				.getSubmitAutoCancelBankRqstRefund())) {
			// Handle the automated request refund (will send MF
			// reversal)

			// send Reversal to Mainframe via AgentConnect
			doAutoRefund(form, up, tm, errors, request, true);
			//Added for MBO-5605 to fetch updated status of txn from DB-> SEN/BRR to CXL/BRR
			tran = tm.getTransaction(tranId);
			//MBO-2610 logic to change status from SEN/BKR to CXL/BKR without bank reversal call

			if (!((TransactionStatus.SENT_CODE)
					.equalsIgnoreCase(tran.getStatus()
							.getStatusCode()) && (TransactionStatus.BANK_REJECTED_CODE)
					.equalsIgnoreCase(tran.getStatus()
							.getSubStatusCode()))) {
				if (errors.isEmpty()) {
					// Now do the Bank reversal/refund
					long tmpTime = System.currentTimeMillis();
					tranService.doBankReversal(
							Integer.parseInt(form.getTranId()),
							up.getUID(), request.getContextPath());
					EventMonitor
							.getInstance()
							.recordEvent(
									EventListConstant.BANK_PAYMENT_SERVICE_ADJUSTMENT,
									tmpTime,
									System.currentTimeMillis());
					// Set up txn state transition for SEN/BRR
					// status
					changeStatus = false;
				}
			} else {
				tranStatus = TransactionStatus.CANCELED_CODE;
				fundStatus = TransactionStatus.BANK_REJECTED_CODE;
				changeStatus = true;
			}	
		
				 if (tran.getStatus()
						.getStatusCode()
						.equalsIgnoreCase(
								TransactionStatus.SENT_CODE)
						&& tran.getStatus()
								.getSubStatusCode()
								.equalsIgnoreCase(
										TransactionStatus.BANK_REFUND_REQUEST_CODE)) {
					tranStatus = TransactionStatus.CANCELED_CODE;
					fundStatus = TransactionStatus.BANK_REFUND_REQUEST_CODE;
					changeStatus = true;						
				}
				 /** This parameter will be checked in TranscationDetailAction
					 * to render Transaction detail page after canceling transaction.
					 */
					request.setAttribute("SubmitAutoCancelBankRqstRefund", "true");
			} else if (!StringHelper.isNullOrEmpty(form
				.getSubmitRecCcCxlRef())) {
			if (TransactionStatus.SENT_CODE.equals(tran.getStatus().getStatusCode())) {
				//MBO-2610 The manual Credit card refund button is pressed
				tm.setTransactionComment(
						tranId,
						"OTH",
						"Manual Cancel button (Manual Record CC Cancel/Refund) pressed",
						up.getUID());

				// call the new method to change status. Testing for
				// now
				changeTranStatusRefundManual(tranId,
						TransactionStatus.CANCELED_CODE,
						tran.getStatus().getSubStatusCode(), up.getUID(),
						null, tm);
				// call the new method to change status. Testing for
				// now
				changeTranStatusRefundManual(tranId,
						TransactionStatus.CANCELED_CODE,
						TransactionStatus.CC_REFUND_REQUESTED_CODE,
						up.getUID(), null, tm);
				tranStatus = TransactionStatus.CANCELED_CODE;

			} else {
				changeTranStatusRefundManual(tranId,
						tran.getStatus().getStatusCode(),
						TransactionStatus.CC_REFUND_REQUESTED_CODE,
						up.getUID(), null, tm);
				tranStatus = tran.getStatus().getStatusCode();
			}
			// Now do the CC funding reversal
			doAutoRefund(form, up, tm, errors, request, false);
			ConsumerAccountType primaryAccountType = ConsumerAccountType
					.getInstance(tran.getSndAcctTypeCode());
			// Call again the service for the transaction according
			// the pymt_prvd_srv_code
			tranService = ServiceFactory.getInstance()
					.getTransactionServicePymtProvider(
							tran.getTCProviderCode(),
							tran.getPartnerSiteId());
			//refund person to person - Agent connect send reversal will not be called 
			// hence the methdod doAutoRefund success flow is taken as success case and email sent
			try {
				tranService.refundPersonToPersonSuccessEmail(tran,
						primaryAccountType);
				//** This parameter will be checked in TranscationDetailAction
				 // to render Transaction detail page after canceling transaction.
				 //*
				request.setAttribute("SubmitRecCcCxlRef", "true");
			} catch (Exception e) {
				logger.error("Exception occurs while sending success email", e);
			}

		} else if (!StringHelper.isNullOrEmpty(form
						.getSubmitCancelBankRqstRefund())) {
					// Handle the non automated requestrefund (no MF
					// reversal)
					// MBO-2610 Added transaction comment when Cancel and
					// Request Bank Refund button is clicked
					tm.setTransactionComment(
							tranId,
							"OTH",
							"Manual Cancel button (Cancel and Request Bank Refund) pressed",
							up.getUID());				
					//MBO-2610 Removed bank reversal call if the Status in SEN/BKR
					if (!((TransactionStatus.SENT_CODE)
							.equalsIgnoreCase(tran.getStatus()
									.getStatusCode()) && (TransactionStatus.BANK_REJECTED_CODE)
							.equalsIgnoreCase(tran.getStatus()
									.getSubStatusCode()))) {						
					// Now do the Bank reversal/refund
					long tmpTime = System.currentTimeMillis();
					tranService.doBankReversal(
							Integer.parseInt(form.getTranId()),
							up.getUID(), request.getContextPath());
					EventMonitor
							.getInstance()
							.recordEvent(
									EventListConstant.BANK_PAYMENT_SERVICE_ADJUSTMENT,
									tmpTime, System.currentTimeMillis());
					}
	
					// Change 1st status (SEN/BKS -> SEN/BRR, SEN/BKR ->
					// CXL/BKR)
					if (tran.getStatus()
							.getSubStatusCode()
							.equalsIgnoreCase(
									TransactionStatus.BANK_SETTLED_CODE)) {
						tran.setTranSubStatCode(TransactionStatus.BANK_REFUND_REQUEST_CODE);
					} else {
						tran.setTranStatCode(TransactionStatus.CANCELED_CODE);
					}
					tran.setTranStatDate(new Date());
					tm.updateTransaction(tran, up.getUID());
					
					// Set up txn state transition for SEN/BRR status
					changeStatus = false;
					if (tran.getStatus().getStatusCode()
							.equalsIgnoreCase(TransactionStatus.SENT_CODE)
							&& tran.getStatus()
									.getSubStatusCode()
									.equalsIgnoreCase(
											TransactionStatus.BANK_REFUND_REQUEST_CODE)) {
						tranStatus = TransactionStatus.CANCELED_CODE;
						fundStatus = TransactionStatus.BANK_REFUND_REQUEST_CODE;
						changeStatus = true;	
						try {
							ConsumerAccountType primaryAccountType = ConsumerAccountType
							.getInstance(tran.getSndAcctTypeCode());
							if (!StringHelper.isNullOrEmpty(form
									.getSubmitAutoCancelBankRqstRefund())
									|| !StringHelper.isNullOrEmpty(form
											.getSubmitCancelBankRqstRefund()))
								if (tran.getEmgTranTypeCode().equals("DSSEND")
										&& tranStatus
												.equals(TransactionStatus.CANCELED_CODE)
										&& fundStatus
												.equals(TransactionStatus.BANK_REFUND_REQUEST_CODE)) {
									tranService.refundPersonToPersonSuccessEmail(tran,
											primaryAccountType);
								}
						} catch (Exception e) {
							logger.error("Exception occurs while sending Cancel email", e);
						}
					}
					
					try {
						
					/** This parameter will be checked in TranscationDetailAction
					 * to render Transaction detail page after canceling transaction.
					 */
					request.setAttribute("SubmitCancelBankRqstRefund", "true");
					}catch (Exception e) {
						logger.error("Exception occured :::", e);
					}
			}	else if (!StringHelper.isNullOrEmpty(form.getSubmitCancelTransaction())
					|| !StringHelper.isNullOrEmpty(form.getSubmitManCancelTran())) {
			
			if (null != tran.getEmgTranTypeCode()
					&& (TransactionType.MONEY_TRANSFER_RTBANK_SEND_CODE
							.equalsIgnoreCase(tran.getEmgTranTypeCode()) || TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE
							.equalsIgnoreCase(tran.getEmgTranTypeCode())
							|| TransactionType.MONEY_TRANSFER_SEND_CODE.equalsIgnoreCase(tran.getEmgTranTypeCode())
							|| TransactionType.EXPRESS_PAYMENT_SEND_CODE.equalsIgnoreCase(tran.getEmgTranTypeCode())
							)) {
				// TPS
				TPSClientExecutor tpsExec = new TPSClientExecutor();
				TPSCancelResponse tpsResponse = tpsExec
						.processTransactionCancelation(StringUtils
								.isNotBlank(form.getSubmitManCancelTran()), up
								.getUID(), tranId);
				if (tpsResponse == null) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"exception.unexpected"));
					saveErrors(request, errors);
				}
			} else {
				// /add code to call OSL proxy
				OSLClientExecutor exec = new OSLClientExecutor();
				CancelTransactionResponse oslResponse = exec
						.processTransactionCancelation(StringUtils
								.isNotBlank(form.getSubmitManCancelTran()), up
								.getUID(), tranId);
				if (oslResponse == null) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"exception.unexpected"));
					saveErrors(request, errors);
				}
			}
                    
                    if(!StringHelper.isNullOrEmpty(form.getSubmitCancelTransaction())){request.setAttribute("SubmitCancelTran", "true");}
                    if(!StringHelper.isNullOrEmpty(form.getSubmitManCancelTran())){request.setAttribute("SubmitManualCancelTran", "true");}
			}		 
		if (changeStatus) {
			Transaction oldTran = tm.getTransaction(tranId);
			if (oldTran.getTranStatCode().equalsIgnoreCase(
					tranStatus)
					&& oldTran.getTranSubStatCode()
							.equalsIgnoreCase(fundStatus)) {
				errors.add(ActionErrors.GLOBAL_ERROR,
						new ActionError(
								"error.transaction.token.invalid"));
				saveErrors(request, errors);
			} else {
				tranService.setTransactionStatus(tranId,
						tranStatus, fundStatus, up.getUID());
				if (!StringHelper.isNullOrEmpty(form
						.getSubmitAutoChargebackAndWriteOffLoss())) {
					tranService.setTransactionStatus(tranId,
							tranStatus,
							TransactionStatus.WRITE_OFF_LOSS_CODE,
							up.getUID());
				}
			}
		}		
		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	protected void getCommandDescription(HttpServletRequest request) {
		Locale locale = Locale.getDefault();
				
		MessageResources msgRes = MessageResources
				.getMessageResources("emgadm.resources.TransactionConfirmationDescription");
		MessageFormat formatter = null;
		/**
		 * This parameter will come from TrasncationDetailAction for 
		 * particular Cancel action. Based on Cancel Action, related message will be 
		 * displayed on JSP.
		 */
		String cancelAction = request.getAttribute("cancelAction")!=null ? request.getAttribute("cancelAction").toString() : null;
		
		if(cancelAction!=null && !cancelAction.isEmpty()) {
			if("autoCancel".equalsIgnoreCase(cancelAction)) {
				formatter = new MessageFormat(
						msgRes.getMessage("confirmation.record.cc.auto.refund"));
			} else if("manualCancel".equalsIgnoreCase(cancelAction)) {
				formatter = new MessageFormat(
						msgRes.getMessage("confirmation.record.cc.cancel.refund"));
			} else if("submitCancelBankRqstRefund".equalsIgnoreCase(cancelAction)) {
				formatter = new MessageFormat(
						msgRes.getMessage("confirmation.cancel.bank.rqst.refund"));
			} else if("autoCancelBankRefund".equalsIgnoreCase(cancelAction)) {
				formatter = new MessageFormat(
						msgRes.getMessage("confirmation.auto.cancel.bank.rqst.refund"));
			} else if("autoCancelRTBANK".equalsIgnoreCase(cancelAction)){
				formatter = new MessageFormat(
						msgRes.getMessage("confirmation.cancel.transaction"));
			} else if("manualCancelRTBANK".equalsIgnoreCase(cancelAction)){
				formatter = new MessageFormat(
						msgRes.getMessage("confirmation.manual.cancel.transaction"));
			}
		}
		formatter.setLocale(locale);
		String msgText = formatter.toPattern();
		request.setAttribute("msgText", msgText);		
	}
	
	private void doAutoRefund(ConfirmSubReasonForm form, UserProfile up,
			TransactionManager tm, ActionErrors errors,
			HttpServletRequest request, boolean cancelMainframe)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException, DataSourceException, SQLException {
		if(form.getTranId()==null) {
			throw new DataSourceException("TranId is null");
		} else {
			logger.info("Inside CancelAction -> doAutoRefund - tranId is " + ESAPI.encoder().encodeForHTMLAttribute(form.getTranId()));
		}
		Transaction tmp = tm.getTransaction(Integer.parseInt(form.getTranId()));
		TransactionService tranService = ServiceFactory.getInstance()
				.getTransactionServicePymtProvider(tmp.getTCProviderCode(),
						tmp.getPartnerSiteId());

		int tranId = Integer.parseInt(form.getTranId());
		if (cancelMainframe) {
			try {
				tranService.refundPersonToPerson(tranId, up.getUID());
			} catch (AgentConnectException e) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"error.agent.connect.failed"));
				saveErrors(request, errors);
				return;
			} catch (EMGRuntimeException rt) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"error.cancel.mainframe.failed"));
				saveErrors(request, errors);
				return;
			}
		}
		if(form.getTranSubStatusCode()!=null) {
			 logger.info("Tran status is " + ESAPI.encoder().encodeForHTMLAttribute(form.getTranSubStatusCode()));
		}
		//MBO-2813 changes, added the EWL status
		if (form.getTranSubStatusCode().equalsIgnoreCase(
				TransactionStatus.CC_SALE_CODE)
				|| form.getTranSubStatusCode().equalsIgnoreCase(
						TransactionStatus.EXCEPTION_WRITE_OFF_LOSS_CODE)) {
			tranService.doCreditCardReversal(tranId, up.getUID(),
					request.getContextPath());
		}

	}
	
	private void changeTranStatusRefundManual(int tranId, String tranStatus,
			String fundStatus, String userId, String partnerSiteId,
			TransactionManager tm) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		// Get the transaction
		Transaction transaction = tm.getTransaction(tranId);
		// Create the transaction service according the pymt_prvdr_srv_code
		TransactionService tranService = ServiceFactory.getInstance()
				.getTransactionServicePymtProvider(
						transaction.getTCProviderCode(),
						transaction.getPartnerSiteId());
		// Set the transaction status for the transaction
		tranService
				.setTransactionStatus(tranId, tranStatus, fundStatus, userId);
	}
}
