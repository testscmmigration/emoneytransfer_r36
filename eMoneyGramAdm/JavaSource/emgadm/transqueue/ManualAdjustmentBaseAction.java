/*
 * Created on Apr 12, 2005
 *
 */
package emgadm.transqueue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;

/**
 * @author A131
 *
 */
public abstract class ManualAdjustmentBaseAction
	extends EMoneyGramAdmBaseAction
{
	protected static final String FORWARD_SUCCESS = "success";
	protected static final String FORWARD_CANCEL = "cancel";
	protected static final String FORWARD_INVALID_TOKEN = "invalidToken";
	protected static final String FORWARD_INIT = "init";

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws
			NumberFormatException,
			TransactionAlreadyInProcessException,
			TransactionOwnershipException
	{
		ActionForward forward;
		ManualAdjustmentForm inputForm = (ManualAdjustmentForm) form;
		if (StringUtils.isNotBlank(inputForm.getSubmitRecord()))
		{
			if (isTokenValid(request, true))
			{
				forward =
					processAdjustment(mapping, inputForm, request, response);
			} else
			{
				forward = mapping.findForward(FORWARD_INVALID_TOKEN);
			}
		} else if (StringUtils.isNotBlank(inputForm.getSubmitCancel()))
		{
			forward = mapping.findForward(FORWARD_CANCEL);
			resetToken(request);
		} else
		{
			getCommandDescription(request);
			forward = initialize(mapping, inputForm, request, response);
			saveToken(request);
		}
		return forward;
	}

	protected abstract ActionForward processAdjustment(
		ActionMapping mapping,
		ManualAdjustmentForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws
			NumberFormatException,
			TransactionAlreadyInProcessException,
			TransactionOwnershipException;

	protected ActionForward initialize(
		ActionMapping mapping,
		ManualAdjustmentForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws
			NumberFormatException,
			TransactionAlreadyInProcessException,
			TransactionOwnershipException
	{
		return mapping.findForward(FORWARD_INIT);
	}
	
	protected abstract void getCommandDescription(HttpServletRequest request);
	
}
