package emgadm.transqueue;

import java.text.MessageFormat;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.model.UserProfile;
import emgadm.services.ServiceFactory;
import emgadm.services.TransactionService;
import emgshared.model.TransactionStatus;

/**
 * @author A131
 *
 */
public class RecordCcChargebackAction extends ManualAdjustmentBaseAction
{
	protected ActionForward processAdjustment(
		ActionMapping mapping,
		ManualAdjustmentForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws
			NumberFormatException,
			TransactionAlreadyInProcessException,
			TransactionOwnershipException
	{
//		ActionErrors errors = new ActionErrors();
		UserProfile userProfile = getUserProfile(request);

//		TransactisonManager tm = getTransactionManager(request);
		TransactionService tranService =
			ServiceFactory.getInstance().getTransactionService();

//		int tranId = Integer.parseInt(form.getTranId());
//		Transaction tran = tm.getTransaction(tranId);

//		List tas;
		tranService.recordCcChargeback(Integer.parseInt(form.getTranId()),
				Float.parseFloat(form.getAmount()),userProfile.getUID(),
				TransactionStatus.CC_CHARGEBACK_CODE,form.getSubReasonCode());

		return mapping.findForward(FORWARD_SUCCESS);
	}

	protected void getCommandDescription(HttpServletRequest request)
	{
		Locale locale = Locale.getDefault();
		MessageResources msgRes = MessageResources.getMessageResources("emgadm.resources.TransactionConfirmationDescription");
		MessageFormat formatter = new MessageFormat(msgRes.getMessage("confirmation.record.cc.chargeback"));
		formatter.setLocale(locale);
		String msgText = formatter.toPattern();
		request.setAttribute("msgText", msgText);
		
	}
}
