/*
 * Created on Jul 27,  2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.transqueue;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;
import emgshared.exceptions.ParseDateException;
import emgshared.util.DateFormatter;

/**
 * @author Jawahar Varadhan
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CcChargebackTrackingForm extends EMoneyGramAdmBaseValidatorForm{

	private String chgbkStatCode;
	private String chgbkTrkDate;
	private String gndrCode;
	private String custCntctMhtdCode;
	private String emailCnstntFlag;
	private String phNbrVerfdFlag;
	private String custVrblCntctFlag;
	private String unsolicitPhCallFlag;
	private String custPrflVldnFlag;
	private String sndRcvMileQty;
	private String linkedCustPrflCnt;
	private String chgbkCmntText;
	
	private String chgbkPrvdrCaseId;
	private String chgbkDenialCode;
	private String chgbkOrgnStmtDate;
	private Date chgbkCreateDate;
	
	private String submitSaveHeader;
	private String submitSaveComment;
	private String submitSaveCase;

	public ActionErrors validate(ActionMapping mapping,	HttpServletRequest request)
	{
		ActionErrors errors = super.validate(mapping, request);
		DateFormatter dfmt = new DateFormatter("dd/MMM/yyyy", false);
		
		if (!StringHelper.isNullOrEmpty(submitSaveHeader)){

			if (!StringHelper.isNullOrEmpty(chgbkOrgnStmtDate)){ 
				try
				{
					dfmt.parse(chgbkTrkDate);
				} catch (ParseDateException e)
				{
					errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError("error.malformed.date"));
				}
			}

			if (StringHelper.isNullOrEmpty(chgbkStatCode)){ 
				errors.add(ActionErrors.GLOBAL_ERROR, 
					new ActionError("errors.required", "Chargeback Status Code"));
			}

			if (StringHelper.isNullOrEmpty(gndrCode)){ 
				errors.add(ActionErrors.GLOBAL_ERROR, 
					new ActionError("errors.required", "Gender Type"));
			}

			if (StringHelper.isNullOrEmpty(emailCnstntFlag)){ 
				errors.add(ActionErrors.GLOBAL_ERROR, 
					new ActionError("errors.required", "Email Consistent Flag"));
			}

			if (StringHelper.isNullOrEmpty(phNbrVerfdFlag)){ 
				errors.add(ActionErrors.GLOBAL_ERROR, 
					new ActionError("errors.required", "Phone Number Verification"));
			}

			if (StringHelper.isNullOrEmpty(custVrblCntctFlag)){ 
				errors.add(ActionErrors.GLOBAL_ERROR, 
					new ActionError("errors.required", "Customer Verbal Contact Flag"));
			}

			if (StringHelper.isNullOrEmpty(unsolicitPhCallFlag)){ 
				errors.add(ActionErrors.GLOBAL_ERROR, 
					new ActionError("errors.required", "Unsolicit Phone Call Flag"));
			}

			if (StringHelper.isNullOrEmpty(custPrflVldnFlag)){ 
				errors.add(ActionErrors.GLOBAL_ERROR, 
					new ActionError("errors.required", "Customer Profile Validation"));
			}

			if (!StringHelper.isNullOrEmpty(linkedCustPrflCnt)){ 
				try
				{
					Integer.parseInt(linkedCustPrflCnt);
				} catch (NumberFormatException e)
				{
					errors.add(ActionErrors.GLOBAL_ERROR, 
							new ActionError("errors.required", "Numeric value for Linked Profiles count"));				
				}
			}

			if (!StringHelper.isNullOrEmpty(sndRcvMileQty)){ 
				try
				{
					Integer.parseInt(sndRcvMileQty);
				} catch (NumberFormatException e)
				{
					errors.add(ActionErrors.GLOBAL_ERROR, 
							new ActionError("errors.required", "Numeric value for miles between sender and receiver"));				
				}
			}

		} 			

		if (!StringHelper.isNullOrEmpty(submitSaveCase)){
			if (StringHelper.isNullOrEmpty(chgbkPrvdrCaseId)){ 
				errors.add(ActionErrors.GLOBAL_ERROR, 
					new ActionError("errors.required", "Chargeback Provider Case Id"));
			}

			if (StringHelper.isNullOrEmpty(chgbkDenialCode)){ 
				errors.add(ActionErrors.GLOBAL_ERROR, 
					new ActionError("errors.required", "Denial Type"));
			}

			if (StringHelper.isNullOrEmpty(chgbkOrgnStmtDate)){ 
				errors.add(ActionErrors.GLOBAL_ERROR, 
					new ActionError("errors.required", "Originator Statement Date"));
			}else{
				try
				{
					dfmt.parse(chgbkOrgnStmtDate);
				} catch (ParseDateException e)
				{
					errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError("error.malformed.date"));
				}
			}
		}

		if (!StringHelper.isNullOrEmpty(submitSaveComment)){
			if (StringHelper.isNullOrEmpty(chgbkCmntText)){ 
				errors.add(ActionErrors.GLOBAL_ERROR, 
					new ActionError("errors.required", "Chargeback Comment text"));
			}
		}
		
/*		
		if (errors.size() == 0)
		{
			try
			{
				Date dt = dfmt.parse(chgbkTrkDate);
				Date dt1 = dfmt.parse(chgbkOrgnStmtDate);
			} catch (ParseDateException e)
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("error.malformed.date"));
			}
		}		
*/
		return errors;
	}	
	/**
	 * @return Returns the chgbkDenialCode.
	 */
	public String getChgbkDenialCode() {
		return chgbkDenialCode;
	}
	/**
	 * @param chgbkDenialCode The chgbkDenialCode to set.
	 */
	public void setChgbkDenialCode(String chgbkDenialCode) {
		this.chgbkDenialCode = chgbkDenialCode;
	}
	/**
	 * @return Returns the chgbkOrgnStmtDate.
	 */
	public String getChgbkOrgnStmtDate() {
		return chgbkOrgnStmtDate;
	}
	/**
	 * @param chgbkOrgnStmtDate The chgbkOrgnStmtDate to set.
	 */
	public void setChgbkOrgnStmtDate(String chgbkOrgnStmtDate) {
		this.chgbkOrgnStmtDate = chgbkOrgnStmtDate;
	}
	/**
	 * @return Returns the chgbkPrvdrCaseId.
	 */
	public String getChgbkPrvdrCaseId() {
		return chgbkPrvdrCaseId;
	}
	/**
	 * @param chgbkPrvdrCaseId The chgbkPrvdrCaseId to set.
	 */
	public void setChgbkPrvdrCaseId(String chgbkPrvdrCaseId) {
		this.chgbkPrvdrCaseId = chgbkPrvdrCaseId;
	}

	
	
	/**
	 * @return Returns the chgbkCmntText.
	 */
	public String getChgbkCmntText() {
		return chgbkCmntText;
	}
	/**
	 * @param chgbkCmntText The chgbkCmntText to set.
	 */
	public void setChgbkCmntText(String chgbkCmntText) {
		this.chgbkCmntText = chgbkCmntText;
	}
	/**
	 * @return Returns the chgbkStatCode.
	 */
	public String getChgbkStatCode() {
		return chgbkStatCode;
	}
	/**
	 * @param chgbkStatCode The chgbkStatCode to set.
	 */
	public void setChgbkStatCode(String chgbkStatCode) {
		this.chgbkStatCode = chgbkStatCode;
	}


	/**
	 * @return Returns the chgbkTrkDate.
	 */
	public String getChgbkTrkDate() {
		return chgbkTrkDate;
	}
	/**
	 * @param chgbkTrkDate The chgbkTrkDate to set.
	 */
	public void setChgbkTrkDate(String chgbkTrkDate) {
		this.chgbkTrkDate = chgbkTrkDate;
	}
	/**
	 * @return Returns the custCntctMhtdCode.
	 */
	public String getCustCntctMhtdCode() {
		return custCntctMhtdCode;
	}
	/**
	 * @param custCntctMhtdCode The custCntctMhtdCode to set.
	 */
	public void setCustCntctMhtdCode(String custCntctMhtdCode) {
		this.custCntctMhtdCode = custCntctMhtdCode;
	}
	/**
	 * @return Returns the custPrflVldnFlag.
	 */
	public String getCustPrflVldnFlag() {
		return custPrflVldnFlag;
	}
	/**
	 * @param custPrflVldnFlag The custPrflVldnFlag to set.
	 */
	public void setCustPrflVldnFlag(String custPrflVldnFlag) {
		this.custPrflVldnFlag = custPrflVldnFlag;
	}
	/**
	 * @return Returns the custVrblCntctFlag.
	 */
	public String getCustVrblCntctFlag() {
		return custVrblCntctFlag;
	}
	/**
	 * @param custVrblCntctFlag The custVrblCntctFlag to set.
	 */
	public void setCustVrblCntctFlag(String custVrblCntctFlag) {
		this.custVrblCntctFlag = custVrblCntctFlag;
	}
	/**
	 * @return Returns the emailCnstntFlag.
	 */
	public String getEmailCnstntFlag() {
		return emailCnstntFlag;
	}
	/**
	 * @param emailCnstntFlag The emailCnstntFlag to set.
	 */
	public void setEmailCnstntFlag(String emailCnstntFlag) {
		this.emailCnstntFlag = emailCnstntFlag;
	}
	/**
	 * @return Returns the gndrCode.
	 */
	public String getGndrCode() {
		return gndrCode;
	}
	/**
	 * @param gndrCode The gndrCode to set.
	 */
	public void setGndrCode(String gndrCode) {
		this.gndrCode = gndrCode;
	}
	/**
	 * @return Returns the linkedCustPrflCnt.
	 */
	public String getLinkedCustPrflCnt() {
		return linkedCustPrflCnt;
	}
	/**
	 * @param linkedCustPrflCnt The linkedCustPrflCnt to set.
	 */
	public void setLinkedCustPrflCnt(String linkedCustPrflCnt) {
		this.linkedCustPrflCnt = linkedCustPrflCnt;
	}
	/**
	 * @return Returns the phNbrVerfdFlag.
	 */
	public String getPhNbrVerfdFlag() {
		return phNbrVerfdFlag;
	}
	/**
	 * @param phNbrVerfdFlag The phNbrVerfdFlag to set.
	 */
	public void setPhNbrVerfdFlag(String phNbrVerfdFlag) {
		this.phNbrVerfdFlag = phNbrVerfdFlag;
	}
	/**
	 * @return Returns the sndRcvMileQty.
	 */
	public String getSndRcvMileQty() {
		return sndRcvMileQty;
	}
	/**
	 * @param sndRcvMileQty The sndRcvMileQty to set.
	 */
	public void setSndRcvMileQty(String sndRcvMileQty) {
		this.sndRcvMileQty = sndRcvMileQty;
	}
	/**
	 * @return Returns the unsolicitPhCallFlag.
	 */
	public String getUnsolicitPhCallFlag() {
		return unsolicitPhCallFlag;
	}
	/**
	 * @param unsolicitPhCallFlag The unsolicitPhCallFlag to set.
	 */
	public void setUnsolicitPhCallFlag(String unsolicitPhCallFlag) {
		this.unsolicitPhCallFlag = unsolicitPhCallFlag;
	}
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);
	}

	/**
	 * @return Returns the submitSaveComment.
	 */
	public String getSubmitSaveComment() {
		return submitSaveComment;
	}
	/**
	 * @param submitSaveComment The submitSaveComment to set.
	 */
	public void setSubmitSaveComment(String submitSaveComment) {
		this.submitSaveComment = submitSaveComment;
	}
	/**
	 * @return Returns the submitSaveHeader.
	 */
	public String getSubmitSaveHeader() {
		return submitSaveHeader;
	}
	/**
	 * @param submitSaveHeader The submitSaveHeader to set.
	 */
	public void setSubmitSaveHeader(String submitSaveHeader) {
		this.submitSaveHeader = submitSaveHeader;
	}
	/**
	 * @return Returns the submitSaveCase.
	 */
	public String getSubmitSaveCase() {
		return submitSaveCase;
	}
	/**
	 * @param submitSaveCase The submitSaveCase to set.
	 */
	public void setSubmitSaveCase(String submitSaveCase) {
		this.submitSaveCase = submitSaveCase;
	}
    /**
     * @return Returns the chgbkCreateDate.
     */
    public Date getChgbkCreateDate() {
        return chgbkCreateDate;
    }
    /**
     * @param chgbkCreateDate The chgbkCreateDate to set.
     */
    public void setChgbkCreateDate(Date cbcd) {
        this.chgbkCreateDate = cbcd;
    }
}
