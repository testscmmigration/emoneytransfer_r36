package emgadm.transqueue;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class ConfirmSubReasonForm extends EMoneyGramAdmBaseValidatorForm
{
	private String tranId;
	private String subReasonCode;
	private String submitRecord;
	private String submitCancel;
	private String subReasonDescription;
	private String tranSubStatusCode;
	private String submitAutoCancelBankRqstRefund;
	private String submitAutoChargebackAndWriteOffLoss;
	private String submitCancelBankRqstRefund;
	private String submitRecCcAutoRef;
	private String submitRecCcCxlRef;
	private String tranStatusCode;
	private String submitBankRqstRefund;
	private String submitCancelTransaction;
	private String submitManCancelTran;
	
	public String getTranId()
	{
		return tranId;
	}

	public void setTranId(String string)
	{
		tranId = string;
	}

	public String getSubmitRecord()
	{
		return submitRecord;
	}

	public void setSubmitRecord(String string)
	{
		submitRecord = string;
	}

	public String getSubmitCancel()
	{
		return submitCancel;
	}

	public void setSubmitCancel(String string)
	{
		submitCancel = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		subReasonCode = "";
		tranId = "";
		submitRecord = "";
		submitCancel = "";

	}
	
	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors;
		errors = super.validate(mapping, request);
		errors = errors == null ? new ActionErrors() : errors;

		String cancelAction = request.getAttribute("cancelAction")!=null ? request.getAttribute("cancelAction").toString() : null;
		if(null==cancelAction) { //2820 -no sub reason implemented yet for Cancel Transaction
		if (!StringHelper.isNullOrEmpty(submitRecord))
		{
//			float tmpAmt = 0F;
			if (StringHelper.isNullOrEmpty(subReasonCode))
			{
				errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("errors.required", "Sub-Reason Type"));
			}
		 }
		}
		return errors;
	}
    /**
     * @return Returns the subReasonCode.
     */
    public String getSubReasonCode() {
        return subReasonCode;
    }
    /**
     * @param subReasonCode The subReasonCode to set.
     */
    public void setSubReasonCode(String srCode) {
        this.subReasonCode = srCode;
    }
    
    /**
     * 
     * @return Returns the subReasonDescription
     */
    public String getSubReasonDescription() {
		return subReasonDescription;
	}

    /**
     * subReasonDescription The subReasonDescrption to set.
     * @param subReasonDescription
     */
	public void setSubReasonDescription(String subReasonDescription) {
		this.subReasonDescription = subReasonDescription;
	}

	public String getSubmitAutoCancelBankRqstRefund() {
		return submitAutoCancelBankRqstRefund;
	}

	public void setSubmitAutoCancelBankRqstRefund(
			String submitAutoCancelBankRqstRefund) {
		this.submitAutoCancelBankRqstRefund = submitAutoCancelBankRqstRefund;
	}

	public String getTranSubStatusCode() {
		return tranSubStatusCode;
	}

	public void setTranSubStatusCode(String tranSubStatusCode) {
		this.tranSubStatusCode = tranSubStatusCode;
	}

	public String getSubmitAutoChargebackAndWriteOffLoss() {
		return submitAutoChargebackAndWriteOffLoss;
	}

	public void setSubmitAutoChargebackAndWriteOffLoss(
			String submitAutoChargebackAndWriteOffLoss) {
		this.submitAutoChargebackAndWriteOffLoss = submitAutoChargebackAndWriteOffLoss;
	}

	public String getSubmitRecCcAutoRef() {
		return submitRecCcAutoRef;
	}

	public void setSubmitRecCcAutoRef(String submitRecCcAutoRef) {
		this.submitRecCcAutoRef = submitRecCcAutoRef;
	}

	public String getSubmitRecCcCxlRef() {
		return submitRecCcCxlRef;
	}

	public void setSubmitRecCcCxlRef(String submitRecCcCxlRef) {
		this.submitRecCcCxlRef = submitRecCcCxlRef;
	}

	public String getTranStatusCode() {
		return tranStatusCode;
	}

	public void setTranStatusCode(String tranStatusCode) {
		this.tranStatusCode = tranStatusCode;
	}

	public String getSubmitCancelBankRqstRefund() {
		return submitCancelBankRqstRefund;
	}

	public void setSubmitCancelBankRqstRefund(String submitCancelBankRqstRefund) {
		this.submitCancelBankRqstRefund = submitCancelBankRqstRefund;
	}

	public String getSubmitBankRqstRefund() {
		return submitBankRqstRefund;
	}

	public void setSubmitBankRqstRefund(String submitBankRqstRefund) {
		this.submitBankRqstRefund = submitBankRqstRefund;
	}

	public String getSubmitCancelTransaction() {
		return submitCancelTransaction;
	}

	public void setSubmitCancelTransaction(String submitCancelTransaction) {
		this.submitCancelTransaction = submitCancelTransaction;
	}

	public String getSubmitManCancelTran() {
		return submitManCancelTran;
	}

	public void setSubmitManCancelTran(String submitManCancelTran) {
		this.submitManCancelTran = submitManCancelTran;
	}
	
	
	

}
