/*
 * Created on Mar 9, 2005
 *
 */
package emgadm.transqueue;

import java.text.MessageFormat;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.model.UserProfile;
import emgadm.services.ServiceFactory;
import emgadm.services.TransactionService;

/**
 * @author A131
 *
 */
public class CollectFeeAction extends ManualAdjustmentBaseAction {
	protected ActionForward processAdjustment(
		ActionMapping mapping,
		ManualAdjustmentForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws
			NumberFormatException,
			TransactionAlreadyInProcessException,
			TransactionOwnershipException {
				
		UserProfile userProfile = getUserProfile(request);
		TransactionService tranService =
			ServiceFactory.getInstance().getTransactionService();
		tranService.collectFee(
			Integer.parseInt(form.getTranId()),
			Float.parseFloat(form.getAmount()),
			userProfile.getUID());
		return mapping.findForward(FORWARD_SUCCESS);
	}

	protected void getCommandDescription(HttpServletRequest request) {
		Locale locale = Locale.getDefault();
		MessageResources msgRes = MessageResources.getMessageResources("emgadm.resources.TransactionConfirmationDescription");
		MessageFormat formatter = new MessageFormat(msgRes.getMessage("confirmation.manual.collect.fee"));
		formatter.setLocale(locale);
		String msgText = formatter.toPattern();
		request.setAttribute("msgText", msgText);
	}
}
