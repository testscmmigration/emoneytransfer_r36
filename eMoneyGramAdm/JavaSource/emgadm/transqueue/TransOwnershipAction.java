/*
 * Created on Jan 25, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.transqueue;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.model.UserProfile;
import emgadm.services.ServiceFactory;
import emgadm.services.TransactionService;
import emgadm.util.StringHelper;
import emgshared.model.Transaction;
import emgshared.model.TransactionStatus;
import emgshared.model.UserActivityType;

/**
 * @author T008
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TransOwnershipAction extends EMoneyGramAdmBaseAction
{
	private static final TransactionService tranService =
		ServiceFactory.getInstance().getTransactionService();

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		ActionErrors errors = new ActionErrors();
		UserProfile up = getUserProfile(request);

		ShowTransQueueForm showTransQueueForm = (ShowTransQueueForm)form;
		request.setAttribute("parentSiteId", showTransQueueForm);

		String act = null;
		if (request.getParameter("action") == null) {
			act = (String) request.getAttribute("action");
		} else {
			act = (String) request.getParameter("action");
		}

		List<Transaction> tranIdList = (List<Transaction>) request.getAttribute("tranIdList");
		List<String> tranIdStringList = new ArrayList<String>();
		
		if(tranIdList == null) {
			String id = null;
			if (request.getParameter("id") == null) {
				id = (String) request.getAttribute("id");
			} else {
				id = (String) request.getParameter("id");
			}
			tranIdStringList.add(id);
		} 
		else {
			String docAction = (String) request.getAttribute("docAction");
			List<TransactionStatus> validStatus = new ArrayList<TransactionStatus>();
			validStatus.add(TransactionStatus.SAVE_TRAN);
			if(docAction.equals("approve") || docAction.equals("pend")) {
				validStatus.add(TransactionStatus.PENDING_NOT_FUNDED);
			}
			for(Transaction tran: tranIdList) {
				for(TransactionStatus status: validStatus) {
					if(tran.getStatus().toString().equals(status.toString())) {
						tranIdStringList.add(String.valueOf(tran.getEmgTranId()));
						break;
					}
				}
			}
		}
		
		for(String id : tranIdStringList) {
			int tranId = Integer.parseInt(id);
			String oldUserId = request.getParameter("oldUserId");
			if (oldUserId == null)
			{
				oldUserId = (String) request.getAttribute("oldUserId");
			}
	
			//  retrieve the transaction record for comparison
			TransactionManager tm = getTransactionManager(request);
			Transaction tran = tm.getTransaction(tranId);
			if ((act.equalsIgnoreCase("TakeOwnership")
				|| act.equalsIgnoreCase("TakeOver"))
				&& !tm.checkASPTran(tran, up.getUID()))
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("error.cannot.own.asp"));
				saveErrors(request, errors);
			}
	
			if (act.equalsIgnoreCase("TakeOwnership")
				&& !StringHelper.isNullOrEmpty(tran.getCsrPrcsUserid()))
			{
				String[] parms = { id, tran.getCsrPrcsUserid()};
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("error.another.user.own", parms));
				saveErrors(request, errors);
			}
	
			if (act.equalsIgnoreCase("TakeOver"))
			{
				if (!tm.canTakeoverFromSysOwner(tran))
				{
					errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError("error.background.job.maybe.in.process"));
					saveErrors(request, errors);
				}
	
				if (tran.getCsrPrcsUserid() != null
					&& !tran.getCsrPrcsUserid().equalsIgnoreCase(oldUserId))
				{
					String[] parms = { id, tran.getCsrPrcsUserid(), oldUserId };
					errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError("error.another.user.takeover", parms));
					saveErrors(request, errors);
				}
			}
	
			if (errors.size() == 0)
			{
				if ((act == null || id == null)
					|| (act.equalsIgnoreCase("TakeOver") && oldUserId == null))
				{
					errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError("error.query.parameters.missing"));
					saveErrors(request, errors);
				} else
				{
					String myUserId = up.getUID();
	
					//  Check take ownership action
					try
					{
						if (act.equalsIgnoreCase("TakeOwnership"))
						{
							tranService.requestOwnership(tranId, myUserId);
						} else if (act.equalsIgnoreCase("TakeOver"))
						{
							tranService.takeOverOwnership(tranId, myUserId);
						} else if (act.equalsIgnoreCase("Release"))
						{
							tranService.releaseOwnership(tranId, myUserId);
						}
					} catch (TransactionAlreadyInProcessException e)
					{
						String[] parm = { id, e.getCurrentOwner()};
						errors.add(
							ActionErrors.GLOBAL_ERROR,
							new ActionError(
								"error.already.actively.being.processed",
								parm));
						saveErrors(request, errors);
					} catch (TransactionOwnershipException e)
					{
						String[] parm = { e.getCurrentOwner(), id };
						errors.add(
							ActionErrors.GLOBAL_ERROR,
							new ActionError(
								"error.already.owned.by.other.user",
								parm));
						saveErrors(request, errors);
					}
					try{
					    String logType = null;
					    
					    if (act.equalsIgnoreCase("TakeOwnership"))
					        logType = UserActivityType.EVENT_TYPE_TRANSACTIONTAKEOWNERSHIP;
						else if (act.equalsIgnoreCase("TakeOver"))
						    logType = UserActivityType.EVENT_TYPE_TRANSACTIONTAKEOWNERSHIP;
						else if (act.equalsIgnoreCase("Release"))
						    logType = UserActivityType.EVENT_TYPE_TRANSACTIONRELEASEOWNERSHIP;
						super.insertActivityLog(this.getClass(),request,null,logType,String.valueOf(tranId));
					} catch (Exception e) {}
				}
			}
		}
		
		request.setAttribute("tranIdList",null);

		if (request.getParameter("documentStatus") != null
				|| request.getAttribute("documentStatus") != null) {
			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_UPDATE_DOCUMENT_STATUS);
		}
		else if (request.getParameter("releaseTransactionDocumentStatus") != null
				|| request.getAttribute("releaseTransactionDocumentStatus") != null) {
			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_RELEASE_TRANSACTIONS_DOCUMENT_STATUS);
		}
		else if (request.getParameter("detail") != null
			|| request.getAttribute("detail") != null) {
			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRANSCTION_DETAIL);
		} 
		else {
			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		}
	}
}
