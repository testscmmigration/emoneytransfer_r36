
package emgadm.transqueue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.owasp.esapi.ESAPI;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.model.ChargebackTransactionHeader;
import emgadm.model.TranAction;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.model.AccountStatus;
import emgshared.model.ConsumerAccount;
import emgshared.model.ConsumerAccountType;
import emgshared.model.ConsumerCreditCardAccount;
import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerProfile;
import emgshared.model.Transaction;
import emgshared.model.TransactionSearchRequest;
import emgshared.model.TransactionStatus;
import emgshared.services.ConsumerAccountService;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ScoringService;
import emgshared.services.ServiceFactory;
import emgshared.util.DateFormatter;

/**
 * @author Jawahar Varadhan
 *
 */
public class CcChargebackTrackingAction extends EMoneyGramAdmBaseAction
{
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm actionForm,
			HttpServletRequest request,
			HttpServletResponse response)
			throws Exception{

			DateFormatter sdf = new DateFormatter ("dd/MMM/yyyy hh:mm:ss", true);
			DateFormatter sdfYear = new DateFormatter ("yyyy", true);
			DateFormatter simpleDateFormat = new DateFormatter ("dd/MMM/yyyy", true);
			String tranId = null;
			if ( request.getParameter("tranId") != null )
			{
				tranId = (String) request.getParameter("tranId");  //MBO-4895
			    //tranId = (String) request.getParameter("tranId");9989
				if(tranId.matches("[0-9]+")) { //MBO-9993
			    request.getSession().setAttribute("ccTrackingTranId",tranId);
				}
			}
			else
			    tranId = (String) request.getSession().getAttribute("ccTrackingTranId");

//			ActionErrors errors = new ActionErrors();
			CcChargebackTrackingForm form = (CcChargebackTrackingForm) actionForm;
			UserProfile up = getUserProfile(request);

			TransactionManager tm = getTransactionManager(request);
	        ConsumerProfileService profileService = ServiceFactory.getInstance().getConsumerProfileService();
	        ConsumerAccountService accountService = ServiceFactory.getInstance().getConsumerAccountService();
	        ChargebackTransactionHeader header = new  ChargebackTransactionHeader();


			if (!StringHelper.isNullOrEmpty(form.getSubmitSaveComment()) ||
				!StringHelper.isNullOrEmpty(form.getSubmitSaveCase())    ||
                !StringHelper.isNullOrEmpty(form.getSubmitSaveHeader())  ){

				//Inserting a new Comment
				if (!StringHelper.isNullOrEmpty(form.getSubmitSaveComment())){
				  tm.insertChargebackTranComment(up.getUID(), Integer.valueOf(tranId), form.getChgbkCmntText());
                  request.getSession().setAttribute("chargebackComments", tm.getChargebackComments(up.getUID(), Integer.parseInt(tranId)));
                  form.setChgbkCmntText("");
			      return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
				}

				//Inserting a new Case
				if (!StringHelper.isNullOrEmpty(form.getSubmitSaveCase())){
					  tm.insertChargebackProviderCase(up.getUID(), Integer.parseInt(tranId),
					  		form.getChgbkDenialCode(), form.getChgbkPrvdrCaseId(),
					  		simpleDateFormat.parse(form.getChgbkOrgnStmtDate()));
					  request.getSession().setAttribute("chargebackProviderCases", tm.getChargebackProviderCase(up.getUID(), Integer.parseInt(tranId)));
					  form.setChgbkDenialCode("");
					  form.setChgbkPrvdrCaseId("");
					  form.setChgbkOrgnStmtDate("");
				      return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
				}

				//Inserting or Updating header record
				if (!StringHelper.isNullOrEmpty(form.getSubmitSaveHeader())){
					  if (form.getChgbkTrkDate() == null || form.getChgbkTrkDate().equals(""))
						  tm.insertUpdateChargebackTransaction(up.getUID(), Integer.parseInt(tranId),
						  		form.getChgbkStatCode(), null,
								form.getGndrCode(), form.getCustCntctMhtdCode(), form.getEmailCnstntFlag(),
								form.getPhNbrVerfdFlag(), form.getCustVrblCntctFlag(),
								form.getUnsolicitPhCallFlag(), form.getCustPrflVldnFlag(),
								form.getSndRcvMileQty(), form.getLinkedCustPrflCnt());
					  else
						  tm.insertUpdateChargebackTransaction(up.getUID(), Integer.parseInt(tranId),
						  		form.getChgbkStatCode(), simpleDateFormat.parse(form.getChgbkTrkDate()),
								form.getGndrCode(), form.getCustCntctMhtdCode(), form.getEmailCnstntFlag(),
								form.getPhNbrVerfdFlag(), form.getCustVrblCntctFlag(),
								form.getUnsolicitPhCallFlag(), form.getCustPrflVldnFlag(),
								form.getSndRcvMileQty(), form.getLinkedCustPrflCnt());

			          header = tm.getChargebackTransactionHeader(up.getUID(), Integer.parseInt(tranId));
					  request.getSession().setAttribute("chargebackHeader", header);
					  request.getSession().setAttribute("chargebackActions", tm.getChargebackActions(up.getUID(), Integer.parseInt(tranId)));
					  form.setChgbkDenialCode("");
					  form.setChgbkPrvdrCaseId("");
					  form.setChgbkOrgnStmtDate("");
					  form.setChgbkCreateDate(header.getCreateDate());
				      return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
				}

			}else{
				Transaction tran = null;
				ConsumerProfile profile = null;

				tran =	tm.getTransaction(Integer.parseInt(tranId));
				request.getSession().setAttribute("tran", tran);
				request.getSession().setAttribute("sndTranDate", sdf.format(tran.getSndTranDate()));

				profile = profileService.getConsumerProfile(Integer.valueOf(tran.getSndCustId()), up.getUID(), null);
				request.getSession().setAttribute("profile", profile);
				request.getSession().setAttribute("birthdate", sdfYear.format(profile.getBirthdate()));
				request.getSession().setAttribute("profileCreateDate", sdf.format(profile.getCreateDate()));

				StringBuilder emailAddresses = new StringBuilder();
				for (Iterator iter = profile.getEmails().iterator(); iter.hasNext();) {
					if (!emailAddresses.toString().equals(""))
						emailAddresses.append(", ");

					ConsumerEmail email = (ConsumerEmail) iter.next();
					emailAddresses.append(email.getConsumerEmail());
		        }
				request.getSession().setAttribute("emailAddresses", emailAddresses.toString());
				int numberOfPhoneNumbers = 0;
				String phoneState = "N/A";
				String phoneStateAlt = "N/A";
				ScoringService ss = ServiceFactory.getInstance().getScoringService();
				if (profile.getPhoneNumber() != null)
				{
				    numberOfPhoneNumbers++;
				    try {
					    String state = ss.getStateForAreaCode(up.getUID(),"USA",profile.getPhoneNumber().substring(0,3));
					    if (state.equals(""))
					        state = "Unknown";
					    phoneState = profile.getPhoneNumber().substring(0,3)  + " - " + state;
                    } catch (Exception ignore) {
                        phoneState = "N/A";
                    }
				}
				if (profile.getPhoneNumberAlternate()!= null)
				{
				    numberOfPhoneNumbers++;
				    try {
				        String state = ss.getStateForAreaCode(up.getUID(),"USA",profile.getPhoneNumberAlternate().substring(0,3));
				        if (state.equals(""))
					        state = "Unknown";
					    phoneStateAlt = profile.getPhoneNumberAlternate().substring(0,3) + " - " + state;
                    } catch (Exception ignore) {
                        phoneStateAlt = "N/A";
                    }
				}
				
				//code changed for 2557
				if(profile!=null && profile.getAddress()!=null && profile.getAddress().getState()!=null)
					request.getSession().setAttribute("senderState", profile.getAddress().getState());
				else
					request.getSession().setAttribute("senderState", " ");
				//ended
				request.getSession().setAttribute("numberOfPhoneNumbers", String.valueOf(numberOfPhoneNumbers));
				if(isPhoneStateValid(phoneState)){
				request.getSession().setAttribute("phoneState", phoneState);//9989
				}
				if(isPhoneStateValid(phoneStateAlt)){
				request.getSession().setAttribute("phoneStateAlt", phoneStateAlt);//9989
				}
				TransactionSearchRequest tsr = new TransactionSearchRequest();
				tsr.setConsumerId(Integer.valueOf(tran.getSndCustId()));
				request.getSession().setAttribute("previousTransactions", Integer.valueOf(tm.getTransactions(tsr, up.getUID()).size()));

		        Set accounts = accountService.getAllAccountsByConsumerId(tran.getSndCustId());
		        ArrayList distinctCcAccounts = new ArrayList();

				int failedCcAccounts = 0;

		        for (Iterator iter = accounts.iterator(); iter.hasNext();) {
		            ConsumerAccount account = (ConsumerAccount) iter.next();
		            ConsumerAccountType type = account.getAccountType();

		            if (type.isCreditCardAccount()) {
		            	ConsumerCreditCardAccount ccAccount = (ConsumerCreditCardAccount) account;

		            	if (ccAccount.getStatusCode().equals(AccountStatus.ACTIVE_CODE)) {
		            		String ccHashValue = ccAccount.getAccountNumberHash();
		            		if (!distinctCcAccounts.contains(ccHashValue)){
		            			distinctCcAccounts.add(ccHashValue);
		            		}
		            	}
		            	else{
			            	if (ccAccount.getSubStatusCode().equals(AccountStatus.NON_ACTIVE_FAILURE))
			            		failedCcAccounts++;
		            	}
		            }

		        }

		        request.getSession().setAttribute("distinctCCs", Integer.valueOf(distinctCcAccounts.size()));
		        request.getSession().setAttribute("failedCCs", Integer.valueOf(failedCcAccounts));

		        String approvedUser = new String();
		        String everPended = "No";

		        ArrayList tranActions = (ArrayList)tm.getTranActions(Integer.parseInt(tranId),up.getUID());
		        for (Iterator iter = tranActions.iterator(); iter.hasNext();) {
		            TranAction tranAction = (TranAction) iter.next();

		            if (tranAction.getTranOldStatCode()!= null && tranAction.getTranStatCode() != null){
			            if (!tranAction.getTranOldStatCode().equals(tranAction.getTranStatCode())){
			            	if (tranAction.getTranStatCode().equals(TransactionStatus.APPROVED_CODE)){
			            		approvedUser = tranAction.getTranCreateUserid();
			            	}
			            }
			            if (tranAction.getTranStatCode().equals(TransactionStatus.PENDING_CODE)){
		            		everPended = "Yes";
			            }
		            }
		        }

		        request.getSession().setAttribute("approvedUser", approvedUser);
		        request.getSession().setAttribute("everPended", everPended);

		        request.getSession().setAttribute("chargebackStatuses", tm.getChargebackStatuses());
		        request.getSession().setAttribute("genderTypes", tm.getGenderTypes());
		        request.getSession().setAttribute("customerContactMethodTypes", tm.getCustomerContactMethodTypes());
		        request.getSession().setAttribute("chargebackDenialTypes", tm.getChargebackDenialTypes());


		        // ================== Chargeback Header Information
		        header = tm.getChargebackTransactionHeader(up.getUID(), Integer.parseInt(tranId));
		        if (header != null){
			        form.setChgbkStatCode(header.getChargebackStatusCode());
			        if (header.getChargebackTrackingDate() != null)
			        	form.setChgbkTrkDate(simpleDateFormat.format(header.getChargebackTrackingDate()));
			        form.setCustCntctMhtdCode(header.getCustomerContactMethodCode());
			        form.setCustPrflVldnFlag(header.getCustomerProfileValidationFlag());
			        form.setCustVrblCntctFlag(header.getCustomerVerbalContactFlag());
			        form.setEmailCnstntFlag(header.getEmailConsistentFlag());
			        form.setGndrCode(header.getGenderCode());
			        form.setLinkedCustPrflCnt(Integer.toString(header.getLinkedCustomerProfiles()));
			        form.setPhNbrVerfdFlag(header.getPhoneNumberVerifiedFlag());
			        form.setSndRcvMileQty(Integer.toString(header.getSenderReceiverDistance()));
			        form.setUnsolicitPhCallFlag(header.getUnSolicitPhoneCallFlag());
			        form.setChgbkCreateDate(header.getCreateDate());
		        }
				  request.getSession().setAttribute("chargebackHeader", header);
		        // ================== END - Chargeback Header Information

		        // ================== Chargeback Comment History =============
		        request.getSession().setAttribute("chargebackComments", tm.getChargebackComments(up.getUID(), Integer.parseInt(tranId)));
		        // ================== END - Chargeback Comment History =======

		        // ================== Chargeback Actions History =============
		        request.getSession().setAttribute("chargebackActions", tm.getChargebackActions(up.getUID(), Integer.parseInt(tranId)));
		        // ================== END - Chargeback Actions History =======

		        // ================== Chargeback Case History =============
		        request.getSession().setAttribute("chargebackProviderCases", tm.getChargebackProviderCase(up.getUID(), Integer.parseInt(tranId)));
		        // ================== END - Chargeback Case History =======
			}

	        return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

//	private void destroySessionVariables(HttpServletRequest request){
//		request.getSession().removeAttribute("tran");
//		request.getSession().removeAttribute("sndTranDate");
//		request.getSession().removeAttribute("profile");
//		request.getSession().removeAttribute("birthdate");
//		request.getSession().removeAttribute("profileCreateDate");
//		request.getSession().removeAttribute("emailAddresses");
//		request.getSession().removeAttribute("previousTransactions");
//        request.getSession().removeAttribute("distinctCCs");
//        request.getSession().removeAttribute("failedCCs");
//        request.getSession().removeAttribute("approvedUser");
//        request.getSession().removeAttribute("everPended");
//        request.getSession().removeAttribute("chargebackStatuses");
//        request.getSession().removeAttribute("genderTypes");
//        request.getSession().removeAttribute("customerContactMethodTypes");
//        request.getSession().removeAttribute("chargebackComments");
//        request.getSession().removeAttribute("chargebackActions");
//        request.getSession().removeAttribute("chargebackProviderCases");
//	}
	
	//MBO-9993
	private static boolean isPhoneStateValid(String name){
		Matcher matcher = null;
		Pattern pattern = Pattern.compile("[0-9a-zA-Z-_/ ]*"); 
		if (null != name) {
			matcher = pattern.matcher(name);
		}
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}

	}
}
