/*
 * Created on Mar 9, 2005
 *
 */
package emgadm.transqueue;

import java.text.MessageFormat;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import emgadm.dataaccessors.ManagerFactory;
import emgadm.dataaccessors.TransactionManager;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.model.UserProfile;
import emgadm.services.ServiceFactory;
import emgadm.services.TransactionService;
import emgadm.util.StringHelper;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.Transaction;
import emgshared.model.TransactionStatus;

public class ChargeBackupCreditCardAction extends ManualAdjustmentBaseAction {
	protected ActionForward processAdjustment(
		ActionMapping mapping,
		ManualAdjustmentForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws TransactionAlreadyInProcessException, TransactionOwnershipException {

		TransactionManager tm = getTransactionManager(request);
		TransactionService tranService =
			ServiceFactory.getInstance().getTransactionService();
		UserProfile userProfile = getUserProfile(request);
		Float fee = new Float(form.getAmount());

		String callerLoginId = userProfile.getUID();
		int tranId = Integer.valueOf(form.getTranId());

		//  get the previous CC auth confirmation id
		String requestId;
		try {
			requestId = tm.getCCAuthConfId(tranId, "28");
		} catch (Exception e) {
			throw new EMGRuntimeException(e);
		}

		if (!StringHelper.isNullOrEmpty(requestId)) {
			Transaction tran;
			tran =
				ManagerFactory.createTransactionManager().getTransaction(
					tranId);

			//			if (tran.isSendable()) {
			//				tranService.sendPersonToPerson(tranId, callerLoginId, true);
			//				tran =
			//					ManagerFactory.createTransactionManager().getTransaction(
			//						tranId);

			//  Change fund status to FEE
			if (fee != null) {
				tran.setTranSubStatCode(TransactionStatus.FEE_CODE);
				tm.updateTransaction(tran, callerLoginId, null, fee);
			}

			Long effortId=Long.valueOf(1); 
			//  do CC funding and change fund status to CCS
			if (tran.isFundable()) {
				tranService.fundPersonToPerson(
					tranId,
					requestId,
					callerLoginId,
					true,
					request.getContextPath(),
					effortId.longValue());
			}
			//			}
		}

		return mapping.findForward(FORWARD_SUCCESS);
	}

	protected void getCommandDescription(HttpServletRequest request) {

		Locale locale = Locale.getDefault();
		MessageResources msgRes = MessageResources.getMessageResources("emgadm.resources.TransactionConfirmationDescription");
		MessageFormat formatter = new MessageFormat(msgRes.getMessage("confirmation.charge.backup.credit.card"));
		formatter.setLocale(locale);
		String msgText = formatter.toPattern();
		request.setAttribute("msgText", msgText);
	}
}
