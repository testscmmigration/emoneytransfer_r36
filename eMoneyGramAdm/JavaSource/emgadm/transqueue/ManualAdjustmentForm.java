package emgadm.transqueue;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class ManualAdjustmentForm extends EMoneyGramAdmBaseValidatorForm
{
	private String tranId;
	private String amount;
	private String tranFaceAmt;
	private String tranSendAmt;
	private String recoverType;
	private String subReasonCode;
	private String submitRecord;
	private String submitCancel;
	private String subReasonRequired;
	
	public String getTranId()
	{
		return tranId;
	}

	public void setTranId(String string)
	{
		tranId = string;
	}

	public String getAmount()
	{
		return amount;
	}

	public void setAmount(String string)
	{
		amount = string;
	}

	public String getTranFaceAmt()
	{
		return tranFaceAmt;
	}

	public void setTranFaceAmt(String string)
	{
		tranFaceAmt = string;
	}

	public String getRecoverType()
	{
		return recoverType;
	}

	public void setRecoverType(String string)
	{
		recoverType = string;
	}

	public String getSubmitRecord()
	{
		return submitRecord;
	}

	public void setSubmitRecord(String string)
	{
		submitRecord = string;
	}

	public String getSubmitCancel()
	{
		return submitCancel;
	}

	public void setSubmitCancel(String string)
	{
		submitCancel = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		recoverType = "";
		subReasonCode = "";
	}
	
	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors;
		errors = super.validate(mapping, request);
		errors = errors == null ? new ActionErrors() : errors;

		if (!StringHelper.isNullOrEmpty(submitRecord))
		{
//			float tmpAmt = 0F;
			if ( (StringHelper.isNullOrEmpty(subReasonCode)) &&
			     (this.getSubReasonRequired().equalsIgnoreCase("Y")) )
			{
				errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("errors.required", "Sub-Reason Type"));
			}

			if (StringHelper.isNullOrEmpty(amount))
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("errors.required", "Amount"));
			} else
			{
				try
				{
					Float.parseFloat(amount);
				} catch (Exception e)
				{
					errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError("errors.float", "Amount"));
				}
			}

		/*	if (errors.size() == 0 && StringHelper.isNullOrEmpty(recoverType) && addCheckBox)
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("error.must.select.recovery.type"));
			}
		*/
		}

		return errors;
	}
    /**
     * @return Returns the subReasonCode.
     */
    public String getSubReasonCode() {
        return subReasonCode;
    }
    /**
     * @param subReasonCode The subReasonCode to set.
     */
    public void setSubReasonCode(String srCode) {
        this.subReasonCode = srCode;
    }
    /**
     * @return Returns the tranSendAmt.
     */
    public String getTranSendAmt() {
        return tranSendAmt;
    }
    /**
     * @param tranSendAmt The tranSendAmt to set.
     */
    public void setTranSendAmt(String tsa) {
        this.tranSendAmt = tsa;
    }
    /**
     * @return Returns the subReasonRequired.
     */
    public String getSubReasonRequired() {
        return subReasonRequired;
    }
    /**
     * @param subReasonRequired The subReasonRequired to set.
     */
    public void setSubReasonRequired(String srr) {
        this.subReasonRequired = srr;
    }
}
