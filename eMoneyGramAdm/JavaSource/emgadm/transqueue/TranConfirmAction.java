package emgadm.transqueue;

import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.model.Transaction;
import emgshared.model.TransactionScore;
import emgshared.services.ScoringService;
import emgshared.services.ServiceFactory;

public class TranConfirmAction extends EMoneyGramAdmBaseAction {
	
	private static Logger LOGGER = EMGSharedLogger.getLogger(
			TranConfirmAction.class); 
	
	public ActionForward execute(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		TransactionManager tm = getTransactionManager(request);
		Transaction tran = null;
		ActionErrors errors = new ActionErrors();
		TranConfirmForm form = (TranConfirmForm) actionForm;
		UserProfile up = getUserProfile(request);
		boolean exitFlag = false;
		// User selects to cancel the action for the transaction
		if (!StringHelper.isNullOrEmpty(form.getSubmitCancel())) {
			request.setAttribute("confirm", "cancel");
			exitFlag = true;
		}
		// User selects to go ahead to perform the action
		if (!StringHelper.isNullOrEmpty(form.getSubmitOk())) {
			// retrieve the transaction record for comparison
			tran = tm.getTransaction(Integer.parseInt(form.getTranId()));
		    //FIXME S26-below 'if' is encountering null pointer
			Logger log = EMGSharedLogger.getLogger(this.getClass().getName().toString());
			int tranId = 0;
			if (tran != null) {
				tranId = tran.getEmgTranId();
			}
			if (up == null) {
				log.error("S26-TranConfirmAction 'up' is null;tid="+tranId);
			} else if (up.getUID()==null){				
				log.error("S26-TranConfirmAction 'up.UID' is null;tid="+tranId);
			} else if (tran == null) {
				log.error("S26-TranConfirmAction 'tran' is null;tid="+tranId);
			} else if (tran.getCsrPrcsUserid()==null) {
				log.error("S26-TranConfirmAction 'tran.CsrPrcsUserid' is null;tid="+tranId); 
			}				
            //*****************************************************************
			if (!tran.getCsrPrcsUserid().equalsIgnoreCase(up.getUID())) {
				String[] parms = { form.getTranId(), tran.getCsrPrcsUserid() };
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.no.longer.own", parms));
				saveErrors(request, errors);
				request.setAttribute("confirm", "cancel");
			} else {
				request.setAttribute("confirm", "ok");
			}
			if ("Y".equalsIgnoreCase(form.getShowScore()) && tran.getTranScore() != null) {
				String oldRvwCode = tran.getTranScore().getScoreRvwCode();
				oldRvwCode = oldRvwCode == null ? "" : oldRvwCode.trim();
				String oldCmnt = tran.getTranScore().getTranScoreCmntText();
				oldCmnt = oldCmnt == null ? "" : oldCmnt.trim();
				String newRvwCode = form.getScoreRvwCode();
				newRvwCode = newRvwCode == null ? "" : newRvwCode.trim();
				String newCmnt = form.getTranScoreCmntText().replaceAll("\\<.*?>","");
				newCmnt = newCmnt == null ? "" : newCmnt.trim();
				if (!oldRvwCode.equals(newRvwCode) || !oldCmnt.equals(newCmnt)) {
					TransactionScore ts = new TransactionScore();
					ts.setEmgTranId(tran.getEmgTranId());
					ts.setScoreCnfgId(tran.getTranScore().getScoreCnfgId());
					ts.setTranScoreNbr(tran.getTranScore().getTranScoreNbr());
					ts.setSysAutoRsltCode(tran.getTranScore().getSysAutoRsltCode());
					ts.setScoreRvwCode(newRvwCode);
					ts.setTranScoreCmntText(newCmnt);
					ScoringService ss = ServiceFactory.getInstance().getScoringService();
					ss.setEmgTranScore(up.getUID(), ts);
				}
			}
			// MBO-14172: Removing OSL 'Score&Decision' Actimize profile only call
			/*boolean approvetransaction = ((!StringHelper.isNullOrEmpty(form
                    .getSubmitApproveProcess()) ? true : false) || (!StringHelper.isNullOrEmpty(form
                    .getSubmitApprove()) ? true : false));
			//EMT-2469 Mannual processing approval throu actimize
			if(approvetransaction && (form.getScoreRvwCode() != null
				      && form.getScoreRvwCode().trim().equals("")
				      || form.getScoreRvwCode().trim()
				          .equalsIgnoreCase("APRV")) && tran.getTranAppVersionNumber() >= 3.0){
				request.setAttribute("dispatch"," ");
				 updateActimizeApproval(tm, tran, up, tranId);
			}*/
			exitFlag = true;
		}
		if (exitFlag) {
			if (form.getReleaseOwnership().equalsIgnoreCase("on")) {
				request.setAttribute("id", form.getTranId());
				request.setAttribute("oldUserId", up.getUID());
				request.setAttribute("action", "Release");
				request.setAttribute("detail", "Y");
			}
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRANSCTION_DETAIL);
		}
		// set up the display message for the user to confirm
		String msgText = null;
		form.setShowScore("N");
		Locale locale = Locale.getDefault();
		MessageResources msgRes = MessageResources.getMessageResources("emgadm.resources.TransactionConfirmationDescription");
		MessageFormat formatter = null;
		if (!StringHelper.isNullOrEmpty(form.getSubmitMoveToPending())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.move.to.pending"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Move transaction - '" + form.getTranId() + "' to Pending status");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitApprove())) {
			if ((!StringHelper.isNullOrEmpty(form.getSysAutoRsltCode())) && (up.hasPermission("showTranScoreDetail"))) {
				form.setShowScore("Y");
			}
			formatter = new MessageFormat(msgRes.getMessage("confirmation.approve"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Approve transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitApproveProcess())) {
			if (!StringHelper.isNullOrEmpty(form.getSysAutoRsltCode())) {
				form.setShowScore("Y");
			}
			formatter = new MessageFormat(msgRes.getMessage("confirmation.approve.process"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Approve and process transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitUndoApprove())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.undo.approve"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Undo approve transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitUndoDeny())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.undo.deny"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Undo Deny transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitProcess())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.process"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Process transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecAchAutoRef())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.record.ach.auto.refund"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Automatic Cancel/Refund ACH transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecAchCxlRef())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.record.ach.cancel.refund"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Manual Cancel/Refund ACH transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecAchRefundReq())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.record.ach.refund.request"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Record ACH Refund Requested transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecCcAutoRef())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.record.cc.auto.refund"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Automatic Cancel/Refund credit card transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecCcCxlRef())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.record.cc.cancel.refund"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Manual Cancel/Refund credit card transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecCcRefundReq())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.record.cc.refund.request"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Record Credit Card Refund Requested transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitAutoWriteOffLoss())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.auto.write.off.loss"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Automatic write-off loss for transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitManWriteOffLoss())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.manual.write.off.loss"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Manual write-off loss for transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecordMgCancelRefund())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.record.mg.cancel.refund"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Record MG transaction Cancel/Refund for transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitAutoChargebackAndWriteOffLoss())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.auto.chargeback.and.write.off.loss"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Automatic Chargeback and Write-Off Loss for transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitAutoExceptionWOL())) {
			  formatter = new MessageFormat(msgRes.getMessage("confirmation.auto.exception.and.WOL"));
			  formatter.setLocale(locale);
			  msgText = formatter.toPattern();
			  form.setActionDesc("Auto Exception and Write-Off Loss for transaction - '" + form.getTranId() + "'"); 
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitManualExceptionWOL())) {
			  formatter = new MessageFormat(msgRes.getMessage("confirmation.manual.exception.and.WOL"));
			  formatter.setLocale(locale);
			  msgText = formatter.toPattern();
			  form.setActionDesc("Manual Exception and Write-Off Loss for transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecordAchChargeback())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.record.ach.chargeback"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Record ACH Return for transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecordCcChargeback())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.record.cc.chargeback"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Record CC Chargeback for transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitCancelAchWaiting())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.cancel.ach.waiting"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Cancel ACH Waiting for transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitEsMGSend())) {
			form.setShowWarning("Y");
			formatter = new MessageFormat(msgRes.getMessage("confirmation.es.mg.send"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Economy Service MoneyGram Send for transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitEsMGSendCancel())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.es.mg.send.cancel"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Cancel Economy Service MoneyGram Send for transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecordTranError())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.record.tran.error"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Record Transaction Error for transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitReleaseTransaction())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.release.transaction"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Release Transaction for transaction - '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitRetrievalRequest())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.retrieval.request"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Perform a Retrieval Request for transaction - '" + form.getTranId() + "'");		
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitCancelBankRqstRefund())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.cancel.bank.rqst.refund"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Manual Cancel/Refund Bank Payment (Telecheck) transaction -  '" + form.getTranId() + "'");
		
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitBankRqstRefund())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.bank.rqst.refund"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Request Refund Bank Payment (Telecheck) transaction -  '" + form.getTranId() + "'");
				
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitAutoCancelBankRqstRefund())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.auto.cancel.bank.rqst.refund"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("AUTO Cancel/Refund Bank Payment (Telecheck) transaction -  '" + form.getTranId() + "'");
				
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitBankRepresentRejDebit())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.bank.represent.rej.debit"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Use to Represent Bank(Telecheck) Rejected Debit -  '" + form.getTranId() + "'");
				
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitBankRejectedDebit())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.bank.rejected.debit"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Use if Bank(Telecheck) Rejected Debit  '" + form.getTranId() + "'");
		
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitBankRecordVoid())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.bank.record.void"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("Use if Bank(Telecheck) indicated txn not send to customer bank (voided) -  '" + form.getTranId() + "'");
		
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitBankIssueRefund())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.bank.issue.refund"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("If Bank(Telecheck) txn cleared and enough days passed; it is ok to send adjustment -  '" + form.getTranId() + "'");
		
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitBankWriteOffLoss())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.bank.write.off.loss"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("If Bank(Telecheck) reached max retry attempts for NSF; Write txn off. -  '" + form.getTranId() + "'");
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitMoveToReview())) {
			formatter = new MessageFormat(msgRes.getMessage("confirmation.move.to.review"));
			formatter.setLocale(locale);
			msgText = formatter.toPattern();
			form.setActionDesc("This will move transaction to Review from OCR status");
		} else if((!StringHelper.isNullOrEmpty(form.getSubmitCcManualRefund())) || 
				(!StringHelper.isNullOrEmpty(form.getSubmitCcManualVoid())) || 
				(!StringHelper.isNullOrEmpty(form.getSubmitUndoDSError()))){
			request.setAttribute("confirm","ok");
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRANSCTION_DETAIL);
		}
		
		request.getSession().setAttribute("msgText", msgText);
		tran = tm.getTransaction(Integer.parseInt(form.getTranId()));
		
		//added by Ankit Bhatt for 285
		if(tran.getTranScore()!= null && tran.getTranScore().getScoreCnfgId()>0)
			form.setShowScore("N");		
		//ended
		
		request.getSession().setAttribute("tran", tran);
		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
	
	/**
	 * Actimize Manual processing approval 
	 * @param tm
	 * @param tran
	 * @param up
	 * @param tranId
	 * @throws DataSourceException
	 * @throws SQLException
	 */
	/*private void updateActimizeApproval(TransactionManager tm,
			Transaction tran, UserProfile up, int tranId)
			throws DataSourceException, SQLException{
		LOGGER.info("Inside update actimize method");
		ScoreAndDecisionRequest actimizeRequest = new ScoreAndDecisionRequest();
		actimizeRequest.setTransactionId(String.valueOf(tranId));
		try {
			OSLProxy oslProxy = new OSLProxyImpl();
			oslProxy.getActimizeScoreAndDecision(actimizeRequest);
		} catch (OSLException exception) {
			LOGGER.error("updating actimize comments failed for tran id :: "+tranId,exception);
			tm.setTransactionComment(
					tranId,
					Constants.TRAN_COMMENT_REASON_CODE_OTHERS,
					Constants.ACTIMIZE_FAILED_STATEMENT,
					up.getUID());
		}
	}*/
	
}