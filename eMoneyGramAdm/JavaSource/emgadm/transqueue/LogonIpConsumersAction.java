package emgadm.transqueue;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;

/**
 * @version 	1.0
 * @author
 */
public class LogonIpConsumersAction extends EMoneyGramAdmBaseAction
{

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		LogonIpConsumersForm form = (LogonIpConsumersForm) f;
		TransactionManager tm = getTransactionManager(request);
		List list = tm.getLogonIpConsumerList(form.getIpAddress(), "WEB");
		request.setAttribute("ipConsumerList", list);
		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
