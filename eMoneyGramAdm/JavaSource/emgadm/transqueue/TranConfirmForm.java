package emgadm.transqueue;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class TranConfirmForm extends EMoneyGramAdmBaseValidatorForm {
	private static final String approveCode = "APRV";
	private static final String approveDesc = "Approve";
	private static final String rejectCode = "REJ";
	private static final String rejectDesc = "Reject";
	private static final String reviewCode = "REVW";
	private static final String reviewDesc = "Review";

	private String tranId;
	private String custId;
	private String releaseOwnership;
	private String actionDesc;
	private String showWarning;
	private String submitOk;
	private String submitCancel;

	private String submitMoveToPending;
	private String submitApprove;
	private String submitApproveProcess;
	private String submitUndoApprove;
	private String submitUndoDeny;
	private String submitProcess;
	private String submitRecAchAutoRef;
	private String submitRecAchCxlRef;
	private String submitRecAchRefundReq;
	private String submitRecCcAutoRef;
	private String submitRecCcCxlRef;
	private String submitRecCcRefundReq;
	private String submitManWriteOffLoss;
	private String submitRecordMgCancelRefund;
	private String submitRecordAchChargeback;
	private String submitRecordCcChargeback;
	private String submitCancelAchWaiting;
	private String submitAutoWriteOffLoss;
	private String submitAutoChargebackAndWriteOffLoss;
	private String submitAutoExceptionWOL;
	private String submitManualExceptionWOL;
	private String submitRecoveryOfLoss;
	private String submitEsMGSend;
	private String submitEsMGSendCancel;
	private String submitRecordTranError;
	private String submitReleaseTransaction;
	private String submitRetrievalRequest;
	private String submitCancelBankRqstRefund;
	private String submitBankRqstRefund;
	private String submitAutoCancelBankRqstRefund;
	private String submitBankRepresentRejDebit;
	private String submitBankRejectedDebit;
	private String submitBankRecordVoid;
	private String submitBankIssueRefund;
	private String submitBankWriteOffLoss;
	private String submitCcManualRefund;
	private String submitCcManualVoid;
	private String submitUndoDSError;
	private String submitCancelTransaction;
	private String submitManCancelTran;
	private String submitMoveToReview;

	private String tranStatusCode;
	private String tranStatusDesc;
	private String tranSubStatusCode;
	private String tranSubStatusDesc;
	private String tranOwnerId;
	private String tranSenderName;
	private String tranReceiverName;
	private String tranFaceAmt;
	private String tranSendFee;
	private String tranReturnFee;
	private String tranSendAmt;
	private String tranType;
	private String tranTypeDesc;
	private String tranSendCountry;
	private String tranRecvCountry;
	private String tranPrimAcctType;
	private String tranPrimAcctMask;
	private String tranSecAcctType;
	private String tranSecAcctMask;
	private String tranCustIP;
	private String sendCustAcctId;
	private String sendCustBkupAcctId;
	private String tranCcAuthCode;
	private String tranSndMsg1Text;
	private String tranSndMsg2Text;
	private String esMGSendFlag;
	private int transScores;
	private String sysAutoRsltCode;
	private String scoreRvwCode;
	private String tranScoreCmntText;
	private String showScore;

	public String getTranId() {
		return tranId;
	}

	public void setTranId(String string) {
		tranId = string;
	}

	public String getReleaseOwnership() {
		return releaseOwnership;
	}

	public void setReleaseOwnership(String s) {
		releaseOwnership = s;
	}

	public String getActionDesc() {
		return actionDesc;
	}

	public void setActionDesc(String string) {
		actionDesc = string;
	}

	public String getSubmitOk() {
		return submitOk;
	}

	public void setSubmitOk(String string) {
		submitOk = string;
	}

	public String getSubmitCancel() {
		return submitCancel;
	}

	public void setSubmitCancel(String string) {
		submitCancel = string;
	}

	public String getSubmitApprove() {
		return submitApprove;
	}

	public void setSubmitApprove(String string) {
		submitApprove = string;
	}

	public String getSubmitUndoApprove() {
		return submitUndoApprove;
	}

	public void setSubmitUndoApprove(String string) {
		submitUndoApprove = string;
	}

	public String getSubmitUndoDeny() {
		return submitUndoDeny;
	}

	public void setSubmitUndoDeny(String string) {
		submitUndoDeny = string;
	}

	public String getSubmitProcess() {
		return submitProcess;
	}

	public void setSubmitProcess(String string) {
		submitProcess = string;
	}

	public String getTranStatusCode() {
		return tranStatusCode == null ? "" : tranStatusCode;
	}

	public void setTranStatusCode(String string) {
		tranStatusCode = string;
	}

	public String getTranStatusDesc() {
		return tranStatusDesc == null ? "" : tranStatusDesc;
	}

	public void setTranStatusDesc(String string) {
		tranStatusDesc = string;
	}

	public String getTranSubStatusCode() {
		return tranSubStatusCode == null ? "" : tranSubStatusCode;
	}

	public void setTranSubStatusCode(String string) {
		tranSubStatusCode = string;
	}

	public String getTranSubStatusDesc() {
		return tranSubStatusDesc == null ? "" : tranSubStatusDesc;
	}

	public void setTranSubStatusDesc(String string) {
		tranSubStatusDesc = string;
	}

	public String getTranOwnerId() {
		return tranOwnerId == null ? "" : tranOwnerId;
	}

	public void setTranOwnerId(String string) {
		tranOwnerId = string;
	}

	public String getTranSenderName() {
		return tranSenderName == null ? "" : tranSenderName;
	}

	public void setTranSenderName(String string) {
		tranSenderName = string;
	}

	public String getTranReceiverName() {
		return tranReceiverName == null ? "" : tranReceiverName;
	}

	public void setTranReceiverName(String string) {
		tranReceiverName = string;
	}

	public String getTranFaceAmt() {
		return tranFaceAmt;
	}

	public void setTranFaceAmt(String string) {
		tranFaceAmt = string;
	}

	public String getTranSendFee() {
		return tranSendFee;
	}

	public void setTranSendFee(String string) {
		tranSendFee = string;
	}

	public String getTranReturnFee() {
		return tranReturnFee;
	}

	public void setTranReturnFee(String string) {
		tranReturnFee = string;
	}

	public String getTranSendAmt() {
		return tranSendAmt;
	}

	public void setTranSendAmt(String string) {
		tranSendAmt = string;
	}

	public String getTranType() {
		return tranType == null ? "" : tranType;
	}

	public void setTranType(String string) {
		tranType = string;
	}

	public String getTranSendCountry() {
		return tranSendCountry == null ? "" : tranSendCountry;
	}

	public void setTranSendCountry(String string) {
		tranSendCountry = string;
	}

	public String getTranRecvCountry() {
		return tranRecvCountry == null ? "" : tranRecvCountry;
	}

	public void setTranRecvCountry(String string) {
		tranRecvCountry = string;
	}

	public String getTranPrimAcctType() {
		return tranPrimAcctType == null ? "" : tranPrimAcctType;
	}

	public void setTranPrimAcctType(String string) {
		tranPrimAcctType = string;
	}

	public String getTranTypeDesc() {
		return tranTypeDesc == null ? "" : tranTypeDesc;
	}

	public void setTranTypeDesc(String string) {
		tranTypeDesc = string;
	}

	public String getTranPrimAcctMask() {
		return tranPrimAcctMask == null ? "" : tranPrimAcctMask;
	}

	public void setTranPrimAcctMask(String string) {
		tranPrimAcctMask = string;
	}

	public String getTranSecAcctType() {
		return tranSecAcctType == null ? "" : tranSecAcctType;
	}

	public void setTranSecAcctType(String string) {
		tranSecAcctType = string;
	}

	public String getTranSecAcctMask() {
		return tranSecAcctMask == null ? "" : tranSecAcctMask;
	}

	public void setTranSecAcctMask(String string) {
		tranSecAcctMask = string;
	}

	public String getTranCustIP() {
		return tranCustIP == null ? "" : tranCustIP;
	}

	public void setTranCustIP(String string) {
		tranCustIP = string;
	}

	public String getSendCustAcctId() {
		return sendCustAcctId;
	}

	public void setSendCustAcctId(String string) {
		sendCustAcctId = string;
	}

	public String getSendCustBkupAcctId() {
		return sendCustBkupAcctId;
	}

	public String getSubmitMoveToPending() {
		return submitMoveToPending;
	}

	public void setSubmitMoveToPending(String string) {
		submitMoveToPending = string;
	}

	public void setSendCustBkupAcctId(String string) {
		sendCustBkupAcctId = string;
	}

	public String getSubmitRecAchCxlRef() {
		return submitRecAchCxlRef;
	}

	public String getSubmitRecCcCxlRef() {
		return submitRecCcCxlRef;
	}

	public void setSubmitRecAchCxlRef(String string) {
		submitRecAchCxlRef = string;
	}

	public void setSubmitRecCcCxlRef(String string) {
		submitRecCcCxlRef = string;
	}

	public String getSubmitManWriteOffLoss() {
		return submitManWriteOffLoss;
	}

	public void setSubmitManWriteOffLoss(String string) {
		submitManWriteOffLoss = string;
	}

	public String getSubmitRecordAchChargeback() {
		return submitRecordAchChargeback;
	}

	public void setSubmitRecordAchChargeback(String string) {
		submitRecordAchChargeback = string;
	}

	public String getSubmitRecordCcChargeback() {
		return submitRecordCcChargeback;
	}

	public void setSubmitRecordCcChargeback(String string) {
		submitRecordCcChargeback = string;
	}

	public String getTranCcAuthCode() {
		return tranCcAuthCode;
	}

	public void setTranCcAuthCode(String string) {
		tranCcAuthCode = string;
	}

	public String getTranSndMsg1Text() {
		return tranSndMsg1Text;
	}

	public void setTranSndMsg1Text(String string) {
		tranSndMsg1Text = string;
	}

	public String getTranSndMsg2Text() {
		return tranSndMsg2Text;
	}

	public void setTranSndMsg2Text(String string) {
		tranSndMsg2Text = string;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String string) {
		custId = string;
	}

	public String getSubmitRecordMgCancelRefund() {
		return submitRecordMgCancelRefund;
	}

	public void setSubmitRecordMgCancelRefund(String string) {
		submitRecordMgCancelRefund = string;
	}

	public String getSubmitCancelAchWaiting() {
		return submitCancelAchWaiting;
	}

	public void setSubmitCancelAchWaiting(String string) {
		submitCancelAchWaiting = string;
	}

	public String getSubmitRecAchAutoRef() {
		return submitRecAchAutoRef;
	}

	public void setSubmitRecAchAutoRef(String string) {
		submitRecAchAutoRef = string;
	}

	public String getSubmitRecCcAutoRef() {
		return submitRecCcAutoRef;
	}

	public void setSubmitRecCcAutoRef(String string) {
		submitRecCcAutoRef = string;
	}

	public String getSubmitRecAchRefundReq() {
		return submitRecAchRefundReq;
	}

	public void setSubmitRecAchRefundReq(String string) {
		submitRecAchRefundReq = string;
	}

	public String getSubmitRecCcRefundReq() {
		return submitRecCcRefundReq;
	}

	public void setSubmitRecCcRefundReq(String string) {
		submitRecCcRefundReq = string;
	}

	public String getSubmitAutoWriteOffLoss() {
		return submitAutoWriteOffLoss;
	}

	public void setSubmitAutoWriteOffLoss(String string) {
		submitAutoWriteOffLoss = string;
	}

	public String getSubmitAutoChargebackAndWriteOffLoss() {
		return submitAutoChargebackAndWriteOffLoss;
	}

	public void setSubmitAutoChargebackAndWriteOffLoss(String string) {
		submitAutoChargebackAndWriteOffLoss = string;
	}

	public String getSubmitAutoExceptionWOL() {
		return submitAutoExceptionWOL;
	}

	public void setSubmitAutoExceptionWOL(String submitAutoExceptionWOL) {
		this.submitAutoExceptionWOL = submitAutoExceptionWOL;
	}

	public String getSubmitManualExceptionWOL() {
		return submitManualExceptionWOL;
	}

	public void setSubmitManualExceptionWOL(String submitManualExceptionWOL) {
		this.submitManualExceptionWOL = submitManualExceptionWOL;
	}

	public String getSubmitRecoveryOfLoss() {
		return submitRecoveryOfLoss;
	}

	public void setSubmitRecoveryOfLoss(String string) {
		submitRecoveryOfLoss = string;
	}

	public String getSubmitEsMGSend() {
		return submitEsMGSend;
	}

	public void setSubmitEsMGSend(String string) {
		submitEsMGSend = string;
	}

	public String getSubmitEsMGSendCancel() {
		return submitEsMGSendCancel;
	}

	public void setSubmitEsMGSendCancel(String string) {
		submitEsMGSendCancel = string;
	}

	public String getEsMGSendFlag() {
		return esMGSendFlag;
	}

	public void setEsMGSendFlag(String string) {
		esMGSendFlag = string;
	}

	public String getShowWarning() {
		return showWarning == null ? "" : showWarning;
	}

	public void setShowWarning(String string) {
		showWarning = string;
	}

	public String getSubmitRecordTranError() {
		return submitRecordTranError;
	}

	public void setSubmitRecordTranError(String string) {
		submitRecordTranError = string;
	}

	public String getSubmitReleaseTransaction() {
		return submitReleaseTransaction;
	}

	public void setSubmitReleaseTransaction(String string) {
		submitReleaseTransaction = string;
	}

	public int getTransScores() {
		return transScores;
	}

	public void setTransScores(int i) {
		transScores = i;
	}

	public String getSysAutoRsltCode() {
		return sysAutoRsltCode;
	}

	public void setSysAutoRsltCode(String string) {
		sysAutoRsltCode = string;
	}

	public String getScoreRvwCode() {
		return scoreRvwCode;
	}

	public void setScoreRvwCode(String string) {
		scoreRvwCode = string;
	}

	public String getTranScoreCmntText() {
		return tranScoreCmntText;
	}

	public void setTranScoreCmntText(String string) {
		tranScoreCmntText = string;
	}

	public String getShowScore() {
		return showScore;
	}

	public void setShowScore(String string) {
		showScore = string;
	}

	public String getSysAutoRsltDesc() {
		if (approveCode.equalsIgnoreCase(sysAutoRsltCode)) {
			return approveDesc;
		}
		if (rejectCode.equalsIgnoreCase(sysAutoRsltCode)) {
			return rejectDesc;
		}
		if (reviewCode.equalsIgnoreCase(sysAutoRsltCode)) {
			return reviewDesc;
		}
		return "";
	}

	public String getScoreRvwDesc() {
		if (approveCode.equalsIgnoreCase(scoreRvwCode)) {
			return approveDesc;
		}
		if (rejectCode.equalsIgnoreCase(scoreRvwCode)) {
			return rejectDesc;
		}
		if (reviewCode.equalsIgnoreCase(scoreRvwCode)) {
			return reviewDesc;
		}
		return "";
	}

	public String getSubmitApproveProcess() {
		return submitApproveProcess;
	}

	public void setSubmitApproveProcess(String string) {
		submitApproveProcess = string;
	}

	public String getSubmitRetrievalRequest() {
		return submitRetrievalRequest;
	}

	public void setSubmitRetrievalRequest(String string) {
		submitRetrievalRequest = string;
	}

	public String getSubmitCancelBankRqstRefund() {
		return submitCancelBankRqstRefund;
	}

	public void setSubmitCancelBankRqstRefund(String submitCancelBankRqstRefund) {
		this.submitCancelBankRqstRefund = submitCancelBankRqstRefund;
	}

	public String getSubmitBankRqstRefund() {
		return submitBankRqstRefund;
	}

	public void setSubmitBankRqstRefund(String submitBankRqstRefund) {
		this.submitBankRqstRefund = submitBankRqstRefund;
	}

	public String getSubmitAutoCancelBankRqstRefund() {
		return submitAutoCancelBankRqstRefund;
	}

	public void setSubmitAutoCancelBankRqstRefund(
			String submitAutoCancelBankRqstRefund) {
		this.submitAutoCancelBankRqstRefund = submitAutoCancelBankRqstRefund;
	}

	public String getSubmitBankRepresentRejDebit() {
		return submitBankRepresentRejDebit;
	}

	public void setSubmitBankRepresentRejDebit(
			String submitBankRepresentRejDebit) {
		this.submitBankRepresentRejDebit = submitBankRepresentRejDebit;
	}

	public String getSubmitBankRejectedDebit() {
		return submitBankRejectedDebit;
	}

	public void setSubmitBankRejectedDebit(String submitBankRejectedDebit) {
		this.submitBankRejectedDebit = submitBankRejectedDebit;
	}

	public String getSubmitBankRecordVoid() {
		return submitBankRecordVoid;
	}

	public void setSubmitBankRecordVoid(String submitBankRecordVoid) {
		this.submitBankRecordVoid = submitBankRecordVoid;
	}

	public String getSubmitBankIssueRefund() {
		return submitBankIssueRefund;
	}

	public void setSubmitBankIssueRefund(String submitBankIssueRefund) {
		this.submitBankIssueRefund = submitBankIssueRefund;
	}

	public String getSubmitBankWriteOffLoss() {
		return submitBankWriteOffLoss;
	}

	public void setSubmitBankWriteOffLoss(String submitBankWriteOffLoss) {
		this.submitBankWriteOffLoss = submitBankWriteOffLoss;
	}

	public String getSubmitCcManualRefund() {
		return submitCcManualRefund;
	}

	public void setSubmitCcManualRefund(String submitCcManualRefund) {
		this.submitCcManualRefund = submitCcManualRefund;
	}

	public String getSubmitCcManualVoid() {
		return submitCcManualVoid;
	}

	public void setSubmitCcManualVoid(String submitCcManualVoid) {
		this.submitCcManualVoid = submitCcManualVoid;
	}

	public String getSubmitUndoDSError() {
		return submitUndoDSError;
	}

	public void setSubmitUndoDSError(String submitUndoDSError) {
		this.submitUndoDSError = submitUndoDSError;
	}

	public String getSubmitCancelTransaction() {
		return submitCancelTransaction;
	}
	public void setSubmitCancelTransaction(String submitCancelTransaction) {
		this.submitCancelTransaction = submitCancelTransaction;
	}
	public String getSubmitManCancelTran() {
		return submitManCancelTran;
	}
	public void setSubmitManCancelTran(String submitManCancelTran) {
		this.submitManCancelTran = submitManCancelTran;
	}
	public String getSubmitMoveToReview() {
		return submitMoveToReview;
	}
	public void setSubmitMoveToReview(String submitMoveToReview) {
		this.submitMoveToReview = submitMoveToReview;
	}
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		tranId = "";
		releaseOwnership = "off";
		actionDesc = "";
		showWarning = null;
		submitOk = null;
		submitCancel = null;

		submitMoveToPending = null;
		submitApprove = null;
		submitUndoApprove = null;
		submitUndoDeny = null;
		submitProcess = null;
		submitRetrievalRequest = null;
		submitCancelBankRqstRefund = null;
		submitBankRqstRefund = null;
		submitAutoCancelBankRqstRefund = null;
		submitBankRepresentRejDebit = null;
		submitBankRejectedDebit = null;
		submitBankRecordVoid = null;
		submitBankIssueRefund = null;
		submitBankWriteOffLoss = null;
		submitCcManualRefund = null;
		submitCcManualVoid = null;
		submitUndoDSError = null;
		submitCancelTransaction = null;
		submitManCancelTran = null;
		submitMoveToReview = null;

		tranStatusCode = "";
		tranStatusDesc = "";
		tranSubStatusCode = "";
		tranSubStatusDesc = "";
		tranOwnerId = "";
		tranSenderName = "";
		tranReceiverName = "";
		tranFaceAmt = "";
		tranSendFee = "";
		tranReturnFee = "";
		tranSendAmt = "";
		tranType = "";
		tranTypeDesc = "";
		tranSendCountry = "";
		tranRecvCountry = "";
		tranPrimAcctType = "";
		tranPrimAcctMask = "";
		tranSecAcctType = "";
		tranSecAcctMask = "";
		tranCustIP = "";
		sendCustAcctId = "";
		sendCustBkupAcctId = "";
		scoreRvwCode = "";
		tranScoreCmntText = "";

	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);
		if ("Y".equalsIgnoreCase(showScore)
				&& !StringHelper.isNullOrEmpty(submitOk)) {
			if (StringHelper.isNullOrEmpty(scoreRvwCode)) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"errors.required", "Score Review Code"));
			}

			if (StringHelper.isNullOrEmpty(tranScoreCmntText)) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"errors.required", "Score Review Comments"));
			} else if (tranScoreCmntText.length() > 255) {
				String[] parm = { "Score Review Comments", "255" };
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"errors.length.too.long", parm));
			}
		}

		return errors;
	}
}
