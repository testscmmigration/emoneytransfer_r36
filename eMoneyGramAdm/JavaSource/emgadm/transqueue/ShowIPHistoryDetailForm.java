package emgadm.transqueue;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.internaluseradmin.EMoneyGramAdmBaseUserAdminForm;

public class ShowIPHistoryDetailForm extends EMoneyGramAdmBaseUserAdminForm {
	private String logonId;
	private String custId;
	private String ipAddress;

	public String getLogonId() {
		return logonId == null ? "" : logonId.trim();
	}

	public void setLogonId(String string) {
		logonId = string;
	}

	public String getCustId() {
		return custId == null ? "" : custId.trim();
	}

	public void setCustId(String string) {
		custId = string;
	}

	public String getIpAddress() {
		return ipAddress == null ? "" : ipAddress.trim();
	}

	public void setIpAddress(String string) {
		ipAddress = string;
	}

	public void reset(ActionMapping arg0, HttpServletRequest arg1) {
		super.reset(arg0, arg1);
	}
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();
		if (getAdd() != null || getEdit() != null) {
			return super.validate(mapping, request);
		}
		
		return errors;
	}
}
