package emgadm.transqueue;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.model.ChargebackTrackingSearchRequest;
import emgadm.model.ChargebackTransactionHeader;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.util.DateFormatter;

public class ShowChargebackQueueAction extends EMoneyGramAdmBaseAction
{
//	private static EMTSharedDynProperties dynProps =
//		new EMTSharedDynProperties();

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

//		ActionErrors errors = new ActionErrors();
		ShowChargebackQueueForm form = (ShowChargebackQueueForm) f;
		TransactionManager tm = getTransactionManager(request);
		UserProfile up = getUserProfile(request);

		if (StringHelper.isNullOrEmpty(form.getBeginDateText()))
		{
			DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
			long now = System.currentTimeMillis();
			form.setBeginDateText(
				df.format(new Date(now - 7L * 24L * 60L * 60L * 1000L)));
			form.setEndDateText(
				df.format(new Date(now + 24L * 60L * 60L * 1000L)));
		}
		
		ChargebackTrackingSearchRequest searchRequest = new ChargebackTrackingSearchRequest();

		searchRequest.setBeginDateText(form.getBeginDateText());
		searchRequest.setEndDateText(form.getEndDateText());
		searchRequest.setStatus(form.getStatus());

		request.getSession().setAttribute("chargebackStatuses", tm.getChargebackStatuses());
		
		Collection chargebackQueue = tm.getChargebackTrackingQueue(up.getUID(), searchRequest);
		
		if (!StringHelper.isNullOrEmpty(form.getSortBy()))
		{
			ChargebackTransactionHeader.sortBy = form.getSortBy();
		}

		if (form.getSortSeq().equalsIgnoreCase("A"))
		{
			ChargebackTransactionHeader.sortSeq = 1;
		} else
		{
			ChargebackTransactionHeader.sortSeq = -1;
		}
		
		if (chargebackQueue.size() > 1)
		{
			Collections.sort((ArrayList)chargebackQueue);
		}
		
		request.getSession().setAttribute("chargebackQueue", chargebackQueue);
		form.setRecordCount(chargebackQueue.size() + "");
		
		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
