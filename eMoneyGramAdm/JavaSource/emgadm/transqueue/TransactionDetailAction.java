package emgadm.transqueue;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import shared.mgo.dto.MGOAgentProfile;
import shared.mgo.services.AgentConnectAccessor;
import shared.mgo.services.AgentConnectService;
import shared.mgo.services.MGOServiceFactory;

import com.moneygram.agentconnect1305.wsclient.CurrencyInfo;
import com.moneygram.agentconnect1305.wsclient.DetailLookupResponse;
import com.moneygram.common.util.StringUtility;
import com.moneygram.mgo.service.config_v3_4.GetSourceSiteInfoResponse;
import com.moneygram.service.moneygramscoringservice_v3.DecisionAndScoreResponse;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.common.CyberSourceScoring;
import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.exceptions.CreditCardRefundException;
import emgadm.exceptions.InvalidRRNException;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.model.ChargebackTransactionHeader;
import emgadm.model.CustAddress;
import emgadm.model.GlAccount;
import emgadm.model.TranAction;
import emgadm.model.TranActionView;
import emgadm.model.UserProfile;
import emgadm.property.EMTAdmContainerProperties;
import emgadm.property.EMTAdmDynProperties;
import emgadm.security.TranSecurityMatrix;
import emgadm.services.DecryptionService;
import emgadm.services.NotificationAccess;
import emgadm.services.NotificationAccessImpl;
import emgadm.services.ServiceFactory;
import emgadm.services.TransactionScoring;
import emgadm.services.TransactionService;
import emgadm.services.iplookup.melissadata.client.IPLocatorService;
import emgadm.sysmon.EventListConstant;
import emgadm.util.restclient.osl.OSLClientExecutor;
import emgadm.util.restclient.osl.fundandcommit.FundAndCommitResponse;
import emgadm.util.restclient.osl.preparepayment.PreparePaymentResponse;
import emgadm.util.restclient.tps.FundAndCommitResponseVO;
import emgadm.util.restclient.tps.TPSClientExecutor;
import emgadm.util.restclient.tps.capture.TPSCaptureResponse;
import emgshared.cache.locator.EMTSharedSourceSiteCacheService;
import emgshared.cache.locator.EMTSharedSourceSiteCacheServiceImpl;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.dataaccessors.TransactionDAO;
import emgshared.exceptions.AgentConnectException;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.ConsumerAccountType;
import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerStatus;
import emgshared.model.DeliveryOption;
import emgshared.model.ElectronicTypeCode;
import emgshared.model.EmailStatus;
import emgshared.model.IPDetails;
import emgshared.model.TaintIndicatorType;
import emgshared.model.TranSubReason;
import emgshared.model.Transaction;
import emgshared.model.TransactionComment;
import emgshared.model.TransactionStatus;
import emgshared.model.TransactionType;
import emgshared.model.UserActivityType;
import emgshared.property.EMTSharedDynProperties;
import emgshared.property.PropertyLoader;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ScoringService;
import emgshared.util.Constants;
import emgshared.util.DateFormatter;
import emgshared.util.StringHelper;
import engadm.util.restclient.osl.canceltransaction.CancelTransactionResponse;
import eventmon.eventmonitor.EventMonitor;

public class TransactionDetailAction extends EMoneyGramAdmBaseAction {
	private static Logger logger = EMGSharedLogger.getLogger(TransactionDetailAction.class);
	public static final NumberFormat df = new DecimalFormat("#,##0.00");
	public static final NumberFormat rateNumberFormat = new DecimalFormat(
			"#,##0.0000");
	protected static Logger log = EMGSharedLogger.getLogger(TransactionDetailAction.class);
	private static final String CONTAINER_RESOURCE_URI = "java:comp/env/rep/EMTAdminResourceReference";
	private static final String REASON_CODE_HUNDRED = "100";
	
	//MBO-5489
	//English language code for US Transaction - taken from language_tag_master table
	private static final String TXN_LANG_ENG_TAG_ID = "en-US";
	//for MBO-1050
	private static final String bankFundingAccType = "BANK-CHK";
	//ended

	private static final TransactionService tranService = ServiceFactory
			.getInstance().getTransactionService();
	protected static final DecryptionService decryptService = emgadm.services.ServiceFactory
			.getInstance().getDecryptionService();

	static String TRANSID = "transactionId";

	private BigDecimal recoveredAmt = null;
	private static EMTSharedDynProperties dynProps = new EMTSharedDynProperties();

	
	private static final String SOURCE_APPLICATION = "EMT";
	// private static final MailService mailService =
	// ServiceFactory.getInstance().getMailService();
	// TODO Remove hardcoded resource file.
	// private static final String resourceFile =
	// "emgadm.resources.ApplicationResources";
	public ActionForward execute(ActionMapping mapping, ActionForm f,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		TransactionDetailForm form = (TransactionDetailForm) f;
		UserProfile up = getUserProfile(request);
		TransactionManager tm = getTransactionManager(request);
		AgentConnectService acs = MGOServiceFactory.getInstance()
				.getAgentConnectService();

		String lookUp = form.getDispatch();
		String sessionlookUp = (String)request.getAttribute("dispatch");
		String ipAddress = form.getTranCustIP();
		boolean firstTimePageLoad = false;
		String transaction_partnerSiteID = null;

		if (lookUp != null || sessionlookUp!=null) {
			boolean isIpLookuponScoring = false;
			IPDetails ipDetails = getIPDetailsFromLookupService(ipAddress,
					isIpLookuponScoring);
			Transaction transaction = tm.getTransaction(Integer.parseInt(form
					.getTranId()));
			transaction_partnerSiteID = transaction.getPartnerSiteId();
			transaction.setIpDetails(ipDetails);
			tm.updateTransaction(transaction, up.getUID());
			form.setConsumerCity(ipDetails.getConsumerCity());
			form.setConsumerCountry(ipDetails.getConsumerCountry());
			form.setConsumerDomain(ipDetails.getConsumerDomain());
			form.setConsumerISP(ipDetails.getConsumerISP());
			form.setConsumerRegion(ipDetails.getConsumerRegion());
			form.setConsumerZipCode(ipDetails.getConsumerZipCode());
			if(sessionlookUp!=null){
				form.setTranStatusCode(transaction.getTranStatCode());
				form.setTranSubStatusCode(transaction.getTranSubStatCode());
			}
			String countryCode = ipDetails.getCountryCode();

			if (countryCode.equalsIgnoreCase("US")) {
				countryCode = "USA";
			} else if (countryCode.equalsIgnoreCase("UK")) {
				countryCode = "GBR";

			} else if (countryCode.equalsIgnoreCase("DE")) {
				countryCode = "DEU";
			} else if (countryCode.equalsIgnoreCase("AU")) {
				countryCode = "AUS";
			}

			form.setConsumerCountryCode(countryCode);
			if (form.getConsumerCountryCode() != null
					&& form.getTranSendCountry() != null
					&& form.getConsumerCountryCode().equalsIgnoreCase(
							form.getTranSendCountry())) {
				form.setCountrySameFlag(true);
			} else {
				form.setCountrySameFlag(false);
			}

		} else {
			firstTimePageLoad = true;
			// code changed for story 471.
			String tranId = form.getTranId();
			Map<String, String> transferMap = new HashMap();
			if (!StringUtility.isNullOrEmpty(tranId)) {
				if (NumberUtils.isNumber(tranId.trim())) {
					transferMap.put(TRANSID, tranId);
					request.getSession().setAttribute(TRANSID, transferMap);
				}
			} else {
				transferMap = (Map<String, String>) request.getSession()
						.getAttribute(TRANSID);
				if (transferMap != null)
					if (transferMap.containsKey(TRANSID))
						tranId = transferMap.get(TRANSID);
			}

			Transaction transaction = tm.getTransaction(Integer
					.parseInt(tranId));
			transaction_partnerSiteID = transaction.getPartnerSiteId();
			IPDetails ipDetails = transaction.getIpDetails();

			form.setConsumerCity(ipDetails.getConsumerCity());
			form.setConsumerCountry(ipDetails.getConsumerCountry());
			form.setConsumerDomain(ipDetails.getConsumerDomain());
			form.setConsumerISP(ipDetails.getConsumerISP());
			form.setConsumerRegion(ipDetails.getConsumerRegion());
			form.setConsumerZipCode(ipDetails.getConsumerZipCode());
			
			//temp
			
			
			String countryName = ipDetails.getConsumerCountry();
			if (countryName != null) {
				if (countryName.equalsIgnoreCase("United States")) {
					form.setConsumerCountryCode("USA");
				} else if (countryName.equalsIgnoreCase("United Kingdom")) {
					form.setConsumerCountryCode("GBR");

				} else if (countryName.equalsIgnoreCase("Germany")) {
					form.setConsumerCountryCode("DEU");
				} else {
					form.setConsumerCountryCode("");
				}
			} else {
				form.setConsumerCountryCode("");
			}

		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitExit())) {
			request.getSession().removeAttribute("tranId");
			request.setAttribute("partnerSiteId", form.getPartnerSiteId());
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_TRANS_QUEUE);
		}
		// check to see if the status/substatus of the transaction has changed
		if (!StringHelper.isNullOrEmpty(form.getTranStatusCode())
				&& !StringHelper.isNullOrEmpty(form.getTranSubStatusCode())) {
			Transaction tmp = tm.getTransaction(Integer.parseInt(form
					.getTranId()));
			
			if (!tmp.getTranStatCode().equals(form.getTranStatusCode())
					|| !tmp.getTranSubStatCode().equals(
							form.getTranSubStatusCode())) {
				String[] parm = {
						form.getTranStatusCode() + "/"
								+ form.getTranSubStatusCode(),
						tmp.getTranStatCode() + "/" + tmp.getTranSubStatCode() };
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"error.transaction.status.changed.already", parm));
				saveErrors(request, errors);
				return mapping
						.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
			}
		}

		Transaction tmp = null;
		TransactionService tranService = ServiceFactory.getInstance()
				.getTransactionService();
		if (!StringUtility.isNullOrEmpty(form.getTranId())) {
			System.out.println("********** For tranId = " + form.getTranId());
		}
		TransactionScoring tranScoring = ServiceFactory.getInstance()
				.getTransactionScoring();

		if (!StringHelper.isNullOrEmpty(form.getTranId())) {
			tmp = tm.getTransaction(Integer.parseInt(form.getTranId()));
			tranService = ServiceFactory.getInstance().getTransactionService(
					tmp.getPartnerSiteId());
		}

		try {
			List<GlAccount> glAccounts = new ArrayList<GlAccount>(
					tranService.getGLAccounts(up.getUID()));
			request.getSession().setAttribute("glAccounts", glAccounts);

		} catch (Exception e) {
			throw new EMGRuntimeException(e);
		}

		//2820
		/**
		 * These parameters will be available in request from CancelAction once
		 * action will be completed. When TransactionDetails page will be reloaded after canceling
		 * the Transaction, need to make following parameters null in form bean to 
		 * render Transaction detail page.
		 */
		String submitCancelBankRqstRefund = request.getAttribute("SubmitCancelBankRqstRefund")!=null
				?request.getAttribute("SubmitCancelBankRqstRefund").toString():null;
		if("true".equalsIgnoreCase(submitCancelBankRqstRefund)) {
			form.setSubmitCancelBankRqstRefund(null);
		}
		
		String submitRecCcAutoRef = request.getAttribute("SubmitRecCcAutoRef")!=null
						?request.getAttribute("SubmitRecCcAutoRef").toString():null;
		if("true".equalsIgnoreCase(submitRecCcAutoRef)) {
			form.setSubmitRecCcAutoRef(null);
		}
		
		String submitAutoCancelBankRqstRefund = request.getAttribute("SubmitAutoCancelBankRqstRefund")!=null
					?request.getAttribute("SubmitAutoCancelBankRqstRefund").toString():null;
		if("true".equalsIgnoreCase(submitAutoCancelBankRqstRefund)) {
				form.setSubmitAutoCancelBankRqstRefund(null);
		}
		
		String submitRecCcCxlRef = request.getAttribute("SubmitRecCcCxlRef")!=null
							?request.getAttribute("SubmitRecCcCxlRef").toString():null;
		if("true".equalsIgnoreCase(submitRecCcCxlRef)) {
			form.setSubmitRecCcCxlRef(null);
		}
		String submitCancelTran = request.getAttribute("SubmitCancelTran") != null ? 
				request.getAttribute("SubmitCancelTran").toString() : null;
		if ("true".equalsIgnoreCase(submitCancelTran)) {
			form.setSubmitCancelTransaction(null);
		}
		
		String submitManualCancelTran = request.getAttribute("SubmitManualCancelTran") != null ? 
				request.getAttribute("SubmitManualCancelTran").toString() : null;
		if ("true".equalsIgnoreCase(submitManualCancelTran)) {
					form.setSubmitManCancelTran(null);
		}
		
		//2820
	
		String confirmAction = null;
		// user requests a transaction action and needs a confirmation
		confirmAction = (String) request.getAttribute("confirm");
		if ((confirmAction == null)
				&& !(StringHelper.isNullOrEmpty(form.getSubmitApproveProcess())
						&& StringHelper.isNullOrEmpty(form.getSubmitApprove())
						&& StringHelper.isNullOrEmpty(form.getSubmitProcess())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitUndoApprove())
						&& StringHelper.isNullOrEmpty(form.getSubmitUndoDeny())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitRecAchAutoRef())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitRecAchCxlRef())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitRecAchRefundReq())						
						&& StringHelper.isNullOrEmpty(form
								.getSubmitRecCcRefundReq())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitAutoWriteOffLoss())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitManWriteOffLoss())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitRecordMgCancelRefund())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitAutoChargebackAndWriteOffLoss())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitAutoExceptionWOL())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitManualExceptionWOL())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitRecordAchChargeback())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitCancelAchWaiting())
						&& StringHelper.isNullOrEmpty(form.getSubmitEsMGSend())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitEsMGSendCancel())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitRecordTranError())																		
						&& StringHelper.isNullOrEmpty(form
										.getSubmitBankRqstRefund())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitBankRepresentRejDebit())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitBankRejectedDebit())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitBankRecordVoid())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitBankIssueRefund())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitBankWriteOffLoss())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitRetrievalRequest())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitReleaseTransaction())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitUndoDSError())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitCcManualRefund()) 
						&& StringHelper.isNullOrEmpty(form
								.getSubmitCcManualVoid())
						&& StringHelper.isNullOrEmpty(form
								.getSubmitMoveToReview())
						)) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRAN_CONFIRM);
		}
		// user's select an action that will invoke another form action
		if (!StringHelper.isNullOrEmpty(form.getSubmitResubmitACH())) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_RESUBMIT_ACH);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitCcSale())) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_CHARGE_BACKUP_CREDIT_CARD);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitManCollectFee())) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_MAN_COLLECT_FEE);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitManRefundFee())) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_MAN_REFUND_FEE);
		} else if (!StringHelper
				.isNullOrEmpty(form.getSubmitManReturnFunding())) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_MAN_RETURN_FUNDING);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecoveryOfLoss())) {
			List tranSubReasons = this
					.buildSubReasonList(TransactionStatus.RECOVERY_OF_LOSS_CODE);
			request.getSession().setAttribute("TranSubReasons", tranSubReasons);
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_RECOVERY_OF_LOSS);
		} else if (!StringHelper.isNullOrEmpty(form
				.getSubmitRecordCcChargeback())) {
			ChargebackTransactionHeader cbth = null;
			try {
				cbth = tm.getChargebackTransactionHeader(up.getUID(),
						Integer.parseInt(form.getTranId()));
			} catch (Exception e) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.error("Error attempting to get ChargebackHeader", e);
			}
			if (cbth == null) {
				String[] parms = { form.getTranId() };
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"error.chargeback.tracking.required", parms));
				saveErrors(request, errors);
				return mapping
						.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
			} else {
				buildSubReasonListBox(request, "CHB");
				return mapping
						.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_RECORD_CC_CHARGEBACK);
			}
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitDeny())) {
			buildSubReasonListBox(request, TransactionStatus.DENIED_CODE);
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_DENY);
		}		
		//2820
		else if (!StringHelper.isNullOrEmpty(form
				.getSubmitAutoCancelBankRqstRefund())) {
			
			log.info("Inside Auto Cancel Bank refund condition - going to call Struts action - CancelAction");
			request.setAttribute("cancelAction", "autoCancelBankRefund");
			
			buildSubReasonListBox(request, TransactionStatus.CANCELED_CODE);
			return mapping
			.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_CANCEL);
		}
		else if (!StringHelper.isNullOrEmpty(form
				.getSubmitRecCcCxlRef())) {
			log.info("Inside Manual Cancel condition - going to call Struts action - CancelAction");
			request.setAttribute("cancelAction", "manualCancel");

			buildSubReasonListBox(request, TransactionStatus.CANCELED_CODE);
			return mapping
			.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_CANCEL);
			// doAutoRefund(form, up, tm, errors, request); 
		}
		else if (!StringHelper.isNullOrEmpty(form
				.getSubmitCancelBankRqstRefund())) {
			 log.info("Inside Manual Cancel Bank funded condition - going to call Struts action - CancelAction");
			request.setAttribute("cancelAction", "submitCancelBankRqstRefund");
			buildSubReasonListBox(request, TransactionStatus.CANCELED_CODE);
			return mapping
			.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_CANCEL);
		}
		else if (!StringHelper.isNullOrEmpty(form
				.getSubmitRecCcAutoRef())) {
			log.info("Inside Auto Cancel CC funded condition - going to call Struts action - CancelAction");
			request.setAttribute("cancelAction", "autoCancel");
			buildSubReasonListBox(request, TransactionStatus.CANCELED_CODE);
			return mapping
			.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_CANCEL);
			// doAutoRefund(form, up, tm, errors, request); 
		}
		/*else if (!StringHelper.isNullOrEmpty(form
				.getSubmitBankRqstRefund())) {
			 log.info("Inside Auto Cancel Bank funded condition - going to call Struts action - CancelAction");
				request.setAttribute("cancelAction", "submitBankRqstRefund");
			 buildSubReasonListBox(request, TransactionStatus.CANCELED_CODE);
				return mapping
				.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_CANCEL);
		}*/
		//2820 ended
		else if (!StringHelper.isNullOrEmpty(form.getSubmitMoveToPending())) {
			buildSubReasonListBox(request, TransactionStatus.PENDING_CODE);
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_PEND);
		} else if (!StringHelper.isNullOrEmpty(form
				.getSubmitRecordLossAdjustment())) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_RECORD_LOASS_ADJUSTMENT);
		} else if (!StringHelper.isNullOrEmpty(form
				.getSubmitRecordFeeAdjustment())) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_RECORD_FEE_ADJUSTMENT);
		} else if (!StringHelper.isNullOrEmpty(form
				.getSubmitRecordConsumerRefundOrRemittanceAdjustment())) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_RECORD_CONSUMER_REFUND_OR_REMITTANCE_ADJUSTMENT);
		} else if (!StringHelper.isNullOrEmpty(form
				.getSubmitRecordMgActivityAdjustment())) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_RECORD_MG_ACTIVITY_ADJUSTMENT);
		} else if (!StringHelper.isNullOrEmpty(form
				.getSubmitCancelTransaction())) {
			log.info("Inside Auto Cancel for RTBANK- submitCancelTransaction");
			request.setAttribute("cancelAction", "autoCancelRTBANK");
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_CANCEL);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitManCancelTran())) {
			log.info("Inside Manual Cancel for RTBANK- submitManCancelTran");
			request.setAttribute("cancelAction", "manualCancelRTBANK");
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_CANCEL);
		}
		String id = null;
		id = form.getTranId();
		// Pass the id as a queryString for transaction ownership
		if (request.getParameter("id") != null) {
			id = (String) request.getParameter("id");
			//id = request.getParameter("id");
			if(id.matches("[0-9]+")) { //MBO-9993
			request.getSession().setAttribute("tranId", id);
			}
		}
		int tranId = 0;
		if (StringHelper.isNullOrEmpty(id)
				&& StringHelper.containsNonDigits(id)) {
			tranId = Integer.parseInt(form.getTranId());
		} else {
			tranId = Integer.parseInt(id);
		}
		// user click the add comment button
		if (!StringHelper.isNullOrEmpty(form.getSubmitAddComment())) {
			if (StringHelper.isNullOrEmpty(form.getCmntText())) {
				form.setCmntText("");
			}
			tm.setTransactionComment(Integer.parseInt(id),
					form.getTranCmntReasCode(), form.getCmntText(), up.getUID());
			form.reset(mapping, request);
		} else {
			form.setCmntText("");
		}
		
		// user click the re-generate email button
		if (!StringHelper.isNullOrEmpty(form.getSubmitRegenerateEmail())) {
			  regenerateSentEmail(tm, tranId,up);
		}
		
		// user click on the generate transaction score button
		if (!StringHelper.isNullOrEmpty(form.getSubmitGenTranScore())) {
			Transaction transaction = tm.getTransaction(tranId);
			if (transaction.getTranAppVersionNumber() >= 2.0) {

				if (transaction.getTranScore() == null) {

					CyberSourceScoring cyberSourceScoring = new CyberSourceScoring();

					ConsumerProfileService consumerProfileService = emgshared.services.ServiceFactory
							.getInstance().getConsumerProfileService();

					ConsumerProfile consumerProfile = consumerProfileService
							.getConsumerProfile(transaction.getSndCustId(),
									up.getUID(), null);
					String emgTranTypeCode = transaction.getEmgTranTypeCode();

					boolean isEPTran = emgTranTypeCode
							.equals(TransactionType.EXPRESS_PAYMENT_SEND_CODE);
					boolean isMGTran = emgTranTypeCode
							.equals(TransactionType.MONEY_TRANSFER_SEND_CODE);

					// get the results from scoring service
					emgshared.services.ServiceFactory serviceFactory = emgshared.services.ServiceFactory
							.getInstance();
					ScoringService scoringService = serviceFactory
							.getScoringService();
					try {
						DecisionAndScoreResponse decisionAndScoreResponse = cyberSourceScoring
								.getCyberSourceDecision(transaction,
										consumerProfile, scoringService,
										up.getUID(), tm);

						cyberSourceScoring.updateScoreAndAllComments(
								decisionAndScoreResponse, transaction,
								consumerProfile, scoringService,
								decisionAndScoreResponse.getDecision(),
								isEPTran, isMGTran);

					} catch (Exception exception) {
						TransactionDAO transactionDAO = new TransactionDAO();
						String commentText = "Error while manual scoring in CyberDM";
						TransactionComment transactionComment = new TransactionComment();
						transactionComment
								.setTranId(transaction.getEmgTranId());
						transactionComment.setReasonCode("OTH");
						transactionComment.setText(commentText);
						transactionDAO.insertComment(transactionComment,
								up.getUID());
						transactionDAO.commit();
					
					}

				}
			} else {

				tranScoring.getTransactionScore(up.getUID(), tranId);
			}
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
					"msg.tran.score.generated", form.getTranId()));
			saveErrors(request, errors);
		}

		if (!StringHelper.isNullOrEmpty(form.getTelecheckAction())) {
			Transaction tran = tm.getTransaction(tranId);
			tran.setStatus(TransactionStatus.BANK_RECOVER_OF_LOSS);
			String amount = request.getParameter("amount");
			tm.updateTransaction(tran, up.getUID(), null, new Float(amount));
			tran.setStatus(TransactionStatus.SENT_BANK_WRITE_OFF_LOSS);
			String fee = request.getParameter("fee");
			tm.updateTransaction(tran, up.getUID(), null, new Float(fee));
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
					"wap.recovery.loss.successful", form.getTranId()));
			saveErrors(request, errors);
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitCcPartialRefund())) {

			try {

				Transaction tran = tm.getTransaction(tranId);

				tranService.doCreditCardPartialRefund(tranId,
						form.getRefundAmount(), up.getUID(),
						request.getContextPath());

				TranAction ta = new TranAction();

				int debitAccount = Integer.parseInt(form.getDebitAcct());

				ta.setTranId(tranId);
				ta.setDrGLAcctId(debitAccount);
				ta.setCrGLAcctId(2);
				ta.setTranPostAmt(new BigDecimal(form.getRefundAmount()));
				ta.setTranReasDesc(EMoneyGramAdmApplicationConstants.glAccountCodeToAction
						.get(debitAccount));

				tm.insertGLAdjustment(ta, tran.getSndAgentId(), up.getUID());

				ta.setDrGLAcctId(2);
				ta.setCrGLAcctId(1);
				ta.setTranReasDesc("CC Refund");

				tm.insertGLAdjustment(ta, tran.getSndAgentId(), up.getUID());

				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"partial.refund.successful", form.getTranId()));
				saveErrors(request, errors);

			} catch (EMGRuntimeException e) {

				if (e.getRootCause() instanceof CreditCardRefundException) {
					CreditCardRefundException refundException = (CreditCardRefundException) e
							.getRootCause();
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"partial.refund.failed", refundException.getCode(),
							refundException.getDescription()));
					saveErrors(request, errors);
				} else {
					throw new EMGRuntimeException(e);
				}

			} catch (Exception e) {
				throw new EMGRuntimeException(e);
			}
		}

		String tranStatus = form.getTranStatusCode();
		String fundStatus = form.getTranSubStatusCode();
		boolean changeStatus = false;

		List<String> gea = null; // some service methods return a list of error
		// strings to append as global errors. This keeps Struts out of the
		// service.
		// perform transaction update only if confirm is O.K.
		if ("ok".equalsIgnoreCase(confirmAction)) { // perform update only if
			// the transaction taken is
			// valid
			if (isTokenValid(request)) {

				String IPLookup_MDValue = dynProps.getIPLookup_MD();
				if (IPLookup_MDValue != null
						&& IPLookup_MDValue.equals("manualtxns")) {
					boolean isIpLookuponScoring = false;
					String comment = IPLocatorService
							.getFormattedStringForIpLookup(
									form.getTranCustIP(), isIpLookuponScoring);
					tm.setTransactionComment(Integer.parseInt(id), "OTH",
							comment, up.getUID());
				}

				try {
					if (!StringHelper.isNullOrEmpty(form.getSubmitApprove())
							|| !StringHelper.isNullOrEmpty(form
									.getSubmitApproveProcess())) {
						String tranType = form.getTranType();
						PreparePaymentResponse preparePaymentResponse = null;
						// https://mgonline.atlassian.net/browse/SOF-102
						// https://mgonline.atlassian.net/browse/SOF-86
						// https://mgonline.atlassian.net/browse/EMT-4120 defect
						if ((TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE
								.equalsIgnoreCase(tranType) && transaction_partnerSiteID
								.equalsIgnoreCase("MGOAUS"))
								|| TransactionType.MONEY_TRANSFER_RTBANK_SEND_CODE
										.equalsIgnoreCase(tranType)) {

							OSLClientExecutor oslClientExecutor = new OSLClientExecutor();
							preparePaymentResponse = oslClientExecutor
									.callPreparePayment(up.getUID(),
											String.valueOf(tranId));
							if (preparePaymentResponse != null) {
								logger.info("preparePaymentResponse"
										+ new ReflectionToStringBuilder(preparePaymentResponse, new RecursiveToStringStyle()).toString());
								if (preparePaymentResponse.getStatus() != null) {
									if (preparePaymentResponse.getStatus() == 200) {
										logger.info("preparePaymentResponse"
												+ preparePaymentResponse);
									} else if (preparePaymentResponse.getStatus() == 500) {
										logger.error("Error while calling PrepareAPI Call- 500 Response");
										errors.add(
												ActionErrors.GLOBAL_ERROR,
												new ActionError(
														"error.bankpayment.preparepayment",
														form.getTranId()));
									} else if (preparePaymentResponse.getErrorVO() != null) {
										logger.error("Error while calling PrepareAPI Call- 500 Response");
										errors.add(
												ActionErrors.GLOBAL_ERROR,
												new ActionError(
														"error.bankpayment.preparepayment",
														form.getTranId()));
									}
								} else {
									logger.error("Error while calling PrepareAPI Call- Null Response");
									errors.add(ActionErrors.GLOBAL_ERROR,
											new ActionError(
													"error.bankpayment.preparepayment",
													form.getTranId()));
								}
							} else {
								logger.error("Error while calling PrepareAPI Call- Null Response");
								errors.add(
										ActionErrors.GLOBAL_ERROR,
										new ActionError(
												"error.bankpayment.preparepayment", form
														.getTranId()));
							}
							if (errors.size() > 0) {
								saveErrors(request, errors);
							}
						} else {
						//condition modified to add OR condition for MBO-1050
						
						// MBO-396
						// If profile SCRTY_CHLNG_STAT_CODE was "CHALLENGED" AND an EMTAdmin
						// user MANUALLY approves a transaction, this profile status should be
						// changed to 'VERIFIED'.
//						if (trans != null
//								&& trans.getTranStatCode().equalsIgnoreCase(
//										TransactionStatus.APPROVED_CODE)) {
							ConsumerProfileService consumerProfileService = emgshared.services.ServiceFactory
									.getInstance().getConsumerProfileService();
							ConsumerProfile consumerProfile = consumerProfileService
									.getConsumerProfile(tm.getTransaction(tranId).getSndCustId(), up.getUID(), null);
							if (consumerProfile.getRsaCustomerChallengeStatusCode() == ConsumerStatus.SECURITY_CHALLENGE_STATUS_CODE_CHALLENGE) {

								consumerProfile
										.setRsaCustomerChallengeStatusCode(ConsumerStatus.SECURITY_CHALLENGE_STATUS_CODE_VERIFIED);
								consumerProfileService.updateConsumerProfile(consumerProfile,
										true, up.getUID());
							}
//						}
						
						String fundingAccType = null;
						fundingAccType = form.getTranPrimAcctType();
						EMGSharedLogger.getLogger("Funding Account Type in TransactionDetailAction is "+ fundingAccType);
						if (TransactionType.DELAY_SEND_CODE
								.equalsIgnoreCase(form.getTranType())
								|| (fundingAccType!=null && TransactionType.EXPRESS_PAYMENT_SEND_CODE.equalsIgnoreCase(form.getTranType()) 
										&& bankFundingAccType.equalsIgnoreCase(fundingAccType))) {
						//code change end for MBO-1050
							long tmpTime = System.currentTimeMillis();
							gea = tranService.approveDelayedSendTransaction(
									Integer.parseInt(form.getTranId()),
									up.getUID(), form.getTranStatusCode(),
									form.getTranSubStatusCode(),
									request.getContextPath());
							EventMonitor.getInstance().recordEvent(
									EventListConstant.DS_PROCESS, tmpTime,
									System.currentTimeMillis());
							appendGlobalErrors(gea, errors);
							// release ownership if no errors so that automatic
							// processing will pick this up
							if (errors.size() == 0) {
								tranService.releaseOwnership(
										Integer.parseInt(form.getTranId()),
										up.getUID());
								try {
									super.insertActivityLog(
											this.getClass(),
											request,
											null,
											UserActivityType.EVENT_TYPE_TRANSACTIONRELEASEOWNERSHIP,
											String.valueOf(tranId));
								} catch (Exception e) {
								}

							}
						} else {	
							changeTranStatus(tranId,
									TransactionStatus.APPROVED_CODE,
									form.getTranSubStatusCode(),
									up.getUID(), tm.getTransaction(tranId)
											.getPartnerSiteId());
							form.setTranStatusCode(TransactionStatus.APPROVED_CODE);
							//PWMB-58 call to TPS capture API
							if(StringUtils.equalsIgnoreCase(tm.getTransaction(tranId).getSndISOCntryCode(), "USA")
									&& StringUtils.equalsIgnoreCase(tm.getTransaction(tranId).getEmgTranTypeCode(), TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE))
							{
								TPSClientExecutor tpsClientExecutor = new TPSClientExecutor();
								TPSCaptureResponse tpsCaptureResponse = tpsClientExecutor.captureTransaction(Integer.parseInt(form.getTranId()), up.getUID());
								if (tpsCaptureResponse != null) {
									logger.info("tpsCaptureResponse"
											+ new ReflectionToStringBuilder(tpsCaptureResponse, new RecursiveToStringStyle()).toString());
									if (tpsCaptureResponse.getStatus() != null) {
										if (tpsCaptureResponse.getStatus() == 200) {
											logger.info("tpsCaptureResponse"
													+ tpsCaptureResponse);
										} else if (tpsCaptureResponse.getStatus() == 500) {
											logger.error("Error while calling CaptureAPI Call- 500 Response");
											errors.add(
													ActionErrors.GLOBAL_ERROR,
													new ActionError(
															"error.pwmb.transaction.capture",
															form.getTranId()));
										} else if (tpsCaptureResponse.getErrorVO() != null) {
											logger.error("Error while calling CaptureAPI Call- ErrorVO Response");
											errors.add(
													ActionErrors.GLOBAL_ERROR,
													new ActionError(
															"error.pwmb.transaction.capture",
															form.getTranId()));
										}
									} else {
										logger.error("Error while calling CaptureAPI Call- Null Status");
										errors.add(ActionErrors.GLOBAL_ERROR,
												new ActionError(
														"error.pwmb.transaction.capture",
														form.getTranId()));
									}
								} else {
									logger.error("Error while calling CaptureAPI Call- Null Response");
									errors.add(
											ActionErrors.GLOBAL_ERROR,
											new ActionError(
													"error.pwmb.transaction.capture", form
															.getTranId()));
								}
								if (errors.size() > 0) {
									form.setTranStatusCode(TransactionStatus.ERROR_CODE);
									saveErrors(request, errors);									
								}								
							}

						}
					}
					}
					if (errors.size() == 0
							&& ((!StringHelper.isNullOrEmpty(form.getSubmitProcess()) || !StringHelper
									.isNullOrEmpty(form.getSubmitApproveProcess()))
									&& (!(TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE
											.equalsIgnoreCase(form.getTranType()) && transaction_partnerSiteID
											.equalsIgnoreCase("MGOAUS"))) && (!TransactionType.MONEY_TRANSFER_RTBANK_SEND_CODE
									.equalsIgnoreCase(form.getTranType()))
							)) {
						String tranType = form.getTranType();
						//condition changed for MBO-1050
						//Only Credit Card EPSEND Txn should go in this loop, NOT EPSEND Bank funding Txns.
						String fundingAccType = null;
						fundingAccType = form.getTranPrimAcctType();
						EMGSharedLogger.getLogger("Funding Account Type in TransactionDetailAction is "+ fundingAccType);
						
						if (TransactionType.MONEY_TRANSFER_SEND_CODE
								.equalsIgnoreCase(tranType)
								|| TransactionType.EXPRESS_PAYMENT_SEND_CODE
										.equalsIgnoreCase(tranType)
								&& fundingAccType.startsWith("CC")) {
							// TPS
							TPSClientExecutor tpsExec = new TPSClientExecutor();
							FundAndCommitResponseVO tpsResponse = tpsExec
									.processCommitTransaction(
											transaction_partnerSiteID, tranId);
							if (tpsResponse == null) {
								errors.add(ActionErrors.GLOBAL_ERROR,
										new ActionError("exception.unexpected"));
								saveErrors(request, errors);
							}
						}
						
						/**
						 * https://mgonline.atlassian.net/browse/AUS-86
						 * https://mgonline.atlassian.net/browse/SOF-102:-
						 * EMTAdmin - Call preparePayment when Sofort txn is
						 * 'approved' by CSR
						 */
						else if (TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE
								.equalsIgnoreCase(tranType)) {
							long tmpTime = System.currentTimeMillis();

						//	https://mgonline.atlassian.net/browse/SOF-102
									
								tranService.sendEconomyServicePendingEmail(
									tm.getTransaction(tranId), up.getUID());
							
							EventMonitor.getInstance().recordEvent(
									EventListConstant.ES_PROCESS, tmpTime,
									System.currentTimeMillis());
							//condition changed for MBO-1050
							//pull EPSEND bank funded Txn for following processing
						} else if (TransactionType.DELAY_SEND_CODE
								.equalsIgnoreCase(tranType)
								|| (fundingAccType!=null && TransactionType.EXPRESS_PAYMENT_SEND_CODE.equalsIgnoreCase(form.getTranType()) 
										&& bankFundingAccType.equalsIgnoreCase(fundingAccType))) {
							//ended for MBO-1050
							long tmpTime = System.currentTimeMillis();
							gea = tranService.processDelayedSendTransaction(
									Integer.parseInt(form.getTranId()),
									up.getUID(), form.getTranStatusCode(),
									form.getTranSubStatusCode(),
									request.getContextPath());
							EventMonitor.getInstance().recordEvent(
									EventListConstant.DS_PROCESS, tmpTime,
									System.currentTimeMillis());
							appendGlobalErrors(gea, errors);
						} else {
							throw new IllegalStateException(
									"Unable to process transaction type ("
											+ tranType + ")");
						}
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitRecordMgCancelRefund())) {
						tranStatus = TransactionStatus.CANCELED_CODE;
						fundStatus = form.getTranSubStatusCode();
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitUndoApprove())
							|| !StringHelper.isNullOrEmpty(form
									.getSubmitUndoDeny())) {
						List tmpList = getTranActions(request, tm, tranId,
								up.getUID(), form.getTransPartnerSiteId());
						tranStatus = TransactionStatus.FORM_FREE_SEND_CODE;
						if (tmpList.size() > 0) {
							TranActionView tav = (TranActionView) tmpList
									.get(tmpList.size() - 1);
							tranStatus = tav.getTranOldStatCode();
						}
						fundStatus = form.getTranSubStatusCode();
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitRecAchAutoRef())) {
						doAutoRefund(form, up, tm, errors, request, true);
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitRecAchCxlRef())) {
						if (TransactionStatus.SENT_CODE.equals(form
								.getTranStatusCode())) {
							tranStatus = TransactionStatus.CANCELED_CODE;
							changeTranStatus(tranId,
									TransactionStatus.CANCELED_CODE,
									form.getTranSubStatusCode(), up.getUID(),
									null);
						} else {
							tranStatus = form.getTranStatusCode();
						}
						Transaction tran = tm.getTransaction(tranId);
						ConsumerAccountType primaryAccountType = ConsumerAccountType
								.getInstance(tran.getSndAcctTypeCode());
						tranService.refundPersonToPersonSuccessEmail(tran,
								primaryAccountType);
						fundStatus = TransactionStatus.ACH_REFUND_REQUESTED_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitRecAchRefundReq())) {
						tranStatus = form.getTranStatusCode();
						//https://mgonline.atlassian.net/browse/AUS-625  changed ACH_CANCEL_CODE to ACH_REFUND_REQUESTED_CODE
						fundStatus = TransactionStatus.ACH_REFUND_REQUESTED_CODE; 
						changeStatus = true;
					}/* else if (!StringHelper.isNullOrEmpty(form
							.getSubmitRecCcAutoRef())) {
						log.info("Inside Auto Cancel condition - going to call Struts action - CancelAction");
						buildSubReasonListBox(request, TransactionStatus.CANCELED_CODE);
						return mapping
						.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_CANCEL);
						// doAutoRefund(form, up, tm, errors, request);
					} 
					else if (!StringHelper.isNullOrEmpty(form
							.getSubmitRecCcCxlRef())) {
						if (TransactionStatus.SENT_CODE.equals(form
								.getTranStatusCode())) {
							//MBO-2610 The manual Credit card refund button is pressed
							tm.setTransactionComment(
									tranId,
									"OTH",
									"Manual Cancel button (Manual Record CC Cancel/Refund) pressed",
									up.getUID());

							// call the new method to change status. Testing for
							// now
							changeTranStatusRefundManual(tranId,
									TransactionStatus.CANCELED_CODE,
									form.getTranSubStatusCode(), up.getUID(),
									null, tm);
							// call the new method to change status. Testing for
							// now
							changeTranStatusRefundManual(tranId,
									TransactionStatus.CANCELED_CODE,
									TransactionStatus.CC_REFUND_REQUESTED_CODE,
									up.getUID(), null, tm);
							tranStatus = TransactionStatus.CANCELED_CODE;

						} else {
							changeTranStatusRefundManual(tranId,
									form.getTranStatusCode(),
									TransactionStatus.CC_REFUND_REQUESTED_CODE,
									up.getUID(), null, tm);
							tranStatus = form.getTranStatusCode();
						}
						// Now do the CC funding reversal
						doAutoRefund(form, up, tm, errors, request, false); // false
																			// =
																			// don't
																			// do
																			// Mainframe
																			// reversal

						Transaction tran = tm.getTransaction(tranId);
						ConsumerAccountType primaryAccountType = ConsumerAccountType
								.getInstance(tran.getSndAcctTypeCode());
						// Call again the service for the transaction according
						// the pymt_prvd_srv_code
						tranService = ServiceFactory.getInstance()
								.getTransactionServicePymtProvider(
										tran.getTCProviderCode(),
										tran.getPartnerSiteId());
						tranService.refundPersonToPersonSuccessEmail(tran,
								primaryAccountType);
						/*
						 * fundStatus = TransactionStatus.CC_CANCEL_CODE; // set
						 * the transaction service again for another process
						 * tranService =
						 * ServiceFactory.getInstance().getTransactionService();
						 * changeStatus = true;
						 */
					/*}*/ else if (!StringHelper.isNullOrEmpty(form
							.getSubmitRecCcRefundReq())) {
						tranService.doCreditCardReversal(tranId, up.getUID(),
								request.getContextPath());
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitAutoChargebackAndWriteOffLoss())) {
						if (form.getTranSubStatusCode().equals(
								TransactionStatus.ACH_SENT_CODE)) {
							fundStatus = TransactionStatus.ACH_RETURNED_CODE;
						} else {
							fundStatus = TransactionStatus.CC_CHARGEBACK_CODE;
						}
						changeStatus = true;
						//MBO-2813 changes,// Handle Auto Exception Write Off Loss button and Manual Exception WOL Buttons
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitAutoExceptionWOL())
							|| !StringHelper.isNullOrEmpty(form
									.getSubmitManualExceptionWOL())) {
						Transaction tran = tm.getTransaction(tranId);
						System.out.println("Inside if of Auto Exception - going to call isTxnReceived");
						  if (!isTransactionReceived(tm,tranId,request,form)) {
							    //if tran is not received, user should cancel transaction instead so MoneyGram does not lose the money
							    errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							    "error.transaction.not.received"));
							    saveErrors(request, errors);
						  }
						  else{
							// Change 1st status (SEN/CCS -> SEN/EWL)
							  tran.setTranSubStatCode(TransactionStatus.EXCEPTION_WRITE_OFF_LOSS_CODE);
							    tran.setTranStatDate(new Date());
							    tm.updateTransaction(tran, up.getUID());
						  }
						  if (!StringHelper.isNullOrEmpty(form.getSubmitAutoExceptionWOL())) {
						      // Now do the CC funding reversal for 'auto' button
						      //Note, the transition to fundStatus of WRITE_OFF_LOSS happens in the processor reversal code if successful
						      doAutoRefund(form, up, tm, errors, request, false); // false = don't do Mainframe Reversal
						      Transaction transaction = tm.getTransaction(tranId);
						      if (!(TransactionStatus.WRITE_OFF_LOSS_CODE).equalsIgnoreCase(transaction.getTranSubStatCode())) {
						          //something happened with payment reversal, move txn back to SEN/CCS
						    	  transaction.setTranSubStatCode(TransactionStatus.CC_SALE_CODE);
						    	  transaction.setTranStatDate(new Date());
						          tm.updateTransaction(transaction, up.getUID());
						          errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						          "exception.unexpected"));
						          saveErrors(request, errors);
						       }
						     } else {
						       //For Manual mode, just set up txn state transition to SEN/WOL
						       fundStatus = TransactionStatus.WRITE_OFF_LOSS_CODE;
						       changeStatus = true;
						   }
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitRecordAchChargeback())) {
						if (TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE
								.equalsIgnoreCase(form.getTranType())) {
							tranService.sendEconomyServiceAchReturnEmail(
									tm.getTransaction(tranId), up.getUID());
						} else {
							tranStatus = form.getTranStatusCode();
							fundStatus = TransactionStatus.ACH_RETURNED_CODE;
							changeStatus = true;
						}
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitRecordCcChargeback())) {
						tranStatus = form.getTranStatusCode();
						fundStatus = TransactionStatus.CC_CHARGEBACK_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitCancelAchWaiting())) {
						changeTranStatus(tranId,
								TransactionStatus.CANCELED_CODE,
								TransactionStatus.ACH_WAITING_CODE,
								up.getUID(), null);
						Transaction tran = tm.getTransaction(tranId);
						ConsumerAccountType primaryAccountType = ConsumerAccountType
								.getInstance(tran.getSndAcctTypeCode());
						tranService.refundPersonToPersonSuccessEmail(tran,
								primaryAccountType);
						tranStatus = TransactionStatus.CANCELED_CODE;
						fundStatus = TransactionStatus.ACH_CANCEL_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitManWriteOffLoss())) {
						tranStatus = TransactionStatus.MANUAL_CODE;
						fundStatus = TransactionStatus.MANUAL_SUB_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitAutoWriteOffLoss())) {
						fundStatus = TransactionStatus.WRITE_OFF_LOSS_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitEsMGSend())) {
						//https://mgonline.atlassian.net/browse/AUS-623
						/*List<String> erl = tranService
								.processEconomyServiceTransaction(tranId,
										up.getUID(), form.getTranStatusCode(),
										form.getTranSubStatusCode(),
										request.getContextPath());
						//appendGlobalErrors(erl, errors);*/
						OSLClientExecutor oslClientExecutor = new OSLClientExecutor();
						 FundAndCommitResponse fundAndCommitResponse = oslClientExecutor.processFundAndCommit(String.valueOf(tranId));
						if (fundAndCommitResponse.getStatus() == 500) {
							logger.error("Error while calling OSL Fund&Commit Call- 500 Response");
							throw new IllegalStateException(
									"Unable to process transaction type ("
											+ tranId + ")");
						}
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitEsMGSendCancel())) {
						changeTranStatus(tranId,
								TransactionStatus.CANCELED_CODE,
								TransactionStatus.ACH_SENT_CODE, up.getUID(),
								null);
						Transaction tran = tm.getTransaction(tranId);
						ConsumerAccountType primaryAccountType = ConsumerAccountType
								.getInstance(tran.getSndAcctTypeCode());
						tranService.refundPersonToPersonSuccessEmail(tran,
								primaryAccountType);
						tranStatus = TransactionStatus.CANCELED_CODE;
						fundStatus = TransactionStatus.ACH_REFUND_REQUESTED_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitRecordTranError())) {
						tranStatus = TransactionStatus.ERROR_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitReleaseTransaction())) {
						tm.setTransactionComment(
								tranId,
								"OTH",
								"Release transaction from 'Verified By Visa In Process'.",
								up.getUID());
						fundStatus = TransactionStatus.NOT_FUNDED_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitRetrievalRequest())) {
						Transaction tran = tm.getTransaction(tranId);
						tran.setFundRetrievalRequestDate(new Date(System
								.currentTimeMillis()));
						tm.updateTransaction(tran, up.getUID());
						changeStatus = false;
					}
					//commenting as part of 2820
					/*else if (!StringHelper.isNullOrEmpty(form
							.getSubmitCancelBankRqstRefund())) {
						buildSubReasonListBox(request, TransactionStatus.CANCELED_CODE);
						return mapping
						.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_CANCEL);
						// Handle the non automated requestrefund (no MF
						// reversal)
						// MBO-2610 Added transaction comment when Cancel and
						// Request Bank Refund button is clicked
						/*tm.setTransactionComment(
								tranId,
								"OTH",
								"Manual Cancel button (Cancel and Request Bank Refund) pressed",
								up.getUID());
						Transaction tran = tm.getTransaction(tranId);
						//MBO-2610 Removed bank reversal call if the Status in SEN/BKR
						if (!((TransactionStatus.SENT_CODE)
								.equalsIgnoreCase(tran.getStatus()
										.getStatusCode()) && (TransactionStatus.BANK_REJECTED_CODE)
								.equalsIgnoreCase(tran.getStatus()
										.getSubStatusCode()))) {						
						// Now do the Bank reversal/refund
						long tmpTime = System.currentTimeMillis();
						tranService.doBankReversal(
								Integer.parseInt(form.getTranId()),
								up.getUID(), request.getContextPath());
						EventMonitor
								.getInstance()
								.recordEvent(
										EventListConstant.BANK_PAYMENT_SERVICE_ADJUSTMENT,
										tmpTime, System.currentTimeMillis());
						}

						// Change 1st status (SEN/BKS -> SEN/BRR, SEN/BKR ->
						// CXL/BKR)
						if (tran.getStatus()
								.getSubStatusCode()
								.equalsIgnoreCase(
										TransactionStatus.BANK_SETTLED_CODE)) {
							tran.setTranSubStatCode(TransactionStatus.BANK_REFUND_REQUEST_CODE);
						} else {
							tran.setTranStatCode(TransactionStatus.CANCELED_CODE);
						}
						tran.setTranStatDate(new Date());
						tm.updateTransaction(tran, up.getUID());

						// Set up txn state transition for SEN/BRR status
						changeStatus = false;
						if (tran.getStatus().getStatusCode()
								.equalsIgnoreCase(TransactionStatus.SENT_CODE)
								&& tran.getStatus()
										.getSubStatusCode()
										.equalsIgnoreCase(
												TransactionStatus.BANK_REFUND_REQUEST_CODE)) {
							tranStatus = TransactionStatus.CANCELED_CODE;
							fundStatus = TransactionStatus.BANK_REFUND_REQUEST_CODE;
							changeStatus = true;
						}
					} 
					else if (!StringHelper.isNullOrEmpty(form
							.getSubmitAutoCancelBankRqstRefund())) {
												
						// Handle the automated request refund (will send MF
						// reversal)

						// send Reversal to Mainframe via AgentConnect
						doAutoRefund(form, up, tm, errors, request, true);
						//MBO-2610 logic to change status from SEN/BKR to CXL/BKR without bank reversal call
						Transaction tran = tm.getTransaction(tranId);
						if (!((TransactionStatus.SENT_CODE)
								.equalsIgnoreCase(tran.getStatus()
										.getStatusCode()) && (TransactionStatus.BANK_REJECTED_CODE)
								.equalsIgnoreCase(tran.getStatus()
										.getSubStatusCode()))) {
							if (errors.isEmpty()) {
								// Now do the Bank reversal/refund
								long tmpTime = System.currentTimeMillis();
								tranService.doBankReversal(
										Integer.parseInt(form.getTranId()),
										up.getUID(), request.getContextPath());
								EventMonitor
										.getInstance()
										.recordEvent(
												EventListConstant.BANK_PAYMENT_SERVICE_ADJUSTMENT,
												tmpTime,
												System.currentTimeMillis());
								// Set up txn state transition for SEN/BRR
								// status
								changeStatus = false;
							}
						}
						else
						{
							tranStatus = TransactionStatus.CANCELED_CODE;
							fundStatus = TransactionStatus.BANK_REJECTED_CODE;
							changeStatus = true;
						}	
					
							 if (tran.getStatus()
									.getStatusCode()
									.equalsIgnoreCase(
											TransactionStatus.SENT_CODE)
									&& tran.getStatus()
											.getSubStatusCode()
											.equalsIgnoreCase(
													TransactionStatus.BANK_REFUND_REQUEST_CODE)) {
								tranStatus = TransactionStatus.CANCELED_CODE;
								fundStatus = TransactionStatus.BANK_REFUND_REQUEST_CODE;
								changeStatus = true;
							}
						}*/
					else if (!StringHelper.isNullOrEmpty(form
							.getSubmitBankRqstRefund())) {					
						// Handle a request for bank refund
						Transaction tran = tm.getTransaction(tranId);

						// Change 1st status (APP/BKS -> CXL/BKS)
						tran.setTranStatCode(TransactionStatus.CANCELED_CODE);
						tran.setTranStatDate(new Date());
						tm.updateTransaction(tran, up.getUID());

						// Now do the Bank reversal/refund
						long tmpTime = System.currentTimeMillis();
						tranService.doBankReversal(
								Integer.parseInt(form.getTranId()),
								up.getUID(), request.getContextPath());
						EventMonitor
								.getInstance()
								.recordEvent(
										EventListConstant.BANK_PAYMENT_SERVICE_ADJUSTMENT,
										tmpTime, System.currentTimeMillis());

						// Set up txn state transition to CXL/BRR
						tranStatus = TransactionStatus.CANCELED_CODE;
						fundStatus = TransactionStatus.BANK_REFUND_REQUEST_CODE;
						changeStatus = true;
					}
					 else if (!StringHelper.isNullOrEmpty(form
							.getSubmitBankRejectedDebit())) {
						Transaction tran = tm.getTransaction(tranId);
						// Set up txn state transition to SEN/BKR
						
						if (tran.getStatus().getStatusCode()
								.equalsIgnoreCase(TransactionStatus.SENT_CODE)) {
							tranStatus = TransactionStatus.SENT_CODE;
						} else {
							//MBO-2610 Commenting the bank reversal call when we change the status to CXL 
							// Do a bank refund/reversal if in CXL/BRR state
							/*long tmpTime = System.currentTimeMillis();
							tranService.doBankReversal(
									Integer.parseInt(form.getTranId()),
									up.getUID(), request.getContextPath());
							EventMonitor
									.getInstance()
									.recordEvent(
											EventListConstant.BANK_PAYMENT_SERVICE_ADJUSTMENT,
											tmpTime, System.currentTimeMillis());*/
							tranStatus = TransactionStatus.CANCELED_CODE;
						} 
						if (TransactionType.DELAY_SEND_CODE
								.equalsIgnoreCase(tran.getEmgTranTypeCode())) {
							fundStatus = TransactionStatus.BANK_REJECTED_CODE;
						} else if (TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE
								.equalsIgnoreCase(tran.getEmgTranTypeCode())) {
							fundStatus = TransactionStatus.ACH_RETURNED_CODE;
						}
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitBankRepresentRejDebit())) {
						Transaction tran = tm.getTransaction(tranId);

						if (tran.getStatus()
								.getStatusCode()
								.equalsIgnoreCase(
										TransactionStatus.CANCELED_CODE)
								&& tran.getStatus()
										.getSubStatusCode()
										.equalsIgnoreCase(
												TransactionStatus.BANK_REJECTED_CODE)) {
							// Change 1st status (CXL/BKR -> CXL/BKS)
							tran.setTranStatCode(TransactionStatus.CANCELED_CODE);
							tran.setTranSubStatCode(TransactionStatus.BANK_SETTLED_CODE);
							tran.setTranStatDate(new Date());
							tm.updateTransaction(tran, up.getUID());

							// set up for 2nd state transition
							tranStatus = TransactionStatus.CANCELED_CODE;
							fundStatus = TransactionStatus.BANK_REFUND_REQUEST_CODE;
						} else { // SEN/BKR -> SEN/BKS
							tranStatus = TransactionStatus.SENT_CODE;
							fundStatus = TransactionStatus.BANK_SETTLED_CODE;
						}

						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitBankWriteOffLoss())) {
						Transaction tran = tm.getTransaction(tranId);
						//SOF-162
						//if DSSEND then SEN/BWL, else update to SEN/RWL
						if(TransactionType.DELAY_SEND_CODE.equalsIgnoreCase(tran.getEmgTranTypeCode())){
							// Set up txn state transition to SEN/BWL
							tranStatus = TransactionStatus.SENT_CODE;
							fundStatus = TransactionStatus.BANK_WRITE_OFFLOSS_CODE;
						} else if (TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE.equalsIgnoreCase(tran.getEmgTranTypeCode())){
							tranStatus = TransactionStatus.SENT_CODE;
							fundStatus = TransactionStatus.WRITE_OFF_LOSS_CODE;
						} else{
							// Set up txn state transition to SEN/RWL
							tranStatus = TransactionStatus.SENT_CODE;
							fundStatus = TransactionStatus.RTBANK_WRITE_OFF_LOSS;
						}
						changeStatus = true;

					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitBankRecordVoid())) {
						// Set up txn state transition to CXL/BKV
						tranStatus = TransactionStatus.CANCELED_CODE;
						fundStatus = TransactionStatus.BANK_VOID_CODE;
						changeStatus = true;

					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitBankIssueRefund())) {
						// Handle a request for issuing bank refund

						// Do the Bank reversal/refund
						long tmpTime = System.currentTimeMillis();
						tranService.doBankReversal(
								Integer.parseInt(form.getTranId()),
								up.getUID(), request.getContextPath());
						EventMonitor
								.getInstance()
								.recordEvent(
										EventListConstant.BANK_PAYMENT_SERVICE_ADJUSTMENT,
										tmpTime, System.currentTimeMillis());

						// Set up txn state transition to CXL/BRR
						tranStatus = TransactionStatus.CANCELED_CODE;
						fundStatus = TransactionStatus.BANK_CANCEL_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitCcManualRefund())) {
						// Set up txn state transition to CXL/CCC
						fundStatus = TransactionStatus.CC_CANCEL_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitCcManualVoid())) {
						// Set up txn state transition to CXL/CCV
						fundStatus = TransactionStatus.CC_VOID_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form
							.getSubmitUndoDSError())) {
						// Set up txn state transition to APP/BKS
						tranStatus = TransactionStatus.APPROVED_CODE;
						fundStatus = TransactionStatus.BANK_SETTLED_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitMoveToReview())) {
						tranStatus = TransactionStatus.FORM_FREE_SEND_CODE;
						fundStatus = TransactionStatus.NOT_FUNDED_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitCancelTransaction())
								|| !StringHelper.isNullOrEmpty(form.getSubmitManCancelTran())) {
						///add code to call OSL proxy 
						 OSLClientExecutor exec = new OSLClientExecutor();
	                        CancelTransactionResponse oslResponse = exec.processTransactionCancelation(StringUtils.isNotBlank(form.getSubmitManCancelTran()), 
	                                                                                                up.getUID(), 
	                                                                                                tranId);
	                        if (oslResponse == null) {
	                            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("exception.unexpected"));
	                            saveErrors(request, errors);
	                        }
					}

					if (changeStatus) {
						Transaction oldTran = tm.getTransaction(tranId);
						if (oldTran.getTranStatCode().equalsIgnoreCase(
								tranStatus)
								&& oldTran.getTranSubStatCode()
										.equalsIgnoreCase(fundStatus)) {
							errors.add(ActionErrors.GLOBAL_ERROR,
									new ActionError(
											"error.transaction.token.invalid"));
							saveErrors(request, errors);
						} else {
							tranService.setTransactionStatus(tranId,
									tranStatus, fundStatus, up.getUID());
							if (!StringHelper.isNullOrEmpty(form
									.getSubmitAutoChargebackAndWriteOffLoss())) {
								tranService.setTransactionStatus(tranId,
										tranStatus,
										TransactionStatus.WRITE_OFF_LOSS_CODE,
										up.getUID());
							}
						}
					}
				} catch (TransactionAlreadyInProcessException e) {
					String[] parm = { id, e.getCurrentOwner() };
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"error.already.actively.being.processed", parm));
					saveErrors(request, errors);
				} catch (TransactionOwnershipException e) {
					String[] parm = { e.getCurrentOwner(), id };
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"error.already.owned.by.other.user", parm));
					saveErrors(request, errors);
				} catch (InvalidRRNException e) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"exception.invalid.param.value", e.getRRN(),
							"Receiver RRN"));
					saveErrors(request, errors);
				}
			} else {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"error.transaction.token.invalid"));
				saveErrors(request, errors);
			}
		}
		Transaction trans = tm.getTransaction(tranId);

		// If profile SCRTY_CHLNG_STAT_CODE was "CHALLENGED" AND an EMTAdmin
		// user MANUALLY DENIES a transaction, this profile status should be
		// changed to 'FAILED'

		if (trans != null
				&& trans.getTranStatCode().equalsIgnoreCase(
						TransactionStatus.DENIED_CODE)) {
			
			ConsumerProfileService consumerProfileService = emgshared.services.ServiceFactory
					.getInstance().getConsumerProfileService();
			ConsumerProfile consumerProfile = consumerProfileService
					.getConsumerProfile(trans.getSndCustId(), up.getUID(), null);
			if (consumerProfile.getRsaCustomerChallengeStatusCode() == ConsumerStatus.SECURITY_CHALLENGE_STATUS_CODE_CHALLENGE) {

				consumerProfile
						.setRsaCustomerChallengeStatusCode(ConsumerStatus.SECURITY_CHALLENGE_STATUS_CODE_FAILED);
				consumerProfileService.updateConsumerProfile(consumerProfile,
						true, up.getUID());
			}
		}

		// If profile SCRTY_CHLNG_STAT_CODE was "FAILED" AND an EMTAdmin user
		// MANUALLY UNDO DENIES a transaction, this profile status should be
		// changed to 'CHALLENGED'
		if (trans != null
				&& !trans.getTranStatCode().equalsIgnoreCase(
						TransactionStatus.DENIED_CODE)
				&& "Undo Deny Transaction".equalsIgnoreCase(form
						.getSubmitUndoDeny())) {
			ConsumerProfileService consumerProfileService = emgshared.services.ServiceFactory
					.getInstance().getConsumerProfileService();
			ConsumerProfile consumerProfile = consumerProfileService
					.getConsumerProfile(trans.getSndCustId(), up.getUID(), null);
			if (consumerProfile.getRsaCustomerChallengeStatusCode() == ConsumerStatus.SECURITY_CHALLENGE_STATUS_CODE_FAILED) {

				consumerProfile
						.setRsaCustomerChallengeStatusCode(ConsumerStatus.SECURITY_CHALLENGE_STATUS_CODE_CHALLENGE);
				consumerProfileService.updateConsumerProfile(consumerProfile,
						true, up.getUID());
			}
		}

		if (trans == null) {
			throw new EMGRuntimeException("Transaction " + tranId
					+ " no longer exists");
		}
		// get the transaction
		addToQuickList(request, "tranList", String.valueOf(tranId), 8);
		form.setTranId(trans.getEmgTranId() + "");
		form.setCustId(String.valueOf(trans.getCustId()));
		form.setTranStatusCode(trans.getTranStatCode());
		form.setTranStatusDesc(trans.getTranStatDesc());
		form.setTranSubStatusCode(trans.getTranSubStatCode());
		form.setTranSubStatusDesc(trans.getTranSubStatDesc());
		form.setTranLgcyRefNbr(trans.getLgcyRefNbr());
		if (trans.getReceiptTextInfoEng() != null) {
			request.getSession().setAttribute(
					Transaction.RCPT_TXT_INFO_SESSION,
					trans.getReceiptTextInfoEng());
		} else {
			request.getSession().removeAttribute(
					Transaction.RCPT_TXT_INFO_SESSION);
		}
		if (trans.getInternetPurchase() == null) {
			form.setInetPurchFlag("Not Available");
		} else if (trans.getInternetPurchase().equalsIgnoreCase("Y")) {
			form.setInetPurchFlag("Yes");
		} else if (trans.getInternetPurchase().equalsIgnoreCase("N")) {
			form.setInetPurchFlag("No");
		}
		// ConsumerAccountType primaryAccountType =
		// ConsumerAccountType.getInstance(trans.getSndAcctTypeCode());
		if (trans.getTranRvwPrcsCode() == null) {
			form.setTranRvwPrcsCode("MAN");
		} else {
			form.setTranRvwPrcsCode(trans.getTranRvwPrcsCode());
		}
		//
		if ((trans.getLgcyRefNbr() != null)
				&& EMTAdmDynProperties.getDPO(null).isDisplayMoneyGramDetails())
			form.setDisplayMoneygramDetails(true);
		else
			form.setDisplayMoneygramDetails(false);
		form.setTranOwnerId(trans.getCsrPrcsUserid());
		form.setTranSenderName(trans.getSndCustFrstName() + " "
				+ trans.getSndCustLastName());
		form.setTranSenderSecondLastName(trans.getSndCustScndLastName());
		if (trans.getEmgTranTypeCode().equalsIgnoreCase(
				TransactionType.EXPRESS_PAYMENT_SEND_CODE)) {
			form.setTranReceiverName(trans.getRcvAgcyCode());
			form.setTranRcvAgcyAcctMask("*****" + trans.getRcvAgcyAcctMask());
		} else {
			form.setTranReceiverName(trans.getRcvCustFrstName() + " "
					+ trans.getRcvCustLastName());
			form.setRcvCustFrstName(trans.getRcvCustFrstName());
			form.setRcvCustLastName(trans.getRcvCustLastName());
		}
		if (trans.getRcvAcctMaskNbr() != null) {
			if ((trans.getDlvrOptnId() == DeliveryOption.CARD_DEPOSIT_ID || trans
					.getDlvrOptnId() == DeliveryOption.BANK_DEPOSIT_ID)
					&& !(trans.getRcvAgentId() == Constants.PHL_SMARTMONEY_AGENTID))
				form.setRcvAcctMaskNbr(Constants.ACCT_MASK
						+ trans.getRcvAcctMaskNbr());
			else
				form.setRcvAcctMaskNbr(trans.getRcvAcctMaskNbr());
		} else {
			form.setRcvAcctMaskNbr(Constants.STR_EMPTY);
		}

		String currency = trans.getSndISOCrncyId();
		String receiveCurrency = (!trans.getRcvISOCrncyId().equals(
				trans.getRcvPayoutISOCrncyId())) ? trans
				.getRcvPayoutISOCrncyId() : trans.getRcvISOCrncyId();

		form.setTranFaceAmt(df.format(trans.getSndFaceAmt() != null ? trans
				.getSndFaceAmt().doubleValue() : 0) + currency);
		form.setTranSendFee(df.format(trans.getSndFeeAmt() != null ? trans
				.getSndFeeAmt().doubleValue() : 0) + currency);
		form.setTranReturnFee(df.format(trans.getRtnFeeAmt() != null ? trans
				.getRtnFeeAmt().doubleValue() : 0) + currency);
		form.setTranSendAmt(df.format(trans.getSndTotAmt() != null ? trans
				.getSndTotAmt().doubleValue() : 0) + currency);
		form.setExchangeRate(rateNumberFormat
				.format(trans.getSndFxCnsmrRate() != null ? trans
						.getSndFxCnsmrRate().doubleValue() : 0));
		form.setOtherFees(df.format(trans.getRcvNonMgiFeeAmt() != null ? trans
				.getRcvNonMgiFeeAmt() : 0)
				+ " "
				+ ((receiveCurrency != null && receiveCurrency.trim().length() > 0) ? receiveCurrency
						: currency));
		form.setOtherTaxes(df.format(trans.getRcvNonMgiTaxAmt() != null ? trans
				.getRcvNonMgiTaxAmt() : 0)
				+ " "
				+ ((receiveCurrency != null && receiveCurrency.trim().length() > 0) ? receiveCurrency
						: currency));
		form.setTotalReceiveAmount(df.format(trans.getRcvPayoutAmt() != null ? trans
				.getRcvPayoutAmt() : 0)
				+ " "
				+ ((receiveCurrency != null && receiveCurrency.trim().length() > 0) ? receiveCurrency
						: currency));

		form.setSndThrldWarnAmt(df.format(trans.getSndThrldWarnAmt()
				.doubleValue()));
		form.setSndFeeAmtNoDiscountAmt(df.format(trans
				.getSndFeeAmtNoDiscountAmt() != null ? trans
				.getSndFeeAmtNoDiscountAmt().doubleValue() : 0)
				+ currency);

		form.setOverThrldAmt(false);
		if (trans.getSndThrldWarnAmt().doubleValue() != 0) {
			form.setOverThrldAmt(trans.getSndFaceAmt().doubleValue() > trans
					.getSndThrldWarnAmt().doubleValue());
		}
		form.setTranType(trans.getEmgTranTypeCode());
		form.setTranTypeDesc(trans.getEmgTranTypeDesc());
		form.setTranSendCountry(trans.getSndISOCntryCode());
		form.setTranSendCurrency(trans.getSndISOCrncyId());
		form.setTranRecvCountry(trans.getRcvISOCntryCode());
		form.setTranPrimAcctType(trans.getSndAcctTypeCode());
		form.setTranPrimAcctMask(trans.getSndAcctMaskNbr());
		form.setTranSecAcctType(trans.getSndBkupAcctTypeCode());
		form.setTranSecAcctMask(trans.getSndBkupAcctMaskNbr());
		//MBO6924
		form.setGpnID(trans.getNetworkTransID());
		
		form.setTranCustIP(trans.getSndCustIPAddrId());
		form.setTranSndMsg1Text(trans.getSndMsg1Text());
		form.setTranSndMsg2Text(trans.getSndMsg2Text());
		//MBO-8239
		form.setSourceDevice(trans.getSourceDevice());
		form.setSourceApp(trans.getSourceApp());
		//MBO-8239
		//Added for MBO-10467 
		form.setPromoCode(trans.getPromoCode());
		form.setPromoDiscountType(trans.getPromoDiscountType());
		//MBO-10730
		form.setPromoDiscountAmount(df.format(trans.getPromoDiscountAmount() != null ? Double.valueOf(trans
				.getPromoDiscountAmount()) : 0) + currency);
		//MBO-10467 ended
		//MBO-3920
		form.setTranReceiverEmailMessage(trans.getReceiverEmailMessage());
		form.setSendCustAcctId(String.valueOf(trans.getSndCustAcctId()));
		form.setSendCustBkupAcctId(String.valueOf(trans.getSndCustBkupAcctId()));
		form.setTransScores(trans.getTransScores());
		form.setTranRiskScore(trans.getTranRiskScore());
		form.setTranRiskScoreDefaulted(trans.isTranRiskScoreDefaulted());
		form.setSysAutoRsltCode(trans.getSysAutoRsltCode());
		form.setScoreRvwCode("");
		form.setTranScoreCmntText("");
		if (trans.getTranScore() != null) {
			form.setScoreRvwCode(trans.getTranScore().getScoreRvwCode());
			form.setTranScoreCmntText(trans.getTranScore()
					.getTranScoreCmntText());
		}
		String deliveryOptionName = null;
		try {
			com.moneygram.agentconnect1305.wsclient.DeliveryOptionInfo delOptInfo = null;
			if (trans.getDlvrOptnId() == 15) { // FIXME Hardcoded for delivery
												// option 15 (WAP)
				delOptInfo = new com.moneygram.agentconnect1305.wsclient.DeliveryOptionInfo();
				delOptInfo.setDeliveryOptionID(new BigInteger("15"));
				delOptInfo.setDeliveryOption("ONLY_AT");
				delOptInfo.setDeliveryOptionName("ONLY_AT");
			} else {
				delOptInfo = acs.getDeliveryOptionInfo(trans.getDlvrOptnId());
			}
			deliveryOptionName = delOptInfo.getDeliveryOption();
		} catch (Exception ignore) {
			//
		}
		//MBO-12016
		if (null != trans.getEmgTranTypeCode()
				&& (trans.getEmgTranTypeCode().equalsIgnoreCase(
						TransactionType.MONEY_TRANSFER_CASH_TRANSACTION_CODE) || trans.getEmgTranTypeCode().equalsIgnoreCase(
								TransactionType.EXPRESS_PAYMENT_CASH_TRANSACTION_CODE))) {
			if(trans.getFormFreeConfNbr() != 0){
				form.setFormFreeConfNbr(String.valueOf(trans.getFormFreeConfNbr()));
			}
			else {
				form.setFormFreeConfNbr(null);
			}
		}
		if (deliveryOptionName == null) {
			deliveryOptionName = "";
		}
		form.setDeliveryOptionName(deliveryOptionName);
		form.setRcvRRN(trans.getRRN());
		// MGO-12721: SD# 3830128 - MGODE not populating Receive Currency
		if(trans.getRcvPayoutISOCrncyId() != null && !trans.getRcvPayoutISOCrncyId().equals(""))
		{
			form.setTranRecvCurrency(trans.getRcvPayoutISOCrncyId());
		} else {
			form.setTranRecvCurrency(trans.getRcvISOCrncyId());
		}
		String currencyName = "";
		try {

			// CurrencyInfo ci = acs.getCurrencyInfo(trans.getRcvISOCrncyId());
			CurrencyInfo ci = acs.getCurrencyInfo(trans
					.getRcvPayoutISOCrncyId());
			currencyName = ci.getCurrencyName();
		} catch (Exception ignore) {
			//
		}
		if (currencyName == null)
			currencyName = "";
		form.setTranRecvCurrencyName(currencyName);
		form.setRcvCustMatrnlName(trans.getRcvCustMatrnlName());
		form.setRcvCustMidName(trans.getRcvCustMidName());
		form.setRcvCustAddrStateName(trans.getRcvCustAddrStateName());
		form.setRcvAgentRefNbr(trans.getRcvAgentRefNbr());
		form.setRcvCustAddrLine1Text(trans.getRcvCustAddrLine1Text());
		form.setRcvCustAddrLine2Text(trans.getRcvCustAddrLine2Text());
		form.setRcvCustAddrLine3Text(trans.getRcvCustAddrLine3Text());
		form.setRcvCustAddrCityName(trans.getRcvCustAddrCityName());
		form.setRcvCustAddrPostalCode(trans.getRcvCustAddrPostalCode());
		form.setRcvCustDlvrInstr1Text(trans.getRcvCustDlvrInstr1Text());
		form.setRcvCustDlvrInstr2Text(trans.getRcvCustDlvrInstr2Text());
		form.setRcvCustDlvrInstr3Text(trans.getRcvCustDlvrInstr3Text());
		form.setRcvCustPhNbr(trans.getRcvCustPhNbr());
		form.setRcvAgtCity(trans.getRcvAgtCity());
		form.setRcvAgtState(trans.getRcvAgtState());
		form.setRcvAgtCntry(trans.getRcvAgtCntry());
		form.setRcvDate(null);
		form.setRetrievalRequestDate(trans.getFundRetrievalRequestDate());
		DateFormatter df = new DateFormatter("dd/MMM/yyyy hh:mm a", true);

		DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy hh:mm a z");
		dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone(trans.getPartnerSiteId())));
		form.setCreateDate(dateFormat.format(trans.getCreateDate()));

		form.setSndCustOccupationText(trans.getSndCustOccupationText());
		// Changes for Defect 711 starts
		// form.setCustomerAutoEnrollFlag(trans.getCustomerAutoEnrollFlag());
		if (trans.getLoyaltyPgmMembershipId() != null
				&& !(trans.getLoyaltyPgmMembershipId().equals(""))) {
			form.setCustomerAutoEnrollFlag("Y");
		} else {
			form.setCustomerAutoEnrollFlag("N");
		}
		// changes for Defect 711 ends
		form.setTestQuestion(trans.getTestQuestion());
		form.setTestQuestionAnswer(trans.getTestQuestionAnswer());
		form.setLoyaltyPgmMembershipId(trans.getLoyaltyPgmMembershipId());
		form.setSndCustPhotoId(trans.getSndCustPhotoId());
		form.setSndCustPhotoIdCntryCode(trans.getSndCustPhotoIdCntryCode());
		form.setSndCustPhotoIdStateCode(trans.getSndCustPhotoIdStateCode());
		form.setSndCustPhotoIdTypeCode(trans.getSndCustPhotoIdTypeCode());
		if (trans.getPartnerSiteId().equals(
				EMoneyGramAdmApplicationConstants.MGO_PARTNER_SITE_ID)
				|| trans.getPartnerSiteId().equals(
						EMoneyGramAdmApplicationConstants.WAP_PARTNER_SITE_ID)) {
			if (!StringUtility.isNullOrEmpty(form.getSndCustPhotoIdTypeCode())) {
				if (EMoneyGramAdmApplicationConstants.photoIdStringToIdType
						.get(form.getSndCustPhotoIdTypeCode())
						.equals(EMoneyGramAdmApplicationConstants.photoIdTypeDriverLicense)
						|| EMoneyGramAdmApplicationConstants.photoIdStringToIdType
								.get(form.getSndCustPhotoIdTypeCode())
								.equals(EMoneyGramAdmApplicationConstants.photoIdTypeStateId)) {
					form.setSndCustPhotoIdStateCode(EMoneyGramAdmApplicationConstants.photoIdState);
					form.setSndCustPhotoIdStateCode(trans
							.getSndCustPhotoIdStateCode());
				} else {
					form.setSndCustPhotoIdStateCode("");
				}
			}
		} else {
			if (!StringUtility.isNullOrEmpty(form.getSndCustPhotoIdTypeCode())) {
				String photoIdTypeCode = EMoneyGramAdmApplicationConstants.photoIdStringToIdType
						.get(form.getSndCustPhotoIdTypeCode());
				if (photoIdTypeCode != null && (photoIdTypeCode
						.equals(EMoneyGramAdmApplicationConstants.photoIdTypeAlienId)
						|| photoIdTypeCode
								.equals(EMoneyGramAdmApplicationConstants.photoIdTypePassport)
						|| photoIdTypeCode
								.equals(EMoneyGramAdmApplicationConstants.photoIdTypeGovernmentId))) {
					form.setSndCustPhotoIdStateCode("");
				}
			}
		}
		form.setSndCustPhotoIdExpDate(trans.getSndCustPhotoIdExpDate());
		String legalId = trans.getSndCustLegalId();
		/*
		 * if(trans.getPartnerSiteId().equals(EMoneyGramAdmApplicationConstants.
		 * MGODE_PARTNER_SITE_ID)) { legalId =
		 * decryptService.decryptSSN(legalId); }
		 */

		form.setSndCustLegalId(legalId);
		form.setSndCustLegalIdTypeCode(trans.getSndCustLegalIdTypeCode());
		form.setPartnerSiteId((String) request.getParameter("partnerSiteId"));
		form.setTransPartnerSiteId(trans.getPartnerSiteId());
		if (StringHelper.isNullOrEmpty(trans.getMccPartnerProfileId())) {
			form.setMccPartnerProfileId("N/A");
		} else {
			// form.setMccPartnerProfileId(EMoneyGramMerchantIds.getMerchantMap().get(trans.getMccPartnerProfileId()));
			form.setMccPartnerProfileId(trans.getMccPartnerProfileId());
		}
		if (trans.getRcvDate() != null) {
			form.setRcvDate(df.format(trans.getRcvDate()));
		}
		CustAddress ca = tm.getCustAddress(trans.getSndCustAddrId());
		if (ca != null) {
			form.setTranSendCity(ca.getAddrCityName());
			form.setTranSendState(ca.getAddrStateName());
			form.setAddressLine1(ca.getAddrLine1Text());
			form.setAddressLine2(ca.getAddrLine2Text());
			form.setCity(ca.getAddrCityName());
			form.setState(ca.getAddrStateName());
			form.setPostalCode(ca.getAddrPostalCode());			
			form.frameConsumerAddress();
		}
		if (tm.getVBVBeans(tranId).size() != 0) {
			request.getSession().setAttribute("vbvExist", "y");
		} else {
			request.getSession().removeAttribute("vbvExist");
		}
		// setup application scope comment reason codes
		ServletContext sc = request.getSession().getServletContext();
		if (sc.getAttribute("reasons") == null) {
			sc.setAttribute("reasons", tm.getTransactionCommentReasons());
		}
		// get transaction actions
		request.getSession().setAttribute(
				"tranActions",
				getTranActions(request, tm, tranId, up.getUID(),
						form.getTransPartnerSiteId()));
		// get transaction comments in session scope
		request.getSession().setAttribute("comments",
				tm.getTransactionComments(tranId));
		// set message for ACH Refund Requested type of fund status
		//IF sub status=ARR OR provider code = PWMB and status/sub status is CXL and AIP
		if (TransactionStatus.ACH_REFUND_REQUESTED_CODE.equals(trans.getTranSubStatCode()) 
				|| (
						Constants.PWMB_PROVIDER_CODE==trans.getTCProviderCode() 
						&& TransactionStatus.ACH_IN_PROCESS_CODE.equals(trans.getTranSubStatCode()) 
						&& TransactionStatus.CANCELED_CODE.equals(trans.getTranStatCode())
					)
			) {
			setARRMsg(request, trans);
		}
		// transaction security matrix setup in session scope
		TranSecurityMatrix matrix = new TranSecurityMatrix(request, up, trans,
				recoveredAmt);
		// see if we should look for a chargeback tracking record or allow
		// creation of one
		// must be in appropriate funding status.
		// boolean chgBckTrackingAllowed = false;
		String chgBckTrackingViewInd = "VIEW";
		String chargeBackTrackingHyperLink = null;
		// if the user has permission and it's not ESSEND, which doesn't allow
		// CC funding,
		// then check to see if Tracking record exists, if so provide a link to
		// it, else
		// if the trans is in a funding status that allows a Tracking record,
		// then provide
		// a link to add the Tracking record
		if ((up.hasPermission("ccChargebackTracking"))
				&& (!trans.getEmgTranTypeCode().equals(
						TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE))) {
			// chgBckTrackingAllowed = true;
			// see if an record already exists
			ChargebackTransactionHeader cbth = null;
			try {
				cbth = tm.getChargebackTransactionHeader(up.getUID(), tranId);
			} catch (Exception e) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.error("Error attempting to get ChargebackHeader", e);
			}
			if (cbth != null) {
				chargeBackTrackingHyperLink = "ccChargebackTracking.do?tranId="
						+ tranId;
				chgBckTrackingViewInd = "VIEW";
			} else if ((trans.getTranSubStatCode()
					.equals(TransactionStatus.CC_CHARGEBACK_CODE))
					|| (trans.getTranSubStatCode()
							.equals(TransactionStatus.CC_SALE_CODE))
					|| (trans.getTranSubStatCode()
							.equals(TransactionStatus.RECOVERY_OF_LOSS_CODE))
					|| (trans.getTranSubStatCode()
							.equals(TransactionStatus.RECOVERY_OF_LOSS_CYBERSOURCE_CODE))
					|| (trans.getTranSubStatCode()
							.equals(TransactionStatus.WRITE_OFF_LOSS_CODE))) {
				chargeBackTrackingHyperLink = "ccChargebackTracking.do?tranId="
						+ tranId;
				chgBckTrackingViewInd = "ADD";
			}
		}
		if (up.hasPermission("showTranScoreDetail")) {
			request.setAttribute("showTranScoreDetail", "Y");
		}
		request.getSession().setAttribute("chargeBackTrackingHyperLink",
				chargeBackTrackingHyperLink);
		request.getSession().setAttribute("chgBckTrackingViewInd",
				chgBckTrackingViewInd);
		request.getSession().setAttribute("tranAuth", matrix);
		form.setEsMGSendFlag(matrix.getEsSendMG());

		// for story 4051.
		if (firstTimePageLoad) {
			if (form.getConsumerCountryCode() != null
					&& form.getTranSendCountry() != null
					&& form.getConsumerCountryCode().equalsIgnoreCase(
							form.getTranSendCountry())) {
				form.setCountrySameFlag(true);
			} else {
				form.setCountrySameFlag(false);
			}
		}
		// remove transaction ownership if the user checked the release
		// option at the confirmation screen
		if (request.getAttribute("action") != null
				&& request.getAttribute("actionDone") == null) {
			request.setAttribute("actionDone", "Y");
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRANS_OWNERSHIP);
		} else {
			saveToken(request);
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		}
	}


	private void doAutoRefund(TransactionDetailForm form, UserProfile up,
			TransactionManager tm, ActionErrors errors,
			HttpServletRequest request, boolean cancelMainframe)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException, DataSourceException, SQLException {

		Transaction tmp = tm.getTransaction(Integer.parseInt(form.getTranId()));
		TransactionService tranService = ServiceFactory.getInstance()
				.getTransactionServicePymtProvider(tmp.getTCProviderCode(),
						tmp.getPartnerSiteId());

		int tranId = Integer.parseInt(form.getTranId());
		if (cancelMainframe) {
			try {
				tranService.refundPersonToPerson(tranId, up.getUID());
			} catch (AgentConnectException e) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"error.agent.connect.failed"));
				saveErrors(request, errors);
				return;
			} catch (EMGRuntimeException rt) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"error.cancel.mainframe.failed"));
				saveErrors(request, errors);
				return;
			}
		}
		//MBO-2813 changes, added the EWL status
		if (form.getTranSubStatusCode().equalsIgnoreCase(
				TransactionStatus.CC_SALE_CODE)
				|| form.getTranSubStatusCode().equalsIgnoreCase(
						TransactionStatus.EXCEPTION_WRITE_OFF_LOSS_CODE)) {
			tranService.doCreditCardReversal(tranId, up.getUID(),
					request.getContextPath());
		}

	}

	private List getTranActions(HttpServletRequest request,
			TransactionManager tm, int tranId, String logonId,
			String partnerSiteID) throws DataSourceException, SQLException {

		float amount = 0F;
		TranAction appTranAction = null;
		TranAction senTranAction = null;
		List tas = tm.getTranActions(tranId, logonId);
		List tavs = new ArrayList();
		Iterator iter = tas.iterator();
		String timeZone = timeZone(partnerSiteID);
		while (iter.hasNext()) {
			TranAction ta = (TranAction) iter.next();
			if (ta.getTranStatCode().equalsIgnoreCase(
					TransactionStatus.SENT_CODE)
					&& ta.getTranSubStatCode().equalsIgnoreCase(
							TransactionStatus.RECOVERY_OF_LOSS_CODE)) {
				amount = +ta.getTranPostAmt().floatValue();
			}
			TranActionView tav = new TranActionView();
			tav.setTranActionId(ta.getTranActionId());
			tav.setTranId(ta.getTranId());
			// MGO-3313: When Action is Agent AR/AP transaction description
			// should be Refund
			if (ta.getDrGLAcctId() == EMoneyGramAdmApplicationConstants.GL_ACCOUNT_CODE_AGENT_AR_AP
					&& ta.getTranReasDesc().equals("Fee Refund")) {
				tav.setTranReasDesc("Refund");
			} else {
				tav.setTranReasDesc(ta.getTranReasDesc());
			}
			tav.setTranReasCode(ta.getTranReasCode());
			tav.setTranActnDate(ta.getTranActionDate());
			tav.setTranOldStatCode(ta.getTranOldStatCode());
			tav.setTranOldSubStatCode(ta.getTranOldSubStatCode());
			tav.setTranStatCode(ta.getTranStatCode());
			tav.setTranSubStatCode(ta.getTranSubStatCode());
			tav.setTranConfId(ta.getTranConfId());
			tav.setPartnerSiteId(partnerSiteID);
			tav.setTranPostAmt(ta.getTranPostAmt());
			tav.setTranDrGlAcctId(ta.getDrGLAcctId());
			tav.setTranCrGlAcctId(ta.getCrGLAcctId());
			if (StringHelper.isNullOrEmpty(ta.getTranCreateUserid())
					|| ta.getTranCreateUserid().equalsIgnoreCase("java")) {
				tav.setTranCreateUserid("emgwww");
			} else {
				tav.setTranCreateUserid(ta.getTranCreateUserid());
			}
			tav.setTranDrGlAcctDesc(ta.getDrGLAcctDesc());
			tav.setTranCrGlAcctDesc(ta.getCrGLAcctDesc());
			tav.setSubReasonCode(ta.getSubReasonCode());
			tav.setTranSubReasDesc(ta.getTranSubReasDesc());
			tav.setTranReasTypeCode(ta.getTranReasTypeCode());
			tav.setDate(ta.getTranActionDate());
			tavs.add(tav);
			if (TransactionStatus.APPROVED_CODE.equals(ta.getTranStatCode())
					&& TransactionStatus.ACH_SENT_CODE.equals(ta
							.getTranSubStatCode())) {
				appTranAction = ta;
			}
			if (TransactionStatus.SENT_CODE.equals(ta.getTranStatCode())
					&& TransactionStatus.ACH_SENT_CODE.equals(ta
							.getTranSubStatCode())) {
				senTranAction = ta;
			}
		}
		if (tavs.size() > 1) {
			Collections.sort(tavs);
		}
		recoveredAmt = new BigDecimal(Math.round(amount * 100F) / 100F);
		request.setAttribute("appTranAction", appTranAction);
		request.setAttribute("senTranAction", senTranAction);
		request.setAttribute("userTimeZone",timeZone);
		return tavs;
	}

	private void changeTranStatus(int tranId, String tranStatus,
			String fundStatus, String userId, String partnerSiteId)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		TransactionService tranService = ServiceFactory.getInstance()
				.getTransactionService(partnerSiteId);
		tranService
				.setTransactionStatus(tranId, tranStatus, fundStatus, userId);
	}

	/**
	 * This method make the manual record for not same day cancellation
	 * 
	 * @param tranId
	 * @param tranStatus
	 * @param fundStatus
	 * @param userId
	 * @param partnerSiteId
	 * @throws TransactionAlreadyInProcessException
	 * @throws TransactionOwnershipException
	 */
	private void changeTranStatusRefundManual(int tranId, String tranStatus,
			String fundStatus, String userId, String partnerSiteId,
			TransactionManager tm) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		// Get the transaction
		Transaction transaction = tm.getTransaction(tranId);
		// Create the transaction service according the pymt_prvdr_srv_code
		TransactionService tranService = ServiceFactory.getInstance()
				.getTransactionServicePymtProvider(
						transaction.getTCProviderCode(),
						transaction.getPartnerSiteId());
		// Set the transaction status for the transaction
		tranService
				.setTransactionStatus(tranId, tranStatus, fundStatus, userId);
	}

	private List buildSubReasonList(String subReasonType) throws Exception {
		try {
			List subReasons = emgshared.services.ServiceFactory.getInstance()
					.getTransSubReasonService()
					.getTranSubReasonsForTypeCode(subReasonType);
			List tranSubReasons = new ArrayList();
			for (Iterator i = subReasons.iterator(); i.hasNext();) {
				TranSubReason tsr = (TranSubReason) i.next();
				tranSubReasons.add(new LabelValueBean(tsr
						.getTranSubReasonDesc()
						+ " - "
						+ tsr.getTranSubReasonCode(), tsr
						.getTranSubReasonCode()));
			}
			return tranSubReasons;
		} catch (Exception e) {
			throw e;
		}
	}

	private void buildSubReasonListBox(HttpServletRequest request,
			String buildForStatusType) throws Exception {
		try {
			List subReasons = emgshared.services.ServiceFactory.getInstance()
					.getTransSubReasonService()
					.getTranSubReasonsForTypeCode(buildForStatusType);
			List tranSubReasons = new ArrayList();
			for (Iterator i = subReasons.iterator(); i.hasNext();) {
				TranSubReason tsr = (TranSubReason) i.next();
				tranSubReasons.add(new LabelValueBean(tsr
						.getTranSubReasonDesc()
						+ " - "
						+ tsr.getTranSubReasonCode(), tsr
						.getTranSubReasonCode()));
			}
			request.getSession().setAttribute("TranSubReasons", tranSubReasons);
		} catch (Exception e) {
			throw e;
		}
	}

	private void setARRMsg(HttpServletRequest request, Transaction trans) {
		//PWMB-797
		log.info("inside setARRMsg with provider code = " + trans.getTCProviderCode());
		String partialMessage="show";
		if (!TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE.equals(trans
				.getEmgTranTypeCode())
				&& !TransactionType.MONEY_TRANSFER_SEND_CODE.equals(trans
						.getEmgTranTypeCode())) {
			return;
		}
		//PWMB-797
		if(trans.getTCProviderCode()==Constants.PWMB_PROVIDER_CODE){
			log.info("inside setARRMsg with getTranSubStatCode() = " + trans.getTranSubStatCode());
			//For PWMB ARR no need to show message
			if(TransactionStatus.ACH_REFUND_REQUESTED_CODE.equals(trans.getTranSubStatCode())){
				return;
			}
			//For PWMB - CXL/AIP show partial message
			if(TransactionStatus.ACH_IN_PROCESS_CODE.equals(trans.getTranSubStatCode()) && TransactionStatus.CANCELED_CODE.equals(trans.getTranStatCode())){
				partialMessage = "hide";
			}
			
		}
		TranAction appTranAction = null;
		TranAction senTranAction = null;
		TranAction ta = null;
		if (request.getAttribute("appTranAction") != null) {
			appTranAction = (TranAction) request.getAttribute("appTranAction");
			ta = appTranAction;
			request.removeAttribute("appTranAction");
		}
		if (request.getAttribute("senTranAction") != null) {
			senTranAction = (TranAction) request.getAttribute("senTranAction");
			ta = senTranAction;
			request.removeAttribute("senTranAction");
		}
		// Cancel after money gram send, no message
		if (appTranAction != null && senTranAction != null) {
			return;
		}
		Calendar cal = Calendar.getInstance();
		if(ta!=null)
		{
			cal.setTime(ta.getTranActionDate());
		}
		cal.add(Calendar.HOUR, -5);
		Date recordDate = DateFormatter.getAchSendDay(
				cal.getTime(),
				(Map) request.getSession().getServletContext()
						.getAttribute("holidayMap"), EMTAdmDynProperties
						.getDPO(null).getEsSendWaitDays());
		DateFormatter df = new DateFormatter("yyyyMMdd", true);
		String recDate = df.format(recordDate);
		if (df.format(new Date(System.currentTimeMillis())).compareTo(recDate) < 0) {
			cal.setTime(recordDate);
			int clearTime = EMTAdmDynProperties.getDPO(null).getEsClearTime();
			cal.set(Calendar.HOUR_OF_DAY, clearTime / 100);
			cal.set(Calendar.MINUTE, clearTime % 100);
			recordDate = cal.getTime();
			df = new DateFormatter("dd/MMM/yyyy hh:mm a", true);
			request.setAttribute("ARRDate", df.format(recordDate));
		}
		//PWMB-797
		request.setAttribute("partialMessage",partialMessage);
	}

	private static IPDetails getIPDetailsFromLookupService(String ipAddress,
			boolean isIpLookuponScoring) {

		IPDetails ipDetails = IPLocatorService.getIPDetailsFromLookupService(ipAddress,
				isIpLookuponScoring);

		return ipDetails;
	}
	
	private void regenerateSentEmail(TransactionManager tm, int tranId, UserProfile up) throws DataSourceException, SQLException {

		  Transaction transaction = tm.getTransaction(tranId);
		  ConsumerProfileService consumerProfileService = emgshared.services.ServiceFactory.getInstance().getConsumerProfileService();
    
		  ConsumerProfile consumerProfile = consumerProfileService.getConsumerProfile(transaction.getSndCustId(),up.getUID(), null);
		  String resendEmailAddress = EMTAdmContainerProperties.getRegenerateDestEmailAddress(); 
		  ConsumerEmail ce = new ConsumerEmail(transaction.getSndCustId(), resendEmailAddress, ElectronicTypeCode.EMAIL, EmailStatus.ACTIVE, TaintIndicatorType.NOT_BLOCKED, TaintIndicatorType.NOT_BLOCKED);
		  NotificationAccess na = new NotificationAccessImpl();
		  //MBO-1926 Line added for setting the mail id as mgodev_regenerate_email@mailinator.com
		  transaction.setSndCustLogonId(resendEmailAddress);
		//MBO-5319 - Modified Method signature to use Profile object, will be useful in future if any
		  //value needs to be fetched from Profile
		  boolean sendSMS = false;
		  
		  //EMT-1667
		  if(TransactionType.EXPRESS_PAYMENT_SEND_CODE.equalsIgnoreCase(transaction.getEmgTranTypeCode())){
			  na.notifyExpressPaymentTransaction(transaction, ce, transaction.getTranLanguage());
		  }else{
			  na.sendRegenEmails(transaction, transaction.getTranLanguage(),consumerProfile);
		  }
		  //ended
		  
		  //log comment indicating the regenerate email was sent
		  tm.setTransactionComment(tranId,"OTH", "Regenerating Tran Sent email to: " + resendEmailAddress, up.getUID());
	}
	
	//MBO-2813, New private method to add that will call AC ‘detailLookup’ to see if the transaction is received already .
	private boolean isTransactionReceived(TransactionManager tm,int tranId,HttpServletRequest request, ActionForm formObj)throws Exception{
		Transaction transaction = tm.getTransaction(tranId);
		DetailLookupResponse resp = null;
		AgentConnectAccessor agentConnectAccesor = null;
		TransactionDetailForm form = (TransactionDetailForm) formObj;
		String lgcyRefNbr = transaction.getLgcyRefNbr();
		String partnerSiteId = transaction.getPartnerSiteId();
		
		//MBO-6101
		log.info("inside isTransactionReceived method for Tran# " + tranId);
		log.info("going to initialize AC");
		EMTSharedSourceSiteCacheService cacheService = new EMTSharedSourceSiteCacheServiceImpl();
		MGOAgentProfile mgoAgentProfile = cacheService.getACDetails(partnerSiteId,form.getTranType());
		log.info("agent profile is got from cahce service");
		/*MGOAgentProfileService maps = MGOServiceFactory.getInstance().getMGOAgentProfileService(); 
		MGOAgentProfile mgoAgentProfile = maps.getMGOProfile(partnerSiteId,form.getTranType());*/
		
		agentConnectAccesor = new AgentConnectAccessor(
				PropertyLoader.getInstance(CONTAINER_RESOURCE_URI,
		                "EMT Admin Container ", log).getRawProperties(), 
				mgoAgentProfile);
		boolean result= true;
		System.out.println("transaction.getRcvDate() " + transaction.getRcvDate());
		if(transaction.getRcvDate()!= null)
		{
			 //if we have a receive date in EMG DB, it has been received
		}
		else{
			try {
				System.out.println("Going to check detailLookup");
				
				resp = agentConnectAccesor.detailLookup(lgcyRefNbr);
				System.out.println("resp.getTransactionStatus().getValue() " + resp.getTransactionStatus().getValue());
				if("SEN".equals(resp.getTransactionStatus().getValue())){
					System.out.println("Inside if of SEN");
					result= false;
				}
			} catch (Exception exception) {
				EMGSharedLogger.getLogger("Exception while checking the transactions as received"+ exception );
			}		
		}
		return result;
	}

	/**
	 * This method calls the cache implementation for GetSourceSiteInfo API of
	 * Configuration Service to return the time zone
	 * 
	 * @param partnerSite
	 * @throws Exception
	 */
	public String timeZone(String partnerSite){
		String timeZone = null;
		EMTSharedSourceSiteCacheService proxy = EMTSharedSourceSiteCacheServiceImpl.instance();
		try {
			if(null != partnerSite){
			GetSourceSiteInfoResponse resp =proxy.sourceSiteDetails(partnerSite);
			timeZone =resp.getCountryInfo().getTimeZone();
			}
			} catch (Exception e) {
			log.error("Exception in calling the Configuration service for time zone", e);
		}
		return timeZone;
		
	}	
	
	
}
