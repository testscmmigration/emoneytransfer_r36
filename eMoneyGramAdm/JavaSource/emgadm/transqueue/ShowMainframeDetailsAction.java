/*
 * Created on Apr 11, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.transqueue;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import shared.mgo.dto.MGOAgentProfile;
import shared.mgo.services.AgentConnectAccessor;

import com.moneygram.agentconnect1305.wsclient.DetailLookupResponse;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.dataaccessors.TransactionManager;
import emgshared.cache.locator.EMTSharedSourceSiteCacheService;
import emgshared.cache.locator.EMTSharedSourceSiteCacheServiceImpl;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.Transaction;
import emgshared.property.PropertyLoader;

/**
 * @author T348
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ShowMainframeDetailsAction extends EMoneyGramAdmBaseAction {

	private static final String FORWARD_SUCCESS = "success";
	private static final String FORWARD_FAILURE = "failure";
	private static final String CONTAINER_RESOURCE_URI = "java:comp/env/rep/EMTAdminResourceReference";
	protected static Logger LOGGER = EMGSharedLogger.getLogger(ShowMainframeDetailsAction.class);
		
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
			throws Exception
		{
			ActionErrors errors = new ActionErrors();
			String lgcyRefNbr = request.getParameter("tranLgcyRefNbr");
			String partnerSiteId = request.getParameter("partnerSiteId");

			AgentConnectAccessor agentConnectAccesor = null;
			//MBO-6101
			LOGGER.info("ShowMainframeDetailsAction - Getting AC details from Source API");
			ShowMainframeDetailsForm smdf = (ShowMainframeDetailsForm) form;
			/*MGOAgentProfileService maps = MGOServiceFactory.getInstance().getMGOAgentProfileService();			
			MGOAgentProfile mgoAgentProfile = maps.getMGOProfile(partnerSiteId, smdf.getTranType());*/
			EMTSharedSourceSiteCacheService cacheService = new EMTSharedSourceSiteCacheServiceImpl();
			MGOAgentProfile mgoAgentProfile = cacheService.getACDetails(partnerSiteId, smdf.getTranType());
			if(smdf.getTranType().equals("MGCASH")){
				TransactionManager tm = getTransactionManager(request);
				Transaction tran = tm.getTransaction(Integer.valueOf(smdf.getId()));
				mgoAgentProfile.setAgentId(String.valueOf(tran.getSndAgentId()));
			}
			agentConnectAccesor = new AgentConnectAccessor(
					PropertyLoader.getInstance(CONTAINER_RESOURCE_URI,
			                "EMT Admin Container ", LOGGER).getRawProperties(), 
					mgoAgentProfile);
			//ended			
			DetailLookupResponse resp = null;

			try {
				resp = agentConnectAccesor.detailLookup(lgcyRefNbr);
			} catch (Exception e) {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				LOGGER.error(e.getMessage(),e);
				String msg = sw.toString();
				try {
					pw.close();
					sw.close();
				} catch (Exception e1) {}

				if (msg.indexOf("Invalid reference number") > 0 ||
					msg.indexOf("AGT CONNECT REF # IS NULL") > 0) {
					errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError("error.data.no.longer.available",
						lgcyRefNbr));
					saveErrors(request, errors);
					return mapping.findForward(FORWARD_FAILURE);
				} else {
					throw new EMGRuntimeException(e);
				}
			}
			
			populateForm((ShowMainframeDetailsForm)form, resp,request);
			
			return mapping.findForward(FORWARD_SUCCESS);
		}
		
		public void populateForm(ShowMainframeDetailsForm form, DetailLookupResponse resp,HttpServletRequest request)
		{
			form.setTransactionStatus(resp.getTransactionStatus().getValue());
			form.setDateTimeSent(resp.getDateTimeSent());
			form.setFeeAmount(resp.getSendAmounts().getTotalSendFees());
			form.setSendAmount(resp.getSendAmounts().getDetailSendAmounts()[0].getAmount());
			form.setSendCurrency(resp.getSendAmounts().getDetailSendAmounts()[0].getAmountCurrency());
			form.setReceiveAmount(resp.getReceiveAmounts().getTotalReceiveAmount());
			form.setReceiveCurrency(resp.getReceiveAmounts().getReceiveCurrency());
			form.setReceiveCountry(resp.getReceiveCountry());
			form.setDeliveryOption(resp.getDeliveryOption());
			form.setSenderFirstName(resp.getSenderFirstName());
			form.setSenderMiddleInitial(resp.getSenderMiddleName());
			form.setSenderLastName(resp.getSenderLastName());
			form.setSenderAddress(resp.getSenderAddress());
			form.setSenderCity(resp.getSenderCity());
  			form.setSenderState(resp.getSenderState());
		  	form.setSenderZipCode(resp.getSenderZipCode());
		  	form.setSenderCountry(resp.getSenderCountry());
		  	form.setSenderHomePhone(resp.getSenderHomePhone());
		  	form.setReceiverFirstName(resp.getReceiverFirstName());
		  	form.setReceiverMiddleInitial(resp.getReceiverMiddleName());
		  	form.setReceiverLastName(resp.getReceiverLastName());
		  	form.setReceiverLastName2(resp.getReceiverLastName2());
		  	form.setReceiverAddress(resp.getReceiverAddress());
		  	form.setReceiverCity(resp.getReceiverCity());
		  	form.setReceiverState(resp.getReceiverState());
		  	form.setReceiverZipCode(resp.getReceiverZipCode());
		  	form.setReceiverCountry(resp.getReceiverCountry());
		  	form.setReceiverPhone(resp.getReceiverPhone());
		  	form.setDirection1(resp.getDirection1());
		  	form.setDirection2(resp.getDirection2());
		  	form.setDirection3(resp.getDirection3());
		  	form.setMessageField1(resp.getMessageField1());
		  	form.setMessageField2(resp.getMessageField2());
		  	form.setSenderBirthCity(resp.getSenderBirthCity()); 
		  	form.setSenderBirthCountry(resp.getSenderBirthCountry());
		  	form.setReceiverColonia("");
		  	form.setReceiverMunicipio("");
		  	form.setOperatorName(resp.getOperatorName());
		  	form.setValidIndicator(resp.getValidIndicator());
		  	form.setAgentUseSendData(resp.getAgentUseSendData());
		  	form.setAgentUseReceiveData(""); 
		  	form.setPartnerCustomerReceiveNumber("");
		  	form.setExpectedDateOfDelivery(resp.getExpectedDateOfDelivery());
		  	form.setMgCustomerReceiveNumber(resp.getCustomerReceiveNumber()); 
		if (request.getSession()
				.getAttribute(Transaction.RCPT_TXT_INFO_SESSION) != null) {
			form.setReceiptTextInfoEng((String) request.getSession()
					.getAttribute(Transaction.RCPT_TXT_INFO_SESSION));
		} else
			form.setReceiptTextInfoEng(null);
		}
}
