package emgadm.transqueue;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionMapping;

import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.AddressHelper;
import emgadm.view.ConsumerBankAccountView;
import emgadm.view.ConsumerCreditCardAccountView;
import emgadm.view.ConsumerProfileCommentView;
import emgadm.view.TransactionView;
import emgshared.model.ConsumerProfileActivity;
import emgshared.model.CustomerIDUploadInfo;
import emgshared.model.LexisNexisActivity;
import emgshared.util.PhoneNumberFormatter;

public class ShowCustomerProfileForm extends EMoneyGramAdmBaseValidatorForm
{
	private static final int DEFAULT_COMMENT_DISPLAY_LIMIT = 5;
	private String custId;
	private String custLogonId;
	private String custStatus;
	private String custLastName;
    private String custSecondLastName;
	private String custFirstName;
	private String custMiddleName;
	private String custHashedPassword;
	private String custSsn;
	private String saveCustSsn;
	private String custSsnTaintText;
	private String brthDate;
	private String custAge;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	private String isoCountryCode;
	private String postalCode;
	//MBO-3603
	private String postalCodeForDisplay;
	public String getPostalCodeForDisplay() {
		return postalCodeForDisplay;
	}
	//MBO-3603

	private String zip4;
	private String countyName; 
	private String phoneNumber;
	private String phoneNumberTaintText;
	private String phoneNumberAlternate;
	private String phoneNumberAlternateTaintText;
	private String emailAddress;
	private String emailAddressTaintText;
	private String emailDomainTaintText;
	private String createdIPAddress;
	private boolean isEmailActive;
	private ConsumerBankAccountView[] bankAccounts = new ConsumerBankAccountView[0];
	private ConsumerCreditCardAccountView[] creditCardAccounts = new ConsumerCreditCardAccountView[0];
	private ConsumerCreditCardAccountView[] debitCardAccounts = new ConsumerCreditCardAccountView[0];
	private ConsumerCreditCardAccountView[] maestroAccounts = new ConsumerCreditCardAccountView[0];
	private List custStatusOptions = new ArrayList(0);
	private List accountStatusOptions = new ArrayList(0);
	private List customerCommentReasons = new ArrayList(0);
	private ConsumerProfileCommentView[] comments = new ConsumerProfileCommentView[0];
	private ConsumerProfileActivity[] consumerProfileActivity = new ConsumerProfileActivity[0];
	private int commentDisplayLimit = DEFAULT_COMMENT_DISPLAY_LIMIT;
	private TransactionView[] epTransactions = new TransactionView[0];
	private TransactionView[] mgTransactions = new TransactionView[0];
	private TransactionView[] esTransactions = new TransactionView[0];
	private TransactionView[] cashTransactions = new TransactionView[0];
	private TransactionView[] dsTransactions = new TransactionView[0];
	private TransactionView[] epCashTransactions = new TransactionView[0];
	private int invlLoginTryCnt;
	private boolean profileLoginLocked; 
	private boolean profileDeletedFromLdap;
	private boolean acceptPromotionalEmail=false;
	private String basicFlag;
	private String prmrCode;
	private Date prmrCodeDate;
	private List custPrmrCodes = new ArrayList(0);
	private String negativeAddress = null;
	private String loyaltyPgmMembershipId;
	private String custAutoEnrollFlag;
	private String eDirectoryGuid;
	private Date adaptiveAuthProfileCompleteDate;
	private String partnerSiteId;
	private String idExtnlId;
	private String idNumber;
	private String idType;
	private String idTypeFriendlyName;
	private String gender;
	private String docStatus;
	private String submitApproveDocument;
	private String submitDenyDocument;
	private String submitPendDocument;
	private boolean isIdImageAvailable;
	private String viewGBGroupLogLinkString;
	private String gbGroupDetailLink;
	//MBO-100:Show Profile Type on EMT Admin Consumer profile Page
	private String profileType;
	
	//MBO- 2158 EmailAge integration
	
	private String emailScore;
	//MBO-2587 email first seen
	private String emailFirstSeen;
	
	//MBO-3819
	private String emailAgeNameMatch;
	//ended 3819
	//MBO - 1800 starts
	private String idIssueCountry;
	
	//MBO -3320
	private LexisNexisActivity lexisNexisActivity;
	private boolean isBPSEnable = Boolean.FALSE;
	
	//MBO-3513
	private LexisNexisActivity[] lexNexActivtyArr;
	private boolean isRLSEnable = Boolean.FALSE;
	
	//MBO-4742
	private String mrzLine1;
	private String mrzLine2;
	private String mrzLine3;
	
	//MBO-5465
	private String custStatusCode;
	//MBO-5464
	private String addressLine3;
	private String residentStatus;
	private List residentStatusOptions = new ArrayList(0);
	private String sendLimit;
	private List sendLimitOptions = new ArrayList(0);
	private boolean receiveLimit;
	private String countryOfBirth;
	//MBO-5468
	private String siteType;
	//MBO-7310
	private String phoneCountryCode;
	private String phoneDialingCode;
	private String displayCtyDialingCode;
	
	//MBO-7547
	private String tempResidentNumber;
	private String tempResidentExpiry;
	
	private String SourceDevice;
	private String SourceApp;
	
	//private CustomerIDUploadInfo[] consumerIdUploadInfo;
	private CustomerIDUploadInfo[] consumerIdUploadInfo;
	
	private void clear()
	{
		this.custId = null;
		this.custLogonId = null;
		this.custStatus = null;
		this.custLastName = null;
		this.custFirstName = null;
		this.custMiddleName = null;
		this.custHashedPassword = null;
		this.custSsn = null;
		this.brthDate = null;
		this.custAge = null;
		this.addressLine1 = null;
		this.addressLine2 = null;
		this.addressLine3 = null;
		this.emailAddress = null;
		this.isEmailActive = false;
		this.city = null;
		this.state = null;
		this.isoCountryCode = null;
		this.postalCode = null;
		//MBO-3603
		this.postalCodeForDisplay=null;
		//MBO-3603
		this.zip4 = null;
		this.phoneNumber = null;
		this.phoneNumberAlternate = null;
		this.bankAccounts = new ConsumerBankAccountView[0];
		this.creditCardAccounts = new ConsumerCreditCardAccountView[0];
		this.maestroAccounts = new ConsumerCreditCardAccountView[0];
		this.comments = new ConsumerProfileCommentView[0];
		this.commentDisplayLimit = DEFAULT_COMMENT_DISPLAY_LIMIT;
		this.epTransactions = new TransactionView[0];
		this.mgTransactions = new TransactionView[0];
		this.esTransactions = new TransactionView[0];
		this.cashTransactions = new TransactionView[0];
		this.invlLoginTryCnt = 0;
		this.createdIPAddress = null;
		this.acceptPromotionalEmail=false;
		this.prmrCode = "S";
		this.loyaltyPgmMembershipId = null;
		this.custAutoEnrollFlag = null;
		this.adaptiveAuthProfileCompleteDate = null;
		this.eDirectoryGuid = null;
		this.partnerSiteId = null;
		this.isIdImageAvailable = false;
		this.profileType = null;
		this.idExpirationDate = null;
		this.idIssueCountry = null;
		this.lexisNexisActivity = null;
		this.isBPSEnable = Boolean.FALSE;
		this.lexNexActivtyArr = null;
		this.isRLSEnable = Boolean.FALSE;
		this.mrzLine1 = null;
		this.mrzLine2 = null;
		this.mrzLine3 = null;
		this.custStatusCode=null;
		this.residentStatus = null;
		this.sendLimit = null;
		this.receiveLimit = false;
		this.countryOfBirth=null;
		this.siteType = null;
		//MBO-7310
		this.phoneCountryCode = null;
		this.phoneDialingCode = null;
		this.displayCtyDialingCode = null;
		
		this.SourceDevice = null;
		this.SourceApp = null;
		//this.consumerIdUploadInfo = null;
	}


	public String getEDirectoryGuid() {
		return eDirectoryGuid;
	}

	public void setEDirectoryGuid(String directoryGuid) {
		eDirectoryGuid = directoryGuid.trim();
	}

	public String getCustId()
	{
		return custId;
	}

	public void setCustId(String string)
	{
		custId = StringUtils.trimToNull(string);
	}

	public String getCustLogonId()
	{
		return custLogonId;
	}

	public void setCustLogonId(String string)
	{
		custLogonId = StringUtils.trimToNull(string);
	}

	public String getCustLastName()
	{
		return custLastName;
	}

	public void setCustLastName(String string)
	{
		custLastName = StringUtils.trimToNull(string);
	}

	public String getCustFirstName()
	{
		return custFirstName;
	}

	public void setCustFirstName(String string)
	{
		custFirstName = StringUtils.trimToNull(string);
	}

	public String getCustMiddleName()
	{
		return custMiddleName;
	}

	public void setCustMiddleName(String string)
	{
		custMiddleName = StringUtils.trimToNull(string);
	}

	public String getCustSsn()
	{
		return custSsn;
	}

	public void setCustSsn(String string)
	{
		custSsn = StringUtils.trimToNull(string);
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		clear();
	}

	/**
	 * @return
	 * 
	 * Created on Feb 16, 2005
	 */
	public String getAddressLine1()
	{
		return addressLine1;
	}

	/**
	 * @return
	 * 
	 * Created on Feb 16, 2005
	 */
	public String getAddressLine2()
	{
		return addressLine2;
	}

	/**
	 * @return
	 * 
	 * Created on Feb 16, 2005
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @return
	 * 
	 * Created on Feb 16, 2005
	 */
	public String getIsoCountryCode()
	{
		return isoCountryCode;
	}

	/**
	 * @return
	 * 
	 * Created on Feb 16, 2005
	 */
	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	/**
	 * @return
	 * 
	 * Created on Feb 16, 2005
	 */
	public String getPhoneNumberAlternate()
	{
		return phoneNumberAlternate;
	}

	/**
	 * @return
	 * 
	 * Created on Feb 16, 2005
	 */
	public String getPostalCode()
	{
		return postalCode;
	}

	/**
	 * @return
	 * 
	 * Created on Feb 16, 2005
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * @return
	 * 
	 * Created on Feb 16, 2005
	 */
	public String getZip4()
	{
		return zip4;
	}

	/**
	 * @param string
	 * 
	 * Created on Feb 16, 2005
	 */
	public void setAddressLine1(String string)
	{
		addressLine1 = string;
	}

	/**
	 * @param string
	 * 
	 * Created on Feb 16, 2005
	 */
	public void setAddressLine2(String string)
	{
		addressLine2 = string;
	}

	/**
	 * @param string
	 * 
	 * Created on Feb 16, 2005
	 */
	public void setCity(String string)
	{
		city = string;
	}

	/**
	 * @param string
	 * 
	 * Created on Feb 16, 2005
	 */
	public void setIsoCountryCode(String string)
	{
		isoCountryCode = string;
	}

	/**
	 * @param string
	 * 
	 * Created on Feb 16, 2005
	 */
	public void setPhoneNumber(String string)
	{
		phoneNumber = string;
	}

	/**
	 * @param string
	 * 
	 * Created on Feb 16, 2005
	 */
	public void setPhoneNumberAlternate(String string)
	{
		phoneNumberAlternate = string;
	}

	/**
	 * @param string
	 * 
	 * Created on Feb 16, 2005
	 */
	public void setPostalCode(String string)
	{
		postalCode = string;
	}
	
	//MBO-3603
	/**
	 * @param string
	 * 
	 * Created on Jan 19, 2016
	 */
	public void setPostalCodeForDisplay(String string)
	{
		postalCodeForDisplay = string;
	}
	//MBO-3603
	/**
	 * @param string
	 * 
	 * Created on Feb 16, 2005
	 */
	public void setState(String string)
	{
		state = string;
	}

	/**
	 * @param string
	 * 
	 * Created on Feb 16, 2005
	 */
	public void setZip4(String string)
	{
		zip4 = string;
	}

	public String getPhoneNumberFormatted() {
		String formattedPhone = PhoneNumberFormatter.format(phoneNumber);
		return formattedPhone;
	}

	public String getPhoneNumberAlternateFormatted() {
		String formattedPhone = PhoneNumberFormatter.format(phoneNumberAlternate); 
		return formattedPhone;
	}

	public String getCustStatus()
	{
		return custStatus;
	}

	public List getCustStatusOptions()
	{
		return custStatusOptions;
	}

	public List getAccountStatusOptions()
	{
		return accountStatusOptions;
	}

	public List getCustomerCommentReasons()
	{
		return customerCommentReasons;
	}

	public void setCustStatus(String string)
	{
		custStatus = string;
	}

	public void setCustStatusOptions(List list)
	{
		custStatusOptions = (list == null ? new ArrayList(0) : list);
	}

	public void setCustomerCommentReasons(List list)
	{
		customerCommentReasons = (list == null ? new ArrayList(0) : list);
	}

	public void setAccountStatusOptions(List list)
	{
		accountStatusOptions = (list == null ? new ArrayList(0) : list);
	}

	public ConsumerBankAccountView[] getBankAccounts()
	{
		return bankAccounts;
	}

	public ConsumerCreditCardAccountView[] getCreditCardAccounts()
	{
		return creditCardAccounts;
	}

	public void setBankAccounts(ConsumerBankAccountView[] accounts)
	{
		bankAccounts =
			accounts == null ? new ConsumerBankAccountView[0] : accounts;
	}

	public void setCreditCardAccounts(ConsumerCreditCardAccountView[] accounts)
	{
		creditCardAccounts =
			accounts == null ? new ConsumerCreditCardAccountView[0] : accounts;
	}

	public ConsumerProfileActivity[] getConsumerProfileActivity() {
		return consumerProfileActivity;
	}

	public void setConsumerProfileActivity(ConsumerProfileActivity[] cpActy) {
		this.consumerProfileActivity = cpActy.clone();
	}
	
	public ConsumerProfileCommentView[] getComments()
	{
		return comments;
	}

	public void setComments(ConsumerProfileCommentView[] views)
	{
		comments = views.clone();
	}

	public int getCommentDisplayLimit()
	{
		return commentDisplayLimit;
	}

	public void setCommentDisplayLimit(int i)
	{
		commentDisplayLimit = i;
	}

	public TransactionView[] getEpTransactions()
	{
		return epTransactions;
	}

	public TransactionView[] getMgTransactions()
	{
		return mgTransactions;
	}

	public void setEpTransactions(TransactionView[] views)
	{
		epTransactions = views.clone();
	}

	public void setMgTransactions(TransactionView[] views)
	{
		mgTransactions = views.clone();
	}
	/**
	 * @return
	 */
	public String getBrthDate() {
		return brthDate;
	}

	/**
	 * @param string
	 */
	public void setBrthDate(String string) {
		brthDate = string;
	}

	public String getCustAge() {
		return custAge;
	}

	public void setCustAge(String custAge) {
		this.custAge = custAge;
	}


	/**
	 * @return
	 */
	public String getEmailAddress()
	{
		return emailAddress;
	}

	/**
	 * @param string
	 */
	public void setEmailAddress(String string)
	{
		emailAddress = string;
	}

	/**
	 * @return
	 */
	public boolean getIsEmailActive()
	{
		return isEmailActive;
	}

	/**
	 * @param boolean1
	 */
	public void setIsEmailActive(boolean b)
	{
		isEmailActive = b;
	}

	/**
	 * @return
	 */
	public String getCustSsnTaintText() {
		return custSsnTaintText;
	}

	/**
	 * @param string
	 */
	public void setCustSsnTaintText(String string) {
		custSsnTaintText = string;
	}

	/**
	 * @return
	 */
	public String getPhoneNumberTaintText() {
		return phoneNumberTaintText == null ? "" : phoneNumberTaintText;
	}

	/**
	 * @param string
	 */
	public void setPhoneNumberTaintText(String string) {
		phoneNumberTaintText = string;
	}

	/**
	 * @return
	 */
	public String getSaveCustSsn() {
		return saveCustSsn;
	}

	/**
	 * @param string
	 */
	public void setSaveCustSsn(String string) {
		saveCustSsn = string;
	}

	/**
	 * @return
	 */
	public String getPhoneNumberAlternateTaintText() {
		return phoneNumberAlternateTaintText == null ? "" : phoneNumberAlternateTaintText;
	}

	/**
	 * @param string
	 */
	public void setPhoneNumberAlternateTaintText(String string) {
		phoneNumberAlternateTaintText = string;
	}

	/**
	 * @return
	 */
	public TransactionView[] getEsTransactions() {
		return esTransactions;
	}

	/**
	 * @param views
	 */
	public void setEsTransactions(TransactionView[] views) {
		esTransactions = views.clone();
	}

	public TransactionView[] getCashTransactions() {
		return cashTransactions;
	}

	/**
	 * @param views
	 */
	public void setCashTransactions(TransactionView[] views) {
		cashTransactions = views.clone();
	}

	public TransactionView[] getDsTransactions() {
		return dsTransactions;
	}

	public void setDsTransactions(TransactionView[] dsTransactions) {
		this.dsTransactions = dsTransactions.clone();
	}

	/**
	 * @return
	 */
	public String getCustHashedPassword() {
		return custHashedPassword;
	}

	/**
	 * @param string
	 */
	public void setCustHashedPassword(String string) {
		custHashedPassword = string;
	}
	/**
	 * @return
	 */
	public String getEmailAddressTaintText() {
		return emailAddressTaintText;
	}

	/**
	 * @param string
	 */
	public void setEmailAddressTaintText(String string) {
		emailAddressTaintText = string;
	}

	/**
	 * @return
	 */
	public int getInvlLoginTryCnt()
	{
		return invlLoginTryCnt;
	}

	/**
	 * @param i
	 */
	public void setInvlLoginTryCnt(int i)
	{
		invlLoginTryCnt = i;
	}

	/**
	 * @return
	 */
	public String getEmailDomainTaintText()
	{
		return emailDomainTaintText;
	}

	/**
	 * @param string
	 */
	public void setEmailDomainTaintText(String string)
	{
		emailDomainTaintText = string;
	}

	/**
	 * @return
	 */
	public String getCreatedIPAddress()
	{
		return createdIPAddress == null ? "" : createdIPAddress;
	}

	/**
	 * @param string
	 */
	public void setCreatedIPAddress(String string)
	{
		createdIPAddress = string;
	}

	/**
	 * @return
	 */
	public ConsumerCreditCardAccountView[] getDebitCardAccounts()
	{
		return debitCardAccounts;
	}

	/**
	 * @param views
	 */
	public void setDebitCardAccounts(ConsumerCreditCardAccountView[] views)
	{
		debitCardAccounts = views.clone();
	}

	public ConsumerCreditCardAccountView[] getMaestroAccounts() {
		return maestroAccounts;
	}

	public void setMaestroAccounts(ConsumerCreditCardAccountView[] maestroAccounts) {
		this.maestroAccounts = maestroAccounts.clone();
	}

	/**
	 * @return
	 */
	public boolean isAcceptPromotionalEmail()
	{
		return acceptPromotionalEmail;
	}

	/**
	 * @param b
	 */
	public void setAcceptPromotionalEmail(boolean b)
	{
		acceptPromotionalEmail = b;
	}

	/**
	 * @return
	 */
	public String getBasicFlag()
	{
		return basicFlag;
	}

	public void setBasicFlag(String string)
	{
		basicFlag = string;
	}

	public String getPrmrCode() {
        return prmrCode;
    }

    public void setPrmrCode(String prmrCode) {
        this.prmrCode = prmrCode;
    }

	public Date getPrmrCodeDate() {
        return this.prmrCodeDate;
    }

    public void setPrmrCodeDate(Date prmrCodeDate) {
        this.prmrCodeDate = prmrCodeDate;
    }

    public List getCustPrmrCodes() {
        return custPrmrCodes;
    }


    public void setCustPrmrCodes(List custPrmrCodes) {
        this.custPrmrCodes = custPrmrCodes;
    }
     
	public String getNegativeAddress() {
		return negativeAddress;
	}

	public void setNegativeAddress(String negativeAddress) {
		this.negativeAddress = AddressHelper.negativeAddressFormat(this.addressLine1,
				this.postalCode, this.state);
	}

	public String getLoyaltyPgmMembershipId() {
		return loyaltyPgmMembershipId;
	}

	public void setLoyaltyPgmMembershipId(String loyaltyPgmMembershipId) {
		this.loyaltyPgmMembershipId = loyaltyPgmMembershipId;
	}

	public String getCustAutoEnrollFlag() {
		return custAutoEnrollFlag;
	}

	public void setCustAutoEnrollFlag(String custAutoEnrollFlag) {
		this.custAutoEnrollFlag = custAutoEnrollFlag;
	}
	public Date getAdaptiveAuthProfileCompleteDate() {
		return adaptiveAuthProfileCompleteDate;
	}

	public void setAdaptiveAuthProfileCompleteDate(Date adaptiveAuthProfileCompleteDate) {
		this.adaptiveAuthProfileCompleteDate = adaptiveAuthProfileCompleteDate;
	}

	public boolean getProfileLoginLocked() {
		return profileLoginLocked;
	}

	public void setProfileLoginLocked(boolean profileLoginLocked) {
		this.profileLoginLocked = profileLoginLocked;
	}
	
	public boolean isProfileDeletedFromLdap() {
		return profileDeletedFromLdap;
	}

	public void setProfileDeletedFromLdap(boolean profileDeletedFromLdap) {
		this.profileDeletedFromLdap = profileDeletedFromLdap;
	}
	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}

	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}

	public String getCountyName() {
		return countyName;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdTypeFriendlyName(String idTypeFriendlyName) {
		this.idTypeFriendlyName = idTypeFriendlyName;
	}

	public String getIdTypeFriendlyName() {
		return idTypeFriendlyName;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getGender() {
		return gender;
	}

	public String getDocStatus() {
		return docStatus;
	}

	public void setDocStatus(String docStatus) {
		this.docStatus = docStatus;
	}

	public String getDocStatusString() {
		return EMoneyGramAdmApplicationConstants.docStatusMap.get(getDocStatus());
	}

	public String getSubmitApproveDocument() {
		return submitApproveDocument;
	}

	public void setSubmitApproveDocument(String submitApproveDocument) {
		this.submitApproveDocument = submitApproveDocument;
	}

	public String getSubmitDenyDocument() {
		return submitDenyDocument;
	}

	public void setSubmitDenyDocument(String submitDenyDocument) {
		this.submitDenyDocument = submitDenyDocument;
	}

	public String getSubmitPendDocument() {
		return submitPendDocument;
	}

	public void setSubmitPendDocument(String submitPendDocument) {
		this.submitPendDocument = submitPendDocument;
	}

	public String getIdExtnlId() {
		return idExtnlId;
	}

	public void setIdExtnlId(String idExtnlId) {
		this.idExtnlId = idExtnlId;
	}
	
	public boolean getIsIdImageAvailable(){
		return isIdImageAvailable;
	}
	
	public void setIsIdImageAvailable(boolean b) {
		this.isIdImageAvailable = b;
	}

	public String getViewGBGroupLogLinkString() {
		return viewGBGroupLogLinkString;
	}

	public void setViewGBGroupLogLinkString(String viewGBGroupLogLinkString) {
		this.viewGBGroupLogLinkString = viewGBGroupLogLinkString;
	}

	public String getGbGroupDetailLink() {
		return gbGroupDetailLink;
	}

	public void setGbGroupDetailLink(String gbGroupDetailLink) {
		this.gbGroupDetailLink = gbGroupDetailLink;
	}
	
    public String getCustSecondLastName() {
	    return custSecondLastName;
	}

    public void setCustSecondLastName(String custSecondLastName) {
	    this.custSecondLastName = custSecondLastName!= null ? custSecondLastName.trim() : "";
	}

	public String getProfileType() {
		return profileType;
	}

	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}
	
	public String getEmailScore() {
		return emailScore;
	}

	public void setEmailScore(String emailScore) {
		this.emailScore = emailScore;
	}
	
		//MBO-2587 email first seen
	public String getEmailFirstSeen() {
		return emailFirstSeen;
	}

	public void setEmailFirstSeen(String emailFirstSeen) {
		this.emailFirstSeen = emailFirstSeen;
	}
	
	public String getIdIssueCountry() {
		return idIssueCountry;
	}

	public void setIdIssueCountry(String idIssueCountry) {
		this.idIssueCountry = idIssueCountry;
	}

	
	private String idExpirationDate;
		public String getIdExpirationDate() {
		return idExpirationDate;
	}

	public void setIdExpirationDate(String idExpirationDate) {
		this.idExpirationDate = idExpirationDate;
	}

	public LexisNexisActivity getLexisNexisActivity() {
		return lexisNexisActivity;
	}

	public void setLexisNexisActivity(LexisNexisActivity lexisNexisActivity) {
		this.lexisNexisActivity = lexisNexisActivity;
	}

	public boolean getIsBPSEnable() {
		return isBPSEnable;
	}

	public void setBPSEnable(boolean isBPSEnable) {
		this.isBPSEnable = isBPSEnable;
	}

	public LexisNexisActivity[] getLexNexActivtyArr() {
		return lexNexActivtyArr;
	}

	public void setLexNexActivtyArr(LexisNexisActivity[] lexNexActivtyArr) {
		this.lexNexActivtyArr = lexNexActivtyArr.clone();
	}

	public boolean getIsRLSEnable() {
		return isRLSEnable;
	}

	public void setRLSEnable(boolean isRLSEnable) {
		this.isRLSEnable = isRLSEnable;
	}

	public String getEmailAgeNameMatch() {
		return emailAgeNameMatch;
	}

	public void setEmailAgeNameMatch(String emailAgeNameMatch) {
		this.emailAgeNameMatch = emailAgeNameMatch;
	}

	public String getMrzLine1() {
		return mrzLine1;
	}

	public void setMrzLine1(String mrzLine1) {
		this.mrzLine1 = mrzLine1;
	}

	public String getMrzLine2() {
		return mrzLine2;
	}

	public void setMrzLine2(String mrzLine2) {
		this.mrzLine2 = mrzLine2;
	}

	public String getMrzLine3() {
		return mrzLine3;
	}

	public void setMrzLine3(String mrzLine3) {
		this.mrzLine3 = mrzLine3;
	}

	public String getCustStatusCode() {
		return custStatusCode;
	}

	public void setCustStatusCode(String custStatusCode) {
		this.custStatusCode = custStatusCode;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public String getResidentStatus() {
		return residentStatus;
	}

	public void setResidentStatus(String residentStatus) {
		this.residentStatus = residentStatus;
	}

	public List getResidentStatusOptions() {
		return residentStatusOptions;
	}

	public void setResidentStatusOptions(List list) {
		residentStatusOptions = (list == null ? new ArrayList(0) : list);
	}

	public void setSendLimit(String sendLimit) {
		this.sendLimit = sendLimit;
	}

	public String getSendLimit() {
		return sendLimit;
	}

	public void setSendLimitOptions(List list) {
		sendLimitOptions = (list == null ? new ArrayList(0) : list);
	}

	public List getSendLimitOptions() {
		return sendLimitOptions;
	}

	public void setReceiveLimit(boolean receiveLimit) {
		this.receiveLimit = receiveLimit;
	}

	public boolean isReceiveLimit() {
		return receiveLimit;
	}

	public void setCountryOfBirth(String countryOfBirth) {
		this.countryOfBirth = countryOfBirth;
	}

	public String getCountryOfBirth() {
		return countryOfBirth;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public String getSiteType() {
		return siteType;
	}

	public String getPhoneCountryCode() {
		return phoneCountryCode;
	}

	public void setPhoneCountryCode(String phoneCountryCode) {
		this.phoneCountryCode = phoneCountryCode;
	}

	public String getPhoneDialingCode() {
		return phoneDialingCode;
	}

	public void setPhoneDialingCode(String phoneDialingCode) {
		this.phoneDialingCode = phoneDialingCode;
	}

	public String getDisplayCtyDialingCode() {
		return displayCtyDialingCode;
	}

	public void setDisplayCtyDialingCode(String displayCtyDialingCode) {
		this.displayCtyDialingCode = displayCtyDialingCode;
	}

	public String getTempResidentNumber() {
		return tempResidentNumber;
	}

	public void setTempResidentNumber(String tempResidentNumber) {
		this.tempResidentNumber = tempResidentNumber;
	}

	public String getTempResidentExpiry() {
		return tempResidentExpiry;
	}

	public void setTempResidentExpiry(String tempResidentExpiry) {
		this.tempResidentExpiry = tempResidentExpiry;
	}	
	
	public String getSourceDevice() {
		return SourceDevice;
	}

	public void setSourceDevice(String sourceDevice) {
		SourceDevice = sourceDevice;
	}

	public String getSourceApp() {
		return SourceApp;
	}

	public void setSourceApp(String sourceApp) {
		SourceApp = sourceApp;
	}


	public CustomerIDUploadInfo[] getConsumerIdUploadInfo() {
		return consumerIdUploadInfo;
	}


	public void setConsumerIdUploadInfo(CustomerIDUploadInfo[] consumerIdUploadInfo) {
		this.consumerIdUploadInfo = consumerIdUploadInfo;
	}


	public TransactionView[] getEpCashTransactions() {
		return epCashTransactions;
	}


	public void setEpCashTransactions(TransactionView[] epCashTransactions) {
		this.epCashTransactions = epCashTransactions;
	}
	
	
	
}
