package emgadm.reporting;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.exceptions.TooManyResultException;
import emgadm.model.JournalEntry;
import emgadm.util.StringHelper;
import emgshared.property.EMTSharedDynProperties;
import emgshared.util.DateFormatter;

/**
 * @version 	1.0
 * @author
 */
public class JournalEntryAction extends EMoneyGramAdmBaseAction
{

	private static EMTSharedDynProperties dynProps =
		new EMTSharedDynProperties();

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		JournalEntryForm journalEntryForm = (JournalEntryForm) form;
		TransactionManager tm = getTransactionManager(request);
		ArrayList journalEntries = new ArrayList();
		if (journalEntryForm.getSortBy() == null) {
			JournalEntry.sortBy = "entryDate";
			//JournalEntry.setSortBaseAsEntryDate("entryDate");
		} else {
			JournalEntry.sortBy = journalEntryForm.getSortBy();
		}

		if (!StringHelper.isNullOrEmpty(journalEntryForm.getSubmitSearch())) {
			try {
				DateFormatter df1 = new DateFormatter("MM/dd/yyyy", true);
				DateFormatter df2 = new DateFormatter("dd/MMM/yyyy", true);

				journalEntries = (ArrayList) tm.getJournalEntries(StringHelper
						.isNullOrEmpty(journalEntryForm.getEmgTranId()) ? -1
						: Integer.parseInt(journalEntryForm.getEmgTranId()),
						df1.format(df2.parse(journalEntryForm
								.getBeginDateText())), df1.format(df2
								.parse(journalEntryForm.getEndDateText())));
				journalEntries = (ArrayList) filterJournalEntries(journalEntries);
				if (journalEntries.size() != 0) {
					Collections.sort(journalEntries);
				} else {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"error.no.match.found"));
					saveErrors(request, errors);
				}

			} catch (TooManyResultException e) {
				errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError("error.too.many.results", dynProps
								.getMaxDownloadTransactions() + ""));
				saveErrors(request, errors);
			}
		}

		request.getSession().setAttribute("JournalEntries", journalEntries);

		return mapping
				.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private Collection filterJournalEntries(Collection journalEntries)
			throws Exception {
		ArrayList entries = new ArrayList();
		Iterator iter = journalEntries.iterator();

		while (iter.hasNext()) {
			JournalEntry journalEntry = (JournalEntry) iter.next();

			if (journalEntry.getPostAmount() == 0.0) {
				continue;
			}

			entries.add(journalEntry);
		}

		return entries;
	}

}
