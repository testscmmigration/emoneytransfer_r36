package emgadm.reporting;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgshared.exceptions.ParseDateException;
import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;
import emgshared.util.DateFormatter;

public class JournalEntryForm extends EMoneyGramAdmBaseValidatorForm {
	private String beginDateText = null;
	private String endDateText = null;

	private String journalEntryDate = null;
	private String userId = null;
	private String reasonDescription = null;
	private String emgTranId = null;
	private String senderLastName = null;
	private String senderFirstName = null;
	private String debitAmount = null;
	private String creditAmount = null;
	private String submitSearch = null;
	private String sortBy = null;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
		beginDateText = df.format(new Date(System.currentTimeMillis() - 7L
				* 24L * 60L * 60L * 1000L));
		endDateText = df.format(new Date(System.currentTimeMillis() + 24L * 60L
				* 60L * 1000L));
	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);

		if (submitSearch != null) {

			if (!StringHelper.isNullOrEmpty(emgTranId)
					&& StringHelper.containsNonDigits(emgTranId)) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"errors.integer", "Transaction Id"));
			}

			if (errors.size() == 0) {
				DateFormatter dfmt = new DateFormatter("dd/MMM/yyyy", true);
				try {
					if (dfmt.parse(beginDateText)
							.after(dfmt.parse(endDateText))) {
						String[] parms = { beginDateText, endDateText };
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
								"error.begin.date.later.than.end.date", parms));
					}
				} catch (ParseDateException e) {
					e.printStackTrace();
				}
			}

		}

		return errors;
	}

	/**
	 * @return
	 */
	public String getBeginDateText() {
		return beginDateText;
	}

	/**
	 * @return
	 */
	public String getCreditAmount() {
		return creditAmount;
	}

	/**
	 * @return
	 */
	public String getDebitAmount() {
		return debitAmount;
	}

	/**
	 * @return
	 */
	public String getEmgTranId() {
		return emgTranId;
	}

	/**
	 * @return
	 */
	public String getEndDateText() {
		return endDateText;
	}

	/**
	 * @return
	 */
	public String getJournalEntryDate() {
		return journalEntryDate;
	}

	/**
	 * @return
	 */
	public String getReasonDescription() {
		return reasonDescription;
	}

	/**
	 * @return
	 */
	public String getSenderFirstName() {
		return senderFirstName;
	}

	/**
	 * @return
	 */
	public String getSenderLastName() {
		return senderLastName;
	}

	/**
	 * @return
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param string
	 */
	public void setBeginDateText(String string) {
		beginDateText = string;
	}

	/**
	 * @param string
	 */
	public void setCreditAmount(String string) {
		creditAmount = string;
	}

	/**
	 * @param string
	 */
	public void setDebitAmount(String string) {
		debitAmount = string;
	}

	/**
	 * @param string
	 */
	public void setEmgTranId(String string) {
		emgTranId = string;
	}

	/**
	 * @param string
	 */
	public void setEndDateText(String string) {
		endDateText = string;
	}

	/**
	 * @param string
	 */
	public void setJournalEntryDate(String string) {
		journalEntryDate = string;
	}

	/**
	 * @param string
	 */
	public void setReasonDescription(String string) {
		reasonDescription = string;
	}

	/**
	 * @param string
	 */
	public void setSenderFirstName(String string) {
		senderFirstName = string;
	}

	/**
	 * @param string
	 */
	public void setSenderLastName(String string) {
		senderLastName = string;
	}

	/**
	 * @param string
	 */
	public void setUserId(String string) {
		userId = string;
	}

	/**
	 * @return
	 */
	public String getSubmitSearch() {
		return submitSearch;
	}

	/**
	 * @param string
	 */
	public void setSubmitSearch(String string) {
		submitSearch = string;
	}

	/**
	 * @return
	 */
	public String getSortBy() {
		return sortBy;
	}

	/**
	 * @param string
	 */
	public void setSortBy(String string) {
		sortBy = string;
	}

}
