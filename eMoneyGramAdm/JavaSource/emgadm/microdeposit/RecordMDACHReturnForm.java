package emgadm.microdeposit;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class RecordMDACHReturnForm extends EMoneyGramAdmBaseValidatorForm {
	private String batchId;
	private String[] selectedVldn;
	private String[] selectedDeAct;
	private String mode;
	private String submitUpdate;
	private String submitReturn;

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String string) {
		batchId = string;
	}

	public String[] getSelectedVldn() {
		return selectedVldn;
	}

	public void setSelectedVldn(String[] strings) {
		selectedVldn = strings.clone();
	}

	public String[] getSelectedDeAct() {
		return selectedDeAct;
	}

	public void setSelectedDeAct(String[] strings) {
		selectedDeAct = strings.clone();
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String string) {
		mode = string;
	}

	public String getSubmitUpdate() {
		return submitUpdate;
	}

	public void setSubmitUpdate(String string) {
		submitUpdate = string;
	}

	public String getSubmitReturn() {
		return submitReturn;
	}

	public void setSubmitReturn(String string) {
		submitReturn = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		selectedVldn = new String[0];
		selectedDeAct = new String[0];
		mode = "x";
		submitUpdate = null;
		submitReturn = null;
	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);

		if (!StringHelper.isNullOrEmpty(submitUpdate)) {
			if (selectedVldn.length == 0 && selectedDeAct.length == 0) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"errors.no.changes.found"));
			} else {

				boolean matchFound = true;
				String id = "";
				if (selectedDeAct.length != 0) {
					for (int i = 0; i < selectedDeAct.length; i++) {
						matchFound = false;
						id = String.valueOf(selectedDeAct[i]);
						if (selectedVldn.length != 0) {
							for (int j = 0; j < selectedVldn.length; j++) {
								if (selectedDeAct[i].equals(selectedVldn[j])) {
									matchFound = true;
									break;
								}
							}
						}
						if (!matchFound) {
							break;
						}
					}
				}

				if (!matchFound) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"errors.must.check.ach.return", id));
				}
			}
		}
		return errors;
	}
}