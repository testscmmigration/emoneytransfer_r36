package emgadm.microdeposit;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.util.HashMap;
import com.moneygram.common.util.StringUtility;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.exceptions.DataSourceException;
import emgshared.services.MicroDepositService;
import emgshared.services.ServiceFactory;

public class ListMDBatchesAction extends EMoneyGramAdmBaseAction
{
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws DataSourceException, SQLException
	{

		ActionErrors errors = new ActionErrors();
		ListMDBatchesForm form = (ListMDBatchesForm) f;

		UserProfile up = getUserProfile(request);
		MicroDepositService mds =
			ServiceFactory.getInstance().getMicroDepositService();
//		TransactionManager tm = getTransactionManager(request);

		if (up.hasPermission("recordMDACHReturn")) {
			request.getSession().setAttribute("updateACR", "Y");
		}
		

		String rtn = (String) request.getAttribute("mdReturn");
		if (!StringHelper.isNullOrEmpty(rtn)) {
			String type = (String) request.getSession().getAttribute("mdType");
			if ("t".equalsIgnoreCase(type)) {
				Map<String, String> transferMap = (Map<String, String>) request
						.getSession().getAttribute(TRAN_ID);
				String tranid = "";
				if (transferMap != null) {
					tranid = transferMap.get(TRAN_ID);
				}
				form.setTranId(tranid);
				form.setSubmitGo("Go");
			}

			if ("s".equalsIgnoreCase(type)) {
				Map<String, String> transferMap = (Map<String, String>) request
						.getSession().getAttribute(BATCH_DATES);
				String beginDate = "";
				String endDate = "";
				if (transferMap != null) {
					if (transferMap.containsKey(BATCH_BG_DATE))
						beginDate = transferMap.get(BATCH_BG_DATE);
					if (transferMap.containsKey(BATCH_ED_DATE))
						endDate = transferMap.get(BATCH_ED_DATE);
				}
				form.setBatchBeginDate(beginDate);
				form.setBatchEndDate(endDate);
				form.setSubmitSearch("Search");
			}
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitGo())) {
			List vldnList =
				mds.getMDTrans(
					null,
					Integer.valueOf(form.getTranId()),
					null,
					up.getUID());
			if (vldnList == null || vldnList.size() == 0) {
				errors.add(
					"tranId",
					new ActionError(
						"errors.no.match.record.found",
						"Micro Deposit ACH Transaction"));
				saveErrors(request, errors);
				return mapping.findForward(
					EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
			}
			String tranId = form.getTranId();
			request.getSession().setAttribute("mdType", "t");
			if (!StringUtility.isNullOrEmpty(tranId)) {
				Map<String, String> transferMap = new HashMap();
				transferMap.put(TRAN_ID, tranId);
				request.getSession().setAttribute(TRAN_ID, transferMap);
			}
			request.getSession().setAttribute("vldnList", vldnList);
			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_RECORD_MD_ACH_RETURN);
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitSearch())) {
			List batchList =
				mds.getMDBatchList(
					form.getBatchBeginDate(),
					form.getBatchEndDate(),
					up.getUID());

			if (batchList == null || batchList.size() == 0) {
				errors.add(
					"tranId",
					new ActionError(
						"errors.no.match.record.found",
						"Micro Deposit ACH Batch Job"));
				saveErrors(request, errors);
			} else {
				request.getSession().setAttribute("mdType", "s");
				Map<String, String> transferMap = new HashMap();
				transferMap.put(BATCH_BG_DATE, form.getBatchBeginDate());
				transferMap.put(BATCH_ED_DATE, form.getBatchEndDate());

				request.getSession().setAttribute(BATCH_DATES, transferMap);
				request.setAttribute("batchList", batchList);
			}
		}

		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	static String BATCH_DATES = "btchDates";
	static String BATCH_BG_DATE = "mdBegDt";
	static String BATCH_ED_DATE = "mdEndDt";
	static String TRAN_ID = "mdTranId";
}
