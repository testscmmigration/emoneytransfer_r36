package emgadm.roles;

import java.util.ArrayList;
import java.util.Collection;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.model.Role;

public class ViewRolesForm extends EMoneyGramAdmBaseValidatorForm
{
	private Collection roles = new ArrayList();
	private Collection internalRoles = new ArrayList();
	private Collection externalRoles = new ArrayList();
	private String roleId;
	private Role role;

	/**
	 * @return
	 */
	public Collection getExternalRoles()
	{
		return externalRoles;
	}

	/**
	 * @return
	 */
	public Collection getInternalRoles()
	{
		return internalRoles;
	}

	/**
	 * @return
	 */
	public Role getRole()
	{
		return role;
	}

	/**
	 * @return
	 */
	public String getRoleId()
	{
		return roleId;
	}

	/**
	 * @param collection
	 */
	public void setExternalRoles(Collection collection)
	{
		externalRoles = collection;
	}

	/**
	 * @param collection
	 */
	public void setInternalRoles(Collection collection)
	{
		internalRoles = collection;
	}

	/**
	 * @param role
	 */
	public void setRole(Role role)
	{
		this.role = role;
	}

	/**
	 * @param string
	 */
	public void setRoleId(String string)
	{
		roleId = string;
	}
	
	public Collection getRoles()
	{
		if (roles.isEmpty())
		{
			roles.addAll(internalRoles);
			roles.addAll(externalRoles);
		}
		return roles;
	}

}
