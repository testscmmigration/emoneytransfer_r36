package emgadm.actions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.Globals;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.LabelValueBean;

import emgadm.constants.EMoneyGramAdmSessionConstants;
import emgadm.dataaccessors.ACHReturnManager;
import emgadm.dataaccessors.BillerMainOfficeManager;
import emgadm.dataaccessors.ManagerFactory;
import emgadm.dataaccessors.TransactionManager;
import emgadm.dataaccessors.UserProfileManager;
import emgadm.model.Agent;
import emgadm.model.AuditRecord;
import emgadm.model.UserProfile;
import emgadm.util.AuditLogUtils;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.model.UserActivityLog;
import emgshared.model.UserActivityType;
import emgshared.services.ServiceFactory;
import emgshared.services.UserActivityLogService;
import emgshared.util.DateFormatter;

/**
 * This class can be used to contain behavior common
 * to most action classes.
 * @author A131
 *
 */
public abstract class EMoneyGramAdmBaseAction extends Action
{
	protected UserProfile getUserProfile(HttpServletRequest request)
	{
		return (
			(UserProfile) request.getSession().getAttribute(
				EMoneyGramAdmSessionConstants.USER_PROFILE));
	}

	protected ActionMessages getActionMessages(HttpServletRequest request)
	{
		ActionMessages messages =
			(ActionMessages) request.getAttribute(Globals.MESSAGE_KEY);
		return (messages == null ? new ActionMessages() : messages);
	}

	protected UserProfileManager getUserProfileManager(HttpServletRequest request)
	{
		return ManagerFactory.createUserManager();
	}

	protected BillerMainOfficeManager getBillerMainOfficeManager(HttpServletRequest request)
	{
		return ManagerFactory.createBillerMainOfficeManager();
	}

	protected TransactionManager getTransactionManager(HttpServletRequest request)
	{
		return ManagerFactory.createTransactionManager();
	}

	protected ACHReturnManager getACHReturnManager(HttpServletRequest request)
	{
		return ManagerFactory.createACHReturnManager();
	}
	
	
	protected List filterAgents(List agentList, String searchKeys)
	{
		if (searchKeys == null || searchKeys.equals("") || agentList == null
				|| agentList.isEmpty() || agentList.size() == 1)
		{
			return agentList;
		}

		String[] searchKey = split(searchKeys.toUpperCase(), " ");
		if (searchKey.length == 1)
		{
			searchKey = split(searchKeys.toUpperCase(), ",");
		}

		List tempList = new ArrayList();

		Iterator iter = agentList.iterator();
		while (iter.hasNext())
		{
			Agent agent = (Agent) iter.next();
			InnerAgent ia = new InnerAgent(agent);
			String searchString = agent.getDisplayName().toUpperCase();
			for (int i = 0; i < searchKey.length; i++)
			{
				int pos = 0;
				while (searchString.indexOf(searchKey[i], pos) != -1)
				{
					ia.add();
					pos =
						searchString.indexOf(searchKey[i], pos)
							+ searchKey[i].length();
				}
			}
			if (ia.count != 0)
			{
				tempList.add(ia);
			} else
			{
				ia = null;
			}
		}
		Collections.sort(tempList);
		List newList = new ArrayList();
		Iterator it = tempList.iterator();
		while (it.hasNext())
		{
			InnerAgent innerAgent = (InnerAgent) it.next();
			newList.add(innerAgent.agent);
		}
		tempList = null;
		return newList;
	}

	//  use split() method of the String class from jdk 1.4 when migrated to it

	private String[] split(String splittee, String splitChar)
	{
		if (splittee == null || splitChar == null)
		{
			return new String[0];
		}

		int spot;
		while ((spot = splittee.indexOf(splitChar + splitChar)) != -1)
		{
			splittee =
				splittee.substring(0, spot + splitChar.length())
					+ splittee.substring(
						spot + 2 * splitChar.length(),
						splittee.length());
		}

		Vector returns = new Vector();
		int start = 0;
		int length = splittee.length();
		spot = 0;
		while (start < length
			&& (spot = splittee.indexOf(splitChar, start)) > -1)
		{
			if (spot > 0)
			{
				returns.addElement(splittee.substring(start, spot));
			}
			start = spot + splitChar.length();
		}
		if (start < length)
		{
			returns.add(splittee.substring(start));
		}
		String[] values = new String[returns.size()];
		returns.copyInto(values);
		return values;
	}

	//	protected Logger getLogger()
	//	{
	//		return EMoneyGramAdmLogger.getLogger();
	//	}
	//
	protected final void log(
		HttpServletRequest request,
		String event,
		Object[] objects,
		String divId,
		String locId)
	{
		String command = AuditLogUtils.getCommand(request.getRequestURI());
		String details = AuditLogUtils.getDetails(command, event);
		String params = AuditLogUtils.getParamList(command, event, objects);

		AuditRecord auditRecord = new AuditRecord(getUserProfile(request));
		auditRecord.setIpAddress(request.getRemoteAddr());
		auditRecord.setCommand(command);
		auditRecord.setType(event);
		auditRecord.setDetail(details);
		auditRecord.setParamList(params);
		auditRecord.setDivisionNumber(divId);
		auditRecord.setLocationNumber(locId);

		//System.out.println("Audit: '" + auditRecord + "'");
	}

	protected final class InnerAgent implements Comparable<Object>
	{
		
		//MBO-4453
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((agent == null) ? 0 : agent.hashCode());
			result = prime * result + count;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			InnerAgent other = (InnerAgent) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (agent == null) {
				if (other.agent != null)
					return false;
			} else if (!agent.equals(other.agent))
				return false;
			if (count != other.count)
				return false;
			return true;
		}
		//MBO-4453

		protected int count;
		protected Agent agent;

		public InnerAgent(Agent a)
		{
			this.count = 0;
			this.agent = a;
		}

		public int add()
		{
			return count++;
		}

		public int compareTo(Object o)
		{
			InnerAgent i = (InnerAgent) o;
			if (this.count > i.count)
			{
				return -1;
			} else if (this.count < i.count)
			{
				return 1;
			}
			return this.agent.getDisplayName().compareToIgnoreCase(
				i.agent.getDisplayName());
		}

		private EMoneyGramAdmBaseAction getOuterType() {
			return EMoneyGramAdmBaseAction.this;
		}
		
		
	}

	protected static List convertMapToLabelValueListSorted(Map m)
	{
		List list = convertMapToLabelValueList(m);
		Comparator sortByLabel = new Comparator()
		{
			public int compare(Object o1, Object o2)
			{
				LabelValueBean b1 = (LabelValueBean) o1;
				LabelValueBean b2 = (LabelValueBean) o2;
				return b1.getLabel().compareToIgnoreCase(b2.getLabel());
			}
		};
		Collections.sort(list, sortByLabel);
		return list;
	}

	protected static List convertMapToLabelValueList(Map m)
	{
		List list = new ArrayList(m == null ? 0 : m.size());
		if( m != null){
			for (Iterator iter = m.entrySet().iterator(); iter.hasNext();){
				Map.Entry mapEntry = (Map.Entry) iter.next();
				list.add(
					new LabelValueBean(
						mapEntry.getValue().toString(),
						mapEntry.getKey().toString()));
			}
		}
		return list;
	}

	public void addToQuickList(
		HttpServletRequest request,
		String listName,
		String id,
		int cnt)
	{

		Object obj = request.getSession().getAttribute(listName);
		LinkedList list = null;
		if (obj == null)
		{
			list = new LinkedList();
		} else
		{
			list = (LinkedList) obj;
		}

		for (int i = 0; i < list.size(); i++)
		{
			if (id.equals((String) list.get(i)))
			{
				list.remove(i);
			}
		}

		if (list.size() == cnt)
		{
			list.removeLast();
		}

		list.add(0, id);
		request.getSession().setAttribute(listName, list);
	}

	public String getCurrentTime()
	{
		DateFormatter df = new DateFormatter("dd/MMM/yyyy hh:mm a", true);
		return df.format(new Date(System.currentTimeMillis()));
	}

	public static Collection getCollection(
		HttpServletRequest request,
		String listName)
	{

		LinkedList list =
			(LinkedList) request.getSession().getAttribute(listName);
		if (list == null)
		{
			return null;
		}

		Collection col = new ArrayList();
		Iterator iter = list.iterator();
		while (iter.hasNext())
		{
			col.add(iter.next());
		}
		return col;
	}

	/**
	 * Help keep Struts dependancies out of services: return an 
	 * ArraySet of error strings rather then passing in a Struts
	 * ActionErrors object to append to.
	 * In this mission you can employ this method. 
	 * 
	 *  
	 * @param errorStrings
	 * @param errorSet
	 */
	protected static void appendGlobalErrors(
		List<String> errorStrings,
		ActionErrors errorSet)
	{
		if (errorStrings == null
			|| (errorStrings.size() == 0)
			|| (errorSet == null))
			return;
		for (int i = 0; i < errorStrings.size(); i++)
		{
			String err = errorStrings.get(i);
			if (err != null && (err.equals("") == false))
			{
				errorSet.add(ActionErrors.GLOBAL_ERROR, new ActionError(err));
			}
		}
	}

	protected void insertActivityLog(Class loggingClass, HttpServletRequest request, String detailText, String activityTypeCode, String keyValues)
	{
	    try {
		    ServiceFactory sf = ServiceFactory.getInstance();
	        UserActivityLogService uals = sf.getUserActivityLogService();      
	        UserActivityLog ual = new UserActivityLog();
	        ual.setApplicationName(UserActivityLog.APPLICATION_ADMIN);
	        ual.setDataKeyValue(keyValues);
	        if (detailText == null)
	            ual.setDetailText("");
	        else
	            ual.setDetailText(detailText);
	        ual.setEventDate(Calendar.getInstance().getTime());
	        ual.setIPAddress(request.getRemoteAddr());
	        UserActivityType uat = uals.getUserActivityTypeByCode(activityTypeCode);
	        ual.setUserActivityType(uat);
	        ual.setUserId(this.getUserProfile(request).getUID());
	        uals.insertUserActivityLog("EMGMGI",ual);
            
        } catch (Exception e) {
            EMGSharedLogger.getLogger(loggingClass.getClass().getName().toString()).error(
            "User Activity Log insert failed:" + e.toString());
        }
	}
	
}
