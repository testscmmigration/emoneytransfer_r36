package emgadm.actions;

import java.io.File;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.naming.directory.DirContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;



import shared.mgo.dto.MGOAgentProfile;
import shared.mgo.dto.MGOProduct;
import shared.mgo.services.AgentConnectService;
import shared.mgo.services.MGOAgentProfileService;
import shared.mgo.services.MGOServiceFactory;
import emgadm.background.BackgroundProcessMaster;
import emgshared.cache.locator.EMTSharedSourceSiteCacheService;
import emgshared.cache.locator.EMTSharedSourceSiteCacheServiceImpl;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.JNDIManager;
import emgadm.forms.HeartBeatMonForm;
import emgadm.property.EMTAdmContainerProperties;
import emgadm.property.EMTAdmDynProperties;
import emgadm.services.DecryptionService;
import emgadm.services.ServiceFactory;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.dataaccessors.OracleAccess;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.property.EMTSharedDynProperties;
import emgshared.property.EMTSharedEnvProperties;
import emgshared.property.EMTVersionProperties;
import emgshared.services.ObfuscationService;
import emgshared.util.StringHelper;

public class HeartBeatMonAction extends EMoneyGramAdmBaseAction {
	private static org.apache.log4j.Logger LOGGER = EMGSharedLogger.getLogger(HeartBeatMonAction.class);
	private final static String[] propFileList = {
			"emgadm.property.EMTAdmAppProperties",
			"emgadm.property.EMTAdmContainerProperties",
			"emgadm.property.EMTAdmDynProperties",
			"emgshared.property.EMTSharedContainerProperties",
			"emgshared.property.EMTSharedEnvProperties" };

	private final static String[] propType = { "Admin Container",
			"Admin Environment", "Dynamic", "EMT Shared Container",
			"EMT Shared Environment" };

	public final static String ACTION_PATH = "heartBeat";
	private static final ObfuscationService encryptionService = emgshared.services.ServiceFactory
			.getInstance().getObfuscationService();
	private static final DecryptionService decryptionService = ServiceFactory
			.getInstance().getDecryptionService();

	public ActionForward execute(ActionMapping mapping, ActionForm f,
			HttpServletRequest request, HttpServletResponse response) {
		final String[] file = {
				"./webapps/eMoneyGramAdm.war",
				"/var/www/virtual/emgadm.moneygram.com/eMoneyGramAdm.war",
				"/projects/eMoneyGramAdm/WebContent/WEB-INF/classes/"
						+ "emgadm/actions/HeartBeatMonAction.class" };

		final String BUILD_DATE = EMTSharedEnvProperties.getEnvironment()
				+ " Environment, " + EMTVersionProperties.getBuildString()
				+ ", " + StringHelper.getFileBuildDate(file);

		HeartBeatMonForm form = (HeartBeatMonForm) f;
		form.setBuildDate(BUILD_DATE);
		List props = new ArrayList();
		List systems = new ArrayList();

		String pw = form.getPassword();
		//commenting since it is found nto used anywhere MBO-5198
		/*String heartBeatPw = EMTAdmContainerProperties.getHeartBeatPassword();
		String heartBeatPropPw = EMTAdmContainerProperties
				.getHeartBeatPropPassword();*/

		/*if (pw == null || !pw.equals(heartBeatPw)) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_WELCOME);
		}

		// get the system configuration properties
		if (form.getPropPassword().equals(heartBeatPropPw)) {
			try {
				props = checkProperty(request);
			} catch (Exception e) {
				LOGGER.error("exception occured in checkProperty", e);
				return mapping
						.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_ERROR);
			}
		}*/
		//commenting since it is found nto used anywhere MBO-5198
		if ((form.getAction() == null) || (form.getAction().equals("")))
			form.setAction("ALL");
		if (form.getAction().equalsIgnoreCase("ALL")
				|| form.getAction().equalsIgnoreCase("ORACLE"))
			systems.add(checkEmgDb());

		if (form.getAction().equalsIgnoreCase("ALL")
				|| form.getAction().equalsIgnoreCase("LDAP"))
			systems.add(checkLDAP());

		/*if (form.getAction().equalsIgnoreCase("ALL")
				|| form.getAction().equalsIgnoreCase("AC"))
			systems.add(checkAgentConnect());*/

		if (form.getAction().equalsIgnoreCase("ALL")
				|| form.getAction().equalsIgnoreCase("CRYP"))
			systems.add(checkCryptography());

		if (form.getAction().equalsIgnoreCase("ALL")
				|| form.getAction().equalsIgnoreCase("BKGRD"))
			systems.add(checkBackgroundProcess());

		Collections.sort(props);
		request.setAttribute("props", removeDuplicates(props));

		Collections.sort(systems);
		request.setAttribute("systems", systems);

		return mapping
				.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private SystemBean checkCryptography() {
		SystemBean sb = new SystemBean();
		sb.setSystemKey("Encription/Decription Service");

		try {
			String testBA = "123456789";
			String ssnPublic = encryptionService
					.getBankAccountObfuscation(testBA);
			String ssnPrivate = decryptionService.decryptSSN(ssnPublic);
			if (testBA.equals(ssnPrivate)) {
				sb.setSystemStatus("Success");
			} else {
				sb.setSystemStatus("Fail");
				sb.setExceptionClassName("N/A");
				sb.setExceptionMsg("Cannot revert back to original input");
			}
		} catch (Exception e) {
			LOGGER.error("exception occured in checkCryptography()", e);
			sb.setSystemStatus("Fail");
			sb.setExceptionClassName(e.getClass().getName());
			sb.setExceptionMsg(e.getMessage());
		}
		return sb;
	}

	//Commented as a part of UCP-340
	/*private SystemBean checkAgentConnect() {
		SystemBean sb = new SystemBean();
		String ac = EMTAdmContainerProperties.getAC4Address();
		sb.setSystemKey("Agent Connect Web Service - " + ac);

		try {
			AgentConnectService acs = MGOServiceFactory.getInstance()
					.getAgentConnectService();
			//MBO-6101
			EMTSharedSourceSiteCacheService cacheService = new EMTSharedSourceSiteCacheServiceImpl();
			MGOAgentProfile mgoAgentProfile = cacheService.getACDetails("MGO", MGOProduct.SAME_DAY);
			//ended
			MGOAgentProfileService maps = MGOServiceFactory.getInstance()
					.getMGOAgentProfileService();
			MGOAgentProfile mgoAgentProfile = maps.getMGOProfile("MGO",
					MGOProduct.SAME_DAY);
			acs.mgFeeLookup(mgoAgentProfile, new BigDecimal(1.00), "USA",
					"USD", null);
			sb.setSystemStatus("Success");
		} catch (Exception e) {
			LOGGER.error("exception in checkAgentConnect()", e);
			sb.setSystemStatus("Fail");
			sb.setExceptionClassName(e.getClass().getName());
			sb.setExceptionMsg(e.getMessage());
		}
		return sb;
	}*/

	private SystemBean checkEmgDb() {
		SystemBean sb = new SystemBean();
		sb.setSystemKey("Oracle Database Server - "
				+ OracleAccess.E_MONEYGRAM_RESOURCE_REF);

		CallableStatement cs = null;
		Connection conn = null;

		try {
			String storedProcName = "sys.dbms_debug.probe_version";
			StringBuilder sqlBuffer = new StringBuilder(64);
			sqlBuffer.append("{ call " + storedProcName + "(?,?) }");

			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			cs.setNull(1, Types.INTEGER);
			cs.setNull(2, Types.INTEGER);
			cs.execute();
			sb.setSystemStatus("Success");
		} catch (Exception e) {
			LOGGER.error("exception occured in checkEmgDb()", e);
			sb.setSystemStatus("Fail");
			sb.setExceptionClassName(e.getClass().getName());
			sb.setExceptionMsg(e.getMessage());
		} finally {
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return sb;
	}

	private SystemBean checkLDAP() {
		SystemBean sb = new SystemBean();
		String ldap = EMTAdmContainerProperties.getLDAP_SERVER();
		sb.setSystemKey("LDAP Server - " + ldap + ":"
				+ EMTAdmContainerProperties.getLDAP_PORT());

		DirContext jndiContext = null;

		try {
			jndiContext = JNDIManager.getInternalRoleContext();
			sb.setSystemStatus("Success");
		} catch (Exception e) {
			LOGGER.error("exception occured in checkLDAP", e);
			sb.setSystemStatus("Fail");
			sb.setExceptionClassName(e.getClass().getName());
			sb.setExceptionMsg(e.getMessage());
		} finally {
			try {
				JNDIManager.releaseDirContext(jndiContext);
			} catch (Exception e1) {
				LOGGER.error("exception in releaseDirContext", e1);
				// do nothing, close quitely
			}
		}

		return sb;
	}

	private SystemBean checkBackgroundProcess() {
		SystemBean sb = new SystemBean();
		sb.setSystemKey("Automate Transaction Scoring Background Process");
		sb.setSystemStatus(BackgroundProcessMaster.isRunning() ? "Success"
				: "Fail");
		return sb;
	}

	private List checkProperty(HttpServletRequest request) {

		List propList = new ArrayList();

		// Get the current home directory for the web site
		PropBean pb = new PropBean();
		pb.setPropKey("CurrentWebHomeDirectory");
		pb.setPropType("");
		pb.setPropValue((new File(".")).getAbsolutePath());
		propList.add(pb);
		EMTSharedDynProperties obj;

		try {
			// get property values for each of the system property class file
			for (int k = 0; k < propFileList.length; k++) {
				Class cls = Class.forName(propFileList[k]);

				obj = null;
				if (propFileList[k]
						.equalsIgnoreCase("emgadm.property.EMTAdmDynProperties")) {
					obj = EMTAdmDynProperties.getDPO(request);
					cls = obj.getClass();
				}

				// get a list of all methods for the class
				Method[] methodList = cls.getDeclaredMethods();

				// get the name of method and get its value
				for (int i = 0; i < methodList.length; i++) {
					Method method = methodList[i];
					String methodName = method.getName();
					boolean isGetter = false;

					// remove prefix from the method name
					if (methodName.substring(0, 3).equals("get")) {
						methodName = methodName.substring(3);
						isGetter = true;
					} else if (methodName.substring(0, 2).equals("is")) {
						methodName = methodName.substring(2);
						isGetter = true;
					}

					if (isGetter && method.getParameterTypes().length == 0) {
						PropBean propBean = new PropBean();
						propBean.setPropKey(methodName);
						propBean.setPropType(propType[k]);

						String val = null;
						Object result = null;
						result = method.invoke(obj, null);
						if (result != null) {
							val = result.toString();
							if (methodName.toUpperCase().endsWith("PASSWORD")) {
								val = "********";
							}
							if (val.startsWith("[")) {
								val = parseArray(result);
							}
						}
						propBean.setPropValue(val);
						propList.add(propBean);
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error(
					"exception occured in HeartBeatMonAction.checkProperty()",
					e);
			throw new EMGRuntimeException(e);
		}
		return propList;
	}

	private String parseArray(Object obj) {
		String val = "";
		boolean first = true;
		StringBuilder stringBuilder = new StringBuilder();
		try {
			String[] list = (String[]) obj;
			for (int i = 0; i < list.length; i++) {
				stringBuilder.append(first ? list[i] : stringBuilder.toString()
						+ ";" + list[i]);
				first = false;
			}
			return stringBuilder.toString();
		} catch (Exception e) {
			LOGGER.error("exception occured in parseArray", e);
		}

		try {
			int[] list = (int[]) obj;
			for (int i = 0; i < list.length; i++) {
				String tmp = String.valueOf(list[i]);
				stringBuilder.append(first ? String.valueOf(list[i])
						: stringBuilder.toString() + ";"
								+ String.valueOf(list[i]));
				first = false;
			}
			return val;
		} catch (Exception e) {
			LOGGER.error("exception occured in parseArray", e);
		}

		try {
			double[] list = (double[]) obj;
			for (int i = 0; i < list.length; i++) {
				stringBuilder.append(first ? String.valueOf(list[i])
						: stringBuilder.toString() + ";"
								+ String.valueOf(list[i]));
				first = false;
			}
			return stringBuilder.toString();
		} catch (Exception e) {
			LOGGER.error("exception occured in parseArray", e);
		}

		try {
			NumberFormat nf = new DecimalFormat("#9.99");
			float[] list = (float[]) obj;
			for (int i = 0; i < list.length; i++) {
				stringBuilder.append(first ? nf.format(list[i]) : stringBuilder
						.toString() + ";" + nf.format(list[i]));
				first = false;
			}
			return stringBuilder.toString();
		} catch (Exception e) {
			LOGGER.error("exception occured in parseArray", e);
		}

		return val;
	}

	private List removeDuplicates(List props) {
		List outList = new ArrayList();
		PropBean current = null;
		PropBean next = null;

		Iterator iter = props.iterator();
		if (!iter.hasNext()) {
			return outList;
		}

		current = (PropBean) iter.next();
		while (iter.hasNext()) {
			next = (PropBean) iter.next();
			if (!current.propKey.equalsIgnoreCase(next.propKey)
					|| !current.propKey.equalsIgnoreCase(next.propKey)) {
				outList.add(current);
			}
			current = next;
		}
		outList.add(current);
		return outList;
	}

	public final class PropBean implements Comparable {

		private String propKey;
		private String propType;
		private String propValue;

		public String getPropKey() {
			return this.propKey;
		}

		public void setPropKey(String s) {
			this.propKey = s;
		}

		public String getPropType() {
			return this.propType;
		}

		public void setPropType(String s) {
			this.propType = s;
		}

		public String getPropValue() {
			return this.propValue == null ? "" : this.propValue;
		}

		public void setPropValue(String s) {
			this.propValue = s;
		}

		public int compareTo(Object o) {
			PropBean pb = (PropBean) o;
			if (!this.propKey.equalsIgnoreCase(pb.propKey)) {
				return this.propKey.compareToIgnoreCase(pb.propKey);
			}
			return this.propType.compareToIgnoreCase(pb.propType);
		}

	}

	public final class SystemBean implements Comparable {

		private String systemKey;
		private String systemStatus;
		private String exceptionClassName;
		private String exceptionMsg;

		public String getSystemKey() {
			return this.systemKey;
		}

		public void setSystemKey(String s) {
			this.systemKey = s;
		}

		public String getSystemStatus() {
			return this.systemStatus;
		}

		public void setSystemStatus(String s) {
			this.systemStatus = s;
		}

		public String getExceptionClassName() {
			return exceptionClassName;
		}

		public void setExceptionClassName(String string) {
			exceptionClassName = string;
		}

		public String getExceptionMsg() {
			return exceptionMsg;
		}

		public void setExceptionMsg(String string) {
			exceptionMsg = string;
		}

		public int compareTo(Object o) {
			SystemBean sb = (SystemBean) o;
			return this.systemKey.compareToIgnoreCase(sb.systemKey);
		}
	}
}
