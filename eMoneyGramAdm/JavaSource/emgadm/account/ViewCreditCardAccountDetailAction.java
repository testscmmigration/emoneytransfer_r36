/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.account;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;



import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.model.UserProfile;
import emgadm.view.AccountCommentView;

import emgshared.exceptions.DataSourceException;
import emgshared.model.AccountComment;
import emgshared.model.ConsumerCreditCardAccount;
import emgshared.model.TaintIndicatorType;
import emgshared.services.ConsumerAccountService;
import emgshared.services.ServiceFactory;



/**
 * @author A131
 *
 */
public class ViewCreditCardAccountDetailAction extends EMoneyGramAdmBaseAction
{
	private static final String FORWARD_SUCCESS = "success";
	

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws DataSourceException
	{
		ActionForward forward;
		CreditCardAccountDetailForm inputForm =	(CreditCardAccountDetailForm) form;
		UserProfile up = getUserProfile(request);

		int accountId = Integer.parseInt(inputForm.getAccountId());
		ConsumerAccountService accountService =	ServiceFactory.getInstance().getConsumerAccountService();
		ConsumerCreditCardAccount account =	accountService.getCreditCardAccount(accountId, up.getUID());
		populateForm(inputForm, account, up);
		forward = mapping.findForward(FORWARD_SUCCESS);
		
		if (up.hasPermission("addCCBlockedList"))
		{
			request.setAttribute("addBlockedCc", "Y");
		}
						
		if (up.hasPermission("addBinBlockedList"))
		{
			request.setAttribute("addBlockedBin", "Y");
		}
		
		if (up.hasPermission("updateAccountType"))
		{
			request.setAttribute("updateAcctType", "Y");
		}
		
		if (inputForm.getAccountTypeCode().startsWith("DC"))
		{
			request.setAttribute("debitCard", "Y");
		}
								
		return forward;
	}

	/**
	 * @param form
	 * @param account
	 * 
	 * Created on Mar 4, 2005
	 */
	private void populateForm(
		CreditCardAccountDetailForm form,
		ConsumerCreditCardAccount account,
		UserProfile userProfile)
		throws DataSourceException
	{
		int accountId = account.getId();
		form.setAccountId(String.valueOf(accountId));
		form.setAccountTypeCode(account.getType().getCode());
		form.setAccountTypeDesc(account.getTypeDescription());
		form.setCcHashed(account.getAccountNumberHash());
		form.setBinNumber(account.getAccountNumberBin());
		form.setAccountNumberMask(account.getAccountNumberMask());
		form.setCombinedStatusCode(account.getStatus().getCombinedCode());
		form.setCustId(String.valueOf(account.getConsumerId()));
		form.setExpirationMonth(String.valueOf(account.getExpireMonth()));
		form.setExpirationYear(String.valueOf(account.getExpireYear()));
		form.setCardToken(account.getCardToken()); 
		

		if (account.getCreditOrDebitCode().equalsIgnoreCase("U")){
			form.setCardType("Unknown");
		}else if (account.getCreditOrDebitCode().equalsIgnoreCase("C")){
			form.setCardType("Credit");
		}else if (account.getCreditOrDebitCode().equalsIgnoreCase("D")){
			form.setCardType("Debit");
		}else if (account.getCreditOrDebitCode().equalsIgnoreCase("P")){
			form.setCardType("Prepaid");
		}else if (account.getCreditOrDebitCode().equalsIgnoreCase("H")){
			form.setCardType("Charge");
		}else if (account.getCreditOrDebitCode().equalsIgnoreCase("R")){
			form.setCardType("Deferred Debit");
		}
		if (account.isAccountNumberBlocked()) {
			form.setAccountTaintText(TaintIndicatorType.BLOCKED_TEXT);
		} else if (account.isAccountNumberNotBlocked()) {
			form.setAccountTaintText(TaintIndicatorType.NOT_BLOCKED_TEXT);
		} else if (account.isAccountNumberOverridden()) {
			form.setAccountTaintText(TaintIndicatorType.OVERRIDE_TEXT);
		} else {
			form.setAccountTaintText("");
		}

		if (account.isCreditCardBinBlocked()) {
			form.setBinTaintText(TaintIndicatorType.BLOCKED_TEXT);
		} else if (account.isCreditCardBinNotBlocked()) {
			form.setBinTaintText(TaintIndicatorType.NOT_BLOCKED_TEXT);
		} else if (account.isCreditCardBinOverridden()) {
			form.setBinTaintText(TaintIndicatorType.OVERRIDE_TEXT);
		} else {
			form.setBinTaintText("");
		}

		ConsumerAccountService accountService =
			ServiceFactory.getInstance().getConsumerAccountService();
		
// MAS - PCI		
//		DecryptionService decryptionService =
//			emgadm.services.ServiceFactory.getInstance().getDecryptionService();
//		String encryptedAccountNumber =
//			accountService.getEncryptedAccountNumber(
//				accountId,
//				userProfile.getUID());
//		String decryptedAccountNumber =
//			decryptionService.decryptCreditCardNumber(encryptedAccountNumber);
//		if (userProfile.getRole().hasPermission("viewClearAccountNumbers"))
//		{
//			inputForm.setAccountNumberMask(decryptedAccountNumber);
//		} else
//		{
//			inputForm.setAccountNumberMask(
//				decryptedAccountNumber.substring(0, 6)
//					+ "*****"
//					+ account.getAccountNumberMask());
//		}

//		form.setAccountNumberMask("******" + account.getAccountNumberMask());

		form.setAccountStatusOptions(
			convertMapToLabelValueListSorted(
				accountService.getAccountStatusDescriptions()));

		form.setAccountCommentReasons(
			convertMapToLabelValueListSorted(
				accountService.getAccountCommentReasons()));

		form.setComments(
			createAccountProfileCommentViews(
				accountService.getAccountCmnts(
					accountId,
					userProfile.getUID(),null)));
	}

	private AccountCommentView[] createAccountProfileCommentViews(List accountComments)
	{
		AccountCommentView[] views =
			new AccountCommentView[accountComments == null
				? 0
				: accountComments.size()];
		if (accountComments != null)
		{
			int i = 0;
			for (Iterator iter = accountComments.iterator();
				iter.hasNext();
				++i)
			{
				AccountComment comment = (AccountComment) iter.next();
				views[i] = new AccountCommentView(comment);
			}
		}
		return views;
	}
}
