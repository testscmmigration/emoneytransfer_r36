/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.account;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.model.UserProfile;
import emgadm.services.DecryptionService;
import emgadm.view.AccountCommentView;
import emgshared.exceptions.DataSourceException;
import emgshared.model.AccountComment;
import emgshared.model.ConsumerBankAccount;
import emgshared.model.MicroDeposit;
import emgshared.model.TaintIndicatorType;
import emgshared.services.ConsumerAccountService;
import emgshared.services.MicroDepositService;
import emgshared.services.ServiceFactory;

/**
 * @author A131
 *
 */
public class ViewBankAccountDetailAction extends EMoneyGramAdmBaseAction
{
	private static final String FORWARD_SUCCESS = "success";

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws DataSourceException
	{
		ActionForward forward;
		BankAccountDetailForm inputForm = (BankAccountDetailForm) form;
		UserProfile up = getUserProfile(request);
		int accountId = Integer.parseInt(inputForm.getAccountId());

		ConsumerAccountService accountService =
			ServiceFactory.getInstance().getConsumerAccountService();
		ConsumerBankAccount account =
			accountService.getBankAccount(accountId, up.getUID());
		populateForm(inputForm, account, up);

		MicroDepositService mds =
			ServiceFactory.getInstance().getMicroDepositService();
		MicroDeposit md = new MicroDeposit();
		md.setCustAccountId(accountId);
		md.setCustAccountVersionNbr(1);
		Collection col = mds.getMicroDepositValidations(md);
		if (col == null || col.size() == 0)
		{
			inputForm.setMicroDeposit(null);
		} else
		{
			Iterator iter = col.iterator();
			while (iter.hasNext())
			{
				md = (MicroDeposit) iter.next();
			}
			inputForm.setMicroDeposit(md);
		}
		
		if (up.hasPermission("addBankBlockedList"))
		{
			request.setAttribute("addBlockedBank", "Y");
		}

		if (up.hasPermission("validateMicroDeposit"))
		{
			request.setAttribute("validateMD", "Y");
		}

		forward = mapping.findForward(FORWARD_SUCCESS);
		return forward;
	}

	/**
	 * @param inputForm
	 * @param account
	 * 
	 * Created on Mar 4, 2005
	 */
	private void populateForm(
		BankAccountDetailForm inputForm,
		ConsumerBankAccount account,
		UserProfile userProfile)
		throws DataSourceException
	{
		int accountId = account.getId();
		inputForm.setAccountId(String.valueOf(accountId));
		inputForm.setAccountTypeCode(account.getType().getCode());
		inputForm.setAccountTypeDesc(account.getTypeDescription());
		inputForm.setCombinedStatusCode(account.getStatus().getCombinedCode());
		inputForm.setCustId(String.valueOf(account.getConsumerId()));

		if (account.isAccountNumberBlocked())
		{
			inputForm.setAccountTaintText(TaintIndicatorType.BLOCKED_TEXT);
		} else if (account.isAccountNumberNotBlocked())
		{
			inputForm.setAccountTaintText(TaintIndicatorType.NOT_BLOCKED_TEXT);
		} else if (account.isAccountNumberOverridden())
		{
			inputForm.setAccountTaintText(TaintIndicatorType.OVERRIDE_TEXT);
		} else
		{
			inputForm.setAccountTaintText("");
		}

		if (account.isBankAbaNumberBlocked())
		{
			inputForm.setAbaTaintText(TaintIndicatorType.BLOCKED_TEXT);
		} else if (account.isBankAbaNumberNotBlocked())
		{
			inputForm.setAbaTaintText(TaintIndicatorType.NOT_BLOCKED_TEXT);
		} else if (account.isBankAbaNumberOverridden())
		{
			inputForm.setAbaTaintText(TaintIndicatorType.OVERRIDE_TEXT);
		} else
		{
			inputForm.setAbaTaintText("");
		}

		inputForm.setFinancialInstitutionName(
			account.getFinancialInstitutionName());
		inputForm.setRoutingNumber(account.getABANumber());

		ConsumerAccountService accountService =
			ServiceFactory.getInstance().getConsumerAccountService();

		if (userProfile.hasPermission("viewClearAccountNumbers"))
		{
			DecryptionService decryptionService =
				emgadm
					.services
					.ServiceFactory
					.getInstance()
					.getDecryptionService();
			String encryptedAccountNumber =
				accountService.getEncryptedAccountNumber(
					accountId,
					userProfile.getUID());
			String decryptedAccountNumber =
				decryptionService.decryptBankAccountNumber(
					encryptedAccountNumber);
			inputForm.setAccountNumberMask(decryptedAccountNumber);
		} else
		{
			inputForm.setAccountNumberMask(
				"*****" + account.getAccountNumberMask());
		}

		inputForm.setAccountStatusOptions(
			convertMapToLabelValueListSorted(
				accountService.getAccountStatusDescriptions()));

		inputForm.setAccountCommentReasons(
			convertMapToLabelValueListSorted(
				accountService.getAccountCommentReasons()));

		inputForm.setComments(
			createAccountProfileCommentViews(
				accountService.getAccountCmnts(
					accountId,
					userProfile.getUID(), null)));
	}

	private AccountCommentView[] createAccountProfileCommentViews(List accountComments)
	{
		AccountCommentView[] views =
			new AccountCommentView[accountComments == null
				? 0
				: accountComments.size()];
		if (accountComments != null)
		{
			int i = 0;
			for (Iterator iter = accountComments.iterator();
				iter.hasNext();
				++i)
			{
				AccountComment comment = (AccountComment) iter.next();
				views[i] = new AccountCommentView(comment);
			}
		}
		return views;
	}

}
