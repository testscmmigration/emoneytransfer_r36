/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.account;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.view.AccountCommentView;
import emgshared.model.AccountStatus;
import emgshared.model.MicroDeposit;

/**
 * @author A131
 *
 */
public class BankAccountDetailForm extends EMoneyGramAdmBaseValidatorForm
{
	private String accountId;
	private String accountVersionNumber;
	private String custId;
	private String accountNumberMask;
	private String routingNumber;
	private String financialInstitutionName;
	private String statusCode;
	private String subStatusCode;
	private String accountTypeCode;
	private String accountTypeDesc;
	private String accountTaintText;
	private String abaTaintText;
	private List accountStatusOptions = new ArrayList(0);
	private List accountCommentReasons = new ArrayList(0);
	private AccountCommentView[] comments = new AccountCommentView[0];
	private MicroDeposit microDeposit;

	public String getAccountNumberMask()
	{
		return accountNumberMask;
	}

	public String getFinancialInstitutionName()
	{
		return financialInstitutionName;
	}

	public void setAccountNumberMask(String string)
	{
		accountNumberMask = string;
	}

	public void setFinancialInstitutionName(String string)
	{
		financialInstitutionName = string;
	}

	public String getAccountId()
	{
		return accountId;
	}

	public String getRoutingNumber()
	{
		return routingNumber;
	}

	public void setRoutingNumber(String string)
	{
		routingNumber = string;
	}

	public String getStatusCode()
	{
		return statusCode;
	}

	public String getSubStatusCode()
	{
		return subStatusCode;
	}

	public void setStatusCode(String string)
	{
		statusCode = string;
	}

	public void setSubStatusCode(String string)
	{
		subStatusCode = string;
	}

	public String getCombinedStatusCode()
	{
		return this.statusCode + AccountStatus.DELIMITER + this.subStatusCode;
	}

	public void setCombinedStatusCode(String combinedStatusCode)
	{
		String[] codes =
			StringUtils.split(combinedStatusCode, AccountStatus.DELIMITER);
		this.statusCode = (codes.length > 0 ? codes[0] : null);
		this.subStatusCode = (codes.length > 1 ? codes[1] : null);
	}

	public String getAccountTypeDesc()
	{
		return accountTypeDesc;
	}

	public List getAccountStatusOptions()
	{
		return accountStatusOptions;
	}

	public void setAccountTypeDesc(String string)
	{
		accountTypeDesc = string;
	}

	public String getCustId()
	{
		return custId;
	}

	public void setAccountId(String string)
	{
		accountId = string;
	}

	public void setCustId(String string)
	{
		custId = string;
	}

	public void setAccountStatusOptions(List list)
	{
		accountStatusOptions = (list == null ? new ArrayList(0) : list);
	}

	public String getAccountTypeCode()
	{
		return accountTypeCode;
	}

	public void setAccountTypeCode(String string)
	{
		accountTypeCode = string;
	}

	public List getAccountCommentReasons()
	{
		return accountCommentReasons;
	}

	public AccountCommentView[] getComments()
	{
		return comments;
	}

	public void setAccountCommentReasons(List list)
	{
		accountCommentReasons = (list == null ? new ArrayList(0) : list);
	}

	public void setComments(AccountCommentView[] views)
	{
		comments = views.clone();
	}
	/**
	 * @return
	 * 
	 * Created on Mar 4, 2005
	 */
	public String getAccountVersionNumber()
	{
		return accountVersionNumber;
	}

	/**
	 * @param string
	 * 
	 * Created on Mar 4, 2005
	 */
	public void setAccountVersionNumber(String string)
	{
		accountVersionNumber = string;
	}

	/**
	 * @return
	 */
	public String getAccountTaintText() {
		return accountTaintText;
	}

	/**
	 * @param string
	 */
	public void setAccountTaintText(String string) {
		accountTaintText = string;
	}

	/**
	 * @return
	 */
	public String getAbaTaintText() {
		return abaTaintText;
	}

	/**
	 * @param string
	 */
	public void setAbaTaintText(String string) {
		abaTaintText = string;
	}

	/**
	 * @return
	 */
	public MicroDeposit getMicroDeposit()
	{
		return microDeposit;
	}

	/**
	 * @param deposit
	 */
	public void setMicroDeposit(MicroDeposit deposit)
	{
		microDeposit = deposit;
	}

}
