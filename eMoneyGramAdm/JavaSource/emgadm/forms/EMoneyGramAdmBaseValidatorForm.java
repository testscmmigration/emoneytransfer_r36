package emgadm.forms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

import emgadm.constants.EMoneyGramAdmSessionConstants;
import emgadm.model.Role;
import emgadm.model.UserProfile;

public class EMoneyGramAdmBaseValidatorForm extends ValidatorForm {
    private List<Role> roles = new ArrayList<Role>();

    private String add = null;

    private String edit = null;

    private String cancel = null;

    private String view = null;

    private String search = null;

    public EMoneyGramAdmBaseValidatorForm() {
    }

    public String getAdd() {
        return add;
    }

    public String getCancel() {
        return cancel;
    }

    public String getEdit() {
        return edit;
    }

    public Collection getRoles() {
        return roles;
    }

    public String getView() {
        return view;
    }

    public void setAdd(String string) {
        add = string;
    }

    public void setCancel(String string) {
        cancel = string;
    }

    public void setEdit(String string) {
        edit = string;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public void setView(String string) {
        view = string;
    }

    public Role getRole(String guid) {
        for (Iterator<Role> iter = roles.iterator(); iter.hasNext();) {
            Role role = iter.next();
            if (role.getId().equals(guid)) {
                return role;
            }
        }
        return null;
    }

    public List<Role> getRoles(List<String> guid) {
    	List<Role> result = new ArrayList<Role>();
        for (Iterator<Role> iter = roles.iterator(); iter.hasNext();) {
            Role role = iter.next();
            if (role.getId().equals(guid)) {
                result.add(role);
            }
        }
        return result;
    }

    public static UserProfile getUserProfile(HttpServletRequest request) {
        HttpSession session = request.getSession();
        return ((UserProfile) session
                .getAttribute(EMoneyGramAdmSessionConstants.USER_PROFILE));
    }

    public void reset(ActionMapping arg0, HttpServletRequest arg1) {
        super.reset(arg0, arg1);
        add = null;
        edit = null;
        cancel = null;
        view = null;
        search = null;
    }

    public ActionErrors validate(ActionMapping arg0, HttpServletRequest arg1) {
        return super.validate(arg0, arg1);
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String string) {
        search = string;
    }

}
