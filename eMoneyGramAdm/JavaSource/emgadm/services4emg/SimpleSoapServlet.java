package emgadm.services4emg;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.TooManyListenersException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.owasp.esapi.ESAPI;

import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.simplesoap.RequestExceptionResponseBean;
import emgshared.simplesoap.ServerAwakeRequestResponse;

/**
 * @author G.R.Svenddal Created on Jun 22, 2005 SimpleSoapServlet.java
 * 
 * Provides a very simple SOAP-like interface to support proxied communication between the
 * Consumer and Admin services. see emg.services.SimpleSoapClient, which is my counterpart.
 * 
 * Currently also contains the handler code for the soapy calls, which is fine for now since
 * that code is so short.
 * 
 *  
 */

public class SimpleSoapServlet extends HttpServlet
{

	private static HashMap hmListeners = new HashMap();

	public void init() throws ServletException
	{
		// the listener classes must be awoken *Somewhere*
		ListenerVBVEnroll.class.getClass();
		ListenerVBVValidate.class.getClass();
		TransactionProcessListener.class.getClass();
	}

	public static void addListener(SSRequestListener addSSRL)
		throws TooManyListenersException, IllegalArgumentException
	{
		if (addSSRL == null)
			throw new IllegalArgumentException("Can't add a null Listener");
		SSRequestListener sameOne =
			(SSRequestListener) hmListeners.get(addSSRL);
		if (sameOne != null)
			throw new TooManyListenersException(
				"The Listener for Request "
					+ sameOne.getRequestBeanTypeName()
					+ "has already been registered");

		hmListeners.put(addSSRL.getRequestBeanTypeName(), addSSRL);
	}

	public void doPost(
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException
	/*
	 * IT IS UNACCEPTABLE FOR THIS SERVLET TO THROW ANYTHING other then an
	 * IOException, which usually means the server has more serious troubles.
	 * 
	 * SimpleSoapServlet must ALWAYS return a response bean. If there's a
	 * problem, catch it and return the bad news in the response bean.
	 */
	{
		// ==================== EXTRACT REQUEST BEAN ========================
		InputStream is = request.getInputStream();
		XMLDecoder xd = new XMLDecoder(is);
		Object oReq = xd.readObject();

		Class c = oReq.getClass();
		String type = c.getName();
		EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
			"doPost() - Request Bean Type:" + type);

		// Pessimistic coding. We're probably going to need one of these
		RequestExceptionResponseBean rerb = new RequestExceptionResponseBean();
		rerb.setRequestBean(oReq);
		rerb.setUserMessage("default message.");

		// The Return Bean. Presume a problem until proven otherwise.
		Object oRet = rerb;

		// ServerAwake Bean?
		if (type.equals(ServerAwakeRequestResponse.class.getName()))
		{
			String sentMessage =
				((ServerAwakeRequestResponse) oReq).getMessage();
			((ServerAwakeRequestResponse) oReq).setMessage(
				"SSServer Awake. message:" + sentMessage);
			oRet = oReq;
		} else
		{
			Object x = hmListeners.get(type);
			if (x == null)
			{
				rerb.setUserMessage(
					"No Registered Request Handler for " + type);
				rerb.setExceptionStackDump("no stack available");
				oRet = rerb;
			}
			// Call the Listener processCall().
			else
			{
				SSRequestListener handler = null;
				try
				{
					handler = (SSRequestListener) x;
					// this should *never* throw casting exception, right?
					oRet = handler.processCall(oReq, request.getContextPath());
				} catch (SimpleSoapRequestException ssre)
				{
					// we expect any thrower to log themselves first.
					// throw only to send exception data to SimpleSoapClient.
					ssre.fixupRERBean(rerb);
					oRet = rerb;
				}
			}
		}
		// We must return *something*. Null doesn't cut it.
		if (oRet == null)
		{
			oRet = rerb;
			rerb.setUserMessage("Listener processCall() returned null");
		}

		// ====================SEND RESPONSE OBJECT ================

		response.setContentType("text/xml; charset=ISO-8859-1");
		// response.setContentLength((int)xmlb.length()); TODO - do we need to
		// set the length? probably not, since there's little chance we'll get
		// very big.
		OutputStream os = response.getOutputStream();
		XMLEncoder xe = new XMLEncoder(os);
		xe.writeObject(oRet);
		xe.close();
		os.flush();
		os.close();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(
			"Called by Get Method from "
				+ ESAPI.encoder().encodeForHTMLAttribute(request.getRemoteAddr())
				+ " with query "
				+ ESAPI.encoder().encodeForHTMLAttribute(request.getQueryString())
				+ "  This should NOT happen");
		response.setContentType("text/html");
		response.sendError(400); // Bad Request.
	}

}
