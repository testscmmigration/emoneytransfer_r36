package emgadm.services4emg;

import java.util.TooManyListenersException;

import emgadm.services.SecuredCreditCardService;
import emgadm.services.ServiceFactory;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.model.CreditCard;
import emgshared.services.ConsumerAccountService;
import emgshared.services.verifiedbyvisa.CyberSourceVBVServiceImpl;
import emgshared.services.verifiedbyvisa.VBVLogger;
import emgshared.simplesoap.CheckEnrollRequest;

/**
 * @author G.R.Svenddal Created on Jul 14, 2005 ListenerVBVEnroll.java
 */
public class ListenerVBVEnroll implements SSRequestListener
{
	static {
		try
		{
			SimpleSoapServlet.addListener(new ListenerVBVEnroll());
		} catch (TooManyListenersException e)
		{
			EMGSharedLogger.getLogger(
				ListenerVBVEnroll.class.toString()).error(
				"ListenerVBVEnroll added twice!");
		}
	}

	public String getRequestBeanTypeName()
	{
		return "emgshared.simplesoap.CheckEnrollRequest";
	}

	public Object processCall(Object inBean, String contextRoot) throws SimpleSoapRequestException
	{
		try
		{
			CheckEnrollRequest cer = (CheckEnrollRequest) inBean;
			VBVLogger.error("ListenerVBVEnroll acct#:" + cer.getAccount());
			int accountId = cer.getAccount();

			ConsumerAccountService accountService =
				emgshared
					.services
					.ServiceFactory
					.getInstance()
					.getConsumerAccountService();

			SecuredCreditCardService srv =
				ServiceFactory.getInstance().getSecuredCreditCardService();

			String me = "VBVCheckEnroll";
			emgshared.model.ConsumerCreditCardAccount ca =
				accountService.getCreditCardAccount(accountId, me);

			CreditCard card = srv.getCreditCard(ca, me);

			return CyberSourceVBVServiceImpl.getInstance().checkEnrollment(
				card,
				cer.getMerchantReferenceCode(),
				cer.getUnitPrice());

		} catch (Throwable e)
		{
			VBVLogger.getLogger().error("ListenerVBVEnroll Exception", e);
			return new SimpleSoapRequestException(
				"CheckEnrollRequest exception",
				e);
		}
	}

}
