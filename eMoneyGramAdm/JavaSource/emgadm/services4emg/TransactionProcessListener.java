package emgadm.services4emg;

/**
 * @author Jawahar Varadhan
 * Created on Jul 21, 2005
 * Implementation of SSRequestListenerInterface
 * @param requestBean: must be a proper Java bean.
 * @return returns a proper Java bean with response content. 
 * 
 **/
import java.util.TooManyListenersException;

import emgadm.dataaccessors.ManagerFactory;
import emgadm.dataaccessors.TransactionManager;
import emgadm.transqueue.TransactionProcess;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.model.ListenerProcess;
import emgshared.model.ListenerProcessType;
import emgshared.model.Transaction;
import emgshared.property.EMTSharedDynProperties;

public class TransactionProcessListener implements SSRequestListener
{
	private static EMTSharedDynProperties dynProps = new EMTSharedDynProperties();

	static {
		try
		{
			SimpleSoapServlet.addListener(new TransactionProcessListener());
		} catch (TooManyListenersException e)
		{
			EMGSharedLogger.getLogger(
				TransactionProcessListener.class.toString()).error(
				"TransactionProcessListener added twice!");
		}
	}

	public String getRequestBeanTypeName()
	{
		return (ListenerProcess.class.getName());
	}

	public Object processCall(Object requestBean, String contextRoot)
		throws SimpleSoapRequestException
	{

		ListenerProcess processReturn = (ListenerProcess) requestBean;
		TransactionManager tm = ManagerFactory.createTransactionManager();
		Transaction tran = tm.getTransaction(processReturn.getTranId());
		
		ListenerProcessType processType =
			ListenerProcessType.getInstance(
				processReturn.getListenerProcessType().toString());
		if (processType.isAutoExpressPaymentType())
		{
			processReturn = TransactionProcess.AutoApproveEP(tran, contextRoot);
		} else if (processType.isCancelSameDayServiceType())
		{
			processReturn =
				TransactionProcess.consumerCancelTransaction(
					tran.getEmgTranId(), contextRoot);
		} else if (processType.isCancelEconomyServiceType())
		{
			processReturn =
				TransactionProcess.consumerCancelTransaction(
					tran.getEmgTranId(), contextRoot);
		} else if (processType.isCancelExpressPaymentType())
		{
			processReturn =
				TransactionProcess.consumerCancelTransaction(
					tran.getEmgTranId(), contextRoot);
		}
		return processReturn;
	}
}