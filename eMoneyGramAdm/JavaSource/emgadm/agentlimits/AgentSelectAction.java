package emgadm.agentlimits;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.Agent;
import emgadm.util.StringHelper;

/**
 * @version 1.0
 * @author
 */
public class AgentSelectAction extends EMoneyGramAdmBaseAction {

	public ActionForward execute(ActionMapping mapping, ActionForm f,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		AgentSelectForm form = (AgentSelectForm) f;
		// Collection agents = new ArrayList();
		Agent agent = null;
		// MBO-4439
		// Cancel adding biller limits and return to biller limit list
		if (!StringHelper.isNullOrEmpty(form.getSubmitExit())) {
			clearSessionObjects(request);
			if (request.getSession().getAttribute("agent") != null) {
				request.getSession().removeAttribute("agent");
			}
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_BILLER_LIMITS);
		}

		// return to corporation selection screen
		if (!StringHelper.isNullOrEmpty(form.getSubmitReturn())) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_BILLER_AGENT_LIMITS);
		}

		Collection<?> agents = (Collection<?>) request.getSession().getAttribute(
				"agents");
		Collection<?> tmpAgents = agents;

		// user selected an agent
		if (!StringHelper.isNullOrEmpty(form.getSubmitAgent())) {
			// retrieve the specified agent to pass to change limit screen
			Iterator iter = agents.iterator();
			while (iter.hasNext()) {
				agent = (Agent) iter.next();
				if (agent.getAgentId().equals(form.getAgentSelection())) {
					request.getSession().setAttribute("agent", agent);
					break;
				}
			}
			clearSessionObjects(request);
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_CHANGE_LIMIT);
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitAgentFilter())) {
			tmpAgents = filterAgents((List) agents, form.getAgentName());
		}

		if (tmpAgents == null || tmpAgents.size() == 0) {
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
					"error.no.biller.match.found", form.getAgentName()));
			saveErrors(request, errors);
		} else {
			form.setAgents(tmpAgents);
			if (StringHelper.isNullOrEmpty(form.getAgentSelection())) {
				Iterator iter = tmpAgents.iterator();
				form.setAgentSelection(((Agent) iter.next()).getAgentId());
			}
		}
		return mapping
				.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private void clearSessionObjects(HttpServletRequest request) {
		if (request.getSession().getAttribute("corp") != null) {
			request.getSession().removeAttribute("corp");
		}

		if (request.getSession().getAttribute("corps") != null) {
			request.getSession().removeAttribute("corps");
		}

		if (request.getSession().getAttribute("agents") != null) {
			request.getSession().removeAttribute("agents");
		}
	}
}
