package emgadm.agentlimits;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class ShowBillerLimitsForm extends EMoneyGramAdmBaseValidatorForm
{
	private String agentNameSearch = null;
	private String billerCodeSearch = null;
	
	private String submitSearchKey = null;
	private String submitSearchCode = null;
	private String submitAdd = null;
	private String submitShowAll = null;
	
	

	public String getAgentNameSearch() {
		return agentNameSearch == null ? "" : agentNameSearch;
	}

	public void setAgentNameSearch(String string) {
		agentNameSearch = string;
	}

	public String getBillerCodeSearch() {
		return billerCodeSearch == null ? "" : billerCodeSearch;
	}

	public void setBillerCodeSearch(String string) {
		billerCodeSearch = string;
	}

	public String getSubmitSearchKey() {
		return submitSearchKey;
	}

	public void setSubmitSearchKey(String string) {
		submitSearchKey = string;
	}

	public String getSubmitSearchCode() {
		return submitSearchCode;
	}

	public void setSubmitSearchCode(String string) {
		submitSearchCode = string;
	}

	public String getSubmitAdd() {
		return submitAdd;
	}

	public void setSubmitAdd(String string) {
		submitAdd = string;
	}

	public String getSubmitShowAll() {
		return submitShowAll;
	}

	public void setSubmitShowAll(String string) {
		submitShowAll = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);

		billerCodeSearch = "";
		agentNameSearch = "";
		submitSearchKey = null;
		submitSearchCode = null;
		submitAdd = null;
		submitShowAll = null;
	}
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
	{
		ActionErrors errors = super.validate(mapping, request);
		
		if (submitSearchKey != null &&
			StringHelper.isNullOrEmpty(agentNameSearch)) {
			errors.add(ActionErrors.GLOBAL_ERROR, 
				new ActionError("errors.required", "Biller Name Search Key")); 			
		}
		
		if (submitSearchCode != null){
			if (StringHelper.isNullOrEmpty(billerCodeSearch)) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "Biller Code"));
			}else{
				if (StringHelper.containsNonDigits(billerCodeSearch)){
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.integer", "Biller Code"));
				}	
				if (billerCodeSearch.trim().length() != 4) {
					String[] parms = {"Biller Code", "4"};
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.length.wrong", parms));
				}
			}
		}
		
		return errors;
	}
}
