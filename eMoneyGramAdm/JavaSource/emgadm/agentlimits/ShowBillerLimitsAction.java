package emgadm.agentlimits;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.constants.EMoneyGramMerchantIds;
import emgadm.model.CommandAuth;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.BillerLimit;
import emgshared.model.UserActivityType;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.BillerLimitService;
import emgshared.services.ServiceFactory;

/**
 * @version 	1.0
 * @author
 */
public class ShowBillerLimitsAction extends EMoneyGramAdmBaseAction
{

	private static EMTSharedDynProperties dynProps =
		new EMTSharedDynProperties();
	static String AGENTID = "agentId";
	static String SEARCH_BILLER_CODE = "searchBillerCode";
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		ActionErrors errors = new ActionErrors();
		ShowBillerLimitsForm showBillerLimitsForm = (ShowBillerLimitsForm) form;
		UserProfile up = getUserProfile(request);
		setAuth(request, up);
		Collection billerLimits = new ArrayList();

		//  go to add a new biller limit selection screen
		if (!StringHelper.isNullOrEmpty(showBillerLimitsForm.getSubmitAdd()))
		{
			request.getSession().setAttribute("eMGA", "Y");
			return mapping.findForward(	EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_BILLER_AGENT_LIMITS);
		}

		//  the user comes back from the add limit screen
		if (request.getSession().getAttribute("eMGA") != null)
		{
			if (request.getSession().getAttribute(AGENTID) != null) {
				Map<String, String> transferMap = (Map<String, String>) request
						.getSession().getAttribute(AGENTID);
				String agentName = null;
				if (transferMap != null)
					if (transferMap.containsKey(AGENTID))
						agentName = transferMap.get(AGENTID);
				showBillerLimitsForm.setAgentNameSearch(agentName);
			}
			if (request.getSession().getAttribute(SEARCH_BILLER_CODE) != null) {
				Map<String, String> transferMap = (Map<String, String>) request
						.getSession().getAttribute(SEARCH_BILLER_CODE);
				String billerCode = null;
				if (transferMap != null)
					if (transferMap.containsKey(SEARCH_BILLER_CODE))
						billerCode = transferMap.get(SEARCH_BILLER_CODE);
				showBillerLimitsForm.setBillerCodeSearch(billerCode);

			}
			request.getSession().removeAttribute(AGENTID);
			request.getSession().removeAttribute(SEARCH_BILLER_CODE);
			request.getSession().removeAttribute("eMGA");

			if (!StringHelper.isNullOrEmpty(showBillerLimitsForm
					.getAgentNameSearch())) {
				showBillerLimitsForm.setSubmitSearchKey("Y");
			}
			if (!StringHelper.isNullOrEmpty(showBillerLimitsForm
					.getBillerCodeSearch())) {
				showBillerLimitsForm.setSubmitSearchCode("Y");
			}

		}

		if (!StringHelper.isNullOrEmpty(showBillerLimitsForm.getSubmitShowAll()))
		{
			showBillerLimitsForm.setAgentNameSearch("");
		}

		ServiceFactory sf = ServiceFactory.getInstance();
		BillerLimitService bls = sf.getBillerLimitService();
		String id = request.getParameter("id");
		if (id != null)
		{
			String status = "NAT";
			if ("Activate".equalsIgnoreCase(request.getParameter("action"))) {
				status = "ACT";
			}
			BillerLimit billerLimit = bls.getBillerLimit(Integer.parseInt(id));
			billerLimit.setStatus(status);
			bls.setBillerLimit(billerLimit);
			String logType = null;
			if (status.equalsIgnoreCase("NAT"))
			    logType = UserActivityType.EVENT_TYPE_BILLERDEACTIVATE;
			else
			    logType = UserActivityType.EVENT_TYPE_BILLERACTIVATE;

			try {
			    super.insertActivityLog(this.getClass(),request,null,logType,billerLimit.getId());    
            } catch (Exception e) { }
						
			if (request.getSession().getAttribute(AGENTID) != null)
			{
				Map<String, String> transferMap = (Map<String, String>) request
						.getSession().getAttribute(AGENTID);
				String agentName = null;
				if (transferMap != null)
					if (transferMap.containsKey(AGENTID))
						agentName = transferMap.get(AGENTID);
				showBillerLimitsForm.setAgentNameSearch(agentName);
				showBillerLimitsForm.setSubmitSearchKey("Y");
			}

			if (request.getSession().getAttribute(SEARCH_BILLER_CODE) != null) {
				Map<String, String> transferMap = (Map<String, String>) request
						.getSession().getAttribute(SEARCH_BILLER_CODE);
				String billerCode = null;
				if (transferMap != null)
					if (transferMap.containsKey(SEARCH_BILLER_CODE))
						billerCode = transferMap.get(SEARCH_BILLER_CODE);
				showBillerLimitsForm.setBillerCodeSearch(billerCode);
				showBillerLimitsForm.setSubmitSearchCode("Y");
			}
			errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("msg.biller.status.changed", id));
			saveErrors(request, errors);
		}

		if (!StringHelper.isNullOrEmpty(showBillerLimitsForm.getSubmitSearchKey()) || !StringHelper.isNullOrEmpty(
				showBillerLimitsForm.getSubmitShowAll()))
		{
			try
			{
				billerLimits =	bls.getBillerLimits(showBillerLimitsForm.getAgentNameSearch().trim());
				if (billerLimits == null || billerLimits.size() == 0)
				{
					errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("error.no.biller.found"));
					saveErrors(request, errors);
				}
				else {
					for (Iterator iterator = billerLimits.iterator(); iterator.hasNext();) {
						BillerLimit bl = (BillerLimit) iterator.next();
						bl.setPaymentProfileId(EMoneyGramMerchantIds.getMerchantMap().get(bl.getPaymentProfileId()));
					}
				}
					
			} catch (TooManyResultException e)
			{
				errors.add(	ActionErrors.GLOBAL_ERROR,new ActionError("error.too.many.results",	dynProps.getMaxDownloadTransactions() + ""));
				saveErrors(request, errors);
			}
			request.getSession().removeAttribute(SEARCH_BILLER_CODE);

			Map<String, String> transferMap = new HashMap();
			transferMap.put(AGENTID, showBillerLimitsForm.getAgentNameSearch());
			request.getSession().setAttribute(AGENTID, transferMap);
			
			request.setAttribute("billers", billerLimits);
		}

		if (!StringHelper.isNullOrEmpty(showBillerLimitsForm.getSubmitSearchCode()))
		{
			BillerLimit billerLimit = bls.getBillerLimit(showBillerLimitsForm.getBillerCodeSearch().trim());
			if (billerLimit == null)
			{
				errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("error.no.biller.found"));
				saveErrors(request, errors);
			} else
			{
				billerLimits.add(billerLimit);
				request.getSession().removeAttribute(AGENTID);
				
				Map<String, String> transferMap = new HashMap();
				transferMap.put(SEARCH_BILLER_CODE, showBillerLimitsForm.getBillerCodeSearch());
				request.getSession().setAttribute(SEARCH_BILLER_CODE, transferMap);
				
				if(showBillerLimitsForm.getBillerCodeSearch()!=null)
					request.getSession().setAttribute(SEARCH_BILLER_CODE,transferMap);
			}
			request.setAttribute("billers", billerLimits);
		}
		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private void setAuth(HttpServletRequest request, UserProfile up) {
		CommandAuth ca = null;
		//  setup a flag to see if the user has update permission
		if (request.getSession().getAttribute("auth") == null) {
			ca = new CommandAuth();
		} else {
			ca = (CommandAuth) request.getSession().getAttribute("auth");
		}

		ca.setEditAgentLimit(false);
		if (up.hasPermission(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_CHANGE_LIMIT)) {
			ca.setEditAgentLimit(true);
		}

		request.getSession().setAttribute("auth", ca);
	}

}
