package emgadm.agentlimits;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.constants.EMoneyGramMerchantIds;
import emgadm.model.Agent;
import emgadm.util.StringHelper;
import emgshared.model.BillerLimit;
import emgshared.model.SelectOption;
import emgshared.model.UserActivityType;
import emgshared.services.BillerLimitService;
import emgshared.services.ServiceFactory;

/**
 * @version 	1.0
 * @author
 */
public class ChangeLimitAction extends EMoneyGramAdmBaseAction
{

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ChangeLimitForm changeLimitForm = (ChangeLimitForm) form;

		//  remove all session variables and exit the current screen
		if (!StringHelper.isNullOrEmpty(changeLimitForm.getSubmitCancel()))
		{
			if (request.getSession().getAttribute("corps") != null)
			{
				request.getSession().removeAttribute("corps");
			}
			if (request.getSession().getAttribute("corp") != null)
			{
				request.getSession().removeAttribute("corp");
			}
			if (request.getSession().getAttribute("agents") != null)
			{
				request.getSession().removeAttribute("agents");
			}
			if (request.getSession().getAttribute("agentsSave") != null)
			{
				request.getSession().removeAttribute("agentsSave");
			}
			if (request.getSession().getAttribute("agent") != null)
			{
				request.getSession().removeAttribute("corps");
			}
			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_BILLER_LIMITS);
		}

		ServiceFactory sf = ServiceFactory.getInstance();
		BillerLimitService bls = sf.getBillerLimitService();
		Agent agent = null;
		BillerLimit billerLimit = null;
		Collection<SelectOption> merchantList = EMoneyGramMerchantIds.getMerchantList();
		// As part of MBO-5058 - Deleted the code related to removal of EMG and
		// WAP merchant id's from
		// merchant list as we have removed them in EMoneyGramMerchantIds class
		// itself
		request.getSession().setAttribute("merchantIds", merchantList);

		//  the user comes from the maintain biller limits screen with querystring
		if (request.getParameter("id") != null)
		{
			billerLimit = bls.getBillerLimit(Integer.parseInt(request.getParameter("id")));
			agent =	new Agent(billerLimit.getId(), "", billerLimit.getName(),billerLimit.getCity(),
					billerLimit.getReceiveCode(),3,	billerLimit.getStatus(),billerLimit.getState(), billerLimit.getPaymentProfileId());
			changeLimitForm.setLimitAmt(billerLimit.getLimit());
			changeLimitForm.setPaymentProfileId(billerLimit.getPaymentProfileId());
			request.getSession().setAttribute("agent", agent);
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		}

		if (request.getSession().getAttribute("agent") != null)
		{
			agent = (Agent) request.getSession().getAttribute("agent");
		}

		if (!StringHelper.isNullOrEmpty(changeLimitForm.getSubmitSave()))
		{
			billerLimit = new BillerLimit();
			billerLimit.setId(agent.getAgentId());
			billerLimit.setName(agent.getAgentName());
			billerLimit.setCity(agent.getAgentCity());
			billerLimit.setState(agent.getAgentState());
			billerLimit.setReceiveCode(agent.getReceiveCode());
			billerLimit.setLimit(changeLimitForm.getLimitAmt());
			billerLimit.setLagacyId(""); //  reserve for future use
			billerLimit.setStatus("ACT");
			billerLimit.setPaymentProfileId(changeLimitForm.getPaymentProfileId());
			bls.setBillerLimit(billerLimit);
			errors.add(	ActionErrors.GLOBAL_ERROR,	new ActionError("change.limit.successful"));
			try {super.insertActivityLog(this.getClass(),request,null,UserActivityType.EVENT_TYPE_BILLERCHANGELIMIT,billerLimit.getId());    
            } 
			catch (Exception ignore) { }
			
			saveErrors(request, errors);
			//  remove all related session objects	
			if (request.getSession().getAttribute("corp") != null)
			{
				request.getSession().removeAttribute("corp");
			}
			if (request.getSession().getAttribute("corps") != null)
			{
				request.getSession().removeAttribute("corps");
			}
			if (request.getSession().getAttribute("agent") != null)
			{
				request.getSession().removeAttribute("agent");
			}
			if (request.getSession().getAttribute("agentsSave") != null)
			{
				request.getSession().removeAttribute("agentsSave");
			}
			if (request.getSession().getAttribute("agents") != null)
			{
				request.getSession().removeAttribute("agents");
			}
			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_BILLER_LIMITS);
		} else
		{
			changeLimitForm.setLimitAmt("0.000");
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
