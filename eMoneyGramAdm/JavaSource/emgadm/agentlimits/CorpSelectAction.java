package emgadm.agentlimits;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.BillerMainOfficeManager;
import emgadm.model.Agent;
import emgadm.util.StringHelper;
import emgshared.exceptions.DataSourceException;
import emgshared.model.BillerLimit;
import emgshared.services.BillerLimitService;
import emgshared.services.ServiceFactory;

public class CorpSelectAction extends EMoneyGramAdmBaseAction {

	public ActionForward execute(ActionMapping mapping, ActionForm f,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		CorpSelectForm form = (CorpSelectForm) f;
		Collection corps = new ArrayList();
		Agent corp = null;

		// Cancel adding a new biller limit
		if (!StringHelper.isNullOrEmpty(form.getSubmitCancel())) {
			if (request.getSession().getAttribute("corps") != null) {
				request.getSession().removeAttribute("corps");
			}
			if (request.getSession().getAttribute("corp") != null) {
				request.getSession().removeAttribute("corp");
			}
			if (request.getSession().getAttribute("agents") != null) {
				request.getSession().removeAttribute("agents");
			}
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_BILLER_LIMITS);
		}

		if (request.getSession().getAttribute("corps") != null) {
			corps = (Collection) request.getSession().getAttribute("corps");
			form.setCorps(corps);
		}

		// return from agent selection screen
		if (!StringHelper.isNullOrEmpty(form.getSubmitReturn())) {
			if (request.getSession().getAttribute("agents") != null) {
				request.getSession().removeAttribute("agents");
			}
			form.setCorps(corps);
			corp = (Agent) request.getSession().getAttribute("corp");
			if (corp != null) {
				form.setCorpSelection(corp.getAgentId());
			}
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		}

		BillerMainOfficeManager bmoManager = getBillerMainOfficeManager(request);

		// user selected a corporation
		if (!StringHelper.isNullOrEmpty(form.getSubmitCorp())) {
			// get the selected corp
			Iterator iter = corps.iterator();
			while (iter.hasNext()) {
				corp = (Agent) iter.next();
				if (corp.getAgentId().equals(form.getCorpSelection())) {
					break;
				}
			}

			request.getSession().setAttribute("corp", corp);
			Collection agents = mergeWithMG(
					bmoManager.getLocations(form.getCorpSelection()), request);
			if (agents == null || agents.size() == 0) {
				errors.add(ActionErrors.GLOBAL_ERROR,
						new ActionError("error.agentselect.agent.notfound",
								form.getCorpSelection()));
				saveErrors(request, errors);
				return mapping
						.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
			}

			request.getSession().setAttribute("agents", agents);
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_SELECT_AGENT);
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitCorpFilter())) {
			corps = filter(request, form, bmoManager, errors);
			form.setCorpSelection(null);
		} else {
			corps = bmoManager.getMainOffices();
		}

		if (corps == null || corps.size() == 0) {
			errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("error.corpselect.corp.notfound", form
							.getSubmitCorpFilter()));
			saveErrors(request, errors);
		} else {
			form.setCorps(corps);
			if (StringHelper.isNullOrEmpty(form.getCorpSelection())) {
				Iterator iter = corps.iterator();
				form.setCorpSelection(((Agent) iter.next()).getAgentId());
			}
		}
		request.getSession().setAttribute("corps", corps);

		return mapping
				.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private Collection filter(HttpServletRequest request, CorpSelectForm form,
			BillerMainOfficeManager bmoManager, ActionErrors errors)
			throws DataSourceException {
		List filteredCorps = filterAgents((List) bmoManager.getMainOffices(),
				form.getCorporationName());
		if (filteredCorps.size() == 0) {
			errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("error.corpselect.corp.notfound", form
							.getCorporationName()));
			saveErrors(request, errors);
			return null;
		} else {
			return filteredCorps;
		}
	}

	private Collection mergeWithMG(Collection agents, HttpServletRequest request)
			throws DataSourceException,
			emgshared.exceptions.TooManyResultException {

		if (agents == null || agents.size() == 0) {
			return agents;
		}

		ServiceFactory sf = ServiceFactory.getInstance();
		BillerLimitService bls = sf.getBillerLimitService();
		Collection billerLimits = bls.getBillerLimits("");

		if (billerLimits == null || billerLimits.size() == 0) {
			return agents;
		}

		Map tmpMap = new HashMap();
		Iterator iter = billerLimits.iterator();

		while (iter.hasNext()) {
			BillerLimit billerLimit = (BillerLimit) iter.next();
			tmpMap.put(billerLimit.getId(), billerLimit);
		}

		Collection newAgents = new ArrayList();
		iter = agents.iterator();
		while (iter.hasNext()) {
			Agent agent = (Agent) iter.next();
			if (!tmpMap.containsKey(agent.getAgentId())) {
				newAgents.add(agent);
			}
		}
		tmpMap = null;

		return newAgents;
	}
}
