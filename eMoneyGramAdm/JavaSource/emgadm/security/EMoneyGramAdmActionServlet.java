package emgadm.security;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionServlet;

import emgadm.util.EMTEnvironmentInitializer;

public class EMoneyGramAdmActionServlet extends ActionServlet implements Servlet {
	
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        super.doGet(req, resp);
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        super.doPost(req, resp);
    }

    /**
     * @see org.apache.struts.action.ActionServlet#void ()
     */
    public void init() throws ServletException {
        super.init();

        log.debug("S26-EMoneyGramAdmActionServlet.init()...");
        EMTEnvironmentInitializer.initEnvironment(this.getServletContext());
    }
}
