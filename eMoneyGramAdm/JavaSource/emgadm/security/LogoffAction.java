package emgadm.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgshared.model.UserActivityType;

public class LogoffAction extends EMoneyGramAdmBaseAction {
    public final static String ACTION_PATH = "logoff";

    public final static String ACTION_PATH_WITH_EXTENSION = ACTION_PATH + ".do";

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        super.insertActivityLog(this.getClass(),request,null,UserActivityType.EVENT_TYPE_USERLOGOFF,null);
        request.getSession().invalidate();
        ActionMessages messages = getActionMessages(request);
        messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage(
                "messages.logoff.thanks"));
        saveMessages(request, messages);

        //  Clean up session objects for the user

        if (request.getSession().getAttribute("corps") != null) {
            request.getSession().removeAttribute("corps");
        }

        if (request.getSession().getAttribute("corp") != null) {
            request.getSession().removeAttribute("corp");
        }

        if (request.getSession().getAttribute("agents") != null) {
            request.getSession().removeAttribute("agents");
        }

        if (request.getSession().getAttribute("agent") != null) {
            request.getSession().removeAttribute("agent");
        }

        return mapping
                .findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
    }
}
