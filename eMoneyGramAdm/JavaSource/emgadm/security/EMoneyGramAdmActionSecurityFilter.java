/**
 * 
 */
package emgadm.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author wr70
 *
 */
public class EMoneyGramAdmActionSecurityFilter implements Filter {

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	//MGO-PCI changes starts here
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest httpReq = (HttpServletRequest) request;
        HttpServletResponse httpResp = (HttpServletResponse) response;
        String queryString = null;
        boolean URLvalidated = false;
        if(request instanceof HttpServletRequest){
        	queryString = ((HttpServletRequest) request).getQueryString();
        }
        
	    // The below condition is added to filter the scripting elements in request url as well adding up a condition
	    // to ensure the url contains 'moneygram.com' or non production server names
        if(queryString!=null && (queryString.contains("script") || queryString.contains("alert"))){
        	httpReq.getSession().invalidate();
        	//  Clean up session objects for the user
        	httpResp.sendRedirect(httpReq.getContextPath()+"/jsp/misc/error.jsp");
        	chain.doFilter(httpReq, httpResp);
        }
        else {
        	httpReq.setCharacterEncoding("UTF-8");
        	chain.doFilter(httpReq, httpResp);
        }

	}
//MGO-PCI changes ends
	
	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
	}

}
