package emgadm.security;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Arrays;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.StringUtilities;
import org.owasp.esapi.errors.AccessControlException;

import emgadm.actions.HeartBeatMonAction;
import emgadm.constants.EMoneyGramAdmSessionConstants;
import emgadm.dataaccessors.CommandManager;
import emgadm.model.ApplicationCommand;
import emgadm.model.UserProfile;
import emgadm.property.EMTAdmContainerProperties;
import emgadm.util.AuditLogUtils;
import emgadm.util.StringHelper;
import emgshared.dataaccessors.EMGSharedLogger;

public class EMoneyGramAdmSecurityFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp,
            FilterChain chain) throws ServletException, IOException,
            java.security.AccessControlException {
        UserProfile user = null;
        RequestDispatcher rd = null;
        HttpServletRequest hreq = (HttpServletRequest) req;
        HttpServletResponse hresp = (HttpServletResponse) resp;

        if (EMTAdmContainerProperties.isSecureProtocolRequired()) {
            if (!isSecureProtocol(req)) {
                String securePort = EMTAdmContainerProperties.getSecurePort();
                StringBuilder urlBuffer = new StringBuilder(64);
                urlBuffer.append("https://");
				urlBuffer.append(req.getServerName());
                if (!StringHelper.isNullOrEmpty(securePort)) {
                    urlBuffer.append(":");
                    urlBuffer.append(securePort);
                }
                urlBuffer.append(hreq.getRequestURI());
                try {
					ESAPI.httpUtilities().sendRedirect(hresp,urlBuffer.toString());
				} catch (AccessControlException e) {
					
				}
//				hresp.sendRedirect(StringUtilities.stripControls(urlBuffer.toString()));
                return;
            }
        }

        String command = AuditLogUtils.getCommand(hreq.getRequestURI());
        command=StringEscapeUtils.escapeJava(command);
        if (hreq.getRequestedSessionId() != null && !hreq.isRequestedSessionIdValid()) {
            if (!LoginAction.ACTION_PATH.equalsIgnoreCase(command) && !LogoffAction.ACTION_PATH.equalsIgnoreCase(command)) {
                if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isInfoEnabled()) {
                    EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Session (" + ESAPI.encoder().encodeForHTMLAttribute(hreq.getRequestedSessionId()) + ") expired");
                }
                StringBuilder path = new StringBuilder(32);
                path.append(LoginAction.ACTION_PATH_WITH_EXTENSION);
                path.append("?sessionExpired=true");
                rd = req.getRequestDispatcher(path.toString());
                rd.forward(req, resp);
                return;
            }
        }

        if (!HeartBeatMonAction.ACTION_PATH.equalsIgnoreCase(command)) {
            user = (UserProfile) hreq.getSession().getAttribute(EMoneyGramAdmSessionConstants.USER_PROFILE);

            if (user == null) {
                StringBuilder path = new StringBuilder(32);
                path.append(LoginAction.ACTION_PATH_WITH_EXTENSION);
                if (!LoginAction.ACTION_PATH.equalsIgnoreCase(command)) {
                    path.append("?sessionExpired=true");
                }
                rd = req.getRequestDispatcher(path.toString());
                rd.forward(req, resp);
                return;
            }
            try {
                if (CommandManager.contains(command)) {
                    ApplicationCommand com = CommandManager.getCommandByCommandName(command);

                    if (com.isAuthorizationRequired()) {
                        if (!user.hasPermission(command)) {
                            EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Attempting to access " + ESAPI.encoder().decodeFromURL(ESAPI.encoder().encodeForURL(hreq.getRequestURI())));
                            EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("No user role is authorized for this request");
                            rd = req.getRequestDispatcher("unAuthorizedAccess.do");
                            rd.forward(req, resp);
                            return;
                        }
                    }
                } else {
                    EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Attempting to access " + ESAPI.encoder().decodeFromURL(ESAPI.encoder().encodeForURL(hreq.getRequestURI())));
                    EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Permissions are not defined for this request");
                    rd = req.getRequestDispatcher("unAuthorizedAccess.do");
                    rd.forward(req, resp);
                }
			} catch (Exception e) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.error(e.getMessage(), e);
				rd = req.getRequestDispatcher("systemError.do");
				rd.forward(req, resp);
			}
        }

        chain.doFilter(req, resp);
    }

    private boolean isSecureProtocol(ServletRequest req) {
        return ("https".equalsIgnoreCase(req.getScheme()));
    }

    public void init(FilterConfig config) throws ServletException {
    }
}
