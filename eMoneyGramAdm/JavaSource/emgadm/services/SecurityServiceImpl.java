package emgadm.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import emgadm.dataaccessors.CommandManager;
import emgadm.dataaccessors.InvalidGuidException;
import emgadm.dataaccessors.MenuManager;
import emgadm.dataaccessors.RoleManager;
import emgadm.model.ApplicationCommand;
import emgadm.model.Program;
import emgadm.model.Role;
import emgadm.model.UserProfile;
import emgshared.exceptions.DataSourceException;

class SecurityServiceImpl implements SecurityService {
	
	private static final class InstanceHolder {
		public static final SecurityService INSTANCE = new SecurityServiceImpl();
	}

	public static SecurityService getInstance() {
		return InstanceHolder.INSTANCE;
	}

	
	private SecurityServiceImpl() {
	}
	
	public List<Program> getAllPrograms() throws DataSourceException {
		return CommandManager.getPrograms();
	}

	public List<ApplicationCommand> getAllApplicationCommands() throws DataSourceException {
		return CommandManager.getCommands();
	}

	public List<Role> getAllRoles() throws DataSourceException, InvalidGuidException {
		return RoleManager.getFreshedRoles();
	}

	public List<Program> getAuthorizedProgramsByRoleAndCommand(Role role, ApplicationCommand command) {
		return role.getProgramsWithPermission(command.getId());
	}

	public List<Program> getAuthorizedProgramsByUserAndCommand(UserProfile user,
			ApplicationCommand command) {
		Set<Program> programs = new TreeSet<Program>();
		for (Role role : user.getRoles()) {
			programs.addAll(getAuthorizedProgramsByRoleAndCommand(role, command));
		}
		return new ArrayList<Program>(programs);
	}

	public List<ApplicationCommand> getMenuOptions(UserProfile user)
			throws DataSourceException {
		List<ApplicationCommand> options = new ArrayList<ApplicationCommand>();
		HashMap<String, List<ApplicationCommand>> map = MenuManager.retrieveMenu();
		//4439
		for(Entry<String, List<ApplicationCommand>> entry : map.entrySet()){
			List<ApplicationCommand> commands = entry.getValue();
			for (ApplicationCommand command : commands) {
				if (user.hasPermission(command.getId())) {
					options.add(command);
				}
			}
		}
		return options;
	}

	public boolean hasPermission(UserProfile user, ApplicationCommand command, Program program) {
		return getAuthorizedProgramsByUserAndCommand(user, command).contains(program);
	}


	public List<Role> getRolesByProgram(Program program) throws DataSourceException, InvalidGuidException {
		List<Role> programRoles = new ArrayList<Role>();
		if (program == null) {
			return programRoles;
		}
		List<Role> allRoles = getAllRoles();
		for (Role role : allRoles) {
			//if (role.getId().indexOf(rolePrefix) != -1) {
				programRoles.add(role);
			//}
		}
		return programRoles;
	}

}
