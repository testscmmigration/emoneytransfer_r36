package emgadm.internaluseradmin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.ImageButtonBean;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.CommandManager;
import emgadm.dataaccessors.InvalidGuidException;
import emgadm.dataaccessors.RoleManager;
import emgadm.model.ApplicationCommand;
import emgadm.model.CommandFactory;
import emgadm.model.Role;
import emgadm.util.StringHelper;
import emgshared.exceptions.DataSourceException;
import emgshared.model.UserActivityType;

/**
 * @version 	1.0
 * @author
 */
public class EditRoleAction extends EMoneyGramAdmBaseAction
{
	private List available = new ArrayList();
	private List assigned = new ArrayList();
	private Map roleCmdMap1 = new HashMap();
	private Map roleCmdMap2 = new HashMap();
	private Iterator iter;

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		ActionErrors errors = new ActionErrors();
		EditRoleForm form = (EditRoleForm) f;

		if (form.getSubmitExit().isSelected()) {
			request.getSession().removeAttribute("available");
			request.getSession().removeAttribute("assigned");
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_ROLES);
		}

		if (!StringHelper.isNullOrEmpty(form.getDblClickAction())) {
			ImageButtonBean ibb = new ImageButtonBean();
			ibb.setX("1");
			if ("Auth".equals(form.getDblClickAction())) {
				form.setSubmitAuth(ibb);
			} else {
				form.setSubmitUnauth(ibb);
			}
			form.setDblClickAction(null);
		}
		available = new ArrayList();
		assigned = new ArrayList();
		roleCmdMap1 = new HashMap();
		roleCmdMap2 = new HashMap();

		if (form.getSubmitAuth().isSelected())
		{
			if (form.getUnauthCmdId() != null && form.getUnauthCmdId().length != 0)
			{
				listToMap(request);
				for (int i = 0; i < form.getUnauthCmdId().length; i++)
				{
					if (roleCmdMap1.containsKey(form.getUnauthCmdId(i)))
					{
						roleCmdMap2.put(form.getUnauthCmdId(i),	roleCmdMap1.get(form.getUnauthCmdId(i)));
						roleCmdMap1.remove(form.getUnauthCmdId(i));
					}
				}
				mapToList(request);
			}
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		}

		if (form.getSubmitUnauth().isSelected())
		{
			if (form.getAuthCmdId() != null && form.getAuthCmdId().length != 0)
			{
				listToMap(request);
				for (int i = 0; i < form.getAuthCmdId().length; i++)
				{
					if (roleCmdMap2.containsKey(form.getAuthCmdId(i)))
					{
						roleCmdMap1.put(form.getAuthCmdId(i), roleCmdMap2.get(form.getAuthCmdId(i)));
						roleCmdMap2.remove(form.getAuthCmdId(i));
					}
				}
				mapToList(request);
			}
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		}

		if (form.getSubmitSave().isSelected())
		{
			//  check if the role name is a duplicate name
//			if (form.getSaveAction().equals("add"))
//			{
//				iter = RoleManager.getFreshedRoles().iterator();
//				while (iter.hasNext())
//				{
//					RoleImpl tmpRole = (RoleImpl) iter.next();
//					if (form.getRoleName().equals(tmpRole.getName()))
//					{
//						String[] parms = { "Role name", form.getRoleName()};
//						errors.add(
//							ActionErrors.GLOBAL_ERROR,
//							new ActionError("msg.exists", parms));
//						saveErrors(request, errors);
//						break;
//					}
//				}
//			}

			//  authorized command list must not be empty
			List tmpList = (List) request.getSession().getAttribute("assigned");
			if (tmpList.size() == 0)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("msg.must.select.one", "command"));
				saveErrors(request, errors);
			}

			if (errors.size() > 0)
			{
				return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
			}

			Role role = null;
			String logType = null;
			logType = UserActivityType.EVENT_TYPE_ROLEMAINTENANCEMODIFY;
		    logType = UserActivityType.EVENT_TYPE_ROLEMAINTENANCEMODIFY;
			role = getRole(form.getRoleId());

			iter = ((List) request.getSession().getAttribute("assigned")).iterator();
			List applicationCommands = new ArrayList();
			StringBuilder logDetail = new StringBuilder(1024);
			while (iter.hasNext())
			{
				ApplicationCommand ac = CommandFactory.createCommand(((CommandBean) iter.next()).getCmdId());
				applicationCommands.add(ac);
			}
			iter = tmpList.iterator();
			logDetail.append("<commands>");
			while (iter.hasNext())
			{
			    CommandBean cb = new CommandBean();
			    cb = (CommandBean) iter.next();
				logDetail.append(cb.getCmdDesc().substring(cb.getCmdDesc().indexOf(')') + 1, cb.getCmdDesc().length()) + ", ");	
				if (logDetail.length() > 1800)
				{
				    logDetail.append(" more not listed ...");
				    break;
				}
			}
			logDetail.append("</commands>");
			role.setApplicationCommands(applicationCommands);
			RoleManager.updateRolePermissions(role.getId(), applicationCommands);
			RoleManager.getFreshedRoles();
			form.setSaveAction("edit");
			String[] parms = { "Role", role.getName()};
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("msg.updated", parms));
			
			try {
		    	super.insertActivityLog(this.getClass(), request,logDetail.toString(), logType, role.getId());    
			} catch (Exception e) { }

			saveErrors(request, errors);
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		}

		// builds list of available and assigned commands for a specific role
		if (form.getAction() != null && form.getAction().equals("edit"))
		{
			form.setSaveAction("edit");
			form.setAction("edit");

			Role role = getRole(form.getRoleId());
			form.setRoleName(role.getName());
			form.setRoleDesc(role.getDescription());

			Map roleCmdMap = new HashMap();
			iter = role.getApplicationCommands().iterator();
			while (iter.hasNext())
			{
				ApplicationCommand ac = (ApplicationCommand) iter.next();
				roleCmdMap.put(ac.getId(), ac);
				assigned.add(createCommandBean(ac));
			}

			iter = CommandManager.getCommands().iterator();
			while (iter.hasNext())
			{
				ApplicationCommand ac = (ApplicationCommand) iter.next();
				if (ac.isAuthorizationRequired()
					&& roleCmdMap.get(ac.getId()) == null)
				{
					available.add(createCommandBean(ac));
				}
			}

			Collections.sort(available);
			Collections.sort(assigned);
			request.getSession().setAttribute("available", available);
			request.getSession().setAttribute("assigned", assigned);
		}

		/*
		if (form.getAction() != null && form.getAction().equals("add"))
		{
			form.setSaveAction("add");
			form.setAction("edit");

			iter = CommandManager.getCommands().iterator();
			while (iter.hasNext())
			{
				ApplicationCommand ac = (ApplicationCommand) iter.next();
				if (ac.isAuthorizationRequired())
				{
					available.add(createCommandBean(ac));
				}
			}
			Collections.sort(available);
			request.getSession().setAttribute("available", available);
			request.getSession().setAttribute("assigned", assigned);
		}
*/
		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private CommandBean createCommandBean(ApplicationCommand ac)
	{
		CommandBean cb = new CommandBean();
		cb.setCmdId(ac.getId());
		String desc = "";
		if (!StringHelper.isNullOrEmpty(ac.getMenuGroupName()))
		{
			desc = "(" + ac.getMenuGroupName() + ") ";
		}
		if (ac.getMenuGroupName() != null
			&& ac.getMenuGroupName().equals("Button"))
		{
			cb.setCmdDesc(desc + ac.getDisplayName());
		} else
		{
			cb.setCmdDesc(desc + ac.getDescription());
		}
		return cb;
	}

	private void listToMap(HttpServletRequest request)
	{
		if (request.getSession().getAttribute("available") != null)
		{
			available = (List) request.getSession().getAttribute("available");
			iter = available.iterator();
			while (iter.hasNext())
			{
				CommandBean cb = (CommandBean) iter.next();
				roleCmdMap1.put(cb.getCmdId(), cb);
			}
		}

		if (request.getSession().getAttribute("assigned") != null)
		{
			assigned = (List) request.getSession().getAttribute("assigned");
			iter = assigned.iterator();
			while (iter.hasNext())
			{
				CommandBean cb = (CommandBean) iter.next();
				roleCmdMap2.put(cb.getCmdId(), cb);
			}
		}
	}

	private void mapToList(HttpServletRequest request)
	{
		iter = roleCmdMap1.values().iterator();
		available = new ArrayList();
		while (iter.hasNext())
		{
			available.add(iter.next());
		}

		iter = roleCmdMap2.values().iterator();
		assigned = new ArrayList();
		while (iter.hasNext())
		{
			assigned.add(iter.next());
		}

		Collections.sort(available);
		Collections.sort(assigned);

		request.getSession().setAttribute("available", available);
		request.getSession().setAttribute("assigned", assigned);
	}

	private Role getRole(String guid)
		throws DataSourceException, InvalidGuidException
	{

		Role role = null;
		Iterator it = ((List) RoleManager.getFreshedRoles()).iterator();
		while (it.hasNext())
		{
			role = (Role) it.next();
			if (role.getId().equals(guid))
			{
				break;
			}
		}
		return role;
	}

	public final class CommandBean implements Comparable
	{
		private String cmdId;
		private String cmdDesc;

		public String getCmdId()
		{
			return cmdId;
		}

		public void setCmdId(String string)
		{
			cmdId = string;
		}

		public String getCmdDesc()
		{
			return cmdDesc;
		}

		public void setCmdDesc(String string)
		{
			cmdDesc = string;
		}

		public int compareTo(Object obj)
		{
			CommandBean cb = (CommandBean) obj;
			return this.cmdDesc.compareTo(cb.cmdDesc);
		}
	}
}
