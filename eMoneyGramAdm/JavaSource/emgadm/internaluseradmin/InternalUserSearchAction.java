package emgadm.internaluseradmin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.RoleManager;
import emgadm.model.CommandAuth;
import emgadm.model.UserProfile;
import emgadm.model.UserQueryCriterion;

/**
 * @version 	1.0
 * @author
 */
public class InternalUserSearchAction
		extends EMoneyGramAdmBaseAction { 

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
				HttpServletResponse response)
			throws Exception {

		CommandAuth ca = new CommandAuth();
		UserProfile up = getUserProfile(request);

		if (up.hasPermission(	EMoneyGramAdmForwardConstants.LOCAL_FORWARD_EDIT_ADMIN_USER_PROFILE)) {
			ca.setEditAdminUserProfile(true);
		} else {
			ca.setEditAdminUserProfile(false);
		}
		
		if (up.hasPermission(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_RESET_ADMIN_USER_PASSWORD)) {
			ca.setResetAdminUserPassword(true);
		} else {
			ca.setResetAdminUserPassword(false);
		}
		
		request.getSession().setAttribute("auth", ca);
		
		ActionErrors errors = new ActionErrors();
		// return value
		InternalUserSearchForm internalUserSearch = (InternalUserSearchForm) form;
		internalUserSearch.setInternalRoles(RoleManager.getInternalRoles());
		if (internalUserSearch.getSearch() != null) {
			String userID = internalUserSearch.getUserID();
			String firstName = internalUserSearch.getFirstName();
			String lastName = internalUserSearch.getLastName();

			UserQueryCriterion criteria = new UserQueryCriterion(userID, firstName, lastName, internalUserSearch.getWebStatus(), internalUserSearch.getRoleID(), internalUserSearch.isInternal());
			List<UserProfile> users = getUserProfileManager(request).findHollowUsers(criteria);
			if (users.size() == 0) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.user.search.notfound")); //$NON-NLS-1$
				saveErrors(request, errors);
			}
			internalUserSearch.setUsers(users);
		}
		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
