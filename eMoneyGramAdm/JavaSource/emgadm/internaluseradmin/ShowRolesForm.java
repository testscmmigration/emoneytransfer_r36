package emgadm.internaluseradmin;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class ShowRolesForm extends EMoneyGramAdmBaseValidatorForm
{
	private String delete;
	private String roleName;

	public String getDelete()
	{
		return delete;
	}

	public void setDelete(String string)
	{
		delete = string;
	}

	public String getRoleName()
	{
		return roleName;
	}

	public void setRoleName(String string)
	{
		roleName = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);

		delete = "";
		roleName = "";
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = super.validate(mapping, request);
		return errors;
	}
}
