package emgadm.internaluseradmin;

import java.util.Comparator;

import emgadm.model.CommandAndRoleMatrixBean;
import emgadm.util.StringHelper;

/**
 * Sorts first by menu group name, then by the descriptive text of the command.
 */
public class MenuGroupAndRoleDescriptionComparator implements Comparator<CommandAndRoleMatrixBean> {

	public int compare(CommandAndRoleMatrixBean c1, CommandAndRoleMatrixBean c2) {
		int comparison = 0;
		if (c1 == null) {
			comparison = (c2 == null ? 0 : 1);
		} else if (c2 == null) {
			comparison = -1;
		} else {
			comparison = StringHelper.compareToIgnoreCase(c1.getMenuGroupName(), c2.getMenuGroupName());
			if (comparison == 0) {
				comparison = StringHelper.compareToIgnoreCase(c1.getDisplayName(), c2.getDisplayName());
			}
		}
		return comparison;
	}


}
