package emgadm.internaluseradmin;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for a Struts application.
 * Users may access 7 fields on this form:
 * <ul>
 * <li>roleID - [your comment here]
 * <li>webStatus - [your comment here]
 * <li>lastName - [your comment here]
 * <li>userID - [your comment here]
 * <li>search - [your comment here]
 * <li>firstName - [your comment here]
 * <li>add - [your comment here]
 * </ul>
 * @version 	1.0
 * @author
 */
public class InternalUserSearchForm extends EMoneyGramAdmBaseUserAdminForm {

	private String roleID = null;
	private String userID = null;
	private String lastName = null;
	private String webStatus = null;
	private String search = null;
	private String firstName = null;
	private Collection internalRoles = new ArrayList();
	private Collection users = new ArrayList();
	private boolean isInternal = true;

	/**
	 * Get roleID
	 * @return String[]
	 */
	public String getRoleID() {
		return roleID;
	}

	/**
	 * Set roleID
	 * @param <code>String[]</code>
	 */
	public void setRoleID(String r) {
		this.roleID = r;
	}

	/**
	 * Get userID
	 * @return String
	 */
	public String getUserID() {
		if (userID != null) {
			userID = userID.replace('*', ' ').trim();
		}
		return userID;
	}

	/**
	 * Set userID
	 * @param <code>String</code>
	 */
	public void setUserID(String u) {
		this.userID = u;
	}

	/**
	 * Get lastName
	 * @return String
	 */
	public String getLastName() {
		if (lastName != null) {
			lastName = lastName.replace('*', ' ').trim();
		}
		return lastName;
	}

	/**
	 * Set lastName
	 * @param <code>String</code>
	 */
	public void setLastName(String l) {
		this.lastName = l;
	}

	/**
	 * Get webStatus
	 * @return String[]
	 */
	public String getWebStatus() {
		return webStatus;
	}

	/**
	 * Set webStatus
	 * @param <code>String[]</code>
	 */
	public void setWebStatus(String w) {
		this.webStatus = w;
	}

	/**
	 * Get search
	 * @return String
	 */
	public String getSearch() {
		return search;
	}

	/**
	 * Set search
	 * @param <code>String</code>
	 */
	public void setSearch(String s) {
		this.search = s;
	}

	/**
	 * Get firstName
	 * @return String
	 */
	public String getFirstName() {
		if (firstName != null) {
			firstName = firstName.replace('*', ' ').trim();
		}
		return firstName;
	}

	/**
	 * Set firstName
	 * @param <code>String</code>
	 */
	public void setFirstName(String f) {
		this.firstName = f;
	}

	public Collection getInternalRoles() {
		return internalRoles;
	}

	public void setInternalRoles(Collection collection) {
		internalRoles = collection;
	}

	public Collection getUsers() {
		return users;
	}

	public void setUsers(Collection collection) {
		users = collection;
	}
	
	public int getUserCount() {
		return getUsers().size();
	}

	public boolean isInternal() {
		return isInternal;
	}

	public void setInternal(boolean b) {
		isInternal = b;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		// Reset values are provided as samples only. Change as appropriate.

		roleID = null;
		userID = null;
		lastName = null;
		webStatus = null;
		search = null;
		firstName = null;

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = super.validate(mapping, request);
		return errors;

	}
}
