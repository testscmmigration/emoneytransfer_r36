package emgadm.internaluseradmin;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

public class ShowUserForm extends EMoneyGramAdmBaseUserAdminForm
{

	public void reset(ActionMapping arg0, HttpServletRequest arg1)
	{
		super.reset(arg0, arg1);
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		if (getAdd() != null || getEdit() != null)
		{
			return super.validate(mapping, request);
		}

		return errors;
	}

}
