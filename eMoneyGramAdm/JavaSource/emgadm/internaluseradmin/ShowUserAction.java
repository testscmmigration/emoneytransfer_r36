package emgadm.internaluseradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.UserProfileManager;
import emgadm.model.UserProfile;
import emgadm.services.RoleValidationService;
import emgadm.services.RoleValidationServiceImpl;

/**
 * @version 1.0
 * @author
 */
public class ShowUserAction extends EMoneyGramAdmBaseAction {

	public ActionForward execute(ActionMapping mapping, ActionForm aform,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ShowUserForm form = (ShowUserForm) aform;
		UserProfile up = getUserProfile(request);
		/*
		 * MBO-2848: Only users who have view users privileges can view user
		 * profile
		 */
		if (up.hasPermission(EMoneyGramAdmApplicationConstants.USER_SEARCH_PRIVILEGE)) {
			UserProfileManager userManager = getUserProfileManager(request);
			UserProfile user = null;
			try {
				user = userManager.getUser(form.getUserId());
				form.setUser(user);
			} catch (Exception e) {
				request.setAttribute("invalidUser", "y");
			}
		} else {
			request.setAttribute("unauthorizedUser", "y");
		}
		return mapping
				.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
