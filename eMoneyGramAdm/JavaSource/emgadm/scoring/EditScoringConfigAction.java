package emgadm.scoring;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.model.CodeDescription;
import emgshared.model.ScoreCategory;
import emgshared.model.ScoreConfiguration;
import emgshared.model.ScoreValue;
import emgshared.model.UserActivityType;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ScoringService;
import emgshared.services.ServiceFactory;

public class EditScoringConfigAction extends EMoneyGramAdmBaseAction {

	private static final String cat1 = ScoreCategory.cat1;
	private static final String cat2 = ScoreCategory.cat2;
	private static final String cat3 = ScoreCategory.cat3;
	private static final String cat4 = ScoreCategory.cat4;
	private static final String cat5 = ScoreCategory.cat5;
	private static final String cat6 = ScoreCategory.cat6;
	private static final String cat7 = ScoreCategory.cat7;
	private static final String cat8 = ScoreCategory.cat8;
	private static final String cat9 = ScoreCategory.cat9;
	private static final String cat10 = ScoreCategory.cat10;
	private static final String cat11 = ScoreCategory.cat11;
	private static final String cat12 = ScoreCategory.cat12;
	private static final String cat13 = ScoreCategory.cat13;
	private static final String cat15 = ScoreCategory.cat15;
	private static final String[] cats = ScoreCategory.catList;

	public ActionForward execute(ActionMapping mapping, ActionForm strutsform, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		EditScoringConfigForm form = (EditScoringConfigForm) strutsform;
		UserProfile up = getUserProfile(request);
		ServiceFactory sf = ServiceFactory.getInstance();
		ScoringService ss = sf.getScoringService();
		ConsumerProfileService cps = sf.getConsumerProfileService();
		boolean invalid = false;
		boolean globalMsg = false;
		int valueCount = 0;

		// Button CANCEL Clicked
		if (!StringHelper.isNullOrEmpty(form.getSubmitCancel())) {
			request.getSession().removeAttribute("scoreCategory1");
			request.getSession().removeAttribute("scoreCategory2");
			request.getSession().removeAttribute("scoreCategory3");
			request.getSession().removeAttribute("scoreCategory4");
			request.getSession().removeAttribute("scoreCategory5");
			request.getSession().removeAttribute("scoreCategory6");
			request.getSession().removeAttribute("scoreCategory7");
			request.getSession().removeAttribute("scoreCategory8");
			request.getSession().removeAttribute("scoreCategory9");
			request.getSession().removeAttribute("scoreCategory10");
			request.getSession().removeAttribute("scoreCategory11");
			request.getSession().removeAttribute("scoreCategory12");
			request.getSession().removeAttribute("scoreCategory13");
			request.getSession().removeAttribute("scoreCategory14");
			request.getSession().removeAttribute("scoreCategory15");
			request.getSession().removeAttribute("chosenConfig");
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SAVE_SCORING_CONFIG);
		}

		// Button CLONE Clicked for the view Form
		if (request.getAttribute("action") != null) {
			if (((String) request.getAttribute("action")).equalsIgnoreCase("clone")) {
				Map<String, String> transferMap = (Map<String, String>) request
				.getSession().getAttribute("configId");
				String confid = null;
				if (transferMap != null) {
					if (transferMap.containsKey("configId"))
						confid = transferMap.get("configId");
				}
				Collection chosenConfig = ss.getScoreConfigurations(
						up.getUID(), confid, null);
				// There should be only one. Change the code to get only one
				// instead of iterating
				for (Iterator it = chosenConfig.iterator(); it.hasNext();) {
					ScoreConfiguration scoreConfig = (ScoreConfiguration) it.next();
					scoreConfig.setConfigurationId(0);
					StoreInSession(scoreConfig, false, form, request);
					break;
				}
				request.getSession().setAttribute("addEditFlag", "add");
				request.getSession().removeAttribute("configId");
			}
		}

		// Button SAVE Clicked
		if (!StringHelper.isNullOrEmpty(form.getSubmitSave())) {
			invalid = false;
			ScoreConfiguration scoreConfiguration = (ScoreConfiguration) request.getSession().getAttribute("chosenConfig");
			StoreInSession(scoreConfiguration, true, form, request);
			// make sure there are values for each active category
			// added on Dec. 8, 2005 - begin
			for (int i = 0; i < cats.length; i++) {
				ScoreCategory tmpSC = (ScoreCategory) scoreConfiguration.getScoreCategories().get(cats[i]);
				if (tmpSC == null) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.scoring.category.not.define", cats[i]));
					saveErrors(request, errors);
					invalid = true;
					globalMsg = true;
					break;
				} else if (tmpSC.isIncluded() == true && (tmpSC.getScoreValues() == null || tmpSC.getScoreValues().size() == 0)) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.active.scoring.category.no.value", cats[i]));
					saveErrors(request, errors);
					invalid = true;
					globalMsg = true;
					break;
				}
			}
			// added on Dec. 8, 2005 - end
			if (!invalid) {
				ss.setScoreConfiguration(up.getUID(), scoreConfiguration);
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("msg.scoring.configuration.updated"));
				saveErrors(request, errors);
				request.getSession().removeAttribute("chosenConfig");
				try {
					String logType = UserActivityType.EVENT_TYPE_TRANSACTIONSCORINGEDIT;
					if (request.getSession().getAttribute("addEditFlag") != null) {
						if (String.valueOf(request.getSession().getAttribute("addEditFlag")).equalsIgnoreCase("ADD"))
							logType = UserActivityType.EVENT_TYPE_TRANSACTIONSCORINGADD;
					}
					request.getSession().removeAttribute("addEditFlag");
					super.insertActivityLog(this.getClass(), request, null, logType, String
							.valueOf(scoreConfiguration.getConfigurationId()));
				} catch (Exception e) {
				}

				return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SAVE_SCORING_CONFIG);
			}
		}

		// Get the configId from the forw where edit was clicked
		if (request.getAttribute("configId") != null) {
			Collection chosenConfig = ss.getScoreConfigurations(up.getUID(), (String) request.getAttribute("configId"), null);
			for (Iterator it = chosenConfig.iterator(); it.hasNext();) {
				ScoreConfiguration scoreConfiguration = (ScoreConfiguration) it.next();
				StoreInSession(scoreConfiguration, false, form, request);
				break;
			}
		}

		ScoreConfiguration scoreConfiguration = (ScoreConfiguration) request.getSession().getAttribute("chosenConfig");

		// Set the form with the chosen TranType
		if (request.getSession().getAttribute("currentTranType") != null) {
			
		}

		// Boolean List
		ArrayList<CodeDescription> bList = new ArrayList<CodeDescription>();
		bList.add(new CodeDescription("True", "True"));
		bList.add(new CodeDescription("False", "False"));
		Collection<CodeDescription> booleanList = bList;
		request.getSession().setAttribute("booleanList", booleanList);

		// Consumer profile status
		if (request.getSession().getAttribute("statusList") == null) {
			request.getSession().setAttribute("statusList", cps.getConsumerSubStatuses(true));
		}

		// Deleting a ScoreValue of a Score Category
		if (form.getHiddenEditAction() != null && form.getHiddenEditAction().equals("delete")) {
			String scoreCategoryName = form.getHiddenCategory();
			int scoreValueIndex = Integer.parseInt(form.getHiddenValue());
			ScoreCategory selectCategory = (ScoreCategory) scoreConfiguration.getScoreCategories().get(scoreCategoryName);
			selectCategory.removeScoreValue(scoreValueIndex);
			StoreInSession(scoreConfiguration, true, form, request);
			form.setHiddenEditAction(null);
			request.setAttribute("jumpTo", getAnchorName(scoreCategoryName));
		}

		// Add a ScoreValue of a Score Category "Pswd reset 24 Hrs"
		if (!StringHelper.isNullOrEmpty(form.getSubmitAdd1())) {
			request.setAttribute("jumpTo", "cat1");
			invalid = false;
			valueCount = 0;
			ScoreCategory selectCategory = (ScoreCategory) scoreConfiguration.getScoreCategories().get(form.getHiddenCategoryName1());
			for (Iterator it = selectCategory.getScoreValues().iterator(); it.hasNext();) {
				ScoreValue scoreValue = (ScoreValue) it.next();
				if (scoreValue.getValue().equalsIgnoreCase(form.getValue1())) {
					errors.add("Category1Error", new ActionError("error.scoring.duplicate.value"));
					invalid = true;
					saveErrors(request, errors);
					break;
				}
				++valueCount;
			}

			if (!invalid) {
				selectCategory.addScoreValue(valueCount, new ScoreValue(form.getValue1(), Integer.parseInt(form.getPointsText1())));
				StoreInSession(scoreConfiguration, true, form, request);
				form.setValue1("");
				form.setPointsText1("");
			}

		}

		// Add a ScoreValue of a Score Category "Profile Status"
		if (!StringHelper.isNullOrEmpty(form.getSubmitAdd2())) {
			request.setAttribute("jumpTo", "cat2");
			invalid = false;
			valueCount = 0;

			ScoreCategory selectCategory = (ScoreCategory) scoreConfiguration.getScoreCategories().get(form.getHiddenCategoryName2());
			for (Iterator it = selectCategory.getScoreValues().iterator(); it.hasNext();) {
				ScoreValue scoreValue = (ScoreValue) it.next();
				if (scoreValue.getValue().equalsIgnoreCase(form.getValue2())) {
					errors.add("Category2Error", new ActionError("error.scoring.duplicate.value"));
					invalid = true;
					saveErrors(request, errors);
					break;
				}
				++valueCount;
			}

			if (!invalid) {
				selectCategory.addScoreValue(valueCount, new ScoreValue(form.getValue2(), Integer.parseInt(form.getPointsText2())));
				StoreInSession(scoreConfiguration, true, form, request);
				form.setValue2("");
				form.setPointsText2("");
			}
		}

		// Add a ScoreValue of a Score Category "Chargeback Count"
		if (!StringHelper.isNullOrEmpty(form.getSubmitAdd3())) {
			request.setAttribute("jumpTo", "cat3");
			invalid = false;
			valueCount = 0;

			ScoreCategory selectCategory = (ScoreCategory) scoreConfiguration.getScoreCategories().get(form.getHiddenCategoryName3());
			for (Iterator it = selectCategory.getScoreValues().iterator(); it.hasNext();) {
				ScoreValue scoreValue = (ScoreValue) it.next();
				if (Integer.parseInt(scoreValue.getValue()) == Integer.parseInt(form.getValue3())) {
					errors.add("Category3Error", new ActionError("error.scoring.duplicate.value"));
					invalid = true;
					saveErrors(request, errors);
					break;
				}
				++valueCount;
			}

			if (!invalid) {
				selectCategory.addScoreValue(valueCount, new ScoreValue(form.getValue3(), Integer.parseInt(form.getPointsText3())));
				StoreInSession(scoreConfiguration, true, form, request);
				form.setValue3("");
				form.setPointsText3("");
			}
		}

		// Add a ScoreValue of a Score Category "Days since Last Transaction"
		if (!StringHelper.isNullOrEmpty(form.getSubmitAdd4())) {
			request.setAttribute("jumpTo", "cat4");
			invalid = false;
			valueCount = 0;
			ScoreCategory selectCategory = (ScoreCategory) scoreConfiguration.getScoreCategories().get(form.getHiddenCategoryName4());
			int min = Integer.parseInt(form.getMinimumValueText4());
			int max = Integer.parseInt(form.getMaximumValueText4());
			for (Iterator it = selectCategory.getScoreValues().iterator(); it.hasNext();) {
				ScoreValue scoreValue = (ScoreValue) it.next();
				if ((min >= scoreValue.getMinimumValue() && min <= scoreValue.getMaximumValue())
						|| (max >= scoreValue.getMinimumValue() && max <= scoreValue.getMaximumValue())) {
					errors.add("Category4Error", new ActionError("error.scoring.duplicate.value"));
					invalid = true;
					saveErrors(request, errors);
					break;
				}
				++valueCount;
			}

			if (!invalid) {
				selectCategory.addScoreValue(valueCount, new ScoreValue(min, max, Integer.parseInt(form.getPointsText4())));
				StoreInSession(scoreConfiguration, true, form, request);
				form.setMinimumValueText4("");
				form.setMaximumValueText4("");
				form.setPointsText4("");
			}
		}

		// Add a ScoreValue of a Score Category "Email Address in Blocked List"
		if (!StringHelper.isNullOrEmpty(form.getSubmitAdd5())) {
			request.setAttribute("jumpTo", "cat5");
			invalid = false;
			valueCount = 0;

			ScoreCategory selectCategory = (ScoreCategory) scoreConfiguration.getScoreCategories().get(form.getHiddenCategoryName5());
			for (Iterator it = selectCategory.getScoreValues().iterator(); it.hasNext();) {
				ScoreValue scoreValue = (ScoreValue) it.next();
				if (scoreValue.getValue().equalsIgnoreCase(form.getValue5())) {
					errors.add("Category5Error", new ActionError("error.scoring.duplicate.value"));
					invalid = true;
					saveErrors(request, errors);
					break;
				}
				++valueCount;
			}

			if (!invalid) {
				selectCategory.addScoreValue(valueCount, new ScoreValue(form.getValue5(), Integer.parseInt(form.getPointsText5())));
				StoreInSession(scoreConfiguration, true, form, request);
				form.setValue5("");
				form.setPointsText5("");
			}

		}

		// Add a ScoreValue of a Score Category "Country Threshold Exceeded"
		if (!StringHelper.isNullOrEmpty(form.getSubmitAdd6())) {
			request.setAttribute("jumpTo", "cat6");
			invalid = false;
			valueCount = 0;

			ScoreCategory selectCategory = (ScoreCategory) scoreConfiguration.getScoreCategories().get(form.getHiddenCategoryName6());
			for (Iterator it = selectCategory.getScoreValues().iterator(); it.hasNext();) {
				ScoreValue scoreValue = (ScoreValue) it.next();
				if (scoreValue.getValue().equalsIgnoreCase(form.getValue6())) {
					errors.add("Category6Error", new ActionError("error.scoring.duplicate.value"));
					invalid = true;
					saveErrors(request, errors);
					break;
				}
				++valueCount;
			}

			if (!invalid) {
				selectCategory.addScoreValue(valueCount, new ScoreValue(form.getValue6(), Integer.parseInt(form.getPointsText6())));
				StoreInSession(scoreConfiguration, true, form, request);
				form.setValue6("");
				form.setPointsText6("");
			}

		}

		// Add a ScoreValue of a Score Category
		// "Rcv state matches profile state"
		if (!StringHelper.isNullOrEmpty(form.getSubmitAdd7())) {
			request.setAttribute("jumpTo", "cat7");
			invalid = false;
			valueCount = 0;

			ScoreCategory selectCategory = (ScoreCategory) scoreConfiguration.getScoreCategories().get(form.getHiddenCategoryName7());
			for (Iterator it = selectCategory.getScoreValues().iterator(); it.hasNext();) {
				ScoreValue scoreValue = (ScoreValue) it.next();
				if (scoreValue.getValue().equalsIgnoreCase(form.getValue7())) {
					errors.add("Category7Error", new ActionError("error.scoring.duplicate.value"));
					invalid = true;
					saveErrors(request, errors);
					break;
				}
				++valueCount;
			}

			if (!invalid) {
				selectCategory.addScoreValue(valueCount, new ScoreValue(form.getValue7(), Integer.parseInt(form.getPointsText7())));
				StoreInSession(scoreConfiguration, true, form, request);
				form.setValue7("");
				form.setPointsText7("");
			}
		}

		// Add a ScoreValue of a Score Category "Age Threshold"
		if (!StringHelper.isNullOrEmpty(form.getSubmitAdd8())) {
			request.setAttribute("jumpTo", "cat8");
			invalid = false;
			valueCount = 0;
			ScoreCategory selectCategory = (ScoreCategory) scoreConfiguration.getScoreCategories().get(form.getHiddenCategoryName8());
			for (Iterator it = selectCategory.getScoreValues().iterator(); it.hasNext();) {
				ScoreValue scoreValue = (ScoreValue) it.next();
				if (scoreValue.getValue().equalsIgnoreCase(form.getValue8())) {
					errors.add("Category8Error", new ActionError("error.scoring.duplicate.value"));
					invalid = true;
					saveErrors(request, errors);
					break;
				}
				++valueCount;
			}

			if (!invalid) {
				selectCategory.addScoreValue(valueCount, new ScoreValue(form.getValue8(), Integer.parseInt(form.getPointsText8())));
				StoreInSession(scoreConfiguration, true, form, request);
				form.setValue8("");
				form.setPointsText8("");
			}
		}

		// Add a ScoreValue of a Score Category "Zip To State"
		if (!StringHelper.isNullOrEmpty(form.getSubmitAdd9())) {
			request.setAttribute("jumpTo", "cat9");
			invalid = false;
			valueCount = 0;

			ScoreCategory selectCategory = (ScoreCategory) scoreConfiguration.getScoreCategories().get(form.getHiddenCategoryName9());
			for (Iterator it = selectCategory.getScoreValues().iterator(); it.hasNext();) {
				ScoreValue scoreValue = (ScoreValue) it.next();
				if (scoreValue.getValue().equalsIgnoreCase(form.getValue9())) {
					errors.add("Category9Error", new ActionError("error.scoring.duplicate.value"));
					invalid = true;
					saveErrors(request, errors);
					break;
				}
				++valueCount;
			}

			if (!invalid) {
				selectCategory.addScoreValue(valueCount, new ScoreValue(form.getValue9(), Integer.parseInt(form.getPointsText9())));
				StoreInSession(scoreConfiguration, true, form, request);
				form.setValue9("");
				form.setPointsText9("");
			}

		}

		// Add a ScoreValue of a Score Category "Area To State"
		if (!StringHelper.isNullOrEmpty(form.getSubmitAdd10())) {
			request.setAttribute("jumpTo", "cat10");
			invalid = false;
			valueCount = 0;

			ScoreCategory selectCategory = (ScoreCategory) scoreConfiguration.getScoreCategories().get(form.getHiddenCategoryName10());
			for (Iterator it = selectCategory.getScoreValues().iterator(); it.hasNext();) {
				ScoreValue scoreValue = (ScoreValue) it.next();
				if (scoreValue.getValue().equalsIgnoreCase(form.getValue10())) {
					errors.add("Category10Error", new ActionError("error.scoring.duplicate.value"));
					invalid = true;
					saveErrors(request, errors);
					break;
				}
				++valueCount;
			}

			if (!invalid) {
				selectCategory.addScoreValue(valueCount, new ScoreValue(form.getValue10(), Integer.parseInt(form.getPointsText10())));
				StoreInSession(scoreConfiguration, true, form, request);
				form.setValue10("");
				form.setPointsText10("");
			}
		}

		// Add a ScoreValue of a Score Category "Address In Negative Filter"
		if (!StringHelper.isNullOrEmpty(form.getSubmitAdd11())) {
			request.setAttribute("jumpTo", "cat11");
			invalid = false;
			valueCount = 0;

			ScoreCategory selectCategory = (ScoreCategory) scoreConfiguration.getScoreCategories().get(form.getHiddenCategoryName11());
			for (Iterator it = selectCategory.getScoreValues().iterator(); it.hasNext();) {
				ScoreValue scoreValue = (ScoreValue) it.next();
				if (scoreValue.getValue().equalsIgnoreCase(form.getValue11())) {
					errors.add("Category11Error", new ActionError("error.scoring.duplicate.value"));
					invalid = true;
					saveErrors(request, errors);
					break;
				}
				++valueCount;
			}

			if (!invalid) {
				selectCategory.addScoreValue(valueCount, new ScoreValue(form.getValue11(), Integer.parseInt(form.getPointsText11())));
				StoreInSession(scoreConfiguration, true, form, request);
				form.setValue11("");
				form.setPointsText11("");
			}
		}

		// Add a ScoreValue of a Score Category "Key Words in Email"
		if (!StringHelper.isNullOrEmpty(form.getSubmitAdd12())) {
			request.setAttribute("jumpTo", "cat12");
			invalid = false;
			valueCount = 0;

			ScoreCategory selectCategory = (ScoreCategory) scoreConfiguration.getScoreCategories().get(form.getHiddenCategoryName12());
			for (Iterator it = selectCategory.getScoreValues().iterator(); it.hasNext();) {
				ScoreValue scoreValue = (ScoreValue) it.next();
				if (scoreValue.getValue().equalsIgnoreCase(form.getValue12())) {
					errors.add("Category12Error", new ActionError("error.scoring.duplicate.value"));
					invalid = true;
					saveErrors(request, errors);
					break;
				}
				++valueCount;
			}

			if (!invalid) {
				selectCategory.addScoreValue(valueCount, new ScoreValue(form.getValue12(), Integer.parseInt(form.getPointsText12())));
				StoreInSession(scoreConfiguration, true, form, request);
				form.setValue12("");
				form.setPointsText12("");
			}
		}

		// Add a ScoreValue of a Score Category "Transaction Monitoring Score"
		if (!StringHelper.isNullOrEmpty(form.getSubmitAdd13())) {
			request.setAttribute("jumpTo", "cat13");
			invalid = false;
			valueCount = 0;
			ScoreCategory selectCategory = (ScoreCategory) scoreConfiguration.getScoreCategories().get(form.getHiddenCategoryName13());
			int min = Integer.parseInt(form.getMinimumValueText13());
			int max = Integer.parseInt(form.getMaximumValueText13());
			for (Iterator it = selectCategory.getScoreValues().iterator(); it.hasNext();) {
				ScoreValue scoreValue = (ScoreValue) it.next();
				if ((min >= scoreValue.getMinimumValue() && min <= scoreValue.getMaximumValue())
						|| (max >= scoreValue.getMinimumValue() && max <= scoreValue.getMaximumValue())) {
					errors.add("Category13Error", new ActionError("error.scoring.duplicate.value"));
					invalid = true;
					saveErrors(request, errors);
					break;
				}
				++valueCount;
			}

			if (!invalid) {
				selectCategory.addScoreValue(valueCount, new ScoreValue(min, max, Integer.parseInt(form.getPointsText13())));
				StoreInSession(scoreConfiguration, true, form, request);
				form.setMinimumValueText13("");
				form.setMaximumValueText13("");
				form.setPointsText13("");
			}
		}

		// Amount Transaction
		if (!StringHelper.isNullOrEmpty(form.getSubmitAdd15())) {
			request.setAttribute("jumpTo", "cat15");
			invalid = false;
			valueCount = 0;
			ScoreCategory selectCategory = (ScoreCategory) scoreConfiguration.getScoreCategories().get(form.getHiddenCategoryName15());
			int min = Integer.parseInt(form.getMinimumValueText15());
			int max = Integer.parseInt(form.getMaximumValueText15());
			for (Iterator it = selectCategory.getScoreValues().iterator(); it.hasNext();) {
				ScoreValue scoreValue = (ScoreValue) it.next();
				if ((min >= scoreValue.getMinimumValue() && min <= scoreValue.getMaximumValue())
						|| (max >= scoreValue.getMinimumValue() && max <= scoreValue.getMaximumValue())) {
					errors.add("Category15Error", new ActionError("error.scoring.duplicate.value"));
					invalid = true;
					saveErrors(request, errors);
					break;
				}
				++valueCount;
			}

			if (!invalid) {
				selectCategory.addScoreValue(valueCount, new ScoreValue(min, max, Integer.parseInt(form.getPointsText15())));
				StoreInSession(scoreConfiguration, true, form, request);
				form.setMinimumValueText15("");
				form.setMaximumValueText15("");
				form.setPointsText15("");
			}
		}

		if (request.getParameter("action") != null) {
			if (request.getParameter("action").equals("edit")) {
				Collection chosenConfig = ss.getScoreConfigurations(up.getUID(), (String) request.getParameter("configId"), null);
				// There should be only one. Change the code to get only one
				// instead of iterating
				for (Iterator it = chosenConfig.iterator(); it.hasNext();) {
					ScoreConfiguration scoreConfig = (ScoreConfiguration) it.next();
					StoreInSession(scoreConfig, false, form, request);
				}
			}
		}

		if (invalid && !globalMsg) {
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.page.contains.error"));
			saveErrors(request, errors);
		}

		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private void StoreInSession(ScoreConfiguration scoreConfiguration, boolean retainFormVariables, EditScoringConfigForm form,
			HttpServletRequest request) {

		if (!retainFormVariables) {
			form.setAcceptThreshold(String.valueOf(scoreConfiguration.getAcceptThreshold()));
			form.setRejectThreshold(String.valueOf(scoreConfiguration.getRejectThreshold()));

			int count = 1;
			for (Iterator it = scoreConfiguration.getScoreCategories().values().iterator(); it.hasNext();) {
				ScoreCategory scoreCategory = (ScoreCategory) it.next();
				if (scoreCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[0])) {
					form.setDisable1(scoreCategory.isIncluded() ? null : "on");
					form.setWeightText1(String.valueOf(scoreCategory.getWeight()));
					request.getSession().setAttribute("scoreCategory1", scoreCategory);
				}
				if (scoreCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[1])) {
					form.setDisable2(scoreCategory.isIncluded() ? null : "on");
					form.setWeightText2(String.valueOf(scoreCategory.getWeight()));
					request.getSession().setAttribute("scoreCategory2", scoreCategory);
				}
				if (scoreCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[2])) {
					form.setDisable3(scoreCategory.isIncluded() ? null : "on");
					form.setWeightText3(String.valueOf(scoreCategory.getWeight()));
					request.getSession().setAttribute("scoreCategory3", scoreCategory);
				}
				if (scoreCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[3])) {
					form.setDisable4(scoreCategory.isIncluded() ? null : "on");
					form.setWeightText4(String.valueOf(scoreCategory.getWeight()));
					request.getSession().setAttribute("scoreCategory4", scoreCategory);
				}
				if (scoreCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[4])) {
					form.setDisable5(scoreCategory.isIncluded() ? null : "on");
					form.setWeightText5(String.valueOf(scoreCategory.getWeight()));
					request.getSession().setAttribute("scoreCategory5", scoreCategory);
				}
				if (scoreCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[5])) {
					form.setDisable6(scoreCategory.isIncluded() ? null : "on");
					form.setWeightText6(String.valueOf(scoreCategory.getWeight()));
					request.getSession().setAttribute("scoreCategory6", scoreCategory);
				}
				if (scoreCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[6])) {
					form.setDisable7(scoreCategory.isIncluded() ? null : "on");
					form.setWeightText7(String.valueOf(scoreCategory.getWeight()));
					request.getSession().setAttribute("scoreCategory7", scoreCategory);
				}
				if (scoreCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[7])) {
					form.setDisable8(scoreCategory.isIncluded() ? null : "on");
					form.setWeightText8(String.valueOf(scoreCategory.getWeight()));
					request.getSession().setAttribute("scoreCategory8", scoreCategory);
				}
				if (scoreCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[8])) {
					form.setDisable9(scoreCategory.isIncluded() ? null : "on");
					form.setWeightText9(String.valueOf(scoreCategory.getWeight()));
					request.getSession().setAttribute("scoreCategory9", scoreCategory);
				}
				if (scoreCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[9])) {
					form.setDisable10(scoreCategory.isIncluded() ? null : "on");
					form.setWeightText10(String.valueOf(scoreCategory.getWeight()));
					request.getSession().setAttribute("scoreCategory10", scoreCategory);
				}
				if (scoreCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[10])) {
					form.setDisable11(scoreCategory.isIncluded() ? null : "on");
					form.setWeightText11(String.valueOf(scoreCategory.getWeight()));
					request.getSession().setAttribute("scoreCategory11", scoreCategory);
				}
				if (scoreCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[11])) {
					form.setDisable12(scoreCategory.isIncluded() ? null : "on");
					form.setWeightText12(String.valueOf(scoreCategory.getWeight()));
					request.getSession().setAttribute("scoreCategory12", scoreCategory);
				}
				if (scoreCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[12])) {
					form.setDisable13(scoreCategory.isIncluded() ? null : "on");
					form.setWeightText13(String.valueOf(scoreCategory.getWeight()));
					request.getSession().setAttribute("scoreCategory13", scoreCategory);
				}
				if (scoreCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[14])) {
					form.setDisable15(scoreCategory.isIncluded() ? null : "on");
					form.setWeightText15(String.valueOf(scoreCategory.getWeight()));
					request.getSession().setAttribute("scoreCategory15", scoreCategory);
				}
				count++;
			}
		} else {
			scoreConfiguration.setAcceptThreshold(Integer.parseInt(form.getAcceptThreshold()));
			scoreConfiguration.setRejectThreshold(Integer.parseInt(form.getRejectThreshold()));

			int count = 1;
			for (Iterator it1 = scoreConfiguration.getScoreCategories().values().iterator(); it1.hasNext();) {
				ScoreCategory scoringCategory = (ScoreCategory) it1.next();

				if (scoringCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[0])) {
					scoringCategory.setIncluded(!"on".equals(form.getDisable1()));
					scoringCategory.setWeight(Integer.parseInt(form.getWeightText1()));
					request.getSession().setAttribute("scoreCategory1", scoringCategory);
				}
				if (scoringCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[1])) {
					scoringCategory.setIncluded(!"on".equals(form.getDisable2()));
					scoringCategory.setWeight(Integer.parseInt(form.getWeightText2()));
					request.getSession().setAttribute("scoreCategory2", scoringCategory);
				}
				if (scoringCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[2])) {
					scoringCategory.setIncluded(!"on".equals(form.getDisable3()));
					scoringCategory.setWeight(Integer.parseInt(form.getWeightText3()));
					request.getSession().setAttribute("scoreCategory3", scoringCategory);
				}
				if (scoringCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[3])) {
					scoringCategory.setIncluded(!"on".equals(form.getDisable4()));
					scoringCategory.setWeight(Integer.parseInt(form.getWeightText4()));
					request.getSession().setAttribute("scoreCategory4", scoringCategory);
				}
				if (scoringCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[4])) {
					scoringCategory.setIncluded(!"on".equals(form.getDisable5()));
					scoringCategory.setWeight(Integer.parseInt(form.getWeightText5()));
					request.getSession().setAttribute("scoreCategory5", scoringCategory);
				}
				if (scoringCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[5])) {
					scoringCategory.setIncluded(!"on".equals(form.getDisable6()));
					scoringCategory.setWeight(Integer.parseInt(form.getWeightText6()));
					request.getSession().setAttribute("scoreCategory6", scoringCategory);
				}
				if (scoringCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[6])) {
					scoringCategory.setIncluded(!"on".equals(form.getDisable7()));
					scoringCategory.setWeight(Integer.parseInt(form.getWeightText7()));
					request.getSession().setAttribute("scoreCategory7", scoringCategory);
				}
				if (scoringCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[7])) {
					scoringCategory.setIncluded(!"on".equals(form.getDisable8()));
					scoringCategory.setWeight(Integer.parseInt(form.getWeightText8()));
					request.getSession().setAttribute("scoreCategory8", scoringCategory);
				}
				if (scoringCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[8])) {
					scoringCategory.setIncluded(!"on".equals(form.getDisable9()));
					scoringCategory.setWeight(Integer.parseInt(form.getWeightText9()));
					request.getSession().setAttribute("scoreCategory9", scoringCategory);
				}
				if (scoringCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[9])) {
					scoringCategory.setIncluded(!"on".equals(form.getDisable10()));
					scoringCategory.setWeight(Integer.parseInt(form.getWeightText10()));
					request.getSession().setAttribute("scoreCategory10", scoringCategory);
				}
				if (scoringCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[10])) {
					scoringCategory.setIncluded(!"on".equals(form.getDisable11()));
					scoringCategory.setWeight(Integer.parseInt(form.getWeightText11()));
					request.getSession().setAttribute("scoreCategory11", scoringCategory);
				}
				if (scoringCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[11])) {
					scoringCategory.setIncluded(!"on".equals(form.getDisable12()));
					scoringCategory.setWeight(Integer.parseInt(form.getWeightText12()));
					request.getSession().setAttribute("scoreCategory12", scoringCategory);
				}
				if (scoringCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[12])) {
					scoringCategory.setIncluded(!"on".equals(form.getDisable13()));
					scoringCategory.setWeight(Integer.parseInt(form.getWeightText13()));
					request.getSession().setAttribute("scoreCategory13", scoringCategory);
				}
				if (scoringCategory.getScoreCategoryName().equalsIgnoreCase(ScoreCategory.catList[14])) {
					scoringCategory.setIncluded(!"on".equals(form.getDisable15()));
					scoringCategory.setWeight(Integer.parseInt(form.getWeightText15()));
					request.getSession().setAttribute("scoreCategory15", scoringCategory);
				}
				count++;
			}
		}
		request.getSession().setAttribute("chosenConfig", scoreConfiguration);

	}

	private String getAnchorName(String scoringCategoryName) {
		int i = 0;
		for (i = 0; i < cats.length; i++) {
			if (scoringCategoryName.equals(cats[i])) {
				break;
			}
		}
		return i == cats.length ? "" : "cat" + String.valueOf(i + 1);
	}
}
