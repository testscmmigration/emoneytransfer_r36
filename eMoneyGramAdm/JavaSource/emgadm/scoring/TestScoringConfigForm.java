package emgadm.scoring;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgshared.model.ScoreConfiguration;

public class TestScoringConfigForm extends EMoneyGramAdmBaseValidatorForm {
	private static final long serialVersionUID = 1L;
	private String submitTestScore;
	private String submitTestDone;
	private String result1;
	private String result2;
	private String result3;
	private String result4;
	private String result5;
	private String result6;
	private String result7;
	private String result8;
	private String result9;
	private String result10;
	private String result11;
	private String result12;
	private String result13;
	private String result14;
	private String result15;
	private ScoreConfiguration scoreConfiguration;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		submitTestScore = null;
		submitTestDone = null;
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);
		return errors;
	}

	public String getResult1() {
		return result1;
	}

	public String getResult2() {
		return result2;
	}

	public String getResult3() {
		return result3;
	}

	public String getResult4() {
		return result4;
	}

	public String getSubmitTestDone() {
		return submitTestDone;
	}

	public String getSubmitTestScore() {
		return submitTestScore;
	}

	public void setResult1(String string) {
		result1 = string;
	}

	public void setResult2(String string) {
		result2 = string;
	}

	public void setResult3(String string) {
		result3 = string;
	}

	public void setResult4(String string) {
		result4 = string;
	}

	public void setSubmitTestDone(String string) {
		submitTestDone = string;
	}

	public void setSubmitTestScore(String string) {
		submitTestScore = string;
	}

	public ScoreConfiguration getScoreConfiguration() {
		return scoreConfiguration;
	}

	public void setScoreConfiguration(ScoreConfiguration configuration) {
		scoreConfiguration = configuration;
	}

	public String getResult5() {
		return result5;
	}

	public String getResult6() {
		return result6;
	}

	public String getResult7() {
		return result7;
	}

	public String getResult8() {
		return result8;
	}

	public String getResult9() {
		return result9;
	}

	public String getResult10() {
		return result10;
	}

	public String getResult11() {
		return result11;
	}

	public String getResult12() {
		return result12;
	}

	public String getResult13() {
		return result13;
	}

	public void setResult5(String string) {
		result5 = string;
	}

	public void setResult6(String string) {
		result6 = string;
	}

	public void setResult7(String string) {
		result7 = string;
	}

	public void setResult8(String string) {
		result8 = string;
	}

	public void setResult9(String string) {
		result9 = string;
	}

	public void setResult10(String string) {
		result10 = string;
	}

	public void setResult11(String string) {
		result11 = string;
	}

	public void setResult12(String string) {
		result12 = string;
	}

	public void setResult13(String string) {
		result13 = string;
	}

	public void setResult14(String result14) {
		this.result14 = result14;
	}

	public String getResult14() {
		return result14;
	}

	public void setResult15(String result15) {
		this.result15 = result15;
	}

	public String getResult15() {
		return result15;
	}
}
