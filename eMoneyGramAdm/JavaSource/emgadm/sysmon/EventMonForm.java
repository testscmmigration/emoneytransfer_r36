package emgadm.sysmon;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;
import emgshared.exceptions.ParseDateException;
import emgshared.util.DateFormatter;

public class EventMonForm extends EMoneyGramAdmBaseValidatorForm
{
	private static final DateFormatter df =
		new DateFormatter("dd/MMM/yyyy", true);

	private String selType;
	private String refTime;
	private String archiveDate;
	private String displayDate;
	private String submitGo;

	public String getSelType()
	{
		return selType;
	}

	public void setSelType(String string)
	{
		selType = string;
	}

	public String getRefTime()
	{
		return refTime;
	}

	public void setRefTime(String string)
	{
		refTime = string;
	}

	public String getArchiveDate()
	{
		return archiveDate;
	}

	public void setArchiveDate(String string)
	{
		archiveDate = string;
	}

	public String getDisplayDate()
	{
		return displayDate;
	}

	public void setDisplayDate(String string)
	{
		displayDate = string;
	}

	public String getSubmitGo()
	{
		return submitGo;
	}

	public void setSubmitGo(String string)
	{
		submitGo = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);
		selType = "R";
		long now = System.currentTimeMillis();
		long oneDay = 24L * 60L * 60L * 1000L;
		archiveDate = df.format(new Date(now - oneDay));
		submitGo = null;
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = super.validate(mapping, request);

		if (!StringHelper.isNullOrEmpty(submitGo))
		{
			if ("R".equalsIgnoreCase(selType))
			{
				if (StringHelper.isNullOrEmpty(refTime))
				{
					errors.add(
						"refTime",
						new ActionError(
							"errors.required",
							"Refresh Time Period"));
				} else if (refTime.length() > 3)
				{
					String[] parm = { "Refresh Time Period", "3" };
					errors.add(
						"refTime",
						new ActionError("error.length.wrong", parm));
				} else if (StringHelper.containsNonDigits(refTime))
				{
					errors.add(
						"refTime",
						new ActionError(
							"errors.integer",
							"Refresh Time Period"));
				}
			}

			if ("A".equalsIgnoreCase(selType))
			{
				long oneDay = 24L * 60L * 60L * 1000L;
				Date yesterday = new Date(System.currentTimeMillis() - oneDay);
				try
				{
					Date tmpDate = df.parse(archiveDate);
					if (tmpDate.after(yesterday))
					{
						String[] parm = { "today", "archive" };
						errors.add(
							"archiveDate",
							new ActionError("error.date.before", parm));
					}
				} catch (ParseDateException e)
				{
					errors.add(
						"archiveDate",
						new ActionError("error.malformed.date"));
				}
			}
		}
		return errors;
	}
}