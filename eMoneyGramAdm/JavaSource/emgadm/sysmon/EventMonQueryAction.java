package emgadm.sysmon;

import java.io.FileInputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.util.StringHelper;
import emgshared.property.EMTSharedContainerProperties;
import emgshared.util.DateFormatter;
import eventmon.eventmonitor.DailyEventStat;
import eventmon.eventmonitor.EventMonitor;

public class EventMonQueryAction extends EMoneyGramAdmBaseAction
{
	private static final DateFormatter df =
		new DateFormatter("dd/MMM/yyyy", true);
	private static final NumberFormat nf1 = new DecimalFormat("0000");
	private static final NumberFormat nf2 = new DecimalFormat("00");

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		EventMonQueryForm form = (EventMonQueryForm) f;

		if (!StringHelper.isNullOrEmpty(form.getSubmitGo()))
		{
			form.setSelEvent(form.getEvent());
			form.setSelDateRange(
				form.getBeginDate() + " - " + form.getEndDate());

			ActionErrors errors = new ActionErrors();

			Calendar beginCal = Calendar.getInstance();
			beginCal.setTime(df.parse(form.getBeginDate()));

			Calendar endCal = Calendar.getInstance();
			endCal.setTime(df.parse(form.getEndDate()));

			byte start, end;
			if ("A".equals(form.getTimeType()))
			{
				start = 0;
				end = 23;
			} else
			{
				start = Byte.parseByte(form.getStartTime());
				end = Byte.parseByte(form.getEndTime());
			}

			String timeRange =
				(start < 12 ? start : start - 12)
					+ ":00 "
					+ (start < 12 ? "AM" : "PM")
					+ " - "
					+ (end < 12 ? end : end - 12)
					+ ":59"
					+ (end < 12 ? "AM" : "PM");
			form.setSelTimeRange(timeRange);

			List resultList =
				getData(
					form.getEvent(),
					beginCal,
					endCal,
					start,
					end,
					timeRange);

			if (resultList == null || resultList.size() == 0)
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError(
						"errors.no.match.record.found",
						"Event Monitor"));
				saveErrors(request, errors);
			} else
			{
				request.getSession().setAttribute("resultList", resultList);
			}
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private EventMonitor deserializeObj(String archiveDate) throws Exception
	{
		EventMonitor em = null;
		String dir = EMTSharedContainerProperties.getEventMonDir();

		Calendar cal = Calendar.getInstance();
		cal.setTime(df.parse(archiveDate));
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int day = cal.get(Calendar.DAY_OF_MONTH);
		String archiveFile =
			dir
				+ "/"
				+ nf1.format(year)
				+ "-"
				+ nf2.format(month)
				+ "-"
				+ nf2.format(day)
				+ ".ser";

		ObjectInput in = null;
		try
		{
			in = new ObjectInputStream(new FileInputStream(archiveFile));
			em = (EventMonitor) in.readObject();
		} finally
		{
			if (in != null)
			{
				in.close();
			}
		}

		return em;
	}

	private List getData(
		String event,
		Calendar beginCal,
		Calendar endCal,
		byte idx1,
		byte idx2,
		String timeRange)
	{

		List archiveList = new ArrayList();
		Calendar tmpCal = beginCal;

		while (!tmpCal.after(endCal))
		{
			EventMonitor em = null;
			try
			{
				em = deserializeObj(df.format(tmpCal.getTime()));
			} catch (Exception e)
			{
			}

			if (em != null)
			{
				archiveList.add(em);
			}
			tmpCal.add(Calendar.DATE, 1);
		}

		if (archiveList.size() == 0)
		{
			return null;
		}

		List eventDataList = new ArrayList();
		Iterator iter = archiveList.iterator();
		while (iter.hasNext())
		{
			EventMonitor em = (EventMonitor) iter.next();
			DailyEventStat des = (DailyEventStat) em.getEventMap().get(event);

			if (des != null)
			{
				EventStatView esv = new EventStatView();

				esv.setDate(
					nf2.format(em.getMonth())
						+ "/"
						+ nf2.format(em.getDay())
						+ "/"
						+ nf1.format(em.getYear()));
				esv.setTimePeriod(timeRange);

				if (idx1 == 0 && idx2 == 23)
				{
					esv.setCount(String.valueOf(des.getDailyCount()));
					esv.setHigh(String.valueOf(des.getDailyHigh()));
					esv.setAverage(String.valueOf(des.getDailyAverage()));
					esv.setLow(String.valueOf(des.getDailyLow()));
				} else
				{
					short tmpCnt = 0;
					float tmpHigh = 0F;
					float tmpTotal = 0F;
					float tmpLow = 9999F;
					for (byte i = idx1; i <= idx2; i++)
					{
						tmpCnt += des.getCount(i);
						tmpHigh =
							tmpHigh > des.getHigh(i) ? tmpHigh : des.getHigh(i);
						tmpTotal += des.getAverage(i) * des.getCount(i);
						float tLow =
							des.getLow(i) == 0F ? 9999F : des.getLow(i);
						tmpLow = tLow < tmpLow ? tLow : tmpLow;
					}
					tmpLow = (tmpLow == 9999F ? 0F : tmpLow);
					esv.setCount(String.valueOf(tmpCnt));
					esv.setHigh(String.valueOf(tmpHigh));
					float tmpAvg =
						tmpCnt == 0
							? 0
							: Math.round(tmpTotal / tmpCnt * 100F) / 100F;
					esv.setAverage(String.valueOf(tmpAvg));
					esv.setLow(String.valueOf(tmpLow));
				}

				if (Integer.parseInt(esv.getCount()) != 0)
				{
					eventDataList.add(esv);
				}
			}
		}

		return eventDataList;
	}
}
