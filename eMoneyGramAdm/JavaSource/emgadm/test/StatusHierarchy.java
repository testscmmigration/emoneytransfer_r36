package emgadm.test;


public class StatusHierarchy {

	private String reasonID;
	private String tranReasCode;
	private String tranStatCode;
	private String tranSubStatCode;
	private String tranOldStatCode;
	private String tranOldSubStatCode;
	private String tranReasDesc;
	private int drGLAcctId;
	private int crGLAcctId;
	private String drGLAcctDesc;
	private String crGLAcctDesc;
	private String tranReasTypeCode;
	private String tranSubReasDesc;
	
	public String getCrGLAcctDesc() {
		return crGLAcctDesc;
	}
	public void setCrGLAcctDesc(String crGLAcctDesc) {
		this.crGLAcctDesc = crGLAcctDesc;
	}
	public int getCrGLAcctId() {
		return crGLAcctId;
	}
	public void setCrGLAcctId(int crGLAcctId) {
		this.crGLAcctId = crGLAcctId;
	}
	public String getDrGLAcctDesc() {
		return drGLAcctDesc;
	}
	public void setDrGLAcctDesc(String drGLAcctDesc) {
		this.drGLAcctDesc = drGLAcctDesc;
	}
	public int getDrGLAcctId() {
		return drGLAcctId;
	}
	public void setDrGLAcctId(int drGLAcctId) {
		this.drGLAcctId = drGLAcctId;
	}
	public String getTranOldStatCode() {
		return tranOldStatCode;
	}
	public void setTranOldStatCode(String tranOldStatCode) {
		this.tranOldStatCode = tranOldStatCode;
	}
	public String getTranOldSubStatCode() {
		return tranOldSubStatCode;
	}
	public void setTranOldSubStatCode(String tranOldSubStatCode) {
		this.tranOldSubStatCode = tranOldSubStatCode;
	}
	public String getTranReasCode() {
		return tranReasCode;
	}
	public void setTranReasCode(String tranReasCode) {
		this.tranReasCode = tranReasCode;
	}
	public String getTranReasDesc() {
		return tranReasDesc;
	}
	public void setTranReasDesc(String tranReasDesc) {
		this.tranReasDesc = tranReasDesc;
	}
	public String getTranReasTypeCode() {
		return tranReasTypeCode;
	}
	public void setTranReasTypeCode(String tranReasTypeCode) {
		this.tranReasTypeCode = tranReasTypeCode;
	}
	public String getTranStatCode() {
		return tranStatCode;
	}
	public void setTranStatCode(String tranStatCode) {
		this.tranStatCode = tranStatCode;
	}
	public String getTranSubReasDesc() {
		return tranSubReasDesc;
	}
	public void setTranSubReasDesc(String tranSubReasDesc) {
		this.tranSubReasDesc = tranSubReasDesc;
	}
	public String getTranSubStatCode() {
		return tranSubStatCode;
	}
	public void setTranSubStatCode(String tranSubStatCode) {
		this.tranSubStatCode = tranSubStatCode;
	}
	public String getReasonID() {
		return reasonID;
	}
	public void setReasonID(String reasonID) {
		this.reasonID = reasonID;
	}
	
}
