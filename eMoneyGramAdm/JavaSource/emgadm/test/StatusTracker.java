package emgadm.test;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class StatusTracker {

	protected static Connection DBconn = null;
	protected static String pDBConnString = "jdbc:oracle:thin:@172.16.32.201:1522:EMGD";
	protected static String pDBUserId = "emgmgi";
	protected static String pDBDriverName = "oracle.jdbc.driver.OracleDriver";
	protected static ArrayList masterList = new ArrayList();
	protected static HashMap exhaustedList = new HashMap();
	private static int linecount = 0;

	private static void buildHierarchy(String statusCombo, int level) {
		StatusTracker.exhaustedList.put(statusCombo, statusCombo);
		for (Iterator iter = StatusTracker.masterList.iterator(); iter
				.hasNext();) {
			StatusHierarchy sh = (StatusHierarchy) iter.next();
			String key = sh.getTranOldStatCode() + "/"
					+ sh.getTranOldSubStatCode();
			if (key.equalsIgnoreCase(statusCombo)) {
				String linecount2 = String.valueOf(linecount);
				if (linecount2.length() < 2)
					linecount2 = "0" + linecount2;
				System.out.print(linecount2);
				for (int i = 1; i < level; i++) {
					if (i % 2 == 0)
						System.out.print("|  ");
					else
						System.out.print("-  ");
				}
				StatusTracker.linecount++;
				// System.out.println("-Level:" + level + "=" + statusCombo
				// +" -->"+ sh.getTranStatCode() + "/" + sh.getTranSubStatCode()
				// + "  " + sh.getTranReasDesc());
				String key2 = sh.getTranStatCode() + "/"
						+ sh.getTranSubStatCode();
				if (!StatusTracker.exhaustedList.containsKey(key2))
					buildHierarchy(
							sh.getTranStatCode() + "/"
									+ sh.getTranSubStatCode(), level + 1);
			}

		}
		return;
	}

}
