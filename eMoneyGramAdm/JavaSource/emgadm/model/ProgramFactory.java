package emgadm.model;

public class ProgramFactory {
	public static final String DEFAULT_PROGRAM_ID = "MGO";

	public static Program createProgram(String id, String name, String description) {
		ProgramImpl program = new ProgramImpl(id, name, description);
		return program;
	}
	
	public static Program getInstance(String id) {
		Program instance = ProgramImpl.getInstance(id);
		return (instance != null) ? instance : ProgramImpl.getInstance(DEFAULT_PROGRAM_ID);
	}
}
