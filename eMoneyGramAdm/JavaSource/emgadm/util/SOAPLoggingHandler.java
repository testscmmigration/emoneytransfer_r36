package emgadm.util;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.axis.AxisFault;
import org.apache.axis.MessageContext;
import org.apache.axis.handlers.BasicHandler;
import org.apache.axis.soap.SOAPConstants;
import org.apache.axis.transport.http.HTTPConstants;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import javax.xml.parsers.ParserConfigurationException;

public class SOAPLoggingHandler extends BasicHandler {

	private static Logger LOG = LogFactory.getInstance().getLogger(
			SOAPLoggingHandler.class);
	private static final long serialVersionUID = 1L;
	private static List<String> redactedFieldsList = new ArrayList<String>(15);
	private static String REDACTED = "REDACTED";
	private static String CONTENT_TYPE = "Content-Type: ";
	private static String PAYLOAD = "Payload: ";
	private static String STATUS = "Response-Code: ";
	private static String NEWLINE = "\n";
	private static String ID = "ID";
	private Long value = 1l;

	static {
		redactedFieldsList.add("accountNumber");
		redactedFieldsList.add("cvv");
		redactedFieldsList.add("abaNumber");
		redactedFieldsList.add("bankRoutingNumber");
		redactedFieldsList.add("senderPhotoIdNumber");
		redactedFieldsList.add("encryptedAccountNumber");
		redactedFieldsList.add("number");
		redactedFieldsList.add("externalId");
		redactedFieldsList.add("identification");
		redactedFieldsList.add("binNumber");
		redactedFieldsList.add("fileBytes");
		redactedFieldsList.add("securityAnswer");
	}

	public List<String> getRedactedFieldsList() {
		return redactedFieldsList;
	}

	public void setRedactedFieldsList(List<String> redactedFieldsList) {
		this.redactedFieldsList = redactedFieldsList;
	}

	public void invoke(MessageContext msgContext) throws AxisFault {
		String soapXML = "";
		if (msgContext.getResponseMessage() != null
				&& msgContext.getResponseMessage().getSOAPPart() != null) {
			soapXML = maskSensitiveInformation(msgContext.getResponseMessage()
					.getSOAPPartAsString());
			Integer statusCode = (Integer) msgContext
					.getProperty(HTTPConstants.MC_HTTP_STATUS_CODE);
			msgContext.getProperty(ID);
			printSOAPMessage(soapXML, msgContext, String.valueOf(statusCode));
		} else {
			if (msgContext.getRequestMessage() != null
					&& msgContext.getRequestMessage().getSOAPPartAsString() != null) {
				soapXML = maskSensitiveInformation(msgContext
						.getRequestMessage().getSOAPPartAsString());
				msgContext.setProperty(ID, value++);
				printSOAPMessage(soapXML, msgContext, null);
			}
		}
	}

	@Override
	public void onFault(MessageContext context) {
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(NEWLINE + "ID :" + context.getProperty(ID));
			sb.append(NEWLINE + STATUS);
			Integer responseCode = (Integer) context
					.getProperty(HTTPConstants.MC_HTTP_STATUS_CODE);
			sb.append(responseCode + NEWLINE);
			sb.append(PAYLOAD);
			sb.append(context.getResponseMessage().getSOAPPartAsString());
			LOG.info(sb.toString());
		} catch (AxisFault e) {
			throw new RuntimeException("Failed to log unsuccessful response", e);
		}
	}

	private String maskSensitiveInformation(String inputXmlString) {
		try {
			Document document = loadXMLFromString(inputXmlString);
			NodeList nodeList = document.getElementsByTagName("*");
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					for (String sensitiveField : redactedFieldsList) {
						if (node.getNodeName().contains(sensitiveField)) {
							node.setTextContent(REDACTED);
						}
					}
				}
			}
			Source domSource = new DOMSource(document);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);

			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(
					"{http://xml.apache.org/xslt}indent-amount", "2");
			transformer.transform(domSource, result);
			return writer.toString();
		} catch (Exception ex) {
			throw new RuntimeException("Failed to replace the element value.",
					ex);
		}
	}

	private Document loadXMLFromString(String xml) throws Exception {DocumentBuilderFactory factory;
    DocumentBuilder builder = null;
    String FEATURE = null;
    try {
        factory = DocumentBuilderFactory.newInstance();
        FEATURE = "http://apache.org/xml/features/disallow-doctype-decl";
        factory.setFeature(FEATURE, true);
        FEATURE = "http://xml.org/sax/features/external-general-entities";
        factory.setFeature(FEATURE, false);
        FEATURE = "http://xml.org/sax/features/external-parameter-entities";
        factory.setFeature(FEATURE, false);
        FEATURE = "http://apache.org/xml/features/nonvalidating/load-external-dtd";
        factory.setFeature(FEATURE, false);
        factory.setXIncludeAware(false);
        factory.setExpandEntityReferences(false);
        builder = factory.newDocumentBuilder();
 
    } catch (ParserConfigurationException e) {
        LOG.info("ParserConfigurationException was thrown. The feature '"
                + FEATURE
                + "' is probably not supported by your XML processor.");
    }
    return builder.parse(new ByteArrayInputStream(xml.getBytes("UTF-8")));}
	

	@SuppressWarnings("static-access")
	private static void printSOAPMessage(String xml, MessageContext msgContext,
			String id) {
		StringBuilder sb = new StringBuilder();
		sb.append(NEWLINE);
		sb.append("-----------------------------------------------------------");
		sb.append(NEWLINE + ID + ": " + msgContext.getProperty("ID"));
		sb.append(NEWLINE);
		SOAPConstants soapConstants = msgContext.getCurrentContext() == null ? SOAPConstants.SOAP11_CONSTANTS
				: msgContext.getCurrentContext().getSOAPConstants();
		sb.append(CONTENT_TYPE + soapConstants.getContentType());
		sb.append(NEWLINE);
		if (null != id) {
			sb.append(STATUS + id + NEWLINE);
		}
		sb.append(PAYLOAD);
		sb.append(xml);
		sb.append(NEWLINE);
		sb.append("-----------------------------------------------------------");
		LOG.info(sb.toString());
	}
}
