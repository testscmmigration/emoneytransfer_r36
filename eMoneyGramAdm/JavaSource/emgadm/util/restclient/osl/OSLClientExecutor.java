/**
 * 
 */
package emgadm.util.restclient.osl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


import emgadm.property.EMTAdmContainerProperties;
import emgadm.util.restclient.osl.fundandcommit.FundAndCommitRequest;
import emgadm.util.restclient.osl.fundandcommit.FundAndCommitResponse;
import emgadm.util.restclient.osl.preparepayment.PreparePaymentResponse;
import engadm.util.restclient.osl.canceltransaction.CancelTransactionRequest;
import engadm.util.restclient.osl.canceltransaction.CancelTransactionRequest.CancelReasonCode;
import engadm.util.restclient.osl.canceltransaction.CancelTransactionResponse;
import engadm.util.restclient.osl.canceltransaction.CancelTransactionRequest.CancelReasonCode;

/**  
 * @author bok0
 * 
 */
public class OSLClientExecutor {

	final static Logger logger = Logger.getLogger(OSLClientExecutor.class);

	private final String oslUrl = EMTAdmContainerProperties.getOslUcpCreateEndPoint();
	private final String OSL_SERVICE_FUND_AND_COMMIT_END_POINT = "transactions/fundAndCommit";
	private static final String OSL_SERVICE_PREPAREPAYMENT_METHOD = "transactions/preparePayment";
	private static final String OSL_CANCEL_TRANSACTION_API = "transactions/cancel";
	private final String SOURCE_APPLICATION = "EMT";
	private final String CSR_CANCEL_REASON_DESC ="CSR Canceled"; 


	public FundAndCommitResponse processFundAndCommit(String transactionid) {

		DefaultHttpClient httpClient = new DefaultHttpClient();
		String endPointUrl  = oslUrl + OSL_SERVICE_FUND_AND_COMMIT_END_POINT;
		logger.info("endPointUrl"+endPointUrl);
		HttpPost httpPost = new HttpPost(endPointUrl);
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Content-type", "application/json");
		HttpResponse httpResponse = null;

		String apiResponse = null;
		String apiRequest = null;
		FundAndCommitResponse fundAndCommitResponse = null;
		FundAndCommitRequest fundAndCommitRequest = new FundAndCommitRequest();
		fundAndCommitRequest.setSourceApplication(SOURCE_APPLICATION);
		fundAndCommitRequest.setTransactionId(transactionid);
		ObjectMapper objectMapperReader = null;
		ObjectMapper objectMappeWriter = null;
		StringEntity stringEntity = null;
		try {
			objectMapperReader = new ObjectMapper();
			objectMappeWriter = new ObjectMapper();
			apiRequest = objectMappeWriter.writeValueAsString(fundAndCommitRequest);
			stringEntity = new StringEntity(apiRequest);
			httpPost.setEntity(stringEntity);
			httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			apiResponse = EntityUtils.toString(httpEntity);
			logger.info("apiResponse" + ESAPI.encoder().encodeForHTMLAttribute(apiResponse));
			fundAndCommitResponse = objectMapperReader.readValue(apiResponse,
					FundAndCommitResponse.class);
		} 
		
		catch (JsonParseException jsonParseException) {
			// TODO Auto-generated catch block
			logger.error("JsonParseException, while calling Preparepayment"
					+ jsonParseException.getMessage());

		}
		
		catch (JsonMappingException jsonMappingException) {
			// TODO Auto-generated catch block
			logger.error("JsonMappingException while calling Preparepayment"
					+ jsonMappingException.getMessage());
		}
		
		catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			logger.error("UnsupportedEncodingException while calling Preparepayment"
					+ e.getMessage());

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			logger.error("ClientProtocolException while calling Preparepayment"
					+ e.getMessage());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("IOException while calling Preparepayment" + e.getMessage());

		}

		return fundAndCommitResponse;
	}
	
	public PreparePaymentResponse callPreparePayment(String csrUserid,
			String transactionId) throws Exception {
		
		logger.info("****start of callPreparePayment()--> Call to OSL-PreparePayment API Rest Call->csrUserid:"+ESAPI.encoder().encodeForHTMLAttribute(csrUserid)+"|transactionId:"+ESAPI.encoder().encodeForHTMLAttribute(transactionId)+"****");
		String oslUrl = EMTAdmContainerProperties.getOslUcpCreateEndPoint();
		logger.info("oslUrl"+oslUrl);
		DefaultHttpClient httpClient = new DefaultHttpClient();
		String endPointUrl = oslUrl + OSL_SERVICE_PREPAREPAYMENT_METHOD;
		logger.info("endPointUrl"+endPointUrl);
		HttpPost httpPostRequest = new HttpPost(endPointUrl);
		httpPostRequest.setHeader("Accept", "application/json");
		httpPostRequest.setHeader("Content-type", "application/json");
		String jsonString = "{ \"csrUserid\": \"" + csrUserid
				+ "\",\"sourceApplication\": \"" + SOURCE_APPLICATION
				+ "\",\"transactionId\": \"" + transactionId + "\" }";
		StringEntity stringEntity = null;
		PreparePaymentResponse preparePaymentResponse = null;
		try {
			stringEntity = new StringEntity(jsonString);
			httpPostRequest.setEntity(stringEntity);
			HttpResponse response = httpClient.execute(httpPostRequest);
			HttpEntity httpEntity = response.getEntity();
			String apiOutput = EntityUtils.toString(httpEntity);
			logger.info("preparePaymentResponse->apiOutput:"+ESAPI.encoder().encodeForHTMLAttribute(apiOutput)+"****");
			ObjectMapper objectMapper = new ObjectMapper();
			preparePaymentResponse = objectMapper.readValue(apiOutput,
					PreparePaymentResponse.class);
		} catch (UnsupportedEncodingException e) {
			logger.error("UnsupportedEncodingException while calling Preparepayment" +e.getMessage());
			  throw new IllegalStateException(
			  "call Preparepayment Failed  - unable to process transaction type ("
			  + transactionId + ")");
		} catch (MalformedURLException e) {
			logger.error("MalformedURLException while calling Preparepayment" +e.getMessage());
			  throw new IllegalStateException(
			  "call Preparepayment Failed  - unable to process transaction type ("
			  + transactionId + ")");
			 
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			logger.error("ClientProtocolException while calling Preparepayment" +e.getMessage());
			  throw new IllegalStateException(
			  "call Preparepayment Failed  - unable to process transaction type ("
			  + transactionId + ")");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("IOException while calling Preparepayment" +e.getMessage());
			  throw new IllegalStateException(
			 "call Preparepayment Failed  - unable to process transaction type ("
			  + transactionId + ")");
		}
		logger.info("****end of callPreparePayment()--> Call to OSL-PreparePayment API Rest Call->preparePaymentResponse:"+preparePaymentResponse+"****");
		return preparePaymentResponse;
	}
	
	
	public CancelTransactionResponse processTransactionCancelation(boolean isManualTransaction, String csrUserid, long transactionId) {
	    logger.info("Preparing to call OSL for transaction cancellation");
		DefaultHttpClient client = new DefaultHttpClient();
		HttpPost request = new HttpPost(oslUrl + OSL_CANCEL_TRANSACTION_API);
		request.addHeader("Accept", "application/json");
		request.addHeader("Content-type", "application/json");
		CancelTransactionRequest reqEntity = new CancelTransactionRequest();
		reqEntity.setSourceApplication(SOURCE_APPLICATION);
		reqEntity.setTransactionId(transactionId);
		reqEntity.setCsrUserid(csrUserid);
		
		reqEntity.setSkipACReversal(Boolean.valueOf(isManualTransaction));
		reqEntity.setReasonCode(CancelReasonCode.OTHER);
		reqEntity.setCancellationDescription(CSR_CANCEL_REASON_DESC);
		ObjectMapper writerMapper = new ObjectMapper();
		CancelTransactionResponse response = null;
		try {
			String payload = writerMapper.writeValueAsString(reqEntity);
			request.setEntity(new StringEntity(payload));
			HttpResponse resp = client.execute(request);
			HttpEntity respEntity = resp.getEntity();
			ObjectMapper reader = new ObjectMapper();			
			response = reader.readValue(EntityUtils.toString(respEntity), CancelTransactionResponse.class);
		} catch (JsonProcessingException e) {
			logger.error("Error writing JSON String with provided params : { UserId: " 
														+ ESAPI.encoder().encodeForHTMLAttribute(csrUserid) 
														+ " transactionid: " 
														+ ESAPI.encoder().encodeForHTMLAttribute(String.valueOf(transactionId)) 
														+ " isManualCancelButtonClicked: " 
														+ ESAPI.encoder().encodeForHTMLAttribute(String.valueOf(isManualTransaction)) + "} : " + e.getMessage());
		} catch (UnsupportedEncodingException e) {
			// this needs to be caught since compiler forces us to but really shouldn't ever happen as string created is plain text.
			logger.error("Programming error, provided JSON string doesn't match charset of apache HttpPost object : " + e.getMessage());
		} catch (ClientProtocolException e) {
			logger.error("OSL HTTP protocol error : "+ e.getMessage());
		} catch (IOException e) {
			logger.error("OSL connection problem : " + e.getMessage());
		}
		return response;
	}
}
