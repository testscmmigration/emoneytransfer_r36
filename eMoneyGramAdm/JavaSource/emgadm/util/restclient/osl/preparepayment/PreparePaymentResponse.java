
package emgadm.util.restclient.osl.preparepayment;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "australiaBpay",
    "errorVO",
    "redirectUrl",
    "status"
})
public class PreparePaymentResponse {

    @JsonProperty("australiaBpay")
    private AustraliaBpay australiaBpay;
    @JsonProperty("errorVO")
    private ErrorVO errorVO;
    @JsonProperty("redirectUrl")
    private String redirectUrl;
    @JsonProperty("status")
    private Integer status;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("australiaBpay")
    public AustraliaBpay getAustraliaBpay() {
        return australiaBpay;
    }

    @JsonProperty("australiaBpay")
    public void setAustraliaBpay(AustraliaBpay australiaBpay) {
        this.australiaBpay = australiaBpay;
    }

    @JsonProperty("errorVO")
    public ErrorVO getErrorVO() {
        return errorVO;
    }

    @JsonProperty("errorVO")
    public void setErrorVO(ErrorVO errorVO) {
        this.errorVO = errorVO;
    }

    @JsonProperty("redirectUrl")
    public String getRedirectUrl() {
        return redirectUrl;
    }

    @JsonProperty("redirectUrl")
    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    @JsonProperty("status")
    public Integer getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(Integer status) {
        this.status = status;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
