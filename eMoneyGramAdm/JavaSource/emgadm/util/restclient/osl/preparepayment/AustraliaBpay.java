
package emgadm.util.restclient.osl.preparepayment;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "billerId",
    "customerPaymentReference"
})
public class AustraliaBpay {

    @JsonProperty("billerId")
    private String billerId;
    @JsonProperty("customerPaymentReference")
    private String customerPaymentReference;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("billerId")
    public String getBillerId() {
        return billerId;
    }

    @JsonProperty("billerId")
    public void setBillerId(String billerId) {
        this.billerId = billerId;
    }

    @JsonProperty("customerPaymentReference")
    public String getCustomerPaymentReference() {
        return customerPaymentReference;
    }

    @JsonProperty("customerPaymentReference")
    public void setCustomerPaymentReference(String customerPaymentReference) {
        this.customerPaymentReference = customerPaymentReference;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
