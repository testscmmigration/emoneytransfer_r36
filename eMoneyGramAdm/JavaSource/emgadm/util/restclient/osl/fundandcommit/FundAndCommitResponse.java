
package emgadm.util.restclient.osl.fundandcommit;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "errorVO",
    "referenceNbr",
    "status"
})
public class FundAndCommitResponse {

    @JsonProperty("errorVO")
    private ErrorVO errorVO;
    @JsonProperty("referenceNbr")
    private String referenceNbr;
    @JsonProperty("status")
    private Integer status;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("errorVO")
    public ErrorVO getErrorVO() {
        return errorVO;
    }

    @JsonProperty("errorVO")
    public void setErrorVO(ErrorVO errorVO) {
        this.errorVO = errorVO;
    }

    @JsonProperty("referenceNbr")
    public String getReferenceNbr() {
        return referenceNbr;
    }

    @JsonProperty("referenceNbr")
    public void setReferenceNbr(String referenceNbr) {
        this.referenceNbr = referenceNbr;
    }

    @JsonProperty("status")
    public Integer getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(Integer status) {
        this.status = status;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("errorVO", errorVO).append("referenceNbr", referenceNbr).append("status", status).append("additionalProperties", additionalProperties).toString();
    }

}
