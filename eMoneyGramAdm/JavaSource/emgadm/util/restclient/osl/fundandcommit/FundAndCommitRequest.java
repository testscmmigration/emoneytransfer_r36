
package emgadm.util.restclient.osl.fundandcommit;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "clientRequestId",
    "clientSessionId",
    "sourceApplication",
    "transactionId"
})
public class FundAndCommitRequest {

    @JsonProperty("clientRequestId")
    private String clientRequestId;
    @JsonProperty("clientSessionId")
    private String clientSessionId;
    @JsonProperty("sourceApplication")
    private String sourceApplication;
    @JsonProperty("transactionId")
    private String transactionId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("clientRequestId")
    public String getClientRequestId() {
        return clientRequestId;
    }

    @JsonProperty("clientRequestId")
    public void setClientRequestId(String clientRequestId) {
        this.clientRequestId = clientRequestId;
    }

    @JsonProperty("clientSessionId")
    public String getClientSessionId() {
        return clientSessionId;
    }

    @JsonProperty("clientSessionId")
    public void setClientSessionId(String clientSessionId) {
        this.clientSessionId = clientSessionId;
    }

    @JsonProperty("sourceApplication")
    public String getSourceApplication() {
        return sourceApplication;
    }

    @JsonProperty("sourceApplication")
    public void setSourceApplication(String sourceApplication) {
        this.sourceApplication = sourceApplication;
    }

    @JsonProperty("transactionId")
    public String getTransactionId() {
        return transactionId;
    }

    @JsonProperty("transactionId")
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("clientRequestId", clientRequestId).append("clientSessionId", clientSessionId).append("sourceApplication", sourceApplication).append("transactionId", transactionId).append("additionalProperties", additionalProperties).toString();
    }

}
