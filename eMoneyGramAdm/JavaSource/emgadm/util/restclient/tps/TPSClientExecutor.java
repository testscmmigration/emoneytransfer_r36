/**
 * 
 */
package emgadm.util.restclient.tps;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


import emgadm.property.EMTAdmContainerProperties;
import emgadm.util.restclient.tps.cancel.TPSCancelRequest;
import emgadm.util.restclient.tps.cancel.TPSCancelResponse;
import emgadm.util.restclient.tps.capture.TPSCaptureRequest;
import emgadm.util.restclient.tps.capture.TPSCaptureResponse;
import engadm.util.restclient.osl.canceltransaction.CancelTransactionRequest.CancelReasonCode;

/**  
 * @author bok0
 * 
 */
public class TPSClientExecutor {

	final static Logger logger = Logger.getLogger(TPSClientExecutor.class);

	private final String tpsUrl = EMTAdmContainerProperties.getTpsEndPoint();
	private final String TPS_SERVICE_CANCEL_END_POINT = "/cancel";
	private final String TPS_SERVICE_CAPTURE_END_POINT = "/capture";
	private final String CSR_CANCEL_REASON_DESC ="CSR Canceled"; 
	private final String SOURCE_APPLICATION = "EMT";
	
	private final String TPS_SERVICE_COMMIT_END_POINT = "/commit";


	public TPSCancelResponse processCancelTransaction(Long transactionid) {

		DefaultHttpClient httpClient = new DefaultHttpClient();
		String endPointUrl  = tpsUrl + TPS_SERVICE_CANCEL_END_POINT;
		logger.info("endPointUrl"+endPointUrl);
		HttpPost httpPost = new HttpPost(endPointUrl);
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Content-type", "application/json");
		HttpResponse httpResponse = null;

		String apiResponse = null;
		String apiRequest = null;
		TPSCancelResponse tpsCancelResponse = null;
		TPSCancelRequest tpsCancelRequest = new TPSCancelRequest();
		tpsCancelRequest.setSourceApplication(SOURCE_APPLICATION);
		tpsCancelRequest.setTransactionId(transactionid);
		ObjectMapper objectMapperReader = null;
		ObjectMapper objectMappeWriter = null;
		StringEntity stringEntity = null;
		try {
			objectMapperReader = new ObjectMapper();
			objectMappeWriter = new ObjectMapper();
			apiRequest = objectMappeWriter.writeValueAsString(tpsCancelRequest);
			stringEntity = new StringEntity(apiRequest);
			httpPost.setEntity(stringEntity);
			httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			apiResponse = EntityUtils.toString(httpEntity);
			logger.info("apiResponse" + ESAPI.encoder().encodeForHTMLAttribute(apiResponse));
			tpsCancelResponse = objectMapperReader.readValue(apiResponse,
					TPSCancelResponse.class);
		} 
		
		catch (JsonParseException jsonParseException) {
			// TODO Auto-generated catch block
			logger.error("JsonParseException, while calling Preparepayment"
					+ jsonParseException.getMessage());

		}
		
		catch (JsonMappingException jsonMappingException) {
			// TODO Auto-generated catch block
			logger.error("JsonMappingException while calling Preparepayment"
					+ jsonMappingException.getMessage());
		}
		
		catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			logger.error("UnsupportedEncodingException while calling Preparepayment"
					+ e.getMessage());

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			logger.error("ClientProtocolException while calling Preparepayment"
					+ e.getMessage());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("IOException while calling Preparepayment" + e.getMessage());

		}

		return tpsCancelResponse;
	}
	
	
	
	public TPSCancelResponse processTransactionCancelation(boolean isManualTransaction, String csrUserid, long transactionId) {
	    logger.info("Preparing to call TPS for transaction cancellation");
		DefaultHttpClient client = new DefaultHttpClient();
		HttpPost request = new HttpPost(tpsUrl + TPS_SERVICE_CANCEL_END_POINT);
		request.addHeader("Accept", "application/json");
		request.addHeader("Content-type", "application/json");
		TPSCancelRequest reqEntity = new TPSCancelRequest();
		reqEntity.setSourceApplication(SOURCE_APPLICATION);
		reqEntity.setTransactionId(transactionId);
		reqEntity.setCsrUserid(csrUserid);
        reqEntity.setReasonCode(CancelReasonCode.OTHER.toString());
        reqEntity.setCancellationDescription(CSR_CANCEL_REASON_DESC);
		reqEntity.setSkipACReversal(Boolean.valueOf(isManualTransaction));
		ObjectMapper writerMapper = new ObjectMapper();
		TPSCancelResponse response = null;
		try {
			String payload = writerMapper.writeValueAsString(reqEntity);
			request.setEntity(new StringEntity(payload));
			HttpResponse resp = client.execute(request);
			HttpEntity respEntity = resp.getEntity();
			ObjectMapper reader = new ObjectMapper();
			logger.info("Sending request to TPS "+request.getURI()+" Payload :"+payload);
			
			response = reader.readValue(EntityUtils.toString(respEntity), TPSCancelResponse.class);
			if(null != response && null != response.getErrorVO())
			{
				logger.info("Response from TPS service :"
						+ " Status = " + response.getStatus()
						+ ", Error Code = "+ response.getErrorVO().getErrorCode()
						+ ", Error Message = " + response.getErrorVO().getDeveloperMessage());
			}
		} catch (JsonProcessingException e) {
			logger.error("Error writing JSON String with provided params : { UserId: " 
														+ ESAPI.encoder().encodeForHTMLAttribute(csrUserid) 
														+ " transactionid: " 
														+ ESAPI.encoder().encodeForHTMLAttribute(String.valueOf(transactionId)) 
														+ " isManualCancelButtonClicked: " 
														+ ESAPI.encoder().encodeForHTMLAttribute(String.valueOf(isManualTransaction)) + "} : " + e.getMessage());
		} catch (UnsupportedEncodingException e) {
			// this needs to be caught since compiler forces us to but really shouldn't ever happen as string created is plain text.
			logger.error("Programming error, provided JSON string doesn't match charset of apache HttpPost object : " + e.getMessage());
		} catch (ClientProtocolException e) {
			logger.error("TPS HTTP protocol error : "+ e.getMessage());
		} catch (IOException e) {
			logger.error("TPS connection problem : " + e.getMessage());
		}
		return response;
	}
	
	public FundAndCommitResponseVO processCommitTransaction(String sourceSiteId, int transactionId) {
		logger.info("Preparing to call TPS for transaction commit");
		DefaultHttpClient client = new DefaultHttpClient();
		HttpPost request = new HttpPost(tpsUrl + TPS_SERVICE_COMMIT_END_POINT);
		request.addHeader("Accept", "application/json");
		request.addHeader("Content-type", "application/json");
		FundAndCommitRequestVO reqEntity = new FundAndCommitRequestVO();
		reqEntity.setSourceApplication(SOURCE_APPLICATION);
		reqEntity.setTransactionId(String.valueOf(transactionId));
		ObjectMapper writerMapper = new ObjectMapper();
		FundAndCommitResponseVO response = null;
		try {
			String payload = writerMapper.writeValueAsString(reqEntity);
			request.setEntity(new StringEntity(payload));
			HttpResponse resp = client.execute(request);
			HttpEntity respEntity = resp.getEntity();
			ObjectMapper reader = new ObjectMapper();
			logger.info("Sending request to TPS "+request.getURI()+" [{ transactionId: "
				+ ESAPI.encoder().encodeForHTMLAttribute(String.valueOf(reqEntity.getTransactionId())) 
				+ ", sourceApplication : " + reqEntity.getSourceApplication()
				+" }]");
			
			response = reader.readValue(EntityUtils.toString(respEntity), FundAndCommitResponseVO.class);
			if(null != response && null != response.getErrorVO())
			{
				logger.info("Response from TPS service :"
						+ " Status = " + response.getStatus()
						+ ", Error Code = "+ response.getErrorVO().getErrorCode()
						+ ", Error Message = " + response.getErrorVO().getDeveloperMessage());
			}
		} catch (JsonProcessingException e) {
			logger.error("Error writing JSON String with provided params : { transactionid: " 
														+ ESAPI.encoder().encodeForHTMLAttribute(String.valueOf(transactionId)) 
														+ "} : " + e.getMessage());
		} catch (UnsupportedEncodingException e) {
			// this needs to be caught since compiler forces us to but really shouldn't ever happen as string created is plain text.
			logger.error("Programming error, provided JSON string doesn't match charset of apache HttpPost object : " + e.getMessage());
		} catch (ClientProtocolException e) {
			logger.error("TPS HTTP protocol error : "+ e.getMessage());
		} catch (IOException e) {
			logger.error("TPS connection problem : " + e.getMessage());
		}
	return response;
	}
	
	public TPSCaptureResponse captureTransaction(long emgTranId, String loginId){

	    logger.info("Preparing to call TPS for transaction cancellation");
		DefaultHttpClient client = new DefaultHttpClient();
		HttpPost request = new HttpPost(tpsUrl + TPS_SERVICE_CAPTURE_END_POINT);
		request.addHeader("Accept", "application/json");
		request.addHeader("Content-type", "application/json");
		TPSCaptureRequest reqEntity = new TPSCaptureRequest();
		reqEntity.setSourceApplication(SOURCE_APPLICATION);
		reqEntity.setEmgTransactionId(emgTranId);
		reqEntity.setCsrLoginId(loginId);

		ObjectMapper writerMapper = new ObjectMapper();
		TPSCaptureResponse response = null;
		try {
			String payload = writerMapper.writeValueAsString(reqEntity);
			request.setEntity(new StringEntity(payload));
			logger.info("Sending request to TPS "+request.getURI()+" [{ UserId: "
					+ESAPI.encoder().encodeForHTMLAttribute(reqEntity.getCsrLoginId()) 
					+ ", EmgtransactionId: " + ESAPI.encoder().encodeForHTMLAttribute(String.valueOf(reqEntity.getEmgTransactionId())) 
					+ ", sourceApplication : " + reqEntity.getSourceApplication()+" }]");
			HttpResponse resp = client.execute(request);
			HttpEntity respEntity = resp.getEntity();
			ObjectMapper reader = new ObjectMapper();		
			
			response = reader.readValue(EntityUtils.toString(respEntity), TPSCaptureResponse.class);
			if(null != response && null != response.getErrorVO())
			{
				logger.info("Response from TPS service :"
						+ " Status = " + response.getStatus()
						+ ", Error Code = "+ response.getErrorVO().getErrorCode()
						+ ", Error Message = " + response.getErrorVO().getDeveloperMessage());
			}
		} catch (JsonProcessingException e) {
			logger.error("Error writing JSON String with provided params : { UserId: " 
														+ ESAPI.encoder().encodeForHTMLAttribute(loginId) 
														+ " transactionid: " 
														+ ESAPI.encoder().encodeForHTMLAttribute(String.valueOf(emgTranId)) 
														+ "} : " + e.getMessage());
		} catch (UnsupportedEncodingException e) {
			// this needs to be caught since compiler forces us to but really shouldn't ever happen as string created is plain text.
			logger.error("Programming error, provided JSON string doesn't match charset of apache HttpPost object : " + e.getMessage());
		} catch (ClientProtocolException e) {
			logger.error("TPS HTTP protocol error : "+ e.getMessage());
		} catch (IOException e) {
			logger.error("TPS connection problem : " + e.getMessage());
		} catch(Exception e) {
			logger.error("Exception occurred : "+e.getMessage());
		}
		return response;	
	}
	
}
