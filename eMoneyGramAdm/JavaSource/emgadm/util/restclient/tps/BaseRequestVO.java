package emgadm.util.restclient.tps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * Added new Base class for EMT-3613
 * for addition of two parameters
 * clientRequestId and clientSessionId
 * 
 * 
 */
public class BaseRequestVO {
	private static Logger logger = LoggerFactory.getLogger(BaseRequestVO.class);

	private String clientRequestId;
	private String clientSessionId;

	public String getClientRequestId() {
		return clientRequestId;
	}

	public void setClientRequestId(String clientRequestId) {
		this.clientRequestId = clientRequestId;
		//FIX-ME: Remove after OSL log file includes clientRequestId as one of the logging keys
		logger.debug("clientRequestId : {}",clientRequestId);
	}

	public String getClientSessionId() {
		return clientSessionId;
	}

	public void setClientSessionId(String clientSessionId) {
		this.clientSessionId = clientSessionId;
		//FIX-ME: Remove after OSL log file includes clientSessionId as one of the logging keys
		logger.debug("clientSessionId : {}",clientSessionId);
	}

}
