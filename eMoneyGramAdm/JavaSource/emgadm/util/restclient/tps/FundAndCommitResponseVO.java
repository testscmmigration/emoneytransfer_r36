package emgadm.util.restclient.tps;


public class FundAndCommitResponseVO {

	private String referenceNbr;
	
	private ServiceErrorVO errorVO;
	
	private int status;
	//MBO-11759
	private String confirmationNbr;
	
	

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getReferenceNbr() {
		return referenceNbr;
	}

	public void setReferenceNbr(String referenceNbr) {
		this.referenceNbr = referenceNbr;
	}

	public ServiceErrorVO getErrorVO() {
		return errorVO;
	}

	public void setErrorVO(ServiceErrorVO errorVO) {
		this.errorVO = errorVO;
	}

	/**
	 * @return the confirmationNbr
	 */
	public String getConfirmationNbr() {
		return confirmationNbr;
	}

	/**
	 * @param confirmationNbr the confirmationNbr to set
	 */
	public void setConfirmationNbr(String confirmationNbr) {
		this.confirmationNbr = confirmationNbr;
	}
	
	
}
