package emgadm.util.restclient.tps;

import emgadm.util.restclient.tps.NameValuePair;
import java.util.List;

//EMT-3613 - extending ClientHeaderVO for clientRequestId and clientSessionId
public class FundAndCommitRequestVO extends BaseRequestVO {
	
	private String transactionId;
	private String sourceApplication;

	private List<NameValuePair> fieldValues;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getSourceApplication() {
		return sourceApplication;
	}

	public void setSourceApplication(String sourceApplication) {
		this.sourceApplication = sourceApplication;
	}

	public List<NameValuePair> getFieldValues() {
		return fieldValues;
	}

	public void setFieldValues(List<NameValuePair> fieldValues) {
		this.fieldValues = fieldValues;
	}
}
