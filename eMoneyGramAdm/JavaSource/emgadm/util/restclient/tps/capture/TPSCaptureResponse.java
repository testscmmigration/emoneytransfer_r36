package emgadm.util.restclient.tps.capture;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import emgadm.util.restclient.osl.fundandcommit.ErrorVO;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TPSCaptureResponse {
	@JsonProperty("errorVO")	
	private ErrorVO errorVO;
	
	@JsonProperty("status")
	private Integer status;
	
	@JsonProperty("errorVO")
	public ErrorVO getErrorVO() {
		return errorVO;
	}

	@JsonProperty("errorVO")
	public void setErrorVO(ErrorVO errorVO) {
		this.errorVO = errorVO;
	}

	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(Integer status) {
		this.status = status;
	}


}
