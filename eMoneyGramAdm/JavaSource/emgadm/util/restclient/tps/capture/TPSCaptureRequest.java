package emgadm.util.restclient.tps.capture;

public class TPSCaptureRequest {
	
	private String clientRequestId;
	private String clientSessionId;
	private String csrLoginId;
	private long emgTransactionId;
	private String sourceApplication;
	
	public String getClientRequestId() {
		return clientRequestId;
	}
	public void setClientRequestId(String clientRequestId) {
		this.clientRequestId = clientRequestId;
	}
	public String getClientSessionId() {
		return clientSessionId;
	}
	public void setClientSessionId(String clientSessionId) {
		this.clientSessionId = clientSessionId;
	}
	public String getCsrLoginId() {
		return csrLoginId;
	}
	public void setCsrLoginId(String csrLoginId) {
		this.csrLoginId = csrLoginId;
	}
	public long getEmgTransactionId() {
		return emgTransactionId;
	}
	public void setEmgTransactionId(long emgTransactionId) {
		this.emgTransactionId = emgTransactionId;
	}
	public String getSourceApplication() {
		return sourceApplication;
	}
	public void setSourceApplication(String sourceApplication) {
		this.sourceApplication = sourceApplication;
	}

}
