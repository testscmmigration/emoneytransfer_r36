package emgadm.util.restclient.tps.cancel;

public class RequiredFields {
	private String accountNumber;
    private String billerAccountNumber;
    private String customerReceiveNumber;
    private String direction1;
    private String direction2;
    private String direction3;
    private boolean marketingOptInprivate;
    private String messageField1;
    private String messageField2;
    private String mgiRewardsNumber;
    private String receiverAddress;
    private String receiverAddress2;
    private String receiverAddress3;
    private String receiverCity;
    private String receiverFirstName;
    private String receiverLastName;
    private String receiverLastName2;
    private String receiverMiddleName;
    private String receiverPhone;
    private String receiverZipCode;
    private String senderBirthCity;
    private String senderLegalIdIssueCountry;
    private String senderLegalIdNumber;
    private String senderLegalIdType;
    private String senderNationalityAtBirthCountry;
    private String senderNationalityCountry;
    private String senderOccupation;
    private String senderPassportIssueCity;
    private String senderPassportIssueCountry;
    private String senderPassportIssueDate;
    private String senderPhotoIdIssueDate;
    private boolean senderPhotoIdStored;
    private String thirdPartyAddress;
    private String thirdPartyAddress2;
    private String thirdPartyAddress3;
    private String thirdPartyCity;
    private String thirdPartyCountry;
    private String thirdPartyDOB;
    private String thirdPartyFirstName;
    private String thirdPartyLastName;
    private String thirdPartyLastName2;
    private String thirdPartyLegalIdIssueCountry;
    private String thirdPartyLegalIdNumber;
    private String thirdPartyLegalIdType;
    private String thirdPartyMiddleName;
    private String thirdPartyOccupation;
    private String thirdPartyOrg;
    private String thirdPartyPhotoIdIssueCountry;
    private String thirdPartyPhotoIdNumber;
    private String thirdPartyPhotoIdState;
    private String thirdPartyPhotoIdType;
    private String thirdPartyState;
    private String thirdPartyZipCode;
    private String validateAccountNumber;
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getBillerAccountNumber() {
		return billerAccountNumber;
	}
	public void setBillerAccountNumber(String billerAccountNumber) {
		this.billerAccountNumber = billerAccountNumber;
	}
	public String getCustomerReceiveNumber() {
		return customerReceiveNumber;
	}
	public void setCustomerReceiveNumber(String customerReceiveNumber) {
		this.customerReceiveNumber = customerReceiveNumber;
	}
	public String getDirection1() {
		return direction1;
	}
	public void setDirection1(String direction1) {
		this.direction1 = direction1;
	}
	public String getDirection2() {
		return direction2;
	}
	public void setDirection2(String direction2) {
		this.direction2 = direction2;
	}
	public String getDirection3() {
		return direction3;
	}
	public void setDirection3(String direction3) {
		this.direction3 = direction3;
	}
	public boolean isMarketingOptInprivate() {
		return marketingOptInprivate;
	}
	public void setMarketingOptInprivate(boolean marketingOptInprivate) {
		this.marketingOptInprivate = marketingOptInprivate;
	}
	public String getMessageField1() {
		return messageField1;
	}
	public void setMessageField1(String messageField1) {
		this.messageField1 = messageField1;
	}
	public String getMessageField2() {
		return messageField2;
	}
	public void setMessageField2(String messageField2) {
		this.messageField2 = messageField2;
	}
	public String getMgiRewardsNumber() {
		return mgiRewardsNumber;
	}
	public void setMgiRewardsNumber(String mgiRewardsNumber) {
		this.mgiRewardsNumber = mgiRewardsNumber;
	}
	public String getReceiverAddress() {
		return receiverAddress;
	}
	public void setReceiverAddress(String receiverAddress) {
		this.receiverAddress = receiverAddress;
	}
	public String getReceiverAddress2() {
		return receiverAddress2;
	}
	public void setReceiverAddress2(String receiverAddress2) {
		this.receiverAddress2 = receiverAddress2;
	}
	public String getReceiverAddress3() {
		return receiverAddress3;
	}
	public void setReceiverAddress3(String receiverAddress3) {
		this.receiverAddress3 = receiverAddress3;
	}
	public String getReceiverCity() {
		return receiverCity;
	}
	public void setReceiverCity(String receiverCity) {
		this.receiverCity = receiverCity;
	}
	public String getReceiverFirstName() {
		return receiverFirstName;
	}
	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}
	public String getReceiverLastName() {
		return receiverLastName;
	}
	public void setReceiverLastName(String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}
	public String getReceiverLastName2() {
		return receiverLastName2;
	}
	public void setReceiverLastName2(String receiverLastName2) {
		this.receiverLastName2 = receiverLastName2;
	}
	public String getReceiverMiddleName() {
		return receiverMiddleName;
	}
	public void setReceiverMiddleName(String receiverMiddleName) {
		this.receiverMiddleName = receiverMiddleName;
	}
	public String getReceiverPhone() {
		return receiverPhone;
	}
	public void setReceiverPhone(String receiverPhone) {
		this.receiverPhone = receiverPhone;
	}
	public String getReceiverZipCode() {
		return receiverZipCode;
	}
	public void setReceiverZipCode(String receiverZipCode) {
		this.receiverZipCode = receiverZipCode;
	}
	public String getSenderBirthCity() {
		return senderBirthCity;
	}
	public void setSenderBirthCity(String senderBirthCity) {
		this.senderBirthCity = senderBirthCity;
	}
	public String getSenderLegalIdIssueCountry() {
		return senderLegalIdIssueCountry;
	}
	public void setSenderLegalIdIssueCountry(String senderLegalIdIssueCountry) {
		this.senderLegalIdIssueCountry = senderLegalIdIssueCountry;
	}
	public String getSenderLegalIdNumber() {
		return senderLegalIdNumber;
	}
	public void setSenderLegalIdNumber(String senderLegalIdNumber) {
		this.senderLegalIdNumber = senderLegalIdNumber;
	}
	public String getSenderLegalIdType() {
		return senderLegalIdType;
	}
	public void setSenderLegalIdType(String senderLegalIdType) {
		this.senderLegalIdType = senderLegalIdType;
	}
	public String getSenderNationalityAtBirthCountry() {
		return senderNationalityAtBirthCountry;
	}
	public void setSenderNationalityAtBirthCountry(
			String senderNationalityAtBirthCountry) {
		this.senderNationalityAtBirthCountry = senderNationalityAtBirthCountry;
	}
	public String getSenderNationalityCountry() {
		return senderNationalityCountry;
	}
	public void setSenderNationalityCountry(String senderNationalityCountry) {
		this.senderNationalityCountry = senderNationalityCountry;
	}
	public String getSenderOccupation() {
		return senderOccupation;
	}
	public void setSenderOccupation(String senderOccupation) {
		this.senderOccupation = senderOccupation;
	}
	public String getSenderPassportIssueCity() {
		return senderPassportIssueCity;
	}
	public void setSenderPassportIssueCity(String senderPassportIssueCity) {
		this.senderPassportIssueCity = senderPassportIssueCity;
	}
	public String getSenderPassportIssueCountry() {
		return senderPassportIssueCountry;
	}
	public void setSenderPassportIssueCountry(String senderPassportIssueCountry) {
		this.senderPassportIssueCountry = senderPassportIssueCountry;
	}
	public String getSenderPassportIssueDate() {
		return senderPassportIssueDate;
	}
	public void setSenderPassportIssueDate(String senderPassportIssueDate) {
		this.senderPassportIssueDate = senderPassportIssueDate;
	}
	public String getSenderPhotoIdIssueDate() {
		return senderPhotoIdIssueDate;
	}
	public void setSenderPhotoIdIssueDate(String senderPhotoIdIssueDate) {
		this.senderPhotoIdIssueDate = senderPhotoIdIssueDate;
	}
	public boolean isSenderPhotoIdStored() {
		return senderPhotoIdStored;
	}
	public void setSenderPhotoIdStored(boolean senderPhotoIdStored) {
		this.senderPhotoIdStored = senderPhotoIdStored;
	}
	public String getThirdPartyAddress() {
		return thirdPartyAddress;
	}
	public void setThirdPartyAddress(String thirdPartyAddress) {
		this.thirdPartyAddress = thirdPartyAddress;
	}
	public String getThirdPartyAddress2() {
		return thirdPartyAddress2;
	}
	public void setThirdPartyAddress2(String thirdPartyAddress2) {
		this.thirdPartyAddress2 = thirdPartyAddress2;
	}
	public String getThirdPartyAddress3() {
		return thirdPartyAddress3;
	}
	public void setThirdPartyAddress3(String thirdPartyAddress3) {
		this.thirdPartyAddress3 = thirdPartyAddress3;
	}
	public String getThirdPartyCity() {
		return thirdPartyCity;
	}
	public void setThirdPartyCity(String thirdPartyCity) {
		this.thirdPartyCity = thirdPartyCity;
	}
	public String getThirdPartyCountry() {
		return thirdPartyCountry;
	}
	public void setThirdPartyCountry(String thirdPartyCountry) {
		this.thirdPartyCountry = thirdPartyCountry;
	}
	public String getThirdPartyDOB() {
		return thirdPartyDOB;
	}
	public void setThirdPartyDOB(String thirdPartyDOB) {
		this.thirdPartyDOB = thirdPartyDOB;
	}
	public String getThirdPartyFirstName() {
		return thirdPartyFirstName;
	}
	public void setThirdPartyFirstName(String thirdPartyFirstName) {
		this.thirdPartyFirstName = thirdPartyFirstName;
	}
	public String getThirdPartyLastName() {
		return thirdPartyLastName;
	}
	public void setThirdPartyLastName(String thirdPartyLastName) {
		this.thirdPartyLastName = thirdPartyLastName;
	}
	public String getThirdPartyLastName2() {
		return thirdPartyLastName2;
	}
	public void setThirdPartyLastName2(String thirdPartyLastName2) {
		this.thirdPartyLastName2 = thirdPartyLastName2;
	}
	public String getThirdPartyLegalIdIssueCountry() {
		return thirdPartyLegalIdIssueCountry;
	}
	public void setThirdPartyLegalIdIssueCountry(
			String thirdPartyLegalIdIssueCountry) {
		this.thirdPartyLegalIdIssueCountry = thirdPartyLegalIdIssueCountry;
	}
	public String getThirdPartyLegalIdNumber() {
		return thirdPartyLegalIdNumber;
	}
	public void setThirdPartyLegalIdNumber(String thirdPartyLegalIdNumber) {
		this.thirdPartyLegalIdNumber = thirdPartyLegalIdNumber;
	}
	public String getThirdPartyLegalIdType() {
		return thirdPartyLegalIdType;
	}
	public void setThirdPartyLegalIdType(String thirdPartyLegalIdType) {
		this.thirdPartyLegalIdType = thirdPartyLegalIdType;
	}
	public String getThirdPartyMiddleName() {
		return thirdPartyMiddleName;
	}
	public void setThirdPartyMiddleName(String thirdPartyMiddleName) {
		this.thirdPartyMiddleName = thirdPartyMiddleName;
	}
	public String getThirdPartyOccupation() {
		return thirdPartyOccupation;
	}
	public void setThirdPartyOccupation(String thirdPartyOccupation) {
		this.thirdPartyOccupation = thirdPartyOccupation;
	}
	public String getThirdPartyOrg() {
		return thirdPartyOrg;
	}
	public void setThirdPartyOrg(String thirdPartyOrg) {
		this.thirdPartyOrg = thirdPartyOrg;
	}
	public String getThirdPartyPhotoIdIssueCountry() {
		return thirdPartyPhotoIdIssueCountry;
	}
	public void setThirdPartyPhotoIdIssueCountry(
			String thirdPartyPhotoIdIssueCountry) {
		this.thirdPartyPhotoIdIssueCountry = thirdPartyPhotoIdIssueCountry;
	}
	public String getThirdPartyPhotoIdNumber() {
		return thirdPartyPhotoIdNumber;
	}
	public void setThirdPartyPhotoIdNumber(String thirdPartyPhotoIdNumber) {
		this.thirdPartyPhotoIdNumber = thirdPartyPhotoIdNumber;
	}
	public String getThirdPartyPhotoIdState() {
		return thirdPartyPhotoIdState;
	}
	public void setThirdPartyPhotoIdState(String thirdPartyPhotoIdState) {
		this.thirdPartyPhotoIdState = thirdPartyPhotoIdState;
	}
	public String getThirdPartyPhotoIdType() {
		return thirdPartyPhotoIdType;
	}
	public void setThirdPartyPhotoIdType(String thirdPartyPhotoIdType) {
		this.thirdPartyPhotoIdType = thirdPartyPhotoIdType;
	}
	public String getThirdPartyState() {
		return thirdPartyState;
	}
	public void setThirdPartyState(String thirdPartyState) {
		this.thirdPartyState = thirdPartyState;
	}
	public String getThirdPartyZipCode() {
		return thirdPartyZipCode;
	}
	public void setThirdPartyZipCode(String thirdPartyZipCode) {
		this.thirdPartyZipCode = thirdPartyZipCode;
	}
	public String getValidateAccountNumber() {
		return validateAccountNumber;
	}
	public void setValidateAccountNumber(String validateAccountNumber) {
		this.validateAccountNumber = validateAccountNumber;
	}

}
