package emgadm.util.restclient.tps.cancel;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

public class Transaction {
	private Integer accountId;
	private Integer addressId;
    private AustraliaBpay australiaBpay;
    private Integer authorizationId;
	private Biller biller;

	private Date dateCanceled; 
	private Date dateInitiated;
	private Date dateReadyForPickup;
	private Date dateReceived;
	private String deliveryOption; 
	private Integer deliveryOptionId; 
	private String deliveryOptionKey; 
	private String disclaimerTxtENG; 

	private Map<String, String> dynamicFields = new HashMap<String, String>();

	private Integer exchangeRate;
	private Integer id;
	private String mgiTransactionSessionId;
	private String mgoTransactionStatus;
	private MonerisPaymentInfo monerisPaymentInfo;
	private boolean onlineTransaction;
	private String paymentProduct;
	private String paymentProviderOrderId;
	private String productType;
    private Integer profileId;
    private String promoCode;
    private Integer promoDiscountAmount;
    private String promoDiscountType;
    private String receiptTranType;
    private String receiveAgentConfirmationNumber;
    private String receiveAgentId;
    private Integer receiveAmount;
    private String receiveCurrency;
    private Integer receiveFee;
    private String receiveOption;
    private Integer receiveTax;
    private Receiver receiver;
    private String receiverRegistrationNumber;
    private String referenceNumber;
    private String regulatoryAgencyNameENG;
    private String regulatoryAgencyNameSPA;
    private String regulatoryAgencyPhone;
    private String regulatoryAgencyUrl;
    private RequiredFields requiredFields;
    private Integer sendAmount;
    private String sendCountry;
    private String sendCurrency;
    private Integer sendFee;
    private Integer endTax;
    private String senderEmail;
    private String senderFirstName;
    private String senderLastName;
    private String senderMiddleName;
    private String senderSecondLastName;
    private String sourceSite;
    private String status;
    private String subStatus;
    private String threeMinuteFreePhoneNumber;
    private String threeMinuteFreePinNumber;
    private Integer totalReceiveAmount;
    private Integer totalSendAmount;
    private String transactionLanguage;
    private String transactionType;
    private boolean validCurrencyIndicator;
    
	public Integer getAuthorizationId() {
		return authorizationId;
	}
	public void setAuthorizationId(Integer authorizationId) {
		this.authorizationId = authorizationId;
	}
	public Biller getBiller() {
		return biller;
	}
	public void setBiller(Biller biller) {
		this.biller = biller;
	}
	public Date getDateCanceled() {
		return dateCanceled;
	}
	public void setDateCanceled(Date dateCanceled) {
		this.dateCanceled = dateCanceled;
	}
	public Date getDateInitiated() {
		return dateInitiated;
	}
	public void setDateInitiated(Date dateInitiated) {
		this.dateInitiated = dateInitiated;
	}
	public Date getDateReadyForPickup() {
		return dateReadyForPickup;
	}
	public void setDateReadyForPickup(Date dateReadyForPickup) {
		this.dateReadyForPickup = dateReadyForPickup;
	}
	public Date getDateReceived() {
		return dateReceived;
	}
	public void setDateReceived(Date dateReceived) {
		this.dateReceived = dateReceived;
	}
	public String getDeliveryOption() {
		return deliveryOption;
	}
	public void setDeliveryOption(String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}
	public Integer getDeliveryOptionId() {
		return deliveryOptionId;
	}
	public void setDeliveryOptionId(Integer deliveryOptionId) {
		this.deliveryOptionId = deliveryOptionId;
	}
	public String getDeliveryOptionKey() {
		return deliveryOptionKey;
	}
	public void setDeliveryOptionKey(String deliveryOptionKey) {
		this.deliveryOptionKey = deliveryOptionKey;
	}
	public String getDisclaimerTxtENG() {
		return disclaimerTxtENG;
	}
	public void setDisclaimerTxtENG(String disclaimerTxtENG) {
		this.disclaimerTxtENG = disclaimerTxtENG;
	}
	public Map<String, String> getDynamicFields() {
		return dynamicFields;
	}
	public void setDynamicFields(Map<String, String> dynamicFields) {
		this.dynamicFields = dynamicFields;
	}
	public Integer getExchangeRate() {
		return exchangeRate;
	}
	public void setExchangeRate(Integer exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMgiTransactionSessionId() {
		return mgiTransactionSessionId;
	}
	public void setMgiTransactionSessionId(String mgiTransactionSessionId) {
		this.mgiTransactionSessionId = mgiTransactionSessionId;
	}
	public String getMgoTransactionStatus() {
		return mgoTransactionStatus;
	}
	public void setMgoTransactionStatus(String mgoTransactionStatus) {
		this.mgoTransactionStatus = mgoTransactionStatus;
	}
	public MonerisPaymentInfo getMonerisPaymentInfo() {
		return monerisPaymentInfo;
	}
	public void setMonerisPaymentInfo(MonerisPaymentInfo monerisPaymentInfo) {
		this.monerisPaymentInfo = monerisPaymentInfo;
	}
	public boolean isOnlineTransaction() {
		return onlineTransaction;
	}
	public void setOnlineTransaction(boolean onlineTransaction) {
		this.onlineTransaction = onlineTransaction;
	}
	public String getPaymentProduct() {
		return paymentProduct;
	}
	public void setPaymentProduct(String paymentProduct) {
		this.paymentProduct = paymentProduct;
	}
	public String getPaymentProviderOrderId() {
		return paymentProviderOrderId;
	}
	public void setPaymentProviderOrderId(String paymentProviderOrderId) {
		this.paymentProviderOrderId = paymentProviderOrderId;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public Integer getProfileId() {
		return profileId;
	}
	public void setProfileId(Integer profileId) {
		this.profileId = profileId;
	}
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	public Integer getPromoDiscountAmount() {
		return promoDiscountAmount;
	}
	public void setPromoDiscountAmount(Integer promoDiscountAmount) {
		this.promoDiscountAmount = promoDiscountAmount;
	}
	public String getPromoDiscountType() {
		return promoDiscountType;
	}
	public void setPromoDiscountType(String promoDiscountType) {
		this.promoDiscountType = promoDiscountType;
	}
	public String getReceiptTranType() {
		return receiptTranType;
	}
	public void setReceiptTranType(String receiptTranType) {
		this.receiptTranType = receiptTranType;
	}
	public String getReceiveAgentConfirmationNumber() {
		return receiveAgentConfirmationNumber;
	}
	public void setReceiveAgentConfirmationNumber(
			String receiveAgentConfirmationNumber) {
		this.receiveAgentConfirmationNumber = receiveAgentConfirmationNumber;
	}
	public String getReceiveAgentId() {
		return receiveAgentId;
	}
	public void setReceiveAgentId(String receiveAgentId) {
		this.receiveAgentId = receiveAgentId;
	}
	public Integer getReceiveAmount() {
		return receiveAmount;
	}
	public void setReceiveAmount(Integer receiveAmount) {
		this.receiveAmount = receiveAmount;
	}
	public String getReceiveCurrency() {
		return receiveCurrency;
	}
	public void setReceiveCurrency(String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}
	public Integer getReceiveFee() {
		return receiveFee;
	}
	public void setReceiveFee(Integer receiveFee) {
		this.receiveFee = receiveFee;
	}
	public String getReceiveOption() {
		return receiveOption;
	}
	public void setReceiveOption(String receiveOption) {
		this.receiveOption = receiveOption;
	}
	public Integer getReceiveTax() {
		return receiveTax;
	}
	public void setReceiveTax(Integer receiveTax) {
		this.receiveTax = receiveTax;
	}
	public Receiver getReceiver() {
		return receiver;
	}
	public void setReceiver(Receiver receiver) {
		this.receiver = receiver;
	}
	public String getReceiverRegistrationNumber() {
		return receiverRegistrationNumber;
	}
	public void setReceiverRegistrationNumber(String receiverRegistrationNumber) {
		this.receiverRegistrationNumber = receiverRegistrationNumber;
	}
	public String getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	public String getRegulatoryAgencyNameENG() {
		return regulatoryAgencyNameENG;
	}
	public void setRegulatoryAgencyNameENG(String regulatoryAgencyNameENG) {
		this.regulatoryAgencyNameENG = regulatoryAgencyNameENG;
	}
	public String getRegulatoryAgencyNameSPA() {
		return regulatoryAgencyNameSPA;
	}
	public void setRegulatoryAgencyNameSPA(String regulatoryAgencyNameSPA) {
		this.regulatoryAgencyNameSPA = regulatoryAgencyNameSPA;
	}
	public String getRegulatoryAgencyPhone() {
		return regulatoryAgencyPhone;
	}
	public void setRegulatoryAgencyPhone(String regulatoryAgencyPhone) {
		this.regulatoryAgencyPhone = regulatoryAgencyPhone;
	}
	public String getRegulatoryAgencyUrl() {
		return regulatoryAgencyUrl;
	}
	public void setRegulatoryAgencyUrl(String regulatoryAgencyUrl) {
		this.regulatoryAgencyUrl = regulatoryAgencyUrl;
	}
	public RequiredFields getRequiredFields() {
		return requiredFields;
	}
	public void setRequiredFields(RequiredFields requiredFields) {
		this.requiredFields = requiredFields;
	}
	public Integer getSendAmount() {
		return sendAmount;
	}
	public void setSendAmount(Integer sendAmount) {
		this.sendAmount = sendAmount;
	}
	public String getSendCountry() {
		return sendCountry;
	}
	public void setSendCountry(String sendCountry) {
		this.sendCountry = sendCountry;
	}
	public String getSendCurrency() {
		return sendCurrency;
	}
	public void setSendCurrency(String sendCurrency) {
		this.sendCurrency = sendCurrency;
	}
	public Integer getSendFee() {
		return sendFee;
	}
	public void setSendFee(Integer sendFee) {
		this.sendFee = sendFee;
	}
	public Integer getEndTax() {
		return endTax;
	}
	public void setEndTax(Integer endTax) {
		this.endTax = endTax;
	}
	public String getSenderEmail() {
		return senderEmail;
	}
	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}
	public String getSenderFirstName() {
		return senderFirstName;
	}
	public void setSenderFirstName(String senderFirstName) {
		this.senderFirstName = senderFirstName;
	}
	public String getSenderLastName() {
		return senderLastName;
	}
	public void setSenderLastName(String senderLastName) {
		this.senderLastName = senderLastName;
	}
	public String getSenderMiddleName() {
		return senderMiddleName;
	}
	public void setSenderMiddleName(String senderMiddleName) {
		this.senderMiddleName = senderMiddleName;
	}
	public String getSenderSecondLastName() {
		return senderSecondLastName;
	}
	public void setSenderSecondLastName(String senderSecondLastName) {
		this.senderSecondLastName = senderSecondLastName;
	}
	public String getSourceSite() {
		return sourceSite;
	}
	public void setSourceSite(String sourceSite) {
		this.sourceSite = sourceSite;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSubStatus() {
		return subStatus;
	}
	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}
	public String getThreeMinuteFreePhoneNumber() {
		return threeMinuteFreePhoneNumber;
	}
	public void setThreeMinuteFreePhoneNumber(String threeMinuteFreePhoneNumber) {
		this.threeMinuteFreePhoneNumber = threeMinuteFreePhoneNumber;
	}
	public String getThreeMinuteFreePinNumber() {
		return threeMinuteFreePinNumber;
	}
	public void setThreeMinuteFreePinNumber(String threeMinuteFreePinNumber) {
		this.threeMinuteFreePinNumber = threeMinuteFreePinNumber;
	}
	public Integer getTotalReceiveAmount() {
		return totalReceiveAmount;
	}
	public void setTotalReceiveAmount(Integer totalReceiveAmount) {
		this.totalReceiveAmount = totalReceiveAmount;
	}
	public Integer getTotalSendAmount() {
		return totalSendAmount;
	}
	public void setTotalSendAmount(Integer totalSendAmount) {
		this.totalSendAmount = totalSendAmount;
	}
	public String getTransactionLanguage() {
		return transactionLanguage;
	}
	public void setTransactionLanguage(String transactionLanguage) {
		this.transactionLanguage = transactionLanguage;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public boolean isValidCurrencyIndicator() {
		return validCurrencyIndicator;
	}
	public void setValidCurrencyIndicator(boolean validCurrencyIndicator) {
		this.validCurrencyIndicator = validCurrencyIndicator;
	}
	public Integer getAccountId() {
		return accountId;
	}
	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}
	public Integer getAddressId() {
		return addressId;
	}
	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}
	public AustraliaBpay getAustraliaBpay() {
		return australiaBpay;
	}
	public void setAustraliaBpay(AustraliaBpay australiaBpay) {
		this.australiaBpay = australiaBpay;
	}
    

}
