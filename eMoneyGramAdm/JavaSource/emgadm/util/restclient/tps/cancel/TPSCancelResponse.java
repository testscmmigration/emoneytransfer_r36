package emgadm.util.restclient.tps.cancel;

import java.sql.Timestamp;
import emgadm.util.restclient.osl.fundandcommit.ErrorVO;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TPSCancelResponse {
	
	private Timestamp timestamp;
	
	@JsonProperty("error")
	private ErrorVO errorVO;
	
	@JsonIgnore
    private Error error;
	
    @JsonIgnore
    private Transaction transaction;

	@JsonProperty("status")
	private Integer status;

	@JsonAnyGetter
	public Transaction getTransaction() {
		return transaction;
	}
	
	@JsonAnyGetter
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	@JsonProperty("error")
	public ErrorVO getErrorVO() {
		return errorVO;
	}

	@JsonProperty("error")
	public void setErrorVO(ErrorVO errorVO) {
		this.errorVO = errorVO;
	}

	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(Integer status) {
		this.status = status;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

}
