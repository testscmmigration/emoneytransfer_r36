package emgadm.util.restclient.tps.cancel;

public class MonerisPaymentInfo {
	private String issuerConfirmationNumber;
	private String issuerName;
	public String getIssuerConfirmationNumber() {
		return issuerConfirmationNumber;
	}
	public void setIssuerConfirmationNumber(String issuerConfirmationNumber) {
		this.issuerConfirmationNumber = issuerConfirmationNumber;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

}
