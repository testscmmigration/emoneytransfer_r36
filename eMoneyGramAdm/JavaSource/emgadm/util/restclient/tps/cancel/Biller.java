package emgadm.util.restclient.tps.cancel;

public class Biller {
	private String accountNumber;
    private String address1;
    private boolean billerActive;
    private String billerCity;
    private String billerCountry;
    private String billerCutOffTime;
    private String billerName;
    private String billerNotes;
    private String billerState;
    private String expectedPostingTimeFrame;
    private Integer maximumAmount;
    private Integer minimumFeeAmount;
    private boolean onlineBillPaySupport;
    private String receiveCode;
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public boolean isBillerActive() {
		return billerActive;
	}
	public void setBillerActive(boolean billerActive) {
		this.billerActive = billerActive;
	}
	public String getBillerCity() {
		return billerCity;
	}
	public void setBillerCity(String billerCity) {
		this.billerCity = billerCity;
	}
	public String getBillerCountry() {
		return billerCountry;
	}
	public void setBillerCountry(String billerCountry) {
		this.billerCountry = billerCountry;
	}
	public String getBillerCutOffTime() {
		return billerCutOffTime;
	}
	public void setBillerCutOffTime(String billerCutOffTime) {
		this.billerCutOffTime = billerCutOffTime;
	}
	public String getBillerName() {
		return billerName;
	}
	public void setBillerName(String billerName) {
		this.billerName = billerName;
	}
	public String getBillerNotes() {
		return billerNotes;
	}
	public void setBillerNotes(String billerNotes) {
		this.billerNotes = billerNotes;
	}
	public String getBillerState() {
		return billerState;
	}
	public void setBillerState(String billerState) {
		this.billerState = billerState;
	}
	public String getExpectedPostingTimeFrame() {
		return expectedPostingTimeFrame;
	}
	public void setExpectedPostingTimeFrame(String expectedPostingTimeFrame) {
		this.expectedPostingTimeFrame = expectedPostingTimeFrame;
	}
	public Integer getMaximumAmount() {
		return maximumAmount;
	}
	public void setMaximumAmount(Integer maximumAmount) {
		this.maximumAmount = maximumAmount;
	}
	public Integer getMinimumFeeAmount() {
		return minimumFeeAmount;
	}
	public void setMinimumFeeAmount(Integer minimumFeeAmount) {
		this.minimumFeeAmount = minimumFeeAmount;
	}
	public boolean isOnlineBillPaySupport() {
		return onlineBillPaySupport;
	}
	public void setOnlineBillPaySupport(boolean onlineBillPaySupport) {
		this.onlineBillPaySupport = onlineBillPaySupport;
	}
	public String getReceiveCode() {
		return receiveCode;
	}
	public void setReceiveCode(String receiveCode) {
		this.receiveCode = receiveCode;
	}
    
}
