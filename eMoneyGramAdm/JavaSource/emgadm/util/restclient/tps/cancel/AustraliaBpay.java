package emgadm.util.restclient.tps.cancel;

public class AustraliaBpay {
	private String billerId;
	private String customerPaymentReference;
	public String getBillerId() {
		return billerId;
	}
	public void setBillerId(String billerId) {
		this.billerId = billerId;
	}
	public String getCustomerPaymentReference() {
		return customerPaymentReference;
	}
	public void setCustomerPaymentReference(String customerPaymentReference) {
		this.customerPaymentReference = customerPaymentReference;
	}

}
