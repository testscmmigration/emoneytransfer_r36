package emgadm.util.restclient.tps.cancel;

public class Receiver {
	private String city;
    private String country;
    private String firstName;
    private String hashedReceiverAccountNumber;
    private String lastName;
    private String middleName;
    private String receiverMTAccountNbrMask;
    private String state;
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getHashedReceiverAccountNumber() {
		return hashedReceiverAccountNumber;
	}
	public void setHashedReceiverAccountNumber(String hashedReceiverAccountNumber) {
		this.hashedReceiverAccountNumber = hashedReceiverAccountNumber;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getReceiverMTAccountNbrMask() {
		return receiverMTAccountNbrMask;
	}
	public void setReceiverMTAccountNbrMask(String receiverMTAccountNbrMask) {
		this.receiverMTAccountNbrMask = receiverMTAccountNbrMask;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

}
