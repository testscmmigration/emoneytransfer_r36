package emgadm.util.restclient.tps.cancel;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TPSCancelRequest {

	    private String cancellationDescription = "Customer requested Cancel";
	    private String clientRequestId;
		private String clientSessionId;
		private String csrUserid;
		private String reasonCode = "MONEY_NOT_NEEDED";
		private Boolean skipACReversal;
		private String sourceApplication;
		private Long transactionId;
		public String getCancellationDescription() {
			return cancellationDescription;
		}
		public void setCancellationDescription(String cancellationDescription) {
			this.cancellationDescription = cancellationDescription;
		}
		public String getClientRequestId() {
			return clientRequestId;
		}
		public void setClientRequestId(String clientRequestId) {
			this.clientRequestId = clientRequestId;
		}
		public String getClientSessionId() {
			return clientSessionId;
		}
		public void setClientSessionId(String clientSessionId) {
			this.clientSessionId = clientSessionId;
		}
		public String getCsrUserid() {
			return csrUserid;
		}
		public void setCsrUserid(String csrUserid) {
			this.csrUserid = csrUserid;
		}
		public String getReasonCode() {
			return reasonCode;
		}
		public void setReasonCode(String reasonCode) {
			this.reasonCode = reasonCode;
		}
		public Boolean getSkipACReversal() {
			return skipACReversal;
		}
		public void setSkipACReversal(Boolean skipACReversal) {
			this.skipACReversal = skipACReversal;
		}
		public String getSourceApplication() {
			return sourceApplication;
		}
		public void setSourceApplication(String sourceApplication) {
			this.sourceApplication = sourceApplication;
		}
		public Long getTransactionId() {
			return transactionId;
		}
		public void setTransactionId(Long transactionId) {
			this.transactionId = transactionId;
		}
		
}
