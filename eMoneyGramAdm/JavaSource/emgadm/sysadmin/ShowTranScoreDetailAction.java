package emgadm.sysadmin;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.Transaction;
import emgshared.services.CyberDMData;
import emgshared.services.ScoringData;

public class ShowTranScoreDetailAction extends EMoneyGramAdmBaseAction
{
	private static final Logger LOG = EMGSharedLogger.getLogger(
			ShowTranScoreDetailAction.class);
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		ShowTranScoreDetailForm form = (ShowTranScoreDetailForm) f;

		if (!StringHelper.isNullOrEmpty(form.getSubmitReturn()))
		{
			if ("q".equalsIgnoreCase(form.getType()))
			{
				return mapping.findForward(
					EMoneyGramAdmForwardConstants
						.LOCAL_FORWARD_SHOW_TRANS_QUEUE);
			} else
			{
				return mapping.findForward(
					EMoneyGramAdmForwardConstants
						.LOCAL_FORWARD_TRANSCTION_DETAIL);
			}
		}

		if (StringHelper.isNullOrEmpty(form.getTranId())
			|| StringHelper.containsNonDigits(form.getTranId()))
		{
			throw new EMGRuntimeException("Invalid Transaction Id");
		}

		TransactionManager tm = getTransactionManager(request);
		Transaction tran = null;
		tran = tm.getTransaction(Integer.parseInt(form.getTranId()));

		if (tran == null)
		{
			throw new EMGRuntimeException(
				"Transaction Id " + form.getTranId() + "' does not exist");
		}
		// EMT-2516
		if (tran.getTranAppVersionNumber() >= 3.0) {
			LOG.info("Going to fetch user id");
			LOG.info("Txn customer id is " + tran.getCustId());
			UserProfile up = getUserProfile(request);
			ScoringData actimizeScoreData = null;
			try{
				actimizeScoreData = tm.getActimizeTriggeredRules(
						Integer.parseInt(form.getTranId()), tran.getCreateDate());
			}catch(SQLException se){
				ActionErrors errors = new ActionErrors();
				errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("error.scoring.actimize.timeout"));
				saveErrors(request, errors);
			}
			request.setAttribute("actimizeScoreData", actimizeScoreData);
		}
		// ended
		//added by Ankit Bhatt for 285
		else if(tran.getTranScore()==null || tran.getTranScore().getScoreCnfgId() < 1) {
			LOG.info("Going to fetch user id");			
			LOG.info("Txn customer id is " +tran.getCustId());
			UserProfile up = getUserProfile(request);
			CyberDMData cyberDMData = tm.getCybersourceDMData(Integer.parseInt(form.getTranId()),up.getUID());
			request.setAttribute("cyberDMData", cyberDMData);
		}
		//ended
		
		request.setAttribute("tran", tran);
		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
