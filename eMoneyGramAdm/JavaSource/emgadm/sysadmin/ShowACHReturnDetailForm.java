package emgadm.sysadmin;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class ShowACHReturnDetailForm extends EMoneyGramAdmBaseValidatorForm
{
	private String achTranId;
	private String achStatusCode;
	private String submitReturn = null;
	private String submitSave = null;

	public String getSubmitReturn()
	{
		return submitReturn;
	}

	public void setSubmitReturn(String string)
	{
		submitReturn = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);
		submitReturn = null;
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		return errors;
	}

	public String getAchTranId() {
		return achTranId;
	}

	public void setAchTranId(String achTranId) {
		this.achTranId = achTranId;
	}

	public String getAchStatusCode() {
		return achStatusCode;
	}

	public void setAchStatusCode(String achStatusCode) {
		this.achStatusCode = achStatusCode;
	}

	/**
	 * @return Returns the submitSave.
	 */
	public String getSubmitSave() {
		return submitSave;
	}
	/**
	 * @param submitSave The submitSave to set.
	 */
	public void setSubmitSave(String submitSave) {
		this.submitSave = submitSave;
	}
}
