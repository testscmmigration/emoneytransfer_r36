package emgadm.sysadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.util.StringHelper;
import emgshared.dataaccessors.PropertyDAO;
import emgshared.model.PropertyBean;
import emgshared.model.PropertyKey;
import emgshared.property.EMTSharedDynProperties;

public class UpdateDynPropertyAction extends EMoneyGramAdmBaseAction
{

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		ActionErrors errors = new ActionErrors();
		UpdateDynPropertyForm form = (UpdateDynPropertyForm) f;

		errors = editData(request, form, errors);
		if (errors.size() == 0)
		{
			PropertyBean pb =
				new PropertyBean(
					new PropertyKey(
						form.getPropName(),
						null,
						form.getPropVal()),
					form.getPropDesc(),
					form.getAdminUseFlag(),
					form.getMultivalueFlag(),
					form.getDataTypeCode(),
					null,
					null,
					null,
					null,
					true,
					true,
					true);
			PropertyDAO.setPropertyValue(pb);
			EMTSharedDynProperties.refreshDynProperties("emgadm");
		} else
		{
			request.setAttribute("propErrors", errors);
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private ActionErrors editData(
		HttpServletRequest request,
		UpdateDynPropertyForm form,
		ActionErrors errors)
	{
		ActionErrors errs = errors;
		String msg =
			"Property value for the Property '" + form.getPropName() + "'";
		if (StringHelper.isNullOrEmpty(form.getPropVal()))
		{
			errors.add(
				ActionErrors.GLOBAL_ERROR,
				new ActionError("errors.required", msg));
		} else if (!isValidVal(form))
		{
			errors.add(
				ActionErrors.GLOBAL_ERROR,
				new ActionError("errors.invalid", msg));
		}
		return errs;
	}

	private boolean isValidVal(UpdateDynPropertyForm form)
	{
		try
		{
			if ("boolean".equalsIgnoreCase(form.getDataTypeCode()))
			{
				Boolean.valueOf(form.getPropVal());
			} else if ("float".equalsIgnoreCase(form.getDataTypeCode()))
			{
				Float.parseFloat(form.getPropVal());
			} else if ("int".equalsIgnoreCase(form.getDataTypeCode()))
			{
				Integer.parseInt(form.getPropVal());
			} else if ("double".equalsIgnoreCase(form.getDataTypeCode()))
			{
				Double.parseDouble(form.getPropVal());
			}
		} catch (Exception e)
		{
			return false;
		}

		return true;
	}

}