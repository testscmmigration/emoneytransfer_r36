package emgadm.sysadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.owasp.esapi.ESAPI;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.ACHReturnManager;
import emgadm.model.AchReturn;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;

public class ShowACHReturnDetailAction extends EMoneyGramAdmBaseAction
{
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ShowACHReturnDetailForm form = (ShowACHReturnDetailForm) f;
		UserProfile up = getUserProfile(request);

		String achTranReturnId = null;
		if ( request.getParameter("achTranReturnId") != null )
		{
			achTranReturnId = (String) request.getParameter("achTranReturnId");
			if(achTranReturnId.matches("[0-9]+")) { //MBO-9993
		    request.getSession().setAttribute("achTranReturnId",achTranReturnId);
			}
		}
		else
			achTranReturnId = (String) request.getSession().getAttribute("achTranReturnId");

		
		if (!StringHelper.isNullOrEmpty(form.getSubmitReturn()))
		{
			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_SHOW_ACHRETURN_QUEUE);
		}

		ACHReturnManager tm = getACHReturnManager(request);
		if (!StringHelper.isNullOrEmpty(form.getSubmitSave()))
		{
			tm.setAchReturnStatus(up.getUID(), Integer.parseInt(achTranReturnId), form.getAchStatusCode());
			errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("msg.achreturn.status.updated"));
				saveErrors(request, errors);			
		}
		

		
		request.setAttribute("achTranStatuses", tm.getAchReturnStatus(up.getUID()));

		AchReturn achReturn = null;
		achReturn = tm.getAchReturn(up.getUID(), Integer.parseInt(achTranReturnId));
		form.setAchStatusCode(achReturn.getReturnStatCode());		
		form.setAchTranId(achTranReturnId);

		request.setAttribute("achReturn", achReturn);
		
		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
	
}
