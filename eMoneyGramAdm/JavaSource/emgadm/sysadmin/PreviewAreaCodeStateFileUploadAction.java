package emgadm.sysadmin;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.model.UploadFile;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;

/**
 * @version 	1.0
 * @author
 */
public class PreviewAreaCodeStateFileUploadAction extends EMoneyGramAdmBaseAction
{
	public ActionForward execute(ActionMapping mapping,
		ActionForm form, 
		HttpServletRequest request, 
		HttpServletResponse response) throws Exception {

		TransactionManager tm = getTransactionManager(request);

		ServletContext sc = request.getSession().getServletContext();		
//		ActionErrors errors = new ActionErrors();
		
		PreviewAreaCodeStateFileUploadForm previewAreaCodeStateFileUploadForm = (PreviewAreaCodeStateFileUploadForm) form;
		UserProfile up = getUserProfile(request);

		if (!StringHelper.isNullOrEmpty(previewAreaCodeStateFileUploadForm.getSubmitCancel())){
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_AREACODE_STATE_FILE_UPLOAD);			
		}

		UploadFile uploadFile = (UploadFile)sc.getAttribute("uploadFile");

		if (StringHelper.isNullOrEmpty(previewAreaCodeStateFileUploadForm.getSubmitUpload())){
			uploadFile.parseFile();
			sc.setAttribute("uploadclick", "0");
		}else{
			sc.setAttribute("uploadclick", "1");
			
			//Create a File Control Record to track this upload
			int fileControlNbr = tm.insertFileControl(up.getUID(),
									EMoneyGramAdmApplicationConstants.AREACODE_STATE_FILE_UPLOAD);
			//Delete all rows 
			tm.deletePhoneAreaCode();
			
			//Upload all the rows into the database
			uploadFile.uploadFile(up.getUID(), tm);

			//Update File Control Record with all the record counts
			tm.updateFileControl(fileControlNbr, uploadFile.getTotalUploadRows(), uploadFile.getTotalUploadGoodRows(), 
					             uploadFile.getTotalUploadFaultyRows());		
		}

		sc.setAttribute("uploadFile", uploadFile);
		
		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
	
}
