package emgadm.sysadmin;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.model.UploadFile;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;


/**
 * @version 	1.0
 * @author
 */
public class PreviewAchReturnFileUploadAction extends EMoneyGramAdmBaseAction
{
	public ActionForward execute(ActionMapping mapping,
		ActionForm form, 
		HttpServletRequest request, 
		HttpServletResponse response) throws Exception {

		TransactionManager tm = getTransactionManager(request);
		

		ServletContext sc = request.getSession().getServletContext();		
		ActionErrors errors = new ActionErrors();
		
		PreviewAchReturnFileUploadForm previewAchReturnFileUploadForm = (PreviewAchReturnFileUploadForm) form;
		UserProfile up = getUserProfile(request);

		if (!StringHelper.isNullOrEmpty(previewAchReturnFileUploadForm.getSubmitCancel())){
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_ACH_RETURN_FILE_UPLOAD);			
		}

		UploadFile uploadFile = (UploadFile)sc.getAttribute("uploadFile");

		if (StringHelper.isNullOrEmpty(previewAchReturnFileUploadForm.getSubmitUpload())){
			uploadFile.parseFile();
			sc.setAttribute("uploadclick", "0");
		}else{
			sc.setAttribute("uploadclick", "1");
			
			//Create a File Control Record to track this upload
			int fileControlNbr = tm.insertFileControl(up.getUID(),
									EMoneyGramAdmApplicationConstants.ACH_RETURN_FILE_UPLOAD);
			
			//Upload all the rows into the database
			uploadFile.setFileControlNumber(fileControlNbr);
			uploadFile.uploadFile(up.getUID(), tm);

			tm.updateFileControl(fileControlNbr, uploadFile.getTotalUploadRows(), uploadFile.getTotalUploadGoodRows(), 
		             uploadFile.getTotalUploadFaultyRows());	
			errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("msg.file.upload.updated"));
				saveErrors(request, errors);			
			
		}

		sc.setAttribute("uploadFile", uploadFile);
		
		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);	
	}
	
}
