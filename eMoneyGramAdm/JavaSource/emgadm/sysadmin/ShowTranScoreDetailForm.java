package emgadm.sysadmin;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class ShowTranScoreDetailForm extends EMoneyGramAdmBaseValidatorForm
{
	private String type;
	private String tranId;
	private String partnerSiteId;
	private String submitReturn = null;

	public String getType()
	{
		return type;
	}

	public void setType(String string)
	{
		type = string;
	}

	public String getTranId()
	{
		return tranId;
	}

	public void setTranId(String string)
	{
		tranId = string;
	}

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}

	public String getSubmitReturn()
	{
		return submitReturn;
	}

	public void setSubmitReturn(String string)
	{
		submitReturn = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);
		submitReturn = null;
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		return errors;
	}
}
