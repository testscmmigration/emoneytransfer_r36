package emgadm.sysadmin;


import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.owasp.esapi.ESAPI;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.EPartnerSite;
import emgadm.util.StringHelper;
import emgshared.model.CountryExceptionBean;
import emgshared.model.CountryMasterBean;
import emgshared.model.UserActivityLog;
import emgshared.model.UserActivityType;
import emgshared.services.CountryExceptionService;
import emgshared.services.ServiceFactory;


/**
 * @version 	1.0
 * @author
 */
public class EditCountryExceptionAction extends EMoneyGramAdmBaseAction
{

	public static final String SELECTED_COUNTRY_OVR = "selectedCountry_ovr";

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm rawForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		ActionErrors errors = new ActionErrors();
		EditCountryExceptionForm form =
			(EditCountryExceptionForm) rawForm;

		if (!StringHelper
			.isNullOrEmpty(form.getSubmitCancel()))
		{
			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_SHOW_COUNTRY_EXCEPTION);
		}
		ServiceFactory sf = ServiceFactory.getInstance();
		CountryExceptionService ces = sf.getCountryExceptionService();
		CountryExceptionBean excp = null;
		form.setPartnerSiteId(ESAPI.encoder().encodeForHTMLAttribute(request.getParameter("selectedCountry")));
		if (form.getAddEditFlag() == null) {
		    form.setAddEditFlag("edit");
		}
		//  the user comes from the add country list screen with querystring
		if (request.getParameter("cntryId") != null) {
			CountryMasterBean countryMaster = (CountryMasterBean) ces.getCountryMasterList().get(request.getParameter("cntryId"));
			request.setAttribute("countryMaster", countryMaster);
			form.setRcvIsoCntryCode(ESAPI.encoder().encodeForHTMLAttribute(request.getParameter("cntryId")));  
			//System.out.println("----request.getAttribute('selectedPartner'):"+request.getSession().getAttribute("selectedPartner"));
			String selectedCountry = (String) request.getSession().getAttribute("selectedCountry");
			form.setIsoCntryCode(ESAPI.encoder().encodeForHTMLAttribute(selectedCountry));
			form.setPartnerSiteCurrency(ESAPI.encoder().encodeForHTMLAttribute(EPartnerSite.getPartnerByCountry(selectedCountry).getPartnerSiteCurrency()));
			form.setSndAllowFlag("Y");
			form.setManualReviewAllowFlag("N");
			form.setSndMaxAmt("0.000");
			form.setSndThrldWarnAmt("0.000");
			form.setExcpCmntText("");
			form.setAddEditFlag("add");
			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		}


		//  the user comes from the country exception list screen with querystring
		String sndContry = ESAPI.encoder().encodeForHTMLAttribute(request.getParameter("id1"));
		//sndContry=ESAPI.encoder().encodeForHTMLAttribute(sndContry);
		if (sndContry != null) {
			String rcvCountry = ESAPI.encoder().encodeForHTMLAttribute(request.getParameter("id2"));
			//rcvCountry=ESAPI.encoder().encodeForHTMLAttribute(rcvCountry);
			excp = ces.getCntryException(sndContry, rcvCountry);
			CountryMasterBean countryMaster = (CountryMasterBean) ces.getCountryMasterList().get(sndContry);
			request.setAttribute("countryMaster", countryMaster);
			form.setRcvIsoCntryCode(ESAPI.encoder().encodeForHTMLAttribute(excp.getRcvIsoCntryCode()));
			form.setPartnerSiteCurrency(ESAPI.encoder().encodeForHTMLAttribute((EPartnerSite.getPartnerByCountry(rcvCountry).getPartnerSiteCurrency())));
			form.setIsoCntryCode(ESAPI.encoder().encodeForHTMLAttribute(excp.getIsoCntryCode()));
			form.setSndAllowFlag(ESAPI.encoder().encodeForHTMLAttribute(excp.getSndAllowFlag()));
			form.setSndMaxAmt(ESAPI.encoder().encodeForHTMLAttribute(excp.getSndMaxAmt()));
			form.setManualReviewAllowFlag(ESAPI.encoder().encodeForHTMLAttribute(excp.getManualReviewAllowFlag()));
			if ("Y".equalsIgnoreCase(excp.getSndAllowFlag())
					&& excp.getSndMaxAmtNbr() != 0F) {
				form.setDefaultMaxAmtFlag("N");
			}
			form.setSndThrldWarnAmt(ESAPI.encoder().encodeForHTMLAttribute(excp.getSndThrldWarnAmt()));
			form.setExcpCmntText(ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTML(excp.getExcpCmntText())));
			
			// Trust Boundary Violation Veracode issue fix //
			if(!StringHelper.isNullOrEmpty(excp.getSndAllowFlag()) && excp.getSndAllowFlag().matches("[0-9a-zA-Z_]+"))
			request.getSession().setAttribute("SndAllowFlag",excp.getSndAllowFlag());
			if(!StringHelper.isNullOrEmpty(excp.getManualReviewAllowFlag()) && excp.getManualReviewAllowFlag().matches("[0-9a-zA-Z_]+"))
			request.getSession().setAttribute("ManualReviewAllowFlag", excp.getManualReviewAllowFlag());
			if(!StringHelper.isNullOrEmpty(excp.getSndMaxAmt()) && excp.getSndMaxAmt().matches("^[0-9][\\.\\d]*(,\\d+)?$"))
			request.getSession().setAttribute("SndMaxAmt",excp.getSndMaxAmt());
			if(!StringHelper.isNullOrEmpty(excp.getSndThrldWarnAmt()) && excp.getSndThrldWarnAmt().matches("^[0-9][\\.\\d]*(,\\d+)?$"))
			request.getSession().setAttribute("SndThrldWarnAmt",excp.getSndThrldWarnAmt());
			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitSave())) {
			excp = new CountryExceptionBean();
			excp.setRcvIsoCntryCode(
				form.getRcvIsoCntryCode());
			String selectedCountry = form.getIsoCntryCode();
			excp.setIsoCntryCode(selectedCountry);
			form.setPartnerSiteCurrency(EPartnerSite.getPartnerByCountry(selectedCountry).getPartnerSiteCurrency());
			excp.setSndAllowFlag(form.getSndAllowFlag());
			excp.setManualReviewAllowFlag(form.getManualReviewAllowFlag());
			excp.setSndMaxAmt(form.getSndMaxAmt());
			excp.setSndThrldWarnAmt(
				form.getSndThrldWarnAmt());
			if (StringHelper.isNullOrEmpty(form.getExcpCmntText())) {
				excp.setExcpCmntText(" ");
			} else {
				excp.setExcpCmntText(
					form.getExcpCmntText());
			}
			ces.setCntryException(excp);
			errors.add(
				ActionErrors.GLOBAL_ERROR,
				new ActionError(
					"msg.county.exception.saved",
					form.getRcvIsoCntryCode()));
			saveErrors(request, errors);
			
			Map<String, String> transferMap = new HashMap();
			transferMap.put(SELECTED_COUNTRY_OVR, excp.getIsoCntryCode());
			request.getSession().setAttribute(SELECTED_COUNTRY_OVR, transferMap);
			try {
			    String logType = null;
			    StringBuilder detailText = new StringBuilder(256);
			    if (form.getAddEditFlag().equalsIgnoreCase("ADD"))
			        logType = UserActivityType.EVENT_TYPE_COUNTRYEXCEPTIONSADD;
			    else {
			        // TODO get an EDIT type
			        logType = UserActivityType.EVENT_TYPE_COUNTRYEXCEPTIONSADD;
			     // Trust Boundary Violation Veracode issue fix //
			        detailText.append("<FROM_ALLOW>" + request.getSession().getAttribute("SndAllowFlag") + "</FROM_ALLOW>");
			        detailText.append("<FROM_MANUAL_REVIEW>" + request.getSession().getAttribute("ManualReviewAllowFlag") + "</FROM_MANUAL_REVIEW>");
			        detailText.append("<FROM_AUTHENTICATE_TRAN>" + request.getSession().getAttribute("ManualReviewAllowFlag") + "</FROM_AUTHENTICATE_TRAN>");
			        detailText.append("<FROM_MAX>" + request.getSession().getAttribute("SndMaxAmt") + "</FROM_MAX>");
			        detailText.append("<FROM_THRESHOLD>" + request.getSession().getAttribute("SndThrldWarnAmt") + "</FROM_THRESHOLD>");
			        detailText.append("<TO_ALLOW>" + excp.getSndAllowFlag()+ "</TO_ALLOW>");
			        detailText.append("<TO_MANUAL_REVIEW>" + excp.getManualReviewAllowFlag() + "</TO_MANUAL_REVIEW>");
			        detailText.append("<TO_AUTHENTICATE_TRAN>" + excp.getManualReviewAllowFlag() + "</TO_AUTHENTICATE_TRAN>");
			        detailText.append("<TO_MAX>" + excp.getSndMaxAmt() + "</TO_MAX>");
			        detailText.append("<TO_THRESHOLD>" + excp.getSndThrldWarnAmt() + "</TO_THRESHOLD>");
			    }
		    	super.insertActivityLog(this.getClass(),request,detailText.toString(),logType,excp.getRcvIsoCntryCode() + UserActivityLog.dataKeyValueSeperator + excp.getIsoCntryCode());
			} catch (Exception e) { }

			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_SHOW_COUNTRY_EXCEPTION);
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
