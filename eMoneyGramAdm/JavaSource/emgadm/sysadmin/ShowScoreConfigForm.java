package emgadm.sysadmin;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class ShowScoreConfigForm extends EMoneyGramAdmBaseValidatorForm
{
	private String scoreCnfgId;
	private String from;
	private String submitCnfg = null;
	private String partnerSiteId = null;

	public String getScoreCnfgId()
	{
		return scoreCnfgId;
	}

	public void setScoreCnfgId(String string)
	{
		scoreCnfgId = string;
	}

	public String getFrom()
	{
		return from == null ? "" : from;
	}

	public void setFrom(String string)
	{
		from = string;
	}

	public String getSubmitCnfg()
	{
		return submitCnfg;
	}

	public void setSubmitCnfg(String string)
	{
		submitCnfg = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);
		submitCnfg = null;
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		return errors;
	}

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}
}
