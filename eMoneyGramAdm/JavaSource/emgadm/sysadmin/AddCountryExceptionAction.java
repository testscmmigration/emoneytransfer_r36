package emgadm.sysadmin;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.owasp.esapi.ESAPI;

import shared.mgo.dto.MGOCountry;
import shared.mgo.services.AgentConnectService;
import shared.mgo.services.MGOServiceFactory;
import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.EPartnerSite;
import emgshared.model.CountryExceptionBean;
import emgshared.services.CountryExceptionService;
import emgshared.services.ServiceFactory;

/**
 * @version 	1.0
 * @author
 */
public class AddCountryExceptionAction extends EMoneyGramAdmBaseAction {

	public ActionForward execute(
				ActionMapping mapping,
				ActionForm rawForm,
				HttpServletRequest request,
				HttpServletResponse response)
			throws Exception {

		AddCountryExceptionForm form = (AddCountryExceptionForm) rawForm;
		ServiceFactory sf = ServiceFactory.getInstance();
		CountryExceptionService ces = sf.getCountryExceptionService();
		AgentConnectService agentConnectService = MGOServiceFactory.getInstance().getAgentConnectService();
		Collection countries = (Collection)agentConnectService.getActiveCountryList();
		Collection cntryExcps = ces.getCntryExceptions(form.getSelectedCountry());
		HashMap countryMaster = ces.getCountryMasterList();
		Collection excps = new ArrayList();
		Map tmpMap = new HashMap();
		Iterator iter = cntryExcps.iterator();

		while (iter.hasNext()) {
			CountryExceptionBean ce = (CountryExceptionBean) iter.next();
			tmpMap.put(ce.getRcvIsoCntryCode(), ce);
		}

		iter = countries.iterator();
		while (iter.hasNext()) {
			MGOCountry emtc = (MGOCountry) iter.next();
			if (!tmpMap.containsKey(emtc.getCountryCode())) {
				excps.add(emtc);
			}
		}
		String selectedCountry = (String)request.getParameter("selectedCountry");
			if(selectedCountry.matches("[A-Z]+") && selectedCountry.length() == 3) { //MBO-9993
		request.getSession().setAttribute("selectedCountry", selectedCountry);
		}
		request.setAttribute("excps", excps);
		request.setAttribute("countryMaster", countryMaster);

		tmpMap = null;

		if (excps.size() == 0) {
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("label.no.countries.to.add"));
			saveErrors(request, errors);
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
	
}
