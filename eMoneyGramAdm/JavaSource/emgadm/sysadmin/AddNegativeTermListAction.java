/*
 * Created on Jul 31, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.sysadmin;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.model.ConsumerProfileComment;
import emgshared.model.PurposeType;
import emgshared.services.ConsumerProfileService;
import emgshared.services.FraudService;
import emgshared.services.ServiceFactory;

/**
 * @author A136
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AddNegativeTermListAction  extends EMoneyGramAdmBaseAction{

    private static final ConsumerProfileService profileService = ServiceFactory
    .getInstance().getConsumerProfileService();

public ActionForward execute(
			ActionMapping mapping,
			ActionForm f,
			HttpServletRequest request,
			HttpServletResponse response)
			throws Exception
		{
			ActionErrors errors = new ActionErrors();
			ConsumerProfileComment comment =
				new ConsumerProfileComment();
			UserProfile up = getUserProfile(request);
			NegativeTermListForm form =	(NegativeTermListForm) f;
//			ServletContext sc = request.getSession().getServletContext();
			
			ServiceFactory sf = ServiceFactory.getInstance();
			FraudService fs = sf.getFraudService();
		
			// cancel add action
			if (!StringHelper.isNullOrEmpty(form.getSubmitCancel()))
			{
		        request.setAttribute("negativeType", "");
				return mapping.findForward(
						EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_NEGATIVE_TERM_LIST);				
			}
			
			// add address token into the list
			if (!StringHelper.isNullOrEmpty(form.getNegativeAddress()))
			{
				Collection list = fs.getNegativeTermList(up.getUID(), 
						"ADD", form.getNegativeAddress(), 1);
				
				String typeDescription = getPurposeTypeDesc(request, "ADD");
		
				if (list == null || list.size() == 0)
				{
					// set negative term to the list in database
					fs.setNegativeTerm(up.getUID(), "ADD", form.getNegativeAddress());
					
			        ActionMessages messages = getActionMessages(request);
			        messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage(
			                "message.negativeterm.add.success", form.getNegativeAddress()));
			        saveMessages(request, messages);
					
					// Add profile change comment
//					int custId = Integer.parseInt(form.getCustId());
					comment.setCustId(Integer.parseInt(form.getCustId()));
					comment.setReasonCode("OTH");
					comment.setText("Added \"" + form.getNegativeAddress() + "\" as Negative Address" );
					profileService.addConsumerProfileComment(comment, up.getUID());
				}
				else
				{			
					errors.add(
							ActionErrors.GLOBAL_ERROR,
							new ActionError("error.negativeterm.found", typeDescription, form.getNegativeAddress()));
						saveErrors(request, errors);
				}			
				
				form.setNegativeTerm(null);
				form.setNegativeType("ADD");
				form.setSubmitShowAll("Show All");
				return mapping.findForward(
						EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_NEGATIVE_TERM_LIST);		
			}
			
			// add negative term to the list
			if (!StringHelper.isNullOrEmpty(form.getSubmitProcess()))
			{
				//MGO-11644 story fix starts
				boolean isValidToken = isTokenValid(request);
				if(!isValidToken){
					form.setNegativeTerm("");
					form.setNegativeType("NTL");
					// When the user already logged in, the request comes from new tab or new window, then we are logging the information as such
					// as the customer information will not be available in the object, we are passing a constant which represents the above scenario.
					errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("error.csrf"));
					saveErrors(request, errors);
					request.getSession().invalidate();
					return mapping.findForward("errorPage");
					//MGO-11644 story changes ends
				}
				if (!StringHelper
						.isNullOrEmpty((String) request.getAttribute("negativeType")))
						form.setNegativeType(
							(String) request.getAttribute("negativeType"));

				if (!StringHelper
					.isNullOrEmpty((String) request.getAttribute("negativeTerm")))
					form.setNegativeTerm(
						(String) request.getAttribute("negativeTerm"));
				
				// due security reason removing all the html and single quotes codes
				form.setNegativeTerm(form.getNegativeTerm().replaceAll("\\<.*?>",""));
				form.setNegativeTerm(form.getNegativeTerm().replaceAll("\\'",""));
				//resetToken(request);
				if ("ADD".equalsIgnoreCase(form.getNegativeType())) {
					errors.add(
							ActionErrors.GLOBAL_ERROR,
							new ActionError("error.negativeterm.cannot.add.address"));
					saveErrors(request, errors);
						
					return mapping.findForward(
								EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
				}
				
				String typeDescription = getPurposeTypeDesc(request, form.getNegativeType());
				
				Collection list = fs.getNegativeTermList(up.getUID(), 
								form.getNegativeType(), form.getNegativeTerm(), 1);
				
				if (list == null || list.size() == 0)
				{
					// set negative term to the list in database
					fs.setNegativeTerm(up.getUID(), form.getNegativeType(), form.getNegativeTerm());
			        ActionMessages messages = getActionMessages(request);
			        messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage(
			                "message.negativeterm.add.success", form.getNegativeTerm()));
			        saveMessages(request, messages);
			        form.setNegativeTerm(null);
				}
				else
				{			
					errors.add(
							ActionErrors.GLOBAL_ERROR,
							new ActionError("error.negativeterm.found", typeDescription, form.getNegativeTerm()));
					saveErrors(request, errors);
				}
			}
			
			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		}

	private String getPurposeTypeDesc(HttpServletRequest request, String type) {
		String description = "";
		ArrayList list = (ArrayList) request.getSession().getServletContext().getAttribute("custPurposeTypes");

		for (int i = 0; i < list.size(); i++) {
			PurposeType code = (PurposeType) list.get(i);
			if (code.getPurposeTypeCode().equalsIgnoreCase(type)) {
				description = code.getPurposeTypeDesc();
				break;
			}
		}
		return description;
	}
}
