package emgadm.sysadmin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;
import org.owasp.esapi.ESAPI;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.model.ScoreConfiguration;
import emgshared.services.ScoringService;
import emgshared.services.ServiceFactory;
import emgshared.util.Constants;

public class ShowScoreConfigAction extends EMoneyGramAdmBaseAction
{
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		ShowScoreConfigForm form = (ShowScoreConfigForm) f;
		UserProfile up = getUserProfile(request);
		ScoringService ss = ServiceFactory.getInstance().getScoringService();

		String partnerSiteId = form.getPartnerSiteId();
		System.out.println("ShowScoreConfig got partnerSiteId=" + partnerSiteId);
		
		if (partnerSiteId != null && partnerSiteId.matches("[a-zA-Z_]+")) {
			partnerSiteId = (String) request.getParameter("partnerSiteId");
			if(partnerSiteId.matches("[A-Z]+")) { //MBO-9993
			request.getSession().setAttribute("partnerSiteId", partnerSiteId);
			}
		} else {
			partnerSiteId = Constants.MGO_PARTNER_SITE_ID;
		}
		
//		request.getSession().setAttribute("partnerSiteId", partnerSiteId);
//
//		if (partnerSiteId == null) {
//			partnerSiteId = Constants.MGO_PARTNER_SITE_ID;
//		}
		List unfilteredScoreConfigurationList = (List) ss.getScoreConfigurationsList(up.getUID(), null, null, partnerSiteId);
		int partnerSiteCode = (Integer) Constants.partnerSiteIdToCode.get(partnerSiteId);
		List scoreConfigList = getConfigList(unfilteredScoreConfigurationList, partnerSiteCode);

		request.setAttribute("configs",	scoreConfigList);

		if (!StringHelper.isNullOrEmpty(form.getSubmitCnfg()))
		{
			request.setAttribute("scoreConfig",	ss.getScoreConfigView(up.getUID(),Integer.parseInt(form.getScoreCnfgId())));
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private List getConfigList(List list, int partnerSiteCode)
	{
		List newList = new ArrayList();
		Iterator iter = list.iterator();
		while (iter.hasNext())
		{
			ScoreConfiguration sc = (ScoreConfiguration) iter.next();
			if (sc.getPartnerSiteCode() == partnerSiteCode) {
				newList.add(new LabelValueBean( sc.getConfigurationId()	+ "-"+ sc.getTransactionType()
						+ " - (Version:" + sc.getConfigurationVersionNumber()+ ") - (Status: " +sc.getStatus()+ ") - (Created:" + sc.getCreateDate() + ")", String.valueOf(sc.getConfigurationId())));
			}
		}
		return newList;
	}
}
