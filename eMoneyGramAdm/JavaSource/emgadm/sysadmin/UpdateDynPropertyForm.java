package emgadm.sysadmin;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class UpdateDynPropertyForm extends EMoneyGramAdmBaseValidatorForm {
	private String propName;
	private String propDesc;
	private String propVal;
	private String adminUseFlag;
	private String multivalueFlag;
	private String dataTypeCode;
	private String submitUpdate;
	private String submitAdd;

	public String getPropName() {
		return propName;
	}

	public void setPropName(String string) {
		propName = string;
	}

	public String getPropDesc() {
		return propDesc;
	}

	public void setPropDesc(String string) {
		propDesc = string;
	}

	public String getPropVal() {
		return propVal;
	}

	public void setPropVal(String string) {
		propVal = string;
	}

	public String getAdminUseFlag() {
		return adminUseFlag;
	}

	public void setAdminUseFlag(String string) {
		adminUseFlag = string;
	}

	public String getMultivalueFlag() {
		return multivalueFlag;
	}

	public void setMultivalueFlag(String string) {
		multivalueFlag = string;
	}

	public String getDataTypeCode() {
		return dataTypeCode;
	}

	public void setDataTypeCode(String string) {
		dataTypeCode = string;
	}

	public String getSubmitUpdate() {
		return submitUpdate;
	}

	public void setSubmitUpdate(String string) {
		submitUpdate = string;
	}

	public String getSubmitAdd() {
		return submitAdd;
	}

	public void setSubmitAdd(String string) {
		submitAdd = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);

		return errors;
	}
}