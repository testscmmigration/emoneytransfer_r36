package emgadm.blockedList;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.services.DecryptionService;
import emgadm.util.StringHelper;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.BlockedBean;
import emgshared.model.CustomerProfileAccount;
import emgshared.services.FraudService;
import emgshared.services.ObfuscationService;
import emgshared.services.ServiceFactory;

/**
 * @version 1.0
 * @author
 */
public class SaveBankBlockedListAction extends EMoneyGramAdmBaseAction {
	
	private static final String NOT_BLOCKED = "NBK";
	
	public void saveDB(HttpServletRequest request, UserProfile up,
			ActionErrors errors) throws DataSourceException,
			TooManyResultException, ParseException {
		ServiceFactory sf = ServiceFactory.getInstance();
		FraudService fs = sf.getFraudService();
		ObfuscationService os = sf.getObfuscationService();

		BlockedBean blockedAcct = (BlockedBean) request.getSession()
				.getAttribute("blockedABA");
		blockedAcct.setEncryptedNumber(os
				.getBankAccountObfuscation("**********"));
		blockedAcct.setHashText(os.getBankAccountHash("**********"));
		blockedAcct.setMaskText("****");

		String aba = blockedAcct.getAbaNumber();
		String mask = blockedAcct.getMaskText();
		Collection consumerProfileAccounts = fs.getConsumersByAccounts(
				up.getUID(), aba, mask, false, 0);

		fs.setBlockedBankAccounts(up.getUID(), blockedAcct,
				consumerProfileAccounts);
		request.setAttribute("blockedABA", null);
	}

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		SaveBankBlockedListForm saveBankBlockedListForm = (SaveBankBlockedListForm) form;
		UserProfile up = getUserProfile(request);
		Collection consumerProfileAccounts = new ArrayList();

		ServiceFactory sf = ServiceFactory.getInstance();
		FraudService fs = sf.getFraudService();

		ObfuscationService os = sf.getObfuscationService();
		DecryptionService ds = emgadm.services.ServiceFactory.getInstance()
				.getDecryptionService();

		if (!StringHelper.isNullOrEmpty(saveBankBlockedListForm
				.getSubmitDBCancel())) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_BANK_BLOCKED_LIST);
		}

		BlockedBean blockedAcct = (BlockedBean) request.getSession()
				.getAttribute("blockedAcct");

		// Getting all the Blocked Bank Accounts records that get affected
		Collection blockedAccts = StringHelper.isNullOrEmpty(blockedAcct.getHashText())? fs.getBlockedBankAccounts(up.getUID(),
				blockedAcct.getAbaNumber(), null, "****") : fs
				.getBlockedBankAccounts(up.getUID(),
						blockedAcct.getAbaNumber(),
						blockedAcct.getHashText(), null);
		

		for (Iterator it = blockedAccts.iterator(); it.hasNext();) {
			BlockedBean acct = (BlockedBean) it.next();
			acct.setClearText(ds.decryptBankAccountNumber(acct
					.getEncryptedNumber()));
			if (acct.getClearText().equals("**********"))
				acct.setClearText(null);

		}
		request.setAttribute("blockedAccts", blockedAccts);

		// Getting all the customer profiles that get affected

		String aba = blockedAcct.getAbaNumber();
		String mask = blockedAcct.getMaskText();

		Collection allConsumerProfileAccounts = fs.getConsumersByAccounts(
				up.getUID(), aba, mask, false, 0);

		// Spin through the list of filtered Bank accounts and find the exact
		// matches
		for (Iterator it = allConsumerProfileAccounts.iterator(); it.hasNext();) {
			CustomerProfileAccount cpa = (CustomerProfileAccount) it.next();

			if (!StringHelper.isNullOrEmpty(blockedAcct.getHashText())) {
				if (blockedAcct
						.getClearText()
						.trim()
						.equals(ds.decryptBankAccountNumber(
								cpa.getAcctEncrypNbr()).trim())) {
					consumerProfileAccounts.add(cpa);
				}
			} else {
				consumerProfileAccounts.add(cpa);
			}
		}

		request.setAttribute("consumerProfileAccounts", consumerProfileAccounts);

		if (!StringHelper.isNullOrEmpty(saveBankBlockedListForm
				.getSubmitDBSave())) {

			if (StringHelper.isNullOrEmpty(blockedAcct.getHashText())) {
				blockedAcct.setEncryptedNumber(os
						.getBankAccountObfuscation("**********"));
				blockedAcct.setHashText(os.getBankAccountHash("**********"));
				blockedAcct.setMaskText("****");
			}

			fs.setBlockedBankAccounts(up.getUID(), blockedAcct,
					consumerProfileAccounts);

			if(blockedAcct.getStatusCode().equals(NOT_BLOCKED)){
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
				"msg.unblockedbank.added"));
			}
			else {
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
					"msg.blockedbank.added"));
			}
			saveErrors(request, errors);

			request.setAttribute("blockedAccts", null);

			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_BANK_BLOCKED_LIST);
		}

		return mapping
				.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
