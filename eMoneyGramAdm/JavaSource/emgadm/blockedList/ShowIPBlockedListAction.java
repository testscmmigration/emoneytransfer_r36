package emgadm.blockedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.CommandAuth;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.exceptions.TooManyResultException;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.FraudService;
import emgshared.services.ServiceFactory;

/**
 * @version 	1.0
 * @author
 */
public class ShowIPBlockedListAction extends EMoneyGramAdmBaseAction
{

	private static EMTSharedDynProperties dynProps =
		new EMTSharedDynProperties();

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		ActionErrors errors = new ActionErrors();
		ShowIPBlockedListForm showIPBlockedListForm =
			(ShowIPBlockedListForm) form;
		UserProfile up = getUserProfile(request);
		CommandAuth ca = new CommandAuth();
		Collection blockedIPs = new ArrayList();

		if (up.hasPermission(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_ADD_IP_BLOCKED_LIST))
		{
			ca.setAddIPBlockedList(true);
		} else
		{
			ca.setAddIPBlockedList(false);
		}
		request.getSession().setAttribute("auth", ca);

		request.setAttribute(
			"ipaddress1",
			showIPBlockedListForm.getIpAddress() == null
				? null
				: showIPBlockedListForm.getIpAddress().trim());

		//MBO-2847 changes starts here
		//Adding saveToken will add token to he request,which in turn prevents the unauthorized access.
		saveToken(request);
		//MBO-2847 changes ends here
		
		//  go to add a new biller limit selection screen
		if (!StringHelper.isNullOrEmpty(showIPBlockedListForm.getSubmitAdd()))
		{
			
			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_ADD_IP_BLOCKED_LIST);
		}

		if (!StringHelper
			.isNullOrEmpty(showIPBlockedListForm.getSubmitSearch())
			|| !StringHelper.isNullOrEmpty(
				showIPBlockedListForm.getSubmitShowAll()))
		{

			try
			{
				ServiceFactory sf = ServiceFactory.getInstance();
				FraudService fs = sf.getFraudService();

				String tmpIP = null;
				if (!StringHelper
					.isNullOrEmpty(showIPBlockedListForm.getSubmitSearch()))
				{
					tmpIP = showIPBlockedListForm.getIpAddress().trim();
				}

				blockedIPs = fs.getBlockedIPs(up.getUID(), tmpIP, null);

				if (blockedIPs == null || blockedIPs.size() == 0)
				{
					errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError("error.no.blockedip.found"));
					saveErrors(request, errors);
				}

			} catch (TooManyResultException e)
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError(
						"error.too.many.results",
						dynProps.getMaxDownloadTransactions() + ""));
				saveErrors(request, errors);
			}

			Collections.sort((List) blockedIPs);
			request.setAttribute("blockedIPs", blockedIPs);
			showIPBlockedListForm.setSubmitSearch(null);
			showIPBlockedListForm.setSubmitShowAll(null);
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
