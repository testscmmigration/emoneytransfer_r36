package emgadm.blockedList;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.model.BlockedBean;
import emgshared.model.BlockedIP;
import emgshared.model.ConsumerCreditCardAccount;
import emgshared.model.CustomerProfileAccount;
import emgshared.model.ElectronicTypeCode;
import emgshared.services.ConsumerProfileService;
import emgshared.services.FraudService;
import emgshared.services.ObfuscationService;
import emgshared.services.PCIService;
import emgshared.services.ServiceFactory;

/**
 * @version 	1.0
 * @author
 */
public class AddBlockedListAction extends EMoneyGramAdmBaseAction
{
	private ConsumerProfileService profileService = ServiceFactory.getInstance().getConsumerProfileService();

	private static final PCIService pciService = 
		emgshared.services.ServiceFactory.getInstance().getPCIService();

	private void fillBlockingCommentsAndReasons(HttpServletRequest request, FraudService fs, UserProfile up) throws Exception {
		String[] types = {"EML", "PHON", "BANK", "BIN", "CC", "IP"};
		ServletContext sc = request.getSession().getServletContext();
		String blockingReason = "blockingReason";
		String blockingAction = "blockingAction";
		for (String type : types) {
		  Collection br = fs.getBlockedReasons(up.getUID(), type);
		  Collection bs = fs.getBlockedStatus(up.getUID(), type);
		  sc.setAttribute(blockingReason + type, br);
		  sc.setAttribute(blockingAction + type, bs);
		}
	}

	public void processEmail(AddBlockedListForm addBlockedListForm, HttpServletRequest request) {
		BlockedBean email = new BlockedBean();
		email.setClearText((String)request.getSession().getAttribute("blockEmailAddress"));
		email.setAddrFmtCode("A");
		email.setElecAddrTypeCode(ElectronicTypeCode.EMAIL.getTypeCode());
		request.getSession().setAttribute("blockedEmail", email);
	}

	public void processIP(AddBlockedListForm addBlockedListForm, HttpServletRequest request) {
		BlockedIP blockedIP = new BlockedIP();
		String ip = (String)request.getSession().getAttribute("blockIP");
		blockedIP.setIpAddress1(ip.trim());
		request.getSession().setAttribute("blockedIP", blockedIP);
	}

	public void processPhone(AddBlockedListForm addBlockedListForm, HttpServletRequest request) {
		
	}

	private void processCC(AddBlockedListForm form, HttpServletRequest request) {
		BlockedBean cc = new BlockedBean();
		ConsumerCreditCardAccount blockCC = (ConsumerCreditCardAccount)request.getSession().getAttribute("blockCC");
		cc.setMaskText(blockCC.getAccountNumberMask());
		cc.setHashText(blockCC.getAccountNumberHash());
		cc.setBinText(blockCC.getAccountNumberBin());
		request.getSession().setAttribute("blockedCC", cc);
	}

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		ActionErrors errors = new ActionErrors();
		AddBlockedListForm addBlockedListForm = (AddBlockedListForm) form;

		ServiceFactory sf = ServiceFactory.getInstance();
		FraudService fs = sf.getFraudService();
		UserProfile up = getUserProfile(request);

		fillBlockingCommentsAndReasons(request, fs, up);
		
		// checkbox list
		Set<String> cbList = new HashSet<String>(); 
		cbList.add("emailBlocker");
		cbList.add("phoneBlocker");
		cbList.add("ccBlocker");
		cbList.add("binBlocker");
		cbList.add("ipBlocker");
		cbList.add("abaBlocker");
		cbList.add("accountBlocker");

		// Check if there is checked at least one checkbox
		int hmOptions = 0;
		for(String cbOption : cbList) {
			if(request.getParameter(cbOption) != null) {
				hmOptions++;
			}
		}
		// Evaluate
		if(hmOptions == 0) {
			// Error
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("label.blocked.error.noneselected"));
			saveErrors(request, errors);
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_ERROR);
		}

		String email = request.getParameter("emailAddress");
		if (request.getParameter("emailBlocker") != null && !StringHelper.isNullOrEmpty(email)) {
			Collection emails = null;
			Collection consumerProfiles = null;
			String value = request.getParameter("detail");
			if (value.equals("full")) {
				emails = fs.getBlockedEmails(up.getUID(), email, null);
				consumerProfiles = fs.getConsumersByEmail(up.getUID(), email,	null);
			} else {
				emails = fs.getBlockedEmails(up.getUID(), null, email.substring(email.indexOf('@') + 1));
				consumerProfiles = fs.getConsumersByEmail(up.getUID(), null, email.substring(email.indexOf('@') + 1));
			}
			request.setAttribute("blockedEmails", emails);
			request.setAttribute("consumerProfiles", consumerProfiles);
			request.setAttribute("emailBlocker", true);
		}

		String phone = request.getParameter("phoneNumber");
		if (request.getParameter("phoneBlocker") != null && !StringHelper.isNullOrEmpty(phone)) {
			Collection phones = fs.getBlockedPhones(up.getUID(), phone);
			Collection consumers = fs.getConsumersByPhone(up.getUID(), phone);
			request.setAttribute("blockedPhones", phones);
			request.setAttribute("consumerList", consumers);
			request.setAttribute("phoneBlocker", true);
		}

		ObfuscationService os = ServiceFactory.getInstance().getObfuscationService();
		
		String hash = request.getParameter("hash");
		
		String blockedId = request.getParameter("blockedId");
		String bin = request.getParameter("bin");
		String mask = request.getParameter("mask");
		if (request.getParameter("ccBlocker") != null && !StringHelper.isNullOrEmpty(bin)) {
			
			Collection blockedCCs = fs.getBlockedCCAccounts(up.getUID(), blockedId,
				null);
			
			
			for (Iterator it = blockedCCs.iterator(); it.hasNext();)
			{
				BlockedBean cc = (BlockedBean) it.next();
				
				cc.setClearText(pciService.retrieveCardNumber(blockedId,
					true));
			
			}
			
			request.setAttribute("blockedCCs", blockedCCs);
			
			Collection customerProfileAccounts = fs.getConsumersByAccounts(up.getUID(), os.getBinHash(bin), mask, true,0);
			Collection filteredCustomerProfileAccounts = new LinkedList();
			
			// Spin through the list of filtered cc accounts and find the exact matches 
			for (Iterator it = customerProfileAccounts.iterator(); it.hasNext();) {
				CustomerProfileAccount cpa = (CustomerProfileAccount) it.next();
				
				String decryptedAccountNumber =	
					pciService.retrieveCardNumber(cpa.getCustAcctId(), true);
				
				if (!StringHelper.isNullOrEmpty(blockedId) && 
					blockedId.equals(
						pciService.retrieveBlockedId(decryptedAccountNumber))) {
					filteredCustomerProfileAccounts.add(cpa);
				}
			}

			request.setAttribute("customerProfileAccounts",	filteredCustomerProfileAccounts);
			request.setAttribute("ccBlocker", true);
		}

		if (request.getParameter("binBlocker") != null && !StringHelper.isNullOrEmpty(bin)) {
			Collection bins = fs.getBlockedBins(up.getUID(), hash);
			request.setAttribute("bins", bins);
			request.setAttribute("binBlocker", true);
		}

		String ip = request.getParameter("ip");
		String ipBlocker = request.getParameter("ipBlocker");
		if (ipBlocker != null && !StringHelper.isNullOrEmpty(ip)) {
			Collection blockedIPs = fs.getBlockedIPs(up.getUID(), ip, null);
			if (blockedIPs != null && blockedIPs.size() > 0) {
				BlockedIP blockedIP = (BlockedIP) blockedIPs.toArray()[0];
				request.setAttribute("blockedIPs", blockedIPs);
			}
			request.setAttribute("ipBlocker", true);
		}

		String aba = request.getParameter("abaNumber");
		if (request.getParameter("abaBlocker") != null && !StringHelper.isNullOrEmpty(aba)) {
			Collection blockedAccts = fs.getBlockedBankAccounts(up.getUID(), aba, null, "****");
			request.setAttribute("blockedAccts", blockedAccts);

			Collection allConsumerProfileAccounts = 
				fs.getConsumersByAccounts(up.getUID(), aba, null, false, 0);
			request.setAttribute("consumerProfileAccounts",	allConsumerProfileAccounts);
			request.setAttribute("abaBlocker", true);
		}	
		
		String account = request.getParameter("account");
		if (request.getParameter("accountBlocker") != null && !StringHelper.isNullOrEmpty(account)) {
			Collection blockedAcct = fs.getBlockedBankAccounts(up.getUID(), aba, os.getBankAccountHash(account), null);
			request.setAttribute("blockedAcct", blockedAcct);
			
			request.setAttribute("accountBlocker", true);
		}

		request.setAttribute("custId", addBlockedListForm.getCustId());

		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
 