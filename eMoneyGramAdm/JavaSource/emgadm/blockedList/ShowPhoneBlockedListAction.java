package emgadm.blockedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.BlockedBean;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.FraudService;
import emgshared.services.ServiceFactory;

/**
 * @version 	1.0
 * @author
 */
public class ShowPhoneBlockedListAction extends EMoneyGramAdmBaseAction
{
	private static EMTSharedDynProperties dynProps =
		new EMTSharedDynProperties();

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws DataSourceException
	{

		ActionErrors errors = new ActionErrors();
		ShowPhoneBlockedListForm form = (ShowPhoneBlockedListForm) f;
		UserProfile up = getUserProfile(request);
		Collection blockedPhones = new ArrayList();

		//		if (StringHelper.isNullOrEmpty(form.getSubmitSearch()) &&
		//			StringHelper.isNullOrEmpty(form.getPhNbr()) &&
		//			request.getSession().getAttribute("savePhone") != null) {
		//			form.setPhNbr((String)request.getSession().
		//				getAttribute("savePhone"));
		//			form.setSubmitSearch("Search");
		//		}
		//				
		//  go to add a new blocked phone screen
		if (!StringHelper.isNullOrEmpty(form.getSubmitNew()))
		{
			if (!StringHelper.isNullOrEmpty(form.getPhNbr()))
			{
				 request.setAttribute("passedPhNbr", phoneNumberConversion(form.getPhNbr()));
			}
			form.setAction("add");
			return mapping.findForward("editPhoneBlockedList");
		}

		if (up.hasPermission("editPhoneBlockedList"))
		{
			request.getSession().setAttribute("phoneAuth", "Y");
		} else
		{
			request.getSession().setAttribute("phoneAuth", "N");
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitSearch())
			|| !StringHelper.isNullOrEmpty(form.getSubmitShowAll()))
		{

			try
			{
				ServiceFactory sf = ServiceFactory.getInstance();
				FraudService fs = sf.getFraudService();

				if (!StringHelper.isNullOrEmpty(form.getSubmitShowAll()))
				{
					String tmp = null;
					blockedPhones = fs.getBlockedPhones(up.getUID(), tmp);
				} else
				{
					blockedPhones =
						fs.getBlockedPhones(up.getUID(), form.getPhNbr());
				}

				if (blockedPhones == null || blockedPhones.size() == 0)
				{
					errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError(
							"errors.no.match.record.found",
							"Blocked Phone"));
					saveErrors(request, errors);
					//				} else {
					//					request.getSession().setAttribute("savePhone",
					//													form.getPhNbr());
				}

			} catch (TooManyResultException e)
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError(
						"error.too.many.results",
						dynProps.getMaxDownloadTransactions() + ""));
				saveErrors(request, errors);
			}

			BlockedBean.sortBy = BlockedBean.SORT_BY_CLEAR_TEXT;
			Collections.sort((List) blockedPhones);

			request.setAttribute("blockedPhones", blockedPhones);
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
	 private String phoneNumberConversion(String phone) {
	        String phoneNo = phone.trim();
	        StringBuilder sb = new StringBuilder();
	        String[] phoneNumber = phoneNo.split(" ");
	        if (phoneNumber.length >= 3) {
	            sb.append("0");
	        }
	        if (phoneNumber.length == 0) {
	            sb.append(phoneNo);
	        }
	        for (int i = 0; i < phoneNumber.length - 1; i++) {
	            sb.append(phoneNumber[i + 1]);
	        }
	        String formattedNumber = sb.toString().replaceAll("-", "");
	        String formatNumber = formattedNumber.replaceAll(" ", "");
	        return formatNumber;
	 
	    }
}
