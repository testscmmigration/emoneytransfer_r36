package emgadm.blockedList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class ShowBinBlockedListForm extends EMoneyGramAdmBaseValidatorForm {
	private String binText = null;
	private String submitSearch = null;
	private String submitShowAll = null;
	private String submitAdd = null;

	public String getSubmitAdd() {
		return submitAdd;
	}

	public void setSubmitAdd(String string) {
		submitAdd = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);

		binText = null;
		submitSearch = null;
		submitShowAll = null;
		submitAdd = null;
	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);

		if (submitSearch != null) {
			if (StringHelper.isNullOrEmpty(binText)) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"errors.required", "Credit Card Bin Number"));
			} else if (StringHelper.containsNonDigits(binText)) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"errors.integer", "Credit Card Bin Number"));
			} else if (binText.length() != 6) {
				String[] parms = { "Credit Card Bin Number", "6" };
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"error.length.wrong", parms));
			}

		}

		return errors;
	}

	/**
	 * @return
	 */
	public String getBinText() {
		return binText;
	}

	/**
	 * @return
	 */
	public String getSubmitSearch() {
		return submitSearch;
	}

	/**
	 * @param string
	 */
	public void setBinText(String string) {
		binText = string;
	}

	/**
	 * @param string
	 */
	public void setSubmitSearch(String string) {
		submitSearch = string;
	}

	/**
	 * @return
	 */
	public String getSubmitShowAll() {
		return submitShowAll;
	}

	/**
	 * @param string
	 */
	public void setSubmitShowAll(String string) {
		submitShowAll = string;
	}

}
