package emgadm.blockedList;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class AddBlockedListForm extends EMoneyGramAdmBaseValidatorForm
{
	private String custId = null;
	private String emailDomain = null;
	private String emailAddress = null;
	private String phoneNumber = null;
	private String binText = null;
	private String maskedCC = null;
	private String hashedCC = null;
	private String binNumber = null;
	private String singleIPSelect = null;
	private String ipAddress1 = null;
	private String blockEmail = null;
	private String blockPhone = null;
	private String blockBin = null;
	private String blockCC = null;
	private String blockIP = null;
	private String blockABANumber = null;
	private String blockAccountNumber = null;
	private String submitProcess = null;
	private String submitCancel = null;
	private String blockEmailIndicator = null;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);

		emailDomain = null;
		emailAddress = null;

		submitProcess = null;
		submitCancel = null;
		blockEmail = null;
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors  = super.validate(mapping, request);
        if (submitProcess != null) {
        	String[] checkboxes = {blockEmail, blockABANumber, blockAccountNumber, blockBin, blockCC};
        	int checkedCheckboxes = 0;
        	for (String checkbox : checkboxes) {
        		++checkedCheckboxes;
        	}
        	if (checkedCheckboxes < 1) {
        		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "Select at least one checkbox"));
        	}
        }
		return errors;
	}

	public String getSubmitCancel() {
		return submitCancel;
	}

	public String getSubmitProcess() {
		return submitProcess;
	}

	public void setSubmitCancel(String string) {
		submitCancel = string;
	}

	public void setSubmitProcess(String string) {
		submitProcess = string;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getEmailDomain() {
		return emailDomain;
	}

	public void setEmailAddress(String string) {
		emailAddress = string;
	}

	public void setEmailDomain(String string) {
		emailDomain = string;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getBlockEmail() {
		return blockEmail;
	}

	public void setBlockEmail(String blockEmail) {
		this.blockEmail = blockEmail;
	}

	public String getBlockPhone() {
		return blockPhone;
	}

	public void setBlockPhone(String blockPhone) {
		this.blockPhone = blockPhone;
	}

	public String getBlockBin() {
		return blockBin;
	}

	public void setBlockBin(String blockBin) {
		this.blockBin = blockBin;
	}

	public String getBlockCC() {
		return blockCC;
	}

	public void setBlockCC(String blockCC) {
		this.blockCC = blockCC;
	}

	public String getBlockIP() {
		return blockIP;
	}

	public void setBlockIP(String blockIP) {
		this.blockIP = blockIP;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getBinText() {
		return binText;
	}

	public void setBinText(String binText) {
		this.binText = binText;
	}

	public String getMaskedCC() {
		return maskedCC;
	}

	public void setMaskedCC(String maskedCC) {
		this.maskedCC = maskedCC;
	}

	public String getHashedCC() {
		return hashedCC;
	}

	public void setHashedCC(String hashedCC) {
		this.hashedCC = hashedCC;
	}

	public String getBinNumber() {
		return binNumber;
	}

	public void setBinNumber(String binNumber) {
		this.binNumber = binNumber;
	}

	public String getSingleIPSelect() {
		return singleIPSelect;
	}

	public void setSingleIPSelect(String singleIPSelect) {
		this.singleIPSelect = singleIPSelect;
	}

	public String getIpAddress1() {
		return ipAddress1;
	}

	public void setIpAddress1(String ipAddress1) {
		this.ipAddress1 = ipAddress1;
	}

	public String getBlockEmailIndicator() {
		return blockEmailIndicator;
	}

	public void setBlockEmailIndicator(String blockEmailIndicator) {
		this.blockEmailIndicator = blockEmailIndicator;
	}

	public String getBlockABANumber() {
		return blockABANumber;
	}

	public void setBlockABANumber(String blockABANumber) {
		this.blockABANumber = blockABANumber;
	}

	public String getBlockAccountNumber() {
		return blockAccountNumber;
	}

	public void setBlockAccountNumber(String blockAccountNumber) {
		this.blockAccountNumber = blockAccountNumber;
	}

	
}
