package emgadm.blockedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.services.DecryptionService;
import emgadm.util.StringHelper;
import emgshared.model.BlockedBean;
import emgshared.services.FraudService;
import emgshared.services.ObfuscationService;
import emgshared.services.ServiceFactory;

/**
 * @version 1.0
 * @author
 */
public class SaveBinBlockedListAction extends EMoneyGramAdmBaseAction {
	public void saveDB(SaveBinBlockedListForm form, HttpServletRequest request)
			throws Exception {
		ServiceFactory sf = ServiceFactory.getInstance();
		FraudService fs = sf.getFraudService();
		UserProfile up = getUserProfile(request);
		BlockedBean blockedBin = (BlockedBean) request.getSession()
				.getAttribute("blockedBin");
		fs.setBlockedBin(up.getUID(), blockedBin);
	}

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		SaveBinBlockedListForm saveBinBlockedListForm = (SaveBinBlockedListForm) form;
		UserProfile up = getUserProfile(request);
		ServiceFactory sf = ServiceFactory.getInstance();
		FraudService fs = sf.getFraudService();

		ObfuscationService os = sf.getObfuscationService();
		DecryptionService ds = emgadm.services.ServiceFactory.getInstance()
				.getDecryptionService();

		if (!StringHelper.isNullOrEmpty(saveBinBlockedListForm
				.getSubmitDBCancel())) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_BIN_BLOCKED_LIST);
		}

		BlockedBean blockedBin = (BlockedBean) request.getSession()
				.getAttribute("blockedBin");

		Collection blockedBins = fs.getBlockedBins(up.getUID(),
				blockedBin.getHashText());
		for (Iterator it = blockedBins.iterator(); it.hasNext();) {
			BlockedBean br = (BlockedBean) it.next();
			br.setClearText(ds.decryptCreditCardBinNumber(br
					.getEncryptedNumber()));
		}

		request.setAttribute("blockedBins", blockedBins);

		// Getting all the customer profiles that get affected
		String bin = blockedBin.getClearText();
		String binHash = os.getBinHash(bin);
		Collection customerProfileAccounts = fs.getConsumersByAccounts(up.getUID(),
				binHash, null, true, 0);

		request.setAttribute("customerProfileAccounts", customerProfileAccounts);

		if (!StringHelper.isNullOrEmpty(saveBinBlockedListForm
				.getSubmitDBSave())) {
			fs.setBlockedBin(up.getUID(), blockedBin);

			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
					"msg.blockedbin.added"));
			saveErrors(request, errors);

			request.setAttribute("blockedBins", null);

			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_BIN_BLOCKED_LIST);
		}

		return mapping
				.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
