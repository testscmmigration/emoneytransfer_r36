package emgadm.blockedList;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class AddCCBlockedListForm extends EMoneyGramAdmBaseValidatorForm
{
	private String maskedCC = null;
	private String hashedCC = null;
	private String binNumber = null;
	private int    cardNumberLength;
	private String blockedId = null;
	private String riskLevel = null;	
	private String blockingReason = null;
	private String blockingComment = null;
	private String blockingAction = null;	
	private String submitProcess = null;
	private String submitCancel = null;
	private String cardToken = null;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);

		submitProcess = null;
		submitCancel = null;
	}
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request){
		ActionErrors errors = super.validate(mapping, request);
		
		if (submitProcess != null){

//			if (StringHelper.isNullOrEmpty(fullCC)){ 
//				errors.add(ActionErrors.GLOBAL_ERROR, 
//					new ActionError("errors.required", "Credit Card Number"));
//			} else if (StringHelper.containsNonDigits(fullCC)){
//				errors.add(ActionErrors.GLOBAL_ERROR, 
//					new ActionError("errors.integer", "Credit Card number"));				
//			}

			
			if (blockingReason.equals("None")){ 
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "Reason for blocking"));
			}
			
			if (blockingAction.equals("None")){ 
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "Action"));
			}
							
			if (StringHelper.isNullOrEmpty(blockingComment))
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "Comment for blocking Credit Card"));
		

			if (!StringHelper.isNullOrEmpty(riskLevel)){
				try{
					if (Integer.parseInt(riskLevel) < 0){ 
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.invalid.risklevel", "Risk Level must be between 0 to 999."));
					}
				}catch(NumberFormatException nfe){
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.invalid.risklevel", "Risk Level must be between 0 to 999."));
				}
			}
							
		} 			

		return errors;
	}

	

	/**
	 * @return
	 */
	public String getBlockingAction() {
		return blockingAction;
	}

	/**
	 * @return
	 */
	public String getBlockingComment() {
		return blockingComment;
	}

	/**
	 * @return
	 */
	public String getBlockingReason() {
		return blockingReason;
	}


	/**
	 * @return
	 */
	public String getRiskLevel() {
		return riskLevel;
	}

	/**
	 * @return
	 */
	public String getSubmitCancel() {
		return submitCancel;
	}

	/**
	 * @return
	 */
	public String getSubmitProcess() {
		return submitProcess;
	}

	/**
	 * @param string
	 */
	public void setBlockingAction(String string) {
		blockingAction = string;
	}

	/**
	 * @param string
	 */
	public void setBlockingComment(String string) {
		blockingComment = string;
	}

	/**
	 * @param string
	 */
	public void setBlockingReason(String string) {
		blockingReason = string;
	}

	/**
	 * @param string
	 */
	public void setRiskLevel(String string) {
		riskLevel = string;
	}

	/**
	 * @param string
	 */
	public void setSubmitCancel(String string) {
		submitCancel = string;
	}

	/**
	 * @param string
	 */
	public void setSubmitProcess(String string) {
		submitProcess = string;
	}

	public String getMaskedCC() {
		return maskedCC;
	}

	public void setMaskedCC(String maskedCC) {
		this.maskedCC = maskedCC;
	}

	public String getHashedCC() {
		return hashedCC;
	}

	public void setHashedCC(String hashedCC) {
		this.hashedCC = hashedCC;
	}

	public String getBinNumber() {
		return binNumber;
	}

	public void setBinNumber(String binNumber) {
		this.binNumber = binNumber;
	}

	public int getCardNumberLength() {
		return cardNumberLength;
	}

	public void setCardNumberLength(int cardNumberLength) {
		this.cardNumberLength = cardNumberLength;
	}
	
	public String getBlockedId() {
		return blockedId;
	}

	public void setBlockedId(String blockedId) {
		this.blockedId = blockedId;
	}

	public String getCardToken() {
		return cardToken;
	}

	public void setCardToken(String cardToken) {
		this.cardToken = cardToken;
	}

}
