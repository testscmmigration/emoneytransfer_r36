package emgadm.blockedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.CommandAuth;
import emgadm.model.UserProfile;
import emgadm.services.DecryptionService;
import emgadm.util.StringHelper;
import emgshared.exceptions.MaxRowsHashCryptoException;
import emgshared.model.BlockedBean;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.FraudService;
import emgshared.services.ObfuscationService;
import emgshared.services.ServiceFactory;

/**
 * @version 	1.0
 * @author
 */
public class ShowBankBlockedListAction extends EMoneyGramAdmBaseAction{
	private static EMTSharedDynProperties dynProps =
		new EMTSharedDynProperties();

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		ShowBankBlockedListForm showBankBlockedListForm = (ShowBankBlockedListForm) form;
		UserProfile up = getUserProfile(request);
		CommandAuth ca = new CommandAuth();
		Collection accts = new ArrayList();

		if (up.hasPermission(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_ADD_BANK_BLOCKED_LIST)) {
			ca.setAddBankBlockedList(true);
		} else {
			ca.setAddBankBlockedList(false);
		}
		request.getSession().setAttribute("auth", ca);

		if (!StringHelper.isNullOrEmpty(showBankBlockedListForm.getSubmitAdd())) {
			request.setAttribute("abaNumber", showBankBlockedListForm
					.getBankAba().trim());
			request.setAttribute("acctNumber", showBankBlockedListForm
					.getBankFull().trim());

			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_ADD_BANK_BLOCKED_LIST);
		}

		if (!StringHelper.isNullOrEmpty(showBankBlockedListForm
				.getSubmitShowAll())
				|| !StringHelper.isNullOrEmpty(showBankBlockedListForm
						.getSubmitAcctSearch())
				|| !StringHelper.isNullOrEmpty(showBankBlockedListForm
						.getSubmitMaskSearch())) {

			try {
				ServiceFactory sf = ServiceFactory.getInstance();
				FraudService fs = sf.getFraudService();
				ObfuscationService os = sf.getObfuscationService();
				DecryptionService ds = emgadm.services.ServiceFactory
						.getInstance().getDecryptionService();

				String hashText = StringHelper
						.isNullOrEmpty(showBankBlockedListForm.getBankFull()) == true ? null
						: os.getCreditCardHash(showBankBlockedListForm
								.getBankFull());

				if (!StringHelper.isNullOrEmpty(showBankBlockedListForm
						.getSubmitAcctSearch())) {
					accts = fs.getBlockedBankAccounts(up.getUID(),
							showBankBlockedListForm.getBankAba(), hashText,
							null);
				} else if (!StringHelper.isNullOrEmpty(showBankBlockedListForm
						.getSubmitMaskSearch())) {
					accts = fs.getBlockedBankAccounts(up.getUID(), null, null,
							showBankBlockedListForm.getBankMask());
				} else {
					accts = fs.getBlockedBankAccounts(up.getUID(), null, null,
							null);
				}

				if (accts == null || accts.size() == 0) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"error.no.blockedbank.found"));
					saveErrors(request, errors);
				} else {
					for (Iterator it = accts.iterator(); it.hasNext();) {
						BlockedBean acct = (BlockedBean) it.next();

						acct.setClearText(ds.decryptBankAccountNumber(acct
								.getEncryptedNumber()));
						if (acct.getClearText().equals("**********"))
							acct.setClearText(null);
					}
					BlockedBean.sortBy = BlockedBean.SORT_BY_ABA_NUMBER;
					Collections.sort((List) accts);
					request.setAttribute("accts", accts);
				}

			} catch (MaxRowsHashCryptoException te) {
				errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError("error.too.many.results", dynProps
								.getMaxRowsHashCrypto() + ""));
				saveErrors(request, errors);
			}
		}

		return mapping
				.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
