package emgadm.blockedList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class ShowEmailBlockedListForm extends EMoneyGramAdmBaseValidatorForm {
	private String emailAddress = null;
	private String emailDomain = null;
	private String submitEmailSearch = null;
	private String submitDomainSearch = null;
	private String submitShowAll = null;
	private String submitAdd = null;

	public String getSubmitAdd() {
		return submitAdd;
	}

	public void setSubmitAdd(String string) {
		submitAdd = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);

		emailAddress = null;
		emailDomain = null;
		submitEmailSearch = null;
		submitDomainSearch = null;
		submitShowAll = null;
		submitAdd = null;
	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);

		if (submitEmailSearch != null) {
			if (StringHelper.isNullOrEmpty(emailAddress)) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"errors.required", "Email Address"));
			}
		}

		if (submitDomainSearch != null) {
			if (StringHelper.isNullOrEmpty(emailDomain)) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"errors.required", "Email Domain"));
			}
		}

		return errors;
	}

	/**
	 * @return
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @return
	 */
	public String getEmailDomain() {
		return emailDomain;
	}

	/**
	 * @return
	 */
	public String getSubmitEmailSearch() {
		return submitEmailSearch;
	}

	/**
	 * @param string
	 */
	public void setEmailAddress(String string) {
		emailAddress = string;
	}

	/**
	 * @param string
	 */
	public void setEmailDomain(String string) {
		emailDomain = string;
	}

	/**
	 * @param string
	 */
	public void setSubmitEmailSearch(String string) {
		submitEmailSearch = string;
	}

	/**
	 * @return
	 */
	public String getSubmitDomainSearch() {
		return submitDomainSearch;
	}

	/**
	 * @param string
	 */
	public void setSubmitDomainSearch(String string) {
		submitDomainSearch = string;
	}

	/**
	 * @return
	 */
	public String getSubmitShowAll() {
		return submitShowAll;
	}

	/**
	 * @param string
	 */
	public void setSubmitShowAll(String string) {
		submitShowAll = string;
	}

}
