package emgadm.blockedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.model.BlockedIP;
import emgshared.model.BlockedReason;
import emgshared.model.BlockedStatus;
import emgshared.services.FraudService;
import emgshared.services.ServiceFactory;

/**
 * @version 1.0
 * @author
 */
public class AddIPBlockedListAction extends EMoneyGramAdmBaseAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		AddIPBlockedListForm addIPBlockedListForm = (AddIPBlockedListForm) form;
		ServletContext sc = request.getSession().getServletContext();
		UserProfile up = getUserProfile(request);

		ServiceFactory sf = ServiceFactory.getInstance();
		FraudService fs = sf.getFraudService();

		Collection blockingReasons = fs.getBlockedReasons(up.getUID(), "IP");
		Collection blockingActions = fs.getBlockedStatus(up.getUID(), "IP");
		sc.setAttribute("blockingReasons", blockingReasons);
		sc.setAttribute("blockingActions", blockingActions);

		if (!StringHelper.isNullOrEmpty((String) request
				.getAttribute("ipaddress1")))
			addIPBlockedListForm.setIpAddress1((String) request
					.getAttribute("ipaddress1"));

		String ip = request.getParameter("ip");
		if (ip != null) {
			//MBO-4439
			Collection<?> blockedIPs = fs.getBlockedIPs(up.getUID(), ip, null);
			if (blockedIPs.size() > 0) {
				BlockedIP blockedIP = (BlockedIP) blockedIPs.toArray()[0];
				addIPBlockedListForm.setIpAddress1(ip);
				addIPBlockedListForm.setBlockingReason(blockedIP
						.getReasonCode());
				addIPBlockedListForm.setBlockingAction(blockedIP
						.getStatusCode());
				addIPBlockedListForm.setRiskLevel(blockedIP
						.getFraudRiskLevelCode());
				addIPBlockedListForm.setBlockingComment(blockedIP.getComment());
			}
		}

		if (!StringHelper.isNullOrEmpty(addIPBlockedListForm.getSubmitCancel())) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_IP_BLOCKED_LIST);
		}

		if (!StringHelper.isNullOrEmpty(addIPBlockedListForm.getSubmitSave())) {
			// MBO-2847 changes starts
			boolean isValidToken = isTokenValid(request);
			if (!isValidToken) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"error.csrf"));
				saveErrors(request, errors);
				request.getSession().invalidate();
				return mapping.findForward("errorPage");
			}
			// MBO-2847 changes ends
			BlockedIP blockedIP = new BlockedIP();
			blockedIP
					.setIpAddress1(addIPBlockedListForm.getIpAddress1().trim());
			blockedIP
					.setIpAddress2(addIPBlockedListForm.getIpAddress2().trim());
			blockedIP.setReasonCode(addIPBlockedListForm.getBlockingReason());
			blockedIP.setComment(addIPBlockedListForm.getBlockingComment());
			blockedIP.setStatusCode(addIPBlockedListForm.getBlockingAction());
			blockedIP
					.setFraudRiskLevelCode(addIPBlockedListForm.getRiskLevel());

			for (Iterator it = blockingReasons.iterator(); it.hasNext();) {
				BlockedReason br = (BlockedReason) it.next();
				if (br.getReasonCode().equals(
						addIPBlockedListForm.getBlockingReason())) {
					blockedIP.setReasonDesc(br.getReasonDesc());
					break;
				}
			}

			for (Iterator it = blockingActions.iterator(); it.hasNext();) {
				BlockedStatus bs = (BlockedStatus) it.next();
				if (bs.getBlockedStatusCode().equals(
						addIPBlockedListForm.getBlockingAction())) {
					blockedIP.setStatusDesc(bs.getBlockedStatusDesc());
					break;
				}
			}

			request.getSession().setAttribute("blockedIP", blockedIP);

			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SAVE_IP_BLOCKED_LIST);

		}

		return mapping
				.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
