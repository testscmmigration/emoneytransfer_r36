package emgadm.blockedList;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class AddBankBlockedListForm extends EMoneyGramAdmBaseValidatorForm
{
	private String abaNumber = null;
	private String fullAcct = null;
	private String riskLevel = null;	
	private String blockingReason = null;
	private String blockingComment = null;
	private String blockingAction = null;	
	private String submitProcess = null;
	private String submitCancel = null;
	

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);

		abaNumber = null;
		fullAcct = null;
		riskLevel = null;	
		blockingReason = null;
		blockingComment = null;
		blockingAction = null;
		submitProcess = null;
		submitCancel = null;
	}
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
	{
		ActionErrors errors = super.validate(mapping, request);
		//MBO-4439
		if (submitProcess != null){

			if (StringHelper.isNullOrEmpty(abaNumber)){ 
				errors.add(ActionErrors.GLOBAL_ERROR, 
					new ActionError("errors.required", "ABA Number"));
			} else if (StringHelper.containsNonDigits(abaNumber)){
				errors.add(ActionErrors.GLOBAL_ERROR, 
					new ActionError("errors.integer", "ABA Number"));
			} else if (abaNumber.trim().length() < 9){
				errors.add(ActionErrors.GLOBAL_ERROR, 
					new ActionError("errors.required", "Invalid ABA Number. Length of 9 digits"));				
			}

			if (!StringHelper.isNullOrEmpty(fullAcct)){
				if (fullAcct.length() < 4)
					errors.add(ActionErrors.GLOBAL_ERROR, 
						new ActionError("errors.required", "For Bank Account Number, atleast 4 digits"));				
				 
				if (StringHelper.containsNonDigits(fullAcct))
					errors.add(ActionErrors.GLOBAL_ERROR, 
						new ActionError("errors.integer", "Bank Account Number"));			

			}

			if (blockingReason.equals("None")){ 
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "Reason for blocking"));
			}
			
			if (blockingAction.equals("None")){ 
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "Action"));
			}
							
			if (StringHelper.isNullOrEmpty(blockingComment))
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "Comment for blocking Account Number"));
		

			if (!StringHelper.isNullOrEmpty(riskLevel)){
				try{
					if (Integer.parseInt(riskLevel) < 0){ 
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.invalid.risklevel", "Risk Level must be between 0 to 999."));
					}
				}catch(NumberFormatException nfe){
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.invalid.risklevel", "Risk Level must be between 0 to 999."));
				}
			}
							
		} 			

		return errors;
	}

	

	/**
	 * @return
	 */
	public String getBlockingAction() {
		return blockingAction;
	}

	/**
	 * @return
	 */
	public String getBlockingComment() {
		return blockingComment;
	}

	/**
	 * @return
	 */
	public String getBlockingReason() {
		return blockingReason;
	}

	/**
	 * @return
	 */
	public String getFullAcct() {
		return fullAcct;
	}

	/**
	 * @return
	 */
	public String getRiskLevel() {
		return riskLevel;
	}

	/**
	 * @return
	 */
	public String getSubmitCancel() {
		return submitCancel;
	}

	/**
	 * @return
	 */
	public String getSubmitProcess() {
		return submitProcess;
	}

	/**
	 * @param string
	 */
	public void setBlockingAction(String string) {
		blockingAction = string;
	}

	/**
	 * @param string
	 */
	public void setBlockingComment(String string) {
		blockingComment = string;
	}

	/**
	 * @param string
	 */
	public void setBlockingReason(String string) {
		blockingReason = string;
	}

	/**
	 * @param string
	 */
	public void setFullAcct(String string) {
		if (string != null) {
			string = string.trim();
		}
		fullAcct = string;
	}

	/**
	 * @param string
	 */
	public void setRiskLevel(String string) {
		riskLevel = string;
	}

	/**
	 * @param string
	 */
	public void setSubmitCancel(String string) {
		submitCancel = string;
	}

	/**
	 * @param string
	 */
	public void setSubmitProcess(String string) {
		submitProcess = string;
	}

	/**
	 * @return
	 */
	public String getAbaNumber() {
		return abaNumber;
	}

	/**
	 * @param string
	 */
	public void setAbaNumber(String string) {
		if (string != null) {
			string = string.trim();
		}
		abaNumber = string;
	}

}
