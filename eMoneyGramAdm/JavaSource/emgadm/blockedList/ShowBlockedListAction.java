package emgadm.blockedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;



import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.BlockedAccountBean;
import emgadm.model.CommandAuth;
import emgadm.model.UserProfile;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.model.BlockedBean;
import emgshared.model.ConsumerBankAccount;
import emgshared.model.ConsumerCreditCardAccount;
import emgshared.model.ConsumerProfile;
import emgshared.services.ConsumerProfileService;
import emgshared.services.FraudService;
import emgshared.services.PCIService;
import emgshared.services.ServiceFactory;

public class ShowBlockedListAction extends EMoneyGramAdmBaseAction {

	private ConsumerProfileService profileService = ServiceFactory
			.getInstance().getConsumerProfileService();

	private static final PCIService pciService = emgshared.services.ServiceFactory
			.getInstance().getPCIService();

	private static final Logger LOGGER = EMGSharedLogger.getLogger(
			ShowBlockedListAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		LOGGER.debug("Enter excute.");

		ShowBlockedListForm showBlockedListForm = (ShowBlockedListForm) form;

		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		FraudService fraudService = serviceFactory.getFraudService();
		UserProfile userProfile = getUserProfile(request);		
		
		fillBlockingCommentsAndReasons(request, fraudService, userProfile);
		request.getSession().setAttribute("pciCardNotFound", false);
		CommandAuth commandAuth = new CommandAuth();

		commandAuth
				.setAddBankBlockedList(userProfile
						.hasPermission(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_ADD_BANK_BLOCKED_LIST));
		commandAuth
				.setAddCCBlockedList(userProfile
						.hasPermission(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_ADD_CC_BLOCKED_LIST));
		request.getSession().setAttribute("auth", commandAuth);

		String custId = showBlockedListForm.getCustId();
		Integer id = Integer.valueOf(custId);

		ConsumerProfile consumerProfile = null;
		consumerProfile = profileService.getConsumerProfile(id,
				userProfile.getUID(), null,false);

	
		showBlockedListForm.setPrimaryPhoneNumber(consumerProfile.getPhoneNumber());
		request.getSession().setAttribute("blockPrimaryPhoneNumber",
					consumerProfile.getPhoneNumber());
		
		showBlockedListForm.setAltPhoneNumber(consumerProfile.getPhoneNumberAlternate());
		request.getSession().setAttribute("blockAltPhoneNumber",
					consumerProfile.getPhoneNumberAlternate());
		

		ConsumerCreditCardAccount[] creditCards = consumerProfile
				.getCreditCardAccounts();

		ConsumerBankAccount[] consumerBankAccountArray = consumerProfile
				.getBankAccounts();

		List<BlockedAccountBean> blockCCList = getUniqueCreditCards(
				creditCards, commandAuth.isAddCCBlockedList(), request);
		List<BlockedAccountBean> blockBankList = getUniqueBankCards(
				consumerBankAccountArray, commandAuth.isAddBankBlockedList());

		request.getSession().setAttribute("blockCCList", blockCCList);
		request.getSession().setAttribute("blockBankList", blockBankList);
//		if(StringUtils.isAlphanumeric(custId)){
			request.getSession().setAttribute("customer",id);
//		}

		LOGGER.debug("Exit excute.");

		return mapping
				.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	/**
	 * getUniqueCreditCards. will iterate through ConsumerCreditCardAccount
	 * array & will ONLY add new BlockedAccountBean item to list
	 * 
	 * @param consumerCreditCardAccountArray
	 * @param authorized
	 * @param request
	 * @return blockedAccountBeanList
	 */
	private List<BlockedAccountBean> getUniqueCreditCards(
			ConsumerCreditCardAccount[] consumerCreditCardAccountArray,
			boolean authorized, HttpServletRequest request) {

		LOGGER.debug("Enter getUniqueCreditCards.");

		List<BlockedAccountBean> blockedAccountBeanList = new ArrayList<BlockedAccountBean>();
		Map<String, BlockedAccountBean> blockedAccountBeanMap = new HashMap<String, BlockedAccountBean>();

		for (ConsumerCreditCardAccount consumerCreditCardAccount : consumerCreditCardAccountArray) {

			BlockedAccountBean blockedAccountBean = new BlockedAccountBean();			
			try {				
				if (consumerCreditCardAccount.getCardToken() != null) {
					UserProfile up = getUserProfile(request);
					Collection<?> blockedEntries = ServiceFactory.getInstance().getFraudService().getBlockedCCAccounts(up.getUID(), null, null, consumerCreditCardAccount.getCardToken());
					if(null!= blockedEntries && blockedEntries.size()>0){
					BlockedBean blockedEntry = (BlockedBean) blockedEntries.toArray()[0];
					String blockedId = blockedEntry.getBlockedId();
					if (blockedId.trim().equals("")) {
						blockedAccountBean.setBlockedCCId("");
					} else {
						blockedAccountBean.setBlockedCCId(blockedId.trim());
					}
					}
				} else {
					request.getSession().setAttribute("pciCardNotFound", true);
				}
			} catch (Exception exception) {
				// note available in PCI DB
				request.getSession().setAttribute("pciCardNotFound", true);
			}
			Boolean isavailablePcidb = (Boolean) request.getSession()
					.getAttribute("pciCardNotFound");
			if (!isavailablePcidb) {
				blockedAccountBean.setBlock(authorized);
				blockedAccountBean.setMaskedText(consumerCreditCardAccount
						.getAccountNumberBin()
						.concat("*")
						.concat(consumerCreditCardAccount
								.getAccountNumberMask()));
				blockedAccountBean.setEncryptedText(null);
				blockedAccountBean.setCardToken(consumerCreditCardAccount.getCardToken());
				blockedAccountBeanMap.put(
						consumerCreditCardAccount.getAccountNumberBin().concat(
								consumerCreditCardAccount
										.getAccountNumberMask()),
						blockedAccountBean);
			}
		}

		for (BlockedAccountBean blockedAccountBean : blockedAccountBeanMap
				.values()) {
			blockedAccountBeanList.add(blockedAccountBean);
		}

		LOGGER.debug("Exit getUniqueCreditCards.");

		return blockedAccountBeanList;

	}

	/**
	 * 
	 * getUniqueBankCards will iterate through ConsumerbankAccount array & will ONLY add
	 * new BlockedAccountBean item to list
	 * @param consumerBankAccountArray
	 * @param authorized
	 * @return blockedAccountBeanList
	 */
	private List<BlockedAccountBean> getUniqueBankCards(
			ConsumerBankAccount[] consumerBankAccountArray, boolean authorized) {

		LOGGER.debug("Enter getUniqueBankCards.");

		Map<String, BlockedAccountBean> blockedAccountBeanMap = new HashMap<String, BlockedAccountBean>();

		for (ConsumerBankAccount consumerBankAccount : consumerBankAccountArray) {

			BlockedAccountBean blockedAccountBean = new BlockedAccountBean();
			blockedAccountBean.setMaskedText(consumerBankAccount
					.getAccountNumberMask());
			blockedAccountBean.setAba(consumerBankAccount
					.getABANumber());			
			blockedAccountBean.setEncryptedText(consumerBankAccount
					.getEncryptedText());
			blockedAccountBean.setBlock(authorized);

			blockedAccountBeanMap.put(consumerBankAccount.getEncryptedText(),
					blockedAccountBean);
		}

		List<BlockedAccountBean> blockedAccountBeanList = new ArrayList<BlockedAccountBean>();
		blockedAccountBeanList.addAll(blockedAccountBeanMap.values());

		LOGGER.debug("Exit getUniqueBankCards.");

		return blockedAccountBeanList;

	}

	/**
	 * fillingBlockingCommentsAndReason will populate the comments and reason
	 * from database.
	 * 
	 * @param request
	 * @param fraudService
	 * @param userProfile
	 * @throws Exception
	 */
	private void fillBlockingCommentsAndReasons(HttpServletRequest request,
			FraudService fraudService, UserProfile userProfile)
			throws Exception {

		LOGGER.debug("Enter getUniqueBankCards.");

		String[] types = { "EML", "PHON", "BANK", "BIN", "CC", "IP" };
		ServletContext servletContext = request.getSession()
				.getServletContext();
		String blockingReason = "blockingReason";
		String blockingAction = "blockingAction";
		for (String type : types) {
			Collection blockReason = fraudService.getBlockedReasons(
					userProfile.getUID(), type);
			Collection blockStatus = fraudService.getBlockedStatus(
					userProfile.getUID(), type);
			servletContext.setAttribute(blockingReason + type, blockReason);
			servletContext.setAttribute(blockingAction + type, blockStatus);
		}

		LOGGER.debug("Exit getUniqueBankCard.");

	}

}
