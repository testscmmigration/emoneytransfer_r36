package emgadm.blockedList;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
//import org.owasp.esapi.ESAPI;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.model.BlockedAccountBean;
import emgadm.util.StringHelper;

public class ShowBlockedListForm extends EMoneyGramAdmBaseValidatorForm {

	private String custId = null;
	private String primaryPhoneNumber = null;
	private String altPhoneNumber = null;
	private String blockPrimaryPhoneNumber = null;
	private String blockAltPhoneNumber = null;
	private String blockCC = null;
	private String blockAccountNumber = null;
	private String submitProcess = null;
	private String submitCancel = null;
	private String blockingAction = null;
	private String blockingReason = null;
	private String blockingComment = null;
	private String blockingActionCC = null;
	private String blockingReasonCC = null;
	private List<String> blockedStatusCode = null;
	private List<String> reasonCode = null;
	private String blockedStatusDesc = null;
	private String reasonDesc = null;

	

	public String getBlockPrimaryPhoneNumber() {
		return blockPrimaryPhoneNumber;
	}

	public void setBlockPrimaryPhoneNumber(String blockPrimaryPhoneNumber) {
		this.blockPrimaryPhoneNumber = blockPrimaryPhoneNumber;
	}

	BlockedAccountBean blockedaccountbean = new BlockedAccountBean();

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		primaryPhoneNumber = null;
		altPhoneNumber = null;
		blockPrimaryPhoneNumber = null;
		blockCC = null;
		blockAccountNumber = null;
		blockAltPhoneNumber = null;
		blockingReason = null;
		blockingComment = null;
		blockingAction = null;
		submitProcess = null;
		submitCancel = null;

	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);
		boolean checkMarkFound = false;
		
		

		@SuppressWarnings("unchecked")
		List<BlockedAccountBean> blockCCList = (List<BlockedAccountBean>) request
				.getSession().getAttribute("blockCCList");
		@SuppressWarnings("unchecked")
		List<BlockedAccountBean> blockBankList = (List<BlockedAccountBean>) request
				.getSession().getAttribute("blockBankList");

		if (submitProcess != null) {

			 
			  String[] checkboxes = { blockAccountNumber, blockCC }; int
			  checkedCheckboxes = 0; 
			  
			  for ( String checkbox : checkboxes) {
			  ++checkedCheckboxes; 
			  } 
			  if (checkedCheckboxes < 1) {
			  errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
			   "label.blocked.error.noneselected")); }
			 

			if (StringHelper.isNullOrEmpty(request
					.getParameter("primaryPhoneBlocker"))
					|| StringHelper.isNullOrEmpty(request
							.getParameter("altPhoneBlocker"))) {
				checkMarkFound = false;

			}

			//  spin through blockCCList and blockBankList and check
			// if 'block' field is 'true' for any

			for (BlockedAccountBean blockedaccountbean : blockBankList) {
				if (blockedaccountbean.isBlock()) {
					checkMarkFound = true;
					break;
				}

			}

			for (BlockedAccountBean blockedaccountbean : blockCCList) {
				if (blockedaccountbean.isBlock()) {
					checkMarkFound = true;
					break;
				}

			}

			if (!checkMarkFound) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"label.blocked.error.noneselected"));

			}

			if (blockingReason.equals("None")) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"option.select.one"));
			}

			
			if (StringHelper.isNullOrEmpty(blockingComment)) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"label.add.comment"));
			}
			//blockingReason = ESAPI.encoder().encodeForHTMLAttribute(blockingReason);
			//blockingAction = ESAPI.encoder().encodeForHTMLAttribute(blockingAction);
			//blockingComment = ESAPI.encoder().encodeForHTMLAttribute(blockingComment);
			//MBO-9993
			if (isAlphaNumBlockerValid(blockingReason)){
			request.getSession().setAttribute("blockingReason",blockingReason);
			}
			if(isAlphaNumBlockerValid(blockingAction)){
			request.getSession().setAttribute("blockingAction",blockingAction);
			}
			if(isAlphaNumBlockerValid(blockingComment)){
				request.getSession().setAttribute("blockingComment",blockingComment);
			}

		}

		return errors;

	}

	public String getSubmitCancel() {
		return submitCancel;
	}

	public String getSubmitProcess() {
		return submitProcess;
	}

	public void setSubmitCancel(String string) {
		submitCancel = string;
	}

	public void setSubmitProcess(String string) {
		submitProcess = string;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getBlockPhoneNumber() {
		return blockPrimaryPhoneNumber;
	}

	public void setBlockPhoneNumber(String blockPhoneNumber) {
		this.blockPrimaryPhoneNumber = blockPhoneNumber;
	}

	public String getBlockAltPhoneNumber() {
		return blockAltPhoneNumber;
	}

	public void setBlockAltPhoneNumber(String blockAltPhoneNumber) {
		this.blockAltPhoneNumber = blockAltPhoneNumber;
	}

	public String getBlockCC() {
		return blockCC;
	}

	public void setBlockCC(String blockCC) {
		this.blockCC = blockCC;
	}

	public String getPrimaryPhoneNumber() {
		return primaryPhoneNumber;
	}

	public void setPrimaryPhoneNumber(String primaryPhoneNumber) {
		this.primaryPhoneNumber = primaryPhoneNumber;
	}

	public String getAltPhoneNumber() {
		return altPhoneNumber;
	}

	public void setAltPhoneNumber(String altPhoneNumber) {
		this.altPhoneNumber = altPhoneNumber;
	}

	public String getBlockAccountNumber() {
		return blockAccountNumber;
	}

	public void setBlockAccountNumber(String blockAccountNumber) {
		this.blockAccountNumber = blockAccountNumber;
	}

	public String getBlockingAction() {
		return blockingAction;
	}

	public void setBlockingAction(String blockingAction) {
		this.blockingAction = blockingAction;
	}

	public String getBlockingReason() {
		return blockingReason;
	}

	public void setBlockingReason(String blockingReason) {
		this.blockingReason = blockingReason;
	}

	public String getBlockingComment() {
		return blockingComment;
	}

	public void setBlockingComment(String blockingComment) {
		this.blockingComment = blockingComment;
	}

	public String getBlockingActionCC() {
		return blockingActionCC;
	}

	public void setBlockingActionCC(String blockingActionCC) {
		this.blockingActionCC = blockingActionCC;
	}

	public String getBlockingReasonCC() {
		return blockingReasonCC;
	}

	public void setBlockingReasonCC(String blockingReasonCC) {
		this.blockingReasonCC = blockingReasonCC;
	}

	public List<String> getBlockedStatusCode() {
		return blockedStatusCode;
	}

	public void setBlockedStatusCode(List<String> blockedStatusCode) {
		this.blockedStatusCode = blockedStatusCode;
	}

	public List<String> getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(List<String> reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getBlockedStatusDesc() {
		return blockedStatusDesc;
	}

	public void setBlockedStatusDesc(String blockedStatusDesc) {
		this.blockedStatusDesc = blockedStatusDesc;
	}

	public String getReasonDesc() {
		return reasonDesc;
	}

	public void setReasonDesc(String reasonDesc) {
		this.reasonDesc = reasonDesc;
	}
	//MBO-9993 to validate all characters
	private static boolean isAlphaNumBlockerValid(String name){
		Matcher matcher = null;
		Pattern pattern = Pattern.compile("[0-9a-zA-Z_ ]*"); 
		if (null != name) {
			matcher = pattern.matcher(name);
		}
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}

	}

}
