package emgadm.blockedList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class EditBlockedBaseForm extends EMoneyGramAdmBaseValidatorForm {
	protected String action;
	protected String saveAction;
	protected String blkdReasCode;
	protected String blkdReasDesc;
	protected String blkdStatCode;
	protected String blkdStatDesc;
	protected String blkdCmntText;
	protected String riskLvlCode;
	protected String statUpdateDate;
	protected String createDate;
	protected String createUser;
	protected String lastUpdateDate;
	protected String lastUpdateUser;
	protected String submitProcess;
	protected String submitSave;
	protected String submitNew;
	protected String submitCancel;

	public String getAction() {
		return action == null ? "add" : action.trim();
	}

	public void setAction(String string) {
		action = string;
	}

	public String getSaveAction() {
		return saveAction == null ? "" : saveAction.trim();
	}

	public void setSaveAction(String string) {
		saveAction = string;
	}

	public String getBlkdReasCode() {
		return blkdReasCode == null ? "" :blkdReasCode.trim();
	}

	public void setBlkdReasCode(String string) {
		blkdReasCode = string;
	}

	public String getBlkdReasDesc() {
		return blkdReasDesc == null ? "" : blkdReasDesc.trim();
	}

	public void setBlkdReasDesc(String string) {
		blkdReasDesc = string;
	}

	public String getBlkdStatCode() {
		return blkdStatCode == null ? "" : blkdStatCode.trim();
	}

	public void setBlkdStatCode(String string) {
		blkdStatCode = string;
	}

	public String getBlkdStatDesc() {
		return blkdStatDesc == null ? "" : blkdStatDesc.trim();
	}

	public void setBlkdStatDesc(String string) {
		blkdStatDesc = string;
	}

	public String getBlkdCmntText() {
		return blkdCmntText == null ? "" : blkdCmntText;
	}

	public void setBlkdCmntText(String string) {
		blkdCmntText = string;
	}

	public String getRiskLvlCode() {
		return riskLvlCode == null ? "" : riskLvlCode.trim();
	}

	public void setRiskLvlCode(String string) {
		riskLvlCode = string;
	}

	public String getStatUpdateDate() {
		return statUpdateDate == null ? "" : statUpdateDate;
	}

	public void setStatUpdateDate(String string) {
		statUpdateDate = string;
	}

	public String getCreateDate() {
		return createDate == null ? "" : createDate;
	}

	public void setCreateDate(String string) {
		createDate = string;
	}

	public String getCreateUser() {
		return createUser == null ? "" : createUser;
	}

	public void setCreateUser(String string) {
		createUser = string;
	}

	public String getLastUpdateDate() {
		return lastUpdateDate == null ? "" : lastUpdateDate;
	}

	public void setLastUpdateDate(String string) {
		lastUpdateDate = string;
	}

	public String getLastUpdateUser() {
		return lastUpdateUser == null ? "" : lastUpdateUser;
	}

	public void setLastUpdateUser(String string) {
		lastUpdateUser = string;
	}

	public String getSubmitProcess() {
		return submitProcess;
	}

	public void setSubmitProcess(String string) {
		submitProcess = string;
	}

	public String getSubmitSave() {
		return submitSave;
	}

	public void setSubmitSave(String string) {
		submitSave = string;
	}

	public String getSubmitNew() {
		return submitNew;
	}

	public void setSubmitNew(String string) {
		submitNew = string;
	}

	public String getSubmitCancel() {
		return submitCancel;
	}

	public void setSubmitCancel(String string) {
		submitCancel = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		action = "";
		saveAction = "";
		blkdReasCode = "";
		blkdReasDesc = "";
		blkdStatCode = "";
		blkdStatDesc = "";
		blkdCmntText = "";
		riskLvlCode = "";
		statUpdateDate = "";
		createDate = "";
		createUser = "";
		lastUpdateDate = "";
		lastUpdateUser = "";
		submitSave = null;
		submitNew = null;
		submitCancel = null;
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);

		if (!StringHelper.isNullOrEmpty(submitSave) ||
			!StringHelper.isNullOrEmpty(submitProcess)) {
			if (StringHelper.isNullOrEmpty(blkdReasCode)) {
				errors.add("blkdReasCode", 
					new ActionError("errors.required", "Blocked Reason Code"));
			}
			if (StringHelper.isNullOrEmpty(blkdStatCode)) {
				errors.add("blkdStatCode", 
					new ActionError("errors.required", "Blocked Status Code"));
			}
			if (StringHelper.isNullOrEmpty(blkdCmntText)) {
				errors.add("blkdCmntText", 
					new ActionError("errors.required", "Blocked Comment"));
			} else if (blkdCmntText.trim().length() > 128) {
				String[] parms = {"Blocked Comment", "128"};
				errors.add("blkCmntText", 
					new ActionError("errors.length.too.long", parms));					
			}
			
			if (!StringHelper.isNullOrEmpty(riskLvlCode)) {
 
				if (riskLvlCode.trim().length() > 3) {
					String[] parms = {"Fraud Risk Level", "3"};
					errors.add("riskLvlCode",
						new ActionError("errors.length.too.long", parms));					
				} else if (StringHelper.containsNonDigits(riskLvlCode.trim())) {
					errors.add("riskLvlCode",
						new ActionError("errors.numeric"));									
				} else if (Integer.parseInt(riskLvlCode.trim()) < 0) {
					errors.add("riskLvlCode",
						new ActionError("error.must.be.a.positive.number"));													
				}
			}
		}
		
		if (!StringHelper.isNullOrEmpty(submitNew)) {
			reset(mapping, request);
			action = "add";
		}

		return errors;
	}
}
