package emgadm.blockedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.owasp.esapi.ESAPI;


import com.moneygram.www.PCIDecryptService_v2.AccountNotFoundException;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.BlockedAccountBean;
import emgadm.model.UserProfile;
import emgadm.services.DecryptionService;
import emgadm.util.StringHelper;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.BlockedBean;
import emgshared.model.ConsumerProfileComment;
import emgshared.model.ConsumerProfileSearchView;
import emgshared.model.CustomerProfileAccount;
import emgshared.services.ConsumerProfileService;
import emgshared.services.FraudService;
import emgshared.services.ObfuscationService;
import emgshared.services.PCIService;
import emgshared.services.ServiceFactory;

public class SaveBlockedListAction extends EMoneyGramAdmBaseAction {

	private ConsumerProfileService profileService = ServiceFactory
			.getInstance().getConsumerProfileService();
	private static final PCIService pciService = emgshared.services.ServiceFactory
			.getInstance().getPCIService();
	ObfuscationService os = ServiceFactory.getInstance()
			.getObfuscationService();
	DecryptionService decryptionService = emgadm.services.ServiceFactory
			.getInstance().getDecryptionService();

	private UserProfile userProfile;

	private static Logger LOGGER = EMGSharedLogger.getLogger(
			SaveBlockedListAction.class);

	/**
	 * addCustomerResults. Will add the customer details to the
	 * BlockedListCustomer class
	 * 
	 * @param custID
	 * @param custLogonID
	 * @param custSubStatDesc
	 * @param primaryPhoneNumber
	 * @param altPhoneNumber
	 * @param ccBinNumber
	 * @param acctmaskNumber
	 * @param firstName
	 * @param lastName
	 * @return blockedListCustomer
	 */
	private BlockedListCustomer addCustomerResults(int custID,
			String custLogonID, String custSubStatDesc,
			String primaryPhoneNumber, String altPhoneNumber,
			String ccBinNumber, String acctmaskNumber, String firstName,
			String lastName) {

		LOGGER.debug("Enter addCustomerResults :: Start");

		BlockedListCustomer blockedListCustomer = new BlockedListCustomer();
		blockedListCustomer.setCustId(custID);
		blockedListCustomer.setLoginId(custLogonID);
		blockedListCustomer.setProfileSubstatus(custSubStatDesc);
		if (primaryPhoneNumber != null) {
			blockedListCustomer.setPrimaryPhoneNumber(primaryPhoneNumber);
		} else if (altPhoneNumber != null) {
			blockedListCustomer.setAltPhoneNumber(altPhoneNumber);
		} else if (ccBinNumber != null) {
			blockedListCustomer.setCreditCard(ccBinNumber);
		} else if (acctmaskNumber != null) {
			blockedListCustomer.setAccountNumber(acctmaskNumber);
		}
		blockedListCustomer.setFirstName(firstName);
		blockedListCustomer.setLastName(lastName);

		LOGGER.debug("Exits addCustomerResult :: End");

		return blockedListCustomer;
	}

	/**
	 * getCustomerResultsList .Will return the phone number ,account number
	 * ,credit card number selected by the user to BlockedListCustomer class.
	 * 
	 * @param request
	 * @return custResults
	 */
	private List<BlockedListCustomer> getCustomerResultsList(
			HttpServletRequest request, List<BlockedAccountBean> blockCCList,
			List<BlockedAccountBean> blockBankList) {
		
		LOGGER.debug("Enter getCustomerResults. :: Start");

		userProfile = getUserProfile(request);
		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		FraudService fraudService = serviceFactory.getFraudService();
		List<BlockedListCustomer> custResults = new ArrayList<BlockedListCustomer>();
		try {

			String primaryPhoneNumber = request
					.getParameter("blockPrimaryPhoneNumber");
			String altPhoneNumber = request.getParameter("blockAltPhoneNumber");
			String primaryPhoneBlocker=null;
			LOGGER.debug("Super tainting for Primary Phone number. :: Start");
			if (request.getParameter("primaryPhoneBlocker") != null
					&& !StringHelper.isNullOrEmpty(primaryPhoneNumber)) {
				primaryPhoneBlocker = (String) request.getParameter("primaryPhoneBlocker");
				if(isAlphaBlockerValid(primaryPhoneBlocker)){
				request.getSession().setAttribute("showPryphone",
						primaryPhoneBlocker);
				}

				try {
				Collection consumers = null;
					consumers = fraudService.getConsumersByPhone(
							userProfile.getUID(), primaryPhoneNumber);
					Iterator iterator = consumers.iterator();
					while (iterator.hasNext()) {
						ConsumerProfileSearchView consumerProfileSearchView = (ConsumerProfileSearchView) iterator
								.next();
						custResults.add(addCustomerResults(Integer
								.parseInt(consumerProfileSearchView.getCustId()),
								consumerProfileSearchView.getCustLogonId(),
								consumerProfileSearchView.getCustSubStatDesc(),
								primaryPhoneNumber, null, null, null,
								consumerProfileSearchView.getCustFrstName(),
								consumerProfileSearchView.getCustLastName()));
					}
				} catch (DataSourceException e) {

					LOGGER.error(e.getMessage(),e);
				} catch (TooManyResultException e) {

					LOGGER.error(e.getMessage(),e);
				}
				request.setAttribute("primaryPhoneBlocker", true);

				
			}
			LOGGER.debug("Super tainting for Primary Phone number. :: End");
			LOGGER.debug("Super tainting for Alternate Phone number. :: Start");
			if (request.getParameter("altPhoneBlocker") != null
					&& !StringHelper.isNullOrEmpty(altPhoneNumber)) {
				String altPhoneBlocker = (String) request.getParameter("altPhoneBlocker");
				if(isAlphaBlockerValid(altPhoneBlocker)){
				request.getSession().setAttribute("showAltphone",
						altPhoneBlocker);
				}

				Collection<?> consumers = new ArrayList();
				try {
					consumers = fraudService.getConsumersByPhone(
							userProfile.getUID(), altPhoneNumber);
				} catch (DataSourceException dataSourceException) {
					LOGGER.error(dataSourceException.getMessage(),dataSourceException);
					dataSourceException.printStackTrace();
				} catch (TooManyResultException tooManyResultException) {
					LOGGER.error(tooManyResultException.getMessage(),tooManyResultException);
				}
				request.setAttribute("altPhoneBlocker", true);

				Iterator iterator1 = consumers.iterator();
				while (iterator1.hasNext()) {
					ConsumerProfileSearchView consumer = (ConsumerProfileSearchView) iterator1
							.next();
					if (custResults.size() == 0) {
						custResults.add(addCustomerResults(
								Integer.parseInt(consumer.getCustId()),
								consumer.getCustLogonId(),
								consumer.getCustSubStatDesc(), null,
								altPhoneNumber, null, null,
								consumer.getCustFrstName(),
								consumer.getCustLastName()));
					} else {
						boolean isExist = false;
						for (BlockedListCustomer custObj : custResults) {
							if (custObj.getCustId() == Integer
									.parseInt(consumer.getCustId())
									&& custObj.getLoginId().equals(
											consumer.getCustLogonId())) {
								custObj.setAltPhoneNumber(altPhoneNumber);
								isExist = true;
								break;
							}
						}
						if (isExist) {
							custResults.add(addCustomerResults(
									Integer.parseInt(consumer.getCustId()),
									consumer.getCustLogonId(),
									consumer.getCustSubStatDesc(), null,
									altPhoneNumber, null, null,
									consumer.getCustFrstName(),
									consumer.getCustLastName()));
						}
					}
				}
			}
			LOGGER.debug("Super tainting for Alternate Phone number. :: End");

			LOGGER.debug("Super tainting for Credit card. :: Start");
			String blkCC = (String) request.getParameter("blockCC");
			if(isAlphaBlockerValid(blkCC)){
			request.getSession().setAttribute("showCredit", blkCC);
			}
			if (null != blkCC) {
				List blockedAccIds = new ArrayList<ArrayList>();
				for (BlockedAccountBean blockedAccountBean : blockCCList) {
					if (blockedAccountBean.isBlock()) {
						Collection customerProfileAccounts = fraudService
								.getConsumersByAccounts(userProfile.getUID(),
										blockedAccountBean.getMaskedText()
												.substring(7), true, 0, blockedAccountBean.getCardToken());

						for (Iterator it = customerProfileAccounts.iterator(); it
								.hasNext();) {
							CustomerProfileAccount consumer = (CustomerProfileAccount) it
									.next();
								
								/*if (blockedAccountBean.getCardToken()
										.equals(blockedAccountBean
												.getClearText())) {*/

									if (custResults.size() == 0) {
										custResults
												.add(addCustomerResults(
														consumer.getCustId(),
														consumer.getCustLogonId(),
														consumer.getCustSubStatDesc(),
														null,
														null,
														consumer.getCrcdBinText()
																+ "*"
																+ consumer
																		.getAcctMaskNbr(),
														null,
														consumer.getCustFrstName(),
														consumer.getCustLastName()));
										blockedAccIds.add(consumer
												.getCustAcctId());
									} else {
										boolean isExist = false;
										// Below code to add Customer object to
										// the existing custResults if the
										// customer already present.
										for (BlockedListCustomer custObj : custResults) {
											if (custObj.getCustId() == consumer
													.getCustId()
													&& custObj
															.getLoginId()
															.equals(consumer
																	.getCustLogonId())) {
												isExist = true;
												String ccMaskText = null;
												ccMaskText = consumer
														.getCrcdBinText()
														+ "*"
														+ consumer
																.getAcctMaskNbr();
												if (ccMaskText.equals(custObj
														.getCreditCard())) {
													custObj.setCreditCard(ccMaskText);
												} else {
													if (custObj.getCreditCard() != null) {
														ccMaskText = custObj
																.getCreditCard()
																+ ", "
																+ ccMaskText;
														custObj.setCreditCard(ccMaskText);
													} else {
														custObj.setCreditCard(ccMaskText);
													}
												}
												blockedAccIds.add(consumer
														.getCustAcctId());
												break;
											}
										}
										// Below code to add New Customer object
										// to the custResults if the customer
										// not already present.
										if (!isExist) {
											custResults
													.add(addCustomerResults(
															consumer.getCustId(),
															consumer.getCustLogonId(),
															consumer.getCustSubStatDesc(),
															null,
															null,
															consumer.getCrcdBinText()
																	+ "*"
																	+ consumer
																			.getAcctMaskNbr(),
															null,
															consumer.getCustFrstName(),
															consumer.getCustLastName()));
											blockedAccIds.add(consumer
													.getCustAcctId());
										}

									}
								//}
							} 
						}					
					blockedAccountBean.setBlockedAcctIds(blockedAccIds);
				}
			}
			LOGGER.debug("Super tainting for Credit card. :: End");
			LOGGER.debug("Super tainting for Bank account Number. :: Start");
			String decrtptAcctNbr = null;
			String blkAcct = (String) request.getParameter("blockAcct");
			if(isAlphaBlockerValid(blkAcct)){
			request.getSession().setAttribute("showBankac", blkAcct);
			}
			if (null != blkAcct) {
				List blockedAccIds = new ArrayList<ArrayList>();
				for (BlockedAccountBean blockedAccountBean : blockBankList) {
					if (blockedAccountBean.isBlock()) {

						Collection accounts;
						try {
							accounts = fraudService.getConsumersByAccounts(
									userProfile.getGuid(),
									blockedAccountBean.getMaskedText(), false,
									0, blockedAccountBean.getCardToken());
							decrtptAcctNbr = decryptionService
									.decryptBankAccountNumber(blockedAccountBean
											.getEncryptedText().trim());
							Iterator iterator = accounts.iterator();
							while (iterator.hasNext()) {
								CustomerProfileAccount customerProfileAccount = (CustomerProfileAccount) iterator
										.next();
								if (decryptionService.decryptBankAccountNumber(
										customerProfileAccount
												.getAcctEncrypNbr().trim())
										.equals(decrtptAcctNbr)) {
									if (custResults.size() == 0) {
										custResults.add(addCustomerResults(
												customerProfileAccount
														.getCustId(),
												customerProfileAccount
														.getCustLogonId(),
												customerProfileAccount
														.getCustSubStatDesc(),
												null, null, null,
												customerProfileAccount
														.getAcctMaskNbr(),
												customerProfileAccount
														.getCustFrstName(),
												customerProfileAccount
														.getCustLastName()));
										blockedAccIds
												.add(customerProfileAccount
														.getCustAcctId());

									} else {
										boolean isExist = false;
										Iterator iterator2 = custResults
												.iterator();
										for (BlockedListCustomer custObj : custResults) {
											if (custObj.getCustId() == customerProfileAccount
													.getCustId()) {
												isExist = true;
												String acctMaskText = null;
												acctMaskText = customerProfileAccount
														.getAcctMaskNbr();
												if (acctMaskText.equals(custObj
														.getAccountNumber())) {
													custObj.setAccountNumber(acctMaskText);
												} else {
													if (custObj
															.getAccountNumber() != null) {
														acctMaskText = custObj
																.getAccountNumber()
																+ ", *"
																+ acctMaskText;
														custObj.setAccountNumber(acctMaskText);
													} else {
														custObj.setAccountNumber(acctMaskText);
													}
												}

												blockedAccIds
														.add(customerProfileAccount
																.getCustAcctId());
												break;
											}
										}
										if (!isExist) {
											custResults
													.add(addCustomerResults(
															customerProfileAccount
																	.getCustId(),
															customerProfileAccount
																	.getCustLogonId(),
															customerProfileAccount
																	.getCustSubStatDesc(),
															null,
															null,
															null,
															customerProfileAccount
																	.getAcctMaskNbr(),
															customerProfileAccount
																	.getCustFrstName(),
															customerProfileAccount
																	.getCustLastName()));
											blockedAccIds
													.add(customerProfileAccount
															.getCustAcctId());
										}
									}
								}
							}

							blockedAccountBean.setBlockedAcctIds(blockedAccIds);
						} catch (DataSourceException dataSourceException) {
							dataSourceException.printStackTrace();
						} catch (TooManyResultException tooManyResultException) {

							tooManyResultException.printStackTrace();
						}

					}
				}
			}
			LOGGER.debug("Super tainting for Bank account Number. :: Ends");
		} catch (Exception exception) {

			LOGGER.error("exceptionn occued in getting list of cust results ",
					exception);
		}

		LOGGER.debug("Exits getCustomerResults :: End");

		return custResults;
	}

	/**
	 * @param blockedCustomers
	 *            will save the comments into database by profileService
	 * @param request
	 * @throws Exception
	 */
	private List<BlockedListCustomer> addCustomerBlockComment(
			List<BlockedListCustomer> blockedCustomers, String blockingReason,
			String blockingAction, String blockingComment,
			Map<String, String> reasonDescriptions, HttpServletRequest request)
			throws Exception {

		LOGGER.debug("Enter addCustomerBlockComment :: Start");

		UserProfile up = getUserProfile(request);

		for (BlockedListCustomer blkCustomer : blockedCustomers) {
			ConsumerProfileComment comment = new ConsumerProfileComment();

			// To Display the Blocked/Unblocked Text in Save Blocked list page
			// :: Start
			StringBuilder blockCommentData = new StringBuilder();

			if (blkCustomer.getCreditCard() != null) {
				blockCommentData.append(blkCustomer.getCreditCard());
			}
			if (blkCustomer.getPrimaryPhoneNumber() != null) {
				blockCommentData.append(", "
						+ blkCustomer.getPrimaryPhoneNumber());
			}
			if (blkCustomer.getAltPhoneNumber() != null) {
				blockCommentData.append(", " + blkCustomer.getAltPhoneNumber());
			}
			if (blkCustomer.getAccountNumber() != null) {
				blockCommentData.append(", " + "*"
						+ blkCustomer.getAccountNumber());
			}

			if (blkCustomer.getCreditCard() != null) {
				blkCustomer.setDisplayData(blockCommentData.toString());
			} else {
				blkCustomer.setDisplayData(blockCommentData.toString()
						.substring(1));// Substring is used to remove comma from
										// data
			}
			// To Display the Blocked/Unblocked Text in Save Blocked list page
			// :: End

			// To set the Comment data to the Blocked Customer Object :: Start
			if (StringUtils.equals(blockingAction, "NBK")) {
				blkCustomer.setBlockData("Not Blocked");
			} else {
				blkCustomer.setBlockData("Blocked");
			}
			blkCustomer.setBlockReason(reasonDescriptions.get(blockingReason));
			blkCustomer.setBlockComment(blockingComment);
			// To set the Comment data to the Customer Object :: End

			StringBuilder blockComment = new StringBuilder();
			blockComment.append(blkCustomer.getBlockData() + " | ");
			blockComment.append(blockCommentData + " | "
					+ blkCustomer.getBlockReason() + " | ");
			blockComment.append(blkCustomer.getBlockComment());
			blkCustomer.setBlockComment(blockComment.toString().replace("|  ,",
					"|"));
			comment.setText(blkCustomer.getBlockComment());
			comment.setCustId(blkCustomer.getCustId());
			comment.setReasonCode("OTH");
			profileService.addConsumerProfileComment(comment, up.getUID());
		}

		LOGGER.debug("Exits addCustomerBlockComment :: End");
		return blockedCustomers;
	}

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		LOGGER.debug("Enter Execute SaveBlockedList Action :: Start");

		ActionErrors errors = new ActionErrors();

		userProfile = getUserProfile(request);
		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		List<BlockedAccountBean> blockList = new ArrayList();
		List<BlockedAccountBean> selCCList = new ArrayList();
		List<BlockedAccountBean> selBankList = new ArrayList();

		String selectedCCStr = (String) request.getParameter("blockCCVal");
		String selectedBnkAccStr = request.getParameter("blockBnkAccVal");

		String primaryPhoneNumber = (String) request.getSession().getAttribute(
				"blockPrimaryPhoneNumber");
		String altPhoneNumber = (String) request.getSession().getAttribute(
				"blockAltPhoneNumber");

		if (StringUtils.isNotEmpty(selectedCCStr)) {
			LOGGER.debug("Selected Credit Cards to Block ::" + ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTML(selectedCCStr)));
			blockList = (List<BlockedAccountBean>) request.getSession()
					.getAttribute("blockCCList");
			StringTokenizer st = new StringTokenizer(selectedCCStr, "|");
			while (st.hasMoreTokens()) {
				String maskedText = (String) st.nextElement();
				for (BlockedAccountBean blkBean : blockList) {
					if (blkBean.getMaskedText().equals(maskedText)) {
						selCCList.add(blkBean);
					}
				}
			}
		}

		if (StringUtils.isNotEmpty(selectedBnkAccStr)) {
			blockList = (List<BlockedAccountBean>) request.getSession()
					.getAttribute("blockBankList");
			StringTokenizer st = new StringTokenizer(selectedBnkAccStr, "|");
			while (st.hasMoreTokens()) {
				String encyptText = (String) st.nextElement();
				for (BlockedAccountBean blkBean : blockList) {
					if (blkBean.getEncryptedText().equals(encyptText)) {
						selBankList.add(blkBean);
						blockList.remove(blkBean);
						break;
					}
				}
			}
		}

		Map<String, String> reasonDescriptions = new HashMap<String, String>();
		reasonDescriptions.put("R0010", "CB / Return");
		reasonDescriptions.put("R0011", "Compromised Data");
		reasonDescriptions.put("R0009", "Fraud");
		reasonDescriptions.put("R0008", "ID Theft");
		reasonDescriptions.put("R0103", "Other");

		Map<String, String> blockStatusDescriptions = new HashMap<String, String>();
		blockStatusDescriptions.put("NBK", "Not Blocked");
		blockStatusDescriptions.put("BLK", "Blocked");

		String blockingReason = (String) request
				.getParameter("blockingReasonCC");
		String blockingAction = (String) request
				.getParameter("blockingActionCC");
		String blockingComment = (String) request
				.getParameter("blockingComment");

		List<BlockedListCustomer> custResults = getCustomerResultsList(request,
				selCCList, selBankList);
		custResults = addCustomerBlockComment(custResults, blockingReason,
				blockingAction, blockingComment, reasonDescriptions, request);
		request.setAttribute("blockedCustomerList", custResults);

		if (!StringHelper.isNullOrEmpty(request
				.getParameter("primaryPhoneBlocker"))) {
			EditPhoneBlockedListForm saveForm = new EditPhoneBlockedListForm();
			saveForm.setPhoneNumber(primaryPhoneNumber);
			saveForm.setBlkdReasCode(blockingReason);
			saveForm.setBlkdStatCode(blockingAction);
			saveForm.setBlkdCmntText(blockingComment);
			saveForm.setRiskLvlCode(null);
			EditPhoneBlockedListAction saveAction = new EditPhoneBlockedListAction();
			saveAction.saveDB(saveForm, request);
			String[] parms = { "Blocked Primary Phone Number",
					primaryPhoneNumber };
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("msg.added",
					parms));
			saveErrors(request, errors);
		}

		if (!StringHelper
				.isNullOrEmpty(request.getParameter("altPhoneBlocker"))) {
			EditPhoneBlockedListForm saveForm = new EditPhoneBlockedListForm();
			saveForm.setPhoneNumber(altPhoneNumber);
			saveForm.setBlkdReasCode(blockingReason);
			saveForm.setBlkdStatCode(blockingAction);
			saveForm.setBlkdCmntText(blockingComment);
			saveForm.setRiskLvlCode(null);
			EditPhoneBlockedListAction saveAction = new EditPhoneBlockedListAction();
			saveAction.saveDB(saveForm, request);
			String[] parms = { "Blocked Alternate Phone Number", altPhoneNumber };
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("msg.added",
					parms));
			saveErrors(request, errors);
		}

		FraudService fraudService = serviceFactory.getFraudService();
		String blkCC = (String) request.getSession().getAttribute("showCredit");
		if (null != blkCC) {
			for (BlockedAccountBean blockedAccountBean : selCCList) {

				if (blockedAccountBean.isBlock()) {
					BlockedBean blockedBean = new BlockedBean();
					if (blockedAccountBean.getBlockedCCId() == null
							|| blockedAccountBean.getBlockedCCId().equals("")) {
						blockedBean.setBlockedId("");
					} else {
						blockedBean
								.setBlockedId(new StringBuilder().append(
										blockedAccountBean.getBlockedCCId())
										.toString());
					}
					blockedBean.setCardToken(blockedAccountBean.getCardToken());
					blockedBean.setMaskText(blockedAccountBean.getMaskedText()
							.substring(7));
					blockedBean.setReasonCode(blockingReason);
					blockedBean.setComment(blockingComment);
					blockedBean.setStatusCode(blockingAction);
					blockedBean.setClearText(blockedAccountBean.getClearText());
					blockedBean.setFraudRiskLevelCode("0");
					blockedBean.setBinText(blockedAccountBean.getMaskedText()
							.substring(0, 6));

					Collection customerProfileAccountList = new ArrayList<CustomerProfileAccount>();
					List blockedIds = new ArrayList<BlockedAccountBean>();
					blockedIds = blockedAccountBean.getBlockedAcctIds();
					CustomerProfileAccount cpa = new CustomerProfileAccount();
					if (null != blockedIds && !blockedIds.isEmpty()) {
						for (int i = 0; i < blockedIds.size(); i++) {
							int blockedId = (Integer) blockedIds.get(i);

							if (customerProfileAccountList.size() == 0) {
								cpa.setCustAcctId(blockedId);
								customerProfileAccountList.add(cpa);
							} else {
								CustomerProfileAccount cpa1 = new CustomerProfileAccount();
								cpa1.setCustAcctId(blockedId);
								customerProfileAccountList.add(cpa1);
							}
						}
					}

					fraudService.setBlockedCC(userProfile.getUID(),
							blockedBean, customerProfileAccountList);
				}

			}
		}

		String blkAcct = (String) request.getSession().getAttribute(
				"showBankac");
		if (null != blkAcct) {
			for (BlockedAccountBean blockedAccountBean : selBankList) {
				if (blockedAccountBean.isBlock()) {
					BlockedBean blockedBean = new BlockedBean();
					blockedBean.setAbaNumber(blockedAccountBean.getAba());
					String decryptedAccountNumber = decryptionService
							.decryptBankAccountNumber(blockedAccountBean
									.getEncryptedText());
					blockedBean.setHashText(os
							.getBankAccountHash(decryptedAccountNumber));
					blockedBean.setMaskText(blockedAccountBean.getMaskedText());
					blockedBean.setEncryptedNumber(blockedAccountBean
							.getEncryptedText());
					blockedBean.setReasonCode(blockingReason);
					blockedBean.setComment(blockingComment);
					blockedBean.setStatusCode(blockingAction);
					blockedBean.setFraudRiskLevelCode("0");

					Collection customerProfileAccountList = new ArrayList<CustomerProfileAccount>();
					List blockedIds = new ArrayList<BlockedAccountBean>();
					blockedIds = blockedAccountBean.getBlockedAcctIds();
					CustomerProfileAccount cpa = new CustomerProfileAccount();
					if (null != blockedIds && !blockedIds.isEmpty()) {
						for (int i = 0; i < blockedIds.size(); i++) {
							int blockedId = (Integer) blockedIds.get(i);

							if (customerProfileAccountList.size() == 0) {
								cpa.setCustAcctId(blockedId);
								customerProfileAccountList.add(cpa);
							} else {
								CustomerProfileAccount cpa1 = new CustomerProfileAccount();
								cpa1.setCustAcctId(blockedId);
								customerProfileAccountList.add(cpa1);
							}
						}
					}

					fraudService.setBlockedBankAccounts(userProfile.getUID(),
							blockedBean, customerProfileAccountList);
				}
			}
		}

		request.setAttribute("blockingActionCC", blockingAction);
		request.setAttribute("blockingReasonCC", blockingReason);
		request.setAttribute("blockingComment", blockingComment);

		request.setAttribute("blockedReasonDescription", reasonDescriptions
				.get(request.getAttribute("blockingReasonCC")));
		request.setAttribute("blockedStatusDescription",
				blockStatusDescriptions.get(request
						.getAttribute("blockingActionCC")));

		LOGGER.debug("Enter Execute SaveBlockedList Action :: End");

		return mapping
				.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
				
	}
	//MBO-9993
	private static boolean isAlphaBlockerValid(String name){
		if(null != name && name.matches("[a-z]+")) {
			return true;
		}
		else {
			return false;
		}
	}

}
