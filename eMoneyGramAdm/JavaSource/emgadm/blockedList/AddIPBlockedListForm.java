package emgadm.blockedList;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;
import emgshared.util.IPAddressValidator;
import emgshared.model.BlockedStatus;

public class AddIPBlockedListForm extends EMoneyGramAdmBaseValidatorForm
{
	private String singleIPSelect = null;
	private String ipAddress1 = null;
	private String ipAddress2 = null;	
	private String riskLevel = null;	
	private String blockingReason = null;
	private String blockingComment = null;
	private String blockingAction = null;		
	private String submitSave = null;
	private String submitCancel = null;
	

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);

		ipAddress2 = null;
		submitSave = null;
		submitCancel = null;
	}
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
	{
		ActionErrors errors = super.validate(mapping, request);
		
		if (submitSave != null){

			if (StringHelper.isNullOrEmpty(ipAddress1)){
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "IP Address"));
			}else{		
				IPAddressValidator ipValidator = new IPAddressValidator(ipAddress1);
				if (ipValidator.isIpInvalid()){
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.invalid.ip", ipValidator.getInvalidReason()));				
				}
			}
			
			if (singleIPSelect.equals("1")){ 
				if (StringHelper.isNullOrEmpty(ipAddress1) || StringHelper.isNullOrEmpty(ipAddress2))
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "IP Address range"));
				if (!StringHelper.isNullOrEmpty(ipAddress2)){
					IPAddressValidator ipValidator = new IPAddressValidator(ipAddress2);
					if (ipValidator.isIpInvalid()){
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.invalid.ip", ipValidator.getInvalidReason()));				
					}
				}
					
			}

			if (blockingReason.equals("None")){ 
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "Reason for blocking"));
			}

			if (blockingAction.equals("None")){ 
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "Action for blocking"));
			}

			if (blockingAction.equals(BlockedStatus.HIGH_RISK_CODE) && StringHelper.isNullOrEmpty(riskLevel)){ 
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "Risk Level(0-999)"));
			}
			
							
			if (StringHelper.isNullOrEmpty(blockingComment))
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "Comment for blocking IP address"));

			if (!StringHelper.isNullOrEmpty(ipAddress1)){
				if (ipAddress1.indexOf("*") >= 0)	
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "IP Address can not have wild characters. Full IP address"));
			}

			if (singleIPSelect.equals("1") && 
				!StringHelper.isNullOrEmpty(ipAddress2)){
				if (ipAddress2.indexOf("*") >= 0)	
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "IP Address can not have wild characters. Full IP address"));
			}
			
			if (errors.size() == 0 &&
				singleIPSelect.equals("1") && 
				!IPAddressValidator.checkValidIpRange(ipAddress1, ipAddress2)) {
				String[] parms = {ipAddress1, ipAddress2};
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.ip.range.error", parms));
			}
			
/*
			if (StringHelper.isNullOrEmpty(riskLevel))
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "Risk Level"));
			else{
				try{
					if (Integer.parseInt(riskLevel) < 0){ 
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.invalid.risklevel", "Risk Level must be between 0 to 999."));
					}
				}catch(NumberFormatException nfe){
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.invalid.risklevel", "Risk Level must be between 0 to 999."));
				}
			}
*/							
		} 			

		return errors;
	}

	
	/**
	 * @param string
	 */
	public void setSubmitCancel(String string) {
		submitCancel = string;
	}

	/**
	 * @param string
	 */
	public void setSubmitSave(String string) {
		submitSave = string;
	}

	/**
	 * @return
	 */
	public String getSingleIPSelect() {
		return singleIPSelect == null ? "0" : singleIPSelect;
	}

	/**
	 * @param string
	 */
	public void setSingleIPSelect(String string) {
		singleIPSelect = string;
	}

	/**
	 * @return
	 */
	public String getBlockingComment() {
		return blockingComment;
	}

	/**
	 * @return
	 */
	public String getBlockingReason() {
		return blockingReason;
	}

	/**
	 * @return
	 */
	public String getIpAddress1() {
		return ipAddress1;
	}

	/**
	 * @return
	 */
	public String getIpAddress2() {
		return ipAddress2;
	}

	/**
	 * @return
	 */
	public String getSubmitCancel() {
		return submitCancel;
	}

	/**
	 * @return
	 */
	public String getSubmitSave() {
		return submitSave;
	}

	/**
	 * @param string
	 */
	public void setBlockingComment(String string) {
		blockingComment = string;
	}

	/**
	 * @param string
	 */
	public void setBlockingReason(String string) {
		blockingReason = string;
	}

	/**
	 * @param string
	 */
	public void setIpAddress1(String string) {
		ipAddress1 = string;
	}

	/**
	 * @param string
	 */
	public void setIpAddress2(String string) {
		ipAddress2 = string;
	}


	/**
	 * @return
	 */
	public String getRiskLevel() {
		return riskLevel;
	}

	/**
	 * @param string
	 */
	public void setRiskLevel(String string) {
		riskLevel = string;
	}

	/**
	 * @return
	 */
	public String getBlockingAction() {
		return blockingAction;
	}

	/**
	 * @param string
	 */
	public void setBlockingAction(String string) {
		blockingAction = string;
	}

}
