package emgadm.blockedList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class ShowPhoneBlockedListForm extends EMoneyGramAdmBaseValidatorForm {
	private String phNbr = null;
	private String action = null;
	private String submitSearch = null;
	private String submitShowAll = null;
	private String submitNew = null;

	public String getPhNbr() {
		return phNbr;
	}

	public void setPhNbr(String string) {
		phNbr = string;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String string) {
		action = string;
	}

	public String getSubmitSearch() {
		return submitSearch;
	}

	public void setSubmitSearch(String string) {
		submitSearch = string;
	}

	public String getSubmitShowAll() {
		return submitShowAll;
	}

	public void setSubmitShowAll(String string) {
		submitShowAll = string;
	}

	public String getSubmitNew() {
		return submitNew;
	}

	public void setSubmitNew(String string) {
		submitNew = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);

		phNbr = "";
		action = null;
		submitSearch = null;
		submitShowAll = null;
		submitNew = null;
	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);

		if (submitSearch != null) {
			if (StringHelper.isNullOrEmpty(phNbr)) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"errors.required", "Phone Number Search Key"));
			} else if (StringHelper.containsNonDigits(phNbr)) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"errors.integer", "Phone Number Search Key"));
			}
		}

		return errors;
	}
}
