package emgadm.blockedList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class SaveEmailBlockedListForm extends EMoneyGramAdmBaseValidatorForm {
	private String submitDBSave = null;
	private String submitDBCancel = null;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);

		submitDBSave = null;
		submitDBCancel = null;
	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);

		if (submitDBSave != null) {

		}

		return errors;
	}

	/**
	 * @param string
	 */
	public void setSubmitDBCancel(String string) {
		submitDBCancel = string;
	}

	/**
	 * @param string
	 */
	public void setSubmitDBSave(String string) {
		submitDBSave = string;
	}

	/**
	 * @return
	 */
	public String getSubmitDBCancel() {
		return submitDBCancel;
	}

	/**
	 * @return
	 */
	public String getSubmitDBSave() {
		return submitDBSave;
	}

	/**
	 * @param string
	 */

}
