package emgadm.blockedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.CommandAuth;
import emgadm.model.UserProfile;
import emgadm.services.DecryptionService;
import emgadm.util.StringHelper;
import emgshared.exceptions.MaxRowsHashCryptoException;
import emgshared.model.BlockedBean;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.FraudService;
import emgshared.services.ObfuscationService;
import emgshared.services.ServiceFactory;

/**
 * @version 1.0
 * @author
 */
public class ShowBinBlockedListAction extends EMoneyGramAdmBaseAction {
	private static EMTSharedDynProperties dynProps = new EMTSharedDynProperties();

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ActionErrors errors = new ActionErrors();
		ShowBinBlockedListForm showBinBlockedListForm = (ShowBinBlockedListForm) form;
		UserProfile up = getUserProfile(request);
		CommandAuth ca = new CommandAuth();
		

		if (up.hasPermission(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_ADD_BIN_BLOCKED_LIST)) {
			ca.setAddBinBlockedList(true);
		} else {
			ca.setAddBinBlockedList(false);
		}
		request.getSession().setAttribute("auth", ca);

		if (!StringHelper.isNullOrEmpty(showBinBlockedListForm.getSubmitAdd())) {
			return mapping
					.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_ADD_BIN_BLOCKED_LIST);
		}

		if (!StringHelper.isNullOrEmpty(showBinBlockedListForm
				.getSubmitSearch())
				|| !StringHelper.isNullOrEmpty(showBinBlockedListForm
						.getSubmitShowAll())) {

			try {
				ServiceFactory sf = ServiceFactory.getInstance();
				FraudService fs = sf.getFraudService();
				ObfuscationService os = sf.getObfuscationService();
				DecryptionService ds = emgadm.services.ServiceFactory
						.getInstance().getDecryptionService();

				String hashText = StringHelper
						.isNullOrEmpty(showBinBlockedListForm.getBinText()) == true ? null
						: os.getCreditCardHash(showBinBlockedListForm
								.getBinText());

				Collection bins = !StringHelper.isNullOrEmpty(showBinBlockedListForm
						.getSubmitSearch()) ? 
								fs.getBlockedBins(up.getUID(),hashText) :
									fs.getBlockedBins(up.getUID(), null);

				if (!StringHelper.isNullOrEmpty(showBinBlockedListForm
						.getSubmitSearch())) {
					bins = fs.getBlockedBins(up.getUID(), hashText);
				} else {
					bins = fs.getBlockedBins(up.getUID(), null);
				}

				if (bins == null || bins.size() == 0) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"error.no.blockedbin.found"));
					saveErrors(request, errors);
				} else {

					for (Iterator it = bins.iterator(); it.hasNext();) {
						BlockedBean br = (BlockedBean) it.next();
						br.setClearText(ds.decryptCreditCardBinNumber(br
								.getEncryptedNumber()));
					}

					BlockedBean.sortBy = BlockedBean.SORT_BY_CLEAR_TEXT;
					Collections.sort((List) bins);
					request.setAttribute("bins", bins);
				}

			} catch (MaxRowsHashCryptoException te) {
				errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError("error.too.many.results", dynProps
								.getMaxRowsHashCrypto() + ""));
				saveErrors(request, errors);
			}
		}

		return mapping
				.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
