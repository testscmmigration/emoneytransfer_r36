/*
 * Created on Mar 2, 2005
 *
 */
package emgshared.model;

/**
 * @author A131
 *
 */
public class ConsumerProfileComment extends Comment
{
	private int custId;

	public int getCustId()
	{
		return custId;
	}

	public void setCustId(int i)
	{
		custId = i;
	}
}
