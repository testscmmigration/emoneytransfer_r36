/*
 * Created on July 29, 2005
 *
 */
package emgshared.model;

import emgshared.util.I18NHelper;

/**
 * @author T349
 *
 */
public class ListenerProcessType
{

	private static final String AUTO_EXPRESS_PAYMENT_CODE = "AutoEP";
	private static final String CANCEL_SAME_DAY_SERVICE_CODE = "CanelMG";
	private static final String CANCEL_ECONOMY_SERVICE_CODE = "CancelES";
	private static final String CANCEL_EXPRESS_PAYMENT_CODE = "CancelEP";

	public static final ListenerProcessType AUTO_EXPRESS_PAYMENT =
		new ListenerProcessType(AUTO_EXPRESS_PAYMENT_CODE);
	public static final ListenerProcessType CANCEL_SAME_DAY_SERVICE =
		new ListenerProcessType(CANCEL_SAME_DAY_SERVICE_CODE);
	public static final ListenerProcessType CANCEL_ECONOMY_SERVICE =
		new ListenerProcessType(CANCEL_ECONOMY_SERVICE_CODE);
	public static final ListenerProcessType CANCEL_EXPRESS_PAYMENT =
		new ListenerProcessType(CANCEL_EXPRESS_PAYMENT_CODE);

	private String typeCode;

	public ListenerProcessType()
	{
		// Default empty constructor, must exist to be used
		// by the Proxy - XML Encoder/Decoder.
	}
	public ListenerProcessType(String type)
	{
		this.typeCode = type;
	}

	public static ListenerProcessType getInstance(String processCode)
	{
		ListenerProcessType listenerProcessType;
		if (AUTO_EXPRESS_PAYMENT_CODE.equalsIgnoreCase(processCode))
		{
			listenerProcessType = AUTO_EXPRESS_PAYMENT;
		} else if (CANCEL_SAME_DAY_SERVICE_CODE.equalsIgnoreCase(processCode))
		{
			listenerProcessType = CANCEL_SAME_DAY_SERVICE;
		} else if (CANCEL_ECONOMY_SERVICE_CODE.equalsIgnoreCase(processCode))
		{
			listenerProcessType = CANCEL_ECONOMY_SERVICE;
		} else if (CANCEL_EXPRESS_PAYMENT_CODE.equalsIgnoreCase(processCode))
			{
				listenerProcessType = CANCEL_EXPRESS_PAYMENT;
		} else
		{
			String errorMessage =
				I18NHelper.getFormattedMessage(
					"exception.invalid.param.value",
					new Object[] { processCode, "code" });
			throw new IllegalArgumentException(errorMessage);
		}
		return (listenerProcessType);
	}

	public String getTypeCode()
	{
		return typeCode;
	}

	public boolean isAutoExpressPaymentType()
	{
		return AUTO_EXPRESS_PAYMENT_CODE.equalsIgnoreCase(this.typeCode);
	}
	
	public boolean isCancelSameDayServiceType()
	{
		return CANCEL_SAME_DAY_SERVICE_CODE.equalsIgnoreCase(this.typeCode);
	}
	
	public boolean isCancelEconomyServiceType()
	{
		return CANCEL_ECONOMY_SERVICE_CODE.equalsIgnoreCase(this.typeCode);
	}
	
	public boolean isCancelExpressPaymentType()
	{
		return CANCEL_EXPRESS_PAYMENT_CODE.equalsIgnoreCase(this.typeCode);
	}
	
	public String toString()
	{
		return typeCode;
	}
	/**
	 * @param string
	 */
	public void setTypeCode(String string)
	{
		typeCode = string;
	}

}