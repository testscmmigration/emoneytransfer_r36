package emgshared.model;

public class BlockedReason {

	private String reasonCode;
	private String reasonDesc;
	private String reasonType;	
	private String createDate;
	private String createUserId;
	private String updateDate;
	private String updateUserId;


	/**
	 * @return
	 */
	public String getCreateDate() {
		return createDate;
	}

	/**
	 * @return
	 */
	public String getCreateUserId() {
		return createUserId;
	}

	/**
	 * @return
	 */
	public String getReasonCode() {
		return reasonCode;
	}

	/**
	 * @return
	 */
	public String getReasonDesc() {
		return reasonDesc;
	}

	/**
	 * @return
	 */
	public String getReasonType() {
		return reasonType;
	}

	/**
	 * @return
	 */
	public String getUpdateDate() {
		return updateDate;
	}

	/**
	 * @return
	 */
	public String getUpdateUserId() {
		return updateUserId;
	}

	/**
	 * @param string
	 */
	public void setCreateDate(String string) {
		createDate = string;
	}

	/**
	 * @param string
	 */
	public void setCreateUserId(String string) {
		createUserId = string;
	}

	/**
	 * @param string
	 */
	public void setReasonCode(String string) {
		reasonCode = string;
	}

	/**
	 * @param string
	 */
	public void setReasonDesc(String string) {
		reasonDesc = string;
	}

	/**
	 * @param string
	 */
	public void setReasonType(String string) {
		reasonType = string;
	}

	/**
	 * @param string
	 */
	public void setUpdateDate(String string) {
		updateDate = string;
	}

	/**
	 * @param string
	 */
	public void setUpdateUserId(String string) {
		updateUserId = string;
	}

}
