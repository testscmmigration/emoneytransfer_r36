package emgshared.model;
import java.util.Date;

public class ScoreValue
{
	private int scoreConfigId;
	private String scoreCatCode;
	private String value;
	private int points;
	private Date createDate;
	private String valCreateDate;
	private String createUserid;
	private Date lastUpdateDate;
	private String valLastUpdateDate;
	private String lastUpdateUserid;
	private int minimumValue;
	private int maximumValue;

	public ScoreValue(String value, int points)
	{
		this.value = value;
		this.points = points;
	}

	public ScoreValue(int minimumValue, int maximumValue, int points)
	{
		this.minimumValue = minimumValue;
		this.maximumValue = maximumValue;
		this.points = points;
	}

	public ScoreValue()
	{
	}

	/**
	 * @return
	 */
	public Date getCreateDate()
	{
		return createDate;
	}

	/**
	 * @return
	 */
	public String getCreateUserid()
	{
		return createUserid;
	}

	/**
	 * @return
	 */
	public Date getLastUpdateDate()
	{
		return lastUpdateDate;
	}

	/**
	 * @return
	 */
	public String getLastUpdateUserid()
	{
		return lastUpdateUserid;
	}

	/**
	 * @return
	 */
	public int getPoints()
	{
		return points;
	}

	/**
	 * @return
	 */
	public String getValue()
	{
		return value;
	}

	/**
	 * @param date
	 */
	public void setCreateDate(Date date)
	{
		createDate = date;
	}

	/**
	 * @param string
	 */
	public void setCreateUserid(String string)
	{
		createUserid = string;
	}

	/**
	 * @param date
	 */
	public void setLastUpdateDate(Date date)
	{
		lastUpdateDate = date;
	}

	/**
	 * @param string
	 */
	public void setLastUpdateUserid(String string)
	{
		lastUpdateUserid = string;
	}

	/**
	 * @param i
	 */
	public void setPoints(int i)
	{
		points = i;
	}

	/**
	 * @param string
	 */
	public void setValue(String string)
	{
		value = string;
	}

	/**
	 * @return
	 */
	public int getMaximumValue()
	{
		return maximumValue;
	}

	/**
	 * @return
	 */
	public int getMinimumValue()
	{
		return minimumValue;
	}

	/**
	 * @param i
	 */
	public void setMaximumValue(int i)
	{
		maximumValue = i;
	}

	/**
	 * @param i
	 */
	public void setMinimumValue(int i)
	{
		minimumValue = i;
	}

	/**
	 * @return
	 */
	public int getScoreConfigId()
	{
		return scoreConfigId;
	}

	/**
	 * @param i
	 */
	public void setScoreConfigId(int i)
	{
		scoreConfigId = i;
	}

	/**
	 * @return
	 */
	public String getScoreCatCode()
	{
		return scoreCatCode;
	}

	/**
	 * @param string
	 */
	public void setScoreCatCode(String string)
	{
		scoreCatCode = string;
	}

	/**
	 * @return
	 */
	public String getValCreateDate()
	{
		return valCreateDate;
	}

	/**
	 * @param string
	 */
	public void setValCreateDate(String string)
	{
		valCreateDate = string;
	}

	/**
	 * @return
	 */
	public String getValLastUpdateDate()
	{
		return valLastUpdateDate;
	}

	/**
	 * @param string
	 */
	public void setValLastUpdateDate(String string)
	{
		valLastUpdateDate = string;
	}

}
