package emgshared.model;

import java.util.Date;

import emgshared.util.DateFormatter;

public class MessageRecipientBean implements Comparable
{
	private DateFormatter df = new DateFormatter("dd/MMM/yyyy hh:mm a", true);
	
	private int adminMsgId;
	private String RecipientId;
	private Date msgRcvDate;
	private String firstName;
	private String lastName;
	private String status;
	private String roleName;
	private String email;
	private Date lastAccessTime;

	public MessageRecipientBean(int msgId, String recipntId, Date rcvDate)
	{
		this.adminMsgId = msgId;
		this.RecipientId = recipntId;
		this.msgRcvDate = rcvDate;
	}
	
	public int getAdminMsgId()
	{
		return adminMsgId;
	}

	public void setAdminMsgId(int i)
	{
		adminMsgId = i;
	}

	public String getRecipientId()
	{
		return RecipientId;
	}

	public void setRecipientId(String string)
	{
		RecipientId = string;
	}

	public Date getMsgRcvDate()
	{
		return msgRcvDate;
	}

	public String getMsgRcvDateText()
	{
		return df.format(msgRcvDate);
	}

	public void setMsgRcvDate(Date date)
	{
		msgRcvDate = date;
	}

	public boolean isRead()
	{
		return msgRcvDate == null ? false : true;
	}
	
	public boolean isUnread()
	{
		return msgRcvDate == null ? true : false;
	}
	
	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String string)
	{
		firstName = string;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String string)
	{
		lastName = string;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String string)
	{
		status = string;
	}

	public String getRoleName()
	{
		return roleName;
	}

	public void setRoleName(String string)
	{
		roleName = string;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String string)
	{
		email = string;
	}

	public Date getLastAccessTime()
	{
		return lastAccessTime;
	}

	public String getLastAccessTimeText()
	{
		return df.format(lastAccessTime);
	}

	public void setLastAccessTime(Date date)
	{
		lastAccessTime = date;
	}

	public int compareTo(Object o)
	{
		MessageRecipientBean mrb = (MessageRecipientBean) o;
		return this.RecipientId.compareTo(mrb.RecipientId);
	}
}
