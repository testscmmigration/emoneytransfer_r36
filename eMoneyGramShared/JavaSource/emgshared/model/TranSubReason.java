/*
 * Created on Jul 20, 2006
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
package emgshared.model;

/**
 * @author A119
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class TranSubReason {
    
    private String tranReasonTypeCode = null;
    private String tranReasonTypeDesc = null;
    private String tranSubReasonCode = null;
    private String tranSubReasonDesc = null;
       
    /**
     * @return Returns the tranReasonTypeCode.
     */
    public String getTranReasonTypeCode() {
        return tranReasonTypeCode;
    }
    /**
     * @param tranReasonTypeCode The tranReasonTypeCode to set.
     */
    public void setTranReasonTypeCode(String tranReasonTypeCode) {
        this.tranReasonTypeCode = tranReasonTypeCode;
    }
    /**
     * @return Returns the Tran Reason Type Desc
     */
    public String getTranReasonTypeDesc() {
        return tranReasonTypeDesc;
    }
    /**
     * @param set Tran Reason Type Desc
     */
    public void setTranReasonTypeDesc(String desc) {
        this.tranReasonTypeDesc = desc;
    }
    /**
     * @return Returns the tranSubReasonCode.
     */
    public String getTranSubReasonCode() {
        return tranSubReasonCode;
    }
    /**
     * @param tranSubReasonCode The tranSubReasonCode to set.
     */
    public void setTranSubReasonCode(String tranSubReasonCode) {
        this.tranSubReasonCode = tranSubReasonCode;
    }
    /**
     * @return Returns the tranSubReasonDesc.
     */
    public String getTranSubReasonDesc() {
        return tranSubReasonDesc;
    }
    /**
     * @param tranSubReasonDesc The tranSubReasonDesc to set.
     */
    public void setTranSubReasonDesc(String tranSubReasonDesc) {
        this.tranSubReasonDesc = tranSubReasonDesc;
    }
}
