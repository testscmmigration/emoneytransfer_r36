/*
 * Created on May 31, 2005
 *
 */
package emgshared.model;

import emgshared.util.I18NHelper;

/**
 * @author T349
 *
 */
public class ConsumerProfileCommentType
{

	private static final String CASE_NUMBER_CODE = "R001";
	private static final String BAD_IP_CODE = "R002";
	private static final String MANUAL_VERIFICATION_CODE = "R003";
	private static final String OTHER_CODE = "OTH";

	public static final ConsumerProfileCommentType
			CASE_NUMBER = new ConsumerProfileCommentType(CASE_NUMBER_CODE);
	public static final ConsumerProfileCommentType
			BAD_IP = new ConsumerProfileCommentType(BAD_IP_CODE);
	public static final ConsumerProfileCommentType
			MANUAL_VERIFICATION = new ConsumerProfileCommentType(MANUAL_VERIFICATION_CODE);
	public static final ConsumerProfileCommentType
		OTHER = new ConsumerProfileCommentType(OTHER_CODE);
	
	private final String reasonCode;

	public ConsumerProfileCommentType(String code)
	{
		this.reasonCode = code;
	}

	public static ConsumerProfileCommentType getInstance(String reasonCode)
	{
		ConsumerProfileCommentType code;
		if (CASE_NUMBER_CODE.equalsIgnoreCase(reasonCode))
		{
			code = CASE_NUMBER;
		} else if (BAD_IP_CODE.equalsIgnoreCase(reasonCode))
		{
			code = BAD_IP;
		} else if (
			MANUAL_VERIFICATION_CODE.equalsIgnoreCase(reasonCode))
		{
			code = MANUAL_VERIFICATION;
		} else if (OTHER_CODE.equalsIgnoreCase(reasonCode))
		{
			code = OTHER;
		} else
		{
			String errorMessage =
				I18NHelper.getFormattedMessage(
					"exception.invalid.param.value",
					new Object[] { reasonCode, "code" });
			throw new IllegalArgumentException(errorMessage);
		}
		return (code);
	}

	public String getStatusCode()
	{
		return reasonCode;
	}

	public String toString()
	{
		return reasonCode;
	}
}
