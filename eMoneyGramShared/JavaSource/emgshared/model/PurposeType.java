/*
 * Created on Aug 2, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgshared.model;

/**
 * @author A136
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class PurposeType {
	private String purposeTypeCode;
	private String purposeTypeDesc;
	
	
	public PurposeType()
	{
	}

	public PurposeType(String typeCode, String typeDesc)
	{
		this.purposeTypeCode = typeCode;
		this.purposeTypeDesc = typeDesc;
	}

	public String getPurposeTypeCode()
	{
		return purposeTypeCode;
	}

	public void setPurposeTypeCode(String string)
	{
		purposeTypeCode = string;
	}

	public String getPurposeTypeDesc()
	{
		return purposeTypeDesc;
	}

	public void setPurposeTypeDesc(String string)
	{
		purposeTypeDesc = string;
	}
    
    public String getLabel() {
        return purposeTypeDesc;
    }
    
    public String getValue() {
        return purposeTypeCode;
    }
}
