/*
 * Created on Dec 31, 2004
 *
 */
package emgshared.model;

import java.util.Calendar;
import java.util.GregorianCalendar;
/**
 * @author A131
 *
 */
public class ConsumerCreditCardAccount extends ConsumerAccount
{
	public static final String CREDIT_CODE = "C";
	public static final String DEBIT_CODE = "D";
	public static final String DEBITF_CODE = "F";
	public static final String UNKNOWN_CODE = "U";
	private String accountHolderFullName;
	private int expireMonth;
	private int expireYear;
	private String cvvNumber;
	private String encryptedText;
	private String accountNumberBin;
	private String accountNumberBinHash;
	private String creditOrDebitCode;
	private String accountNumberHash;
	private String cardToken;


	/**
	 * @param accountNumber
	 */
	public ConsumerCreditCardAccount(
		int id,
		int consumerId,
		AccountStatus status,
		ConsumerAccountType type,
		String typeDescription,
		String accountNumber,
		String accountNumberBin,
		String accountNumberBinHash,
		String accountNumberHash,
		ConsumerAddress billingAddress,
		TaintIndicatorType accountNumberTaintCode,
		TaintIndicatorType bankAbaTaintCode,
		TaintIndicatorType creditCardBinTaintCode)
	{
		super(
			id,
			consumerId,
			status,
			type,
			typeDescription,
			accountNumber,
			billingAddress,
			accountNumberTaintCode,
			bankAbaTaintCode,
			creditCardBinTaintCode);

		this.accountNumberBin = accountNumberBin;
		this.accountNumberBinHash = accountNumberBinHash;
		this.accountNumberHash = accountNumberHash;

		if (!type.isCreditCardAccount() && !type.isDebitCardAccount())
		    /* Commenting out as this won't be used due to CyberSource not
		     * being able to provide us an accurate list of cards types.
			&& type.isDebitCardAccount() == false)
			*/
		{
			throw new IllegalArgumentException("type must be a kind of credit or debit card account");
		}
	}

    public String getAccountHolderFullName() {
		return accountHolderFullName;
	}

    public String getCvvNumber() {
		return cvvNumber;
	}

    public int getExpireMonth() {
		return expireMonth;
	}

    public int getExpireYear() {
		return expireYear;
	}

    public void setAccountHolderFullName(String string) {
		accountHolderFullName = string;
	}

    public void setCvvNumber(String string) {
		cvvNumber = string;
	}

    public void setExpireMonth(int i) {
		expireMonth = i;
	}

    public void setExpireYear(int i) {
		expireYear = i;
	}

    public String getDisplayName() {
		if (getTypeDescription() != null) {
			return getTypeDescription() + ": " + super.getDisplayName();
		}
		return super.getDisplayName();
	}

    public boolean isValid() {
		return (isExpiredDate(this.expireYear, this.expireMonth) == false);
	}

    public static boolean isExpiredDate(int expireYear, int expireMonth) {
		Calendar calendar = GregorianCalendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		//  Add one to month because Calendar.get(Calendar.MONTH) returns
		//  a zero-based month.  In other words, Calendar.JANUARY == 0.
		int month = calendar.get(Calendar.MONTH) + 1;
		if (year > expireYear) {
			return true;
        } else if ((year == expireYear) && (month > expireMonth)){
			return true;
		}
		return false;
	}
	/**
	 * @return
	 */
    public String getEncryptedText() {
		return encryptedText;
	}

	/**
	 * @param string
	 */
    public void setEncryptedText(String string) {
		encryptedText = string;
	}

	/**
	 * @return
	 */
    public String getAccountNumberBinHash() {
		return accountNumberBinHash;
	}

	/**
	 * @return
	 */
    public String getCreditOrDebitCode() {
		return creditOrDebitCode =
			creditOrDebitCode == null ? UNKNOWN_CODE : creditOrDebitCode;
	}

	/**
	 * @param string
	 */
    public void setCreditOrDebitCode(String s) {
		creditOrDebitCode = s.equals(DEBITF_CODE) ? DEBIT_CODE : s;
	}

    public boolean isCardTypeCredit() {
		return (creditOrDebitCode.equals(CREDIT_CODE))
			|| (creditOrDebitCode.equals(UNKNOWN_CODE));
	}

    public boolean isCardTypeDebit() {
		return creditOrDebitCode.equals(DEBIT_CODE);
	}

    public void setType(ConsumerAccountType type) {
		this.type = type;
	}

	public String getAccountNumberHash() {
		return accountNumberHash;
	}

	public void setAccountNumberHash(String accountNumberHash) {
		this.accountNumberHash = accountNumberHash;
	}

	public String getAccountNumberBin() {
		return accountNumberBin;
	}

	public void setAccountNumberBin(String accountNumberBin) {
		this.accountNumberBin = accountNumberBin;
	}

	public String getCardToken() {
		return cardToken;
	}

	public void setCardToken(String cardToken) {
		this.cardToken = cardToken;
	}

	
}
