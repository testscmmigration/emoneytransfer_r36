/*
 * Created on Mar 1, 2005
 *
 */
package emgshared.model;

/**
 * @author A131
 *
 */
public class AccountSearchRequest
{
	private Integer custId;
	private String statusCode;

	/**
	 * @return
	 * 
	 * Created on Mar 1, 2005
	 */
	public Integer getCustId()
	{
		return custId;
	}

	/**
	 * @return
	 * 
	 * Created on Mar 1, 2005
	 */
	public String getStatusCode()
	{
		return statusCode;
	}

	/**
	 * @param integer
	 * 
	 * Created on Mar 1, 2005
	 */
	public void setCustId(Integer integer)
	{
		custId = integer;
	}

	/**
	 * @param string
	 * 
	 * Created on Mar 1, 2005
	 */
	public void setStatusCode(String string)
	{
		statusCode = string;
	}
}
