package emgshared.model;

public class MDBatch
{
	private String batchId;
	private String receiveDate;
	private String recCnt;

	public String getBatchId()
	{
		return batchId;
	}

	public void setBatchId(String string)
	{
		batchId = string;
	}

	public String getReceiveDate()
	{
		return receiveDate;
	}

	public void setReceiveDate(String string)
	{
		receiveDate = string;
	}

	public String getRecCnt()
	{
		return recCnt;
	}

	public void setRecCnt(String string)
	{
		recCnt = string;
	}
}
