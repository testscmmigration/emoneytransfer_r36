package emgshared.model;

import java.util.Date;
import java.util.List;

public class CustomerIDUploadInfo {
	
	private String idType;
	private String idNumber;
	private String countryOfIssue;
	private String stateOfIssue;
	private String idStatus;
	private String uploadReasonCode;
	private Date idLastUpdateDate;
	private Date idUploadDate;
	private String captifyReasonCode;
	private String mitekReasoncodes;
	private String sessionID;
	private String manualReviewRefId;
	private String fName;
	private String lName;
	private String dob;
	private String address;
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public String getCountryOfIssue() {
		return countryOfIssue;
	}
	public void setCountryOfIssue(String countryOfIssue) {
		this.countryOfIssue = countryOfIssue;
	}
	public String getStateOfIssue() {
		return stateOfIssue;
	}
	public void setStateOfIssue(String stateOfIssue) {
		this.stateOfIssue = stateOfIssue;
	}
	public String getIdStatus() {
		return idStatus;
	}
	public void setIdStatus(String idStatus) {
		this.idStatus = idStatus;
	}
	public String getUploadReasonCode() {
		return uploadReasonCode;
	}
	public void setUploadReasonCode(String uploadReasonCode) {
		this.uploadReasonCode = uploadReasonCode;
	}
	public Date getIdLastUpdateDate() {
		return idLastUpdateDate;
	}
	public void setIdLastUpdateDate(Date idLastUpdateDate) {
		this.idLastUpdateDate = idLastUpdateDate;
	}
	public Date getIdUploadDate() {
		return idUploadDate;
	}
	public void setIdUploadDate(Date idUploadDate) {
		this.idUploadDate = idUploadDate;
	}
	public String getCaptifyReasonCode() {
		return captifyReasonCode;
	}
	public void setCaptifyReasonCode(String captifyReasonCode) {
		this.captifyReasonCode = captifyReasonCode;
	}
	public String getMitekReasoncodes() {
		return mitekReasoncodes;
	}
	public void setMitekReasoncodes(String mitekReasoncodes) {
		this.mitekReasoncodes = mitekReasoncodes;
	}
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}
	public String getDob() { 
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getManualReviewRefId() {
		return manualReviewRefId;
	}
	public void setManualReviewRefId(String manualReviewRefId) {
		this.manualReviewRefId = manualReviewRefId;
	}   
	

}
