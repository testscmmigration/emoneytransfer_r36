package emgshared.model;

import java.sql.Timestamp;

public class SimpleCache
{

	public static final String TRANSACTION_SCORE = "TransactionScore";
	public static final String ADMIN_MESSAGE = "AdminMessage";
	public static final String COUNTRY_EXCEPTION_OVERRIDE = "CountryExOverride";

	private String name;
	private Timestamp timeStamp;

	/**
	 * @return
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param string
	 */
	public void setName(String string)
	{
		name = string;
	}

	/**
	 * @return
	 */
	public Timestamp getTimeStamp()
	{
		return timeStamp;
	}

	/**
	 * @param timestamp
	 */
	public void setTimeStamp(Timestamp timestamp)
	{
		timeStamp = timestamp;
	}

}
