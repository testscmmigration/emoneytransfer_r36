package emgshared.model;

import java.util.Date;

/**
 * Bean class which is being used to store all the document related details that
 * are being fetched from proc calls,prc_get_cust_doc_img_cv and
 * prc_get_cust_upld_doc_cv
 * 
 * @version 1.0
 * @author wx51
 */

public class ConsumerDocument {
    private Long customerId;
    private int docImgId;
    private Date docCreatedTime;
    private String docTypeCode;
    private String docImgFileName;
    private byte[] docImgBlob;

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public int getDocImgId() {
        return docImgId;
    }

    public void setDocImgId(int docImgId) {
        this.docImgId = docImgId;
    }

    public Date getDocCreatedTime() {
        return docCreatedTime;
    }

    public void setDocCreatedTime(Date docCreatedTime) {
        this.docCreatedTime = docCreatedTime;
    }

    public String getDocTypeCode() {
        return docTypeCode;
    }

    public void setDocTypeCode(String docTypeCode) {
        this.docTypeCode = docTypeCode;
    }

    public String getDocImgFileName() {
        return docImgFileName;
    }

    public void setDocImgFileName(String docImgFileName) {
        this.docImgFileName = docImgFileName;
    }

    public byte[] getDocImgBlob() {
        return docImgBlob.clone();
    }

    public void setDocImgBlob(byte[] docImgBlob) {
        this.docImgBlob = docImgBlob.clone();
    }
}
