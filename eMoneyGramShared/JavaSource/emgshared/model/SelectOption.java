/*
 */
package emgshared.model;

/**
 * @author A119
 *
 */
public class SelectOption {
	private String displayValue;
	private String formValue;
	
	public SelectOption(String display, String value) {
		this.setDisplayValue(display);
		this.setFormValue(value);
	}
	public String getDisplayValue() {
		return displayValue;
	}
	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}
	public String getFormValue() {
		return formValue;
	}
	public void setFormValue(String formValue) {
		this.formValue = formValue;
	}
	

}
