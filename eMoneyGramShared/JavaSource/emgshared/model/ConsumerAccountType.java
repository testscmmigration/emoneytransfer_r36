package emgshared.model;

import java.io.Serializable;

import emgshared.util.I18NHelper;
//TODO tom c. - DOES NOT IMPLEMENT SERIALIZABLE
public class ConsumerAccountType implements Serializable
{
	private static final String CREDIT_CARD_VISA_CODE = "CC-VISA";
	private static final String CREDIT_CARD_MASTERCARD_CODE = "CC-MSTR";
	private static final String CREDIT_CARD_DISCOVER_CODE = "CC-DSCVR";
	
	/* Commenting out as this won't be used due to CyberSource not 
     * being able to provide us an accurate list of cards types.
	 private static final String DEBIT_CARD_VISA_CODE = "DC-VISA";
	 private static final String DEBIT_CARD_MASTERCARD_CODE = "DC-MSTR";
	 */

	// The consumer site decided to use 'CC-MAEST' so EMT Admin is honoring it.
	private static final String DEBIT_CARD_MAESTRO_CODE = "CC-MAEST";
	private static final String BANK_CHECKING_CODE = "BANK-CHK";
	private static final String BANK_SAVINGS_CODE = "BANK-SAV";

	public static final ConsumerAccountType CREDIT_CARD_VISA =
		new ConsumerAccountType(CREDIT_CARD_VISA_CODE, true, false, true, false,
		        false, false, false, true, false,false);
	public static final ConsumerAccountType CREDIT_CARD_DISCOVER =
		new ConsumerAccountType(CREDIT_CARD_DISCOVER_CODE, true, false, true, false,
		        false, false, false, false, false,true);
	public static final ConsumerAccountType CREDIT_CARD_MASTERCARD =
		new ConsumerAccountType(CREDIT_CARD_MASTERCARD_CODE, true, false, true, 
		        false, false, false, false, false, true, false);
	public static final ConsumerAccountType DEBIT_CARD_MAESTRO = 
	    new ConsumerAccountType(DEBIT_CARD_MAESTRO_CODE, true, false, false, 
	            true, false, false, true, false, false, false);
	/* Commenting out as this won't be used due to CyberSource not 
	 * being able to provide us an accurate list of cards types.
	 public static final ConsumerAccountType DEBIT_CARD_VISA =
	 	new ConsumerAccountType(DEBIT_CARD_VISA_CODE, true, false, false, true, true, false, false, false);
	 public static final ConsumerAccountType DEBIT_CARD_MASTERCARD =
	 	new ConsumerAccountType(DEBIT_CARD_MASTERCARD_CODE, true, false, false, true, false, true, false, false);
	*/
	public static final ConsumerAccountType BANK_CHECKING =
		new ConsumerAccountType(BANK_CHECKING_CODE, false, true, false, false,
		        false, false, false, false, false, false);
	public static final ConsumerAccountType BANK_SAVINGS =
		new ConsumerAccountType(BANK_SAVINGS_CODE, false, true, false, false,
		        false, false, false, false, false, false);
	private final String code;
	private final boolean cardAccount;
	private final boolean bankAccount;
	private final boolean creditCardAccount;
	private final boolean debitCardAccount;
//	private final boolean visaDebitCardAccount;
//	private final boolean mstrDebitCardAccount;
	private final boolean maestroDebitCardAccount;
	private final boolean visaCreditCardAccount;
	private final boolean mstrCreditCardAccount;
	private final boolean dscvrCreditCardAccount;

	private ConsumerAccountType(
		String code,
		boolean cardAccount,
		boolean bankAccount,
		boolean creditCardAccount,
		boolean debitCardAccount,
		boolean visaDebitCardAccount,
		boolean mstrDebitCardAccount,
		boolean maestroDebitCardAccount,
		boolean visaCreditCardAccount,
		boolean mstrCreditCardAccount,
		boolean dscvrCreditCardAccount)
	{
		this.code = code;
		this.cardAccount = cardAccount;
		this.bankAccount = bankAccount;
		this.creditCardAccount = creditCardAccount;
		this.debitCardAccount = debitCardAccount;
//		this.visaDebitCardAccount = visaDebitCardAccount;
//		this.mstrDebitCardAccount = mstrDebitCardAccount;
		this.maestroDebitCardAccount = maestroDebitCardAccount;
		this.visaCreditCardAccount = visaCreditCardAccount;
		this.mstrCreditCardAccount = mstrCreditCardAccount;
		this.dscvrCreditCardAccount = dscvrCreditCardAccount;
	}

	public static ConsumerAccountType getInstance(String code)
	{
		ConsumerAccountType status;
		if (CREDIT_CARD_VISA_CODE.equalsIgnoreCase(code))
		{
			status = CREDIT_CARD_VISA;
		} else if (CREDIT_CARD_MASTERCARD_CODE.equalsIgnoreCase(code))
		{
			status = CREDIT_CARD_MASTERCARD;
		} else if (CREDIT_CARD_DISCOVER_CODE.equalsIgnoreCase(code))
		{
			status = CREDIT_CARD_DISCOVER;
		} else if (DEBIT_CARD_MAESTRO_CODE.equalsIgnoreCase(code))
		{
		    status = DEBIT_CARD_MAESTRO;
		/* Commenting out as this won't be used due to CyberSource not 
	     * being able to provide us an accurate list of cards types.
	 	} else if (DEBIT_CARD_VISA_CODE.equalsIgnoreCase(code))
		{
			status = DEBIT_CARD_VISA;
		} else if (DEBIT_CARD_MASTERCARD_CODE.equalsIgnoreCase(code))
		{
			status = DEBIT_CARD_MASTERCARD;
		}*/
		} else if (BANK_CHECKING_CODE.equalsIgnoreCase(code))
		{
			status = BANK_CHECKING;
		} else if (BANK_SAVINGS_CODE.equalsIgnoreCase(code))
		{
			status = BANK_SAVINGS;
		} else
		{
			String errorMessage =
				I18NHelper.getFormattedMessage(
					"exception.invalid.param.value",
					new Object[] { code, "code" });
			throw new IllegalArgumentException(errorMessage);
		}
		return (status);
	}

	public String getCode()
	{
		return code;
	}

	public boolean isCardAccount()
	{
		return cardAccount;
	}
	
	public boolean isBankAccount()
	{
		return bankAccount;
	}

	public boolean isCreditCardAccount()
	{
		return creditCardAccount;
	}
	
	public boolean isDebitCardAccount()
	{
	    return debitCardAccount;
	}

	public boolean isMaestroDebitCardAccount()
	{
	    return maestroDebitCardAccount; 
	}
	/* Commenting out as this won't be used due to CyberSource not 
	 * being able to provide us an accurate list of cards types.
	public boolean isDebitCardAccount()
	{
		return debitCardAccount;
	}
	
	public boolean isVisaDebitCardAccount()
	{
		return visaDebitCardAccount;
	}
	
	public boolean isMstrDebitCardAccount()
	{
		return mstrDebitCardAccount;
	}
	*/
	public boolean isVisaCreditCardAccount()
	{
		return visaCreditCardAccount;
	}
	
	public boolean isDiscoverCreditCardAccount()
	{
		return dscvrCreditCardAccount;
	}
	public boolean isMstrCreditCardAccount()
	{
		return mstrCreditCardAccount;
	}
}
