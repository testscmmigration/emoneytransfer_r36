/*
 * Created on Dec 31, 2004
 *
 */
package emgshared.model;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import emgshared.util.EUSites;

/**
 * @author A131
 *
 */
public class BaseAddress implements Serializable
{
	public static final String UK_COUNTRY_CODE = "GBR"; 
    public static final String CANADA_COUNTRY_CODE = "CAN";
	private String addressLine1;
	private String addressLine2;
	private String addressLine3;	
	private String city;
	private String state;
	private String isoCountryCode;
	private String postalCode;
	private String zip4;
	private String countyName;

	public BaseAddress()
	{
	}

	/**
	 * Copy constructor.
	 * 
	 * @param source	- must not be null
	 * @throws IllegalArgumentException
	 */
	public BaseAddress(BaseAddress source)
	{
		Validate.notNull(
			source,
			"source object is required and must not be null.");
		this.addressLine1 = source.getAddressLine1();
		this.addressLine2 = source.getAddressLine2();
		this.addressLine3 = source.getAddressLine3();		
		this.city = source.getCity();
		this.state = source.getState();
		this.isoCountryCode = source.getIsoCountryCode();
		this.postalCode = source.getPostalCode();
		this.zip4 = source.getZip4();
		this.countyName = source.getCountyName();
	}

	public String getAddressLine1()
	{
		return addressLine1;
	}

	public String getAddressLine2()
	{
		return addressLine2;
	}

	public String getAddressLine3()
	{
		return addressLine3;
	}

	public String getCity()
	{
		return city;
	}

	public String getPostalCode()
	{
		return postalCode;
	}

	public String getPostalCodePlusZip4()
	{
		StringBuilder buffer = new StringBuilder(getPostalCodePlusZip4length());
		if (postalCode != null)
		{
			buffer.append(postalCode);
			if (zip4 != null)
			{
				buffer.append(zip4);
			}
		}
		return buffer.toString();
	}
	public int getPostalCodePlusZip4length(){
		int postalCodeLength = postalCode != null? postalCode.length() : 0;
		int zip4Length = zip4 != null ? zip4.length() : 0;
		return postalCodeLength + zip4Length;
	}

	public String getState()
	{
		return state;
	}

	public String getIsoCountryCode()
	{
		return isoCountryCode;
	}

	public String getZip4()
	{
		return zip4;
	}

	public void setAddressLine1(String string)
	{
		addressLine1 = StringUtils.trimToNull(string);
	}

	public void setAddressLine2(String string)
	{
		addressLine2 = StringUtils.trimToNull(string);
	}

	public void setAddressLine3(String string)
	{
		addressLine3 = StringUtils.trimToNull(string);
	}

	public void setCity(String string)
	{
		city = StringUtils.trimToNull(string);
	}

	public void setPostalCode(String string)
	{
		postalCode = StringUtils.trimToNull(string);
	}

	public void setPostalCodePlusZip4(String string)
	{
		String temp = StringUtils.trimToNull(string);
		if (temp != null)
		{
			if (temp.length() > 5
					&& !(UK_COUNTRY_CODE.equals(isoCountryCode) || CANADA_COUNTRY_CODE
                            .equals(isoCountryCode) || EUSites.contains(isoCountryCode))) {
				postalCode = temp.substring(0, 5);
				zip4 = temp.substring(5);	
			} else
			{
				postalCode = temp;
				zip4 = null;
			}
		} else
		{
			postalCode = null;
			zip4 = null;
		}
	}

	public void setState(String string)
	{
		state = StringUtils.trimToNull(string);
	}

	public void setIsoCountryCode(String string)
	{
		string = StringUtils.trimToNull(string);
		if (string == null || string.length() != 3)
		{
			throw new IllegalArgumentException("ISO Country Code must be three characters");
		}
		isoCountryCode = string;
	}

	public void setZip4(String string)
	{
		zip4 = string;
	}

	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}

	public String getCountyName() {
		return countyName;
	}

	
}
