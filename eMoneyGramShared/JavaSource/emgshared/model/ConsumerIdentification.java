package emgshared.model;

import java.util.Date;

/**
 * This class represents a consumer identification e.g. passport, driver license, etc.
 * @author up30
 *
 */
public class ConsumerIdentification {

	public static final String ECDL = "ECDL";
	public static final String SP = "SP";
	public static final String IPSP = "IPSP";
	public static final String SNID = "SNID";
	public static final String EUID = "EUID";
	public static final String TEMP_RESIDENT_ID = "TEMP_RESIDENT_ID";

	public static final String DRIVER_LICENSE = "Driver License";
	public static final String PASSPORT = "Passport";
	public static final String NATIONAL_ID = "National ID";
	public static final String CONSUMER_PASSPORT = "Consumer Passport Number";
	public static final String EUROPEAN_ID = "European ID";

	public String id;
	public String idNumber;
	public String idType;
	public String idTypeFriendlyName;
	public String docStatusCode;
	//MBO : 1800 ::::::::: starts
	public Date idExpirationDate;
	//MBO-4742 
	public String mrzLine1;
	public String mrzLine2;
	public String mrzLine3;
	
	public Date getIdExpirationDate() {
		return idExpirationDate;
	}
	public void setIdExpirationDate(Date idExpirationDate) {
		this.idExpirationDate = idExpirationDate;
	}
	public String idIssueCountry;
	public String getIdIssueCountry() {
		return idIssueCountry;
	}
	public void setIdIssueCountry(String idIssueCountry) {
		this.idIssueCountry = idIssueCountry;
	}
//MBO : 1800 ::::::::: ends 
	
	private ConsumerIdentification(String id, String idNumber, String idType, String idTypeFriendlyName, String docStatusCode) {
		super();
		this.id = id;
		this.idNumber = idNumber;
		this.idType = idType;
		this.idTypeFriendlyName = idTypeFriendlyName;
		this.docStatusCode = docStatusCode;
	}
	//MBO : 1800 ::::::::: 
	private ConsumerIdentification(String id, String idNumber, String idType,
			String idTypeFriendlyName, String docStatusCode,
			Date idExpirationDate, String idIssueCountry, String mrzLine1,
			String mrzLine2, String mrzLine3) {
		super();
		this.id = id;
		this.idNumber = idNumber;
		this.idType = idType;
		this.idTypeFriendlyName = idTypeFriendlyName;
		this.docStatusCode = docStatusCode;
		this.idExpirationDate = idExpirationDate;
		this.idIssueCountry = idIssueCountry;
		this.mrzLine1 = mrzLine1;
		this.mrzLine2 = mrzLine2;
		this.mrzLine3 = mrzLine3;
	}
	//MBO : 1800 ::::::::: 
	
	public static ConsumerIdentification getConsumer(String id,
			String idNumber, String type, String docStatusCode,
			Date idExpirationDate,String idTypeFriendlyName, String idissueCountry, String mrzLine1,
			String mrzLine2, String mrzLine3) {
		
		return new ConsumerIdentification(id, idNumber, type,
				idTypeFriendlyName, docStatusCode, idExpirationDate,
				idissueCountry, mrzLine1, mrzLine2, mrzLine3);		
	}

	public String getIdNumber() {
		return idNumber;
	}

	public String getIdType() {
		return idType;
	}

	public String getIdTypeFriendlyName() {
		return idTypeFriendlyName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getDocStatusCode() {
		return docStatusCode;
	}

	public void setDocStatusCode(String docStatusCode) {
		this.docStatusCode = docStatusCode;
	}
	public String getMrzLine1() {
		return mrzLine1;
	}
	public void setMrzLine1(String mrzLine1) {
		this.mrzLine1 = mrzLine1;
	}
	public String getMrzLine2() {
		return mrzLine2;
	}
	public void setMrzLine2(String mrzLine2) {
		this.mrzLine2 = mrzLine2;
	}
	public String getMrzLine3() {
		return mrzLine3;
	}
	public void setMrzLine3(String mrzLine3) {
		this.mrzLine3 = mrzLine3;
	}
	
	

}
