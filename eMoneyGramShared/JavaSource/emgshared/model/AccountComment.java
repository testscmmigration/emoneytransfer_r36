/*
 * Created on Mar 2, 2005
 *
 */
package emgshared.model;

/**
 * @author A131
 *
 */
public class AccountComment extends Comment
{
	private int accountId;

	public int getAccountId()
	{
		return accountId;
	}

	public void setAccountId(int i)
	{
		accountId = i;
	}
}
