package emgshared.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BlockedIP 
	implements Comparable {

	private String ipAddress1;
	private String ipAddress2;	
	private String reasonCode;
	private String reasonDesc;
	private String comment;
	private String statusCode;
	private String statusDesc;
	private String fraudRiskLevelCode;
	private String createDate;
	private String createUserId;
	private String statusUpdateDate;
	private String updateUserId;

	/**
	 * @return
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @return
	 */
	public String getCreateDate() {
		return getFormattedCreateDate(createDate);
	}

	/**
	 * @return
	 */
	public String getCreateUserId() {
		return createUserId;
	}


	/**
	 * @return
	 */
	public String getReasonCode() {
		return reasonCode;
	}

	/**
	 * @return
	 */
	public String getReasonDesc() {
		return reasonDesc;
	}

	/**
	 * @return
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * @return
	 */
	public String getStatusUpdateDate() {
		return getFormattedStatusUpdateDate(statusUpdateDate);
	}

	/**
	 * @return
	 */
	public String getUpdateUserId() {
		return updateUserId;
	}

	/**
	 * @param string
	 */
	public void setComment(String string) {
		comment = string;
	}

	/**
	 * @param string
	 */
	public void setCreateDate(String string) {
		createDate = string;
	}

	/**
	 * @param string
	 */
	public void setCreateUserId(String string) {
		createUserId = string;
	}


	/**
	 * @param string
	 */
	public void setReasonCode(String string) {
		reasonCode = string;
	}

	/**
	 * @param string
	 */
	public void setReasonDesc(String string) {
		reasonDesc = string;
	}

	/**
	 * @param string
	 */
	public void setStatusCode(String string) {
		statusCode = string;
	}

	/**
	 * @param string
	 */
	public void setStatusUpdateDate(String string) {
		statusUpdateDate = string;
	}

	/**
	 * @param string
	 */
	public void setUpdateUserId(String string) {
		updateUserId = string;
	}

	/**
	 * @return
	 */
	public String getIpAddress1() {
		return ipAddress1;
	}

	/**
	 * @return
	 */
	public String getIpAddress2() {
		return ipAddress2;
	}

	/**
	 * @param string
	 */
	public void setIpAddress1(String string) {
		ipAddress1 = string;
	}

	/**
	 * @param string
	 */
	public void setIpAddress2(String string) {
		ipAddress2 = string;
	}

	/**
	 * @return
	 */
	public String getFraudRiskLevelCode() {
		return fraudRiskLevelCode;
	}

	/**
	 * @param string
	 */
	public void setFraudRiskLevelCode(String string) {
		fraudRiskLevelCode = string;
	}

	/**
	 * @return
	 */
	public String getStatusDesc() {
		return statusDesc;
	}

	/**
	 * @param string
	 */
	public void setStatusDesc(String string) {
		statusDesc = string;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o) {
		BlockedIP other = (BlockedIP) o;
		return ipAddress1.compareTo(other.ipAddress1);
	}

	public String getFormattedCreateDate(String date) {
		return format(date);
	}

	public String getFormattedStatusUpdateDate(String date) {
		return format(date);
	}

	private String format(String date) {
		try {
			DateFormat fromFormat = new SimpleDateFormat("dd/mm/yyyy hh:mm:ss");
			DateFormat toFormat = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");		
			Date d = fromFormat.parse(date);
			return toFormat.format(d);
		} catch (ParseException e) {
			return date;
		} catch(RuntimeException e) {
			throw e;
		}
	}
}
