/*
 * Created on Aug 1, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package emgshared.model;

import java.math.BigDecimal;

/**
 * @author a234
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class StateProvinceLimit {
	private String stateAbbrev; 
	private String stateName;
	private String limit; 
	private float fLimit;  
	private BigDecimal bLimit;
//	private String currencyCode; 
	

	/**
	 * @return Returns the limit.
	 */
	public String getRawLimit() {
		return limit;
	}
	/**
	 * @param limit The limit to set.
	 */
	public void setLimit(String limit) {
		this.limit = limit;
		bLimit = new BigDecimal(limit);
		fLimit = Float.valueOf(limit).floatValue();
	}
	/**
	 * @return Returns the stateAbbrev.
	 */
	public String getStateAbbrev() {
		return stateAbbrev;
	}
	/**
	 * @param stateAbbrev The stateAbbrev to set.
	 */
	public void setStateAbbrev(String stateAbbrev) {
		this.stateAbbrev = stateAbbrev;
	}
	/**
	 * @return Returns the stateName.
	 */
	public String getStateName() {
		return stateName;
	}
	/**
	 * @param stateName The stateName to set.
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	
	public float getLimitAsFloat() {
		return fLimit; 
	}
	
	public BigDecimal getLimitAsBigDeciomal() {
		return bLimit;
	}
}
