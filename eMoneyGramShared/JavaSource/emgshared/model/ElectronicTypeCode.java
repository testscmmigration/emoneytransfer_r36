/*
 * Created on April 27, 2005
 *
 */
package emgshared.model;

import java.io.Serializable;

import emgshared.util.I18NHelper;

/**
 * @author T349
 *
 */
public class ElectronicTypeCode implements Serializable
{

	private static final String EMAIL_CODE = "EMAIL";

	public static final ElectronicTypeCode EMAIL = 
		new ElectronicTypeCode(EMAIL_CODE);

	private final String typeCode;

	public ElectronicTypeCode(String type)
	{
		this.typeCode = type;
	}

	public static ElectronicTypeCode getInstance(String statusCode)
	{
		ElectronicTypeCode electronicTypeCode;
		if (EMAIL_CODE.equalsIgnoreCase(statusCode))
		{
			electronicTypeCode = EMAIL;
		} else
		{
			String errorMessage =
				I18NHelper.getFormattedMessage(
					"exception.invalid.param.value",
					new Object[] { statusCode, "code" });
			throw new IllegalArgumentException(errorMessage);
		}
		return (electronicTypeCode);
	}

	public String getTypeCode()
	{
		return typeCode;
	}

	public boolean isEmailType()
	{
		return EMAIL_CODE.equalsIgnoreCase(this.typeCode);
	}

	public String toString()
	{
		return typeCode;
	}
}
