package emgshared.model;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

public class CountryExceptionBean implements Comparable {

	private String rcvIsoCntryCode;
	private String isoCntryCode;
	private String rcvCountryName;
	private String sndCountryName;
	private String sndAllowFlag;
	private float sndMaxAmtNbr;  
	private float sndThrldWarnAmtNbr;
	private String excpCmntText;
	private String manualReviewAllowFlag;
	// private String tranAuthFlag;
	private static NumberFormat nf=new DecimalFormat("#0.00");
	
	public String getRcvIsoCntryCode() {
		return rcvIsoCntryCode == null ?  "" : rcvIsoCntryCode;
	}

	public void setRcvIsoCntryCode(String string) {
		rcvIsoCntryCode = string;
	}

	public String getIsoCntryCode() {
		return isoCntryCode == null ? "" : isoCntryCode;
	}

	public void setIsoCntryCode(String string) {
		isoCntryCode = string;
	}

	public String getSndAllowFlag() {
		return sndAllowFlag == null ? "" : sndAllowFlag;
	}

	public void setSndAllowFlag(String string) {
		sndAllowFlag = string;
	}
	
	public void setManualReviewAllowFlag(String string){
		manualReviewAllowFlag = string;
	}
	
	public String getManualReviewAllowFlag(){
		return manualReviewAllowFlag == null ? "" : manualReviewAllowFlag;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isSndAllowed() {
		return "Y".equalsIgnoreCase(sndAllowFlag);
	}
	
	public boolean isManualReviewAllowed(){
		return "Y".equalsIgnoreCase(manualReviewAllowFlag);
	}
	
	public String getSndMaxAmt() {
		return nf.format(getSndMaxAmtNbr());
	}

	public void setSndMaxAmt(String string) throws ParseException
	{
		setSndMaxAmtNbr(nf.parse(string).floatValue());
	}

	public String getSndThrldWarnAmt() {
		return nf.format(getSndThrldWarnAmtNbr());
	}

	public void setSndThrldWarnAmt(String string) throws ParseException
	{
		sndThrldWarnAmtNbr =nf.parse(string).floatValue();
	}

	public String getExcpCmntText() {
		return excpCmntText == null ? "" : excpCmntText;
	}

	public void setExcpCmntText(String string) {
		excpCmntText = string;
	}

	public int compareTo(Object o) {
		CountryExceptionBean ceb = (CountryExceptionBean) o;
		return rcvIsoCntryCode.compareTo(ceb.rcvIsoCntryCode);
	}
/**
 * @return
 */
public float getSndMaxAmtNbr()
{
	return sndMaxAmtNbr;
}

/**
 * @param f
 */
public void setSndMaxAmtNbr(float f)
{
	sndMaxAmtNbr = f;
}

	/**
	 * @return
	 */
	public float getSndThrldWarnAmtNbr()
	{
		return sndThrldWarnAmtNbr;
	}

	/**
	 * @param f
	 */
	public void setSndThrldWarnAmtNbr(float f)
	{
		sndThrldWarnAmtNbr = f;
	}

	/**
	 * @return
	 */
	public String getRcvCountryName() {
		return rcvCountryName;
	}

	/**
	 * @param string
	 */
	public void setRcvCountryName(String name) {
		rcvCountryName = name;
	}

	/**
	 * @return
	 */
	public String getSndCountryName() {
		return sndCountryName;
	}

	/**
	 * @param string
	 */
	public void setSndCountryName(String name) {
		sndCountryName = name;
	}

}
