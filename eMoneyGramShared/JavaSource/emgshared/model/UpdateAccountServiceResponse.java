/**
 * 
 */
package emgshared.model;

/**
 * @author aip8
 *
 */
public class UpdateAccountServiceResponse {
	
	private String status;
	
	private Error error;

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the error
	 */
	public Error getError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(Error error) {
		this.error = error;
	}
	
	

}
