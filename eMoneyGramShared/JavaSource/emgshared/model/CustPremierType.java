package emgshared.model;

public class CustPremierType {

    private String custPrmrCode;
    private String custPrmrDesc;

    public CustPremierType(String code, String desc) {
        this.custPrmrCode = code;
        this.custPrmrDesc = desc;
    }
    
    public String getCustPrmrCode() {
        return custPrmrCode;
    }

    public void setCustPrmrCode(String custPrmrCode) {
        this.custPrmrCode = custPrmrCode;
    }

    public String getCustPrmrDesc() {
        return custPrmrDesc;
    }

    public void setCustPrmrDesc(String custPrmrDesc) {
        this.custPrmrDesc = custPrmrDesc;
    }
    
    public String getLabel() {
        return custPrmrDesc;
    }
    
    public String getValue() {
        return custPrmrCode;
    }
}
