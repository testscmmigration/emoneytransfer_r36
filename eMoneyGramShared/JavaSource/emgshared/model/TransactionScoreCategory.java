package emgshared.model;


public class TransactionScoreCategory implements Comparable
{
	private int emgTranId;
	private int scoreCnfgId;
	private String scoreCatCode;
	private String scoreCatName;
	private String scoreCatDesc;
	private String listTypeCode;
	private String dataTypeCode;
	private String inclFlag;
	private String scoreQueryRsltData;
	private int catWgtNbr;
	private String wgtPct;				//  not a database field
	private int rawScoreNbr;
	private int wgtScoreNbr;

	public int getEmgTranId()
	{
		return emgTranId;
	}

	public void setEmgTranId(int i)
	{
		emgTranId = i;
	}

	public int getScoreCnfgId()
	{
		return scoreCnfgId;
	}

	public void setScoreCnfgId(int i)
	{
		scoreCnfgId = i;
	}

	public String getScoreCatCode()
	{
		return scoreCatCode;
	}

	public void setScoreCatCode(String string)
	{
		scoreCatCode = string;
	}

	public String getScoreCatName()
	{
		return scoreCatName;
	}

	public void setScoreCatName(String string)
	{
		scoreCatName = string;
	}

	public String getScoreCatDesc()
	{
		return scoreCatDesc;
	}

	public void setScoreCatDesc(String string)
	{
		scoreCatDesc = string;
	}

	public String getListTypeCode()
	{
		return listTypeCode;
	}

	public void setListTypeCode(String string)
	{
		listTypeCode = string;
	}

	public String getDataTypeCode()
	{
		return dataTypeCode;
	}

	public void setDataTypeCode(String string)
	{
		dataTypeCode = string;
	}

	public String getInclFlag()
	{
		return inclFlag;
	}

	public void setInclFlag(String string)
	{
		inclFlag = string;
	}

	public String getScoreQueryRsltData()
	{
		return scoreQueryRsltData == null ? "" : scoreQueryRsltData;
	}

	public void setScoreQueryRsltData(String string)
	{
		scoreQueryRsltData = string;
	}

	public int getCatWgtNbr()
	{
		return catWgtNbr;
	}

	public void setCatWgtNbr(int i)
	{
		catWgtNbr = i;
	}

	public String getWgtPct()
	{
		return wgtPct;
	}

	public void setWgtPct(String string)
	{
		wgtPct = string;
	}

	public int getRawScoreNbr()
	{
		return rawScoreNbr;
	}

	public void setRawScoreNbr(int i)
	{
		rawScoreNbr = i;
	}

	public int getWgtScoreNbr()
	{
		return wgtScoreNbr;
	}

	public void setWgtScoreNbr(int i)
	{
		wgtScoreNbr = i;
	}
	
	public int compareTo(Object o) {
		TransactionScoreCategory other = (TransactionScoreCategory) o;
		
		return Integer.parseInt(this.scoreCatCode) - Integer.parseInt(other.scoreCatCode);
	}
	
}
