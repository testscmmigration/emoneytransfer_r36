/*
 * Created on Aug 1, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package emgshared.model;

/**
 * @author A119
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class KeyColumns {
    
    private String columnName;
    private String tableName;
    private Integer columnSequenceNumber;

    /**
     * @return Returns the columnName.
     */
    public String getColumnName() {
        return columnName;
    }
    /**
     * @param columnName The columnName to set.
     */
    public void setColumnName(String cn) {
        this.columnName = cn;
    }
    /**
     * @return Returns the columnSequenceNumber.
     */
    public Integer getColumnSequenceNumber() {
        return columnSequenceNumber;
    }
    /**
     * @return Returns the columnSequenceNumber.
     */
    public int getColumnSequenceNumberInt() {
        return columnSequenceNumber.intValue();
    }
    /**
     * @param columnSequenceNumber The columnSequenceNumber to set.
     */
    public void setColumnSequenceNumber(Integer csn) {
        this.columnSequenceNumber = csn;
    }

    /**
     * @param columnSequenceNumber The columnSequenceNumber to set.
     */
    public void setColumnSequenceNumber(int csn) {
        this.setColumnSequenceNumber(Integer.valueOf(csn));
    }
/**
     * @return Returns the tableName.
     */
    public String getTableName() {
        return tableName;
    }
    /**
     * @param tableName The tableName to set.
     */
    public void setTableName(String tn) {
        this.tableName = tn;
    }
}
