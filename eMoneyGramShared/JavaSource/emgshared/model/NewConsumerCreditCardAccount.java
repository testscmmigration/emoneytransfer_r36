/*
 * Created on Jan 20, 2005
 *
 */
package emgshared.model;

import emgshared.services.ObfuscationService;
import emgshared.services.ServiceFactory;
import emgshared.util.StringHelper;

/**
 * @author A131
 *
 */
public class NewConsumerCreditCardAccount
{
	private static final int NEW_ACCOUNT_ID = -1;

	private final ConsumerCreditCardAccount account;
	private final String accountNumber;

	public NewConsumerCreditCardAccount(
		int consumerId,
		ConsumerAccountType type,
		String accountNumber,
		String accountNumberMask,
		String accountNumberBinHash,
		ConsumerAddress billingAddress,
		TaintIndicatorType accountNumberTaintCode,
		TaintIndicatorType bankAbaTaintCode,
		TaintIndicatorType creditCardBinTaintCode)
	{
		ObfuscationService os = ServiceFactory.getInstance().getObfuscationService();
		
		this.account =
			new ConsumerCreditCardAccount(
				NEW_ACCOUNT_ID,
				consumerId,
				AccountStatus.ACTIVE_VALIDATION_LEVEL_1,
				type,
				null,
				accountNumberMask,
				StringHelper.getCcBin(accountNumber),
				accountNumberBinHash,
				os.getCreditCardHash(accountNumber),
				billingAddress,
				accountNumberTaintCode,
				bankAbaTaintCode,
				creditCardBinTaintCode);
		this.accountNumber = accountNumber;
		this.account.setVersionNumber(1);
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getAccountHolderFullName()
	{
		return account.getAccountHolderFullName();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getAccountNumber()
	{
		return accountNumber;
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getAccountNumberMask()
	{
		return account.getAccountNumberMask();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getAccountNumberBinHash()
	{
		return account.getAccountNumberBinHash();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public ConsumerAccountType getAccountType()
	{
		return account.getAccountType();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public int getConsumerId()
	{
		return account.getConsumerId();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getCvvNumber()
	{
		return account.getCvvNumber();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public int getExpireMonth()
	{
		return account.getExpireMonth();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public int getExpireYear()
	{
		return account.getExpireYear();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public int getId()
	{
		return account.getId();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getStatusCode()
	{
		return account.getStatusCode();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getSubStatusCode()
	{
		return account.getSubStatusCode();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public int getVersionNumber()
	{
		return account.getVersionNumber();
	}

	/**
	 * @param string
	 *
	 * Created on Jan 20, 2005
	 */
	public void setAccountHolderFullName(String string)
	{
		account.setAccountHolderFullName(string);
	}

	/**
	 * @param string
	 *
	 * Created on Jan 20, 2005
	 */
	public void setCvvNumber(String string)
	{
		account.setCvvNumber(string);
	}

	/**
	 * @param i
	 *
	 * Created on Jan 20, 2005
	 */
	public void setExpireMonth(int i)
	{
		account.setExpireMonth(i);
	}

	/**
	 * @param i
	 *
	 * Created on Jan 20, 2005
	 */
	public void setExpireYear(int i)
	{
		account.setExpireYear(i);
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getAddressLine1()
	{
		return account.getAddressLine1();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getAddressLine2()
	{
		return account.getAddressLine2();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getCity()
	{
		return account.getCity();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getIsoCountryCode()
	{
		return account.getIsoCountryCode();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getPostalCode()
	{
		return account.getPostalCode();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getPostalCodePlusZip4()
	{
		return account.getPostalCodePlusZip4();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getState()
	{
		return account.getState();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getZip4()
	{
		return account.getZip4();
	}

	/**
	 * @param string
	 *
	 * Created on Jan 20, 2005
	 */
	public void setAddressLine1(String string)
	{
		account.setAddressLine1(string);
	}

	/**
	 * @param string
	 *
	 * Created on Jan 20, 2005
	 */
	public void setAddressLine2(String string)
	{
		account.setAddressLine2(string);
	}

	/**
	 * @param string
	 *
	 * Created on Jan 20, 2005
	 */
	public void setAddressLine3(String string)
	{
		account.setAddressLine3(string);
	}

	/**
	 * @param string
	 *
	 * Created on Jan 20, 2005
	 */
	public void setCity(String string)
	{
		account.setCity(string);
	}

	/**
	 * @param string
	 *
	 * Created on Jan 20, 2005
	 */
	public void setIsoCountryCode(String string)
	{
		account.setIsoCountryCode(string);
	}

	/**
	 * @param string
	 *
	 * Created on Jan 20, 2005
	 */
	public void setPostalCode(String string)
	{
		account.setPostalCode(string);
	}

	/**
	 * @param string
	 *
	 * Created on Jan 20, 2005
	 */
	public void setState(String string)
	{
		account.setState(string);
	}

	/**
	 * @param string
	 *
	 * Created on Jan 20, 2005
	 */
	public void setZip4(String string)
	{
		account.setZip4(string);
	}

	/**
	 * @return
	 *
	 * Created on Jan 26, 2005
	 */
	public int getAddressId()
	{
		return account.getAddressId();
	}

	public void setStatus(AccountStatus newStatus)
	{
		account.setStatus(newStatus);
	}
	/**
		 * @return
		 */
	public TaintIndicatorType getAccountNumberTaintCode()
	{
		return account.getAccountNumberTaintCode();
	}

	/**
	 * @return
	 */
	public TaintIndicatorType getCreditCardBinTaintCode()
	{
		return account.getCreditCardBinTaintCode();
	}

	/**
	 * @param type
	 */
	public void setAccountNumberTaintCode(TaintIndicatorType type)
	{
		account.setAccountNumberTaintCode(type);
	}

	/**
	 * @param type
	 */
	public void setCreditCardBinTaintCode(TaintIndicatorType type)
	{
		account.setCreditCardBinTaintCode(type);
	}
	/**
		 * @return
		 */
	public TaintIndicatorType getBankAbaTaintCode()
	{
		return account.getBankAbaTaintCode();
	}

	/**
	 * @param type
	 */
	public void setBankAbaTaintCode(TaintIndicatorType type)
	{
		account.setBankAbaTaintCode(type);
	}
	/**
		 * @return
		 */
	public String getCreditOrDebitCode()
	{
		String tmpCode = account.getCreditOrDebitCode();
		return tmpCode = tmpCode == null ? "U" : tmpCode;
	}

	/**
	 * @param type
	 */
	public void setCreditOrDebitCode(String s)
	{
		account.setCreditOrDebitCode(s);
	}
	
	public String getAccountNumberHash() {
		return account.getAccountNumberHash();
	}

	public void setAccountNumberHash(String accountNumberHash) {
		account.setAccountNumberHash(accountNumberHash);
	}

	public String getAccountNumberBin() {
		return account.getAccountNumberBin();
	}

	public void setAccountNumberBin(String accountNumberBin) {
		account.setAccountNumberBin(accountNumberBin);
	}	
}
