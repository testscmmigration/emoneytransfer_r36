/*
 * Created on Mar 3, 2005
 *
 */
package emgshared.model;

import emgshared.exceptions.ParseDateException;
import emgshared.util.DateFormatter;

/**
 * @author A131
 *
 */
public class TransactionSearchRequest
{
	DateFormatter df1 = new DateFormatter("MM/dd/yyyy", true);
	DateFormatter df2 = new DateFormatter("dd/MMM/yyyy", true);

	private Integer consumerId;
	private String beginDate;
	private String endDate;
	private String createBeginDate;
	private String createEndDate;
	private String accountTypeCode;
	private String tranStatCode;
	private String tranSubStatCode;
	private String tranType;
	private String custLogonId;
	private String receiverLastName;
	private String destinationCountry;
	private String bankAbaNbr;
	private String primaryAccountMaskNbr;
	private String secondaryAccountMaskNbr;
	private String ssnMaskNbr;
	private String useScore;
	private int lowScore;
	private int highScore;
	private String sysAutoRsltCode;
	private Integer tranId;
	private Double minimumAmount;
	private Double maximumAmount;
	private String tranDate;
	private java.util.Date statusDate;
	private String ipAddress;
	private String sndCustAcctId;
	private String rcvAgcyCode;
	private String tranRvwPrcsCode;
	private String retrievalRequestCode;
	private String referenceNumber;
	private String additionalIdMasked;
	private String additionalIdEncrypted;
	private String additionalIdType;

	private String docStatus;
	private String partnerSiteId;
	private String fullIdNumber;
	private String last4DigitsId;
	private String idType;
	private int bsnsCode;
	
	//added by Ankit Bhatt for MBO-128
	private String profileType;
	//ended

	public Integer getConsumerId()
	{
		return consumerId;
	}

	public void setConsumerId(Integer i)
	{
		consumerId = i;
	}


	/**
	 * @return
	 */
	public String getAccountTypeCode() {
		return accountTypeCode;
	}

	/**
	 * @return
	 */
	public String getBankAbaNbr() {
		return bankAbaNbr;
	}

	/**
	 * @return
	 */
	public String getBeginDate() {
		return beginDate;
	}

	/**
	 * @return
	 */
	public String getCustLogonId() {
		return custLogonId;
	}

	/**
	 * @return
	 */
	public String getDestinationCountry() {
		return destinationCountry;
	}

	/**
	 * @return
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @return
	 */
	public String getPrimaryAccountMaskNbr() {
		return primaryAccountMaskNbr;
	}

	/**
	 * @return
	 */
	public String getReceiverLastName() {
		return receiverLastName;
	}

	/**
	 * @return
	 */
	public String getSecondaryAccountMaskNbr() {
		return secondaryAccountMaskNbr;
	}

	/**
	 * @return
	 */
	public String getSsnMaskNbr() {
		return ssnMaskNbr;
	}

	/**
	 * @return
	 */
	public String getTranStatCode() {
		return tranStatCode;
	}

	/**
	 * @return
	 */
	public String getTranSubStatCode() {
		return tranSubStatCode;
	}

	/**
	 * @return
	 */
	public String getTranType() {
		return tranType;
	}

	/**
	 * @param string
	 */
	public void setAccountTypeCode(String string) {
		accountTypeCode = string;
	}

	/**
	 * @param string
	 */
	public void setBankAbaNbr(String string) {
		bankAbaNbr = string;
	}

	/**
	 * @param string
	 */
	public void setBeginDate(String string) {
		try
		{
			beginDate = df1.format(df2.parse(string));
		}
		catch (ParseDateException e)
		{
			beginDate = string;
		}
	}

	/**
	 * @param string
	 */
	public void setCustLogonId(String string) {
		custLogonId = string;
	}

	/**
	 * @param string
	 */
	public void setDestinationCountry(String string) {
		destinationCountry = string;
	}

	/**
	 * @param string
	 */
	public void setEndDate(String string) {
		try
		{
			endDate = df1.format(df2.parse(string));
		}
		catch (ParseDateException e)
		{
			endDate = string;
		}
	}

	/**
	 * @param string
	 */
	public void setPrimaryAccountMaskNbr(String string) {
		primaryAccountMaskNbr = string;
	}

	/**
	 * @param string
	 */
	public void setReceiverLastName(String string) {
		receiverLastName = string;
	}

	/**
	 * @param string
	 */
	public void setSecondaryAccountMaskNbr(String string) {
		secondaryAccountMaskNbr = string;
	}

	/**
	 * @param string
	 */
	public void setSsnMaskNbr(String string) {
		ssnMaskNbr = string;
	}

	/**
	 * @param string
	 */
	public void setTranStatCode(String string) {
		tranStatCode = string;
	}

	/**
	 * @param string
	 */
	public void setTranSubStatCode(String string) {
		tranSubStatCode = string;
	}

	/**
	 * @param string
	 */
	public void setTranType(String string) {
		tranType = string;
	}

	/**
	 * @return
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @return
	 */
	public String getTranDate() {
		return tranDate;
	}


	/**
	 * @param string
	 */
	public void setIpAddress(String string) {
		ipAddress = string;
	}

	/**
	 * @param string
	 */
	public void setTranDate(String string) {
		try
		{
			tranDate = df1.format(df2.parse(string));
		}
		catch (ParseDateException e)
		{
			tranDate = string;
		}
	}


	/**
	 * @param double1
	 */
	public void setMaximumAmount(Double double1) {
		maximumAmount = double1;
	}

	/**
	 * @param double1
	 */
	public void setMinimumAmount(Double double1) {
		minimumAmount = double1;
	}

	/**
	 * @param integer
	 */
	public void setTranId(Integer integer) {
		tranId = integer;
	}

	/**
	 * @return
	 */
	public Double getMaximumAmount() {
		return maximumAmount;
	}

	/**
	 * @return
	 */
	public Double getMinimumAmount() {
		return minimumAmount;
	}

	/**
	 * @return
	 */
	public Integer getTranId() {
		return tranId;
	}

	/**
	 * @return
	 */
	public String getSndCustAcctId() {
		return sndCustAcctId;
	}

	/**
	 * @param string
	 */
	public void setSndCustAcctId(String string) {
		sndCustAcctId = string;
	}

	/**
	 * @return
	 */
	public java.util.Date getStatusDate() {
		return statusDate;
	}

	/**
	 * @param date
	 */
	public void setStatusDate(java.util.Date date) {
		statusDate = date;
	}

	/**
	 * @return
	 */
	public String getRcvAgcyCode() {
		return rcvAgcyCode;
	}

	/**
	 * @param string
	 */
	public void setRcvAgcyCode(String string) {
		rcvAgcyCode = string;
	}

	/**
	 * @return
	 */
	public String getCreateBeginDate()
	{
		return createBeginDate;
	}

	/**
	 * @param string
	 */
	public void setCreateBeginDate(String string)
	{
		try
		{
			createBeginDate = df1.format(df2.parse(string));
		}
		catch (ParseDateException e)
		{
			createBeginDate = string;
		}
	}

	/**
	 * @return
	 */
	public String getCreateEndDate()
	{
		return createEndDate;
	}

	/**
	 * @param string
	 */
	public void setCreateEndDate(String string)
	{
		try
		{
			createEndDate = df1.format(df2.parse(string));
		}
		catch (ParseDateException e)
		{
			createEndDate = string;
		}
	}

	/**
	 * @return
	 */
	public String getUseScore()
	{
		return useScore;
	}

	/**
	 * @param string
	 */
	public void setUseScore(String string)
	{
		useScore = string;
	}

	/**
	 * @return
	 */
	public int getLowScore()
	{
		return lowScore;
	}

	/**
	 * @param i
	 */
	public void setLowScore(int i)
	{
		lowScore = i;
	}

	/**
	 * @return
	 */
	public int getHighScore()
	{
		return highScore;
	}

	/**
	 * @param i
	 */
	public void setHighScore(int i)
	{
		highScore = i;
	}

	/**
	 * @return
	 */
	public String getSysAutoRsltCode()
	{
		return sysAutoRsltCode;
	}

	/**
	 * @param string
	 */
	public void setSysAutoRsltCode(String string)
	{
		sysAutoRsltCode = string;
	}
    /**
     * @return Returns the transPrcsCode.
     */
    public String getTranRvwPrcsCode() {
        return tranRvwPrcsCode;
    }

    /**
     * @param transPrcsCode The transPrcsCode to set.
     */
    public void setTranRvwPrcsCode(String tpc) {
        this.tranRvwPrcsCode = tpc;
    }
    /**
     * @return Returns the retrievalRequestCode.
     */
    public String getRetrievalRequestCode() {
        return retrievalRequestCode;
    }
    /**
     * @param retrievalRequestCode The retrievalRequestCode to set.
     */
    public void setRetrievalRequestCode(String rrc) {
        this.retrievalRequestCode = rrc;
    }

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}

	public void setBsnsCode(int bsnsCode) {
		this.bsnsCode = bsnsCode;
	}

	public int getBsnsCode() {
		return bsnsCode;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdType() {
		return idType;
	}

	public void setFullIdNumber(String fullIdNumber) {
		this.fullIdNumber = fullIdNumber;
	}

	public String getFullIdNumber() {
		return fullIdNumber;
	}

	public void setLast4DigitsId(String last4DigitsId) {
		this.last4DigitsId = last4DigitsId;
	}

	public String getLast4DigitsId() {
		return last4DigitsId;
	}

	public String getAdditionalIdMasked() {
		return additionalIdMasked;
	}

	public void setAdditionalIdMasked(String additionalIdMasked) {
		this.additionalIdMasked = additionalIdMasked;
	}

	public String getAdditionalIdEncrypted() {
		return additionalIdEncrypted;
	}

	public void setAdditionalIdEncrypted(String additionalIdEncrypted) {
		this.additionalIdEncrypted = additionalIdEncrypted;
	}

	public String getAdditionalIdType() {
		return additionalIdType;
	}

	public void setAdditionalIdType(String additionalIdType) {
		this.additionalIdType = additionalIdType;
	}

	public String getDocStatus() {
		return docStatus;
	}

	public void setDocStatus(String docStatus) {
		this.docStatus = docStatus;
	}
	
	//added by Ankit Bhatt for MBO-128
	public String getProfileType() {
		return profileType;
	}

	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}
	//ended
}
