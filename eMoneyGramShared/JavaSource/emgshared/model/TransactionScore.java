package emgshared.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class TransactionScore implements Serializable
{
	private int emgTranId;
	private int scoreCnfgId;
	private int scoreCnfgVerNbr;
	private String scoreCnfgStatCode;
	private int tranScoreNbr;
	private String sysAutoRsltCode;
	private String scoreRvwCode;
	private String tranScoreCmntText;
	private String createDate;
	private String createUserid;
	private String lastUpdateDate;
	private String lastUpdateUserid;

	private int totalWeight;
	private List tranScoreCatList;

	public int getEmgTranId()
	{
		return emgTranId;
	}

	public void setEmgTranId(int i)
	{
		emgTranId = i;
	}

	public int getScoreCnfgId()
	{
		return scoreCnfgId;
	}

	public void setScoreCnfgId(int i)
	{
		scoreCnfgId = i;
	}

	public int getScoreCnfgVerNbr()
	{
		return scoreCnfgVerNbr;
	}

	public void setScoreCnfgVerNbr(int i)
	{
		scoreCnfgVerNbr = i;
	}

	public String getScoreCnfgStatCode()
	{
		return scoreCnfgStatCode;
	}

	public void setScoreCnfgStatCode(String string)
	{
		scoreCnfgStatCode = string;
	}

	public int getTranScoreNbr()
	{
		return tranScoreNbr;
	}

	public void setTranScoreNbr(int i)
	{
		tranScoreNbr = i;
	}

	public String getSysAutoRsltCode()
	{
		return sysAutoRsltCode;
	}

	public void setSysAutoRsltCode(String string)
	{
		sysAutoRsltCode = string;
	}

	public String getScoreRvwCode()
	{
		return scoreRvwCode;
	}

	public void setScoreRvwCode(String string)
	{
		scoreRvwCode = string;
	}

	public String getTranScoreCmntText()
	{
		return tranScoreCmntText;
	}

	public void setTranScoreCmntText(String string)
	{
		tranScoreCmntText = string;
	}

	public String getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(String date)
	{
		createDate = date;
	}

	public String getCreateUserid()
	{
		return createUserid;
	}

	public void setCreateUserid(String string)
	{
		createUserid = string;
	}

	public String getLastUpdateDate()
	{
		return lastUpdateDate;
	}

	public void setLastUpdateDate(String date)
	{
		lastUpdateDate = date;
	}

	public String getLastUpdateUserid()
	{
		return lastUpdateUserid;
	}

	public void setLastUpdateUserid(String string)
	{
		lastUpdateUserid = string;
	}

	public List getTranScoreCatList()
	{
		return tranScoreCatList;
	}

	public void setTranScoreCatList(List list)
	{
		tranScoreCatList = list;
		if (tranScoreCatList != null)
			Collections.sort(tranScoreCatList);
	}

	public int getTotalWeight()
	{
		return totalWeight;
	}

	public void setTotalWeight(int i)
	{
		totalWeight = i;
	}
}
