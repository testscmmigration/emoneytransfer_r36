package emgshared.model;

import java.util.ArrayList;
import java.util.List;

public class CountryCodeOSLResponse {

	private String status;
	private List<CountryDialingInfo> countryDialingInfo 
	= new ArrayList<CountryDialingInfo>();
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<CountryDialingInfo> getCountryDialingInfo() {
		return countryDialingInfo;
	}
	public void setCountryDialingInfo(
			List<CountryDialingInfo> countryDialingInfo) {
		this.countryDialingInfo = countryDialingInfo;
	}
	
}
