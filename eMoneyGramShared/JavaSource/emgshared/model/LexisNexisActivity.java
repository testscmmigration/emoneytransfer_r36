/*
 * Created on Mar 2, 2005
 *
 */
package emgshared.model;

import java.util.ArrayList;
import java.util.List;

import com.moneygram.mgo.service.consumerValidationV2_1.RiskIndicator;

import emgshared.util.SSNIndicatorEnum;
import emgshared.util.SSNValidIndicatorEnum;

public class LexisNexisActivity extends Activity implements Cloneable {

	private int id;// primary key
	private String uniqueId;
	private int custId;
	private List<RiskIndicator> highRiskIndicator = new ArrayList<RiskIndicator>();;
	private String ssnValidityIndCode;
	private String ssnValidityIndDesc;
	private String ssnIndicator;
	private String comments;
	private String riskIndicator;
	private String responseXML;
	// display fields
	private String ssnDisp;
	private String ssnValidDisp;
	private String activityDateDisp;
    //Field added for relative search
	private String searchType;
	
	
	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getResponseXML() {
		return responseXML;
	}

	public void setResponseXML(String responseXML) {
		this.responseXML = responseXML;
	}

	public String getRiskIndicator() {
		return riskIndicator;
	}

	public void setRiskIndicator(String riskIndicator) {
		this.riskIndicator = riskIndicator;
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getSsnValidityIndCode() {
		return ssnValidityIndCode;
	}

	public void setSsnValidityIndCode(String ssnValidityIndCode) {
		this.ssnValidityIndCode = ssnValidityIndCode;
	}

	public String getSsnValidityIndDesc() {
		return ssnValidityIndDesc;
	}

	public void setSsnValidityIndDesc(String ssnValidityIndDesc) {
		this.ssnValidityIndDesc = ssnValidityIndDesc;
	}

	public String getSsnIndicator() {
		return ssnIndicator;
	}

	public void setSsnIndicator(String ssnIndicator) {
		this.ssnIndicator = ssnIndicator;
	}

	public List<RiskIndicator> getHighRiskIndicator() {
		return highRiskIndicator;
	}

	public void setHighRiskIndicator(List<RiskIndicator> highRiskIndicator) {
		this.highRiskIndicator = highRiskIndicator;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getSsnDisp() {
		return ssnDisp;
	}

	public void setSsnDisp(String ssnDisp) {
		if (null != ssnDisp) {
			this.ssnDisp = SSNIndicatorEnum.getLabelValueById(Integer.valueOf(ssnDisp));
		} else {
			this.ssnDisp = ssnDisp;
		}
	}

	public String getSsnValidDisp() {
		return ssnValidDisp;
	}

	public void setSsnValidDisp(String ssnValidDisp) {
		if (null != ssnValidDisp) {
			this.ssnValidDisp = SSNValidIndicatorEnum
					.getLabelValue(ssnValidDisp);
		} else {
			this.ssnValidDisp = ssnValidDisp;
		}
	}

	public String getActivityDateDisp() {
		return activityDateDisp;
	}

	public void setActivityDateDisp(String activityDateDisp) {
		this.activityDateDisp = activityDateDisp;
	}

}
