package emgshared.model;

import java.io.Serializable;
import java.util.Date;

public class SearchCriteriaDTO implements Serializable {

	//getActivityLog starts
	private String logonId;
	private int custId;
	private boolean searchTypeFLag;
	private String callerLoinId;
	private Date begindDate;
	private Date endDate;
	//getActivityLog ends
	
	private ConsumerProfile consumerProfile;
	private String ipAddress;
	
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	public boolean isSearchTypeFLag() {
		return searchTypeFLag;
	}
	public void setSearchTypeFLag(boolean searchTypeFLag) {
		this.searchTypeFLag = searchTypeFLag;
	}
	public ConsumerProfile getConsumerProfile() {
		return consumerProfile;
	}
	public void setConsumerProfile(ConsumerProfile consumerProfile) {
		this.consumerProfile = consumerProfile;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getCallerLoinId() {
		return callerLoinId;
	}
	public void setCallerLoinId(String callerLoinId) {
		this.callerLoinId = callerLoinId;
	}
	public Date getBegindDate() {
		return begindDate;
	}
	public void setBegindDate(Date begindDate) {
		this.begindDate = begindDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getLogonId() {
		return logonId;
	}
	public void setLogonId(String logonId) {
		this.logonId = logonId;
	}
	
	
}
