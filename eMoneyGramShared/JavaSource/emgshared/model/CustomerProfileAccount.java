package emgshared.model;

public class CustomerProfileAccount{
	private int custAcctId;           
	private int custAcctVerNbr;      
	private int custId;                
	private int addrId;                
	private String acctTypeCode;         
	private String acctTypeDesc;        
	private String acctStatCode;         
	private String acctStatDesc;        
	private String acctSubStatCode;     
	private String acctSubStatDesc;   
	private String acctEncrypNbr;        
	private String acctMaskNbr;          
	private String acctHoldrName;        
	private String acctName;              
	private String bankAbaNbr;      
	private String crcdHashText;
	private String blkdCrcdAcctId;
	private String crcdBinText;
	private String crcdBinHashText;     
	private String crcdExpMthNbr;       
	private String crcdExpYrNbr;        
	private String crcdCvvNbr;           
	private String createDate;            
	private String createUserid;          
	private String acctBlkdCode;         
	private String bankAbaBlkdCode;     
	private String crcdBinBlkdCode;     
	private String custLogonId;          
	private String custStatCode;         
	private String custStatDesc;         
	private String custSubStatCode;     
	private String custSubStatDesc;     
	private String custLastName;         
	private String custMatrnlName;       
	private String custFrstName;         
	private String custMidName;          
	private String custSsnMaskNbr;      
	private String custBrthDate;         
	private String custGuid;             
	private String promoEmailCode;       
	private String ssnBlkdCode;          

	
	/**
	 * @return
	 */
	public String getAcctBlkdCode() {
		return acctBlkdCode;
	}

	/**
	 * @return
	 */
	public String getAcctEncrypNbr() {
		return acctEncrypNbr;
	}

	/**
	 * @return
	 */
	public String getAcctHoldrName() {
		return acctHoldrName;
	}

	/**
	 * @return
	 */
	public String getAcctMaskNbr() {
		return acctMaskNbr;
	}

	/**
	 * @return
	 */
	public String getAcctName() {
		return acctName;
	}

	/**
	 * @return
	 */
	public String getAcctStatCode() {
		return acctStatCode;
	}

	/**
	 * @return
	 */
	public String getAcctStatDesc() {
		return acctStatDesc;
	}

	/**
	 * @return
	 */
	public String getAcctSubStatCode() {
		return acctSubStatCode;
	}

	/**
	 * @return
	 */
	public String getAcctSubStatDesc() {
		return acctSubStatDesc;
	}

	/**
	 * @return
	 */
	public String getAcctTypeCode() {
		return acctTypeCode;
	}

	/**
	 * @return
	 */
	public String getAcctTypeDesc() {
		return acctTypeDesc;
	}

	/**
	 * @return
	 */
	public int getAddrId() {
		return addrId;
	}

	/**
	 * @return
	 */
	public String getBankAbaBlkdCode() {
		return bankAbaBlkdCode;
	}

	/**
	 * @return
	 */
	public String getBankAbaNbr() {
		return bankAbaNbr;
	}

	/**
	 * @return
	 */
	public String getCrcdBinBlkdCode() {
		return crcdBinBlkdCode;
	}

	/**
	 * @return
	 */
	public String getCrcdBinHashText() {
		return crcdBinHashText;
	}
	
	public String getBlkCrcdAcctId() {
		return blkdCrcdAcctId;
	}

	/**
	 * @return
	 */
	public String getCrcdCvvNbr() {
		return crcdCvvNbr;
	}

	/**
	 * @return
	 */
	public String getCrcdExpMthNbr() {
		return crcdExpMthNbr;
	}

	/**
	 * @return
	 */
	public String getCrcdExpYrNbr() {
		return crcdExpYrNbr;
	}

	/**
	 * @return
	 */
	public String getCreateDate() {
		return createDate;
	}

	/**
	 * @return
	 */
	public String getCreateUserid() {
		return createUserid;
	}

	/**
	 * @return
	 */
	public int getCustAcctId() {
		return custAcctId;
	}

	/**
	 * @return
	 */
	public int getCustAcctVerNbr() {
		return custAcctVerNbr;
	}

	/**
	 * @return
	 */
	public String getCustBrthDate() {
		return custBrthDate;
	}

	/**
	 * @return
	 */
	public String getCustFrstName() {
		return custFrstName;
	}

	/**
	 * @return
	 */
	public String getCustGuid() {
		return custGuid;
	}

	/**
	 * @return
	 */
	public int getCustId() {
		return custId;
	}

	/**
	 * @return
	 */
	public String getCustLastName() {
		return custLastName;
	}

	/**
	 * @return
	 */
	public String getCustLogonId() {
		return custLogonId;
	}

	/**
	 * @return
	 */
	public String getCustMatrnlName() {
		return custMatrnlName;
	}

	/**
	 * @return
	 */
	public String getCustMidName() {
		return custMidName;
	}

	/**
	 * @return
	 */
	public String getCustSsnMaskNbr() {
		return custSsnMaskNbr;
	}

	/**
	 * @return
	 */
	public String getCustStatCode() {
		return custStatCode;
	}

	/**
	 * @return
	 */
	public String getCustStatDesc() {
		return custStatDesc;
	}

	/**
	 * @return
	 */
	public String getCustSubStatCode() {
		return custSubStatCode;
	}

	/**
	 * @return
	 */
	public String getCustSubStatDesc() {
		return custSubStatDesc;
	}

	/**
	 * @return
	 */
	public String getPromoEmailCode() {
		return promoEmailCode;
	}

	/**
	 * @return
	 */
	public String getSsnBlkdCode() {
		return ssnBlkdCode;
	}

	/**
	 * @param string
	 */
	public void setAcctBlkdCode(String string) {
		acctBlkdCode = string;
	}

	/**
	 * @param string
	 */
	public void setAcctEncrypNbr(String string) {
		acctEncrypNbr = string;
	}

	/**
	 * @param string
	 */
	public void setAcctHoldrName(String string) {
		acctHoldrName = string;
	}

	/**
	 * @param string
	 */
	public void setAcctMaskNbr(String string) {
		acctMaskNbr = string;
	}

	/**
	 * @param string
	 */
	public void setAcctName(String string) {
		acctName = string;
	}

	/**
	 * @param string
	 */
	public void setAcctStatCode(String string) {
		acctStatCode = string;
	}

	/**
	 * @param string
	 */
	public void setAcctStatDesc(String string) {
		acctStatDesc = string;
	}

	/**
	 * @param string
	 */
	public void setAcctSubStatCode(String string) {
		acctSubStatCode = string;
	}

	/**
	 * @param string
	 */
	public void setAcctSubStatDesc(String string) {
		acctSubStatDesc = string;
	}

	/**
	 * @param string
	 */
	public void setAcctTypeCode(String string) {
		acctTypeCode = string;
	}

	/**
	 * @param string
	 */
	public void setAcctTypeDesc(String string) {
		acctTypeDesc = string;
	}

	/**
	 * @param i
	 */
	public void setAddrId(int i) {
		addrId = i;
	}

	/**
	 * @param string
	 */
	public void setBankAbaBlkdCode(String string) {
		bankAbaBlkdCode = string;
	}

	/**
	 * @param string
	 */
	public void setBankAbaNbr(String string) {
		bankAbaNbr = string;
	}

	/**
	 * @param string
	 */
	public void setCrcdBinBlkdCode(String string) {
		crcdBinBlkdCode = string;
	}

	/**
	 * @param string
	 */
	public void setCrcdBinHashText(String string) {
		crcdBinHashText = string;
	}
	
	/**
	 * @param string
	 */
	public void setBlkCrcdAcctId(String string) {
		blkdCrcdAcctId = string;
	}

	/**
	 * @param string
	 */
	public void setCrcdCvvNbr(String string) {
		crcdCvvNbr = string;
	}

	/**
	 * @param string
	 */
	public void setCrcdExpMthNbr(String string) {
		crcdExpMthNbr = string;
	}

	/**
	 * @param string
	 */
	public void setCrcdExpYrNbr(String string) {
		crcdExpYrNbr = string;
	}

	/**
	 * @param string
	 */
	public void setCreateDate(String string) {
		createDate = string;
	}

	/**
	 * @param string
	 */
	public void setCreateUserid(String string) {
		createUserid = string;
	}

	/**
	 * @param i
	 */
	public void setCustAcctId(int i) {
		custAcctId = i;
	}

	/**
	 * @param i
	 */
	public void setCustAcctVerNbr(int i) {
		custAcctVerNbr = i;
	}

	/**
	 * @param string
	 */
	public void setCustBrthDate(String string) {
		custBrthDate = string;
	}

	/**
	 * @param string
	 */
	public void setCustFrstName(String string) {
		custFrstName = string;
	}

	/**
	 * @param string
	 */
	public void setCustGuid(String string) {
		custGuid = string;
	}

	/**
	 * @param i
	 */
	public void setCustId(int i) {
		custId = i;
	}

	/**
	 * @param string
	 */
	public void setCustLastName(String string) {
		custLastName = string;
	}

	/**
	 * @param string
	 */
	public void setCustLogonId(String string) {
		custLogonId = string;
	}

	/**
	 * @param string
	 */
	public void setCustMatrnlName(String string) {
		custMatrnlName = string;
	}

	/**
	 * @param string
	 */
	public void setCustMidName(String string) {
		custMidName = string;
	}

	/**
	 * @param string
	 */
	public void setCustSsnMaskNbr(String string) {
		custSsnMaskNbr = string;
	}

	/**
	 * @param string
	 */
	public void setCustStatCode(String string) {
		custStatCode = string;
	}

	/**
	 * @param string
	 */
	public void setCustStatDesc(String string) {
		custStatDesc = string;
	}

	/**
	 * @param string
	 */
	public void setCustSubStatCode(String string) {
		custSubStatCode = string;
	}

	/**
	 * @param string
	 */
	public void setCustSubStatDesc(String string) {
		custSubStatDesc = string;
	}

	/**
	 * @param string
	 */
	public void setPromoEmailCode(String string) {
		promoEmailCode = string;
	}

	/**
	 * @param string
	 */
	public void setSsnBlkdCode(String string) {
		ssnBlkdCode = string;
	}

	public String getCrcdHashText() {
		return crcdHashText;
	}

	public void setCrcdHashText(String crcdHashText) {
		this.crcdHashText = crcdHashText;
	}

	public String getCrcdBinText() {
		return crcdBinText;
	}

	public void setCrcdBinText(String crcdBinText) {
		this.crcdBinText = crcdBinText;
	}

}
