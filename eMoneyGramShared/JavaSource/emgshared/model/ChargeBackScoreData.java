package emgshared.model;


import java.math.*;

public class ChargeBackScoreData {

	private int tranId;
	private long cbScoreA;
	private float cbScoreAVersion;
	private long cbScoreB;
	private float cbScoreBVersion;
	
	public int getTranId()
	{
		return tranId;
	}

	public void setTranId(int i)
	{
		tranId = i;
	}

	public long getCbScoreA() {
		return cbScoreA;
	}

	public void setCbScoreA(long cbScoreA) {
		this.cbScoreA = cbScoreA;
	}

	public float getCbScoreAVersion() {
		return cbScoreAVersion;
	}

	public void setCbScoreAVersion(float cbScoreAVersion) {
		this.cbScoreAVersion = cbScoreAVersion;
	}

	public long getCbScoreB() {
		return cbScoreB;
	}

	public void setCbScoreB(long cbScoreB) {
		this.cbScoreB = cbScoreB;
	}

	public float getCbScoreBVersion() {
		return cbScoreBVersion;
	}

	public void setCbScoreBVersion(float cbScoreBVersion) {
		this.cbScoreBVersion = cbScoreBVersion;
	}
	

}
