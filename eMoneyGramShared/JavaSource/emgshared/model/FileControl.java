package emgshared.model;

import java.util.Date;

public class FileControl{

	private int file_cntl_seq_nbr;
	private Date receive_date;
	private int rec_cnt;
	private int rec_amt;
	private int proc_cnt_1;
	private int proc_cnt_2;
	private int proc_cnt_3;
	private int proc_cnt_4;
	private int proc_amt_1;
	private int proc_amt_2;
	private int proc_amt_3;
	private int proc_amt_4;
	private String process_name;
	private Date process_date;
	private String insrt_event;
	private Date insrt_date;
	private String updt_event;
	private Date updt_date;

	/**
	 * @return Returns the file_cntl_seq_nbr.
	 */
	public int getFile_cntl_seq_nbr() {
		return file_cntl_seq_nbr;
	}
	/**
	 * @param file_cntl_seq_nbr The file_cntl_seq_nbr to set.
	 */
	public void setFile_cntl_seq_nbr(int file_cntl_seq_nbr) {
		this.file_cntl_seq_nbr = file_cntl_seq_nbr;
	}
	/**
	 * @return Returns the insrt_date.
	 */
	public Date getInsrt_date() {
		return insrt_date;
	}
	/**
	 * @param insrt_date The insrt_date to set.
	 */
	public void setInsrt_date(Date insrt_date) {
		this.insrt_date = insrt_date;
	}
	/**
	 * @return Returns the insrt_event.
	 */
	public String getInsrt_event() {
		return insrt_event;
	}
	/**
	 * @param insrt_event The insrt_event to set.
	 */
	public void setInsrt_event(String insrt_event) {
		this.insrt_event = insrt_event;
	}
	/**
	 * @return Returns the proc_amt_1.
	 */
	public int getProc_amt_1() {
		return proc_amt_1;
	}
	/**
	 * @param proc_amt_1 The proc_amt_1 to set.
	 */
	public void setProc_amt_1(int proc_amt_1) {
		this.proc_amt_1 = proc_amt_1;
	}
	/**
	 * @return Returns the proc_amt_2.
	 */
	public int getProc_amt_2() {
		return proc_amt_2;
	}
	/**
	 * @param proc_amt_2 The proc_amt_2 to set.
	 */
	public void setProc_amt_2(int proc_amt_2) {
		this.proc_amt_2 = proc_amt_2;
	}
	/**
	 * @return Returns the proc_amt_3.
	 */
	public int getProc_amt_3() {
		return proc_amt_3;
	}
	/**
	 * @param proc_amt_3 The proc_amt_3 to set.
	 */
	public void setProc_amt_3(int proc_amt_3) {
		this.proc_amt_3 = proc_amt_3;
	}
	/**
	 * @return Returns the proc_amt_4.
	 */
	public int getProc_amt_4() {
		return proc_amt_4;
	}
	/**
	 * @param proc_amt_4 The proc_amt_4 to set.
	 */
	public void setProc_amt_4(int proc_amt_4) {
		this.proc_amt_4 = proc_amt_4;
	}
	/**
	 * @return Returns the proc_cnt_1.
	 */
	public int getProc_cnt_1() {
		return proc_cnt_1;
	}
	/**
	 * @param proc_cnt_1 The proc_cnt_1 to set.
	 */
	public void setProc_cnt_1(int proc_cnt_1) {
		this.proc_cnt_1 = proc_cnt_1;
	}
	/**
	 * @return Returns the proc_cnt_2.
	 */
	public int getProc_cnt_2() {
		return proc_cnt_2;
	}
	/**
	 * @param proc_cnt_2 The proc_cnt_2 to set.
	 */
	public void setProc_cnt_2(int proc_cnt_2) {
		this.proc_cnt_2 = proc_cnt_2;
	}
	/**
	 * @return Returns the proc_cnt_3.
	 */
	public int getProc_cnt_3() {
		return proc_cnt_3;
	}
	/**
	 * @param proc_cnt_3 The proc_cnt_3 to set.
	 */
	public void setProc_cnt_3(int proc_cnt_3) {
		this.proc_cnt_3 = proc_cnt_3;
	}
	/**
	 * @return Returns the proc_cnt_4.
	 */
	public int getProc_cnt_4() {
		return proc_cnt_4;
	}
	/**
	 * @param proc_cnt_4 The proc_cnt_4 to set.
	 */
	public void setProc_cnt_4(int proc_cnt_4) {
		this.proc_cnt_4 = proc_cnt_4;
	}
	/**
	 * @return Returns the process_date.
	 */
	public Date getProcess_date() {
		return process_date;
	}
	/**
	 * @param process_date The process_date to set.
	 */
	public void setProcess_date(Date process_date) {
		this.process_date = process_date;
	}
	/**
	 * @return Returns the process_name.
	 */
	public String getProcess_name() {
		return process_name;
	}
	/**
	 * @param process_name The process_name to set.
	 */
	public void setProcess_name(String process_name) {
		this.process_name = process_name;
	}
	/**
	 * @return Returns the rec_amt.
	 */
	public int getRec_amt() {
		return rec_amt;
	}
	/**
	 * @param rec_amt The rec_amt to set.
	 */
	public void setRec_amt(int rec_amt) {
		this.rec_amt = rec_amt;
	}
	/**
	 * @return Returns the rec_cnt.
	 */
	public int getRec_cnt() {
		return rec_cnt;
	}
	/**
	 * @param rec_cnt The rec_cnt to set.
	 */
	public void setRec_cnt(int rec_cnt) {
		this.rec_cnt = rec_cnt;
	}
	/**
	 * @return Returns the receive_date.
	 */
	public Date getReceive_date() {
		return receive_date;
	}
	/**
	 * @param receive_date The receive_date to set.
	 */
	public void setReceive_date(Date receive_date) {
		this.receive_date = receive_date;
	}
	/**
	 * @return Returns the updt_date.
	 */
	public Date getUpdt_date() {
		return updt_date;
	}
	/**
	 * @param updt_date The updt_date to set.
	 */
	public void setUpdt_date(Date updt_date) {
		this.updt_date = updt_date;
	}
	/**
	 * @return Returns the updt_event.
	 */
	public String getUpdt_event() {
		return updt_event;
	}
	/**
	 * @param updt_event The updt_event to set.
	 */
	public void setUpdt_event(String updt_event) {
		this.updt_event = updt_event;
	}
}
