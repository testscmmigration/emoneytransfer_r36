package emgshared.model;

import java.util.Date;

/**
 * @author A119
 *
 *  Gets data from the Country Master table.  This data can be used anyplace 
 *  where Country data is needed.   Country Master is replicated to emt from our
 *  core transaction databases.
 */
public class CountryMasterBean {

	private String ISOCountryCode;
	private Integer ISOCountryNumber;
	private String ISOCountryAbbrCode;
	private String mainFrameCountryCode;
	private String countryName;
	private Date activeBeginDate;
	private Date activeThruDate;
	private Integer countryGeoCodeId;
	private String lclCurrencyCode;
	

	/**
	 * @return
	 */
	public Date getActiveBeginDate() {
		return activeBeginDate;
	}

	/**
	 * @return
	 */
	public Date getActiveThruDate() {
		return activeThruDate;
	}

	/**
	 * @return
	 */
	public Integer getCountryGeoCodeId() {
		return countryGeoCodeId;
	}

	/**
	 * @return
	 */
	public String getCountryName() {
		return countryName;
	}

	/**
	 * @return
	 */
	public String getISOCountryAbbrCode() {
		return ISOCountryAbbrCode;
	}

	/**
	 * @return
	 */
	public String getISOCountryCode() {
		return ISOCountryCode;
	}

	/**
	 * @return
	 */
	public Integer getISOCountryNumber() {
		return ISOCountryNumber;
	}

	/**
	 * @return
	 */
	public String getLclCurrencyCode() {
		return lclCurrencyCode;
	}

	/**
	 * @return
	 */
	public String getMainFrameCountryCode() {
		return mainFrameCountryCode;
	}

	/**
	 * @param date
	 */
	public void setActiveBeginDate(Date date) {
		activeBeginDate = date;
	}

	/**
	 * @param date
	 */
	public void setActiveThruDate(Date date) {
		activeThruDate = date;
	}

	/**
	 * @param integer
	 */
	public void setCountryGeoCodeId(Integer integer) {
		countryGeoCodeId = integer;
	}

	/**
	 * @param string
	 */
	public void setCountryName(String string) {
		countryName = string;
	}

	/**
	 * @param string
	 */
	public void setISOCountryAbbrCode(String string) {
		ISOCountryAbbrCode = string;
	}

	/**
	 * @param string
	 */
	public void setISOCountryCode(String string) {
		ISOCountryCode = string;
	}

	/**
	 * @param integer
	 */
	public void setISOCountryNumber(Integer integer) {
		ISOCountryNumber = integer;
	}

	/**
	 * @param string
	 */
	public void setLclCurrencyCode(String string) {
		lclCurrencyCode = string;
	}

	/**
	 * @param string
	 */
	public void setMainFrameCountryCode(String string) {
		mainFrameCountryCode = string;
	}

}
