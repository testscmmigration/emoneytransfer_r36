package emgshared.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class UserMessages
{
	private String userId;
	private List msgList = new ArrayList();

	public String getUserId()
	{
		return userId;
	}
	
	public void setUserId(String s)
	{
		userId = s;
	}
	
	public void addMsg(int msgId, Date date)
	{
		msgList.add(new MessageInfo(msgId, date));
	}
	
	public int getTotalCount()
	{
		return msgList.size();
	}

	public int getUnreadCount()
	{
		int cnt = 0;
		Iterator iter = msgList.iterator();
		while (iter.hasNext())
		{
			MessageInfo mi = (MessageInfo) iter.next();
			if (mi.getMsgRcvDate() == null)
			{
				cnt++;
			}
		}
		return cnt;
	}

	public void setMsgRead(int msgId)
	{
		Iterator iter = msgList.iterator();
		while (iter.hasNext())
		{
			MessageInfo mi = (MessageInfo) iter.next();
			if (mi.getAdminMsgId() == msgId)
			{
				mi.setMsgRcvDate();
				break;
			}
		}
	}

	public int[] getMsgIds()
	{
		int[] idList = new int[msgList.size()];
		int i = 0;
		Iterator iter = msgList.iterator();
		while (iter.hasNext())
		{
			MessageInfo mi = (MessageInfo) iter.next();
			idList[i++] = mi.getAdminMsgId();
		}
		return idList;
	}

	public int[] getUnreadMsgIds()
	{
		int[] idList = new int[getUnreadCount()];
		int i = 0;
		Iterator iter = msgList.iterator();
		while (iter.hasNext())
		{
			MessageInfo mi = (MessageInfo) iter.next();
			if (mi == null)
			{
				idList[i++] = mi.getAdminMsgId();
			}
		}
		return idList;
	}

	private class MessageInfo
	{
		private int adminMsgId;
		private Date msgRcvDate;

		public MessageInfo(int msgId, Date date)
		{
			this.adminMsgId = msgId;
			this.msgRcvDate = date;
		}

		public int getAdminMsgId()
		{
			return adminMsgId;
		}

		public Date getMsgRcvDate()
		{
			return msgRcvDate;
		}

		public void setMsgRcvDate()
		{
			msgRcvDate = new Date();
		}
	}
}
