/**
 * 
 */
package emgshared.model;

/**
 * @author aip8
 *
 */
public class Error {
	
	private int errorCode;
	
	private String errorMessage;
	
	private String detailedMessage;
	
	private String errorProperty;
	
	private String moreInfo;

	/**
	 * @return the errorCode
	 */
	public int getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCOde(int errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the detailedMessage
	 */
	public String getDetailedMessage() {
		return detailedMessage;
	}

	/**
	 * @param detailedMessage the detailedMessage to set
	 */
	public void setDetailedMessage(String detailedMessage) {
		this.detailedMessage = detailedMessage;
	}

	/**
	 * @return the errorProperty
	 */
	public String getErrorProperty() {
		return errorProperty;
	}

	/**
	 * @param errorProperty the errorProperty to set
	 */
	public void setErrorProperty(String errorProperty) {
		this.errorProperty = errorProperty;
	}

	/**
	 * @return the moreInfo
	 */
	public String getMoreInfo() {
		return moreInfo;
	}

	/**
	 * @param moreInfo the moreInfo to set
	 */
	public void setMoreInfo(String moreInfo) {
		this.moreInfo = moreInfo;
	}

}
