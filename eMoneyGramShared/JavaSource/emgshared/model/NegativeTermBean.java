/*
 * Created on Aug 10, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgshared.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author A136
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class NegativeTermBean implements Comparable {
	
	public static final String SORT_BY_PURPOSE_TYPE = "purposeTypeCode";
	public static final String SORT_BY_NEGATIVE_STRING = "negativeString";
	public static final String SORT_BY_ID_NUMBER = "idNumber";
	public static String sortBy = "purposeTypeCode";

	private String idNumber;
	private String purposeTypeCode;
	private String purposeCodeDesc;
	private String negativeString;
	private String comment;
	private String createDate;
	private String createUserId;

	public int compareTo(Object o) {
		NegativeTermBean other = (NegativeTermBean) o;
		
		if (sortBy.equals(SORT_BY_NEGATIVE_STRING)) {
			return compareStrings(negativeString, other.negativeString);
		} else if (sortBy.equals(SORT_BY_PURPOSE_TYPE)) {
			int returnCode = compareStrings(purposeTypeCode, other.purposeTypeCode);
			if (returnCode == 0)
				return compareStrings(negativeString, other.negativeString);
			return returnCode;
		} else if (sortBy.equals(SORT_BY_ID_NUMBER)) {
			return compareStrings(idNumber, other.idNumber);
		} 		
		return 0;
	}

	private int compareStrings(String str1, String str2)
	{
		if (str1 == null && str2 == null)
		{
			return 0;
		}
		
		if (str1 == null)
		{
			return -1;	
		}
		
		if (str2 == null)
		{
			return 1;
		}
		
		return str1.compareTo(str2);
	}
	/**
	 * @return Returns the comment.
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * @param comment The comment to set.
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	/**
	 * @return Returns the createDate.
	 */
	public String getCreateDate() {
		return getFormattedCreateDate(createDate);
	}
	/**
	 * @param createDate The createDate to set.
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	/**
	 * @return Returns the createUserId.
	 */
	public String getCreateUserId() {
		return createUserId;
	}
	/**
	 * @param createUserId The createUserId to set.
	 */
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	/**
	 * @return Returns the idNumber.
	 */
	public String getIdNumber() {
		return idNumber;
	}
	/**
	 * @param idNumber The idNumber to set.
	 */
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	/**
	 * @return Returns the negativeString.
	 */
	public String getNegativeString() {
		return negativeString;
	}
	/**
	 * @param negativeString The negativeString to set.
	 */
	public void setNegativeString(String negativeString) {
		this.negativeString = negativeString;
	}
	/**
	 * @return Returns the purposeCodeDesc.
	 */
	public String getPurposeCodeDesc() {
		return purposeCodeDesc;
	}
	/**
	 * @param purposeCodeDesc The purposeCodeDesc to set.
	 */
	public void setPurposeCodeDesc(String purposeCodeDesc) {
		this.purposeCodeDesc = purposeCodeDesc;
	}
	/**
	 * @return Returns the purposeTypeCode.
	 */
	public String getPurposeTypeCode() {
		return purposeTypeCode;
	}
	/**
	 * @param purposeTypeCode The purposeTypeCode to set.
	 */
	public void setPurposeTypeCode(String purposeTypeCode) {
		this.purposeTypeCode = purposeTypeCode;
	}

	public String getFormattedCreateDate(String date) {
		return format(date);
	}

	private String format(String date) {
		try {
			DateFormat fromFormat = new SimpleDateFormat("dd/mm/yyyy hh:mm a");
			DateFormat toFormat = new SimpleDateFormat("dd/MMM/yyyy hh:mm a");		
			Date d = fromFormat.parse(date);
			return toFormat.format(d);
		} catch (ParseException e) {
			return date;
		} catch(RuntimeException e) {
			throw e;
		}
	}
}
