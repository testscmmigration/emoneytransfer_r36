package emgshared.model;

public class ResidentStatusCode {
	private int resdStatCode;
	private String resdStatDesc;

	public ResidentStatusCode(int code, String desc) {
		this.resdStatCode = code;
		this.resdStatDesc = desc;
	}

	public void setResdStatCode(int resdStatCode) {
		this.resdStatCode = resdStatCode;
	}

	public int getResdStatCode() {
		return resdStatCode;
	}

	public void setResdStatDesc(String resdStatDesc) {
		this.resdStatDesc = resdStatDesc;
	}

	public String getResdStatDesc() {
		return resdStatDesc;
	}

	public String getLabel() {
		return resdStatDesc;
	}

	public String getValue() {
		return resdStatDesc;
	}
}
