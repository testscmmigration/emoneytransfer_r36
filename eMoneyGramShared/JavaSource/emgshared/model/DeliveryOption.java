package emgshared.model;

public class DeliveryOption {

	   public static final String WILL_CALL = "WILL_CALL";
	   public static final String BANCOMER = "BANCOMER";
	   public static final String CAMBIO_PLUS = "CAMBIO_PLUS";
	   public static final String HDS_USD = "HDS_USD";
	   public static final String HDS_LOCAL = "HDS_LOCAL";
	   public static final String HOME_DELIVERY = "HOME_DELIVERY";
	   public static final String CARD_DEPOSIT = "CARD_DEPOSIT";
	   public static final String BANK_DEPOSIT = "BANK_DEPOSIT";
	   public static final String RECEIVE_AT = "RECEIVE_AT";
	   
	   public static final int CARD_DEPOSIT_ID=9;
	   public static final int BANK_DEPOSIT_ID=10; 
	   public static final int WILL_CALL_ID=0;
	   public static final int ONLY_AT_ID=15; 
	   
}
