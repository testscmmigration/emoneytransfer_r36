/*
 * Created on Jan 20, 2005
 *
 */
package emgshared.model;

/**
 * @author A131
 *
 */
public class NewConsumerBankAccount
{
	private static final int NEW_ACCOUNT_ID = -1;

	private final ConsumerBankAccount account;
	private final String accountNumber;

	public NewConsumerBankAccount(
		int consumerId,
		ConsumerAccountType type,
		String accountNumber,
		String accountNumberMask,
		ConsumerAddress billingAddress,
		TaintIndicatorType accountNumberTaintCode,
		TaintIndicatorType bankAbaTaintCode,
		TaintIndicatorType creditCardBinTaintCode)
	{
		this.account =
			new ConsumerBankAccount(
				NEW_ACCOUNT_ID,
				consumerId,
				AccountStatus.ACTIVE_VALIDATION_LEVEL_1,
				type,
				null,
				accountNumberMask,
				billingAddress,
				accountNumberTaintCode,
				bankAbaTaintCode,
				creditCardBinTaintCode);
		this.accountNumber = accountNumber;
		this.account.setVersionNumber(1);
	}

	/**
	 * @return
	 * 
	 * Created on Jan 20, 2005
	 */
	public String getABANumber()
	{
		return account.getABANumber();
	}

	/**
	 * @return
	 * 
	 * Created on Jan 20, 2005
	 */
	public String getAccountNumber()
	{
		return this.accountNumber;
	}

	/**
	 * @return
	 * 
	 * Created on Jan 20, 2005
	 */
	public String getAccountNumberMask()
	{
		return account.getAccountNumberMask();
	}

	/**
	 * @return
	 * 
	 * Created on Jan 20, 2005
	 */
	public ConsumerAccountType getAccountType()
	{
		return account.getAccountType();
	}

	/**
	 * @return
	 * 
	 * Created on Jan 20, 2005
	 */
	public String getAddressLine1()
	{
		return account.getAddressLine1();
	}

	/**
	 * @return
	 * 
	 * Created on Jan 20, 2005
	 */
	public String getAddressLine2()
	{
		return account.getAddressLine2();
	}

	/**
	 * @return
	 * 
	 * Created on Jan 20, 2005
	 */
	public String getCity()
	{
		return account.getCity();
	}

	/**
	 * @return
	 * 
	 * Created on Jan 20, 2005
	 */
	public int getConsumerId()
	{
		return account.getConsumerId();
	}

	/**
	 * @return
	 * 
	 * Created on Jan 20, 2005
	 */
	public String getFinancialInstitutionName()
	{
		return account.getFinancialInstitutionName();
	}

	/**
	 * @return
	 * 
	 * Created on Jan 20, 2005
	 */
	public int getId()
	{
		return account.getId();
	}

	/**
	 * @return
	 * 
	 * Created on Jan 20, 2005
	 */
	public String getIsoCountryCode()
	{
		return account.getIsoCountryCode();
	}

	/**
	 * @return
	 * 
	 * Created on Jan 20, 2005
	 */
	public String getName()
	{
		return account.getName();
	}

	/**
	 * @return
	 * 
	 * Created on Jan 20, 2005
	 */
	public String getPostalCode()
	{
		return account.getPostalCode();
	}

	/**
	 * @return
	 * 
	 * Created on Jan 20, 2005
	 */
	public String getPostalCodePlusZip4()
	{
		return account.getPostalCodePlusZip4();
	}

	/**
	 * @return
	 * 
	 * Created on Jan 20, 2005
	 */
	public String getState()
	{
		return account.getState();
	}

	/**
	 * @return
	 * 
	 * Created on Jan 20, 2005
	 */
	public String getStatusCode()
	{
		return account.getStatusCode();
	}

	/**
	 * @return
	 * 
	 * Created on Jan 20, 2005
	 */
	public String getSubStatusCode()
	{
		return account.getSubStatusCode();
	}

	/**
	 * @return
	 * 
	 * Created on Jan 20, 2005
	 */
	public int getVersionNumber()
	{
		return account.getVersionNumber();
	}

	/**
	 * @return
	 * 
	 * Created on Jan 20, 2005
	 */
	public String getZip4()
	{
		return account.getZip4();
	}

	/**
	 * @param string
	 * 
	 * Created on Jan 20, 2005
	 */
	public void setABANumber(String string)
	{
		account.setABANumber(string);
	}

	/**
	 * @param string
	 * 
	 * Created on Jan 20, 2005
	 */
	public void setAddressLine1(String string)
	{
		account.setAddressLine1(string);
	}

	/**
	 * @param string
	 * 
	 * Created on Jan 20, 2005
	 */
	public void setAddressLine2(String string)
	{
		account.setAddressLine2(string);
	}

	/**
	 * @param string
	 * 
	 * Created on Jan 20, 2005
	 */
	public void setAddressLine3(String string)
	{
		account.setAddressLine3(string);
	}

	/**
	 * @param string
	 * 
	 * Created on Jan 20, 2005
	 */
	public void setCity(String string)
	{
		account.setCity(string);
	}

	/**
	 * @param string
	 * 
	 * Created on Jan 20, 2005
	 */
	public void setFinancialInstitutionName(String string)
	{
		account.setFinancialInstitutionName(string);
	}

	/**
	 * @param string
	 * 
	 * Created on Jan 20, 2005
	 */
	public void setIsoCountryCode(String string)
	{
		account.setIsoCountryCode(string);
	}

	/**
	 * @param string
	 * 
	 * Created on Jan 20, 2005
	 */
	public void setName(String string)
	{
		account.setName(string);
	}

	/**
	 * @param string
	 * 
	 * Created on Jan 20, 2005
	 */
	public void setPostalCode(String string)
	{
		account.setPostalCode(string);
	}

	/**
	 * @param string
	 * 
	 * Created on Jan 20, 2005
	 */
	public void setState(String string)
	{
		account.setState(string);
	}

	/**
	 * @param string
	 * 
	 * Created on Jan 20, 2005
	 */
	public void setZip4(String string)
	{
		account.setZip4(string);
	}

	/**
	 * @return
	 * 
	 * Created on Jan 26, 2005
	 */
	public int getAddressId()
	{
		return account.getAddressId();
	}

	/**
		 * @return
		 */
	public TaintIndicatorType getAccountNumberTaintCode()
	{
		return account.getAccountNumberTaintCode();
	}

	/**
	 * @return
	 */
	public TaintIndicatorType getCreditCardBinTaintCode()
	{
		return account.getCreditCardBinTaintCode();
	}

	/**
	 * @param type
	 */
	public void setAccountNumberTaintCode(TaintIndicatorType type)
	{
		account.setAccountNumberTaintCode(type);
	}

	/**
	 * @param type
	 */
	public void setCreditCardBinTaintCode(TaintIndicatorType type)
	{
		account.setCreditCardBinTaintCode(type);
	}
	/**
	 * @return
	 */
	public TaintIndicatorType getBankAbaTaintCode()
	{
		return account.getBankAbaTaintCode();
	}

	/**
	 * @param type
	 */
	public void setBankAbaTaintCode(TaintIndicatorType type)
	{
		account.setBankAbaTaintCode(type);
	}

}
