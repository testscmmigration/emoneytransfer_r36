/*
 * Created on May 27, 2005
 *
 */
package emgshared.model;

import java.io.Serializable;

import emgshared.util.I18NHelper;

/**
 * @author T349
 *
 */
public class TaintIndicatorType implements Serializable
{

	private static final String BLOCKED_INDICATOR = "B";
	private static final String NOT_BLOCKED_INDICATOR = "N";
	private static final String OVERRIDE_INDICATOR = "O";

	public static final String BLOCKED_TEXT = "Blocked";
	public static final String NOT_BLOCKED_TEXT = "Not Blocked";
	public static final String OVERRIDE_TEXT = "Overridden";

	public static final TaintIndicatorType BLOCKED =
		new TaintIndicatorType(BLOCKED_INDICATOR);
	public static final TaintIndicatorType NOT_BLOCKED =
		new TaintIndicatorType(NOT_BLOCKED_INDICATOR);
	public static final TaintIndicatorType OVERRIDE =
		new TaintIndicatorType(OVERRIDE_INDICATOR);

	private final String statusIndicator;

	public TaintIndicatorType(String indicator)
	{
		this.statusIndicator = indicator;
	}

	public static TaintIndicatorType getInstance(String statusIndicator)
	{
		TaintIndicatorType indicator;
		if (BLOCKED_INDICATOR.equalsIgnoreCase(statusIndicator))
		{
			indicator = BLOCKED;
		} else if (NOT_BLOCKED_INDICATOR.equalsIgnoreCase(statusIndicator) ||
			statusIndicator == null)
		{
			indicator = NOT_BLOCKED;
		} else if (OVERRIDE_INDICATOR.equalsIgnoreCase(statusIndicator))
		{
			indicator = OVERRIDE;
		} else
		{
			String errorMessage =
				I18NHelper.getFormattedMessage(
					"exception.invalid.param.value",
					new Object[] { statusIndicator, "code" });
			throw new IllegalArgumentException(errorMessage);
		}
		return (indicator);
	}

	public String getStatusIndicator()
	{
		return statusIndicator;
	}

	public boolean isBlocked()
	{
		return BLOCKED_INDICATOR.equalsIgnoreCase(this.statusIndicator);
	}
	public boolean isNotBlocked()
	{
		return NOT_BLOCKED_INDICATOR.equalsIgnoreCase(this.statusIndicator);
	}
	public boolean isOverridden()
	{
		return OVERRIDE_INDICATOR.equalsIgnoreCase(this.statusIndicator);
	}

	public String toString()
	{
		return statusIndicator;
	}
}
