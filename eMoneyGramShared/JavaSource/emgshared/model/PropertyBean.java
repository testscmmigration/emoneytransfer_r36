package emgshared.model;


public class PropertyBean implements Comparable
{
	private PropertyKey propKey;
	private String propDesc;
	private String adminUseFlag;
	private String multivalueFlag;
	private String dataTypeCode;
	private String createDate;
	private String createUserid;
	private String lastUpdateDate;
	private String lastUpdateUserid;
	private boolean definedInDb;
	private boolean definedInClass;
	private String msg = "";
	private boolean editable = false;

	public PropertyBean()
	{
	}

	public PropertyBean(
		PropertyKey key,
		String desc,
		String admFlag,
		String multiFlag,
		String dataType,
		String cDate,
		String cUser,
		String updateDate,
		String updateUser,
		boolean inDb,
		boolean inClass,
		boolean editable)
	{
		this.propKey = key;
		this.propDesc = desc;
		this.adminUseFlag = admFlag;
		this.multivalueFlag = multiFlag;
		this.dataTypeCode = dataType;
		this.createDate = cDate;
		this.createUserid = cUser;
		this.lastUpdateDate = updateDate;
		this.lastUpdateUserid = updateUser;
		this.definedInDb = inDb;
		this.definedInClass = inClass;
		this.editable = editable;
	}

	public PropertyBean(PropertyKey key)
	{
		this(
			key,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			false,
			true,
			false);
	}

	public PropertyKey getPropKey()
	{
		return propKey;
	}

	public void setPropName(PropertyKey key)
	{
		propKey = key;
	}

	public String getPropDesc()
	{
		return propDesc == null ? "" : propDesc;
	}

	public void setPropDesc(String string)
	{
		propDesc = string;
	}

	public String getAdminUseFlag()
	{
		return adminUseFlag == null ? "" : adminUseFlag;
	}

	public void setAdminUseFlag(String string)
	{
		adminUseFlag = string;
	}

	public String getMultivalueFlag()
	{
		return multivalueFlag == null ? "" : multivalueFlag;
	}

	public void setMultivalueFlag(String string)
	{
		multivalueFlag = string;
	}

	public String getDataTypeCode()
	{
		return dataTypeCode == null ? "" : dataTypeCode;
	}

	public void setDataTypeCode(String string)
	{
		dataTypeCode = string;
	}

	public String getCreateDate()
	{
		return createDate == null ? "" : createDate;
	}

	public void setCreateDate(String string)
	{
		createDate = string;
	}

	public String getCreateUserid()
	{
		return createUserid == null ? "" : createUserid;
	}

	public void setCreateUserid(String string)
	{
		createUserid = string;
	}

	public String getLastUpdateDate()
	{
		return lastUpdateDate == null ? "" : lastUpdateDate;
	}

	public void setLastUpdateDate(String string)
	{
		lastUpdateDate = string;
	}

	public String getLastUpdateUserid()
	{
		return lastUpdateUserid == null ? "" : lastUpdateUserid;
	}

	public void setLastUpdateUserid(String string)
	{
		lastUpdateUserid = string;
	}

	public boolean isDefinedInDb()
	{
		return definedInDb;
	}

	public void setDefinedInDb(boolean b)
	{
		definedInDb = b;
	}

	public boolean isDefinedInClass()
	{
		return definedInClass;
	}

	public void setDefinedInClass(boolean b)
	{
		definedInClass = b;
	}

	public String getMsg()
	{
		return msg == null ? "" : msg;
	}

	public void setMsg(String string)
	{
		msg = string;
	}

	public boolean isEditable()
	{
		return editable;
	}

	public void setEditable(boolean b)
	{
		editable = b;
	}

	public int compareTo(Object o)
	{
		int comparison = 0;
		PropertyBean c1 = this;
		PropertyBean c2 = (PropertyBean) o;
		if (c1 == null) {
			comparison = (c2 == null ? 0 : 1);
		} else if (c2 == null) {
			comparison = -1;
		} else {
			comparison = c1.getPropKey().getPropType().compareToIgnoreCase(c2.getPropKey().getPropType());
			
			if (comparison == 0) {
				comparison = c1.getPropKey().getPropName().compareTo(c2.getPropKey().getPropName());
			}

			if (comparison == 0) {
				comparison = c1.getPropKey().getPropVal().compareToIgnoreCase(c2.getPropKey().getPropVal());
			}
		}
		return comparison;
	}
}
