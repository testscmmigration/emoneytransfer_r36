package emgshared.model;

/**
 * Enum class for the document types for South Africa to convert the DB string
 * into Front end string
 * <ul>
 * <li>DocumentType- Fetching from the
 * pkg_em_customer_profile.prc_get_cust_upld_doc_cv proc call
 * </ul>
 * 
 * @version 1.0
 * @author wx51
 */
public enum DocumentType {
	Id("ID Document"), ProofOfIncome("Proof of Income"), ProofOfAddress(
    "Proof of Address"), Other("Other");

    private String status;

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    private DocumentType(String status) {
        this.status = status;
    }

    public static String getNameByCode(String code) {
        for (int i = 0; i < DocumentType.values().length; i++) {
            if (code.equals(DocumentType.values()[i].status)){
                return DocumentType.values()[i].name();
            }
        }
        return null;
    }

    public static String getCodeByName(DocumentType code) {
        for (int i = 0; i < DocumentType.values().length; i++) {
            if (code.equals(DocumentType.values()[i].name())){
                return DocumentType.values()[i].status;
            }
        }
        return null;
    }

    public static String getCodeByName(String code) {
        for (int i = 0; i < DocumentType.values().length; i++) {
            if (code.equals(DocumentType.values()[i].name())) {
                return DocumentType.values()[i].status;
            }
        }
        return null;
    }

}
