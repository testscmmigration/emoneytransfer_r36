/*
 * Created on Aug 1, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package emgshared.model;

import java.util.List;

/**
 * @author A119
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class UserActivityType {

    private String userActivityEventCode;
    private String userActivityEventDesc;
    private String tableName;
    private List keyColumns;
	public static final String EVENT_TYPE_TRANSACTIONTAKEOWNERSHIP = "TTO"; 
	public static final String EVENT_TYPE_TRANSACTIONRELEASEOWNERSHIP = "TRO";
	public static final String EVENT_TYPE_CUSTOMERPROFILEACCESS =	"CPA";
	public static final String EVENT_TYPE_BILLERADD = "BAD";
	public static final String EVENT_TYPE_BILLERACTIVATE = "BAC";
	public static final String EVENT_TYPE_BILLERDEACTIVATE = "BDA"; 
	public static final String EVENT_TYPE_BILLERCHANGELIMIT = "BCL"; 
	public static final String EVENT_TYPE_COUNTRYEXCEPTIONSDELETE = "CED"; 
	public static final String EVENT_TYPE_COUNTRYEXCEPTIONSADD = "CEA"; 
	public static final String EVENT_TYPE_COUNTRYEXCEPTIONSCHGALLOWFLAG = "CAF"; 
	public static final String EVENT_TYPE_COUNTRYEXCEPTIONSCHGMAXAMT = "CMA"; 
	public static final String EVENT_TYPE_COUNTRYEXCEPTIONSCHGTHRESHOLDAMT = "CTA"; 
	public static final String EVENT_TYPE_TRANSACTIONSCORINGEDIT = "SED"; 
	public static final String EVENT_TYPE_TRANSACTIONSCORINGADD = "SAD";
	public static final String EVENT_TYPE_TRANSACTIONSCORINGDELETE = "SDE"; 
	public static final String EVENT_TYPE_TRANSACTIONSCORINGDISABLE = "SDI"; 
	public static final String EVENT_TYPE_TRANSACTIONSCORINGACTIVATE = "SAC"; 
	public static final String EVENT_TYPE_AUTOMATIONTURNOFF = "AOF"; 
	public static final String EVENT_TYPE_AUTOMATIONTURNON = "AON";
	public static final String EVENT_TYPE_SCORINGTURNOFF = "SOF"; 
	public static final String EVENT_TYPE_SCORINGTURNON = "SON";
	public static final String EVENT_TYPE_USERADMINADD = "UAD";
	public static final String EVENT_TYPE_USERADMININACTIVATE = "UIN";
	public static final String EVENT_TYPE_USERADMINACTIVATE = "UAC";
	public static final String EVENT_TYPE_USERADMINCHANGEROLE = "UCR";
	public static final String EVENT_TYPE_USERADMINPASSWORDRESET = "UPR";
	public static final String EVENT_TYPE_ROLEMAINTENANCEADD = "RAD";
	public static final String EVENT_TYPE_ROLEMAINTENANCEMODIFY = "RMO";
	public static final String EVENT_TYPE_CONSUMERPROFILEUPDATE = "CUP";
	public static final String EVENT_TYPE_CONSUMERPROFILECHGSSN = "CCS";
	public static final String EVENT_TYPE_CONSUMERPROFILEPASSWORDCHG = "CPC";
	public static final String EVENT_TYPE_CONSUMERPROFILEPREMIERECODECHG = "CPR";
	public static final String EVENT_TYPE_CONSUMERPROFILESTATUSCHG = "CSC";
	public static final String EVENT_TYPE_CONSUMERPROFILEPROMOEMAILCHG = "CPE";
	public static final String EVENT_TYPE_CONSUMERPROFILEADDBANKACCT = "CAB";
	public static final String EVENT_TYPE_CONSUMERPROFILECHGBANKACCTSTATUS = "CCB";
	public static final String EVENT_TYPE_CONSUMERPROFILECCADD = "CAC";
	public static final String EVENT_TYPE_CONSUMERPROFILEEDITCC = "CEC"; 
	public static final String EVENT_TYPE_CONSUMERPROFILECHGCCSTATUS = "CCC"; 
	public static final String EVENT_TYPE_CONSUMERPROFILEMDVALIDATIONSTATUSCHG = "CMS";
	public static final String EVENT_TYPE_CONSUMERPROFILECLEARMASTERTAINT = "CMT";
	public static final String EVENT_TYPE_USERLOGIN = "ULI"; 
	public static final String EVENT_TYPE_USERLOGOFF = "ULO";             
                                         
    /**
     * @return Returns the keyColumns.
     */
    public List getKeyColumns() {
        return keyColumns;
    }
    /**
     * @param keyColumns The keyColumns to set.
     */
    public void setKeyColumns(List kc) {
        this.keyColumns = kc;
    }
    /**
     * @return Returns the tableName.
     */
    public String getTableName() {
        return tableName;
    }
    /**
     * @param tableName The tableName to set.
     */
    public void setTableName(String tn) {
        this.tableName = tn;
    }
    /**
     * @return Returns the userActivityEventCode.
     */
    public String getUserActivityEventCode() {
        return userActivityEventCode;
    }
    /**
     * @param userActivityEventCode The userActivityEventCode to set.
     */
    public void setUserActivityEventCode(String uaec) {
        this.userActivityEventCode = uaec;
    }
    /**
     * @return Returns the userActivityEventDesc.
     */
    public String getUserActivityEventDesc() {
        return userActivityEventDesc;
    }
    /**
     * @param userActivityEventDesc The userActivityEventDesc to set.
     */
    public void setUserActivityEventDesc(String uaed) {
        this.userActivityEventDesc = uaed;
    }
}
