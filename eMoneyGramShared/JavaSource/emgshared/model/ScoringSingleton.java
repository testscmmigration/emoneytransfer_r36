package emgshared.model;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.services.CacheService;
import emgshared.services.ScoringService;
import emgshared.services.ServiceFactory;
import emgshared.services.TransactionService;
import emgshared.util.Constants;

/**
 * @author T004
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

public class ScoringSingleton {

    static private ScoringSingleton _instance;

    private Map tranTypes = new HashMap();

    private Map scoreConfigurations = new HashMap();

    private ServiceFactory sf = ServiceFactory.getInstance();

    private ScoringService ss = sf.getScoringService();

    private TransactionService ts = sf.getTransactionService();

    private CacheService cs = sf.getCacheService();

    private Timestamp cacheTimestamp = null;

    private boolean currentlyLoading = false;

    static public ScoringSingleton getInstance() {
        if (_instance == null) {
            synchronized (ScoringSingleton.class) {
                if (_instance == null)
                    _instance = new ScoringSingleton();
            }
        }
        return _instance;
    }

    private ScoringSingleton() {
        EMGSharedLogger.getLogger(this.getClass().getName().toString()).info(
                "Transaction Scoring Cache : SinngleTon Called");
        ValidateCache("TransactionScore");
    }

    private void Load() {
        EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(
                "Transaction Scoring Cache : Entering Load method of cache");

        if (currentlyLoading) {
            EMGSharedLogger.getLogger(this.getClass().getName().toString())
                    .info(
                            "##########Thread "
                                    + Thread.currentThread().getName()
                                    + " is waiting ##########");
        }

        synchronized (this) {
            EMGSharedLogger.getLogger(this.getClass().getName().toString())
                    .info(
                            "##########Thread "
                                    + Thread.currentThread().getName()
                                    + " is currently Loading ##########");
            currentlyLoading = true;

            try {

                for (int partnerSiteCode : Constants.partnerSiteCodeToId.keySet()) {
                	Collection transactionTypes = ts.getTransactionTypes(partnerSiteCode);
                	for (Iterator it = transactionTypes.iterator(); it.hasNext();) {
                		EMTTransactionType tranType = (EMTTransactionType) it.next();
                		tranTypes.put(tranType.getEmgTranTypeCodeLabel(), tranType);
                	}
                }

                Collection activeConfigurations = ss.getActiveScoreConfigurations(null, null, null);
                for (Iterator it = activeConfigurations.iterator(); it.hasNext();) {
                    ScoreConfiguration sc = (ScoreConfiguration) it.next();
                    EMTTransactionType tt = (EMTTransactionType) tranTypes.get(sc.getTransactionCodeLabel());
                    sc.setAutoAprvFlag(tt.getAutoApproveFlag());
                    sc.setScoreFlag(tt.getScoreFlag());
                    sc.setEmgTranTypeDesc(tt.getEmgTranTypeDesc());
                    scoreConfigurations.put(sc.getTransactionCodeLabel(), sc);
                }

            } catch (Exception e) {
                EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("ERROR : in ScoringSingleton Object in Initial Load method " + e.getMessage(), e);
            }

            EMGSharedLogger.getLogger(this.getClass().getName().toString())
                    .info("##########Thread " + Thread.currentThread().getName() + " finished Loading ##########");
            currentlyLoading = false;
        }

    }

    private void ValidateCache(String cacheName) {
        EMGSharedLogger.getLogger(this.getClass().getName().toString()).info("Transaction Scoring Cache : Validating Cache");

        try {
            SimpleCache cache = cs.getCache(null, "TransactionScore");
            EMGSharedLogger.getLogger(this.getClass().getName().toString())
                    .info("Cache Time : " + cacheTimestamp);
            EMGSharedLogger.getLogger(this.getClass().getName().toString())
                    .info("Database Time : " + cache.getTimeStamp());

            if (cacheTimestamp == null) {
                EMGSharedLogger.getLogger(this.getClass().getName().toString())
                        .info("Transaction Scoring Cache : TimeStamp Null");
                cacheTimestamp = cache.getTimeStamp();
                Load();
            } else {
                if (!cache.getTimeStamp().equals(cacheTimestamp)) {
                    EMGSharedLogger.getLogger(
                            this.getClass().getName().toString()).info(
                            "Transaction Scoring Cache : Cache Dirty");
                    cacheTimestamp = cache.getTimeStamp();
                    Load();
                } else {
                    EMGSharedLogger.getLogger(
                            this.getClass().getName().toString()).info(
                            "Transaction Scoring Cache : Cache up-to-date");
                }

            }
        } catch (Exception e) {
            EMGSharedLogger
                    .getLogger(this.getClass().getName().toString())
                    .error(
                            "ERROR : in ScoringSingleton Object in ValidateCache method");
        }

    }

    public boolean isAllowScoring(EMTTransactionType tranType) {
        EMGSharedLogger.getLogger(this.getClass().getName().toString()).info(
                "Transaction Scoring Cache : isAllowScoring() called");
        ValidateCache("TransactionScore");
        EMTTransactionType tt = (EMTTransactionType) tranTypes.get(tranType
                .getEmgTranTypeCode());

        if (tt.getScoreFlag().equals("Y"))
            return true;
        else
            return false;
    }

    public boolean isAutoApprove(EMTTransactionType tranType) {
        EMGSharedLogger.getLogger(this.getClass().getName().toString()).info(
                "Transaction Scoring Cache : isAutoApprove() called");
        ValidateCache("TransactionScore");
        if (tranType != null) {
        	String tranTypeCode = tranType.getEmgTranTypeCodeLabel();
        	EMTTransactionType tt = (EMTTransactionType) tranTypes.get(tranTypeCode);
        	return tt != null && "Y".equals(tt.getAutoApproveFlag());
        }
        return false;
    }
 
    public ScoreConfiguration getScoringConfiguration(
            EMTTransactionType tranType) {
        EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(
                "Transaction Scoring Cache : getScoringConfiguration() called");
        ValidateCache("TransactionScore");
        return ((ScoreConfiguration) scoreConfigurations.get(tranType.getEmgTranTypeCodeLabel()));
    }
}
