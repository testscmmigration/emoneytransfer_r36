/**
 * 
 */
package emgshared.model;

/**
 * @author aip8
 *
 */
public class UpdateAccountServiceRequest {
	
	private String accountId;
	
	private String accountStatus;
	
	private String accountSubStatus;
	
	private String cardToken;
	
	private String expirationMonth;
	
	private String expirationYear;
	
	//PWMB-1264
	private String custId;
	
	private boolean accountBlocked;
 

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the accountStatus
	 */
	public String getAccountStatus() {
		return accountStatus;
	}

	/**
	 * @param accountStatus the accountStatus to set
	 */
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	/**
	 * @return the accountSubStatus
	 */
	public String getAccountSubStatus() {
		return accountSubStatus;
	}

	/**
	 * @param accountSubStatus the accountSubStatus to set
	 */
	public void setAccountSubStatus(String accountSubStatus) {
		this.accountSubStatus = accountSubStatus;
	}

	/**
	 * @return the cardToken
	 */
	public String getCardToken() {
		return cardToken;
	}

	/**
	 * @param cardToken the cardToken to set
	 */
	public void setCardToken(String cardToken) {
		this.cardToken = cardToken;
	}

	/**
	 * @return the expirationMonth
	 */
	public String getExpirationMonth() {
		return expirationMonth;
	}

	/**
	 * @param expirationMonth the expirationMonth to set
	 */
	public void setExpirationMonth(String expirationMonth) {
		this.expirationMonth = expirationMonth;
	}

	/**
	 * @return the expirationYear
	 */
	public String getExpirationYear() {
		return expirationYear;
	}

	/**
	 * @param expirationYear the expirationYear to set
	 */
	public void setExpirationYear(String expirationYear) {
		this.expirationYear = expirationYear;
	}

	//PWMB-1264 - Adding Getters and setters for Cust Id
	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public boolean isAccountBlocked() {
		return accountBlocked;
	}

	public void setAccountBlocked(boolean accountBlocked) {
		this.accountBlocked = accountBlocked;
	}
	
	

}
