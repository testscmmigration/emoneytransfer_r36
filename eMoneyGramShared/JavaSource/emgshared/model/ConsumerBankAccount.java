/*
 * Created on Dec 31, 2004
 *
 */
package emgshared.model;

/**
 * @author A131
 *
 */
public class ConsumerBankAccount extends ConsumerAccount {
    private String name;
	private String ABANumber;
	private String financialInstitutionName;
	private String encryptedText;

    public ConsumerBankAccount(int id, int consumerId, AccountStatus status,
            ConsumerAccountType type, String typeDescription,
            String accountNumber, ConsumerAddress billingAddress,
            TaintIndicatorType accountNumberTaintCode,
            TaintIndicatorType bankAbaTaintCode,
            TaintIndicatorType creditCardBinTaintCode) {
		super(
			id,
			consumerId,
			status,
			type,
			typeDescription,
			accountNumber,
			billingAddress,
			accountNumberTaintCode,
			bankAbaTaintCode,
			creditCardBinTaintCode);
        if (!type.isBankAccount()) {
            throw new IllegalArgumentException(
                    "type must be a kind of bank account");
        }
	}

    public String getABANumber() {
		return ABANumber;
	}

    public String getName() {
		return name;
	}

    public String getFinancialInstitutionName() {
		return financialInstitutionName;
	}

    public void setABANumber(String string) {
		ABANumber = string;
	}

    public void setName(String string) {
		name = string;
	}

    public void setFinancialInstitutionName(String string) {
		financialInstitutionName = string;
	}

    public String getDisplayName() {
        if (getFinancialInstitutionName() != null) {
			return getFinancialInstitutionName()
				+ ": "
				+ super.getDisplayName();
		}
		return super.getDisplayName();
	}
	/**
	 * @return
	 */
	public String getEncryptedText() {
		return encryptedText;
	}

	/**
	 * @param string
	 */
	public void setEncryptedText(String string) {
		encryptedText = string;
	}

}
