/*
 * Created on Feb 7, 2005
 *
 */
package emgshared.model;

/**
 * Represent a phone number and its associated type.
 * Object is immutable.
 * @author A131
 *
 */
public class PhoneNumber {
	private String phoneNumber;
	private String phoneNumberType;
	private PhoneType type;
	private TaintIndicatorType phoneTaintCode;
	private int hash;
	private String countryCode;
	private String countryDialingCode;

    public PhoneNumber(String phoneNumber, PhoneType type,
            TaintIndicatorType phoneTaintCode) {
        if (type == null) {
			throw new IllegalArgumentException("type must not be null");
		}

		this.phoneNumber = phoneNumber;
		this.type = type;
		this.phoneTaintCode = phoneTaintCode;
		this.hash = getHashValue();
		
	}

    public String getPhoneNumber() {
		return phoneNumber;
	}

    public PhoneType getType() {
		return type;
	}

    public boolean equals(Object obj) {
		if (obj == this)
		{
			return true;
		}

		if (obj instanceof PhoneNumber == false)
		{
			return false;
		}

		PhoneNumber other = (PhoneNumber) obj;

		return (this.type.equals(other.type))
			&& (this.phoneNumber == null
				? other.phoneNumber == null
				: this.phoneNumber.equalsIgnoreCase(other.phoneNumber));
	}

    public boolean isPhoneBlocked() {
		return phoneTaintCode.isBlocked();
	}

    public boolean isPhoneOverridden() {
		return phoneTaintCode.isOverridden();
	}

    public boolean isPhoneNotBlocked() {
		return phoneTaintCode.isNotBlocked();
	}

    public int hashCode() {
        return hash;
	}

    public String toString() {
		return this.phoneNumber == null ? "" : this.phoneNumber;
	}

    private int getHashValue() {
		int hash = 3;
		hash =
			hash * 17 + (this.phoneNumber == null ? 0 : phoneNumber.hashCode());
		hash = hash * 17 + type.hashCode();
		return hash;
	}
	/**
	 * @return
	 */
    public TaintIndicatorType getPhoneTaintCode() {
        return phoneTaintCode;
	}

	/**
	 * @param type
	 */
    public void setPhoneTaintCode(TaintIndicatorType type) {
        if (type == null) {
			phoneTaintCode = TaintIndicatorType.NOT_BLOCKED;
        } else {
			phoneTaintCode = type;	
		}
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryDialingCode() {
		return countryDialingCode;
	}

	public void setCountryDialingCode(String countryDialingCode) {
		this.countryDialingCode = countryDialingCode;
	}

	public void setPhoneNumberType(String phoneNumberType) {
		this.phoneNumberType = phoneNumberType;
	}

	public String getPhoneNumberType() {
		return phoneNumberType;
	}
	
	

}
