package emgshared.model;

public class ConsumerProfileSearchCriteria {
	private String custLogonId;
	private String custFrstName;
	private String custMiddleName;
	private String custLastName;
	private String custScndLastName;
	private String custPhoneNumber;
	private String custSSNMask;
	private String custCCMask;
	private String custBankMask;
	private String custAddrLine1Text;
	private String custAddrCityName;
	private String custAddrStateName;
	private String custAddrPostalCode;
	private String custPswdText;
	private String custStatCode;
	private String custSubstatCode;
	private String custCreateDate;
	private String custCreateDateTo;
	private String custCreateIpAddrId;
	private String custPrmrCode;

	private String custProgramId;
	private String custAdditionalIdType;
	private String custAdditionalIdEncrypted;
	private String custAdditionalIdMask;
	private String custAdditionalDocStatus;

	private String eDirectoryGuid;

	private String sortBy;
	
	//added by Ankit Bhatt for MBO-128
	private String profileType;
	//ended

	public ConsumerProfileSearchCriteria() {
		custLogonId = null;
		custFrstName = null;
		custMiddleName = null;
		custLastName = null;
		custScndLastName = null;
		custPhoneNumber = null;
		custSSNMask = null;
		custCCMask = null;
		custBankMask = null;
		custAddrLine1Text = null;
		custAddrCityName = null;
		custAddrStateName = null;
		custAddrPostalCode = null;
		custPswdText = null;
		custStatCode = null;
		custSubstatCode = null;
		custCreateDate = null;
		custCreateDateTo = null;
		custCreateIpAddrId = null;
		custPrmrCode = null;
		custProgramId = null;
		sortBy = "custId";
	}

	/**
	 * @return
	 */
	public String getCustLogonId() {
		return custLogonId;
	}

	/**
	 * @param string
	 */
	public void setCustLogonId(String string) {
		custLogonId = string;
	}

	/**
	 * @return
	 */
	public String getCustFrstName() {
		return custFrstName;
	}

	/**
	 * @param string
	 */
	public void setCustFrstName(String string) {
		custFrstName = string;
	}

	/**
	 * @return
	 */
	public String getCustLastName() {
		return custLastName;
	}

	/**
	 * @param string
	 */
	public void setCustLastName(String string) {
		custLastName = string;
	}

	/**
	 * @return
	 */
	public String getCustPhoneNumber() {
		return custPhoneNumber;
	}

	/**
	 * @param string
	 */
	public void setCustPhoneNumber(String string) {
		custPhoneNumber = string;
	}

	/**
	 * @return
	 */
	public String getCustSSNMask() {
		return custSSNMask;
	}

	/**
	 * @param string
	 */
	public void setCustSSNMask(String string) {
		custSSNMask = string;
	}

	/**
	 * @return
	 */
	public String getCustCCMask() {
		return custCCMask;
	}

	/**
	 * @param string
	 */
	public void setCustCCMask(String string) {
		custCCMask = string;
	}

	/**
	 * @return
	 */
	public String getCustBankMask() {
		return custBankMask;
	}

	/**
	 * @param string
	 */
	public void setCustBankMask(String string) {
		custBankMask = string;
	}

	/**
	 * @return
	 */
	public String getCustAddrLine1Text() {
		return custAddrLine1Text;
	}

	/**
	 * @param string
	 */
	public void setCustAddrLine1Text(String string) {
		custAddrLine1Text = string;
	}

	/**
	 * @return
	 */
	public String getCustAddrCityName() {
		return custAddrCityName;
	}

	/**
	 * @param string
	 */
	public void setCustAddrCityName(String string) {
		custAddrCityName = string;
	}

	/**
	 * @return
	 */
	public String getCustAddrStateName() {
		return custAddrStateName;
	}

	/**
	 * @param string
	 */
	public void setCustAddrStateName(String string) {
		custAddrStateName = string;
	}

	/**
	 * @return
	 */
	public String getCustAddrPostalCode() {
		return custAddrPostalCode;
	}

	/**
	 * @param string
	 */
	public void setCustAddrPostalCode(String string) {
		custAddrPostalCode = string;
	}

	/**
	 * @return
	 */
	public String getCustPswdText() {
		return custPswdText;
	}

	/**
	 * @param string
	 */
	public void setCustPswdText(String string) {
		custPswdText = string;
	}

	/**
	 * @return
	 */
	public String getCustStatCode() {
		return custStatCode;
	}

	/**
	 * @param string
	 */
	public void setCustStatCode(String string) {
		custStatCode = string;
	}

	/**
	 * @return
	 */
	public String getCustSubstatCode() {
		return custSubstatCode;
	}

	/**
	 * @param string
	 */
	public void setCustSubstatCode(String string) {
		custSubstatCode = string;
	}

	/**
	 * @return
	 */
	public String getCustCreateDate() {
		return custCreateDate;
	}

	/**
	 * @param string
	 */
	public void setCustCreateDate(String string) {
		custCreateDate = string;
	}

	/**
	 * @return
	 */
	public String getCustCreateIpAddrId()
	{
		return custCreateIpAddrId;
	}

	/**
	 * @param string
	 */
	public void setCustCreateIpAddrId(String string)
	{
		custCreateIpAddrId = string;
	}

	/**
	 * @return
	 */
	public String getSortBy()
	{
		return sortBy;
	}

	/**
	 * @param string
	 */
	public void setSortBy(String string)
	{
		sortBy = string;
	}

    public String getCustPrmrCode() {
        return custPrmrCode;
    }

    public void setCustPrmrCode(String custPrmrCode) {
        this.custPrmrCode = custPrmrCode;
    }

	public String getEDirectoryGuid() {
		return eDirectoryGuid;
	}

	public void setEDirectoryGuid(String directoryGuid) {
		eDirectoryGuid = directoryGuid;
	}

	public String getCustProgramId() {
		return custProgramId;
	}

	public void setCustProgramId(String custProgramId) {
		this.custProgramId = custProgramId;
	}

	public String getCustAdditionalIdType() {
		return custAdditionalIdType;
	}

	public void setCustAdditionalIdType(String custAdditionalIdType) {
		this.custAdditionalIdType = custAdditionalIdType;
	}

	public String getCustAdditionalIdEncrypted() {
		return custAdditionalIdEncrypted;
	}

	public void setCustAdditionalIdEncrypted(String custAdditionalIdEncrypted) {
		this.custAdditionalIdEncrypted = custAdditionalIdEncrypted;
	}

	public String getCustAdditionalIdMask() {
		return custAdditionalIdMask;
	}

	public void setCustAdditionalIdMask(String custAdditionalIdMask) {
		this.custAdditionalIdMask = custAdditionalIdMask;
	}

	public String getCustCreateDateTo() {
		return custCreateDateTo;
	}

	public void setCustCreateDateTo(String custCreateDateTo) {
		this.custCreateDateTo = custCreateDateTo;
	}

	public String getCustAdditionalDocStatus() {
		return custAdditionalDocStatus;
	}

	public void setCustAdditionalDocStatus(String custAdditionalDocStatus) {
		this.custAdditionalDocStatus = custAdditionalDocStatus;
	}

	public String getCustScndLastName() {
		return custScndLastName;
	}

	public void setCustScndLastName(String custScndLastName) {
		this.custScndLastName = custScndLastName;
	}

	public String getCustMiddleName() {
		return custMiddleName;
	}

	public void setCustMiddleName(String custMiddleName) {
		this.custMiddleName = custMiddleName;
	}

	//added by Ankit Bhatt for MBO-128
	public String getProfileType() {
		return profileType;
	}

	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}
	//ended for MBO-128
	

}
