package emgshared.model;

import java.util.List;

public class CountryDialingInfo {
	
	private String countryName;
	private String countryCode;
	private String countryDialingCode;
	private boolean smsEnabled;
	
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getCountryDialingCode() {
		return countryDialingCode;
	}
	public void setCountryDialingCode(String countryDialingCode) {
		this.countryDialingCode = countryDialingCode;
	}
	public boolean isSmsEnabled() {
		return smsEnabled;
	}
	public void setSmsEnabled(boolean smsEnabled) {
		this.smsEnabled = smsEnabled;
	}
	

}
