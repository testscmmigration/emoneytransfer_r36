/*
 * Created on Nov 1, 2005
 *
 */
package emgshared.model;

/**
 * @author T349
 *
 */
public class MicroDepositStatus
{
	//	------------------------------------------
	//	status codes to the codes in the database.
	//	------------------------------------------
	public static final String ACH_WAITING_CODE = "ACW";
	public static final String ACH_IN_PROCESS_CODE = "AIP";
	public static final String ACH_SENT_CODE = "ACS";
	public static final String ACH_IN_PROCESS_ERROR_CODE = "AIE";
	public static final String ACH_RETURNED_CODE = "ACR";
	public static final String ACH_DELETED_CODE = "DEL";
	public static final String ACH_EXPIRED_CODE = "EXP";
	public static final String ACH_FAILED_CODE = "MDF";
	public static final String ACH_VALIDATED_CODE = "VAL";

	private String mdStatus;
	private String mdStatusDesc;

	/**
	 * @return
	 */
	public String getMdStatus()
	{
		return mdStatus == null ? "" : mdStatus;
	}

	/**
	 * @param string
	 */
	public void setMdStatus(String string)
	{
		mdStatus = string;
	}
	
	/**
	 * @return
	 */
	public String getMdStatusDesc()
	{
		return mdStatusDesc == null ? "" : mdStatusDesc;
	}

	/**
	 * @param string
	 */
	public void setMdStatusDesc(String string)
	{
		mdStatusDesc = string;
	}

}
