/*
 * Created on Dec 31, 2004
 *
 */
package emgshared.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;

import emgshared.exceptions.DataSourceException;
import emgshared.services.ServiceFactory;
import emgshared.util.Constants;

/**
 * @author A131
 * 
 */
public class ConsumerProfile implements Serializable {
	public static final String HOME_PHONE_TYPE = "{H}";
	public static final String MOBILE_PHONE_TYPE = "{M}";

	private int id;
	private String userId;
	private ConsumerStatus status;
	private String firstName;
	private String middleName;
	private String lastName;
	private String secondLastName;
	private String pswdText;
	private Collection emails = new HashSet();
	private String phoneNumber;
	private String phoneNumberType;
	private String phoneNumberAlternate;
	private String phoneNumberAlternateType;
	private String idExtnlId;
	private String idNumber;
	private String idType;
	private String idTypeFriendlyName;
	private Date birthdate;
	private String guid;
	private String ssnLast4;
	private boolean authStatusFlag;
	private Date authStatusDate;
	private int authStatusFailCount;
	private boolean acceptPromotionalEmail;
	private ConsumerAddress address;
	private Collection accounts = new HashSet();
	private TaintIndicatorType ssnTaintCode;
	private TaintIndicatorType phoneTaintCode;
	private TaintIndicatorType phoneAlternateTaintCode;
	private TaintIndicatorType accountTaintCode;
	private TaintIndicatorType emailTaintCode;
	private TaintIndicatorType emailDomainTaintCode;
	private TaintIndicatorType consumerMasterTaintCode;
	private Integer logonTrySeq;
	private Date createDate;
	private String createIpAddrId;
	private int invlLoginTryCnt;
	private boolean profileLoginLocked;
	private boolean profileDeletedFromLdap;
	private String custPrmrCode = "S";
	private Date custPrmrDate;
	private String edirGuid;
	private String loyaltyPgmMembershipId;
	private Date adaptiveAuthProfileCompleteDate;
	private String partnerSiteId;
	private String gender;
	private String preferedLanguage;
	private String docStatus;
	//MBO-100:Show Profile Type on EMT Admin Consumer profile Page
	private String profileType;
	//MBO 396
	private int rsaCustomerChallengeStatusCode;
	//MBO : 1800 ::::::::: starts
	private String idIssueCountry;
	private Date idExpirationDate;
	// MBO -2158 EmailAge Story 
	private String emailScore;
	//MBO-2587 Email first seen date
	private String emailFirstSeen;
	
	//MBO-3819
	private String emailAgeNameMatch;
	
	//MBO-4742
	private String mrzLine1;
	private String mrzLine2;
	private String mrzLine3;
	
	//MBO-5465
	private String custStatusCode;
	//MBO-5464
	private String residentStatus;
	private String sendLevel;
	private String receiveLevel;
	private String countryOfBirth;
	private String notificationSMSOptInFlag;
	
	//MBO-6804
	private String phnCountryCode;
	//MBO-7310
	private String phoneCountryCode;
	private String phoneDialingCode;
	private List<String> countryCodeList=new ArrayList<String>();
	
	//MBO-7547
	private String tempResidentNumber;
	private Date tempResidentExpiry;
	//MBO-8224
	private String sourceDevice;
	private String sourceApp;
	
	private int cnsmrPrflIdTypeCd;
	

	

	public int getCnsmrPrflIdTypeCd() {
		return cnsmrPrflIdTypeCd;
	}

	public void setCnsmrPrflIdTypeCd(int cnsmrPrflIdTypeCd) {
		this.cnsmrPrflIdTypeCd = cnsmrPrflIdTypeCd;
	}

	public String getNotificationSMSOptInFlag() {
		return notificationSMSOptInFlag;
	}

	public void setNotificationSMSOptInFlag(String notificationSMSOptInFlag) {
		this.notificationSMSOptInFlag = notificationSMSOptInFlag;
	}

	public String getIdIssueCountry() {
		return idIssueCountry;
	}

	public void setIdIssueCountry(String idIssueCountry) {
		this.idIssueCountry = idIssueCountry;
	}
	
	public Date getIdExpirationDate() {
		return idExpirationDate;
	}
	
	public void setIdExpirationDate(Date date) {	
		idExpirationDate = date;
	}
	//MBO : 1800 ::::::::::::::: ends
	public boolean[] getCommandFlags() {
		boolean[] isFlags = { false, 
				false, 
				false 
		};
		// authorization action table
		int[] actionIds = { 1, 
				2, 
				3 
		};
		// Oct2011 - converted some more common ones to constants instead of
		// having to reference arrays...
		if(docStatus!=null&&!docStatus.equals("")){
			for (int i = 0; i < isFlags.length; i++) {
				switch (actionIds[i]) {
				case 1: {
					if (!docStatus.equals(Constants.DENIED_DOC_STATUS)&&!docStatus.equals(Constants.APPROVED_DOC_STATUS)
							&&!docStatus.equals(Constants.REQUESTED_DOC_STATUS)) {
						isFlags[i] = true;
					}
					break;
				}
				case 2: // 1
					if (!docStatus.equals(Constants.DENIED_DOC_STATUS)&&!docStatus.equals(Constants.APPROVED_DOC_STATUS)
							&&!docStatus.equals(Constants.REQUESTED_DOC_STATUS)) {
						isFlags[i] = true;
					}
					break;
				case 3: // 2
					if (!docStatus.equals(Constants.DENIED_DOC_STATUS)&&!docStatus.equals(Constants.APPROVED_DOC_STATUS)
							&&!docStatus.equals(Constants.PENDING_DOC_STATUS)&&!docStatus.equals(Constants.REQUESTED_DOC_STATUS)) {
						isFlags[i] = true;
					}
					break;
				}
			}
		}
		return isFlags;
	}

	/**
	 * @param id
	 *            - persistence identifier
	 * @param ssnLast4
	 *            - last four digits of social security number.
	 * @param status
	 *            - must not be null
	 * @param address
	 *            - must not be null
	 */
	public ConsumerProfile(int id, String ssnLast4, ConsumerStatus status, ConsumerAddress address, TaintIndicatorType ssnTaintCode,
			TaintIndicatorType phoneTaintCode, TaintIndicatorType phoneAlternateTaintCode, TaintIndicatorType emailTaintCode,
			TaintIndicatorType emailDomainTaintCode) {
		Validate.notNull(status, "status is a required parameter");
		Validate.notNull(address, "address is a required parameter");
		this.id = id;
		this.ssnLast4 = ssnLast4;
		this.status = status;
		this.address = new ConsumerAddress(address);
		this.ssnTaintCode = ssnTaintCode;
		this.phoneTaintCode = phoneTaintCode;
		this.phoneAlternateTaintCode = phoneAlternateTaintCode;
		this.emailTaintCode = emailTaintCode;
		this.emailDomainTaintCode = emailDomainTaintCode;
	}
	
	


	/**
	 * Copy constructor.
	 * 
	 * @param source
	 *            - must not be null
	 */
	public ConsumerProfile(ConsumerProfile source) {
		final String blank = "";
		Validate.notNull(source, "source argument must not be null");
		this.id = source.id;
		this.userId = source.userId;
		this.status = source.status;
		this.firstName = source.firstName;
		this.middleName = source.middleName;
		this.lastName = source.lastName;
		this.secondLastName = source.secondLastName;
		this.emails = new HashSet(source.emails);
		this.phoneNumber = source.phoneNumber;
		this.phoneNumberAlternate = source.phoneNumberAlternate;
		this.birthdate = source.birthdate == null ? null : new Date(source.birthdate.getTime());
		//1800
		this.idExpirationDate = source.idExpirationDate;	
		this.idIssueCountry = source.idIssueCountry == blank ? "" : source.idIssueCountry;
		//1800
		this.guid = source.guid;
		this.ssnLast4 = source.ssnLast4;
		this.acceptPromotionalEmail = source.acceptPromotionalEmail;
		this.address = new ConsumerAddress(source.address);
		// TODO consider copying each element of the collection.
		this.accounts = new HashSet(source.accounts);
		this.authStatusFlag = source.authStatusFlag;
		this.authStatusDate = source.authStatusDate;
		this.authStatusFailCount = source.authStatusFailCount;
		this.ssnTaintCode = source.ssnTaintCode;
		this.phoneTaintCode = source.phoneTaintCode;
		this.phoneAlternateTaintCode = source.phoneAlternateTaintCode;
		this.edirGuid = source.edirGuid;
	}

	public boolean addAccount(ConsumerAccount account) {
		boolean added = false;
		if (account != null) {
			added = this.accounts.add(account);
		}
		return added;
	}

	/**
	 * @return array of bank accounts. zero length if none.
	 */
	public ConsumerBankAccount[] getBankAccounts() {
		Set desiredAccounts = new HashSet();
		for (Iterator iter = this.accounts.iterator(); iter.hasNext();) {
			ConsumerAccount account = (ConsumerAccount) iter.next();
			if (account.isBankAccount()) {
				desiredAccounts.add(account);
			}
		}
		return (ConsumerBankAccount[]) desiredAccounts.toArray(new ConsumerBankAccount[0]);
	}

	/**
	 * @return array of debit card accounts. zero length if none.
	 */
	public ConsumerCreditCardAccount[] getCardAccounts() {
		Set desiredAccounts = new HashSet();
		for (Iterator iter = this.accounts.iterator(); iter.hasNext();) {
			ConsumerAccount account = (ConsumerAccount) iter.next();
			if (account.isCardAccount()) {
				desiredAccounts.add(account);
			}
		}
		return (ConsumerCreditCardAccount[]) desiredAccounts.toArray(new ConsumerCreditCardAccount[0]);
	}

	/**
	 * @return array of debit card accounts. zero length if none.
	 */
	public ConsumerCreditCardAccount[] getDebitCardAccounts() {
		Set desiredAccounts = new HashSet();
		for (Iterator iter = this.accounts.iterator(); iter.hasNext();) {
			iter.next();
		}
		return (ConsumerCreditCardAccount[]) desiredAccounts.toArray(new ConsumerCreditCardAccount[0]);
	}

	public ConsumerCreditCardAccount[] getCreditCardAccounts() {
		Set desiredAccounts = new HashSet();
		for (Iterator iter = this.accounts.iterator(); iter.hasNext();) {
			ConsumerAccount account = (ConsumerAccount) iter.next();
			if (account.isCardAccount()) {
				desiredAccounts.add(account);
			}
		}
		return (ConsumerCreditCardAccount[]) desiredAccounts.toArray(new ConsumerCreditCardAccount[0]);
	}


	public int getRsaCustomerChallengeStatusCode() {
		return rsaCustomerChallengeStatusCode;
	}

	public void setRsaCustomerChallengeStatusCode(int rsaCustomerChallengeStatusCode) {
		this.rsaCustomerChallengeStatusCode = rsaCustomerChallengeStatusCode;
	}

	public String getFirstName() {
		return firstName;
	}

	public int getId() {
		return id;
	}
	

	public void setId(int id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public Date getBirthdate() {
		return birthdate == null ? null : new Date(this.birthdate.getTime());
	}

	public String getGuid() {
		return guid;
	}

	public void setFirstName(String string) {
		firstName = StringUtils.trimToNull(string);
	}

	public void setLastName(String string) {
		lastName = StringUtils.trimToNull(string);
	}

	public void setMiddleName(String string) {
		middleName = StringUtils.trimToNull(string);
	}

	/**
	 * sets birthdate to null or copy of date.
	 * 
	 * @param date
	 */
	public void setBirthdate(Date date) {
		birthdate = (date == null ? null : new Date(date.getTime()));
	}

	public void setGuid(String string) {
		guid = StringUtils.trimToNull(string);
	}

	public String getAddressLine1() {
		return address.getAddressLine1();
	}

	public String getAddressLine2() {
		return address.getAddressLine2();
	}
	
	public String getAddressLine3() {
		return address.getAddressLine3();
	}	

	public String getCity() {
		return address.getCity();
	}

	public String getPostalCode() {
		return address.getPostalCode();
	}

	public String getPostalCodePlusZip4() {
		return address.getPostalCodePlusZip4();
	}

	public String getState() {
		return address.getState();
	}

	public String getIsoCountryCode() {
		return address.getIsoCountryCode();
	}

	public void setAddressLine1(String string) {
		address.setAddressLine1(string);
	}

	public void setAddressLine2(String string) {
		address.setAddressLine2(string);
	}
	
	public void setAddressLine3(String string) {
		address.setAddressLine3(string);
	}

	public void setCity(String string) {
		address.setCity(string);
	}

	public void setPostalCode(String string) {
		address.setPostalCode(string);
	}

	public void setState(String string) {
		address.setState(string);
	}

	public void setIsoCountryCode(String code) {
		address.setIsoCountryCode(code);
	}

	public String getCountyName() {
		return address.getCountyName();
	}

	public void setCountyName(String countyName) {
		address.setCountyName(countyName);
	}

	public boolean isAcceptPromotionalEmail() {		return acceptPromotionalEmail;
	}

	public void setAcceptPromotionalEmail(boolean b) {
		acceptPromotionalEmail = b;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String string) {
		userId = StringUtils.trimToNull(string);
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getPhoneNumberAlternate() {
		return phoneNumberAlternate;
	}

	public void setPhoneNumber(String string) {
		phoneNumber = StringUtils.trimToNull(string);
	}

	public void setPhoneNumberAlternate(String string) {
		phoneNumberAlternate = StringUtils.trimToNull(string);
	}

	public String getZip4() {
		return address.getZip4() == null ? "" : address.getZip4();
	}

	public void setZip4(String string) {
		String zip4 = string != null ? string.trim() : "";
		address.setZip4(zip4);
	}

	public ConsumerStatus getStatus() {
		return status;
	}

	public String getStatusCode() {
		return (status == null ? null : status.getStatusCode());
	}

	public String getSubStatusCode() {
		return (status == null ? null : status.getSubStatusCode());
	}

	public void setStatus(ConsumerStatus newStatus) {
		Validate.notNull(newStatus, "newStatus is a required parameter");
		status = newStatus;
	}

	public void setStatus(String statusCode, String subStatusCode) {
		status = ConsumerStatus.getInstance(statusCode, subStatusCode);
	}

	/**
	 * @return copy of address object.
	 * 
	 *         Created on Jan 18, 2005
	 */
	public ConsumerAddress getAddress() {
		return new ConsumerAddress(this.address);
	}

	public ConsumerAccount getAccount(String accountId) {
		ConsumerAccount account = null;
		if (StringUtils.isNotBlank(accountId) && StringUtils.isNumeric(accountId)) {
			account = getAccount(Integer.parseInt(accountId));
		}
		return account;
	}

	// TODO consider returning a copy of the object.
	public ConsumerAccount getAccount(int accountId) {
		ConsumerAccount account = null;
		for (Iterator iter = this.accounts.iterator(); account == null && iter.hasNext();) {
			ConsumerAccount temp = (ConsumerAccount) iter.next();
			if (temp.getId() == accountId) {
				account = temp;
			}
		}
		return account;
	}

	public boolean removeAccount(ConsumerAccount account) {
		return this.accounts.remove(account);
	}

	public boolean isPendingStatus() {
		return status.isPendingStatus();
	}

	public boolean isLoginAllowed() {
		return status.isLoginAllowed();
	}

	public boolean isAccountBlocked() {
		return accountTaintCode.isBlocked();
	}

	public boolean isEmailBlocked() {
		return emailTaintCode.isBlocked();
	}

	public boolean isEmailDomainBlocked() {
		return emailDomainTaintCode.isBlocked();
	}

	public boolean isAccountOverridden() {
		return accountTaintCode.isOverridden();
	}

	public boolean isAccountNotBlocked() {
		return accountTaintCode.isNotBlocked();
	}

	public boolean isEmailNotBlocked() {
		return emailTaintCode.isNotBlocked();
	}

	public boolean isEmailDomainNotBlocked() {
		return emailDomainTaintCode.isNotBlocked();
	}

	public boolean isSsnBlocked() {
		return ssnTaintCode.isBlocked();
	}

	public boolean isSsnOverridden() {
		return ssnTaintCode.isOverridden();
	}

	public boolean isSsnNotBlocked() {
		return ssnTaintCode.isNotBlocked();
	}

	public boolean isPhoneBlocked() {
		return phoneTaintCode.isBlocked();
	}

	public boolean isPhoneOverridden() {
		return phoneTaintCode.isOverridden();
	}

	public boolean isPhoneNotBlocked() {
		return phoneTaintCode.isNotBlocked();
	}

	public boolean isPhoneAlternateBlocked() {
		return phoneAlternateTaintCode.isBlocked();
	}

	public boolean isPhoneAlternateOverridden() {
		return phoneAlternateTaintCode.isOverridden();
	}

	public boolean isPhoneAlternateNotBlocked() {
		return phoneAlternateTaintCode.isNotBlocked();
	}

	public boolean isPersonToPersonAllowed() {
		return status.isPersonToPersonAllowed();
	}

	public boolean isExpressPayAllowed() {
		if ((getBankAccounts() == null) || (getBankAccounts().length < 1))
			return false;

		if ((getCreditCardAccounts() == null) || (getCreditCardAccounts().length < 1))
			return false;

		return status.isExpressPayAllowed();
	}

	public boolean isCardAccountLinked() {
		if ((getCardAccounts() == null) || (getCardAccounts().length < 1)) {
			return false;
		}
		return true;
	}

	/*
	 * public boolean isDebitCardLinked() { if ((getDebitCardAccounts() == null)
	 * || (getDebitCardAccounts().length < 1)) { return false; } return true; }
	 */
	public boolean isCreditCardLinked() {
		if ((getCreditCardAccounts() == null) || (getCreditCardAccounts().length < 1)) {
			return false;
		}
		return true;
	}

	public boolean isACHLinked() {
		if ((getBankAccounts() == null) || (getBankAccounts().length < 1))
			return false;
		return true;
	}

	public boolean goThruVerid() {
		if (!isAuthStatusFlag())
			return true;
		// if not L02/L03
		return (!status.isPersonToPersonAllowed());
	}

	public boolean isEmailActive() {
		ConsumerEmail consumerEmail = null;
		// Loop sends out email to each address and assigns the real
		// consumer id because the Customer record is now created.
		for (Iterator iter = this.getEmails().iterator(); iter.hasNext();) {
			consumerEmail = (ConsumerEmail) iter.next();
			if (consumerEmail.getStatus().equals(EmailStatus.ACTIVE)) {
				return true;
			}
		}
		return false;
	}

	public String getSsnLast4() {
		return ssnLast4;
	}

	
	public void setSsnLast4(String ssnLast4) {
		this.ssnLast4 = ssnLast4;
	}

	public Date getAuthStatusDate() {
		return authStatusDate;
	}

	public int getAuthStatusFailCount() {
		return authStatusFailCount;
	}

	public boolean isAuthStatusFlag() {
		return authStatusFlag;
	}

	public void setAuthStatusDate(Date date) {
		authStatusDate = date;
	}

	public void setAuthStatusFailCount(int i) {
		authStatusFailCount = i;
	}

	public void setAuthStatusFlag(boolean b) {
		authStatusFlag = b;
	}

	public Collection getAccounts() {
		return accounts;
	}

	public void setAccounts(Collection collection) {
		accounts = collection;
	}

	public Collection getEmails() {
		return emails;
	}

	public void setEmails(Collection collection) {
		emails = collection;
	}

	public TaintIndicatorType getPhoneTaintCode() {
		return phoneTaintCode;
	}

	public TaintIndicatorType getSsnTaintCode() {
		return ssnTaintCode;
	}

	public void setPhoneTaintCode(TaintIndicatorType type) {
		if (type == null) {
			phoneTaintCode = TaintIndicatorType.NOT_BLOCKED;
		} else {
			phoneTaintCode = type;
		}
	}

	public void setSsnTaintCode(TaintIndicatorType type) {
		if (type == null) {
			phoneTaintCode = TaintIndicatorType.NOT_BLOCKED;
		} else {
			ssnTaintCode = type;
		}

	}

	public TaintIndicatorType getAccountTaintCode() {
		return accountTaintCode;
	}

	public void setAccountTaintCode(TaintIndicatorType type) {
		if (type == null) {
			phoneTaintCode = TaintIndicatorType.NOT_BLOCKED;
		} else {
			accountTaintCode = type;
		}

	}

	public TaintIndicatorType getPhoneAlternateTaintCode() {
		return phoneAlternateTaintCode;
	}

	public void setPhoneAlternateTaintCode(TaintIndicatorType type) {
		if (type == null) {
			phoneAlternateTaintCode = TaintIndicatorType.NOT_BLOCKED;
		} else {
			phoneAlternateTaintCode = type;
		}

	}

	public Integer getLogonTrySeq() {
		return logonTrySeq;
	}

	public void setLogonTrySeq(Integer integer) {
		logonTrySeq = integer;
	}

	public String getPswdText() {
		return pswdText;
	}

	public void setPswdText(String string) {
		pswdText = string;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date date) {
		createDate = date;
	}

	public TaintIndicatorType getEmailTaintCode() {
		return emailTaintCode;
	}

	public void setEmailTaintCode(TaintIndicatorType type) {
		emailTaintCode = type;
	}

	public TaintIndicatorType getConsumerMasterTaintCode() {
		return consumerMasterTaintCode;
	}

	public void setConsumerMasterTaintCode(TaintIndicatorType consumerMasterTaintCode) {
		this.consumerMasterTaintCode = consumerMasterTaintCode;
	}

	public String getCreateIpAddrId() {
		return createIpAddrId;
	}

	public void setCreateIpAddrId(String string) {
		createIpAddrId = string;
	}

	public int getInvlLoginTryCnt() {
		return invlLoginTryCnt;
	}

	public void setInvlLoginTryCnt(int i) {
		invlLoginTryCnt = i;
	}

	public TaintIndicatorType getEmailDomainTaintCode() {
		return emailDomainTaintCode;
	}

	public void setEmailDomainTaintCode(TaintIndicatorType type) {
		emailDomainTaintCode = type;
	}

	public float getLast30DayTotal() throws DataSourceException {
		BigDecimal monthlyTotal = ServiceFactory.getInstance().getAccountingService().getLast30DayTotal(getUserId(), getId());

		return monthlyTotal.floatValue();
	}

	public String getCustPrmrCode() {
		return custPrmrCode == null ? "S" : custPrmrCode;
	}

	public void setCustPrmrCode(String custPrmrCode) {
		this.custPrmrCode = custPrmrCode;
	}

	public Date getCustPrmrDate() {
		return this.custPrmrDate;
	}

	public void setCustPrmrDate(Date date) {
		this.custPrmrDate = date;
	}

	public void setEdirGuid(String edirGuid) {
		this.edirGuid = edirGuid.trim();
	}

	public String getEdirGuid() {
		return this.edirGuid.trim();
	}

	public String getLoyaltyPgmMembershipId() {
		return loyaltyPgmMembershipId;
	}

	public void setLoyaltyPgmMembershipId(String loyaltyPgmMembershipId) {
		this.loyaltyPgmMembershipId = loyaltyPgmMembershipId;
	}

	public Date getAdaptiveAuthProfileCompleteDate() {
		return adaptiveAuthProfileCompleteDate;
	}

	public void setAdaptiveAuthProfileCompleteDate(Date adaptiveAuthProfileCompleteDate) {
		this.adaptiveAuthProfileCompleteDate = adaptiveAuthProfileCompleteDate;
	}

	public boolean getProfileLoginLocked() {
		return profileLoginLocked;
	}

	public void setProfileLoginLocked(boolean profileLoginLocked) {
		this.profileLoginLocked = profileLoginLocked;
	}

	public boolean isProfileDeletedFromLdap() {
		return profileDeletedFromLdap;
	}

	public void setProfileDeletedFromLdap(boolean profileDeletedFromLdap) {
		this.profileDeletedFromLdap = profileDeletedFromLdap;
	}

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}

	// TODO change for real code once DB changes are done!!!
	public String getProgram() {
		String whatsTheId = id + "";
		if (whatsTheId.equals("1550844")) {
			return "UK";
		}
		return "US";
	}

	// TODO change for real code once DB changes are done!!!
	public boolean getHasRewardProgram() {
		String market = getProgram();
		return market.equals("US");
	}

	public void setPhoneNumberType(String phoneNumberType) {
		this.phoneNumberType = phoneNumberType;
	}

	public String getPhoneNumberType() {
		return phoneNumberType;
	}

	public void setPhoneNumberAlternateType(String phoneNumberalternateType) {
		this.phoneNumberAlternateType = phoneNumberalternateType;
	}

	public String getPhoneNumberAlternateType() {
		return phoneNumberAlternateType;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdTypeFriendlyName(String idTypeFriendlyName) {
		this.idTypeFriendlyName = idTypeFriendlyName;
	}

	public String getIdTypeFriendlyName() {
		return idTypeFriendlyName;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getGender() {
		return gender;
	}

	public String getDocStatus() {
		return docStatus;
	}

	public void setDocStatus(String docStatus) {
		this.docStatus = docStatus;
	}

	public String getIdExtnlId() {
		return idExtnlId;
	}

	public void setIdExtnlId(String idExtnlId) {
		this.idExtnlId = idExtnlId;
	}	
	
	public void setPreferedLanguage(String preferedLanguage) {
		this.preferedLanguage = preferedLanguage;
	}

	/**
	 * @return the preferedLanguage
	 */
	public String getPreferedLanguage() {
		return preferedLanguage;
	}

	public String getSecondLastName() {
		return secondLastName;
	}

	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	public String getProfileType() {
		return profileType;
	}

	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}

	public String getEmailScore() {
		return emailScore;
	}

	public void setEmailScore(String emailScore) {
		this.emailScore = emailScore;
	}

	public String getEmailFirstSeen() {
		return emailFirstSeen;
	}

	public void setEmailFirstSeen(String emailFirstSeen) {
		this.emailFirstSeen = emailFirstSeen;
	}

	public String getEmailAgeNameMatch() {
		return emailAgeNameMatch;
	}

	public void setEmailAgeNameMatch(String emailAgeNameMatch) {
		this.emailAgeNameMatch = emailAgeNameMatch;
	}

	public String getMrzLine1() {
		return mrzLine1;
	}

	public void setMrzLine1(String mrzLine1) {
		this.mrzLine1 = mrzLine1;
	}

	public String getMrzLine2() {
		return mrzLine2;
	}

	public void setMrzLine2(String mrzLine2) {
		this.mrzLine2 = mrzLine2;
	}

	public String getMrzLine3() {
		return mrzLine3;
	}

	public void setMrzLine3(String mrzLine3) {
		this.mrzLine3 = mrzLine3;
	}

	public String getCustStatusCode() {
		return custStatusCode;
	}

	public void setCustStatusCode(String custStatusCode) {
		this.custStatusCode = custStatusCode;
	}

	public String getResidentStatus() {
		return residentStatus;
	}

	public void setResidentStatus(String residentStatus) {
		this.residentStatus = residentStatus;
	}

	public String getSendLevel() {
		return sendLevel;
	}

	public void setSendLevel(String sendLevel) {
		this.sendLevel = sendLevel;
	}

	public String getReceiveLevel() {
		return receiveLevel;
	}

	public void setReceiveLevel(String receiveLevel) {
		this.receiveLevel = receiveLevel;
	}

	public void setCountryOfBirth(String countryOfBirth) {
		this.countryOfBirth = countryOfBirth;
	}

	public String getCountryOfBirth() {
		return countryOfBirth;
	}

	public String getPhnCountryCode() {
		return phnCountryCode;
	}

	public void setPhnCountryCode(String phnCountryCode) {
		this.phnCountryCode = phnCountryCode;
	}

	public String getPhoneCountryCode() {
		return phoneCountryCode;
	}

	public void setPhoneCountryCode(String phoneCountryCode) {
		this.phoneCountryCode = phoneCountryCode;
	}

	public String getPhoneDialingCode() {
		return phoneDialingCode;
	}

	public void setPhoneDialingCode(String phoneDialingCode) {
		this.phoneDialingCode = phoneDialingCode;
	}

	public List<String> getCountryCodeList() {
		return countryCodeList;
	}

	public void setCountryCodeList(List<String> countryCodeList) {
		this.countryCodeList = countryCodeList;
	}

	public String getTempResidentNumber() {
		return tempResidentNumber;
	}

	public void setTempResidentNumber(String tempResidentNumber) {
		this.tempResidentNumber = tempResidentNumber;
	}

	public Date getTempResidentExpiry() {
		return tempResidentExpiry;
	}

	public void setTempResidentExpiry(Date tempResidentExpiry) {
		this.tempResidentExpiry = tempResidentExpiry;
	}

	public String getSourceDevice() {
		return sourceDevice;
	}

	public void setSourceDevice(String sourceDevice) {
		this.sourceDevice = sourceDevice;
	}

	public String getSourceApp() {
		return sourceApp;
	}

	public void setSourceApp(String sourceApp) {
		this.sourceApp = sourceApp;
	}
	
	
	
}
