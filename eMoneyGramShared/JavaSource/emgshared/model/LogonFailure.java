/**
 * @author T349
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgshared.model;

public class LogonFailure
{
	private int id;
	private String userId;
	private Integer logonTrySeq;
	private boolean profileDisabled;
	private boolean blockedIp;
	private boolean blockedSsn;
	private boolean blockedBank;
	private boolean blockedCc;
	private boolean blockedAba;
	private boolean blockedCcBin;
	private boolean blockedPhone;
	private boolean blockedAltPhone;
	private boolean blockedEmail;
	private boolean blockedEmailDomain;

	public LogonFailure()
	{
		id = -1;
		userId = null;
		logonTrySeq = null;
		profileDisabled = false;
		blockedIp = false;
		blockedSsn = false;
		blockedBank = false;
		blockedCc = false;
		blockedAba = false;
		blockedCcBin = false;
		blockedPhone = false;
		blockedAltPhone = false;
		blockedEmail = false;
	}
	/**
	 * @return
	 */
	public boolean isBlockedAba()
	{
		return blockedAba;
	}

	/**
	 * @return
	 */
	public boolean isBlockedAltPhone()
	{
		return blockedAltPhone;
	}

	/**
	 * @return
	 */
	public boolean isBlockedBank()
	{
		return blockedBank;
	}

	/**
	 * @return
	 */
	public boolean isBlockedCc()
	{
		return blockedCc;
	}

	/**
	 * @return
	 */
	public boolean isBlockedCcBin()
	{
		return blockedCcBin;
	}

	/**
	 * @return
	 */
	public boolean isBlockedIp()
	{
		return blockedIp;
	}

	/**
	 * @return
	 */
	public boolean isBlockedPhone()
	{
		return blockedPhone;
	}

	/**
	 * @return
	 */
	public boolean isBlockedSsn()
	{
		return blockedSsn;
	}

	/**
	 * @return
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @return
	 */
	public Integer getLogonTrySeq()
	{
		return logonTrySeq;
	}

	/**
	 * @return
	 */
	public boolean isProfileDisabled()
	{
		return profileDisabled;
	}

	/**
	 * @param b
	 */
	public void setBlockedAba(boolean b)
	{
		blockedAba = b;
	}

	/**
	 * @param b
	 */
	public void setBlockedAltPhone(boolean b)
	{
		blockedAltPhone = b;
	}

	/**
	 * @param b
	 */
	public void setBlockedBank(boolean b)
	{
		blockedBank = b;
	}

	/**
	 * @param b
	 */
	public void setBlockedCc(boolean b)
	{
		blockedCc = b;
	}

	/**
	 * @param b
	 */
	public void setBlockedCcBin(boolean b)
	{
		blockedCcBin = b;
	}

	/**
	 * @param b
	 */
	public void setBlockedIp(boolean b)
	{
		blockedIp = b;
	}

	/**
	 * @param b
	 */
	public void setBlockedPhone(boolean b)
	{
		blockedPhone = b;
	}

	/**
	 * @param b
	 */
	public void setBlockedSsn(boolean b)
	{
		blockedSsn = b;
	}

	/**
	 * @param i
	 */
	public void setId(int i)
	{
		id = i;
	}

	/**
	 * @param integer
	 */
	public void setLogonTrySeq(Integer integer)
	{
		logonTrySeq = integer;
	}

	/**
	 * @param b
	 */
	public void setProfileDisabled(boolean b)
	{
		profileDisabled = b;
	}

	/**
	 * @return
	 */
	public String getUserId()
	{
		return userId;
	}

	/**
	 * @param string
	 */
	public void setUserId(String string)
	{
		userId = string;
	}

	/**
	 * @return
	 */
	public boolean isBlockedEmail()
	{
		return blockedEmail;
	}

	/**
	 * @param b
	 */
	public void setBlockedEmail(boolean b)
	{
		blockedEmail = b;
	}

	/**
	 * @return
	 */
	public boolean isBlockedEmailDomain()
	{
		return blockedEmailDomain;
	}

	/**
	 * @param b
	 */
	public void setBlockedEmailDomain(boolean b)
	{
		blockedEmailDomain = b;
	}

}
