package emgshared.model;

public class MicroDeposit
{
	private int custId;
	
	private int validationId;
	private int custAccountId;
	private int custAccountVersionNbr;
	private MicroDepositStatus status;
	private int validationTryCount;
	private String statUpdateDate;
	private String createDate;
	private String createUser;
	private String lastUpdateDate;
	private String lastUpdateUser;
	private int[] depositTranId = new int[2];
	private float[] depositAmount = new float[2];
	private String[] fmtAmount = new String[2];
	private int[] batchId = new int[2];
	
	private int depositAmountIndex = 0;
	private String callerLoginId;
	private boolean processACR;
	private boolean processDeactiveAcct;
	private String msg;

	public int getCustId()
	{
		return custId;
	}

	public void setCustId(int i)
	{
		custId = i;
	}

	public int getValidationId()
	{
		return validationId;
	}

	public void setValidationId(int i)
	{
		validationId = i;
	}

	public int getCustAccountId()
	{
		return custAccountId;
	}

	public void setCustAccountId(int i)
	{
		custAccountId = i;
	}

	public int getCustAccountVersionNbr()
	{
		return custAccountVersionNbr;
	}

	public void setCustAccountVersionNbr(int i)
	{
		custAccountVersionNbr = i;
	}

	public MicroDepositStatus getStatus()
	{
		return status;
	}

	public void setStatus(MicroDepositStatus status)
	{
		this.status = status;
	}

	public int getValidationTryCount()
	{
		return validationTryCount;
	}

	public void setValidationTryCount(int i)
	{
		validationTryCount = i;
	}

	public String getStatUpdateDate()
	{
		return statUpdateDate;
	}

	public void setStatUpdateDate(String string)
	{
		statUpdateDate = string;
	}

	public String getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(String string)
	{
		createDate = string;
	}

	public String getCreateUser()
	{
		return createUser;
	}

	public void setCreateUser(String string)
	{
		createUser = string;
	}

	public String getLastUpdateDate()
	{
		return lastUpdateDate;
	}

	public void setLastUpdateDate(String string)
	{
		lastUpdateDate = string;
	}

	public String getLastUpdateUser()
	{
		return lastUpdateUser;
	}

	public void setLastUpdateUser(String string)
	{
		lastUpdateUser = string;
	}

	public int getDepositTranId1()
	{
		return depositTranId[0];
	}

	public int getDepositTranId2()
	{
		return depositTranId[1];
	}

	public void setDepositTranId1(int i)
	{
		depositTranId[0] = i;
	}

	public void setDepositTranId2(int i)
	{
		depositTranId[1] = i;
	}

	public float getDepositAmount1()
	{
		return depositAmount[0];
	}

	public float getDepositAmount2()
	{
		return depositAmount[1];
	}

	public void setDepositAmount1(float f)
	{
		depositAmount[0] = f;
	}

	public void setDepositAmount2(float f)
	{
		depositAmount[1] = f;
	}

	public String getFmtAmount1()
	{
		return fmtAmount[0];
	}

	public String getFmtAmount2()
	{
		return fmtAmount[1];
	}

	public void setFmtAmount1(String s)
	{
		fmtAmount[0] = s;
	}

	public void setFmtAmount2(String s)
	{
		fmtAmount[1] = s;
	}

	public int getBatchId1()
	{
		return batchId[0];
	}

	public int getBatchId2()
	{
		return batchId[1];
	}

	public void setBatchId1(int i)
	{
		batchId[0] = i;
	}

	public void setBatchId2(int i)
	{
		batchId[1] = i;
	}

	public int getDepositAmountIndex()
	{
		return depositAmountIndex;
	}

	public void setDepositAmountIndex(int i)
	{
		depositAmountIndex = i;
	}

	public String getCallerLoginId()
	{
		return callerLoginId;
	}

	public void setCallerLoginId(String string)
	{
		callerLoginId = string;
	}
	
	public float[] getDepositAmount()
	{
		return depositAmount;
	}

	public void setDepositAmount(float[] fs)
	{
		depositAmount = fs.clone();
	}

	public boolean isProcessACR()
	{
		return processACR;
	}

	public void setProcessACR(boolean b)
	{
		processACR = b;
	}

	public boolean isProcessDeactiveAcct()
	{
		return processDeactiveAcct;
	}

	public void setProcessDeactiveAcct(boolean b)
	{
		processDeactiveAcct = b;
	}

	public String getMsg()
	{
		return msg == null ? "" : msg;
	}

	public void setMsg(String string)
	{
		msg = string;
	}
}