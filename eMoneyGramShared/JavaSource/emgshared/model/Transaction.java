package emgshared.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.Validate;

import emgshared.property.EMTSharedContainerProperties;
import emgshared.util.Constants;
import emgshared.util.DateFormatter;
import emgshared.util.StringHelper;

public class Transaction implements Comparable, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1238980848005864838L;
	private int emgTranId;
	private String emgTranTypeCode;
	private String emgTranTypeDesc;
	private String sndCustLogonId;
	private int sndCustId;
	private int sndCustAcctId;
	private String sndCustLastName;
	private String sndCustScndLastName;
	private String sndCustFrstName;
	private String sndCustSsnMaskNbr;
	private int sndCustAcctVerNbr;
	private String sndAcctTypeCode;
	private String sndAcctMaskNbr;
	private int sndCustBkupAcctId;
	private int sndCustBkupAcctVerNbr;
	private String sndBkupAcctTypeCode;
	private String sndBkupAcctMaskNbr;
	private String bankAbaNbr;
	private String lgcyRefNbr;
	private Date sndTranDate;
	private TransactionStatus status;
	private String tranStatDesc;
	private String tranSubStatDesc;
	private Date tranStatDate;
	private int sndAgentId;
	private int dlvrOptnId;
	private String sndISOCntryCode;
	private String sndISOCrncyId;
	private BigDecimal sndFaceAmt;
	private BigDecimal sndFeeAmt;
	private BigDecimal rtnFeeAmt;
	private BigDecimal sndTotAmt;
	private BigDecimal sndFxCnsmrRate;
	private BigDecimal sndThrldWarnAmt;
	private int formFreeConfNbr;
	private int intndRcvAgentId;
	private int rcvAgentId;
	private String rcvAgentConfId;
	private String rcvAgcyCode;
	private String rcvISOCntryCode;
	private String rcvISOCrncyId;
	private String sndCustIPAddrId;
	private String sndMsg1Text;
	private String sndMsg2Text;
	private int sndCustAddrId;
	private String rcvCustFrstName;
	private String rcvCustLastName;
	private String rcvAgentName;
	private String riskLvlCode;
	private int transScores;
	private String sysAutoRsltCode;
	private String csrPrcsUserid;
	private Date csrPrcsDate;
	private int achFileCntlSeqNbr;
	private Date createDate;
	private String createUserid;
	private String lastUpdateUserid;
	private Date lastUpdateDate;
	private String rcvAgcyAcctEncryp;
	private String rcvAgcyAcctMask;
	private String esSendable;
	private String rcvCustMatrnlName;
	private String rcvCustMidName;
	private String rcvCustAddrStateName;
	private String rcvAgentRefNbr;
	private String rcvCustAddrLine1Text;
	private String rcvCustAddrLine2Text;
	private String rcvCustAddrLine3Text;
	private String rcvCustAddrCityName;
	private String rcvCustAddrPostalCode;
	private String rcvCustDlvrInstr1Text;
	private String rcvCustDlvrInstr2Text;
	private String rcvCustDlvrInstr3Text;
	private String rcvCustPhNbr;
	private String rcvAgtCity;
	private String rcvAgtState;
	private String rcvAgtCntry;
	// New fields for Dodd Frank
	private BigDecimal rcvNonMgiFeeAmt;
	private BigDecimal rcvNonMgiTaxAmt;
	private BigDecimal rcvPayoutAmt;
	private String rcvPayoutISOCrncyId;
	private Date rcvDate;
	private TransactionScore tranScore;
	private List cmntList;
	private String internetPurchase;
	private String RRN; // receiver registration number
	private String tranRvwPrcsCode; // ("ASP"-automated,"PRE"-premiered,"MAN"-
	// manual)
	// private String tranSubReasonCode;
	private Date fundRetrievalRequestDate;
	private String customerAutoEnrollFlag;
	private String testQuestion;
	private String testQuestionAnswer;
	private String loyaltyPgmMembershipId;
	private String sndCustPhotoIdTypeCode;
	private String sndCustPhotoIdCntryCode;
	private String sndCustPhotoIdStateCode;
	private String sndCustPhotoId;
	private String sndCustLegalIdTypeCode;
	private String sndCustLegalId;
	private String sndCustOccupationText;
	private BigDecimal sndFeeAmtNoDiscountAmt;
	private String rcvCustAddrISOCntryCode;
	private String intndDestStateProvinceCode;
	private String mccPartnerProfileId;
	private String partnerSiteId;
	private Integer tranRiskScore;
	private boolean tranRiskScoreDefaulted;
	// Telecheck parameters
    private java.lang.Integer tCProviderCode;
    private java.lang.String tCProviderTransactionNumber;
    private java.lang.String tCInternalTransactionNumber;
    // Global Collect parameters
    private long tranSeqNumber;

    private String threeMinuteFreePhoneNumber;
    private String threeMinuteFreePinNumber;
    
    //Dodd-Frank Modifications
    private String mgiTransactionSessionID;
    private Date tranAvailabilityDate;
    private String regulationVersion;

    //Increase Send Limits
    private Date sndCustPhotoIdExpDate;
    
	// For requirement 4052
    private IPDetails ipDetails;
    
    private String receiptTextInfoEng;
    private String receiptTextInfoSpa;
    
    private String rcvAcctMaskNbr;
    
    private String avsResult;
    private String rsaAdaptiveAuthResult;
    
    private String guestTransactionFlag;
    
    //MBO-461 changes starts
    private float  tranAppVersionNumber;
    //MBO-461 changes ends
    
    public static String RCPT_TXT_INFO_SESSION="rcptTxtInfo";
    
	//MBO-100:Show Profile Type on EMT Admin Consumer profile Page
	private String profileType;
	
	private String tranSessionBeginId;
	//MBO-1717
	private String rcvAcctHashNbr; 
	
	private ConsumerAddress consumerAddress;
	//MBO-3920
	private String receiverEmailAddress;
	
	private String receiverEmailMessage;
	
	//MBO-5319
	private String tranLanguage;

    //MBO-6101
	private String agentId;
	private String agentUnitProfileId;
	private String agentSeqNo;
	private String agentToken;
	//ended
	//MBO6924
	private String networkTransID;
	//MBO-8239
	private String sourceDevice;
	private String sourceApp;
	
	//MBO-10467
	private String promoCode;
	private String promoDiscountType;
	private String promoDiscountAmount;
	
	public String getNetworkTransID() {
		return networkTransID;
	}

	public void setNetworkTransID(String networkTransID) {
		this.networkTransID = networkTransID;
	}
	
	public String getReceiptTextInfoEng() {
		return receiptTextInfoEng;
	}

	public void setReceiptTextInfoEng(String receiptTextInfoEng) {
		this.receiptTextInfoEng = receiptTextInfoEng;
	}

	public String getReceiptTextInfoSpa() {
		return receiptTextInfoSpa;
	}

	public void setReceiptTextInfoSpa(String receiptTextInfoSpa) {
		this.receiptTextInfoSpa = receiptTextInfoSpa;
	}

	public IPDetails getIpDetails() {
		return ipDetails;
	}

	public void setIpDetails(IPDetails ipDetails) {
		this.ipDetails = ipDetails;
	}
    
    
	public java.lang.Integer getTCProviderCode() {
		return tCProviderCode;
	}

	public void setTCProviderCode(java.lang.Integer providerCode) {
		tCProviderCode = providerCode;
	}

	public java.lang.String getTCProviderTransactionNumber() {
		return tCProviderTransactionNumber;
	}

	public void setTCProviderTransactionNumber(java.lang.String providerTransactionNumber) {
		tCProviderTransactionNumber = providerTransactionNumber;
	}

	public java.lang.String getTCInternalTransactionNumber() {
		return tCInternalTransactionNumber;
	}

	public void setTCInternalTransactionNumber(java.lang.String internalTransactionNumber) {
		tCInternalTransactionNumber = internalTransactionNumber;
	}

	public Transaction() {
	}

	public Transaction(TransactionStatus status) {
		Validate.notNull(status, "status must not be null");
		this.status = status;
	}

	/**
	 * @return
	 */
	public int getAchFileCntlSeqNbr() {
		return achFileCntlSeqNbr;
	}

	/**
	 * @return
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @return
	 */
	public String getCreateUserid() {
		return createUserid;
	}

	/**
	 * @return
	 */
	public Date getCsrPrcsDate() {
		return csrPrcsDate;
	}

	/**
	 * @return
	 */
	public String getCsrPrcsUserid() {
		return csrPrcsUserid;
	}

	/**
	 * @return
	 */
	public int getDlvrOptnId() {
		return dlvrOptnId;
	}

	/**
	 * @return
	 */
	public int getEmgTranId() {
		return emgTranId;
	}

	/**
	 * @return
	 */
	public String getEmgTranTypeCode() {
		return emgTranTypeCode;
	}

	/**
	 * @return
	 */
	public String getEmgTranTypeDesc() {
		return emgTranTypeDesc;
	}

	/**
	 * @return
	 */
	public int getFormFreeConfNbr() {
		return formFreeConfNbr;
	}

	/**
	 * @return
	 */
	public int getIntndRcvAgentId() {
		return intndRcvAgentId;
	}

	/**
	 * @return
	 */
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	/**
	 * @return
	 */
	public String getLastUpdateUserid() {
		return lastUpdateUserid;
	}

	/**
	 * @return
	 */
	public String getLgcyRefNbr() {
		return lgcyRefNbr;
	}

	/**
	 * @return
	 */
	public String getRcvAgcyCode() {
		return rcvAgcyCode;
	}

	/**
	 * @return
	 */
	public int getRcvAgentId() {
		return rcvAgentId;
	}

	/**
	 * @return
	 */
	public String getRcvCustFrstName() {
		return rcvCustFrstName == null ? "" : rcvCustFrstName;
	}

	/**
	 * @return
	 */
	public String getRcvCustLastName() {
		return rcvCustLastName == null ? "" : rcvCustLastName;
	}

	/**
	 * @return
	 */
	public String getRcvISOCntryCode() {
		return rcvISOCntryCode;
	}

	/**
	 * @return
	 */
	public String getRcvISOCrncyId() {
		return rcvISOCrncyId;
	}

	/**
	 * @return
	 */
	public String getRiskLvlCode() {
		return riskLvlCode;
	}

	public int getTransScores() {
		return transScores;
	}

	public BigDecimal getSndFeeAmtNoDiscountAmt() {
		return sndFeeAmtNoDiscountAmt;
	}

	public void setSndFeeAmtNoDiscountAmt(BigDecimal sndFeeAmtNoDiscount) {
		this.sndFeeAmtNoDiscountAmt = sndFeeAmtNoDiscount;
	}

	/**
	 * @return
	 */
	public BigDecimal getRtnFeeAmt() {
		return rtnFeeAmt;
	}

	/**
	 * @return
	 */
	public int getSndAgentId() {
		return sndAgentId;
	}

	/**
	 * @return
	 */
	public int getSndCustAcctId() {
		return sndCustAcctId;
	}

	/**
	 * @return
	 */
	public int getSndCustAcctVerNbr() {
		return sndCustAcctVerNbr;
	}

	/**
	 * @return
	 */
	public int getSndCustAddrId() {
		return sndCustAddrId;
	}

	/**
	 * @return
	 */
	public int getSndCustBkupAcctId() {
		return sndCustBkupAcctId;
	}

	/**
	 * @return
	 */
	public int getSndCustBkupAcctVerNbr() {
		return sndCustBkupAcctVerNbr;
	}

	/**
	 * @return
	 */
	public String getSndBkupAcctTypeCode() {
		return sndBkupAcctTypeCode;
	}

	/**
	 * @param string
	 */
	public void setSndBkupAcctTypeCode(String string) {
		sndBkupAcctTypeCode = string;
	}

	/**
	 * @return
	 */
	public String getSndBkupAcctMaskNbr() {
		return sndBkupAcctMaskNbr;
	}

	/**
	 * @param string
	 */
	public void setSndBkupAcctMaskNbr(String string) {
		sndBkupAcctMaskNbr = string;
	}

	/**
	 * @return
	 */
	public String getSndCustIPAddrId() {
		return sndCustIPAddrId;
	}

	/**
	 * @return
	 */
	public BigDecimal getSndFaceAmt() {
		return sndFaceAmt;
	}

	/**
	 * @return
	 */
	public BigDecimal getSndFeeAmt() {
		return sndFeeAmt;
	}

	/**
	 * @return
	 */
	public BigDecimal getSndFxCnsmrRate() {
		return sndFxCnsmrRate;
	}

	/**
	 * @return
	 */
	public String getSndISOCntryCode() {
		return sndISOCntryCode;
	}

	/**
	 * @return
	 */
	public String getSndISOCrncyId() {
		return sndISOCrncyId;
	}

	/**
	 * @return
	 */
	public String getSndMsg1Text() {
		return sndMsg1Text;
	}

	/**
	 * @return
	 */
	public String getSndMsg2Text() {
		return sndMsg2Text;
	}

	/**
	 * @return
	 */
	public BigDecimal getSndTotAmt() {
		return sndTotAmt;
	}

	/**
	 * @return
	 */
	public Date getSndTranDate() {
		return sndTranDate;
	}

	public Date getSndCustPhotoIdExpDate() {
		return sndCustPhotoIdExpDate;
	}

	public void setSndCustPhotoIdExpDate(Date sndCustPhotoIdExpDate) {
		this.sndCustPhotoIdExpDate = sndCustPhotoIdExpDate;
		
	}
	
	/**
	 * @return
	 */
	public String getTranStatCode() {
		return status.getStatusCode();
	}

	/**
	 * @return
	 */
	public Date getTranStatDate() {
		return tranStatDate;
	}

	/**
	 * @return
	 */
	public String getTranStatDesc() {
		return tranStatDesc;
	}

	/**
	 * @return
	 */
	public String getTranSubStatCode() {
		return status.getSubStatusCode();
	}

	/**
	 * @return
	 */
	public String getTranSubStatDesc() {
		return tranSubStatDesc;
	}

	/**
	 * @param i
	 */
	public void setAchFileCntlSeqNbr(int i) {
		achFileCntlSeqNbr = i;
	}

	/**
	 * @param date
	 */
	public void setCreateDate(Date date) {
		createDate = date;
	}

	/**
	 * @param string
	 */
	public void setCreateUserid(String string) {
		createUserid = string;
	}

	/**
	 * @param date
	 */
	public void setCsrPrcsDate(Date date) {
		csrPrcsDate = date;
	}

	/**
	 * @param string
	 */
	public void setCsrPrcsUserid(String string) {
		csrPrcsUserid = string;
	}

	/**
	 * @param i
	 */
	public void setDlvrOptnId(int i) {
		dlvrOptnId = i;
	}

	/**
	 * @param i
	 */
	public void setEmgTranId(int i) {
		emgTranId = i;
	}

	/**
	 * @param string
	 */
	public void setEmgTranTypeCode(String string) {
		emgTranTypeCode = string;
	}

	/**
	 * @param string
	 */
	public void setEmgTranTypeDesc(String string) {
		emgTranTypeDesc = string;
	}

	/**
	 * @param i
	 */
	public void setFormFreeConfNbr(int i) {
		formFreeConfNbr = i;
	}

	/**
	 * @param i
	 */
	public void setIntndRcvAgentId(int i) {
		intndRcvAgentId = i;
	}

	/**
	 * @param date
	 */
	public void setLastUpdateDate(Date date) {
		lastUpdateDate = date;
	}

	/**
	 * @param string
	 */
	public void setLastUpdateUserid(String string) {
		lastUpdateUserid = string;
	}

	/**
	 * @param string
	 */
	public void setLgcyRefNbr(String string) {
		lgcyRefNbr = string;
	}

	/**
	 * @param string
	 */
	public void setRcvAgcyCode(String string) {
		rcvAgcyCode = string;
	}

	/**
	 * @param i
	 */
	public void setRcvAgentId(int i) {
		rcvAgentId = i;
	}

	/**
	 * @param string
	 */
	public void setRcvCustFrstName(String string) {
		rcvCustFrstName = string;
	}

	/**
	 * @param string
	 */
	public void setRcvCustLastName(String string) {
		rcvCustLastName = string;
	}

	/**
	 * @param string
	 */
	public void setRcvISOCntryCode(String string) {
		rcvISOCntryCode = string;
	}

	/**
	 * @param string
	 */
	public void setRcvISOCrncyId(String string) {
		rcvISOCrncyId = string;
	}

	/**
	 * @param string
	 */
	public void setRiskLvlCode(String string) {
		riskLvlCode = string;
	}

	public void setTransScores(int i) {
		transScores = i;
	}

	/**
	 * @param decimal
	 */
	public void setRtnFeeAmt(BigDecimal decimal) {
		rtnFeeAmt = decimal;
	}

	/**
	 * @param i
	 */
	public void setSndAgentId(int i) {
		sndAgentId = i;
	}

	/**
	 * @param i
	 */
	public void setSndCustAcctId(int i) {
		sndCustAcctId = i;
	}

	/**
	 * @param i
	 */
	public void setSndCustAcctVerNbr(int i) {
		sndCustAcctVerNbr = i;
	}

	/**
	 * @param i
	 */
	public void setSndCustAddrId(int i) {
		sndCustAddrId = i;
	}

	/**
	 * @param i
	 */
	public void setSndCustBkupAcctId(int i) {
		sndCustBkupAcctId = i;
	}

	/**
	 * @param i
	 */
	public void setSndCustBkupAcctVerNbr(int i) {
		sndCustBkupAcctVerNbr = i;
	}

	/**
	 * @param string
	 */
	public void setSndCustIPAddrId(String string) {
		sndCustIPAddrId = string;
	}

	/**
	 * @param decimal
	 */
	public void setSndFaceAmt(BigDecimal decimal) {
		sndFaceAmt = decimal;
	}

	/**
	 * @param decimal
	 */
	public void setSndFeeAmt(BigDecimal decimal) {
		sndFeeAmt = decimal;
	}

	/**
	 * @param decimal
	 */
	public void setSndFxCnsmrRate(BigDecimal decimal) {
		sndFxCnsmrRate = decimal;
	}

	/**
	 * @param string
	 */
	public void setSndISOCntryCode(String string) {
		sndISOCntryCode = string;
	}

	/**
	 * @param string
	 */
	public void setSndISOCrncyId(String string) {
		sndISOCrncyId = string;
	}

	/**
	 * @param string
	 */
	public void setSndMsg1Text(String string) {
		sndMsg1Text = string;
	}

	/**
	 * @param string
	 */
	public void setSndMsg2Text(String string) {
		sndMsg2Text = string;
	}

	/**
	 * @param decimal
	 */
	public void setSndTotAmt(BigDecimal decimal) {
		sndTotAmt = decimal;
	}

	/**
	 * @param date
	 */
	public void setSndTranDate(Date date) {
		sndTranDate = date;
	}

	/**
	 * @param string
	 */
	public void setTranStatCode(String string) {
		status = TransactionStatus.getInstance(string, status.getSubStatusCode());
	}

	/**
	 * @param date
	 */
	public void setTranStatDate(Date date) {
		tranStatDate = date;
	}

	/**
	 * @param string
	 */
	public void setTranStatDesc(String string) {
		tranStatDesc = string;
	}

	/**
	 * @param string
	 */
	public void setTranSubStatCode(String string) {
		status = TransactionStatus.getInstance(status.getStatusCode(), string);
	}

	/**
	 * @param string
	 */
	public void setTranSubStatDesc(String string) {
		tranSubStatDesc = string;
	}

	/**
	 * @return
	 */
	public String getSndCustFrstName() {
		return sndCustFrstName == null ? "" : sndCustFrstName;
	}

	/**
	 * @return
	 */
	public String getSndCustLastName() {
		return sndCustLastName == null ? "" : sndCustLastName;
	}

	/**
	 * @param string
	 */
	public void setSndCustFrstName(String string) {
		sndCustFrstName = string;
	}

	/**
	 * @param string
	 */
	public void setSndCustLastName(String string) {
		sndCustLastName = string;
	}

	/**
	 * @return
	 */
	public String getSndAcctTypeCode() {
		return sndAcctTypeCode;
	}

	/**
	 * @param string
	 */
	public void setSndAcctTypeCode(String string) {
		sndAcctTypeCode = string;
	}

	/**
	 * @return
	 */
	public String getSndAcctMaskNbr() {
		return sndAcctMaskNbr;
	}

	/**
	 * @param string
	 */
	public void setSndAcctMaskNbr(String string) {
		sndAcctMaskNbr = string;
	}

	/**
	 * @return
	 */
	public int getCustId() {
		return sndCustId;
	}

	/**
	 * @param i
	 */
	public void setCustId(int i) {
		sndCustId = i;
	}

	/**
	 * @return
	 */
	public String getRcvAgentConfId() {
		return rcvAgentConfId;
	}

	/**
	 * @param string
	 */
	public void setRcvAgentConfId(String string) {
		rcvAgentConfId = string;
	}

	public boolean isRefundable() {
		return status.isRefundable();
	}

	public boolean isFundable() {
		return status.isFundable();
	}

	public boolean isSendable() {
		return status.isSendable();
	}

	public boolean isManualAdjustable() {
		return status.isManualAdjustable();
	}

	public TransactionStatus getStatus() {
		return status;
	}

	public void setStatus(TransactionStatus status) {
		Validate.notNull(status, "status must not be null");
		this.status = status;
	}

	/**
	 * @return
	 */
	public String getBankAbaNbr() {
		return bankAbaNbr;
	}

	/**
	 * @return
	 */
	public String getSndCustLogonId() {
		return sndCustLogonId;
	}

	/**
	 * @return
	 */
	public String getSndCustSsnMaskNbr() {
		return sndCustSsnMaskNbr;
	}

	/**
	 * @param string
	 */
	public void setBankAbaNbr(String string) {
		bankAbaNbr = string;
	}

	/**
	 * @param string
	 */
	public void setSndCustLogonId(String string) {
		sndCustLogonId = string;
	}

	/**
	 * @param string
	 */
	public void setSndCustSsnMaskNbr(String string) {
		sndCustSsnMaskNbr = string;
	}

	/**
	 * @return
	 */
	public String getRcvAgentName() {
		return rcvAgentName;
	}

	/**
	 * @param string
	 */
	public void setRcvAgentName(String string) {
		rcvAgentName = string;
	}

	/**
	 * @return
	 */
	public String getRcvAgcyAcctMask() {
		return rcvAgcyAcctMask;
	}

	/**
	 * @param string
	 */
	public void setRcvAgcyAcctMask(String string) {
		rcvAgcyAcctMask = string;
	}

	/**
	 * @return
	 */
	public String getRcvAgcyAcctEncryp() {
		return rcvAgcyAcctEncryp;
	}

	/**
	 * @param string
	 */
	public void setRcvAgcyAcctEncryp(String string) {
		rcvAgcyAcctEncryp = string;
	}

	/**
	 * @return
	 */
	public String getEsSendable() {
		return esSendable;
	}

	/**
	 * @param s
	 */
	public void setEsSendable(String s) {
		esSendable = s;
	}

	/**
	 * @return
	 */
	public String getRcvCustMatrnlName() {
		return rcvCustMatrnlName;
	}

	/**
	 * @return
	 */
	public String getRcvCustPhNbr() {
		return rcvCustPhNbr;
	}

	/**
	 * @param string
	 */
	public void setRcvCustMatrnlName(String string) {
		rcvCustMatrnlName = string;
	}

	/**
	 * @param string
	 */
	public void setRcvCustPhNbr(String string) {
		rcvCustPhNbr = string;
	}

	/**
	 * @return
	 */
	public String getRcvCustMidName() {
		return rcvCustMidName;
	}

	/**
	 * @param string
	 */
	public void setRcvCustMidName(String string) {
		rcvCustMidName = string;
	}

	public String getRcvCustAddrStateName() {
		return rcvCustAddrStateName;
	}

	public void setRcvCustAddrStateName(String string) {
		rcvCustAddrStateName = string;
	}

	public String getRcvAgentRefNbr() {
		return rcvAgentRefNbr;
	}

	public void setRcvAgentRefNbr(String string) {
		rcvAgentRefNbr = string;
	}

	public String getRcvCustAddrLine1Text() {
		return rcvCustAddrLine1Text;
	}

	public void setRcvCustAddrLine1Text(String string) {
		rcvCustAddrLine1Text = string;
	}

	public String getRcvCustAddrLine2Text() {
		return rcvCustAddrLine2Text;
	}

	public void setRcvCustAddrLine2Text(String string) {
		rcvCustAddrLine2Text = string;
	}

	public String getRcvCustAddrLine3Text() {
		return rcvCustAddrLine3Text;
	}

	public void setRcvCustAddrLine3Text(String string) {
		rcvCustAddrLine3Text = string;
	}

	public String getRcvCustAddrCityName() {
		return rcvCustAddrCityName;
	}

	public void setRcvCustAddrCityName(String string) {
		rcvCustAddrCityName = string;
	}

	public String getRcvCustAddrPostalCode() {
		return rcvCustAddrPostalCode;
	}

	public void setRcvCustAddrPostalCode(String string) {
		rcvCustAddrPostalCode = string;
	}

	public String getRcvCustDlvrInstr1Text() {
		return rcvCustDlvrInstr1Text;
	}

	public void setRcvCustDlvrInstr1Text(String string) {
		rcvCustDlvrInstr1Text = string;
	}

	public String getRcvCustDlvrInstr2Text() {
		return rcvCustDlvrInstr2Text;
	}

	public void setRcvCustDlvrInstr2Text(String string) {
		rcvCustDlvrInstr2Text = string;
	}

	public String getRcvCustDlvrInstr3Text() {
		return rcvCustDlvrInstr3Text;
	}

	public void setRcvCustDlvrInstr3Text(String string) {
		rcvCustDlvrInstr3Text = string;
	}

	public int getSndCustId() {
		return sndCustId;
	}

	public void setSndCustId(int i) {
		sndCustId = i;
	}

	public String getRcvAgtCity() {
		return rcvAgtCity;
	}

	public void setRcvAgtCity(String string) {
		rcvAgtCity = string;
	}

	public boolean getRcvStateMatchesSendState() {
		if ((this.getRcvAgtState() != null) && (this.getRcvCustAddrStateName() != null)) {
			if (this.getRcvAgtState().equalsIgnoreCase(this.getRcvCustAddrStateName()))
				return true;
			else
				return false;
		} else
			return true;
	}

	public String getRcvAgtState() {
		return rcvAgtState;
	}

	public void setRcvAgtState(String string) {
		rcvAgtState = string;
	}

	public String getRcvAgtCntry() {
		return rcvAgtCntry;
	}

	public void setRcvAgtCntry(String string) {
		rcvAgtCntry = string;
	}

	public Date getRcvDate() {
		return rcvDate;
	}

	public void setRcvDate(Date date) {
		rcvDate = date;
	}

	public String getSysAutoRsltCode() {
		return sysAutoRsltCode;
	}

	public void setSysAutoRsltCode(String string) {
		sysAutoRsltCode = string;
	}

	public TransactionScore getTranScore() {
		return tranScore;
	}

	public void setTranScore(TransactionScore score) {
		tranScore = score;
	}

	public BigDecimal getSndThrldWarnAmt() {
		return sndThrldWarnAmt == null ? new BigDecimal(0) : sndThrldWarnAmt;
	}

	public void setSndThrldWarnAmt(BigDecimal decimal) {
		sndThrldWarnAmt = decimal;
	}

	public List getCmntList() {
		return cmntList;
	}

	public void setCmntList(List list) {
		cmntList = list;
	}

	public boolean[] getCommandFlags(String esSendMG, BigDecimal recoveredAmount) {
		boolean[] isFlags = { false, // 0
				false, // 1
				false, // 2
				false, // 3
				false, // 4
				false, // 5
				false, // 6
				false, // 7
				false, // 8
				false, // 9
				false, // 10
				false, // 11
				false, // 12
				false, // 13
				false, // 14
				false, // 15
				false, // 16
				false, // 17
				false, // 18
				false, // 19
				false, // 20
				false, // 21
				false, // 22
				false, // 23
				false, // 24
				false, // 25
				false, // 26
				false, // 27
				false, // 28
				false, // 29
				false, // 30
				false, // 31
				false, // 32
				false, // 33
				false, // 34
				false  // 35
				,false // 36
				,false // 37
				,false // 38
				,false // 39
				,false // 40
				,false // 41
				,false // 42
				,false // 43
				,false // 44
				,false // 45
				,false // 46 testing
				, false // 47 = Undo DS Error
				, false // 48 = Manual CC Refund
				, false // 49 = Manual CC Void
				, false // 50 = Regenerate Email
				, false // 51 = Auto Exception Write Off Loss
				, false // 52 = Manual Exception Write Off Loss
				, false // 53 = Cancel Transaction
				, false // 54 = Manual Cancel Transaction
				, false // 55 = Move to Review 
				};
		// authorization action table
		int[] actionIds = { 1, // 0
				2, // 1
				3, // 2
				4, // 3
				5, // 4
				6, // 5
				11, // 6
				21, // 7
				30, // 8
				31, // 9
				38, // 10
				40, // 11
				41, // 12
				43, // 13
				44, // 14
				48, // 15
				51, // 16
				52, // 17
				53, // 18
				54, // 19
				55, // 20
				56, // 21
				61, // 22
				62, // 23
				63, // 24
				64, // 25
				71, // 26
				81, // 27
				82, // 28
				83, // 29
				84, // 30
				85, // 31
				86, // 32
				87, // 33
				88, // 34
				89, // 35
				160 // 36
				,161 //37
				,162 //38
				,163 //39
				,164 //40
				,165 //41
				,166 //42
				,167 //43
				,168 //44
				,169 //45
				,170 // 46 testing
				,171 // 47 = Undo DS Error
				,172 // 48 = Manual CC Refund
				,173 // 49 = Manual CC Void
				,174 // 50 = Regenerate Email
				,175 // 51 = Auto Exception Write Off Loss
				,176 // 52 = Manual Exception Write Off Loss
				,177 // 53 = Cancel Transaction
				,178 // 54 = Manual Cancel Transaction
				,179 // 53 = Move to Review
		       };
		// transaction status table
		String[] ts = { "   ", "FFS", // 1
				"APP", // 2
				"DEN", // 3
				"SEN", // 4
				"ERR", // 5
				"CXL", // 6
				"REF", // 7
				"PEN", // 8
				"MAN", //9
				"OCR"}; // 10
		// transaction sub-status (fund) table
		String[] fs = { "   ", "NOF", // 1
				"UNC", // 2
				"ACW", // 3
				"ACS", // 4
				"ACR", // 5
				"ACE", // 6
				"CCF", // 7
				"CCA", // 8
				"CCS", // 9
				"CCR", // 10
				"CCC", // 11
				"CRE", // 12
				"ACC", // 13
				"ARE", // 14
				"AIP", // 15
				"AIE", // 16
				"CRA", // 17
				"COL", // 18
				"COR", // 19
				"ARS", // 20
				"ANF", // 21
				"MAN", // 22
				"ARR", // 23
				"CRR", // 24
				"WOL", // 25
				"ROL", // 26
				"MCE", // 27
				"VBV", //28
				"PRE", // 29
				"RTP", // 30
				"RBW", // 31
				"RTS"};// 32
		String statCode = status.getStatusCode();
		if (statCode == null) {
			statCode = "";
		}
		String fundCode = status.getSubStatusCode();
		if (fundCode == null) {
			fundCode = "";
		}

		DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
		boolean sameDay = isSameDay();
		boolean canAutomatAch = false;
		boolean canAutomatCc = false;
		boolean canAutomatBank = false;
		//FIXME S26-If txn is NSF, latest stat date may not be when txn was sent.
		//Should really find the date of the 'SEN' to MF action...
		//For now, only allow auto cancel if txn create date = current date
			if (TransactionStatus.SENT_CODE.equalsIgnoreCase(statCode)
					&& (fundCode.equalsIgnoreCase(TransactionStatus.ACH_WAITING_CODE) || fundCode
							.equalsIgnoreCase(TransactionStatus.ACH_SENT_CODE))) {
				canAutomatAch = true;
			}
			if (TransactionStatus.SENT_CODE.equalsIgnoreCase(statCode) && fundCode.equalsIgnoreCase(TransactionStatus.CC_SALE_CODE) && TransactionType.EXPRESS_PAYMENT_SEND_CODE.equalsIgnoreCase(this.emgTranTypeCode) ) {
				canAutomatCc = true;
			}
			if ((TransactionType.DELAY_SEND_CODE.equalsIgnoreCase(this.emgTranTypeCode) || TransactionType.EXPRESS_PAYMENT_SEND_CODE.equalsIgnoreCase(this.emgTranTypeCode))) {
				//handle Walmart 'ONLY_AT' option
				if (TransactionStatus.SENT_CODE.equalsIgnoreCase(statCode) && 
						(fundCode.equalsIgnoreCase(TransactionStatus.BANK_SETTLED_CODE) || fundCode.equalsIgnoreCase(TransactionStatus.BANK_REJECTED_CODE))) {
					canAutomatBank = true;
				}
			}

		

		//Oct2011 - converted some more common ones to constants instead of having to reference arrays...
		for (int i = 0; i < isFlags.length; i++) {
			switch (actionIds[i]) {
			case 1:
			{
				boolean correctStatus = (statCode.equalsIgnoreCase(TransactionStatus.FORM_FREE_SEND_CODE) || statCode.equalsIgnoreCase(TransactionStatus.PENDING_CODE));
				boolean correctSubStatus = fundCode.equalsIgnoreCase(TransactionStatus.NOT_FUNDED_CODE);
				boolean correctTransactionType = emgTranTypeCode.equalsIgnoreCase(TransactionType.DELAY_SEND_CODE);
				if (correctStatus && correctSubStatus && correctTransactionType) {
					isFlags[i] = true;
				}
				break;
			}
			case 2: // 1
				if ((statCode.equalsIgnoreCase(TransactionStatus.FORM_FREE_SEND_CODE) || statCode.equalsIgnoreCase(TransactionStatus.PENDING_CODE))
						&& (fundCode.equalsIgnoreCase(TransactionStatus.NOT_FUNDED_CODE))) {
					isFlags[i] = true;
				}
				break;
			case 3: // 2
				if (statCode.equalsIgnoreCase(TransactionStatus.APPROVED_CODE) && (fundCode.equalsIgnoreCase(TransactionStatus.NOT_FUNDED_CODE))) {
					isFlags[i] = true;
				}
				break;
			case 4: // 3
				if (statCode.equalsIgnoreCase(TransactionStatus.DENIED_CODE) && fundCode.equalsIgnoreCase(TransactionStatus.NOT_FUNDED_CODE)) {
					isFlags[i] = true;
				}
				break;
			case 5: // 4
				if (statCode.equalsIgnoreCase(TransactionStatus.FORM_FREE_SEND_CODE) && fundCode.equalsIgnoreCase(TransactionStatus.NOT_FUNDED_CODE)) {
					isFlags[i] = true;
				}
				break;
			case 6: // 5
				if ((statCode.equalsIgnoreCase(TransactionStatus.APPROVED_CODE) && fundCode.equalsIgnoreCase(TransactionStatus.NOT_FUNDED_CODE))
						|| (statCode.equalsIgnoreCase(TransactionStatus.APPROVED_CODE) && fundCode.equalsIgnoreCase(TransactionStatus.BANK_SETTLED_CODE))
						|| (statCode.equalsIgnoreCase(TransactionStatus.APPROVED_CODE) && fundCode.equalsIgnoreCase(TransactionStatus.CC_AUTH_CODE))
						|| (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE) && fundCode.equalsIgnoreCase(TransactionStatus.CC_AUTH_CODE))) {
					isFlags[i] = true;
				}
				break;
			case 11: // 6
			case 12:
			case 13:
				if (!this.emgTranTypeCode.equals(TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE) && statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE)
						&& (fundCode.equalsIgnoreCase(fs[5]) || fundCode.equalsIgnoreCase(fs[16]))) {
					isFlags[i] = true;
				}
				break;
			case 21: // 7
			case 22:
			case 23:
				if ((this.emgTranTypeCode.equals(TransactionType.MONEY_TRANSFER_SEND_CODE) || this.emgTranTypeCode
						.equals(TransactionType.EXPRESS_PAYMENT_SEND_CODE))
						&& statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE)
						&& (fundCode.equalsIgnoreCase(fs[5]) || fundCode.equalsIgnoreCase(fs[6]) || fundCode.equalsIgnoreCase(fs[16]))) {
					isFlags[i] = true;
				}
				break;
			case 30: // 8
				/**
				 * AUS-948 : To hide 'Automatic ACH Cancel/Refund' button on BPAY transaction so that CSR does not use this button for canceling
				 * BPAY transactions in SEN/ACS status.
				 */
				//if (canAutomatAch) {
					isFlags[i] = false;
				//}
				break;
			case 31: // 9
			case 32:
				if (!canAutomatAch
						&& (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE) || statCode.equalsIgnoreCase(TransactionStatus.ERROR_CODE) ||
							statCode.equalsIgnoreCase(TransactionStatus.CANCELED_CODE))
						&& fundCode.equalsIgnoreCase(fs[4])) {
					isFlags[i] = false; //https://mgonline.atlassian.net/browse/AUS-624
				}
				break;
			case 38: // 10 //https://mgonline.atlassian.net/browse/AUS-625 Point 2:  change fs[]23 to TransactionStatus.ACH_SENT_CODE
				if ((statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE) || statCode.equalsIgnoreCase(TransactionStatus.ERROR_CODE)
						|| statCode.equalsIgnoreCase(TransactionStatus.CANCELED_CODE))
						&& fundCode.equalsIgnoreCase(TransactionStatus.ACH_SENT_CODE)) {
					isFlags[i] = true;
				}
				break;
			//FD-213
				/*case 40: // 11
				if (canAutomatCc) {
					isFlags[i] = true;
				}
				break;
				case 41: // 12
			case 42:
				if (TransactionType.EXPRESS_PAYMENT_SEND_CODE.equalsIgnoreCase(this.emgTranTypeCode) && (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE) || statCode.equalsIgnoreCase(TransactionStatus.ERROR_CODE)
							|| statCode.equalsIgnoreCase(TransactionStatus.CANCELED_CODE))
						&& fundCode.equalsIgnoreCase(TransactionStatus.CC_SALE_CODE)) {
					isFlags[i] = true;
				}
				break;*/
			case 43: // 13 //https://mgonline.atlassian.net/browse/AUS-624
				if ((statCode.equalsIgnoreCase(TransactionStatus.APPROVED_CODE) || statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE)
						|| statCode.equalsIgnoreCase(TransactionStatus.ERROR_CODE))
					&& fundCode.equalsIgnoreCase(fs[4])) {
					isFlags[i] = false;
				}
				break;
			case 44: // 14 
				if (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE)
						&& ((fundCode.equalsIgnoreCase(TransactionStatus.CC_SALE_CODE)) || (fundCode.equalsIgnoreCase(TransactionStatus.WRITE_OFF_LOSS_CODE))
							 || (fundCode.equalsIgnoreCase(TransactionStatus.RECOVERY_OF_LOSS_CODE)))) {
					isFlags[i] = true;
				}
				break;
			case 48: // 15
				if (statCode.equalsIgnoreCase(TransactionStatus.CANCELED_CODE) && fundCode.equalsIgnoreCase(fs[24])) {
					isFlags[i] = true;
				}
				break;
			case 51: // 16
				if (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE)
						&& (fundCode.equalsIgnoreCase(TransactionStatus.NOT_FUNDED_CODE) || fundCode.equalsIgnoreCase(fs[5]) || fundCode.equalsIgnoreCase(fs[6])
								|| fundCode.equalsIgnoreCase(fs[7]) || fundCode.equalsIgnoreCase(TransactionStatus.CC_CHARGEBACK_CODE)
								|| fundCode.equalsIgnoreCase(fs[11]) || fundCode.equalsIgnoreCase(fs[13]) || fundCode
								.equalsIgnoreCase(fs[16]))) {
					isFlags[i] = true;
				}
				break;
			case 52: // 17 To hide the "Manual Return Fee" button removed "fundCode.equalsIgnoreCase(fs[13]) ||"
				if ((statCode.equalsIgnoreCase(TransactionStatus.ERROR_CODE) || statCode.equalsIgnoreCase(TransactionStatus.CANCELED_CODE))
						&& (fundCode.equalsIgnoreCase(TransactionStatus.NOT_FUNDED_CODE) || fundCode.equalsIgnoreCase(fs[5]) || fundCode.equalsIgnoreCase(fs[6])
								|| fundCode.equalsIgnoreCase(fs[7]) || fundCode.equalsIgnoreCase(TransactionStatus.CC_CHARGEBACK_CODE)
								|| fundCode.equalsIgnoreCase(fs[11])|| fundCode
								.equalsIgnoreCase(fs[16]))) {
					isFlags[i] = true;
				}
				break;
			case 53: // 18 https://mgonline.atlassian.net/browse/AUS-624 removed "fundCode.equalsIgnoreCase(fs[4]) ||" for displayed for SEN/CCS
				if (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE) && (fundCode.equalsIgnoreCase(TransactionStatus.CC_SALE_CODE))) {
					isFlags[i] = true;
				}
				break;
			case 54: // 19
				if ((statCode.equalsIgnoreCase(TransactionStatus.ERROR_CODE) || statCode.equalsIgnoreCase(TransactionStatus.CANCELED_CODE))
						&& (fundCode.equalsIgnoreCase(fs[4]) || fundCode.equalsIgnoreCase(TransactionStatus.CC_SALE_CODE))) {
					isFlags[i] = true;
				}
				break;
			case 55: // 20
				if (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE) && (fundCode.equalsIgnoreCase(fs[5]) || fundCode.equalsIgnoreCase(TransactionStatus.CC_CHARGEBACK_CODE))) {
					isFlags[i] = true;
				}
				break;
			case 56: // 21 https://mgonline.atlassian.net/browse/AUS-624 removed "fundCode.equalsIgnoreCase(fs[4]) ||" so that the button will only be displayed for SEN/CCS now
				if (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE) && (fundCode.equalsIgnoreCase(TransactionStatus.CC_SALE_CODE))) {
					isFlags[i] = true;
				}
				break;
			case 61: // 22
			case 62: // 23
			case 63: // 24
			case 64: // 25
				if (statCode.equalsIgnoreCase(TransactionStatus.MANUAL_CODE) && fundCode.equalsIgnoreCase(fs[22])) {
					isFlags[i] = true;
				}
				break;
			case 71: // 26 	https://mgonline.atlassian.net/browse/AUS-624 remove "fundCode.equalsIgnoreCase(fs[4]) ||" so that the button will only be displayed for SEN/CCS, SEN/CCF, SEN/CCC, SEN/CCR now 
				if (!canAutomatAch
						&& !canAutomatCc
						&& statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE)
						&& (fundCode.equalsIgnoreCase(fs[7]) || fundCode.equalsIgnoreCase(TransactionStatus.CC_SALE_CODE)
								|| fundCode.equalsIgnoreCase(TransactionStatus.CC_CHARGEBACK_CODE) || fundCode.equalsIgnoreCase(fs[11]) || fundCode
								.equalsIgnoreCase(fs[13]))) {
					isFlags[i] = true;
				}
				break;
			case 81: // 27
				/*if ((statCode.equalsIgnoreCase(TransactionStatus.APPROVED_CODE) || statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE)) && fundCode.equalsIgnoreCase(fs[3])) {
					isFlags[i] = true; 
				}
				break;*/
			case 82: // 28
				if (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE) && fundCode.equalsIgnoreCase(TransactionStatus.WRITE_OFF_LOSS_CODE)) {
					isFlags[i] = true;
				}
				break;
			case 83: // 29
				isFlags[i] = true;
				break;
			case 84: // 30
				String tmpFlag = esSendMG;
				esSendMG = "X";
				if (statCode.equalsIgnoreCase(TransactionStatus.APPROVED_CODE) && fundCode.equalsIgnoreCase(fs[4])) {
					isFlags[i] = true;
					esSendMG = tmpFlag;
				}
				break;
			case 85: // 31 Code modified to change the transaction status from APP/ACS to CXL/AXR  
					//https://mgonline.atlassian.net/browse/AUS-623 falg is made false To hide the "Cancel and Request Refund" button
				if ( statCode.equalsIgnoreCase(TransactionStatus.APPROVED_CODE) && fundCode.equalsIgnoreCase(TransactionStatus.ACH_SENT_CODE)) {
					isFlags[i] =  false;
				}
				break;
			case 86: // 32
				if (statCode.equalsIgnoreCase(TransactionStatus.APPROVED_CODE) && fundCode.equalsIgnoreCase(TransactionStatus.CC_AUTH_CODE)) {
					isFlags[i] = true;
				}
				break;
			case 87: // 33
				if (statCode.equalsIgnoreCase(TransactionStatus.FORM_FREE_SEND_CODE) && fundCode.equalsIgnoreCase(fs[28])) {
					isFlags[i] = true;
				}
				break;
			case 88: // 34
				if (StringHelper.isNullOrEmpty(sysAutoRsltCode)
						&& (statCode.equalsIgnoreCase(TransactionStatus.FORM_FREE_SEND_CODE) || statCode.equalsIgnoreCase(TransactionStatus.APPROVED_CODE)
							 || statCode.equalsIgnoreCase(TransactionStatus.PENDING_CODE))
						&& (fundCode.equalsIgnoreCase(TransactionStatus.NOT_FUNDED_CODE))) {
					isFlags[i] = true;
				}
				break;
			case 89: // 35  Approve & Process
				if ((statCode.equalsIgnoreCase(TransactionStatus.FORM_FREE_SEND_CODE) || statCode.equalsIgnoreCase(TransactionStatus.PENDING_CODE))
					&& fundCode.equalsIgnoreCase(TransactionStatus.NOT_FUNDED_CODE)
					&& !TransactionType.DELAY_SEND_CODE.equalsIgnoreCase(this.getEmgTranTypeCode())) {
					isFlags[i] = true;
				}
				break;
			case 160: // 160
				if (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE)) {
					if ((this.sndBkupAcctTypeCode != null) && (this.sndBkupAcctTypeCode.startsWith("CC")))
						isFlags[i] = true;
					else if ((this.sndAcctTypeCode != null) && (this.sndAcctTypeCode.startsWith("CC")))
						isFlags[i] = true;
				}
				break;
				
			case 161: // 161 - cancelBankRequestRefund - status SEN/BKS or SEN/BKR and NOT same day
				if (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE)
					&& (fundCode.equalsIgnoreCase(TransactionStatus.BANK_SETTLED_CODE)
						 || fundCode.equalsIgnoreCase(TransactionStatus.BANK_REJECTED_CODE))) {
				    isFlags[i] = true;
				}
				break;
			case 162: // 162 - BankRqstRefund - status APP/BKS
				if (statCode.equalsIgnoreCase(TransactionStatus.APPROVED_CODE)
					&& fundCode.equalsIgnoreCase(TransactionStatus.BANK_SETTLED_CODE)) {
				    isFlags[i] = true;
				}
				break;
			case 163: // 163 - autoCancelBankRqstRefund - status SEN/BKS or SEN/BKR and same day
				if (canAutomatBank && statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE)
						&& (fundCode.equalsIgnoreCase(TransactionStatus.BANK_SETTLED_CODE)
							 || fundCode.equalsIgnoreCase(TransactionStatus.BANK_REJECTED_CODE))) {
					    isFlags[i] = true;
				}
				break;
			case 164: // 164 - bankRepresentRejDebit - status SEN/BKR or CXL/BKR
				if (fundCode.equalsIgnoreCase(TransactionStatus.BANK_REJECTED_CODE)
						&& (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE)
							 || statCode.equalsIgnoreCase(TransactionStatus.CANCELED_CODE))) {
					    isFlags[i] = true;
				}
				break;
			case 165: // 165 - bankRejectedDebit - status SEN/BKS or CXL/BRR
				if ( (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE) &&
					  fundCode.equalsIgnoreCase(TransactionStatus.BANK_SETTLED_CODE))
					||
					 (statCode.equalsIgnoreCase(TransactionStatus.CANCELED_CODE) &&
					  fundCode.equalsIgnoreCase(TransactionStatus.BANK_REFUND_REQUEST_CODE))
					  ||
					  (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE) &&
							  fundCode.equalsIgnoreCase(TransactionStatus.ACH_SENT_CODE))) {
						isFlags[i] = true;
				}
				break;
			case 166: // 166 - bankRecordVoid - status CXL/BRR
				if (statCode.equalsIgnoreCase(TransactionStatus.CANCELED_CODE) &&
					fundCode.equalsIgnoreCase(TransactionStatus.BANK_REFUND_REQUEST_CODE)) {
						isFlags[i] = true;
				}
				break;
			case 167: // 167 - bankIssueRefund - status CXL/BRR
				if (statCode.equalsIgnoreCase(TransactionStatus.CANCELED_CODE) &&
   				    this.status.getSubStatusCode().equalsIgnoreCase(TransactionStatus.BANK_REFUND_REQUEST_CODE)) {
						isFlags[i] = true;
				}
				break;
			case 168: // 168 - bankWriteOffLoss - status SEN/BKR
				      //SOF-162 : Adding RTW to display the bankWriteOffLoss button.
				if (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE) &&(
	   				    this.status.getSubStatusCode().equalsIgnoreCase(TransactionStatus.BANK_REJECTED_CODE)||(TransactionStatus.RTBANK_WAITING.equalsIgnoreCase(this.status.getSubStatusCode())
	   				    		||(TransactionStatus.ACH_RETURNED_CODE.equalsIgnoreCase(this.status.getSubStatusCode()))))) {
						isFlags[i] = true;
				}
				break;
			case 169: // 169 - bankRecoverLoss - status SEN/BWL
				if (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE) &&
					fundCode.equalsIgnoreCase(TransactionStatus.BANK_WRITE_OFFLOSS_CODE)) {
						isFlags[i] = true;
				}
				break;
			case 170:
				// 170 - ccPartialRefund
				// not same day transactions
				// only MGSEND transactions
				// MGO US, UK and WAP (handled from role association to commands)
				// status APP / CCF || APP / CCA  || SEN / CCA || SEN / CCS || SEN / CCF || CXL / CCS || CXL / CRR || CXL / CCC

				if (canShowPartialRefundButton()){
						isFlags[i] = true;
				}
				break;
				
		      case 171: // 171 - **Placeholder for undoDSError button**
		          if (statCode.equalsIgnoreCase(TransactionStatus.ERROR_CODE) &&
		                  fundCode.equalsIgnoreCase(TransactionStatus.BANK_SETTLED_CODE)) {
		                    isFlags[i] = true;
		                }

		        break;
		      
		      case 172: // 172 - ccManualRefund - status CXL/CRR  
		    	  if (statCode.equalsIgnoreCase(TransactionStatus.CANCELED_CODE) &&
		    	          fundCode.equalsIgnoreCase(TransactionStatus.CC_REFUND_REQUESTED_CODE)) {
		    	            isFlags[i] = true;
		    	        }
		    	        break;
		    	        

		      
		      case 173: // 173 - ccManualVoid - status CXL/CVR
		          if (statCode.equalsIgnoreCase(TransactionStatus.CANCELED_CODE) &&
		            fundCode.equalsIgnoreCase(TransactionStatus.CC_VOID_CODE)) {
		              isFlags[i] = true;
		          }
		          break;
		          
		      case 174: // 174 - regenerateEmail - status SEN or CXL with ref#
		          if (this.getLgcyRefNbr() != null) {
		              isFlags[i] = true;
		          }
		          break;
		      case 175: // 51 - autoExceptionWOL - status SEN and fundstatus = CCS
		    	  if (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE) && fundCode.equalsIgnoreCase(TransactionStatus.CC_SALE_CODE)) {
		    	    isFlags[i] = true;
		    	  }
		    	  break;
		      case 176: // 52 - manualExceptionWOL - status SEN and fundstatus = CCS
		    	  if (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE) && fundCode.equalsIgnoreCase(TransactionStatus.CC_SALE_CODE)) {
		    	    isFlags[i] = true;
		    	  }
		    	  break; 
		      case 177: //53 // Button - Cancel Transaction, Tran type not DSSEND(MBO-13421) - status APP and fundstatus - PRE/ACS 
		    	  if (!TransactionType.DELAY_SEND_CODE.equalsIgnoreCase(this.emgTranTypeCode) && (statCode.equalsIgnoreCase(TransactionStatus.APPROVED_CODE)  
		    			  && (fundCode.equalsIgnoreCase(TransactionStatus.PREPARE_FUNDING) || fundCode.equalsIgnoreCase(TransactionStatus.ACH_SENT_CODE)
		    			  // APP status & sub-status RTP,PRE,RBW,RTS
		    			  || fundCode.equalsIgnoreCase(TransactionStatus.READY_TO_PAY) || fundCode.equalsIgnoreCase(TransactionStatus.RTBANK_WAITING) 
		    			  || fundCode.equalsIgnoreCase(TransactionStatus.RTBANK_SETTLED_INTERAC)
		    			  || fundCode.equalsIgnoreCase(TransactionStatus.NOT_FUNDED_CODE) || fundCode.equalsIgnoreCase(TransactionStatus.ACH_WAITING_CODE)
		    			  || fundCode.equalsIgnoreCase(TransactionStatus.ACH_IN_PROCESS_CODE))
		    			  // SEN status & sub-status RBW,RBS,RTS
		    			 
		    			  || statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE) 
		    			  && (fundCode.equalsIgnoreCase(TransactionStatus.RTBANK_WAITING) || fundCode.equalsIgnoreCase(TransactionStatus.RTBANK_SETTLED) 
		    					  || fundCode.equalsIgnoreCase(TransactionStatus.RTBANK_SETTLED_INTERAC) || fundCode.equalsIgnoreCase(TransactionStatus.ACH_SENT_CODE) ) 
		    			  // CXL status & sub-status RTS
		    			  || (statCode.equalsIgnoreCase(TransactionStatus.CANCELED_CODE)
						  && fundCode.equalsIgnoreCase(TransactionStatus.RTBANK_SETTLED_INTERAC))
						  //FD-213 - CC						  
						  || canAutomatCc ))
		    	  {		   		    		  isFlags[i] = true;
		    	  }
		    	  break;
		      case 178: //54 SEN status & sub-status ACS
		    	  if (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE) && fundCode.equalsIgnoreCase(TransactionStatus.ACH_SENT_CODE)
		    		// SEN status & sub-status RBW,RBS,RTS
		    			  || statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE) 
		    			  && (fundCode.equalsIgnoreCase(TransactionStatus.RTBANK_WAITING) || fundCode.equalsIgnoreCase(TransactionStatus.RTBANK_SETTLED) 
		    					  || fundCode.equalsIgnoreCase(TransactionStatus.RTBANK_SETTLED_INTERAC))
		    					  //SEN STATUS &  sub status CCS,CXL,EER FD-213
		    			  || ((statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE) || statCode.equalsIgnoreCase(TransactionStatus.ERROR_CODE)
							        || statCode.equalsIgnoreCase(TransactionStatus.CANCELED_CODE))
							      && fundCode.equalsIgnoreCase(TransactionStatus.CC_SALE_CODE))
                                  							     
		    	  ) {
		    		  isFlags[i-1] = true;
		    		  isFlags[i] = true;
		    	  }
		      case 179: //55 // moveToReview - status OCR and fundstatus - NOF
		    	  if (statCode.equalsIgnoreCase(TransactionStatus.OPTICAL_CHARACTER_RECOGNITION_CODE)
		    			  && fundCode.equalsIgnoreCase(TransactionStatus.NOT_FUNDED_CODE)) {
		    		  isFlags[i] = true;
		    	  }
		    	  break;
			}
		}
		return isFlags;
	}


	public boolean canShowPartialRefundButton() {
		return ! isSameDay()
		&& TransactionType.MONEY_TRANSFER_SEND_CODE.equalsIgnoreCase(this.emgTranTypeCode)
		&& isInCorrectStatusForRefund()
		&& isInCorrectPartnerSiteForRefund();
	}


	private boolean isSameDay() {
		boolean sameDay = false;
		DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
		String today = df.format(new Date(System.currentTimeMillis()));
		if (df.format(this.createDate).equals(today)
				&& (TransactionType.MONEY_TRANSFER_SEND_CODE
						.equalsIgnoreCase(this.emgTranTypeCode) || TransactionType.DELAY_SEND_CODE
						.equalsIgnoreCase(this.emgTranTypeCode))) {
			sameDay = true;
		}

		if (df.format(this.tranStatDate).equals(today)
				&& (TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE
						.equalsIgnoreCase(this.emgTranTypeCode) || TransactionType.EXPRESS_PAYMENT_SEND_CODE
						.equalsIgnoreCase(this.emgTranTypeCode))) {
			sameDay = true;
		}
		
		
		return sameDay;
	}

	public boolean showCancel() {
		if (TransactionType.EXPRESS_PAYMENT_SEND_CODE.equalsIgnoreCase(emgTranTypeCode)) {
			return false;
		}
		// Return true only if it's FFS/NOF, APP/ACW, SEND/ACW, and SEN/CCS
		boolean[] flags = getCommandFlags("N", new BigDecimal(0));
		return ((TransactionStatus.FORM_FREE_SEND_CODE.equals(status.getStatusCode())
				|| TransactionStatus.PENDING_CODE.equals(status.getStatusCode()) || TransactionStatus.APPROVED_CODE.equals(status
				.getStatusCode())) && TransactionStatus.NOT_FUNDED_CODE.equals(status.getSubStatusCode()))
				|| flags[8] || flags[11] || (!flags[8] && !flags[11] && (flags[27] || flags[31]));
	}

	public String getInternetPurchase() {
		return internetPurchase;
	}

	public void setInternetPurchase(String string) {
		internetPurchase = string;
	}

	public int compareTo(Object o) {
		Transaction tran = (Transaction) o;
		return this.emgTranId > tran.emgTranId ? -1 : 1;
	}

	public int hashCode() {
		return this.emgTranId;
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof Transaction)) {
			return false;
		}
		return ((Transaction)o).emgTranId == this.emgTranId;
	}

	/**
	 * @return Returns the tran_rvw_prcs_code.
	 */
	public String getTranRvwPrcsCode() {
		return this.tranRvwPrcsCode;
	}

	/**
	 * @param tran_rvw_prcs_code
	 *            The tran_rvw_prcs_code to set.
	 */
	public void setTranRvwPrcsCode(String processCode) {
		this.tranRvwPrcsCode = processCode;
	}

	/**
	 * @return Returns the fundRetrievalRequestDate.
	 */
	public Date getFundRetrievalRequestDate() {
		return fundRetrievalRequestDate;
	}

	/**
	 * @param fundRetrievalRequestDate
	 *            The fundRetrievalRequestDate to set.
	 */
	public void setFundRetrievalRequestDate(Date frrd) {
		if (frrd == null)
			this.fundRetrievalRequestDate = null;
		else
			this.fundRetrievalRequestDate = frrd;
	}

	/**
	 * @return Returns the rRN.
	 */
	public String getRRN() {
		return RRN;
	}

	/**
	 * @param rrn
	 *            The rRN to set.
	 */
	public void setRRN(String string) {
		if (string == null)
			RRN = string;
		else
			RRN = string.toUpperCase();
	}

	public String getCustomerAutoEnrollFlag() {
		return customerAutoEnrollFlag;
	}

	public void setCustomerAutoEnrollFlag(String customerAutoEnrollFlag) {
		this.customerAutoEnrollFlag = customerAutoEnrollFlag;
	}

	public String getTestQuestion() {
		return testQuestion;
	}

	public void setTestQuestion(String testQuestion) {
		this.testQuestion = testQuestion;
	}

	public String getTestQuestionAnswer() {
		return testQuestionAnswer;
	}

	public void setTestQuestionAnswer(String testQuestionAnswer) {
		this.testQuestionAnswer = testQuestionAnswer;
	}

	public String getLoyaltyPgmMembershipId() {
		return loyaltyPgmMembershipId;
	}

	public void setLoyaltyPgmMembershipId(String loyaltyPgmMembershipId) {
		this.loyaltyPgmMembershipId = loyaltyPgmMembershipId;
	}

	public String getSndCustPhotoIdTypeCode() {
		return sndCustPhotoIdTypeCode;
	}

	public void setSndCustPhotoIdTypeCode(String sndCustPhotoIdTypeCode) {
		this.sndCustPhotoIdTypeCode = sndCustPhotoIdTypeCode;
	}

	public String getSndCustPhotoIdCntryCode() {
		return sndCustPhotoIdCntryCode;
	}

	public void setSndCustPhotoIdCntryCode(String sndCustPhotoIdCntryCode) {
		this.sndCustPhotoIdCntryCode = sndCustPhotoIdCntryCode;
	}

	public String getSndCustPhotoIdStateCode() {
		return sndCustPhotoIdStateCode;
	}

	public void setSndCustPhotoIdStateCode(String sndCustPhotoIdStateCode) {
		this.sndCustPhotoIdStateCode = sndCustPhotoIdStateCode;
	}

	public String getSndCustPhotoId() {
		return sndCustPhotoId;
	}

	public void setSndCustPhotoId(String sndCustPhotoId) {
		this.sndCustPhotoId = sndCustPhotoId;
	}

	public String getSndCustLegalIdTypeCode() {
		return sndCustLegalIdTypeCode;
	}

	public void setSndCustLegalIdTypeCode(String sndCustLegalIdTypeCode) {
		this.sndCustLegalIdTypeCode = sndCustLegalIdTypeCode;
	}

	public String getSndCustLegalId() {
		return sndCustLegalId;
	}

	public void setSndCustLegalId(String sndCustLegalId) {
		this.sndCustLegalId = sndCustLegalId;
	}

	public String getSndCustOccupationText() {
		return sndCustOccupationText;
	}

	public void setSndCustOccupationText(String sndCustOccupationText) {
		this.sndCustOccupationText = sndCustOccupationText;
	}

	public String getRcvCustAddrISOCntryCode() {
		return rcvCustAddrISOCntryCode;
	}

	public void setRcvCustAddrISOCntryCode(String rcvCustAddrISOCntryCode) {
		this.rcvCustAddrISOCntryCode = rcvCustAddrISOCntryCode;
	}

	public String getIntndDestStateProvinceCode() {
		return intndDestStateProvinceCode;
	}

	public void setIntndDestStateProvinceCode(String intndDestStateProvinceCode) {
		this.intndDestStateProvinceCode = intndDestStateProvinceCode;
	}

	public String getMccPartnerProfileId() {
		return mccPartnerProfileId;
	}

	public void setMccPartnerProfileId(String mccPartnerProfileId) {
		this.mccPartnerProfileId = mccPartnerProfileId;
	}

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}

	public Integer getTranRiskScore() {
		return tranRiskScore;
	}

	public void setTranRiskScore(Integer tranRiskScore) {
		this.tranRiskScore = tranRiskScore;
	}

	public boolean isTranRiskScoreDefaulted() {
		return tranRiskScoreDefaulted;
	}

	public void setTranRiskScoreDefaulted(boolean tranRiskScoreDefaulted) {
		this.tranRiskScoreDefaulted = tranRiskScoreDefaulted;
	}

	public long getTranSeqNumber() {
		return tranSeqNumber;
	}

	public void setTranSeqNumber(long tranSeqNumber) {
		this.tranSeqNumber = tranSeqNumber;
	}

	private boolean isInCorrectStatusForRefund(){
		String statCode = status.getStatusCode();
		String fundCode = status.getSubStatusCode();

		return (statCode.equalsIgnoreCase(TransactionStatus.SENT_CODE)
			&& (fundCode.equalsIgnoreCase(TransactionStatus.CC_AUTH_CODE)
				 || fundCode.equalsIgnoreCase(TransactionStatus.CC_SALE_CODE)
				 || fundCode.equalsIgnoreCase(TransactionStatus.CC_FAILED_CODE)))
		||
		(statCode.equalsIgnoreCase(TransactionStatus.CANCELED_CODE)
			&& (fundCode.equalsIgnoreCase(TransactionStatus.CC_SALE_CODE)
				 || fundCode.equalsIgnoreCase(TransactionStatus.CC_REFUND_REQUESTED_CODE)
				 || fundCode.equalsIgnoreCase(TransactionStatus.CC_CANCEL_CODE)))
		||
		(statCode.equalsIgnoreCase(TransactionStatus.APPROVED_CODE)
			&& (fundCode.equalsIgnoreCase(TransactionStatus.CC_FAILED_CODE)
				 || fundCode.equalsIgnoreCase(TransactionStatus.CC_AUTH_CODE)));
	}

	private boolean isInCorrectPartnerSiteForRefund() {

		boolean isAbleInOtherSite =
			EMTSharedContainerProperties.getUseGCForProcessing() && this.tCProviderCode.equals(Constants.PYMT_PROVIDER_CODE_GLOBAL_COLLECT);
		boolean isCorrect = (partnerSiteId.equals(Constants.MGOUK_PARTNER_SITE_ID) || partnerSiteId.equals(Constants.MGODE_PARTNER_SITE_ID))
		|| (isAbleInOtherSite && (partnerSiteId.equals(Constants.WAP_PARTNER_SITE_ID) || partnerSiteId.equals(Constants.MGO_PARTNER_SITE_ID)));

		return isCorrect;
	}

	public void setThreeMinuteFreePhoneNumber(String threeMinuteFreePhoneNumber) {
		this.threeMinuteFreePhoneNumber = threeMinuteFreePhoneNumber;
	}

	public String getThreeMinuteFreePhoneNumber() {
		return threeMinuteFreePhoneNumber;
	}

	public void setThreeMinuteFreePinNumber(String threeMinuteFreePinNumber) {
		this.threeMinuteFreePinNumber = threeMinuteFreePinNumber;
	}

	public String getThreeMinuteFreePinNumber() {
		return threeMinuteFreePinNumber;
	}

	public String getMgiTransactionSessionID() {
		return mgiTransactionSessionID;
	}

	public void setMgiTransactionSessionID(String mgiTransactionSessionID) {
		this.mgiTransactionSessionID = mgiTransactionSessionID;
	}

	public Date getTranAvailabilityDate() {
		return tranAvailabilityDate;
	}

	public void setTranAvailabilityDate(Date tranAvailabilityDate) {
		this.tranAvailabilityDate = tranAvailabilityDate;
	}

	public String getRegulationVersion() {
		return regulationVersion;
	}

	public void setRegulationVersion(String regulationVersion) {
		this.regulationVersion = regulationVersion;
	}

	public BigDecimal getRcvNonMgiFeeAmt() {
		return rcvNonMgiFeeAmt;
	}

	public void setRcvNonMgiFeeAmt(BigDecimal rcvNonMgiFeeAmt) {
		this.rcvNonMgiFeeAmt = rcvNonMgiFeeAmt;
	}

	public BigDecimal getRcvNonMgiTaxAmt() {
		return rcvNonMgiTaxAmt;
	}

	public void setRcvNonMgiTaxAmt(BigDecimal rcvNonMgiTaxAmt) {
		this.rcvNonMgiTaxAmt = rcvNonMgiTaxAmt;
	}

	public BigDecimal getRcvPayoutAmt() {
		return rcvPayoutAmt;
	}

	public void setRcvPayoutAmt(BigDecimal rcvPayoutAmt) {
		this.rcvPayoutAmt = rcvPayoutAmt;
	}

	public String getSndCustScndLastName() {
		return sndCustScndLastName;
	}

	public void setSndCustScndLastName(String sndCustScndLastName) {
		this.sndCustScndLastName = sndCustScndLastName;
	}

	public void setRcvPayoutISOCrncyId(String rcvPayoutISOCrncyId) {
		this.rcvPayoutISOCrncyId = rcvPayoutISOCrncyId;
	}

	public String getRcvPayoutISOCrncyId() {
		return rcvPayoutISOCrncyId;
	}

	public String getRcvAcctMaskNbr() {
		return rcvAcctMaskNbr;
	}

	public void setRcvAcctMaskNbr(String rcvAcctMaskNbr) {
		this.rcvAcctMaskNbr = rcvAcctMaskNbr;
	}

	public String getAvsResult() {
		return avsResult;
	}

	public void setAvsResult(String avsResult) {
		this.avsResult = avsResult;
	}

	public String getRsaAdaptiveAuthResult() {
		return rsaAdaptiveAuthResult;
	}

	public void setRsaAdaptiveAuthResult(String rsaAdaptiveAuthResult) {
		this.rsaAdaptiveAuthResult = rsaAdaptiveAuthResult;
	}

	public String getProfileType() {
		return profileType;
	}

	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}

	public String getGuestTransactionFlag() {
		return guestTransactionFlag;
	}

	public void setGuestTransactionFlag(String guestTransactionFlag) {
		this.guestTransactionFlag = guestTransactionFlag;
	}
	//MBO-461 changes starts
	public float getTranAppVersionNumber(){
		return tranAppVersionNumber;
	}
	public void setTranAppVersionNumber(float tranAppVersionNumber){
		this.tranAppVersionNumber= tranAppVersionNumber;
	}
	//MBO-461 changes ends

	public String getTranSessionBeginId() {
		return tranSessionBeginId;
	}

	public void setTranSessionBeginId(String tranSessionBeginId) {
		this.tranSessionBeginId = tranSessionBeginId;
	}

	public String getRcvAcctHashNbr() {
		return rcvAcctHashNbr;
	}

	public String getReceiverEmailAddress() {
		return receiverEmailAddress;
	}

	public void setReceiverEmailAddress(String receiverEmailAddress) {
		this.receiverEmailAddress = receiverEmailAddress;
	}

	public String getReceiverEmailMessage() {
		return receiverEmailMessage;
	}

	public void setReceiverEmailMessage(String receiverEmailMessage) {
		this.receiverEmailMessage = receiverEmailMessage;
	}

	public void setRcvAcctHashNbr(String rcvAcctHashNbr) {
		this.rcvAcctHashNbr = rcvAcctHashNbr;
	}

	public void setConsumerAddress(ConsumerAddress consumerAddress) {
		this.consumerAddress = consumerAddress;
	}

	public ConsumerAddress getConsumerAddress() {
		return consumerAddress;
	}

	public String getTranLanguage() {
		return tranLanguage;
	}

	public void setTranLanguage(String tranLanguage) {
		this.tranLanguage = tranLanguage;
	}


	//MBO-6101	
	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getAgentUnitProfileId() {
		return agentUnitProfileId;
	}

	public void setAgentUnitProfileId(String agentUnitProfileId) {
		this.agentUnitProfileId = agentUnitProfileId;
	}

	public String getAgentSeqNo() {
		return agentSeqNo;
	}

	public void setAgentSeqNo(String agentSeqNo) {
		this.agentSeqNo = agentSeqNo;
	}

	public String getAgentToken() {
		return agentToken;
	}

	public void setAgentToken(String agentToken) {
		this.agentToken = agentToken;
	}

	public String getSourceDevice() {
		return sourceDevice;
	}

	public void setSourceDevice(String sourceDevice) {
		this.sourceDevice = sourceDevice;
	}

	public String getSourceApp() {
		return sourceApp;
	}

	public void setSourceApp(String sourceApp) {
		this.sourceApp = sourceApp;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getPromoDiscountType() {
		return promoDiscountType;
	}

	public void setPromoDiscountType(String promoDiscountType) {
		this.promoDiscountType = promoDiscountType;
	}

	public String getPromoDiscountAmount() {
		return promoDiscountAmount;
	}

	public void setPromoDiscountAmount(String promoDiscountAmount) {
		this.promoDiscountAmount = promoDiscountAmount;
	}
	

	//ended
	
}
