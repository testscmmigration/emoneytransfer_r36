package emgshared.model;

import java.sql.Timestamp;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.services.CacheService;
import emgshared.services.MessageService;
import emgshared.services.ServiceFactory;

public class MessageCacheBean
{
	private static Timestamp cacheTime;
	private static Map messageMap;

	private static Object LATCH = new Object();
	private static Object LATCH1 = new Object();
	private static MessageCacheBean _instance = null;

	private MessageCacheBean() throws DataSourceException
	{
		refreshMessageMap();

		Timer timer = new Timer(true);
		timer.schedule(new RefreshCache(), 60 * 1000, 60 * 1000);
	}

	public static MessageCacheBean getInstance() throws DataSourceException
	{
		synchronized (LATCH)
		{
			if (_instance == null)
			{
				_instance = new MessageCacheBean();
			}
			return _instance;
		}
	}

	private void validateCache()
	{
		try
		{
			CacheService cs = ServiceFactory.getInstance().getCacheService();
			SimpleCache sc = cs.getCache(null, SimpleCache.ADMIN_MESSAGE);
			if (!cacheTime.equals(sc.getTimeStamp()))
			{
				refreshMessageMap();
			}
		} catch (Exception e)
		{
			throw new EMGRuntimeException(e);
		}
	}

	private void refreshMessageMap() throws DataSourceException
	{
		MessageService ms = ServiceFactory.getInstance().getMessageService();
		Map tmpMap = ms.getActiveUserMessages("emgwww");
		try
		{
			CacheService cs = ServiceFactory.getInstance().getCacheService();
			SimpleCache sc = cs.getCache(null, SimpleCache.ADMIN_MESSAGE);
			cacheTime = sc.getTimeStamp();
		} catch (Exception e)
		{
			throw new EMGRuntimeException(e);
		}

		synchronized (LATCH1)
		{
			messageMap = tmpMap;
		}
	}

	public Map getMessageMap()
	{
		validateCache();
		synchronized (LATCH1)
		{
			return messageMap;
		}
	}

	protected class RefreshCache extends TimerTask
	{
		public void run()
		{
			validateCache();
		}
	}
}
