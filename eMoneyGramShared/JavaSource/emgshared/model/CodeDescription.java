package emgshared.model;

public class CodeDescription 
	{

	private String code;	
	private String description;
	
	public CodeDescription(String id, String desc){
		this.code = id;
		this.description = desc;
	}
	
	/**
	 * @return
	 */
	public String getCode()
	{
		return code;
	}

	/**
	 * @return
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @param string
	 */
	public void setCode(String string)
	{
		code = string;
	}

	/**
	 * @param string
	 */
	public void setDescription(String string)
	{
		description = string;
	}

}
