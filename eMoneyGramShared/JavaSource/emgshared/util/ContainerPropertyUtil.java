package emgshared.util;


import org.apache.log4j.Logger;

import com.moneygram.ree.lib.Config;

import emgshared.dataaccessors.EMGSharedLogger;

public class ContainerPropertyUtil {

	public static final int defaultServiceTimeout = 20000;
	private static ResourceConfigFactory resourceConfigFactory = new ResourceConfigFactory();
	private static Config mgoConfig;
	private static final String CONTAINER_RESOURCE_URI = "java:comp/env/rep/EMTAdminResourceReference";
	private static Logger logr =
		EMGSharedLogger.getLogger(ContainerPropertyUtil.class);
	public static ResourceConfigFactory getResourceConfigFactory() {
		return resourceConfigFactory;
	}

	public static void setResourceConfigFactory(
			ResourceConfigFactory resourceConfigFactory) {
		ContainerPropertyUtil.resourceConfigFactory = resourceConfigFactory;
	}

	public static String getContextResourceVariable(String variableName) {
		String returnString = "";
		try {
			if (mgoConfig == null) {
				mgoConfig = getResourceConfigFactory()
						.createResourceConfiguration(CONTAINER_RESOURCE_URI);
			}
			returnString = (String) mgoConfig.getAttribute(variableName);
			EMGSharedLogger.getLogger("Resource Lookup Result for:"
					+ variableName + " :" + returnString);
		} catch (Exception exception) {
			
			EMGSharedLogger.getLogger("Error in ContainerPropertyUtil :"
					+ exception.getLocalizedMessage());
			logr.error(exception.getMessage(),exception);
		}
		return returnString;
	}
}
