/*
 * Created on Feb 2, 2005
 *
 */
package emgshared.util;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import emgshared.dataaccessors.EMGSharedLogger;
/**
 * This class contains 'convenient' static methods
 * for getting the eMail text.
 * 
 * @author T349
 *
 */
public class MailHelper
{
	private MailHelper()
	{
	}

	public static boolean sendMail(
		String mailHost,
		String from,
		String to,
		String subject,
		String body)
	{
		Properties props = new Properties();
		props.put("mail.host", mailHost);
		Session session = Session.getDefaultInstance(props, null);

		try
		{
			Message msg = null;
			msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(from));
			InternetAddress[] address = { new InternetAddress(to)};
			msg.setRecipients(Message.RecipientType.TO, address);
			msg.setSubject(subject);
			msg.setSentDate(new Date());
			msg.setText(body);
   			msg.setHeader( "Content-Type", "text/plain; charset=iso-8859-1");
			Transport.send(msg);
			return true;
		} catch (NoSuchProviderException nsp)
		{
			EMGSharedLogger.getLogger(MailHelper.class.toString()).error(
				"Send Mail failed",
				nsp);
			return false;
		} catch (AddressException a)
		{
			EMGSharedLogger.getLogger(MailHelper.class.toString()).error(
				"Send Mail failed",
				a);
			return false;
		} catch (MessagingException me)
		{
			EMGSharedLogger.getLogger(MailHelper.class.toString()).error(
				"Send Mail failed",
				me);
			return false;
		} catch (Exception e)
		{
			EMGSharedLogger.getLogger(MailHelper.class.toString()).error(
				"Send Mail failed",
				e);
			return false;
		}
	}
}
