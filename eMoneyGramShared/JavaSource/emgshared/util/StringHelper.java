/*
 * Created on Feb 6, 2004
 *
 */
package emgshared.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

/**
 * This class contains 'convenient' static methods for working with Strings.
 * There isn't any reason to instantiate an object of this type but since the
 * StringHelper in the Admin project is extending this object, it could not have
 * a private constructor so made the constructor protected.
 * 
 * @author A131
 * 
 */
public class StringHelper {
    protected StringHelper() {
    }

    public static boolean isNullOrEmpty(String s) {
        return (s == null || s.trim().length() == 0);
    }

    public static int compareToIgnoreCase(String s1, String s2) {
        int comparison = 0;
        if (s1 == null) {
            comparison = (s2 == null ? 0 : 1);
        } else if (s2 == null) {
            comparison = -1;
        } else {
            comparison = s1.compareToIgnoreCase(s2);
        }
        return comparison;
    }

    public static boolean equalIgnoreCase(String s1, String s2) {
        boolean equality;
        if (s1 == null) {
            equality = (s2 == null);
        } else if (s2 == null) {
            equality = false;
        } else {
            equality = s1.equalsIgnoreCase(s2);
        }
        return equality;
    }

    public static String getEmptyIfNull(String s) {
        return (s == null ? "" : s); //$NON-NLS-1$
    }

    public static boolean containsNonDigits(String s) {
        boolean nonDigits = false;
        if (StringHelper.isNullOrEmpty(s) == false) {
            for (int i = 0; nonDigits == false && i < s.length(); ++i) {
                nonDigits = Character.isDigit(s.charAt(i)) == false;
            }
        }
        return (nonDigits);
    }

    public static boolean containsNonDigits(String s, char[] ignore) {
        boolean nonDigits = false;
        if (StringHelper.isNullOrEmpty(s) == false) {
            for (int i = 0; nonDigits == false && i < s.length(); ++i) {
                char c = s.charAt(i);
                if (Character.isDigit(c) == false) {
                    nonDigits = true;
                    for (int j = 0; nonDigits == true && j < ignore.length; j++) {
                        nonDigits = (c == ignore[j]) == false;
                    }
                }
            }
        }
        return (nonDigits);
    }

    public static boolean containsAlpha(String s) {
        boolean alpha = false;
        if (StringHelper.isNullOrEmpty(s) == false) {
            for (int i = 0; alpha == false && i < s.length(); ++i) {
                char c = s.charAt(i);
                if (Character.isLetter(c) == true)
                    alpha = true;
            }
        }
        return (alpha);
    }

    public static boolean containsNonAlpha(String s, char[] ignore) {
        boolean nonAlpha = false;
        if (StringHelper.isNullOrEmpty(s) == false) {
            for (int i = 0; nonAlpha == false && i < s.length(); ++i) {
                char c = s.charAt(i);
                if (Character.isLetter(c) == false) {
                    nonAlpha = true;
                    for (int j = 0; nonAlpha == true && j < ignore.length; j++) {
                        nonAlpha = (c == ignore[j]) == false;
                    }
                }
            }
        }
        return (nonAlpha);
    }

    public static String trim(String s) {
        return s == null ? null : s.trim();
    }

    public static String getLastToken(String s, String delimiter) {
        String token = null;
        if (s != null && delimiter != null) {
            token = s.substring(s.lastIndexOf(delimiter) + delimiter.length());
        }
        return (token);
    }

    public static String removeNonDigits(String s) {
        StringBuilder sb = new StringBuilder();
        if (s != null) {
            for (int i = 0; i < s.length(); ++i) {
                char c = s.charAt(i);
                if (Character.isDigit(c)) {
                    sb.append(c);
                }
            }
        }
        return (sb.toString());
    }

    public static String[] split(String stringToSplit, String splitString) {
        // The WSAD Web Environment does not suppport JDK 1.4
        // (does from a Java Project) so I added this String.split method
        // cannot in this project and it can be called from the Web projects.
        return (String[]) stringToSplit.split(splitString);
    }

    public static Date getFileBuildDate(String[] fileName) {
        long timestamp = 0;
        for (int i = 0; i < fileName.length; i++) {
            timestamp = (new File(fileName[i])).lastModified();
            if (timestamp != 0) {
                break;
            }
        }
        return new Date(timestamp);
    }

    public static String getExceptionDumpMsg(Throwable t) {
        StringBuilder msg = new StringBuilder();

        msg.append("Timestamp:\t"
                + (new Date(System.currentTimeMillis())).toString() + "\n");
        msg.append("Class:\t" + t.getClass().getName() + "\n");
        msg.append("Message:\t" + t.getMessage() + "\n");

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
       // t.printStackTrace(pw);
        msg.append(sw.toString());

        try {
            pw.close();
            sw.close();
        } catch (IOException e) {
        }

        return msg.toString();
    }

    public static String prepad(String s, int length, char c) {
        int needed = length - s.length();
        if (needed <= 0) {
            return s;
        }
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < needed; i++) {
            sb.append(c);
        }
        sb.append(s);
        return (sb.toString());
    }

    public static int compareIP(String ip1, String ip2) {
        if (ip1 == null && ip2 == null) {
            return 0;
        }
        if (ip1 == null) {
            return -1;
        }
        if (ip2 == null) {
            return 1;
        }
        String[] ipStr1 = ip1.split("\\.");
        String[] ipStr2 = ip2.split("\\.");
        if (ipStr1.length != 4 || ipStr2.length != 4) {
            return 0;
        }
        for (int i = 0; i < 4; i++) {
            if (containsNonDigits(ipStr1[i]) || containsNonDigits(ipStr2[i])) {
                return 0;
            }
        }
        int[] ipNbr1 = { 0, 0, 0, 0 };
        int[] ipNbr2 = { 0, 0, 0, 0 };
        for (int i = 0; i < 4; i++) {
            ipNbr1[i] = Integer.parseInt(ipStr1[i]);
            ipNbr2[i] = Integer.parseInt(ipStr2[i]);
            if (ipNbr1[i] > ipNbr2[i]) {
                return 1;
            } else if (ipNbr1[i] < ipNbr2[i]) {
                return -1;
            }
        }
        return 0;
    }

    public static String truncate(String str, int length) {
        if (str == null) {
            return str;
        }
        if (str.length() > length) {
            return str.substring(0, length);
        } else {
            return str;
        }
    }

    public static String replaceAll(String str, String string1, String string2) {
        // The WSAD Web Environment does not suppport JDK 1.4
        // (does from a Java Project) so I added this String.replaceAll method
        // cannot in this project and it can be called from the Web projects.
        return str.replaceAll(string1, string2);
    }

    public static String getCcBin(String creditCardNumber) {
        return creditCardNumber.substring(0, 6);
    }

    public static String maskCc(String creditCardNumber) {
        return creditCardNumber.substring(0, 6) + "******"
                + creditCardNumber.substring(12);
    }
}
