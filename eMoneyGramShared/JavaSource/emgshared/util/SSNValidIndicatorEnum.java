package emgshared.util;

import java.util.HashMap;
import java.util.Map;

public enum SSNValidIndicatorEnum {
	RELATIVE(6, "R", "Relative's SSN", "R=Relative's SSN"), GOOD(1, "G",
			"Good", "G=Good"), BAD(8, "B", "Bad", "B=Bad"), MANUFACTURED(2,
			"M", "Manufactured (from other data found)",
			"M=Manufactured (from other data found)"), SUPPRESED(3, "S",
			"Suppressed", "S=Suppressed"), INSUFFICIENT(4, "X",
			"Insufficient Information", "X=Insufficient Information"), OLD(5,
			"O", "Old (issued before subject DOB)",
			"O=Old (issued before subject DOB)"), TYPHOGRAPHICAL_ERROR(7, "F",
			"Typographical Error", "F=Typographical Error"), UNKNOWN(9, "U",
			"Unknown", "U=Unknown"), DIFF_OWNER(10, "Z",
			"Possible Different Owner", "Z=Possible Different Owner"), YES(11,
			"Y", "Yes", "Y=Yes"), NO(12, "N", "No", "N=No");

	private int id;
	private String code;
	private String desc;
	private String label;
	private static Map<String, String> codeToDescMapping;
	private static Map<String, String> codeToLabelMapping;
	private static Map<String, String> descToCodeMapping;
	private static Map<String, Integer> codeToIdMapping;
	private static Map<Integer, String> idToLabelMapping;

	private SSNValidIndicatorEnum(int id, String code, String desc, String label) {
		this.id = id;
		this.code = code;
		this.desc = desc;
		this.label = label;
	}

	public static String getLabelValue(String code) {
		if (null == codeToLabelMapping) {
			codeToLabelMapping = new HashMap<String, String>();
		}
		intLabelMapping();
		return codeToLabelMapping.get(code);
	}

	public static void intLabelMapping() {
		for (SSNValidIndicatorEnum indicator : values()) {
			codeToLabelMapping.put(indicator.getCode(), indicator.getLabel());
		}
	}

	public static String getDescValue(String code) {
		if (null == codeToDescMapping) {
			codeToDescMapping = new HashMap<String, String>();
		}
		intDescMapping();
		return codeToDescMapping.get(code);
	}

	public static void intDescMapping() {
		for (SSNValidIndicatorEnum indicator : values()) {
			codeToDescMapping.put(indicator.getCode(), indicator.getDesc());
		}
	}
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Map<String, String> getCodeToDescMapping() {
		return codeToDescMapping;
	}

	public void setCodeToDescMapping(Map<String, String> codeToDescMapping) {
		this.codeToDescMapping = codeToDescMapping;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Map<String, String> getCodeToLabelMapping() {
		return codeToLabelMapping;
	}

	public void setCodeToLabelMapping(Map<String, String> codeToLabelMapping) {
		this.codeToLabelMapping = codeToLabelMapping;
	}
	
	public static String getCodeValue(String desc) {
		if (null == descToCodeMapping) {
			descToCodeMapping = new HashMap<String, String>();
		}
		intCodeMapping();
		return descToCodeMapping.get(desc);
	}

	public static void intCodeMapping() {
		for (SSNValidIndicatorEnum indicator : values()) {
			descToCodeMapping.put(indicator.getDesc(), indicator.getCode());
		}
	}
	
	public static void intiDescIdMapping() {
		for (SSNValidIndicatorEnum indicator : values()) {
			codeToIdMapping.put(indicator.getCode().toUpperCase(), indicator.getId());
		}
	}
	
	public static Integer getIdValueByCode(String code) {
		if (null == codeToIdMapping) {
			codeToIdMapping = new HashMap<String, Integer>();
		}
		intiDescIdMapping();
		return codeToIdMapping.get(code.toUpperCase());
	}

	public static Map<String, String> getDescToCodeMapping() {
		return descToCodeMapping;
	}

	public static void setDescToCodeMapping(Map<String, String> descToCodeMapping) {
		SSNValidIndicatorEnum.descToCodeMapping = descToCodeMapping;
	}

	public static Map<String, Integer> getCodeToIdMapping() {
		return codeToIdMapping;
	}

	public static void setCodeToIdMapping(Map<String, Integer> codeToIdMapping) {
		SSNValidIndicatorEnum.codeToIdMapping = codeToIdMapping;
	}
	
	
	public static Map<Integer, String> getIdToLabelMapping() {
		return idToLabelMapping;
	}

	public static void setIdToLabelMapping(Map<Integer, String> idToLabelMapping) {
		SSNValidIndicatorEnum.idToLabelMapping = idToLabelMapping;
	}

	public static void initIdLabelMapping() {
		for (SSNValidIndicatorEnum indicator : values()) {
			idToLabelMapping.put(indicator.getId(), indicator.getLabel());
		}
	}
	
	public static String getLabelValueById(Integer id) {
		if (null == idToLabelMapping) {
			idToLabelMapping = new HashMap<Integer, String>();
		}
		initIdLabelMapping();
		return idToLabelMapping.get(id);
	}

}
