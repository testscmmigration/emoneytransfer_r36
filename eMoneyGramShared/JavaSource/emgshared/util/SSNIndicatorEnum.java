package emgshared.util;

import java.util.HashMap;
import java.util.Map;

public enum SSNIndicatorEnum {
	YES(1,"Y", "Yes", "Y=Yes"), NO(2,"N", "No", "N=No"), MAYBE(3,"M", "Maybe",
			"M=Maybe"), CLOSE(4,"C", "Close", "C=Close"), RELATIVE(5,"R",
			"Relative", "R=Relative");
	private Integer id;
	private String code;
	private String desc;
	private String label;
	private static Map<String, String> codeToDescMapping;
	private static Map<String, String> codeToLabelMapping;
	
	private static Map<String, Integer> descToIdMapping;
	private static Map<Integer, String> idToLabelMapping;

	private SSNIndicatorEnum(int id,String code, String desc, String label) {
		this.id = id;
		this.code = code;
		this.desc = desc;
		this.label = label;
	}

	public static String getLabelValue(String code) {
		if (null == codeToLabelMapping) {
			codeToLabelMapping = new HashMap<String, String>();
		}
		initLabelMapping();
		return codeToLabelMapping.get(code);
	}

	public static void initLabelMapping() {
		for (SSNIndicatorEnum indicator : values()) {
			codeToLabelMapping.put(indicator.getCode(), indicator.getLabel());
		}
	}

	public static String getDescValue(String desc) {
		if (null == codeToDescMapping) {
			codeToDescMapping = new HashMap<String, String>();
		}
		initDescMapping();
		return codeToDescMapping.get(desc.toUpperCase());
	}

	public static void initDescMapping() {
		for (SSNIndicatorEnum indicator : values()) {
			codeToDescMapping.put(indicator.getDesc().toUpperCase(), indicator.getCode());
		}
	}

	public static void initIdLabelMapping() {
		for (SSNIndicatorEnum indicator : values()) {
			idToLabelMapping.put(indicator.getId(), indicator.getLabel());
		}
	}
	
	public static void intiDescIdMapping() {
		for (SSNIndicatorEnum indicator : values()) {
			descToIdMapping.put(indicator.getCode().toUpperCase(), indicator.getId());
		}
	}
	
	public static Integer getIdValueByCode(String code) {
		if (null == descToIdMapping) {
			descToIdMapping = new HashMap<String, Integer>();
		}
		intiDescIdMapping();
		return descToIdMapping.get(code.toUpperCase());
	}
	
	public static String getLabelValueById(Integer id) {
		if (null == idToLabelMapping) {
			idToLabelMapping = new HashMap<Integer, String>();
		}
		initIdLabelMapping();
		return idToLabelMapping.get(id);
	}
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public static Map<String, Integer> getDescToIdMapping() {
		return descToIdMapping;
	}

	public static void setDescToIdMapping(Map<String, Integer> descToIdMapping) {
		SSNIndicatorEnum.descToIdMapping = descToIdMapping;
	}

	public static Map<Integer, String> getIdToLabelMapping() {
		return idToLabelMapping;
	}

	public static void setIdToLabelMapping(Map<Integer, String> idToLabelMapping) {
		SSNIndicatorEnum.idToLabelMapping = idToLabelMapping;
	}

	public Map<String, String> getCodeToDescMapping() {
		return codeToDescMapping;
	}

	public void setCodeToDescMapping(Map<String, String> codeToDescMapping) {
		this.codeToDescMapping = codeToDescMapping;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Map<String, String> getCodeToLabelMapping() {
		return codeToLabelMapping;
	}

	public void setCodeToLabelMapping(Map<String, String> codeToLabelMapping) {
		this.codeToLabelMapping = codeToLabelMapping;
	}
}
