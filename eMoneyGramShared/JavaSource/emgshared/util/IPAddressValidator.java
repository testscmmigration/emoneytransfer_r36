package emgshared.util;

import org.apache.commons.lang.StringUtils;

/**
 * @author A004
 *
 */
public class IPAddressValidator {
	private String ipAddress;
	boolean ipBlank;
	boolean ipInvalid;
	String invalidReason;

    public IPAddressValidator(String ipAddress) {
		this.ipAddress = StringUtils.trimToNull(ipAddress);
		validate();
	}

    public boolean isIpBlank() {
		return ipBlank;
	}

    public boolean isIpInvalid() {
		return ipInvalid;
	}

    private final void validate() {
		this.ipBlank = (this.ipAddress == null);
		this.ipInvalid = meetsRequirements(this.ipAddress) == false;
	}

    public boolean formatIpString() {
		String ip = this.getIpAddress();
		String[] octets = ip.split("\\.");
		StringBuilder ipBuf = new StringBuilder();
		int count = -1, location = -1;

		do count++;
		while ((location = ip.indexOf('.', location + 1)) != -1);
		if (count > 3)
		{
			this.invalidReason = "Invalid Format";
			return false;
		}

        for (int i = 0; i < octets.length; i++) {
			octets[i] = StringHelper.prepad(octets[i], 3, '0');
            try {
				if (Integer.parseInt(octets[i]) > 255)
				{
					this.invalidReason =
						"Octet can not have value more than 255";
					return false;
                }
            } catch (NumberFormatException nfe) {
                this.invalidReason = "Invalid characters";
                return false;
            }
            if (i == 0) {
                ipBuf.append(octets[i]);
            } else {
                ipBuf.append(".");
                ipBuf.append(octets[i]);
            }
		}

		if (octets.length == 0)
		{
			ipBuf.append(ip);
			try
			{
				if (Integer.parseInt(ip) > 255)
				{
					this.invalidReason =
						"Octet can not have value more than 255";
					return false;
				}
			} catch (NumberFormatException nfe)
			{
				this.invalidReason = "Invalid characters";
				return false;
			}
		}

        if (octets.length == 1) {
			this.setIpAddress(ip);
			return true;
		}

		this.setIpAddress(ipBuf.toString());

		return true;
	}

    public static final boolean checkValidIpRange(String ip1, String ip2) {
		String[] ipStr1 = ip1.split("\\.");
		String[] ipStr2 = ip2.split("\\.");
		int[] ipNbr1 = { 0, 0, 0, 0 };
		int[] ipNbr2 = { 0, 0, 0, 0 };
		for (int i = 0; i < 4; i++)
		{
			ipNbr1[i] = Integer.parseInt(ipStr1[i]);
			ipNbr2[i] = Integer.parseInt(ipStr2[i]);
			if (ipNbr1[i] > ipNbr2[i])
			{
				return false;
			}
		}
		return true;
	}

    private final boolean meetsRequirements(String s) {
		if (s == null) {
			return false;
		}

		int count = -1, location = -1;
		do count++;
		while ((location = s.indexOf('.', location + 1)) != -1);
        if (count != 3) {
			this.invalidReason = "Invalid Format";
			return false;
        }

        try {

			String[] octets = s.split("\\.");

			String octet1 = octets[0];
			String octet2 = octets[1];
			String octet3 = octets[2];
			String octet4 = octets[3];

            if (octet1.indexOf("*") > -1) {
				this.invalidReason =
					"First Octet cannot have wild card character";
				return false;
			}

            if (octet2.indexOf("*") > -1) {
				this.invalidReason =
					"Second Octet cannot have wild card character";
				return false;
			}

            if (octet3.indexOf("*") > -1 && octet3.length() > 1) {
				this.invalidReason =
					"Third Octet can only have wild card character *, if present";
				return false;
			}

            if (octet4.indexOf("*") > -1 && octet4.length() > 1) {
				this.invalidReason =
					"Fourth Octet can only have wild card character *, if present";
				return false;
			}

            if (octet1.indexOf("*") == -1 && Integer.parseInt(octet1) > 255) {
					this.invalidReason =
						"First Octet cannot have value more than 255";
					return false;
				
			}

            if (octet2.indexOf("*") == -1 && Integer.parseInt(octet2) > 255) {

                this.invalidReason = "Second Octet cannot have value more than 255";
                return false;

            }

            if (octet3.indexOf("*") == -1 && Integer.parseInt(octet3) > 255) {
                this.invalidReason = "Third Octet cannot have value more than 255";
                return false;
            }
			
            if (octet4.indexOf("*") == -1 && Integer.parseInt(octet4) > 255) {
                this.invalidReason = "Fourth Octet cannot have value more than 255";
                return false;
            }
			

		} catch (NumberFormatException nfe)
		{
			this.invalidReason = "Invalid characters";
			return false;
		} catch (Exception e)
		{
			this.invalidReason = "Invalid format/characters";
			return false;
		}

		return true;
	}
	/**
	 * @return
	 */
    public String getInvalidReason() {
		return invalidReason;
	}

	/**
	 * @return
	 */
    public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param string
	 */
    public void setInvalidReason(String string) {
		invalidReason = string;
	}

	/**
	 * @param string
	 */
    public void setIpAddress(String string) {
		ipAddress = string;
	}

	/**
	 * @param b
	 */
    public void setIpBlank(boolean b) {
		ipBlank = b;
	}

	/**
	 * @param b
	 */
    public void setIpInvalid(boolean b) {
		ipInvalid = b;
	}

}
