/*
 * Created on Mar 8, 2004
 *
 */
package emgshared.util;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author A131
 *
 */
public final class I18NHelper
{
	public static final String DEFAULT_MSG_RESOURCES =
		"emgshared.resources.ApplicationResources";

	private I18NHelper()
	{
	}

	public static String getFormattedMessage(String messageKey)
	{
		return getFormattedMessage(messageKey, (String) null, (Locale) null);
	}

	public static String getFormattedMessage(String messageKey, Locale locale)
	{
		return getFormattedMessage(messageKey, (String) null, locale);
	}

	/**
	 * 
	 * @param messageKey
	 * @param resourceName - can be null, default provided.
	 * @param locale - can be null, default provided.
	 * @return
	 * 
	 * Created on Mar 1, 2005
	 */
	public static String getFormattedMessage(
		String messageKey,
		String resourceName,
		Locale locale)
	{
		if (resourceName == null)
		{
			resourceName = DEFAULT_MSG_RESOURCES;
		}

		if (locale == null)
		{
			locale = Locale.getDefault();
		}
		ResourceBundle msgBundle =
			ResourceBundle.getBundle(resourceName, locale);
		return (msgBundle.getString(messageKey));
	}

	public static String getFormattedMessage(
		String messageKey,
		Object[] params)
	{
		return (
			getFormattedMessage(
				messageKey,
				params,
				(String) null,
				(Locale) null));
	}

	/**
	 * 
	 * @param messageKey
	 * @param params
	 * @param resourceName - can be null, default provided.
	 * @return
	 * 
	 * Created on Mar 1, 2005
	 */
	public static String getFormattedMessage(
		String messageKey,
		Object[] params,
		String resourceName)
	{
		return (
			getFormattedMessage(
				messageKey,
				params,
				resourceName,
				(Locale) null));
	}

	/**
	 * 
	 * @param messageKey
	 * @param params
	 * @param resourceName - can be null, default provided.
	 * @param locale - can be null, default provided.
	 * @return
	 * 
	 * Created on Mar 1, 2005
	 */
	public static String getFormattedMessage(
		String messageKey,
		Object[] params,
		String resourceName,
		Locale locale)
	{
		if (resourceName == null)
		{
			resourceName = DEFAULT_MSG_RESOURCES;
		}

		if (locale == null)
		{
			locale = Locale.getDefault();
		}

		ResourceBundle msgBundle =
			ResourceBundle.getBundle(resourceName, locale);
		String msgTemplate = msgBundle.getString(messageKey);
		MessageFormat formatter = new MessageFormat(msgTemplate);
		formatter.setLocale(locale);
		return (formatter.format(params));
	}
}
