package emgshared.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class Constants {

	public static final String COMMENT_CODE_OTH="OTH";
	public static final String COMMENT_CODE_AVS="AVS";

	public static final String WAP_SITE_IDENTIFIER = "WAP";
	
	//MBO-6163
	public static final String DIGICEL_SITE_IDENTIFIER = "DIGICUSA";
	//ended

	public static final int MGO_PARTNER_SITE_CODE = 1;
	public static final int WAP_PARTNER_SITE_CODE = 2;
	public static final int MGOUK_PARTNER_SITE_CODE = 3;
	public static final int MGODE_PARTNER_SITE_CODE = 4;
	public static final int MGOZAF_PARTNER_SITE_CODE = 5;
	

	public static final String MGO_PARTNER_SITE_ID = "MGO";
	public static final String MGOUK_PARTNER_SITE_ID = "MGOUK";
	public static final String MGODE_PARTNER_SITE_ID = "MGODE";
	public static final String MGOZAF_PARTNER_SITE_ID = "MGOZAF";
	public static final String MGOFRA_PARTNER_SITE_ID = "MGOFRA";
	public static final String MGOESP_PARTNER_SITE_ID = "MGOESP";	
	public static final String WAP_PARTNER_SITE_ID = "WAP";
	public static final String ALL_PARTNER_SITE_ID = "ALL";
	public static final int PYMT_PROVIDER_CODE_GLOBAL_COLLECT = 2;

	public static final String APPROVED_DOC_STATUS = "APP";
	public static final String PENDING_DOC_STATUS = "PEN";
	public static final String DENIED_DOC_STATUS = "DEN";
	public static final String SUBMITTED_DOC_STATUS = "SUB";
	public static final String REQUESTED_DOC_STATUS = "REQ";
	
	public static final String MGI = "MGI";
	public static final String CYBERSOURCE = "CYBSRC";
	
	
	public static Map<Integer, String> partnerSiteCodeToId;
	public static Map<String, Integer> partnerSiteIdToCode;
	
	public static final String CREATE_USER="EMTADMIN";
	
	public static final long PHL_SMARTMONEY_AGENTID = 65599187;
	public static final String BPS = "BPS";
	public static final String RLS = "RLS";
	public static final String BPSCode = "1";
	public static final String RLSCode = "2";

	// Should come from the DB
	static {
		Map<Integer, String> tempCodeToId = new HashMap<Integer, String>();
		tempCodeToId.put(MGO_PARTNER_SITE_CODE, MGO_PARTNER_SITE_ID);
		tempCodeToId.put(WAP_PARTNER_SITE_CODE, WAP_PARTNER_SITE_ID);
		tempCodeToId.put(MGOUK_PARTNER_SITE_CODE, MGOUK_PARTNER_SITE_ID);
		tempCodeToId.put(MGODE_PARTNER_SITE_CODE, MGODE_PARTNER_SITE_ID);
		tempCodeToId.put(MGOZAF_PARTNER_SITE_CODE, MGOZAF_PARTNER_SITE_ID);
		partnerSiteCodeToId = Collections.unmodifiableMap(tempCodeToId);
		Map<String, Integer> tempIdToCode = new HashMap<String, Integer>();
		tempIdToCode.put(MGO_PARTNER_SITE_ID, MGO_PARTNER_SITE_CODE);
		tempIdToCode.put(WAP_PARTNER_SITE_ID, WAP_PARTNER_SITE_CODE);
		tempIdToCode.put(MGOUK_PARTNER_SITE_ID, MGOUK_PARTNER_SITE_CODE);
		tempIdToCode.put(MGODE_PARTNER_SITE_ID, MGODE_PARTNER_SITE_CODE);
		tempIdToCode.put(MGOZAF_PARTNER_SITE_ID, MGOZAF_PARTNER_SITE_CODE);
		partnerSiteIdToCode = Collections.unmodifiableMap(tempIdToCode);
	}
	//SITES INDENTIFIERS
	public static final String SITE_IDENTIFIER = "MGO";
	public static final String SITE_IDENTIFIER_UK = "MGOUK";
	public static final String SITE_IDENTIFIER_DE = "MGODE";	
	public static final String SITE_IDENTIFIER_ZAF = "MGOZAF";
    public static final String SITE_IDENTIFIER_INLANE = "INLANE";
    public static final String SITE_IDENTIFIER_WAP = "WAP";

    public static final String COUNTRY_IDENTIFIER_DE = "GER";
    
    
    public static String MIME_XLS="application/vnd.ms-excel";
    public static String MIME_XLXS="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    
    public static final String ACCT_MASK="****";
    public static final String STR_EMPTY="";
    
    //MBO-100:Profile Type
    public static final String PROFILE_TYPE_REGISTERED="Registered";
    public static final String PROFILE_TYPE_GUEST="Guest";
    
    //MBO-450 : Reason code 
    public static final String REASON_CODE="172";
	public static final String TRAN_COMMENT_REASON_CODE_OTHERS = "OTH";
	public static final String COMMENT_STATEMENT = "Attempt to update Cyber DM status to APPROVED failed";
	//MBO-461
	/*public static final float VERSION_NBR_UK_DE = (float) 1.1;
	public static final float VERSION_NBR_US = (float) 1.2;
	public static final float VERSION_NBR_NXT = (float) 2.0;*/
	//MBO-3749
	public static final float VERSION_WAP_NXT = (float) 2.5;
	//MBO-3619
	public static final float VERSION_NBR_WAP = (float) 2.0;	
	//MBO-1037 IP Address
	public static final String IP_ADDRESS_IPV6 = "0:0:0:0:0:0:0:1";
	public static final String IP_ADDRESS_IPV4 = "127.0.0.1";
	
	//MBO 2158 Email Age Changes
	public static final String RESULT_FORMAT ="xml";
	
	//MBO-2587 Email time zone
	public static final String TIME_UK="GMT";
	public static final String TIME_DE="CET";
	public static final String TIME_US="CST";
	
	//MBO-2938 Statistical score
	public static final String PROVIDER_CODE_MGI = "MGI";
	
	//MBO-4769
	public static final String COUNTRY_CODE_UK = "GBR";
	public static final String COUNTRY_CODE_DE = "DEU";
	public static final String COUNTRY_CODE_USA = "USA";
	
	//MBO-5499	
	public static final String COUNTRY_CODE_ZAF = "ZAF";
	
	public static final String COUNTRY_CODE_ESP = "ESP";
	
	public static final String COUNTRY_CODE_FRA = "FRA";
	
	//CAN-55
	public static final String COUNTRY_CODE_CAN = "CAN";
	
	//AUS-56 Added Constant File AUS
	public static final String COUNTRY_CODE_AUS = "AUS";
	
	//MBO-7547
	public static final String RESDNC_STATUS_TEMP_RESIDENT = "TEMPORARY RESIDENT";
	
	//EMT-2469
	public static final String ACTIMIZE_FAILED_STATEMENT = "Actimize Profiling Update failed";
	
	//PWMB-797
	public static final int PWMB_PROVIDER_CODE = 6;
	
}
