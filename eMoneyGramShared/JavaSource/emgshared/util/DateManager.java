package emgshared.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * The DateManager class have utility functions to convert dates to different time zones.
 * @author up30
 * @version 1.0 02/Jun/2011
 * @see java.util.TimeZone
 */
public class DateManager {

	public static final String CENTRAL_STANDARD_TIMEZONE_NAME = "America/Chicago";
	public static final String UK_TIMEZONE_NAME = "Europe/London";
	public static final String DE_TIMEZONE_NAME = "Europe/Berlin";
	public static final TimeZone CENTRAL_STANDARD_TIMEZONE = TimeZone.getTimeZone(CENTRAL_STANDARD_TIMEZONE_NAME);
	public static final TimeZone UK_TIMEZONE = TimeZone.getTimeZone(UK_TIMEZONE_NAME);
	public static final TimeZone DE_TIMEZONE = TimeZone.getTimeZone(DE_TIMEZONE_NAME);

	public static final String DASHBOARD_DATE_FORMAT = "dd/MMM/yyyy hh:mm a z"; 

	public static final String format(Date date, TimeZone to, String pattern) {
		DateFormat dateFormat = new SimpleDateFormat(pattern);
		dateFormat.setTimeZone(to);
		return dateFormat.format(date);
	}
	
	public static Date getCurrentDate()throws Exception{
    	Calendar cal = Calendar.getInstance();
    	return cal.getTime();
    }

}
