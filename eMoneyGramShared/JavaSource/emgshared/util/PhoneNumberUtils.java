package emgshared.util;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.owasp.esapi.ESAPI;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

import emgshared.dataaccessors.EMGSharedLogger;

public class PhoneNumberUtils {
	private static String CLASS_NAME = "PhoneNumberUtils";

	 private static Map<String, Locale> localeMap;
	 
	 public static boolean validPhoneNumber(String phoneNumber, String iso3Char) {

			boolean validPhoneNumber = false;
			String twoCharISOCode = iso3CountryCodeToIso2CountryCode(iso3Char);
			PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
			PhoneNumber phoneNum;
			try {
				if (StringUtils.isNotEmpty(phoneNumber)
						&& StringUtils.isNotEmpty(twoCharISOCode)) {
					phoneNum = phoneUtil.parse(phoneNumber, twoCharISOCode);
					validPhoneNumber = phoneUtil.isValidNumberForRegion(phoneNum,
							twoCharISOCode);
				} else {
					EMGSharedLogger
							.getLogger(ESAPI.encoder().encodeForHTMLAttribute(CLASS_NAME))
							.error("Both PhoneNumber and CountryDialingCode are mandatory to validate a phoneNumber"
									+ " "
									+ "DialingCode:"
									+ ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTML(twoCharISOCode))
									+ "PhoneNumber:" + ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTML(phoneNumber)));
				}
			} catch (NumberParseException e) {
				EMGSharedLogger
						.getLogger(CLASS_NAME)
						.error("Caught NumberParseException while trying to update phonenumber",
								e);
			}

			return validPhoneNumber;
		}
	 private static void initCountryCodeMapping() {
			String[] countries = Locale.getISOCountries();
			if (countries.length > 0) {
				localeMap = new HashMap<String, Locale>(countries.length);
				for (String country : countries) {
					Locale locale = new Locale("", country);
					localeMap.put(locale.getISO3Country().toUpperCase(), locale);
				}
			} else {
				EMGSharedLogger.getLogger(CLASS_NAME).error(
						"ISOCountries List is empty for this locale"
								+ Locale.getDefault());

			}
		}
	 
	    
	    public static String intlPhoneNumberFormat(String phoneNumberRaw,
				String countryCode) {

			String formatPhoneNum = null;
			PhoneNumber phoneNumber = null;
			PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
			try {
				phoneNumber = phoneUtil.parse(phoneNumberRaw,
						iso3CountryCodeToIso2CountryCode(countryCode));
				formatPhoneNum = phoneUtil.format(phoneNumber,
						PhoneNumberFormat.INTERNATIONAL);
			} catch (NumberParseException e) {
				EMGSharedLogger
				.getLogger(CLASS_NAME)
				.error("Error in International PhoneNumber Formatting:"	+ phoneNumberRaw + ":" + e.getMessage(), e);

			}
			return formatPhoneNum;
		}
	    private static String iso3CountryCodeToIso2CountryCode(
				String iso3CountryCode) {
			if (StringUtils.isNotEmpty(iso3CountryCode)) {
				initCountryCodeMapping();
				if (localeMap.containsKey(iso3CountryCode)) {
					return localeMap.get(iso3CountryCode).getCountry();
				} else {
					
			    	if(isISO3CountryCodeValid(iso3CountryCode))
					EMGSharedLogger.getLogger (ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTML(CLASS_NAME))).error(
							"Given Country" + ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTML(iso3CountryCode))
									+ "is not found in the locale map");
					return null;
				}
			} else {
				EMGSharedLogger.getLogger(CLASS_NAME).error(
						"CountryDialingCode is not present");
				return null;
			}
		}
	    
	    private static boolean isISO3CountryCodeValid(String iso3CountryCode) {
			Matcher matcher = null;
			Pattern pattern = Pattern.compile("[A-Z]*");
			if (null != iso3CountryCode) {
				matcher = pattern.matcher(iso3CountryCode);
			}
			if (matcher.matches() && iso3CountryCode.length() == 3) {
				return true;
			} else {
				return false;
			}
		}
}
