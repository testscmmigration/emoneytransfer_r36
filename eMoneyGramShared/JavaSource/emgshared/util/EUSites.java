package emgshared.util;


public enum EUSites {
	MGOAUT("AUT"), MGOBEL("BEL"), MGOBGR("BGR"), MGOHRV("HRV"), MGOCZE("CZE"), MGODNK("DNK"), MGOEST("EST"), MGOFIN("FIN"), MGOGRC("GRC"), MGOHUN("HUN"), MGOISL("ISL"), MGOIRL("IRL"), MGOITA("ITA"), MGOITL("ITL"), MGOLVA("LVA"), MGOLTU("LTU"), MGOLUX("LUX"), MGOMLT("MLT"), MGONLD("NLD"), MGONOR("NOR"), MGOPOL("POL"), MGOPRT("PRT"), MGOROU("ROU"), MGOSVK("SVK"), MGOSVN("SVN"), MGOSWE("SWE"), MGOCHE("CHE");

	private String value;

	EUSites(String value) {
		this.value = value;
	}

	public String toString() {
		return String.valueOf(value);
	}

	public static EUSites fromValue(String eusite) {
		for (EUSites v : EUSites.values()) {
			if (String.valueOf(v.value).equals(eusite)){
				return v;
			}
		}
		return null;
	}
	
	public static boolean contains(String site) {
		for (EUSites b : EUSites.values()) {
			if (String.valueOf(b.value).equals(site)){
				return true;
			}
		}
		return false;
	}
}
