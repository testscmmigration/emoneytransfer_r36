package emgshared.property;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import shared.mgo.dto.MGOAgentProfile;
import shared.mgo.dto.MGOProduct;
import shared.mgo.services.AgentConnectAccessor;
import shared.mgo.services.MGOAgentProfileService;
import shared.mgo.services.MGOServiceFactory;



import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.model.StateProvinceLimit;
import emgshared.util.Constants;
import emgshared.util.StringHelper;

public class EMTSharedContainerProperties
{
    private static boolean useGCForProcessing;
	private static String MerchantIdGC_Telecom;
	private static String MerchantIdGC_Other;
	private static String MerchantIdGC_Mortgage;
	private static String MerchantIdGC_Auto;
	private static String MerchantIdTelecom;
	private static String MerchantIdOther;
	private static String MerchantIdMortgage;
	private static String MerchantIdAuto;
	private static String cyberSourceKeysDirectory;
	private static String cyberSourceApiVersion;
	private static String cyberSourceUseProduction;
	private static String cyberSourceLogEnabled;
	private static String cyberSourceLogDirectory;
	private static String cyberSourceLogMaxSizeMb;
	private static String cyberSourceTimeoutSeconds;
	private static String publicRSAKey;
	private static String mailHost;
	private static String mailFrom;
	private static String mailACTo;
	private static String CyberSourceURL;
	private static Hashtable stateEPLimits;

	private static String eventMonDir;
	private static int eventMonTimeInterval;
	private static String avsDeclineFlags;
	private static String pciEncryptServiceURL;
	private static String pciDecryptServiceURL;
	private static String pciDecryptUserName;
	private static String pciDecryptPassword;
	private static int maxBackGroundProcessorErrors;
	private static Map merchantMap;
	private static String secureHashSeed;
	//MBO-6817
	private static String configServiceUrl = null;
	private static String  hmacsha1Key = null;
	private static String  unReservedCharacters = null;
	//FD-131
	private static String  accountServiceURL = null;
	
	private static Logger LOGGER = EMGSharedLogger.getLogger(
			EMTSharedContainerProperties.class);
	public static void initialize(PropertyLoader pv)
	{
   		merchantMap = new HashMap();

   		MerchantIdGC_Telecom =	pv.getString("MerchantIdGC_Telecom");
   		merchantMap.put("MerchantIdGC_Telecom", MerchantIdGC_Telecom);
   		MerchantIdGC_Other =	pv.getString("MerchantIdGC_Other");
   		merchantMap.put("MerchantIdGC_Other", MerchantIdGC_Other);
   		MerchantIdGC_Mortgage =	pv.getString("MerchantIdGC_Mortgage");
   		merchantMap.put("MerchantIdGC_Mortgage", MerchantIdGC_Mortgage);
   		MerchantIdGC_Auto =	pv.getString("MerchantIdGC_Auto");
   		merchantMap.put("MerchantIdGC_Auto", MerchantIdGC_Auto);

   		MerchantIdTelecom = pv.getString("MerchantIdTelecom");
   		merchantMap.put("MerchantIdTelecom", MerchantIdTelecom);
   		MerchantIdOther = pv.getString("MerchantIdOther");
   		merchantMap.put("MerchantIdOther", MerchantIdOther);
   		MerchantIdMortgage = pv.getString("MerchantIdMortgage");
   		merchantMap.put("MerchantIdMortgage", MerchantIdMortgage);
   		MerchantIdAuto = pv.getString("MerchantIdAuto");
   		merchantMap.put("MerchantIdAuto", MerchantIdAuto);
   		
   		
        useGCForProcessing = (pv.getString("USE_GC_For_Processing")).equalsIgnoreCase("true")?true:false;

   		//		cyberSourceMerchantId =	      pv.getString("CyberSourceMerchantId");
		cyberSourceKeysDirectory =    pv.getString("CyberSourceKeysDirectory");
		cyberSourceApiVersion =	      pv.getString("CyberSourceAPIVersion");
		cyberSourceUseProduction =    pv.getString("CyberSourceUseProduction");
		cyberSourceLogEnabled =	      pv.getString("CyberSourceLogEnabled");
		CyberSourceURL =	      	  pv.getString("CyberSourceURL");
		cyberSourceLogDirectory =     pv.getString("CyberSourceLogDirectory");
		cyberSourceLogMaxSizeMb =     pv.getString("CyberSourceLogMaxSizeMb");
		cyberSourceTimeoutSeconds =   pv.getString("CyberSourceTimeoutSeconds");

		publicRSAKey =	pv.getString("PublicRSAKey");
		mailHost =    pv.getString("MailHost");
		mailFrom =    pv.getString("MailFrom");
		mailACTo =    pv.getString("MailACTo");
		eventMonDir = pv.getString("EventMonDir");
		eventMonTimeInterval =	pv.getInteger("EventMonTimeInterval");

       
		maxBackGroundProcessorErrors = pv.getInteger("MaxBackGroundProcessorErrors");
		avsDeclineFlags =	pv.getString("AVSDeclineFlags");
		pciEncryptServiceURL = pv.getString("PCIEncryptServiceURL");
		pciDecryptServiceURL = pv.getString("PCIDecryptServiceURL");
		pciDecryptUserName = pv.getString("PCIDecryptUserName");
		pciDecryptPassword = pv.getString("PCIDecryptPwd");

		secureHashSeed = pv.getString("SecureHashSeed_password");
		//MBO-6817 
		  configServiceUrl = pv.getString("MGO_CONFIGURATION_SERVICE_URL");
		  hmacsha1Key = pv.getString("HMACSHA1_KEY");
		  unReservedCharacters = pv.getString("UNRESERVED_CHARACTERS");
		  
		  accountServiceURL = pv.getString("ACCOUNT_SERVICE_URL");
		//get state imposed limits for EP transactions
		//These are limits for online transaction which will override anything that we
		//get from AgentConnect (because AC doesn't know we're online and not a brick and mortar shop).
		String cnt;
		if (null != ( cnt = pv.getString("EP.StateLimitCount"))) {
			int counter = Integer.parseInt(cnt);
			String stateLimitItem = null;
			for(int x = 0; x < counter; x++) {
				//Make sure we have somthing to parse.
				if(null != (stateLimitItem = pv.getString("EP.StateLimit." + x))) {
					String limitInfo[] = StringHelper.split(stateLimitItem, ":");
					//if the parm is the right shape, create a limit objec that
					//and put it into the limits collection.
					if (limitInfo.length == 3) {
						StateProvinceLimit spl = new StateProvinceLimit();
						spl.setStateAbbrev(limitInfo[0]);
						spl.setLimit(limitInfo[1]);
						spl.setStateName(limitInfo[2]);
						if (null == stateEPLimits) {
							 stateEPLimits = new Hashtable();
						}
						stateEPLimits.put(limitInfo[0],spl);
					}
				}
			}
		}
	}

	/**
     * Since these Axis Wrappers require Environment properties to
     * initialize, they're included here. No other reason.
     * @param rawEnvironmentProps
     * @param contextLogger - Log lines tagged for application.
     * @param echoToSystemOut
     * @return false if successful, true if error.
	 */
	 //changing method signature for MBO-6101 as Cache service
	 //is not accessible in emgshared package, so Agent profile 
	 //object will be passed from calling method itself
    public static boolean initAgentConnectAccessors(
        Properties rawEnvironmentProps,
        Logger contextLogger,
        boolean echoToSystemOut, MGOAgentProfile mgoAgentProfile)
    {
        String message = null;
        boolean errorFlag = false;
        String D = (new Date()).toString() + " ";

        /*-------------------------------------------------------------------*/
        message =  "LOADING: Initializing AgentConnect.";
        if ( echoToSystemOut ) System.out.println(D + message);
        contextLogger.info(message);
        try
        {
        	//MBO-6101
			/*MGOAgentProfileService maps = MGOServiceFactory.getInstance().getMGOAgentProfileService();
	        MGOAgentProfile mgoAgentProfile = maps.getMGOProfile("MGO",MGOProduct.SAME_DAY);*/
        	//ended
            new AgentConnectAccessor(rawEnvironmentProps,mgoAgentProfile);
            message = "SUCCESS: AgentConnect Initialized.";
            if ( echoToSystemOut ) System.out.println(D + message);
            contextLogger.info(message);
        }
        catch (Exception e)
        {
        	LOGGER.error(e.getMessage(),e);
            message = "FAILED: AgentConnect initialization.";
            if ( echoToSystemOut ) System.out.println(D + message);
            contextLogger.error(message);
            errorFlag = true;
        }
        return errorFlag;
    }


    public static String convertToGlobalCollectMerchantId(String emgMerchantId) {
    	if(emgMerchantId.contains("auto")) {
    		return getMerchantIdGC_Auto();
    	} else if(emgMerchantId.contains("mortgage")) {
    		return getMerchantIdGC_Mortgage();
    	} else if(emgMerchantId.contains("other")) {
    		return getMerchantIdGC_Other();
    	} else if(emgMerchantId.contains("telecom")) {
    		return getMerchantIdGC_Telecom();
    	}

    	throw new IllegalArgumentException("EMG MerchantId not recognized");
    }

    private EMTSharedContainerProperties()
	{
	}


	public static String getMerchantIdTelecom() {
		return MerchantIdTelecom;
	}

	public static String getMerchantIdOther() {
		return MerchantIdOther;
	}

	public static String getMerchantIdMortgage() {
		return MerchantIdMortgage;
	}

	public static String getMerchantIdAuto() {
		return MerchantIdAuto;
	}

	public static String getMerchantIdGC_Telecom() {
		return MerchantIdGC_Telecom;
	}

	public static String getMerchantIdGC_Other() {
		return MerchantIdGC_Other;
	}

	public static String getMerchantIdGC_Mortgage() {
		return MerchantIdGC_Mortgage;
	}

	public static String getMerchantIdGC_Auto() {
		return MerchantIdGC_Auto;
	}

	public static String getCyberSourceApiVersion()
	{
		return cyberSourceApiVersion;
	}

	public static String getCyberSourceKeysDirectory()
	{
		return cyberSourceKeysDirectory;
	}

	public static String getCyberSourceLogDirectory()
	{
		return cyberSourceLogDirectory;
	}

	public static String getCyberSourceLogEnabled()
	{
		return cyberSourceLogEnabled;
	}

	public static String getCyberSourceLogMaxSizeMb()
	{
		return cyberSourceLogMaxSizeMb;
	}

	public static String getCyberSourceUseProduction()
	{
		return cyberSourceUseProduction;
	}

	public static String getCyberSourceTimeoutSeconds()
	{
		return cyberSourceTimeoutSeconds;
	}
	public static String getPublicRSAKey()
	{
		return publicRSAKey;
	}

	/*********************************************************************/
	/*    Mail                                                           */
	/*********************************************************************/
	public static String getMailHost()
	{
		return mailHost;
	}

	public static String getMailFrom()
	{
		return mailFrom;
	}

	public static String getMailACTo()
	{
		return mailACTo;
	}

	/**********************************************************************/
	/*    Event Monitor Serialize object directory and save time interval */
	/**********************************************************************/
	public static String getEventMonDir()
	{
		return eventMonDir;
	}

	//  to serialize object (in minutes)
	public static int getEventMonTimeInterval()
	{
		return eventMonTimeInterval;
	}

	/**
	 * Return a state emposed limit for EP transactions.
	 * @param state
	 * @return
	 */
	public static StateProvinceLimit getEPStateLimit(String state) {
		return (StateProvinceLimit) stateEPLimits.get(state);
	}

	public static String getAvsDeclineFlags()
	{
		return avsDeclineFlags;
	}

	public static String getPciEncryptServiceURL()
	{
		return pciEncryptServiceURL;
	}

	public static String getPciDecryptServiceURL()
	{
		return pciDecryptServiceURL;
	}

	public static String getPciDecryptUserName()
	{
		return pciDecryptUserName;
	}

	public static String getPciDecryptPassword()
	{
		return pciDecryptPassword;
	}
	 public static int getMaxBackGroundProcessorErrors() {
		return maxBackGroundProcessorErrors;
	}

	public static void setMaxBackGroundProcessorErrors(int maxBackGroundProcessorErrors) {
		EMTSharedContainerProperties.maxBackGroundProcessorErrors = maxBackGroundProcessorErrors;
	}

   	public static Map getMerchantMap() {
		return merchantMap;
	}

	public static void setMerchantMap(Map merchantMap) {
		EMTSharedContainerProperties.merchantMap = merchantMap;
	}

	public static boolean getUseGCForProcessing() {
		return useGCForProcessing;
	}

	public static String getSecureHashSeed() {
		return secureHashSeed;
	}

	public static void setCyberSourceURL(String cyberSourceURL) {
		CyberSourceURL = cyberSourceURL;
	}

	public static String getCyberSourceURL() {
		return CyberSourceURL;
	}
	public static String getConfigServiceUrl(){
		return configServiceUrl;
	}

	public static String getHmacsha1Key() {
		return hmacsha1Key;
	}

	public static void setHmacsha1Key(String hmacsha1Key) {
		EMTSharedContainerProperties.hmacsha1Key = hmacsha1Key;
	}

	public static String getUnReservedCharacters() {
		return unReservedCharacters;
	}

	public static void setUnReservedCharacters(String unReservedCharacters) {
		EMTSharedContainerProperties.unReservedCharacters = unReservedCharacters;
	}

	/**
	 * @return the accountServiceURL
	 */
	public static String getAccountServiceURL() {
		return accountServiceURL;
	}

	/**
	 * @param accountServiceURL the accountServiceURL to set
	 */
	public static void setAccountServiceURL(String accountServiceURL) {
		EMTSharedContainerProperties.accountServiceURL = accountServiceURL;
	}
	
	

}