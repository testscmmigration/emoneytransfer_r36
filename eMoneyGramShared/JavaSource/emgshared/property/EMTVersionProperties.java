package emgshared.property;


public class EMTVersionProperties {
	private static String version;
	private static int buildNumber;
	private static String tagPrefix;
	private static String buildDate;
	private static String builtBy;
	
	public static void initialize(PropertyLoader pv)
	{
		version = pv.getString("version");
		buildNumber = pv.getInteger("build.number");
		tagPrefix = pv.getString("tag.prefix");
		buildDate = pv.getString("build.date");
		builtBy = pv.getString("by");
	}

	public static String getBuildDate() {
		return buildDate;
	}

	public static int getBuildNumber() {
		return buildNumber;
	}

	public static String getBuiltBy() {
		return builtBy;
	}

	public static String getTagPrefix() {
		return tagPrefix;
	}

	public static String getVersion() {
		return version;
	}
	
	public static String getBuildString() {
		return tagPrefix + "_" + buildNumber;
	}
}
