package emgshared.simplesoap;

/**
 * @author G.R.Svenddal
 * Created on Jun 22, 2005
 * ValidateEnrollRequest.java
 *  
 * 
 *  */
public class AuthValidateRequest {
    private String merchantReferenceCode;
    private String signedPARes;
    private int account;
    
   
    /**
     * @return Returns the account.
     */
    public int getAccount() {
        return account;
    }
    /**
     * @return Returns the merchantReferenceCode.
     */
    public String getMerchantReferenceCode() {
        return merchantReferenceCode;
    }
    /**
     * @return Returns the signedPARes.
     */
    public String getSignedPARes() {
        return signedPARes;
    }
    /**
     * @param account The account to set.
     */
    public void setAccount(int account) {
        this.account = account;
    }
    /**
     * @param merchantReferenceCode The merchantReferenceCode to set.
     */
    public void setMerchantReferenceCode(String merchantReferenceCode) {
        this.merchantReferenceCode = merchantReferenceCode;
    }
    /**
     * @param signedPARes The signedPARes to set.
     */
    public void setSignedPARes(String signedPARes) {
        this.signedPARes = signedPARes;
    }
}
