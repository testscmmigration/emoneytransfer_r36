
package emgshared.exceptions;


public class DuplicateActiveProfileException extends EMGBaseException
{
	public DuplicateActiveProfileException()
	{
		super();
	}

	public DuplicateActiveProfileException(String s)
	{
		super(s);
	}

	public DuplicateActiveProfileException(Throwable rootCause)
	{
		super(rootCause);
	}
}
