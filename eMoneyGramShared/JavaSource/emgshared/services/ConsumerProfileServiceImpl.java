/*
 * Created on Jan 6, 2005
 *
 */
package emgshared.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import org.apache.commons.lang.StringUtils;
import emgshared.dataaccessors.ConsumerAccountDAO;
import emgshared.dataaccessors.ConsumerProfileDAO;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.dataaccessors.QuestionDAO;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.DuplicateActiveProfileException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.exceptions.SameStatusException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.AccountSearchRequest;
import emgshared.model.AccountStatus;
import emgshared.model.BlockedIP;
import emgshared.model.ConsumerAccount;
import emgshared.model.ConsumerAddress;
import emgshared.model.ConsumerDocument;
import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileActivity;
import emgshared.model.ConsumerProfileComment;
import emgshared.model.ConsumerProfileSearchCriteria;
import emgshared.model.ConsumerStatus;
import emgshared.model.CustomerAuthenticationInfo;
import emgshared.model.LexisNexisActivity;
import emgshared.model.LogonFailure;
import emgshared.model.NewConsumerProfile;
import emgshared.model.SearchCriteriaDTO;
import emgshared.model.TaintIndicatorStatus;
import emgshared.model.TaintIndicatorType;
import emgshared.property.EMTSharedDynProperties;
/**
 * @author A131
 *
 */
class ConsumerProfileServiceImpl implements ConsumerProfileService {
	private static final ConsumerProfileService instance = new ConsumerProfileServiceImpl();

	public static Map verificationQuestions = new HashMap(0);

	public static Map consumerStatusDescriptions = new HashMap(0);

	public static Map consumerCommentReasons = new HashMap(0);
	
	public static Map<String, String> statesProvinces = new HashMap<String, String>();

	private ConsumerProfileServiceImpl() {
	}

	public static final ConsumerProfileService getInstance() {
		return instance;
	}

	public int addConsumerProfile(NewConsumerProfile consumerProfile) throws DataSourceException {
		int newProfileId;
		ConsumerProfileDAO dao = new ConsumerProfileDAO();
		try {
			newProfileId = dao.insertProfile(consumerProfile);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} catch (Exception ex) {
			dao.rollback();
			throw new EMGRuntimeException(ex);
		} finally {
			dao.close();
		}
		return newProfileId;
	}

	public ConsumerProfile getConsumerProfile(Integer consumerId, String callerLoginId,
			String consumerLogonId) throws DataSourceException {
		return getConsumerProfile(consumerId,  callerLoginId,
				 consumerLogonId, true);
		}

	public ConsumerProfile getConsumerProfile(Integer consumerId, String callerLoginId,
			String consumerLogonId, boolean isFilterActive) throws DataSourceException {
		ConsumerProfile selectedProfile;
		ConsumerProfileDAO daoProfile = new ConsumerProfileDAO();
		ConsumerAccountDAO daoAccount = new ConsumerAccountDAO();
		try {
			selectedProfile = daoProfile.select(consumerId, callerLoginId, consumerLogonId);
			if (selectedProfile != null) {
				AccountSearchRequest searchCriteria = new AccountSearchRequest();
				searchCriteria.setCustId(Integer.valueOf(selectedProfile.getId()));
				// Code use to set
				// searchCriteria.setStatusCode(AccountStatus.ACTIVE_CODE);
				// to get only the active accounts but now we need to inspect
				// all accounts to see if any are blocked.
				selectedProfile.setAccountTaintCode(TaintIndicatorType.NOT_BLOCKED);
				Set accountSet = daoAccount.getAccounts(searchCriteria, callerLoginId);
				ServiceFactory service = ServiceFactory.getInstance();
				EMTSharedDynProperties dynProps = new EMTSharedDynProperties();
				for (Iterator it = accountSet.iterator(); it.hasNext();) {
					ConsumerAccount account = (ConsumerAccount) it.next();
					if (account.getAccountNumberTaintCode() != null) {
						account.setAccountNumberTaintCode(TaintIndicatorType.getInstance(account
								.getAccountNumberTaintCode().toString()));
					}
					if (account.getBankAbaTaintCode() != null) {
						account.setBankAbaTaintCode(TaintIndicatorType.getInstance(account
								.getBankAbaTaintCode().toString()));
					}
					if (account.getCreditCardBinTaintCode() != null) {
						account.setCreditCardBinTaintCode(TaintIndicatorType.getInstance(account
								.getCreditCardBinTaintCode().toString()));
					}
					if (!selectedProfile.isAccountBlocked()) {
						if (account.isAccountNumberBlocked() || account.isBankAbaNumberBlocked()
								|| account.isCreditCardBinBlocked()) {
							selectedProfile.setAccountTaintCode(TaintIndicatorType.BLOCKED);
						} else if (account.isAccountNumberOverridden()
								|| account.isBankAbaNumberOverridden()
								|| account.isCreditCardBinOverridden()) {
							selectedProfile.setAccountTaintCode(TaintIndicatorType.OVERRIDE);
						} else {
							if (!selectedProfile.getAccountTaintCode().isOverridden()) {
								selectedProfile.setAccountTaintCode(TaintIndicatorType.NOT_BLOCKED);
							}
						}
					}
					// isFilterActive flag is true and with Account is Active - only active accounts will be added to profile .
					// isFilterActive flag is false then irrespective of Account status will be added to profile.
					if (!isFilterActive || account.getStatusCode().equals(AccountStatus.ACTIVE_CODE.toString())) {
						selectedProfile.addAccount(account);
					}
				}
				daoAccount.commit();
			}
			daoProfile.commit();
		} catch (DataSourceException e) {
			daoProfile.rollback();
			daoAccount.rollback();
			throw e;
		} catch (Exception ex) {
			daoProfile.rollback();
			daoAccount.rollback();
			throw new EMGRuntimeException(ex);
		} finally {
			daoProfile.close();
			daoAccount.close();
		}
		return selectedProfile;
	}
	
	public List<ConsumerAddress> getConsumerAddressList(ConsumerProfile consumerProfile)  throws DataSourceException{
		List<ConsumerAddress> consumerAddressList;
		ConsumerProfileDAO daoProfile = new ConsumerProfileDAO();
		consumerAddressList = daoProfile.getConsumerAddressList(consumerProfile.getId());
		return consumerAddressList;
	}

	public Map getVerificationQuestions() throws DataSourceException {
		synchronized (verificationQuestions) {
			/*
			 * 20050301 - Logging added to facilitate debugging SCR 23
			 */
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						"In ConsumerProfileServiceImpl.getVerificationQuestions() "
								+ (verificationQuestions == null ? "verificationQuestions is null"
										: "verificationQuestions contains "
												+ verificationQuestions.size() + " elements."));
			}

			if (verificationQuestions.isEmpty()) {
				QuestionDAO dao = new QuestionDAO();
				try {
					verificationQuestions = Collections.unmodifiableMap(dao.getQuestions());
					dao.commit();
				} catch (DataSourceException e) {
					dao.rollback();
					throw e;
				} catch (Exception ex) {
					dao.rollback();
					throw new EMGRuntimeException(ex);
				} finally {
					dao.close();
				}
			}
		}
		return verificationQuestions;
	}

	public boolean isExistingUserId(String userId) throws DataSourceException {
		boolean exists;
		ConsumerProfileDAO dao = new ConsumerProfileDAO();
		try {
			exists = dao.isExistingUserId(userId);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} finally {
			dao.close();
		}
		return exists;
	}

	public String getVerificationQuestionText(int id) throws DataSourceException {
		return getVerificationQuestionText(Integer.valueOf(id));
	}

	public String getVerificationQuestionText(Integer id) throws DataSourceException {
		return (String) getVerificationQuestions().get(id);
	}

	public String[] getVerificationQuestionTextByCust(String userId, int custId, int verifyQuestId)
			throws DataSourceException {
		ConsumerProfileDAO dao = new ConsumerProfileDAO();
		String[] strArray = new String[2];
		try {
			strArray = dao.selectConsumerQuestion(userId, custId, verifyQuestId);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} finally {
			dao.close();
		}
		return strArray;
	}


	public ConsumerProfile updateConsumerProfile(ConsumerProfile consumerProfile, boolean bypassDupCheck, String updatedByUserId)
			throws DataSourceException,DuplicateActiveProfileException {
		return updateConsumerProfile(consumerProfile, bypassDupCheck, updatedByUserId, null);
	}

	public ConsumerProfile updateConsumerProfile(ConsumerProfile consumerProfile, boolean bypassDupCheck, String updatedByUserId, String sourceSite)
			throws DataSourceException,DuplicateActiveProfileException {
		ConsumerProfile savedProfile = null;
		ConsumerProfileDAO dao = new ConsumerProfileDAO();
		boolean exists = false;
		try {
			if (!bypassDupCheck) {
				ConsumerProfile oldProfile = getConsumerProfile(Integer.valueOf(consumerProfile.getId()), "web", (String) null);
				String addressLine1 = "    ";
				if (consumerProfile.getAddressLine1() != null) {
					if (consumerProfile.getAddressLine1().length() > 4)
						addressLine1 = consumerProfile.getAddressLine1().substring(0, 5);
					else
						addressLine1 = consumerProfile.getAddressLine1();
				}
				exists = dao.isDuplicateActiveCustomer(consumerProfile.getId(),consumerProfile.getBirthdate(), oldProfile.getSsnLast4(), consumerProfile.getLastName(), addressLine1, "WEB",consumerProfile.getUserId(),consumerProfile.getIsoCountryCode());
			}
			if (!exists)
				dao.updateProfile(consumerProfile,updatedByUserId, sourceSite);
			else {
				throw new DuplicateActiveProfileException("Consumer Profile changes resulted in a Duplicate Profile found.");
			}
			Integer consumerId = Integer.valueOf(consumerProfile.getId());
			dao.commit();
			savedProfile = getConsumerProfile(consumerId, String.valueOf(consumerId), null);
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} catch (DuplicateActiveProfileException dup) {
			dao.rollback();
			throw dup;
		} catch (Exception ex) {
			dao.rollback();
			throw new EMGRuntimeException(ex);
		} finally {
			dao.close();
		}
		return savedProfile;
	}

	public int updatePassword(String userId, int custId, String password, String existingPassword)
			throws DataSourceException {
		return updatePassword(userId, custId, password, existingPassword, (Date) null);
	}

	public int updatePassword(String userId, int custId, String password, String existingPassword,
			Date expirationDate) throws DataSourceException {
		ConsumerProfileDAO dao = new ConsumerProfileDAO();
		int updateCount = 0;
		try {
			updateCount = dao.updatePassword(userId, custId, password, existingPassword,
					expirationDate);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} catch (Exception e) {
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally {
			dao.close();
		}
		return updateCount;
	}

	public void updateConsumerRewardsInfo(String userId, int consumerId, String rewardsNumber,
			String consumerAutoEnrollFlag) throws DataSourceException {
		ConsumerProfileDAO dao = new ConsumerProfileDAO();
		try {
			dao.updateRewardsInfo(userId, "WEB", consumerId, rewardsNumber, consumerAutoEnrollFlag);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} finally {
			dao.close();
		}
	}

	public void updateEmailStatus(String userId, ConsumerEmail consumerEmail)
			throws DataSourceException {
		ConsumerProfileDAO dao = new ConsumerProfileDAO();
		try {
			dao.updateElectronicEmail(userId, consumerEmail);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} finally {
			dao.close();
		}
	}

	public ConsumerProfile updateStatus(int custId, ConsumerStatus newStatus, String callerLoginId,
			boolean bypassDupCheck) throws DataSourceException, SameStatusException,
			DuplicateActiveProfileException {
		ConsumerProfile oldProfile = getConsumerProfile(Integer.valueOf(custId), callerLoginId,
				(String) null);
		ConsumerStatus oldStatus = oldProfile.getStatus();
		
		boolean exists = true;

		// if status did not change, there is nothing to do here
		if (oldStatus.getCombinedCode().equals(newStatus.getCombinedCode())) {
			throw new SameStatusException(
					"Old Status same as New Status on request to updateStatus.");
		} else if ((newStatus.getStatusCode().equalsIgnoreCase("ACT"))
				&& (!oldStatus.getStatusCode().equalsIgnoreCase("ACT"))) {
			// if activating an inactive profile, ensure an active profile does
			// not already exist
			String addressLine1 = "    ";
			if (oldProfile.getAddressLine1() != null) {
				if (oldProfile.getAddressLine1().length() > 4)
					addressLine1 = oldProfile.getAddressLine1().substring(0, 5);
				else
					addressLine1 = oldProfile.getAddressLine1();
			}
			ConsumerProfileDAO dao = new ConsumerProfileDAO();
			try {
				if (bypassDupCheck)
					exists = false;
				else
					exists = dao.isDuplicateActiveCustomer(custId,oldProfile.getBirthdate(), oldProfile
							.getSsnLast4(), oldProfile.getLastName(), addressLine1, "WEB",oldProfile.getUserId(),oldProfile.getIsoCountryCode());
			} catch (DataSourceException e) {
				throw e;
			} catch (Exception ex) {
				throw new EMGRuntimeException(ex);
			} finally {
				dao.close();
			}
		} // end else if condition
		else
			// changing to something other than active, doesn't matter if there
			// are dups
			exists = false;
		// MBO-7271
		if (null != newStatus && null != oldStatus) {
			oldProfile.setPhoneNumber(null);
		}
		// MBO-7271
		if (exists == false) {
			if (ConsumerStatus.ACTIVE_VALIDATION_LEVEL_2.equals(newStatus)
					|| ConsumerStatus.ACTIVE_VALIDATION_LEVEL_3.equals(newStatus)) {
				oldProfile.setAuthStatusFlag(true);
			} else if (ConsumerStatus.ACTIVE_VALIDATION_LEVEL_1.equals((newStatus))) {
				oldProfile.setAuthStatusFlag(false);
			}
			oldProfile.setStatus(newStatus);
			return (updateConsumerProfile(oldProfile,true,callerLoginId));
		} else {
			throw new DuplicateActiveProfileException(
					"Profile is already associated with an Active Profile");
		}
	}

	public void updateSsn(int consumerId, String ssnMask, String callerLoginId,boolean bypassDupCheck)
			throws DataSourceException, DuplicateActiveProfileException {
		// boolean duplicateSsn;
		ConsumerProfileDAO dao = new ConsumerProfileDAO();
		boolean exists = false;
		try {

			if (bypassDupCheck)
				exists = false;
			else {
				ConsumerProfile oldProfile = getConsumerProfile(Integer.valueOf(consumerId), callerLoginId, (String) null);
				String addressLine1 = "    ";
				if (oldProfile.getAddressLine1() != null) {
					if (oldProfile.getAddressLine1().length() > 4)
						addressLine1 = oldProfile.getAddressLine1().substring(0, 5);
					else
						addressLine1 = oldProfile.getAddressLine1();
				}
				exists = dao.isDuplicateActiveCustomer(consumerId,oldProfile.getBirthdate(), ssnMask, oldProfile.getLastName(), addressLine1, "WEB",oldProfile.getUserId(),oldProfile.getIsoCountryCode());
			}
			if (exists)
				throw new DuplicateActiveProfileException("SSN update will result in a Duplicate Profile");
			else
				dao.updateSsn(consumerId, ssnMask, callerLoginId);
			dao.commit();
		} catch (DuplicateActiveProfileException dape) {
			dao.rollback();
			throw dape;
		}
		catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} catch (Exception ex) {
			dao.rollback();
			throw new EMGRuntimeException(ex);
		} finally {
			dao.close();
		}
		return;
	}

	public Map getConsumerStatusDescriptions() throws DataSourceException {
		synchronized (consumerStatusDescriptions) {
			if (consumerStatusDescriptions.isEmpty()) {
				ConsumerProfileDAO dao = new ConsumerProfileDAO();
				try {
					consumerStatusDescriptions = Collections.unmodifiableMap(dao
							.getConsumerStatusDescriptions());
					dao.commit();
				} catch (DataSourceException e) {
					dao.rollback();
					throw e;
				} catch (Exception ex) {
					dao.rollback();
					throw new EMGRuntimeException(ex);
				} finally {
					dao.close();
				}
			}
		}
		return consumerStatusDescriptions;
	}

	public Map getConsumerCommentReasons() throws DataSourceException {
		synchronized (consumerCommentReasons) {
			if (consumerCommentReasons.isEmpty()) {
				ConsumerProfileDAO dao = new ConsumerProfileDAO();
				try {
					consumerCommentReasons = Collections.unmodifiableMap(dao
							.getConsumerCommentReasons());
					dao.commit();
				} catch (DataSourceException e) {
					dao.rollback();
					throw e;
				} catch (Exception ex) {
					dao.rollback();
					throw new EMGRuntimeException(ex);
				} finally {
					dao.close();
				}
			}
		}
		return consumerCommentReasons;
	}

	public List getConsumerComments(int consumerId, String callerLoginId)
			throws DataSourceException {
		List comments = null;
		ConsumerProfileDAO daoProfile = new ConsumerProfileDAO();
		try {
			comments = daoProfile.selectComments(consumerId, callerLoginId);
			daoProfile.commit();
			Collections.sort(comments);
		} catch (DataSourceException e) {
			daoProfile.rollback();
			throw e;
		} catch (Exception ex) {
			daoProfile.rollback();
			throw new EMGRuntimeException(ex);
		} finally {
			daoProfile.close();
		}
		return (comments == null ? new ArrayList(0) : comments);
	}

	public List getConsumerActivityLogs(int consumerId, String callerLoginId, Integer activityLogCode, Date beginDate, Date endDate)
			throws DataSourceException {
		List activities = null;
		ConsumerProfileDAO daoProfile = new ConsumerProfileDAO();
		try {
			activities = daoProfile.getCustomerActivities(consumerId, callerLoginId, null, null, null);
			daoProfile.commit();
			Collections.sort(activities);
		} catch (DataSourceException e) {
			daoProfile.rollback();
			throw e;
		} catch (Exception ex) {
			daoProfile.rollback();
			throw new EMGRuntimeException(ex);
		} finally {
			daoProfile.close();
		}
		return (activities == null ? new ArrayList(0) : activities);
	}

	public int addConsumerProfileComment(ConsumerProfileComment comment, String callerLoginId)
			throws DataSourceException {
		int newId;
		ConsumerProfileDAO dao = new ConsumerProfileDAO();
		try {
			newId = dao.insertComment(comment, callerLoginId);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} catch (Exception ex) {
			dao.rollback();
			throw new EMGRuntimeException(ex);
		} finally {
			dao.close();
		}
		return newId;
	}

	public Collection searchConsumerProfile(ConsumerProfileSearchCriteria criteria,
			boolean showTainted, String userId, HashMap partnerSites) throws DataSourceException, TooManyResultException {

		Collection col = null;
		ConsumerProfileDAO dao = new ConsumerProfileDAO();

		try {
			col = dao.searchConsumerProfile(criteria, userId, partnerSites);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} catch (TooManyResultException e1) {
			dao.rollback();
			throw new TooManyResultException(e1.getMessage());
		} catch (Exception ex) {
			dao.rollback();
			throw new EMGRuntimeException(ex);
		} finally {
			dao.close();
		}
		return col;
	}

	public void updateLastLoginDate(int custId, String callerLoginId) throws DataSourceException {
		ConsumerProfileDAO dao = new ConsumerProfileDAO();
		try {
			dao.updateLastLoginDate(custId, callerLoginId);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} catch (Exception ex) {
			dao.rollback();
			throw new EMGRuntimeException(ex);
		} finally {
			dao.close();
		}
	}

	public void addLogonFailure(LogonFailure logonFailure) throws DataSourceException {
		ConsumerProfileDAO dao = new ConsumerProfileDAO();

		try {
			dao.addLogonFailure(logonFailure);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} catch (Exception ex) {
			dao.rollback();
			throw new EMGRuntimeException(ex);
		} finally {
			dao.close();
		}
	}

	/**
	 * This method is same as the UpdateStatus except while searching for SSN
	 * hash, It excludes the current user record from the resultset. So, when
	 * changing the status for a user in a NAT/PWD status, If you don't exclude
	 * the current user record, the system complains about duplicate SSN
	 */
	/*
	 * public ConsumerProfile updateUserStatus(int custId, ConsumerStatus
	 * newStatus, String callerLoginId) throws DataSourceException,
	 * SameStatusException { ConsumerProfile oldProfile = getConsumerProfile(new
	 * Integer(custId), callerLoginId, (String) null); ConsumerStatus oldStatus
	 * = oldProfile.getStatus();
	 *
	 * // if status did not change, there is nothing to do here if
	 * (oldStatus.getCombinedCode() == newStatus.getCombinedCode()) { throw new
	 * SameStatusException(
	 * "Old Status same as New Status on request to updateStatus."); } // TODO:
	 * MGO - need to figure out if we need to prevent dup profiles yet // and if
	 * so, how, /* /* else {
	 *
	 * // if activating an inactive profile, ensure an active profile does //
	 * not already exist for the ssn. if (oldStatus.isActiveStatus() == false &&
	 * newStatus.isActiveStatus()) { ConsumerProfileDAO dao = new
	 * ConsumerProfileDAO(); try { String ssnHash = dao.getSsnHash(custId,
	 * callerLoginId); ssnExists = dao.isExistingSsn(ssnHash, custId);
	 * dao.commit(); } catch (DataSourceException e) { dao.rollback(); throw e;
	 * } catch (Throwable t) { dao.rollback(); throw new EMGRuntimeException(t);
	 * } finally { dao.close(); } }
	 */

	// if (ssnExists == false) {
	/*
	 * if (1 == 1) { if
	 * (ConsumerStatus.ACTIVE_VALIDATION_LEVEL_2.equals(newStatus) ||
	 * ConsumerStatus.ACTIVE_VALIDATION_LEVEL_3.equals(newStatus) ||
	 * ConsumerStatus.ACTIVE_VALIDATION_LEVEL_4.equals(newStatus) ||
	 * ConsumerStatus.ACTIVE_VALIDATION_LEVEL_5.equals(newStatus)) {
	 * oldProfile.setAuthStatusFlag(true); } else if
	 * (ConsumerStatus.ACTIVE_VALIDATION_LEVEL_1.equals((newStatus))) {
	 * oldProfile.setAuthStatusFlag(false); } oldProfile.setStatus(newStatus);
	 * return (updateConsumerProfile(oldProfile)); } else { throw new
	 * DuplicateSsnException("SSN is already associated with a profile"); } }
	 */

	public Collection getConsumerSubStatuses(boolean onlyActive) throws DataSourceException {

		Collection col = null;
		ConsumerProfileDAO dao = new ConsumerProfileDAO();

		try {
			col = dao.getConsumerSubStatuses(onlyActive);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} catch (Exception e) {
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally {
			dao.close();
		}

		return col;
	}

	public boolean isProfileBlocked(ConsumerProfile consumerProfile, String ipAddr) {
		// Check for blocked IP, SSN, Phone and Accounts.
		BlockedIP blockedIP = new BlockedIP();
		blockedIP.setIpAddress1(ipAddr);
		blockedIP.setStatusCode(TaintIndicatorStatus.BLOCKED_STATUS.toString());
		FraudService fraudService = emgshared.services.ServiceFactory.getInstance()
				.getFraudService();
		try {
			if (consumerProfile != null
					&& fraudService.getBlockedIPs(consumerProfile.getUserId(), blockedIP).size() == 0
					&& !consumerProfile.isSsnBlocked() && !consumerProfile.isPhoneBlocked()
					&& !consumerProfile.isPhoneAlternateBlocked()
					&& !consumerProfile.isEmailBlocked() && !consumerProfile.isEmailDomainBlocked()
					&& !consumerProfile.isAccountBlocked()
					&& consumerProfile.isPersonToPersonAllowed()) {
				return false;
			}
		} catch (Exception e) {
			EMGSharedLogger
					.getLogger(this.getClass().getName().toString())
					.error(
							"ConsumerProfileServiceImpl.isProfileNotBlocked: Error checking fraudService.getBlockedIPs.  Error:"
									+ e);
			return true;
		}
		return true;
	}

	public void updateUserPromo(int custId, boolean acceptPromoEmails, String callerLoginId, String sourceSite)
			throws DataSourceException {
		ConsumerProfile oldProfile = getConsumerProfile(Integer.valueOf(custId), callerLoginId,
				(String) null);
		oldProfile.setAcceptPromotionalEmail(acceptPromoEmails);
		EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(
				"Passing " + sourceSite + "to updateConsumerProfile - value of flag is " + acceptPromoEmails);
		try {
			updateConsumerProfile(oldProfile,true,callerLoginId, sourceSite);
		} catch (DuplicateActiveProfileException e) {
			// just catch it and do nothing, should never happen on a promo only change.
		}
	}

	public void updateCustBlkdCode(int custId, String blkdCode, String callerLoginId)
			throws DataSourceException {
		ConsumerProfileDAO daoProfile = new ConsumerProfileDAO();
		try {
			daoProfile.updateCustBlkdCode(custId, blkdCode, callerLoginId);
			daoProfile.commit();
		} catch (DataSourceException e) {
			daoProfile.rollback();
			throw e;
		} catch (Exception e) {
			daoProfile.rollback();
			throw new EMGRuntimeException(e);
		} finally {
			daoProfile.close();
		}
		return;
	}

	public boolean isCustomerExist(int custId, String callerLoginId) throws DataSourceException {
		boolean isExist = false;
		ConsumerProfileDAO daoProfile = new ConsumerProfileDAO();
		try {
			isExist = daoProfile.isCustomerExist(custId, callerLoginId);
			daoProfile.commit();
		} catch (DataSourceException e) {
			daoProfile.rollback();
			throw e;
		} catch (Exception e) {
			daoProfile.rollback();
			throw new EMGRuntimeException(e);
		} finally {
			daoProfile.close();
		}
		return isExist;
	}

	public int getCustIdByEdirGuid(String eDirGuid, String callerLoginId) throws DataSourceException {
		int customerProfileId = 0;
		ConsumerProfileDAO daoProfile = new ConsumerProfileDAO();
		try {
			customerProfileId = daoProfile.getCustIdByEdirGuid(eDirGuid, callerLoginId);
			daoProfile.commit();
		} catch (DataSourceException e) {
			daoProfile.rollback();
			throw e;
		} catch (Exception e) {
			daoProfile.rollback();
			throw new EMGRuntimeException(e);
		} finally {
			daoProfile.close();
		}
		return customerProfileId;
	}

	public List getCustPremierTypes(String loginId) throws DataSourceException {

		List list = null;
		ConsumerProfileDAO dao = new ConsumerProfileDAO();

		try {
			list = dao.getCustPremierTypes(loginId);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} catch (Exception e) {
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally {
			dao.close();
		}

		return list;
	}

	public List getPurposeTypes(String loginId) throws DataSourceException {

		List list = null;
		ConsumerProfileDAO dao = new ConsumerProfileDAO();

		try {
			list = dao.getPurposeTypes(loginId);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} catch (Exception e) {
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally {
			dao.close();
		}

		return list;
	}

	public ConsumerProfile getConsumerProfile(String authenticationGuid) {
		// TODO tom c. getConsumerProfile(guid)
		return null;

	}

	public Set getConsumerAddressHistory(int consumerId, String callerLoginId , boolean activeFlag)
			throws DataSourceException {
		Set<ConsumerAddress> addresses = null;
		ConsumerProfileDAO daoProfile = new ConsumerProfileDAO();
		try {
			addresses = daoProfile.getConsumerAddressHistory(consumerId,activeFlag);
			daoProfile.commit();
		} catch (DataSourceException e) {
			daoProfile.rollback();
			throw e;
		} catch (Exception e) {
			daoProfile.rollback();
			throw new EMGRuntimeException(e);
		} finally {
			daoProfile.close();
		}
		return addresses;
	}

	public int insertConsumerActivityLog(ConsumerProfileActivity activity, String callerLoginId) throws DataSourceException {
		int newId;
		ConsumerProfileDAO dao = new ConsumerProfileDAO();
		try {
			newId = dao.insertConsumerActivityLog(activity, callerLoginId);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} catch (Exception e) {
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally {
			dao.close();
		}
		return newId;
	}

	public void updateCustDocumentStatus(ConsumerProfile consumerProfile, String newDocStatus) throws DataSourceException {
		ConsumerProfileDAO daoProfile = new ConsumerProfileDAO();
		try {
			daoProfile.updateCustDocumentStatus(consumerProfile, newDocStatus);
			daoProfile.commit();
		} catch (DataSourceException e) {
			daoProfile.rollback();
			throw e;
		} catch (Exception t) {
			daoProfile.rollback();
			throw new EMGRuntimeException(t);
		} finally {
			daoProfile.close();
		}
		return;
	}

	public List<CustomerAuthenticationInfo> getCustomerAuthInfo(int custId) throws DataSourceException {
		List<CustomerAuthenticationInfo> customerAuthenticationInfoList=null;
		ConsumerProfileDAO daoProfile = new ConsumerProfileDAO();
		try {
			customerAuthenticationInfoList = daoProfile.getCustomerAuthenticationInformation(custId);
			daoProfile.commit();
		} catch (DataSourceException e) {
			daoProfile.rollback();
			throw e;
		} catch (Exception e) {
			daoProfile.rollback();
			throw new EMGRuntimeException(e);
		} finally {
			daoProfile.close();
		}
		return customerAuthenticationInfoList;
	}
	// MBO -3321 getting Response String DB for Showing in popped up window
	public String getBPSResponse(Long searchId,String userId) throws DataSourceException{
		String BPSResponse = "";
		ConsumerProfileDAO daoProfile = new ConsumerProfileDAO();
		
		try {
			BPSResponse = daoProfile.getBPSResponse(searchId,userId);
			daoProfile.commit();
			if (!StringUtils.isEmpty(BPSResponse)) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.info("the response fetched from DB" + BPSResponse);
			} else {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.info("BPS response is NULL from DB for search id : "
								+ searchId);
			}
		} catch (DataSourceException e) {
			daoProfile.rollback();
			throw e;
		} catch (Exception e) {
			daoProfile.rollback();
			throw new EMGRuntimeException(e);
		} finally {
			daoProfile.close();
		}
		return BPSResponse;
	}
	// Ends Here - MBO -3321 getting Response String DB for Showing in popped up window
	
	@SuppressWarnings("rawtypes")
	public List<LexisNexisActivity> getLexisNexisActivityLog(
			SearchCriteriaDTO searchDTO) throws DataSourceException {
		ConsumerProfileDAO daoProfile = new ConsumerProfileDAO();
		List<LexisNexisActivity> lexNexActList = new ArrayList<LexisNexisActivity>();
		List<LexisNexisActivity> activityList = daoProfile
				.getLexisNexisActivityLogs(searchDTO);

		if (null != activityList && !activityList.isEmpty()) {
			Iterator iterActivity = activityList.iterator();
			while (iterActivity.hasNext()) {
				LexisNexisActivity activity = new LexisNexisActivity();
				activity = (LexisNexisActivity) iterActivity.next();
				if (null != activity.getSsnIndicator()) {
					activity.setSsnDisp(activity.getSsnIndicator());
				}
				if (null != activity.getSsnValidityIndCode()) {
					activity.setSsnValidDisp(activity.getSsnValidityIndCode());
				}
				activity.setActivityDateDisp(geDateWithTimeZone(activity
						.getActivityDate()));
				lexNexActList.add(activity);
			}
		}
		return lexNexActList;
	}
	
	public String geDateWithTimeZone(Date input) throws DataSourceException {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(input);
		TimeZone timeZone = TimeZone.getTimeZone("CST");
		calendar.setTimeZone(timeZone);
		SimpleDateFormat format = new SimpleDateFormat("dd/MMM/yyyy h:mm a z");
		String DateToStr = format.format(calendar.getTime());
		return DateToStr;
	}
	
	public int getNumberOfDaysSinceAddressChanged(int custId) throws DataSourceException{
		int nmbrOfDaysCurrentAddressLastChanged;
		ConsumerProfileDAO daoProfile = new ConsumerProfileDAO();
		try {
			nmbrOfDaysCurrentAddressLastChanged = daoProfile.getNumberOfDaysSinceCurrentAddressChanged(custId);	
			daoProfile.commit();
		} catch (DataSourceException e) {
			daoProfile.rollback();
			throw e;
		} catch (Exception e) {
			daoProfile.rollback();
			throw new EMGRuntimeException(e);
		} finally {
			daoProfile.close();
		}
		return nmbrOfDaysCurrentAddressLastChanged;
	}

	/**
	 * MBO - 5468
	 * 
	 * @param consumerId
	 * @param callerLoginId
	 *            returns the cursor from proc containing the list of documents
	 *            uploaded for this particular customer.
	 */
	public List<ConsumerDocument> getConsumerDocuments(int consumerId,
			String callerLoginId) throws DataSourceException {
		List<ConsumerDocument> consumerDocumentList;
		ConsumerProfileDAO daoProfile = new ConsumerProfileDAO();
		try {
			consumerDocumentList = daoProfile.getConsumerDocumentList(
					consumerId, callerLoginId);
			daoProfile.commit();
		} catch (DataSourceException e) {
			daoProfile.rollback();
			throw e;
		} catch (Exception t) {
			daoProfile.rollback();
			throw new EMGRuntimeException(t);
		} finally {
			daoProfile.close();
		}
		return consumerDocumentList;

	}

	/**
	 * MBO - 5468
	 * 
	 * @param imageId
	 *            returns the image that has been uploaded corresponding to the
	 *            image id that has been passed.
	 */
	public ConsumerDocument getDoc(int imageId) throws DataSourceException {
		ConsumerDocument consumerDocument;
		ConsumerProfileDAO daoProfile = new ConsumerProfileDAO();
		try {
			consumerDocument = daoProfile.getDoc(imageId);
			daoProfile.commit();
		} catch (DataSourceException e) {
			daoProfile.rollback();
			throw e;
		} catch (Exception t) {
			daoProfile.rollback();
			throw new EMGRuntimeException(t);
		} finally {
			daoProfile.close();
		}
		return consumerDocument;

	}

	/**
	 * MBO - 6161
	 * 
	 * @param loginId
	 *            returns the list of resident statuses available from DB.
	 */
	public List getResidencyStatusTypes(String loginId)
			throws DataSourceException {
		List list = null;
		ConsumerProfileDAO dao = new ConsumerProfileDAO();

		try {
			list = dao.getResdentStatTypes(loginId);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} catch (Exception e) {
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally {
			dao.close();
		}

		return list;
	}
	
	/**
	 * CAN-56
	 * 
	 * @param CountryCode
	 *             returns the list of States/Provinces from DB.
	 */
	public Map getStatesProvinces(String countryCode) throws DataSourceException {
		synchronized (statesProvinces) {
			ConsumerProfileDAO dao = new ConsumerProfileDAO();

			try {
				statesProvinces = Collections.unmodifiableMap(dao
						.getStatesProvinces(countryCode));
				dao.commit();
			} catch (DataSourceException e) {
				dao.rollback();
				throw e;
			} catch (Exception e) {
				dao.rollback();
				throw new EMGRuntimeException(e);
			} finally {
				dao.close();
			}

			return statesProvinces;
		}
	}
	

}
