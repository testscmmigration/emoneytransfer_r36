package emgshared.services;

import java.text.ParseException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import emgshared.exceptions.DataSourceException;
import emgshared.model.CountryExceptionBean;
import emgshared.model.CountryMasterBean;

public interface CountryExceptionService
{
	List getCntryExceptions(String country)
		throws DataSourceException;

	public boolean isCountryExceptionOverride(String country)
		throws DataSourceException;

	Collection getAllowedCountries()
		throws DataSourceException;

	Collection getAllowedSameDayCaliforniaCountries()
		throws DataSourceException;

	Collection getAllowedEconomyCaliforniaCountries()
		throws DataSourceException;

	CountryExceptionBean getCntryException(String receiveCountry, String sendCountry)
		throws DataSourceException;

	CountryExceptionBean getCountryException(String receiveCountry)
		throws DataSourceException;

	CountryMasterBean getCountryMaster(String isoCountryCode)
		throws DataSourceException;

	HashMap getCountryMasterList()
			throws DataSourceException;

	HashMap getCountryMasterListByStatus(String Status)
			throws DataSourceException;

	void setCntryException(CountryExceptionBean excp)
		throws DataSourceException, ParseException;

	void deleteCntryException(String receiveCountry, String sendCountry)
		throws DataSourceException;
	
	List getSendCountryCodes() throws DataSourceException;
}
