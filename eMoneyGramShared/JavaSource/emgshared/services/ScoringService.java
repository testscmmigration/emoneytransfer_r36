package emgshared.services;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.ScoreConfiguration;
import emgshared.model.Transaction;
import emgshared.model.TransactionScore;
import emgshared.model.TransactionScoreCategory;

public interface ScoringService
{

	Collection getScoreConfigurationsList(
			String userID,
			String scoringConfigId,
			String tranType,
			String partnerSiteId) // TODO uncomment when S25 DB Changes are completed
			throws DataSourceException, TooManyResultException;

	Collection getScoreConfigurations(
		String userID,
		String scoringConfigId,
		String tranType)
		throws DataSourceException, TooManyResultException;

	Collection getActiveScoreConfigurations(
		String userID,
		String scoringConfigId,
		String tranType)
		throws DataSourceException, TooManyResultException;

	Map getScoreCategories(
		String userID,
		int configurationId,
		String categoryCode)
		throws DataSourceException, TooManyResultException;

	Collection getScoreValues(
		String userID,
		int configurationId,
		String categoryCode,
		String listTypeCode)
		throws DataSourceException, TooManyResultException;

	Collection getScoreCategories(String userID, String categoryCode);

	int getBadTranCount(String userId, int custId);
	
	//modified as part of MBO-728
	List<Transaction> getSuccessfulTranCount(String userId, int custId);
	public int getDeniedTranCount(String userId, int custId);
	//ended
	boolean isPasswordChangedWithin24Hours(String userId, int custId);

	int getDaysLastTransaction(String userId, int custId, int tranId);

	void setScoreConfiguration(
		String userID,
		ScoreConfiguration scoreConfiguration)
		throws DataSourceException;

	ScoreConfiguration getScoreConfigView(String userId, int configId);

	void setEmgTranScore(String userID, TransactionScore tranScore)
		throws DataSourceException;

	void setEmgTranScoreCategory(
		String userID,
		TransactionScoreCategory tranScoreCategory)
		throws DataSourceException;

	void UpdateScoreConfigStatus(
		String userID,
		int scoreConfigId,
		String tranType,
		String scoreConfigStatus,
		String parentSiteId)
		throws DataSourceException;

	Collection getAllScoreConfigurations(String userID, String tranType, String partnerSiteId)
		throws DataSourceException, TooManyResultException;

	boolean isBlockedEmailMatch(String userId, int tranId);
	
	boolean isSeniorCitizenMatch(String userId, int tranId);

	boolean isReceiveStateMatch(String userId, int tranId);

	boolean isMaxSendLimitExceeded(String userId, int tranId);
	
	boolean isZipStateMatch(String userId, String countryCode, String postalCode, String state);

	boolean isAreaStateMatch(String userId,  String countryCode, String areaCode, String state);

	boolean isEmailKeywordMatch(String userId, Collection emails);
	
	boolean isNegativeAddressMatch(String userId, String negativeString);
	
	String getStateForAreaCode(String userId, String countryCode, String areaCode) throws Exception;
	
	List getChargeBackScore(int tranId)throws DataSourceException;
}
