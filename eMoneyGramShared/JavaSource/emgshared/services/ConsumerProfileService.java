/*
 * Created on Jan 6, 2005
 *
 */
package emgshared.services;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.DuplicateActiveProfileException;
import emgshared.exceptions.SameStatusException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.ConsumerDocument;
import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileActivity;
import emgshared.model.ConsumerProfileComment;
import emgshared.model.ConsumerProfileSearchCriteria;
import emgshared.model.ConsumerStatus;
import emgshared.model.CustomerAuthenticationInfo;
import emgshared.model.LexisNexisActivity;
import emgshared.model.LogonFailure;
import emgshared.model.NewConsumerProfile;

import emgshared.model.SearchCriteriaDTO;

import emgshared.model.ConsumerAddress;

/**
 * @author A131
 *
 */
public interface ConsumerProfileService {
	public int addConsumerProfile(NewConsumerProfile consumerProfile) throws DataSourceException;

	public ConsumerProfile updateConsumerProfile(ConsumerProfile consumerProfile, boolean bypassDupCheck, String updatedByUserId)
			throws DataSourceException, DuplicateActiveProfileException;

	public ConsumerProfile updateConsumerProfile(ConsumerProfile consumerProfile, boolean bypassDupCheck, String updatedByUserId, String sourceSite)
	        throws DataSourceException,DuplicateActiveProfileException;
	/*
	 * public ConsumerProfile getConsumerProfile(String userID, int
	 * edirAuthStatus, String serverIP, String serverName, String clientIP)
	 * throws DataSourceException, LoginAccountException;
	 */
	public ConsumerProfile getConsumerProfile(Integer consumerId, String callerLoginId,
			String consumerLogonId) throws DataSourceException;
	
	public List<ConsumerAddress> getConsumerAddressList(ConsumerProfile consumerProfile)  throws DataSourceException;
	
	public ConsumerProfile getConsumerProfile(Integer consumerId, String callerLoginId,
			String consumerLogonId,boolean isFilterActive) throws DataSourceException;

	public Set getConsumerAddressHistory(int consumerId, String callerLoginId , boolean activeFlag) throws DataSourceException;

	public void addLogonFailure(LogonFailure logonFailure) throws DataSourceException;

	public boolean isExistingUserId(String userId) throws DataSourceException;

	public Map getVerificationQuestions() throws DataSourceException;

	public String getVerificationQuestionText(int id) throws DataSourceException;

	public String getVerificationQuestionText(Integer id) throws DataSourceException;

	public String[] getVerificationQuestionTextByCust(String userId, int custId, int verifyQuestId) throws DataSourceException;

	public int updatePassword(String userId, int custId, String password, String existingPassword) throws DataSourceException;

	public int updatePassword(String userId, int custId, String password, String existingPassword, Date expirationDate)
			throws DataSourceException;

	public ConsumerProfile updateStatus(int custId, ConsumerStatus newStatus, String callerLoginId, boolean bypassDupCheck)
			throws DataSourceException, SameStatusException, DuplicateActiveProfileException;

	public void updateEmailStatus(String userId, ConsumerEmail consumerEmail) throws DataSourceException;

	public Map getConsumerStatusDescriptions() throws DataSourceException;

	public Map getConsumerCommentReasons() throws DataSourceException;

	public List getConsumerComments(int consumerId, String callerLoginId) throws DataSourceException;

	public int addConsumerProfileComment(ConsumerProfileComment comment, String callerLoginId) throws DataSourceException;

	public Collection searchConsumerProfile(ConsumerProfileSearchCriteria criteria, boolean showTainted, String userId, HashMap partnerSites)
			throws DataSourceException, TooManyResultException;

	public void updateLastLoginDate(int custId, String callerLoginId) throws DataSourceException;

	public Collection getConsumerSubStatuses(boolean onlyActive) throws DataSourceException;

	public boolean isProfileBlocked(ConsumerProfile consumerProfile, String ipAddr);

	public void updateSsn(int consumerId, String ssnMask, String callerLoginId, boolean byPassDuplicateCheck) throws DataSourceException,
			DuplicateActiveProfileException;

	public void updateUserPromo(int custId, boolean acceptPromoEmails, String callerLoginId, String sourceSite) throws DataSourceException;

	public void updateCustBlkdCode(int custId, String blkdCode, String callerLoginId) throws DataSourceException;

	public void updateConsumerRewardsInfo(String userId, int consumerId, String rewardsNumber, String consumerAutoEnrollFlag)
			throws DataSourceException;

	public boolean isCustomerExist(int custId, String callerLoginId) throws DataSourceException;

	public int getCustIdByEdirGuid(String eDirGuid, String callerLoginId) throws DataSourceException;

	List getCustPremierTypes(String loginId) throws DataSourceException;

	List getPurposeTypes(String loginId) throws DataSourceException;

	public ConsumerProfile getConsumerProfile(String authenticationGuid);

	public List getConsumerActivityLogs(int consumerId, String callerLoginId, Integer activityLogCode, Date beginDate, Date endDate)
			throws DataSourceException;

	public int insertConsumerActivityLog(ConsumerProfileActivity activity, String callerLoginId) throws DataSourceException;
	
	public void updateCustDocumentStatus(ConsumerProfile consumerProfile, String newDocStatus) throws DataSourceException;
	
	public List<CustomerAuthenticationInfo> getCustomerAuthInfo(int custId) throws DataSourceException;
	
	public List<LexisNexisActivity> getLexisNexisActivityLog(SearchCriteriaDTO searchDTO) throws DataSourceException;
// MBO - 3321
	public String getBPSResponse(Long searchId , String userId) throws DataSourceException;
	// MBO - 3321
	
	//MBO - 3150
	public int getNumberOfDaysSinceAddressChanged(int custId) throws DataSourceException;
	//MBO-3150
	
	//MBO-5468
	public List<ConsumerDocument> getConsumerDocuments(int consumerId, String callerLoginId) throws DataSourceException;
	
	public ConsumerDocument getDoc(int imageId) throws DataSourceException;
	//MBO-5468 End
	
	//MBO-6161
	List getResidencyStatusTypes(String loginId) throws DataSourceException;
	
	//CAN-56
	public Map getStatesProvinces(String countryCode) throws DataSourceException;
	
}