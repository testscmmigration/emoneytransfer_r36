/*
 * Copyright (c) 2004 MoneyGram Payment Systems, Inc. All rights reserved.
 * 
 * Created on Feb 14, 2005
 */
package emgshared.services;

import java.util.List;

import emgshared.model.TranSubReason;

/**
 * 
 */
public interface TransSubReasonService
{
    TranSubReason getTranSubReason(String reasonTypeCode, String subReasonCode) throws Exception;
    List getTranSubReasonsForTypeCode(String reasonTypeCode) throws Exception;
    List getTranSubReasonsAll() throws Exception;
}
