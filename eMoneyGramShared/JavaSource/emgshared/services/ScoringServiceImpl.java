package emgshared.services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import emgshared.dataaccessors.CacheDAO;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.dataaccessors.ScoringDAO;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.ChargeBackScoreData;
import emgshared.model.ConsumerEmail;
import emgshared.model.ScoreCategory;
import emgshared.model.ScoreConfiguration;
import emgshared.model.ScoreValue;
import emgshared.model.Transaction;
import emgshared.model.TransactionScore;
import emgshared.model.TransactionScoreCategory;
import emgshared.util.StringHelper;

class ScoringServiceImpl implements ScoringService
{
	private static Logger logger = EMGSharedLogger.getLogger(ScoringServiceImpl.class);
	private static final ScoringServiceImpl instance = new ScoringServiceImpl();

	private ScoringServiceImpl()
	{
	}

	public static final ScoringServiceImpl getInstance()
	{
		return instance;
	}

	public Collection getScoreConfigurationsList(String userID, String scoreConfigId,String tranType, String partnerSiteId)
			throws DataSourceException, TooManyResultException
	{
		Collection list;
		ScoringDAO dao = new ScoringDAO();
		try {
			list = dao.getScoreConfigurationsList(userID, scoreConfigId, tranType, partnerSiteId);
		} catch (DataSourceException e) {
			throw e;
		} catch (TooManyResultException e1) {
			throw e1;
		} catch (SQLException sqlex) {
			throw new EMGRuntimeException(sqlex);
		} catch (Exception ex) {
			throw new EMGRuntimeException(ex);
		} finally {
			dao.close();
		}
		return list;		
	}
			
	public Collection getScoreConfigurations(
		String userID,
		String scoreConfigId,
		String tranType)
		throws DataSourceException, TooManyResultException
	{
		Collection list;
		ScoringDAO dao = new ScoringDAO();
		try
		{
			list = dao.getScoreConfigurations(userID, scoreConfigId, tranType);
		} catch (DataSourceException e)
		{
			logger.error("DataSourceException in ScoreService.getScoreConfiguartions " + e.getMessage(), e);
			throw e;
		} catch (TooManyResultException e1)
		{
			logger.error("TooManyResultException in ScoreService.getScoreConfiguartions " + e1.getMessage(), e1);
			throw e1;
		} catch (Exception e)
		{
			logger.error("Throwable in ScoreService.getScoreConfiguartions " + e.getMessage(), e);
			throw new EMGRuntimeException(e);
		} finally
		{
			dao.close();
		}
		return list;
	}

	public Collection getActiveScoreConfigurations(String userID, String scoreConfigId, String tranType)
		throws DataSourceException, TooManyResultException {
		Collection list;
		ScoringDAO dao = new ScoringDAO();
		try {
			list = dao.getActiveScoreConfigurations(userID, scoreConfigId, tranType);
		} catch (DataSourceException e) {
			throw e;
		} catch (TooManyResultException e1) {
			throw e1;
		} catch (Exception ex) {
			throw new EMGRuntimeException(ex);
		} finally {
			dao.close();
		}
		return list;
	}

	public Map getScoreCategories(
		String userID,
		int configurationId,
		String categoryCode)
		throws DataSourceException, TooManyResultException
	{
		Map map;
		ScoringDAO dao = new ScoringDAO();
		try
		{
			map = dao.getScoreCategories(userID, configurationId, categoryCode);
		} catch (DataSourceException e)
		{
			throw e;
		} catch (TooManyResultException e1)
		{
			throw e1;
		} catch (Exception ex)
		{
			throw new EMGRuntimeException(ex);
		} finally
		{
			dao.close();
		}
		return map;
	}

	public Collection getScoreValues(
		String userID,
		int configurationId,
		String categoryCode,
		String listTypeCode)
		throws DataSourceException, TooManyResultException
	{
		Collection list;
		ScoringDAO dao = new ScoringDAO();
		try
		{
			list =
				dao.getScoreValues(
					userID,
					configurationId,
					categoryCode,
					listTypeCode);
		} catch (DataSourceException e)
		{
			throw e;
		} catch (TooManyResultException e1)
		{
			throw e1;
		} catch (Exception ex)
		{
			throw new EMGRuntimeException(ex);
		} finally
		{
			dao.close();
		}
		return list;
	}

	public Collection getScoreCategories(String userID, String categoryCode)
	{
		Collection list;
		ScoringDAO dao = new ScoringDAO();
		try
		{
			list = dao.getScoreCategories(userID, categoryCode);
		} catch (DataSourceException dse)
		{
			throw new EMGRuntimeException(dse);
		} catch (Exception ex)
		{
			throw new EMGRuntimeException(ex);
		}finally
		{
			dao.close();
		}
		return list;
	}

	public int getBadTranCount(String userId, int custId)
	{
		ScoringDAO dao = new ScoringDAO();
		int cnt = 0;
		try
		{
			cnt = dao.getBadTranCount(userId, custId);
		} catch (DataSourceException dse)
		{
			throw new EMGRuntimeException(dse);
		} finally
		{
			dao.close();
		}
		return cnt;
	}

	//modified as a part of MBO-728
	public List getSuccessfulTranCount(String userId, int custId)
	{
		ScoringDAO dao = new ScoringDAO();
		List<Transaction> tranList = new ArrayList<Transaction>();
		try
		{
			tranList = dao.getSuccessTranCount(userId, custId);
		} catch (Exception e)
		{
			throw new EMGRuntimeException(e);
		} finally
		{
			dao.close();
		}
		return tranList;
	}
	//MBO-728 end
	public boolean isPasswordChangedWithin24Hours(String userId, int custId)
	{
		ScoringDAO dao = new ScoringDAO();
		boolean flag = false;
		try
		{
			Date date = dao.getLastPasswordChangeDate(userId, custId);
			if (date != null)
			{
				flag =
					System.currentTimeMillis() - date.getTime()
						< 24 * 60 * 60 * 1000;
			}
		} catch (DataSourceException dse)
		{
			throw new EMGRuntimeException(dse);
		} finally
		{
			dao.close();
		}
		return flag;
	}

	public int getDaysLastTransaction(String userId, int custId, int tranId)
	{
		ScoringDAO dao = new ScoringDAO();
		Date date = null;
		int cnt = 0; //  number of days
		try
		{
			date = dao.getLastTransactionDate(userId, custId, tranId);
			if (date == null)
			{
				cnt = -1;
			} else
			{
				cnt =
					(int) ((System.currentTimeMillis() - date.getTime())
						/ 24L
						/ 60L
						/ 60L
						/ 1000L);
			}
		} catch (DataSourceException dse)
		{
			throw new EMGRuntimeException(dse);
		} finally
		{
			dao.close();
		}
		return cnt;
	}

	public void setScoreConfiguration(
		String userID,
		ScoreConfiguration scoreConfiguration)
		throws DataSourceException
	{

		ScoringDAO dao = new ScoringDAO();
		CacheDAO cacheDao = new CacheDAO();

		try
		{
			//Insert or Update Score Configuration level.
			int scoreConfigId =
				dao.setScoreConfiguration(userID, scoreConfiguration);

			// Move all the possible categories into the category table
			dao.insertAllCategories(userID, scoreConfigId);

			//Update the Score Categories
			for (Iterator it =
				scoreConfiguration.getScoreCategories().values().iterator();
				it.hasNext();
				)
			{
				ScoreCategory scoreCategory = (ScoreCategory) it.next();
				dao.setScoreConfigurationCategory(
					userID,
					scoreConfigId,
					scoreCategory);

				//Delete all the Score Values
				dao.removeScoreValue(
					userID,
					scoreConfigId,
					scoreCategory.getScoreCategoryCode());

				//Update the Score Value
				for (Iterator it1 = scoreCategory.getScoreValues().iterator();
					it1.hasNext();
					)
				{
					ScoreValue scoreValue = (ScoreValue) it1.next();
					dao.setScoreValue(
						userID,
						scoreConfigId,
						scoreCategory.getScoreCategoryCode(),
						scoreValue);
				}
			}

			cacheDao.updateCache("TransactionScore");
			cacheDao.commit();
			dao.commit();

		} catch (DataSourceException e)
		{
			cacheDao.rollback();
			dao.rollback();
			throw e;
		} catch (Exception ex)
		{
			cacheDao.rollback();
			dao.rollback();
			throw new EMGRuntimeException(ex);
		} finally
		{
			cacheDao.close();
			dao.close();
		}

	}

	public ScoreConfiguration getScoreConfigView(String userId, int configId)
	{
		ScoringDAO dao = new ScoringDAO();
		ScoreConfiguration cnfg = null;
		try
		{
			cnfg = dao.getScoreConfigView(userId, configId);
		} catch (DataSourceException dse)
		{
			throw new EMGRuntimeException(dse);
		} finally
		{
			dao.close();
		}
		return cnfg;
	}

	public void setEmgTranScore(String userID, TransactionScore tranScore)
		throws DataSourceException
	{
		ScoringDAO dao = new ScoringDAO();

		try
		{
			//Insert or Update Score Configuration level.
			dao.setEmgTranScore(userID, tranScore);
			dao.commit();

		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} finally
		{
			dao.close();
		}
	}

	public void setEmgTranScoreCategory(
		String userID,
		TransactionScoreCategory tranScoreCategory)
		throws DataSourceException
	{
		ScoringDAO dao = new ScoringDAO();

		try
		{
			//Insert or Update Score Configuration level.
			dao.setEmgTranScoreCategory(userID, tranScoreCategory);
			dao.commit();

		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} finally
		{
			dao.close();
		}
	}

	public void UpdateScoreConfigStatus(
		String userID,
		int scoreConfigId,
		String tranType,
		String scoreConfigStatus,
		String parentSiteId)
		throws DataSourceException
	{
		ScoringDAO dao = new ScoringDAO();
		CacheDAO cacheDao = new CacheDAO();

		try
		{
			//Insert or Update Score Configuration level.
			dao.UpdateScoreConfigStatus(
				userID,
				scoreConfigId,
				tranType,
				scoreConfigStatus,
				parentSiteId);
			cacheDao.updateCache("TransactionScore");

			dao.commit();
			cacheDao.commit();

		} catch (DataSourceException e)
		{
			dao.rollback();
			cacheDao.rollback();
			throw e;
		} finally
		{
			dao.close();
			cacheDao.close();
		}
	}

	public Collection getAllScoreConfigurations(String userID, String tranType, String partnerSiteId)
		throws DataSourceException, TooManyResultException
	{
		Collection list;
		ScoringDAO dao = new ScoringDAO();
		try
		{
			list = dao.getAllScoreConfigurations(userID, tranType, partnerSiteId);
		} catch (DataSourceException e)
		{
			throw e;
		} catch (TooManyResultException e1)
		{
			throw e1;
		} catch (SQLException sqlEx)
		{
			throw new EMGRuntimeException(sqlEx);
		} finally
		{
			dao.close();
		}
		return list;
	}

	public boolean isBlockedEmailMatch(String userId, int tranId)
	{
		ScoringDAO dao = new ScoringDAO();
		int flag = 0;
		try
		{
			flag = dao.getBlockedEmailMatch(userId, tranId);
		} catch (DataSourceException dse)
		{
			throw new EMGRuntimeException(dse);
		} finally
		{
			dao.close();
		}
		if (flag == 0)
			return false;
		else
			return true;
	}

	public boolean isSeniorCitizenMatch(String userId, int tranId)
	{
		ScoringDAO dao = new ScoringDAO();
		int flag = 0;
		try
		{
			flag = dao.getSeniorCitizenMatch(userId, tranId);
		} catch (DataSourceException dse)
		{
			throw new EMGRuntimeException(dse);
		} finally
		{
			dao.close();
		}
		if (flag == 0)
			return false;
		else
			return true;
	}

	public boolean isReceiveStateMatch(String userId, int tranId)
	{
		ScoringDAO dao = new ScoringDAO();
		int flag = 0;
		try
		{
			flag = dao.getReceiveStateMatch(userId, tranId);
		} catch (DataSourceException dse)
		{
			throw new EMGRuntimeException(dse);
		} finally
		{
			dao.close();
		}
		if (flag == 0)
			return false;
		else
			return true;
	}

	public boolean isMaxSendLimitExceeded(String userId, int tranId)
	{
		ScoringDAO dao = new ScoringDAO();
		int flag = 0;
		try
		{
			flag = dao.getMaxSendLimitExceeded(userId, tranId);
		} catch (DataSourceException dse)
		{
			throw new EMGRuntimeException(dse);
		} finally
		{
			dao.close();
		}
		if (flag == 0)
			return false;
		else
			return true;
	}

	public boolean isZipStateMatch(String userId, String countryCode, String postalCode, String state)
	{
		ScoringDAO dao = new ScoringDAO();
		int flag = 0;

		try	{
			if (!StringHelper.isNullOrEmpty(postalCode) &&
					!StringHelper.isNullOrEmpty(state) &&
					!StringHelper.isNullOrEmpty(countryCode)) {
				flag = dao.getZipStateMatch(userId,  countryCode.toUpperCase(), postalCode, state.toUpperCase());
			}
		} catch (DataSourceException dse)
		{
			throw new EMGRuntimeException(dse);
		} finally
		{
			dao.close();
		}
		if (flag == 0)
			return false;
		else
			return true;
	}


	public boolean isAreaStateMatch(String userId,  String countryCode, String areaCode, String state)
	{
		ScoringDAO dao = new ScoringDAO();
		int flag = 0;

		try	{
			if (!StringHelper.isNullOrEmpty(areaCode) &&
					!StringHelper.isNullOrEmpty(state) &&
					!StringHelper.isNullOrEmpty(countryCode)) {
				flag = dao.getAreaStateMatch(userId,  countryCode.toUpperCase(), areaCode, state.toUpperCase());
			}
		} catch (DataSourceException dse)
		{
			throw new EMGRuntimeException(dse);
		} finally
		{
			dao.close();
		}
		if (flag == 0)
			return false;
		else
			return true;
	}

	public boolean isEmailKeywordMatch(String userId, Collection emails)
	{
		ScoringDAO dao = new ScoringDAO();
		int flag = 0;
		try
		{
			Iterator i = ((HashSet) emails).iterator();
			
			while(i.hasNext()) {
				ConsumerEmail email = (ConsumerEmail) i.next();
				String emailString = email.getConsumerEmail();
				if (!StringHelper.isNullOrEmpty(emailString))
					flag = dao.getEmailKeywordsMatch(userId, emailString);
				
				if (flag != 0) 
					break;
			}
		} catch (DataSourceException dse) {
			throw new EMGRuntimeException(dse);
		} finally {
			dao.close();
		}
		if (flag == 0)
			return false;
		else
			return true;
	}

	public boolean isNegativeAddressMatch(String userId, String negativeString)
	{
		ScoringDAO dao = new ScoringDAO();
		int flag = 0;

		try
		{
			if (!StringHelper.isNullOrEmpty(negativeString))
				flag = dao.getNegativeAddressMatch(userId, negativeString);
		} catch (DataSourceException dse)
		{
			throw new EMGRuntimeException(dse);
		} finally
		{
			dao.close();
		}
		if (flag == 0)
			return false;
		else
			return true;	
	}
	
	public String getStateForAreaCode(String userId, String countryCode, String areaCode) throws Exception
	{
		ScoringDAO dao = new ScoringDAO();
		String state = null;
		try	{
		    state = dao.getStateForAreaCode(userId,countryCode,areaCode);
		} catch (DataSourceException dse)
		{
			throw dse;
		} finally
		{
			dao.close();
		}
		return state;	
	}
	
	//added for MBO-728
	public int getDeniedTranCount(String userId, int custId)
	{
		ScoringDAO dao = new ScoringDAO();
		int cnt = 0;
		try
		{
			cnt = dao.getDeniedTranCount(userId, custId);
		} catch (DataSourceException dse)
		{
			throw new EMGRuntimeException(dse);
		} finally
		{
			dao.close();
		}
		return cnt;
	}
	//ended
//MBO-2321 changes to call the DAO
	
	public List<ChargeBackScoreData> getChargeBackScore(int tranId)
			throws DataSourceException {
		List<ChargeBackScoreData> cbValueList = new ArrayList();
		ScoringDAO dao = new ScoringDAO();
		try {
			cbValueList = dao.getChargeBackScoreData(tranId);
		} catch (DataSourceException e) {
			throw e;
		} finally {
			dao.close();
		}
		return cbValueList;

	}

}