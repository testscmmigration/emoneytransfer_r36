package emgshared.services;

import emgshared.model.ConsumerAddress;
import emgshared.model.ConsumerProfile;
import emgshared.model.CreditCard;

/**
 * @author A131
 *
 */
public interface CreditCardService
{
	/**
	 * Reserve funds in a consumer's account.
	 * @param consumerProfile
	 * @param creditCard
	 * @param billingAddress
	 * @param amount
	 * @param performAddressVerification
	 * @param emgTrackingValue
	 * @return
	 * 
	 */
	public CreditCardServiceResponse executeAuthorization(
		ConsumerProfile consumerProfile,
		CreditCard creditCard,
		ConsumerAddress billingAddress,
		double amount,
		boolean performAddressVerification,
		String emgTrackingValue,
		String merchantId
	);

	/**
	 * Transfer funds from the consumer's account.
	 * This call must be preceded by a call to executeAuthorization.
	 * @param obj	- the identifier of a preceding authorization
	 * @param amount
	 * @param emgTrackingValue
	 * @return
	 * 
	 */
	public CreditCardServiceResponse executeCapture(
		Object obj,
		double amount,
		String emgTrackingValue,
		String merchantId
	);



	/**
	 * Credit a consumer's account.  The credit will not be
	 * associated with a preceding capture.
	 * @param accountId
	 * @param amount
	 * @param emgTrackingValue
	 * @param callerUserId
	 * @return
	 * @see emgshared.services.CreditCardService#executeCredit(java.lang.Object, double, java.lang.String)
	 * 
	 */
	public CreditCardServiceResponse executeCredit(
		ConsumerProfile consumerProfile,
		CreditCard creditCard,
		ConsumerAddress billingAddress,
		double amount,
		String emgTrackingValue,
		String merchantId);

	/**
	 * Credit a consumer's account.  The credit is associated
	 * with a preceding capture.
	 * @param obj - the identifier of a previous capture
	 * @param amount
	 * @param emgTrackingValue
	 * @return
	 * @see emgshared.services.CreditCardService#executeCredit(emgshared.model.ConsumerProfile, emgshared.model.CreditCard, emgshared.model.ConsumerAddress, double, java.lang.String)
	 * 
	 */
	public CreditCardServiceResponse executeCredit(
		Object obj,
		double amount,
		String emgTrackingValue,
		String merchantId);

	/**
	 * Release funds reserved by a preceding authorization.
	 * @param obj	- The identifier of preceding authorization request.
	 * @param amount	- must be equal to the authorization amount.
	 * @param emgTrackingValue
	 * 
	 */
	public void reverseAuthorization(
		Object obj,
		double amount,
		String emgTrackingValue,
		String merchantId);
}
