package emgshared.services;

import java.util.List;

public class CyberDMData {
	private List<CyberCode> afsFactorCode;
	private List<CyberDMInfoCodes> infoCodes;
	private List<CyberCode> triggeredRules;
	public List<CyberCode> getAfsFactorCode() {
		return afsFactorCode;
	}
	public void setAfsFactorCode(List<CyberCode> afsFactorCode) {
		this.afsFactorCode = afsFactorCode;
	}
	public List<CyberDMInfoCodes> getInfoCodes() {
		return infoCodes;
	}
	public void setInfoCodes(List<CyberDMInfoCodes> infoCodes) {
		this.infoCodes = infoCodes;
	}
	public List<CyberCode> getTriggeredRules() {
		return triggeredRules;
	}
	public void setTriggeredRules(List<CyberCode> triggeredRules) {
		this.triggeredRules = triggeredRules;
	}
	
	
	
}
