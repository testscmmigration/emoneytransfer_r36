/*
 * Created on Mar 9, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgshared.services;

import java.math.BigDecimal;

import emgshared.exceptions.DataSourceException;

/**
 * @author T348
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface AccountingService {
	BigDecimal getLast30DayTotal(String userID, int custID) 
		throws DataSourceException;
}
