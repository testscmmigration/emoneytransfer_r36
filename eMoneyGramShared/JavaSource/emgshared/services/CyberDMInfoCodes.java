package emgshared.services;

import java.util.List;

public class CyberDMInfoCodes {
	private String infoCodeName;
	private List<CyberCode> infoCodes;
	
	public String getInfoCodeName() {
		return infoCodeName;
	}
	public void setInfoCodeName(String infoCodeName) {
		this.infoCodeName = infoCodeName;
	}
	public List<CyberCode> getInfoCodes() {
		return infoCodes;
	}
	public void setInfoCodes(List<CyberCode> infoCodes) {
		this.infoCodes = infoCodes;
	}
	
	
}
