package emgshared.services;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.lang.Validate;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import emgshared.dataaccessors.ConsumerAccountDAO;
import emgshared.dataaccessors.ConsumerProfileDAO;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.dataaccessors.FraudDAO;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.exceptions.MaxRowsHashCryptoException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.AccountStatus;
import emgshared.model.BlockedBean;
import emgshared.model.BlockedIP;
import emgshared.model.ConsumerAccountType;
import emgshared.model.ConsumerProfileSearchView;
import emgshared.model.CustomerProfileAccount;
import emgshared.model.ElectronicTypeCode;
import emgshared.model.TaintIndicatorStatus;
import emgshared.model.TaintIndicatorType;
import emgshared.model.UpdateAccountServiceRequest;
import emgshared.model.UpdateAccountServiceResponse;
import emgshared.property.EMTSharedContainerProperties;
import emgshared.util.StringHelper;

class FraudServiceImpl implements FraudService
{
	private static final FraudServiceImpl instance = new FraudServiceImpl();
	private static final String NOT_BLOCKED_CODE = "NBK";
	private static final String EMAIL_INDICATOR = "A";
	//PWMB-1264 : Logger added
	private static final Logger LOGGER = EMGSharedLogger.getLogger("FraudServiceImpl");
	private FraudServiceImpl()
	{
	}

	public static final FraudServiceImpl getInstance()
	{
		return instance;
	}

	public Collection getBlockedIPs(
		String userID,
		String beginIP,
		String endIP)
		throws DataSourceException, TooManyResultException
	{
		Collection list;
		FraudDAO dao = new FraudDAO();
		try
		{
			list = dao.getBlockedIPs(userID, beginIP, endIP);
		} catch (DataSourceException e)
		{
			throw e;
		} catch (TooManyResultException e1)
		{
			throw e1;
		} catch (SQLException sqlex)
		{
			throw new EMGRuntimeException(sqlex);
		} finally
		{
			dao.close();
		}
		return list;
	}
	public Collection getBlockedIPs(String userID, BlockedIP blockedIP)
		throws DataSourceException, TooManyResultException
	{
		Collection list;
		FraudDAO dao = new FraudDAO();
		try
		{
			list = dao.getBlockedIPs(userID, blockedIP);
		} catch (DataSourceException e)
		{
			throw e;
		} catch (TooManyResultException e1)
		{
			throw e1;
		} catch (SQLException sqlex)
		{
			throw new EMGRuntimeException(sqlex);
		} finally
		{
			dao.close();
		}
		return list;
	}

	public Collection getBlockedReasons(String userID, String reasonType)
		throws DataSourceException
	{
		Collection list;
		FraudDAO dao = new FraudDAO();
		try
		{
			list = dao.getBlockedReasons(userID, reasonType);
		} catch (DataSourceException e)
		{
			throw e;
		} catch (TooManyResultException resultex)
		{
			throw new EMGRuntimeException(resultex);
		} catch (SQLException sqlex)
		{
			throw new EMGRuntimeException(sqlex);
		} finally
		{
			dao.close();
		}
		return list;
	}

	public void setBlockedIPs(String userID, BlockedIP blockedIP)
		throws DataSourceException, ParseException
	{

		FraudDAO dao = new FraudDAO();
		try {
			dao.setBlockedIPs(userID, blockedIP);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} catch (SQLException sqlex) {
			throw new EMGRuntimeException(sqlex);
		} catch (ParseException sqlex) {
			throw new EMGRuntimeException(sqlex);
		} finally {
			dao.close();
		}
		return;
	}

	public Collection getBlockedStatus(String userID, String blockedType)
		throws DataSourceException
	{
		Collection list;
		FraudDAO dao = new FraudDAO();
		try
		{
			list = dao.getBlockedStatus(userID, blockedType);
		} catch (DataSourceException e)
		{
			throw e;
		} catch (TooManyResultException resultex) {
			throw new EMGRuntimeException(resultex);
		} catch (SQLException sqlex) {
			throw new EMGRuntimeException(sqlex);
		} finally
		{
			dao.close();
		}
		return list;
	}


	public Collection getBlockedCCAccounts(
		String userID,
		BlockedBean blockedBean)
		throws DataSourceException, MaxRowsHashCryptoException
	{
		Collection list;
		FraudDAO dao = new FraudDAO();
		try
		{
			list = dao.getBlockedCCAccounts(userID, blockedBean);
		} catch (DataSourceException e)
		{
			throw e;
		} catch (MaxRowsHashCryptoException e1)
		{
			throw e1;
		} catch (SQLException sqlException)
		{
			throw new EMGRuntimeException(sqlException);
		} finally
		{
			dao.close();
		}
		return list;
	}

	public Collection getBlockedCCAccounts(String userID, String blockedID,
			String maskText)
			throws DataSourceException, MaxRowsHashCryptoException
		{
			Collection list;
			FraudDAO dao = new FraudDAO();
			try
			{
				list = dao.getBlockedCCAccounts(userID, blockedID, maskText);
			} catch (DataSourceException e)
			{
				throw e;
			} catch (MaxRowsHashCryptoException e1)
			{
				throw e1;
			} catch (SQLException sqlException)
			{
				throw new EMGRuntimeException(sqlException);
			} finally
			{
				dao.close();
			}
			return list;
		}

	public Collection getBlockedCCAccounts(String userID, String blockedID,
		String maskText, String cardToken)
		throws DataSourceException, MaxRowsHashCryptoException
	{
		Collection list;
		FraudDAO dao = new FraudDAO();
		try
		{
			list = dao.getBlockedCCAccounts(userID, blockedID, maskText, cardToken);
		} catch (DataSourceException e)
		{
			throw e;
		} catch (MaxRowsHashCryptoException e1)
		{
			throw e1;
		} catch (SQLException sqlException)
		{
			throw new EMGRuntimeException(sqlException);
		} finally
		{
			dao.close();
		}
		return list;
	}

	public void setBlockedCC(
		String userId,
		BlockedBean blockedCC,
		Collection customerProfileAccounts)
		throws DataSourceException
	{

		FraudDAO dao = new FraudDAO();
		ConsumerAccountDAO consumerDAO = new ConsumerAccountDAO();
		boolean accountBlocked = false;

		try
		{
			
			Iterator iter = customerProfileAccounts.iterator();
			//  Insert or Update the blocked BIN. 			
			String blockedId = dao.setBlockedCC(userId, blockedCC);

			String taintedInd = TaintIndicatorType.BLOCKED.toString();

			if (blockedCC.getStatusCode().equalsIgnoreCase(NOT_BLOCKED_CODE))
			{
				taintedInd = TaintIndicatorType.NOT_BLOCKED.toString();
				accountBlocked = false;
			}else{
				accountBlocked = true;
			}

			//StringBuilder accts = new StringBuilder();
			
			//This holds all cust_acct_id delimited by comma
			while (iter.hasNext())
			{
				CustomerProfileAccount cpa =
					(CustomerProfileAccount) iter.next();
				//accts.append(cpa.getCustAcctId() + ",");
				updateStatus(cpa.getCustAcctId(),cpa.getCustId(),accountBlocked);
			}
			//PWMB-1264 Need to change this code to AccountService update
			/*if (accts != null && !accts.toString().trim().equals(""))
			{

				dao.setTaintedAccountInd(
					userId,
					accts.substring(0, accts.length() - 1),
					taintedInd);
			}*/
			dao.commit();
			
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Exception ex)
		{
			dao.rollback();
			throw new EMGRuntimeException(ex);
		} finally
		{
			dao.close();
		}
		return;
	}

	
	private void saveCardInPCINetwork (String accountId, String cardNumber, ConsumerAccountType accountType) throws Exception {
		try {
			// Call PCI service to save card# in PCI system
			PCIService pciService = emgshared.services.ServiceFactory
					.getInstance().getPCIService();
			pciService.storeCardNumber(accountId, cardNumber, accountType);
		} catch (Exception e) {
			EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).error(
					"**PCI ENCRYPT Service call failed**");
			throw e;
		}
	}

	public Collection getBlockedBins(String userID, String hashText)
		throws DataSourceException, MaxRowsHashCryptoException
	{
		Collection list;
		FraudDAO dao = new FraudDAO();
		try
		{
			list = dao.getBlockedBins(userID, hashText);
		} catch (DataSourceException e)
		{
			throw e;
		} catch (MaxRowsHashCryptoException e1)
		{
			throw e1;
		} catch (SQLException sqlex)
		{
			throw new EMGRuntimeException(sqlex);
		} finally
		{
			dao.close();
		}
		return list;
	}

	public void setBlockedBin(String userId, BlockedBean blockedBin)
		throws DataSourceException
	{

		FraudDAO dao = new FraudDAO();

		try
		{
			//  insert or Update the blocked BIN 
			dao.setBlockedBin(userId, blockedBin);

			String taintedInd = TaintIndicatorType.BLOCKED.toString();

			if (blockedBin.getStatusCode().equalsIgnoreCase(NOT_BLOCKED_CODE))
			{
				taintedInd = TaintIndicatorType.NOT_BLOCKED.toString();
			}

			dao.setTaintedBinInd(userId, blockedBin.getHashText(), taintedInd);

			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (SQLException sqlex)
		{
			dao.rollback();
			throw new EMGRuntimeException(sqlex);
		} catch (ParseException parseEx)
		{
			dao.rollback();
			throw new EMGRuntimeException(parseEx);
		} finally
		{
			dao.close();
		}
		return;
	}

	public void setBlockedPhone(String userId, BlockedBean bean)
		throws DataSourceException
	{

		FraudDAO dao = new FraudDAO();
		try
		{
			//  insert or update the blocked phone
			dao.setBlockedPhone(userId, bean);

			//  try to set the phone taint indicator for all consumer profiles
			String taintedInd = TaintIndicatorType.BLOCKED.toString();
			if (bean.getStatusCode().equalsIgnoreCase(NOT_BLOCKED_CODE))
			{
				taintedInd = TaintIndicatorType.NOT_BLOCKED.toString();
			}

			Collection col =
				dao.getConsumersByPhone(userId, bean.getClearText());
			Iterator iter = col.iterator();
			while (iter.hasNext())
			{
				ConsumerProfileSearchView consumer =
					(ConsumerProfileSearchView) iter.next();
				dao.setTaintedPhoneInd(
					userId,
					Integer.parseInt(consumer.getCustId()),
					bean.getClearText(),
					taintedInd);
			}

			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Exception ex)
		{
			dao.rollback();
			throw new EMGRuntimeException(ex);
		} finally
		{
			dao.close();
		}
		return;
	}

	public Collection getBlockedPhones(String userId, BlockedBean blockedBean)
		throws DataSourceException, TooManyResultException
	{
		Collection list;
		FraudDAO dao = new FraudDAO();
		try
		{
			list = dao.getBlockedPhones(userId, blockedBean);
		} catch (DataSourceException e1)
		{
			throw e1;
		} catch (TooManyResultException e2)
		{
			throw e2;
		} finally
		{
			dao.close();
		}
		return list;
	}

	public Collection getBlockedPhones(String userId, String phone)
		throws DataSourceException, TooManyResultException
	{
		Collection list;
		FraudDAO dao = new FraudDAO();
		try
		{
			list = dao.getBlockedPhones(userId, phone);
		} catch (DataSourceException e)
		{
			throw e;
		} catch (TooManyResultException e1)
		{
			throw e1;
		} finally
		{
			dao.close();
		}
		return list;
	}

	public boolean checkBlockedPhone(String userID, String phone)
		throws DataSourceException
	{
		BlockedBean bb = new BlockedBean();
		bb.setClearText(phone);
		bb.setStatusCode(TaintIndicatorStatus.BLOCKED_STATUS.toString());
		FraudDAO dao = new FraudDAO();
		try
		{
			return dao.getBlockedPhones(userID, bb).size() > 0;
		} catch (DataSourceException e)
		{
			throw e;
		} catch (TooManyResultException ex)
		{
			throw new EMGRuntimeException(ex);
		} finally
		{
			dao.close();
		}
	}

	public Collection getConsumersByPhone(String userId, String phone)
		throws DataSourceException, TooManyResultException
	{

		Collection list;
		FraudDAO dao = new FraudDAO();
		try
		{
			list = dao.getConsumersByPhone(userId, phone);
		} catch (DataSourceException e)
		{
			throw e;
		} catch (TooManyResultException e1)
		{
			throw e1;
		} finally
		{
			dao.close();
		}
		return list;
	}

	public Collection getBlockedBankAccounts(
		String userID,
		BlockedBean blockedBean)
		throws DataSourceException, MaxRowsHashCryptoException
	{
		Collection list;
		FraudDAO dao = new FraudDAO();
		try
		{
			list = dao.getBlockedBankAccounts(userID, blockedBean);
		} catch (DataSourceException e)
		{
			throw e;
		} catch (MaxRowsHashCryptoException e1)
		{
			throw e1;
		} catch (SQLException sqlex)
		{
			throw new EMGRuntimeException(sqlex);
		} finally
		{
			dao.close();
		}
		return list;
	}

	public Collection getBlockedBankAccounts(
		String userID,
		String abaNumber,
		String hashText,
		String maskText)
		throws DataSourceException, MaxRowsHashCryptoException
	{
		Collection list;
		FraudDAO dao = new FraudDAO();
		try
		{
			list =
				dao.getBlockedBankAccounts(
					userID,
					abaNumber,
					hashText,
					maskText);
		} catch (DataSourceException e)
		{
			throw e;
		} catch (MaxRowsHashCryptoException e1)
		{
			throw e1;
		} catch (SQLException sqlex)
		{
			throw new EMGRuntimeException(sqlex);
		} finally
		{
			dao.close();
		}
		return list;
	}

	public void setBlockedBankAccounts(
		String userId,
		BlockedBean blockedAcct,
		Collection consumerProfileAccounts)
		throws DataSourceException, ParseException
	{

		//FraudDAO dao = new FraudDAO();
		boolean blocked=false;

		try
		{
			//  insert or Update the blocked ABA/Bank Account 
			//dao.setBlockedBankAccounts(userId, blockedAcct);

			String taintedInd = TaintIndicatorType.BLOCKED.toString();

			if (blockedAcct.getStatusCode().equalsIgnoreCase(NOT_BLOCKED_CODE))
			{
				taintedInd = TaintIndicatorType.NOT_BLOCKED.toString();
				blocked=false;
			}else{
				blocked=true;
			}

			if (blockedAcct.getMaskText().equals("****"))
			{
				blockedAcct.setMaskText(null);
			}
			
				
			

			/*if (blockedAcct.getMaskText() == null)
			{
				//Blocking the ABA Number
				dao.setTaintedAbaInd(
					userId,
					blockedAcct.getAbaNumber(),
					taintedInd);
			} else
			{*/
				//Blocking Bank accounts matching the bank account entered by the user
				StringBuilder accts = new StringBuilder();
				//This holds all cust_acct_id delimited by comma

				Iterator iter = consumerProfileAccounts.iterator();
				while (iter.hasNext())
				{
					CustomerProfileAccount cpa =
						(CustomerProfileAccount) iter.next();

					//accts.append(cpa.getCustAcctId() + ",");
					updateStatus(cpa.getCustAcctId(),cpa.getCustId(),blocked);
				}
				//PWMB-1264 Need to change this code to AccountService update
				/*
				if (!StringHelper.isNullOrEmpty(accts.toString()))
				{
 					dao.setTaintedAccountInd(
						userId,
						accts.substring(0, accts.length() - 1),
						taintedInd);
				}
				*/
			/*}*/

			
			//dao.commit();
		}
		catch (Exception ex)
		{
			throw new EMGRuntimeException(ex);
		} 
		/*catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Exception ex)
		{
			dao.rollback();
			throw new EMGRuntimeException(ex);
		} finally
		{
			dao.close();
		}*/
		return;
	}

	public Collection getConsumersByAccounts(
			String userID,
			String abaOrBinHash,
			String maskNbr,
			boolean isCC, int acctid)
			throws DataSourceException, TooManyResultException
		{
			Collection list;
			FraudDAO dao = new FraudDAO();
			try
			{
				list =
					dao.getConsumersByAccounts(userID, abaOrBinHash, maskNbr, isCC,acctid);
			} catch (DataSourceException e)
			{
				throw e;
			} catch (TooManyResultException e1)
			{
				throw e1;
			} finally
			{
				dao.close();
			}
			return list;
		}
	
	public Collection getConsumersByAccounts(
		String userID,
		String maskNbr,
		boolean isCC, int acctid, String cardToken)
		throws DataSourceException, TooManyResultException
	{
		Collection list;
		FraudDAO dao = new FraudDAO();
		try
		{
			list =
				dao.getConsumersByAccounts(userID, maskNbr, isCC,acctid, cardToken);
		} catch (DataSourceException e)
		{
			throw e;
		} catch (TooManyResultException e1)
		{
			throw e1;
		} finally
		{
			dao.close();
		}
		return list;
	}

	public Collection getConsumersByEmail(String userID, String emailAddress, String emailDomain)
		throws DataSourceException, TooManyResultException
	{

		Collection list;
		FraudDAO dao = new FraudDAO();
		
		try
		{
			list = dao.getConsumersByEmail(userID, emailAddress, emailDomain, 
				ElectronicTypeCode.EMAIL.getTypeCode());
		} catch (DataSourceException e)
		{
			throw e;
		} catch (TooManyResultException e1)
		{
			throw e1;
		} finally
		{
			dao.close();
		}
		return list;
	}

	public Collection getBlockedEmails(String userID, String emailAddress, String emailDomain)
		throws DataSourceException, TooManyResultException
	{
		Collection list;
		FraudDAO dao = new FraudDAO();
		try
		{
			list = dao.getBlockedEmails(userID, emailAddress, emailDomain);
		} catch (DataSourceException e)
		{
			throw e;
		} catch (TooManyResultException e1)
		{
			throw e1;
		} finally
		{
			dao.close();
		}
		return list;
	}

	public void setBlockedEmail(String userId, String custId, BlockedBean bean)
		throws DataSourceException
	{
		FraudDAO dao = new FraudDAO();
		try
		{
			//  insert or update the blocked Email/Domain
			dao.setBlockedEmail(userId, bean);

			//  try to set the Email taint indicator for all consumer profiles
			String taintedInd = TaintIndicatorType.BLOCKED.toString();
			if (bean.getStatusCode().equalsIgnoreCase(NOT_BLOCKED_CODE))
			{
				taintedInd = TaintIndicatorType.NOT_BLOCKED.toString();
			}
			if (bean.getAddrFmtCode().equals(EMAIL_INDICATOR))					
				dao.setTaintedEmailInd(
					userId,
					custId,
					bean.getClearText(),
					null,
					bean.getElecAddrTypeCode(),
					taintedInd,
					null);
			else
				dao.setTaintedEmailInd(
					userId,
					custId,
					null,
					bean.getClearText(),
					bean.getElecAddrTypeCode(),
					null,
					taintedInd);

			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Exception ex)
		{
			dao.rollback();
			throw new EMGRuntimeException(ex);
		} finally
		{
			dao.close();
		}
		return;
	}

	public Collection getNegativeTermList(String userID, 
											String type, 
											String negativeString, 
											int flag)
	throws DataSourceException, TooManyResultException
	{
		Collection list;
		FraudDAO dao = new FraudDAO();
		try
		{
			list = dao.getNegativeTerms(userID, type, negativeString, flag);
		} catch (DataSourceException e)
		{
			throw e;
		} catch (TooManyResultException e1)
		{
			throw e1;
		} finally
		{
			dao.close();
		}
		return list;
	}

	public void setNegativeTerm(String userId, String negativeType, String negativeString)
	throws DataSourceException
	{
		FraudDAO dao = new FraudDAO();
		try
		{
			//  insert or update the blocked Email/Domain
			dao.setNegativeTerm(userId, negativeType, negativeString);	
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} finally
		{
			dao.close();
		}
		return;
	}
	
	public void deleteNegativeTerm(String userID, String negativeNumber)
	throws DataSourceException
	{
		FraudDAO dao = new FraudDAO();
		try
		{
			//  insert or update the blocked Email/Domain
			dao.deleteNegativeTerm(userID, negativeNumber);	
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} finally
		{
			dao.close();
		}
		return;
	}
	
	// used to clear SSN blocks only - MGO project, full SSN dropped from DB, need a way
	// to clear legacy SSN blocks.
	public void clearSSNBlockIndicator(String custId, boolean clearCustomerProfileMasterTaint, String userId)
	throws DataSourceException
	{
		FraudDAO dao = new FraudDAO();
		ConsumerProfileDAO cpDao = new ConsumerProfileDAO();
	
		try
		{
			dao.setTaintedSsnInd(userId,Integer.parseInt(custId),TaintIndicatorType.NOT_BLOCKED.getStatusIndicator());
			if (clearCustomerProfileMasterTaint)
				cpDao.updateCustBlkdCode(Integer.parseInt(custId), TaintIndicatorType.NOT_BLOCKED.getStatusIndicator(), userId);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Exception ex)
		{
			dao.rollback();
			throw new EMGRuntimeException(ex);
		} finally
		{
			dao.close();
			cpDao.close();
		}
		return;
	}

	// used to clear Master profile taint indicator when no other data entities are blocked
	public void clearMasterBlockIndicator(String custId, String userId)
	throws DataSourceException
	{
		ConsumerProfileDAO cpDao = new ConsumerProfileDAO();
		try
		{
			cpDao.updateCustBlkdCode(Integer.valueOf(custId).intValue(), TaintIndicatorType.NOT_BLOCKED.getStatusIndicator(), userId);
			cpDao.commit();
		} catch (DataSourceException e)
		{
			cpDao.rollback();
			throw e;
		} finally
		{			cpDao.close();
		}
		return;
	}
	
	//PWMB-1264 : code to call Account Service update	
	public UpdateAccountServiceResponse updateStatus(int accountId,int custId,boolean accountBlocked)
			throws DataSourceException {
		
		DefaultHttpClient httpClient = null;
		UpdateAccountServiceRequest request = new UpdateAccountServiceRequest();
		UpdateAccountServiceResponse response = new UpdateAccountServiceResponse();
		try {
			httpClient = new DefaultHttpClient();
			Gson gson = new GsonBuilder().create();
			request.setAccountId(String.valueOf(accountId));
			request.setCustId(String.valueOf(custId));
			request.setAccountBlocked(accountBlocked);
			HttpPost postRequest = new HttpPost(EMTSharedContainerProperties
					.getAccountServiceURL().concat("/account/update"));
			String uacRequest = gson.toJson(request);
			LOGGER.info("Account Service URL : "+EMTSharedContainerProperties
					.getAccountServiceURL().concat("/account/update"));
			LOGGER.info("Account Service : Update Status JSON Request :: " + uacRequest);
			StringEntity input = new StringEntity(uacRequest);
			input.setContentType("application/json");
			postRequest.setEntity(input);
			HttpResponse httpResponse = httpClient.execute(postRequest);
			String responseJSONStr = EntityUtils.toString(httpResponse
					.getEntity());
			response = gson.fromJson(responseJSONStr,
					UpdateAccountServiceResponse.class);
		} catch (MalformedURLException e) {
			LOGGER.error("MalformedURLException in updateStatus for accountId {}"+request.getAccountId(),e);
		} catch (Exception e) {
			LOGGER.error("Exception occoured in updateStatus for accountId {}"+request.getAccountId(),e);
		} finally {
			httpClient.getConnectionManager().shutdown();
		}
		return response;
	}

}