package emgshared.services;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.MultiMap;

import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.dataaccessors.MicroDepositDAO;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.MicroDeposit;

public class MicroDepositServiceImpl implements MicroDepositService
{

	private static final MicroDepositServiceImpl instance =
		new MicroDepositServiceImpl();

	private MicroDepositServiceImpl()
	{
	}

	public static final MicroDepositServiceImpl getInstance()
	{
		return instance;
	}

	public MultiMap getMicroDepositAccounts(int custId)
	{
		MicroDepositDAO dao = new MicroDepositDAO();
		MultiMap accounts = null;
		try
		{
			accounts = dao.getMicroDepositAccounts(custId);
			dao.commit();
		} catch (DataSourceException dse)
		{
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).warn(
				"MicroDepositServiceImpl - Error: Failed to get micro deposit accounts.",
				dse);
			dao.rollbackAndIgnoreException();
		} catch (SQLException sqlex)
		{
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).warn(
				"MicroDepositServiceImpl - Error: Failed to get micro deposit accounts.",
				sqlex);
			dao.rollbackAndIgnoreException();
		} finally
		{
			dao.close();
		}
		return accounts;
	}
	public Collection getMicroDepositValidations(MicroDeposit microDeposit)
	{
		MicroDepositDAO dao = new MicroDepositDAO();
		Collection accounts = null;
		try
		{
			accounts = dao.getMicroDepositValidations(microDeposit);
			dao.commit();
		} catch (DataSourceException dse)
		{
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).warn(
				"MicroDepositServiceImpl - Error: Failed to get micro deposit accounts.",
				dse);
			dao.rollbackAndIgnoreException();
		} catch (SQLException sqlex)
		{
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).warn(
				"MicroDepositServiceImpl - Error: Failed to get micro deposit accounts.",
				sqlex);
			dao.rollbackAndIgnoreException();
		} finally
		{
			dao.close();
		}
		return accounts;
	}
	public MicroDeposit validateDepositAmounts(MicroDeposit microDeposit)
	{
		MicroDepositDAO dao = new MicroDepositDAO();
		MicroDeposit mdReturn = null;
		try
		{
			mdReturn = dao.validateMicroDepositAmounts(microDeposit);
			dao.commit();
		} catch (DataSourceException dse)
		{
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).warn(
				"MicroDepositServiceImpl - Error: Failed to get micro deposit accounts.",
				dse);
			dao.rollbackAndIgnoreException();
		} catch (SQLException sqlex)
		{
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).warn(
				"MicroDepositServiceImpl - Error: Failed to get micro deposit accounts.",
				sqlex);
			dao.rollbackAndIgnoreException();
		} finally
		{
			dao.close();
		}
		return mdReturn;
	}

	public void insertMicroDepositTransaction(MicroDeposit microDeposit)
		throws DataSourceException
	{
		MicroDepositDAO dao = new MicroDepositDAO();
		try
		{
			// Make two Micro Deposits.
			int validationId = dao.insertMicroDepositValidation(microDeposit);
			microDeposit.setValidationId(validationId);
			dao.insertMicroDepositTransaction(microDeposit);
			// Call insert again and set the index to 1 so that it
			// grabs deposit amount 2.
			microDeposit.setDepositAmountIndex(1);
			dao.insertMicroDepositTransaction(microDeposit);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} finally
		{
			dao.close();
		}
	}

	public void updateMicroDepositValidation(MicroDeposit microDeposit)
		throws DataSourceException
	{
		MicroDepositDAO dao = new MicroDepositDAO();
		try
		{
			// Make two Micro Deposits.
			dao.updateMicroDepositValidation(microDeposit);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} finally
		{
			dao.close();
		}
	}

	public List getMDBatchList(String beginDate, String endDate, String userId)
		throws DataSourceException
	{
		List batchList = null;
		MicroDepositDAO dao = new MicroDepositDAO();
		try
		{
			batchList = dao.getMDBatchList(beginDate, endDate, userId);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (SQLException sqlex)
		{
			dao.rollback();
			throw new EMGRuntimeException(sqlex);
		} finally
		{
			dao.close();
		}

		return batchList;
	}

	public List getMDTrans(
		Integer batchId,
		Integer tranId,
		String tranStat,
		String userId)
		throws DataSourceException
	{
		List vldnList = null;
		MicroDepositDAO dao = new MicroDepositDAO();
		try
		{
			vldnList = dao.getMDTrans(batchId, tranId, tranStat, userId);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (SQLException sqlex)
		{
			dao.rollback();
			throw new EMGRuntimeException(sqlex);
		} finally
		{
			dao.close();
		}

		return vldnList;
	}

	public void updateMDStatus(MicroDeposit microDeposit)
		throws DataSourceException
	{
		MicroDepositDAO dao = new MicroDepositDAO();
		try
		{
			// Make two Micro Deposits.
			dao.updateMDStatus(microDeposit);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} finally
		{
			dao.close();
		}
	}

	public void overrideMicroDepositStatus(MicroDeposit md)
		throws DataSourceException
	{
		MicroDepositDAO dao = new MicroDepositDAO();
		try
		{
			// Make two Micro Deposits.
			dao.overrideMicroDepositStatus(md);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} finally
		{
			dao.close();
		}
	}

}
