package emgshared.services;

import java.text.ParseException;
import java.util.Collection;

import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.BillerLimit;

public interface BillerLimitService
{
	Collection getBillerLimits(String searchString)
		throws DataSourceException, TooManyResultException;
	
	Collection getActiveBillerLimits(String searchString)
			throws DataSourceException, TooManyResultException;

	BillerLimit getBillerLimit(int id)
		throws DataSourceException;
		
	BillerLimit getActiveBillerLimit(int id)
			throws DataSourceException;
			
	BillerLimit getBillerLimit(String billerCode)
			throws DataSourceException;
	
	BillerLimit getActiveBillerLimit(String billerCode)
				throws DataSourceException;
			
	void setBillerLimit(BillerLimit billerLimits)
		throws DataSourceException, ParseException;

	void deleteBillerLimit(int id)
		throws DataSourceException;
}
