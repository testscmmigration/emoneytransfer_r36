package emgshared.services;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.MultiMap;

import emgshared.exceptions.DataSourceException;
import emgshared.model.MicroDeposit;

public interface MicroDepositService
{

	MultiMap getMicroDepositAccounts(int custId) throws DataSourceException;

	Collection getMicroDepositValidations(MicroDeposit microDeposit)
		throws DataSourceException;

	void insertMicroDepositTransaction(MicroDeposit microDeposit)
		throws DataSourceException;

	void updateMicroDepositValidation(MicroDeposit microDeposit)
		throws DataSourceException;

	MicroDeposit validateDepositAmounts(MicroDeposit microDeposit)
		throws DataSourceException;

	List getMDBatchList(String beginDate, String endDate, String userId)
		throws DataSourceException;

	List getMDTrans(
		Integer batchId,
		Integer tranId,
		String tranStat,
		String userId)
		throws DataSourceException;

	void updateMDStatus(MicroDeposit microDeposit) throws DataSourceException;
	void overrideMicroDepositStatus(MicroDeposit md)
		throws DataSourceException;
}
