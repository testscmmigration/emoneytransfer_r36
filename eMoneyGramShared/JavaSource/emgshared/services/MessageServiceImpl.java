package emgshared.services;

import java.util.List;
import java.util.Map;

import emgshared.dataaccessors.CacheDAO;
import emgshared.dataaccessors.MessageDAO;
import emgshared.exceptions.DataSourceException;
import emgshared.model.AdminMessage;
import emgshared.model.SimpleCache;

public class MessageServiceImpl implements MessageService
{
	private static final MessageServiceImpl instance = new MessageServiceImpl();

	private MessageServiceImpl()
	{
	}

	public static final MessageServiceImpl getInstance()
	{
		return instance;
	}

	public List getAdminMessages(String userId, AdminMessage criteria)
		throws DataSourceException
	{
		List list = null;
		MessageDAO dao = new MessageDAO();
		try
		{
			list = dao.getAdminMessages(userId, criteria);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} finally
		{
			dao.close();
		}
		return list;
	}

	public Map getActiveUserMessages(String userId) throws DataSourceException
	{
		Map map = null;
		MessageDAO dao = new MessageDAO();
		try
		{
			map = dao.getActiveUserMessages(userId);
			dao.commit();
		} catch (Exception e)
		{
			dao.rollback();
			throw new DataSourceException(e);
		} finally
		{
			dao.close();
		}
		return map;
	}

	public List getAdminMessageTypes(String userId) throws DataSourceException
	{
		List list = null;
		MessageDAO dao = new MessageDAO();
		try
		{
			list = dao.getAdminMessageTypes(userId);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} finally
		{
			dao.close();
		}
		return list;
	}

	public AdminMessage insertAdminMessage(
		String userId,
		AdminMessage msg,
		String[] users)
		throws DataSourceException
	{
		AdminMessage am = null;
		MessageDAO dao1 = new MessageDAO();
		CacheDAO dao2 = new CacheDAO();
		try
		{
			am = dao1.insertAdminMessage(userId, msg, users);
			dao2.updateCache(SimpleCache.ADMIN_MESSAGE);
			dao1.commit();
			dao2.commit();
		} catch (DataSourceException e)
		{
			dao1.rollback();
			dao2.rollback();
			throw e;
		} finally
		{
			dao1.close();
			dao2.close();
		}

		return am;
	}

	public List getUserAdminMessages(
		String userId,
		String recipntId,
		boolean unreadOnly)
		throws DataSourceException
	{
		List msgList = null;
		MessageDAO dao = new MessageDAO();
		try
		{
			msgList = dao.getUserAdminMessages(userId, recipntId, unreadOnly);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} finally
		{
			dao.close();
		}

		return msgList;
	}

	public void setMsgRecipnt(int msgId, String recipntId, boolean toUnread)
		throws DataSourceException
	{
		MessageDAO dao1 = new MessageDAO();
		CacheDAO dao2 = new CacheDAO();
		try
		{
			dao1.setMsgRecipnt(msgId, recipntId, toUnread);
			dao2.updateCache(SimpleCache.ADMIN_MESSAGE);
			dao1.commit();
			dao2.commit();
		} catch (DataSourceException e)
		{
			dao1.rollback();
			dao2.rollback();
			throw e;
		} finally
		{
			dao1.close();
			dao2.close();
		}
	}

	public void purgeMessage(String userId, int msgId)
		throws DataSourceException
	{
		MessageDAO dao1 = new MessageDAO();
		CacheDAO dao2 = new CacheDAO();
		try
		{
			dao1.purgeMessage(userId, msgId);
			dao2.updateCache(SimpleCache.ADMIN_MESSAGE);
			dao1.commit();
			dao2.commit();
		} catch (DataSourceException e)
		{
			dao1.rollback();
			dao2.rollback();
			throw e;
		} finally
		{
			dao1.close();
			dao2.close();
		}
	}

	public List getAdminMessageRecipients(String userId, int msgId)
		throws DataSourceException
	{
		MessageDAO dao = new MessageDAO();
		List list = null;

		try
		{
			list = dao.getAdminMessageRecipients(userId, msgId);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} finally
		{
			dao.close();
		}

		return list;
	}
}