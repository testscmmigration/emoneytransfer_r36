package emgshared.services.backgroundprocess;

import java.util.TimerTask;

import emgshared.dataaccessors.EMGSharedLogger;

import shared.mgo.services.AgentConnectService;
import shared.mgo.services.AgentConnectServiceImpl;

/**
 * @author rlopez2@moneygram.com
 */
public class MasterInformationLongTermRefreshRun extends TimerTask {
	
	private AgentConnectService acs = AgentConnectServiceImpl.getInstance();

	public void run() {
		System.out.println("MasterInformationRefreshRun - more than one day");
		// master refresh
		try {
			acs.refreshMasterFromAgentConnect();
		} catch (Exception e) {
			EMGSharedLogger.getLogger(this.getClass()).error(e.getMessage(), e);
		}
	}

}
