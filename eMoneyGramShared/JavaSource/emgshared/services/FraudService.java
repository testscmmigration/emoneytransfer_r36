package emgshared.services;

import java.text.ParseException;
import java.util.Collection;

import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.TooManyResultException;
import emgshared.exceptions.MaxRowsHashCryptoException;
import emgshared.model.BlockedBean;
import emgshared.model.BlockedIP;

public interface FraudService {
	Collection getBlockedIPs(String userID, String beginIP, String endIP)
		throws DataSourceException, TooManyResultException;
	Collection getBlockedIPs(String userID, BlockedIP blockedIP)
		throws DataSourceException, TooManyResultException;
	Collection getBlockedReasons(String userID, String reasonType)
		throws DataSourceException;
	void setBlockedIPs(String userID, BlockedIP blockedIP)
		throws DataSourceException, ParseException;
	Collection getBlockedStatus(String userID, String blockedType)
		throws DataSourceException;
	Collection getBlockedCCAccounts(String userID,String blockedID,
			String maskText)
		throws DataSourceException, MaxRowsHashCryptoException;
	Collection getBlockedCCAccounts(String userID,String blockedID,
		String maskText, String cardToken)
		throws DataSourceException, MaxRowsHashCryptoException;
	Collection getBlockedCCAccounts(String userID, BlockedBean blockedBean)
		throws DataSourceException, MaxRowsHashCryptoException;
	void setBlockedCC(String userID, BlockedBean blockedCC, Collection customerProfileAccounts)
		throws DataSourceException, ParseException;
	Collection getBlockedBins(String userID, String hashText)
		throws DataSourceException, MaxRowsHashCryptoException;
	void setBlockedBin(String userID, BlockedBean blockedBin)
		throws DataSourceException, ParseException;
	void setBlockedPhone(String userId, BlockedBean bean)
		throws DataSourceException;
	Collection getBlockedPhones(String userId, String phone)
		throws DataSourceException, TooManyResultException;
	Collection getBlockedPhones(String userId, BlockedBean blockedBean)
		throws DataSourceException, TooManyResultException;
	Collection getConsumersByPhone(String userId, String phone)
		throws DataSourceException, TooManyResultException;
	Collection getBlockedBankAccounts(String userID,String abaNumber,String hashText,String maskText)
		throws DataSourceException, MaxRowsHashCryptoException;
	Collection getBlockedBankAccounts(String userID, BlockedBean blockedBean)
		throws DataSourceException, MaxRowsHashCryptoException;
	void setBlockedBankAccounts(String userID, BlockedBean blockedAcct, Collection ConsumerProfileAccounts)
		throws DataSourceException, ParseException;
	Collection getConsumersByAccounts(String userID,String abaOrBinHash,String maskNbr,	boolean isCC, int acctId)
		throws DataSourceException, TooManyResultException;
	Collection getConsumersByAccounts(String userID,String maskNbr,	boolean isCC, int acctId, String cardToken)
		throws DataSourceException, TooManyResultException;
	boolean checkBlockedPhone(String userID, String phone)
		throws DataSourceException;
	public Collection getConsumersByEmail(String userID, String emailAddress, String emailDomain)
		throws DataSourceException, TooManyResultException;
	Collection getBlockedEmails(String userID, String emailAddress, String emailDomain)
		throws DataSourceException, TooManyResultException;
	void setBlockedEmail(String userId, String custId, BlockedBean bean)
		throws DataSourceException;
	Collection getNegativeTermList(String userID, String type, String negativeString, int flag)
		throws DataSourceException, TooManyResultException;
	void setNegativeTerm(String userId, String negativeType, String negativeString)
		throws DataSourceException;
	void deleteNegativeTerm(String userID, String negativeNumber)
		throws DataSourceException;
	void clearSSNBlockIndicator(String custId, boolean clearCustomerProfileMasterTaint, String userId) 
		throws DataSourceException;
	void clearMasterBlockIndicator(String custId, String userId) throws DataSourceException;
	
}
