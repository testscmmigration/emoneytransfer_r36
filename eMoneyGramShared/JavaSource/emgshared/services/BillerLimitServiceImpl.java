package emgshared.services;

import java.text.ParseException;
import java.util.Collection;

import emgshared.dataaccessors.BillerLimitDAO;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.BillerLimit;

class BillerLimitServiceImpl implements BillerLimitService
{
	private static final BillerLimitServiceImpl instance =
		new BillerLimitServiceImpl();
	private static String ACTIVE_BILLER="ACT"; 

	private BillerLimitServiceImpl()
	{
	}

	public static final BillerLimitServiceImpl getInstance()
	{
		return instance;
	}

	public void setBillerLimit(BillerLimit billerLimits)
		throws DataSourceException, ParseException {
		BillerLimitDAO dao = new BillerLimitDAO();
		try
		{
			dao.setBillerLimit(billerLimits);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Exception e)
		{
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			dao.close();
		}
		return;
	}

	public void deleteBillerLimit(int id)
		throws DataSourceException {
		BillerLimitDAO dao = new BillerLimitDAO();
		try
		{
			dao.deleteBillerLimit(id);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Exception e)
		{
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			dao.close();
		}
		return;
	}

	/**
	 * 
	 */
	public BillerLimit getBillerLimit(int id)
		throws DataSourceException {
			return getBillerLimit(id, null);
	}
	
	/**
	 * 
	 */
	public BillerLimit getActiveBillerLimit(int id)
			throws DataSourceException {
				return getBillerLimit(id, ACTIVE_BILLER);
	}
	
	
	public BillerLimit getBillerLimit(int id, String filter)
	throws DataSourceException {

		BillerLimit selectedBillerLimit;
		BillerLimitDAO dao = new BillerLimitDAO();
		try
		{
			selectedBillerLimit =
				dao.getBillerLimit(id,filter);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Exception e)
		{
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			dao.close();
		}
		return selectedBillerLimit;
	}
	
	/**
	 * 
	 */
	public BillerLimit getBillerLimit(String  billerCode) throws DataSourceException
	{
		return getBillerLimit(billerCode, null);
	}
	
	/**
	 * 
	 * @param billerCode
	 * @return
	 * @throws DataSourceException
	 */
	public BillerLimit getActiveBillerLimit(String  billerCode) throws DataSourceException
	{
		return getBillerLimit(billerCode, ACTIVE_BILLER);
	}
	
	public BillerLimit getBillerLimit(String  billerCode, String filter)
		throws DataSourceException {

			BillerLimit selectedBillerLimit;
			BillerLimitDAO dao = new BillerLimitDAO();
			try
			{
				selectedBillerLimit =
					dao.getBillerLimit(billerCode, filter);
				dao.commit();
			} catch (DataSourceException e)
			{
				dao.rollback();
				throw e;
			} catch (Exception e)
			{
				dao.rollback();
				throw new EMGRuntimeException(e);
			} finally
			{
				dao.close();
			}
			return selectedBillerLimit;
		}

	
	public Collection getBillerLimits(String searchString) 
			throws DataSourceException, TooManyResultException
	{
		return getBillerLimits(searchString, null);
	}
	
	/**
	 * 
	 * @param searchString
	 * @return
	 * @throws DataSourceException
	 * @throws TooManyResultException
	 */
	public Collection getActiveBillerLimits(String searchString) 
				throws DataSourceException, TooManyResultException
	{
		return getBillerLimits(searchString, ACTIVE_BILLER);
	}
	
	public Collection getBillerLimits(String searchString, String filter) 
		throws DataSourceException, TooManyResultException
	{
		Collection list;
		BillerLimitDAO dao = new BillerLimitDAO();
		try
		{
			list = dao.getBillerLimits(searchString, filter);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (TooManyResultException e1)
		{
			dao.rollback();
			throw new TooManyResultException(e1.getMessage());
		} catch (Exception e)
		{
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			dao.close();
		}
		return list;
	}
}