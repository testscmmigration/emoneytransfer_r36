/*
 * Created on Jan 6, 2005
 *
 */
package emgshared.services;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.Validate;

import emgshared.dataaccessors.ConsumerAccountDAO;
import emgshared.dataaccessors.ConsumerProfileDAO;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.AccountComment;
import emgshared.model.AccountSearchRequest;
import emgshared.model.AccountStatus;
import emgshared.model.ConsumerAccount;
import emgshared.model.ConsumerAccountType;
import emgshared.model.ConsumerAddress;
import emgshared.model.ConsumerBankAccount;
import emgshared.model.ConsumerCreditCardAccount;
import emgshared.model.NewConsumerBankAccount;
import emgshared.model.NewConsumerCreditCardAccount;
import emgshared.model.TaintIndicatorType;
import emgshared.model.UpdateAccountServiceRequest;
import emgshared.model.UpdateAccountServiceResponse;
import emgshared.property.EMTSharedContainerProperties;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
/**
 * @author A131
 *
 */
public class ConsumerAccountServiceImpl implements ConsumerAccountService
{
	private static final ConsumerAccountService instance =
		new ConsumerAccountServiceImpl();

	public static Map accountTypeDescriptions = new HashMap(0);
	public static Map accountStatusDescriptions = new HashMap(0);
	public static Map accountCommentReasons = new HashMap(0);
	
	private static final String APPLICATION_JSON_UTF8 = "application/json; charset=utf-8";
	private static final String UTF8 = "UTF-8";
	private static final Logger LOGGER = EMGSharedLogger
	.getLogger("ConsumerAccountServiceImpl");

	protected ConsumerAccountServiceImpl()
	{
	}

	public static final ConsumerAccountService getInstance()
	{
		return instance;
	}

	public ConsumerCreditCardAccount addConsumerCreditCardAccount(
			NewConsumerCreditCardAccount newAccount)
		throws DataSourceException
	{
		ConsumerCreditCardAccount savedAccount;
		ConsumerAccountDAO accountDao = new ConsumerAccountDAO();
		try
		{
			int accountId = accountDao.insertCreditCardAccount(newAccount);
			int consumerId = newAccount.getConsumerId();
			savedAccount =
				accountDao.getCreditCardAccount(
					accountId,
					Integer.valueOf(consumerId),
					String.valueOf(consumerId));
			//Invokes webservice call to save card to PCI system.  If it fails, we want to 
			//rollback the database change
			saveCardInPCINetwork(accountId, newAccount.getAccountNumber(), newAccount.getAccountType());
			accountDao.commit();
		} catch (DataSourceException e)
		{
			accountDao.rollback();
			throw e;
		} catch (Exception e)
		{
			accountDao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			accountDao.close();
		}
		return savedAccount;
	}

	private void saveCardInPCINetwork (int accountId, String cardNumber, ConsumerAccountType accountType) throws Exception {
		try {
			// Call PCI service to save card# in PCI system
			PCIService pciService = emgshared.services.ServiceFactory
					.getInstance().getPCIService();
			pciService.storeCardNumber(Integer
					.toString(accountId), cardNumber, accountType);
		} catch (Exception e) {
			EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).error(
					"**PCI ENCRYPT Service call failed**");
			throw e;
		}
	}
	public ConsumerBankAccount addConsumerBankAccount(NewConsumerBankAccount newAccount)
		throws DataSourceException
	{
		ConsumerBankAccount savedAccount;
		ConsumerAccountDAO dao = new ConsumerAccountDAO();
		try
		{
			int id = dao.insertBankAccount(newAccount);
			int consumerId = newAccount.getConsumerId();
			savedAccount =
				dao.getBankAccount(
					id,
					Integer.valueOf(consumerId),
					String.valueOf(consumerId));
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Exception e)
		{
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			dao.close();
		}
		return savedAccount;
	}

	public ConsumerBankAccount getBankAccount(
		int accountId,
		String callerUserId)
		throws DataSourceException
	{
		return getBankAccount(accountId, null, callerUserId);
	}

	public ConsumerBankAccount getBankAccount(int accountId, int consumerId)
		throws DataSourceException
	{
		return getBankAccount(
			accountId,
			Integer.valueOf(consumerId),
			String.valueOf(consumerId));
	}

	private ConsumerBankAccount getBankAccount(
		int accountId,
		Integer consumerId,
		String callerUserId)
		throws DataSourceException
	{
		ConsumerBankAccount account = null;
		ConsumerAccountDAO accountDao = new ConsumerAccountDAO();
		ConsumerProfileDAO profileDao = new ConsumerProfileDAO();
		try
		{
			account =
				accountDao.getBankAccount(accountId, consumerId, callerUserId);
			//TODO currently the stored proc for getting accounts doesn't return the address
			//	so for now we handle separately.
			ConsumerAddress address =
				profileDao.getConsumerAddress(
					account.getConsumerId(),
					account.getAddressId());
			if (address != null)
			{
				account.setAddressLine1(address.getAddressLine1());
				account.setAddressLine2(address.getAddressLine2());
				account.setCity(address.getCity());
				account.setIsoCountryCode(address.getIsoCountryCode());
				account.setPostalCode(address.getPostalCode());
				account.setState(address.getState());
				account.setZip4(address.getZip4());
			}
			accountDao.commit();
			profileDao.commit();
		} catch (DataSourceException e)
		{
			accountDao.rollback();
			profileDao.rollback();
			throw e;
		} catch (Exception e)
		{
			accountDao.rollback();
			profileDao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			accountDao.close();
			profileDao.close();
		}
		return account;
	}

	public ConsumerCreditCardAccount getCreditCardAccount(
		int accountId,
		String callerUserId)
		throws DataSourceException
	{
		return getCreditCardAccount(accountId, null, callerUserId);
	}

	public ConsumerCreditCardAccount getCreditCardAccount(
		int accountId,
		int consumerId)
		throws DataSourceException
	{
		return getCreditCardAccount(
			accountId,
			Integer.valueOf(consumerId),
			String.valueOf(consumerId));
	}

	private ConsumerCreditCardAccount getCreditCardAccount(
		int accountId,
		Integer consumerId,
		String callerUserId)
		throws DataSourceException
	{
		ConsumerCreditCardAccount account = null;
		ConsumerAccountDAO accountDao = new ConsumerAccountDAO();
		ConsumerProfileDAO profileDao = new ConsumerProfileDAO();
		try
		{
			account = accountDao.getCreditCardAccount(accountId,consumerId,callerUserId);
			//TODO currently the stored proc for getting accounts doesn't return the address
			//	so for now we handle separately.
			ConsumerAddress address = profileDao.getConsumerAddress(account.getConsumerId(),account.getAddressId());
			if (address != null)
			{
				account.setAddressLine1(address.getAddressLine1());
				account.setAddressLine2(address.getAddressLine2());
				account.setCity(address.getCity());
				account.setIsoCountryCode(address.getIsoCountryCode());
				account.setPostalCode(address.getPostalCode());
				account.setState(address.getState());
				account.setZip4(address.getZip4());
			}
			accountDao.commit();
			profileDao.commit();
		} catch (DataSourceException e)
		{
			accountDao.rollback();
			profileDao.rollback();
			throw e;
		} catch (Exception e)
		{
			accountDao.rollback();
			profileDao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			accountDao.close();
			profileDao.close();
		}
		return account;
	}

	public String getEncryptedAccountNumber(int accountId, String callerUserId)
		throws DataSourceException
	{
		String encryptedNumber = null;
		ConsumerAccountDAO accountDao = new ConsumerAccountDAO();
		try
		{
			encryptedNumber =
				accountDao.getEncryptedAccountNumber(accountId, callerUserId);
			accountDao.commit();
		} catch (DataSourceException e)
		{
			accountDao.rollback();
			throw e;
		} catch (Exception e)
		{
			accountDao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			accountDao.close();
		}
		return encryptedNumber;
	}

	public Set getActiveAccountsByConsumerId(int consumerId)
		throws DataSourceException
	{
		//TODO these account don't have address information!
		Set accounts = new HashSet(0);
		ConsumerAccountDAO accountDao = new ConsumerAccountDAO();
		ConsumerProfileDAO profileDao = new ConsumerProfileDAO();
		try
		{
			AccountSearchRequest searchCriteria = new AccountSearchRequest();
			searchCriteria.setCustId(Integer.valueOf(consumerId));
			searchCriteria.setStatusCode(AccountStatus.ACTIVE_CODE);
			accounts = accountDao.getAccounts(searchCriteria, (String) null);
			ConsumerAddress address = null;
			for (Iterator iter = accounts.iterator(); iter.hasNext();)
			{
				ConsumerAccount account = (ConsumerAccount) iter.next();
				//TODO currently the stored proc for getting accounts doesn't return the address
				//	so for now we handle separately.
				if (address == null)
				{
				    address = profileDao.getConsumerAddress(consumerId,account.getAddressId());
				}
				if (address != null)
				{
					account.setAddressLine1(address.getAddressLine1());
					account.setAddressLine2(address.getAddressLine2());
					account.setCity(address.getCity());
					account.setIsoCountryCode(address.getIsoCountryCode());
					account.setPostalCode(address.getPostalCode());
					account.setState(address.getState());
					account.setZip4(address.getZip4());
					account.setAccountNumberTaintCode(
						TaintIndicatorType.getInstance(
							account.getAccountNumberTaintCode().toString()));
					account.setBankAbaTaintCode(
						TaintIndicatorType.getInstance(
							account.getBankAbaTaintCode().toString()));
					account.setCreditCardBinTaintCode(
						TaintIndicatorType.getInstance(
							account.getCreditCardBinTaintCode().toString()));
				}
			}
			accountDao.commit();
			profileDao.commit();
		} catch (DataSourceException e)
		{
			accountDao.rollback();
			profileDao.rollback();
			throw e;
		} catch (Exception e)
		{
			accountDao.rollback();
			profileDao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			accountDao.close();
			profileDao.close();
		}
		return accounts;
	}

	public Set getAllAccountsByConsumerId(
		int consumerId)
		throws DataSourceException
	{
		Set accounts = new HashSet(0);
		ConsumerAccountDAO accountDao = new ConsumerAccountDAO();
		ConsumerProfileDAO profileDao = new ConsumerProfileDAO();
		try
		{
			AccountSearchRequest searchCriteria = new AccountSearchRequest();
			searchCriteria.setCustId(Integer.valueOf(consumerId));

			accounts = accountDao.getAccounts(searchCriteria, (String) null);
			ConsumerAddress address = null;
			for (Iterator iter = accounts.iterator(); iter.hasNext();)
			{
				ConsumerAccount account = (ConsumerAccount) iter.next();
				//TODO currently the stored proc for getting accounts doesn't return the address
				//	so for now we handle separately.
				if (address == null)
				{
				    address = profileDao.getConsumerAddress(consumerId,account.getAddressId());
				}
				if (address != null)
				{
					account.setAddressLine1(address.getAddressLine1());
					account.setAddressLine2(address.getAddressLine2());
					account.setCity(address.getCity());
					account.setIsoCountryCode(address.getIsoCountryCode());
					account.setPostalCode(address.getPostalCode());
					account.setState(address.getState());
					account.setZip4(address.getZip4());
				}
				account.setAccountNumberTaintCode(
					TaintIndicatorType.getInstance(
						account.getAccountNumberTaintCode().toString()));
				account.setBankAbaTaintCode(
					TaintIndicatorType.getInstance(
						account.getBankAbaTaintCode().toString()));
				account.setCreditCardBinTaintCode(
					TaintIndicatorType.getInstance(
						account.getCreditCardBinTaintCode().toString()));
			}
			accountDao.commit();
			profileDao.commit();
		} catch (DataSourceException e)
		{
			accountDao.rollback();
			profileDao.rollback();
			throw e;
		} catch (Exception e)
		{
			accountDao.rollback();
			profileDao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			accountDao.close();
			profileDao.close();
		}
		return accounts;
	}

	public Set getAllAccountsByConsumerIdWithCmnts(
		int consumerId)
		throws DataSourceException
	{
		Set accounts = new HashSet(0);
		ConsumerAccountDAO accountDao = new ConsumerAccountDAO();
		ConsumerProfileDAO profileDao = new ConsumerProfileDAO();
		try
		{
			AccountSearchRequest searchCriteria = new AccountSearchRequest();
			searchCriteria.setCustId(Integer.valueOf(consumerId));

			accounts = accountDao.getAccountsWithComments(searchCriteria, (String) null);
			ConsumerAddress address = null;
			for (Iterator iter = accounts.iterator(); iter.hasNext();)
			{
				ConsumerAccount account = (ConsumerAccount) iter.next();
				//TODO currently the stored proc for getting accounts doesn't return the address
				//	so for now we handle separately.
				if (address == null)
				{
				    address = profileDao.getConsumerAddress(consumerId,account.getAddressId());
				}

				if (address != null)
				{
					account.setAddressLine1(address.getAddressLine1());
					account.setAddressLine2(address.getAddressLine2());
					account.setCity(address.getCity());
					account.setIsoCountryCode(address.getIsoCountryCode());
					account.setPostalCode(address.getPostalCode());
					account.setState(address.getState());
					account.setZip4(address.getZip4());
				}
				account.setAccountNumberTaintCode(
					TaintIndicatorType.getInstance(
						account.getAccountNumberTaintCode().toString()));
				account.setBankAbaTaintCode(
					TaintIndicatorType.getInstance(
						account.getBankAbaTaintCode().toString()));
				account.setCreditCardBinTaintCode(
					TaintIndicatorType.getInstance(
						account.getCreditCardBinTaintCode().toString()));
			}
			accountDao.commit();
			profileDao.commit();
		} catch (DataSourceException e)
		{
			accountDao.rollback();
			profileDao.rollback();
			throw e;
		} catch (Exception e)
		{
			accountDao.rollback();
			profileDao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			accountDao.close();
			profileDao.close();
		}
		return accounts;
	}

	public ConsumerAccount getAccountByAccountId(int consumerId, int accountId)
		throws DataSourceException
	{
		//TODO these account don't have address information!
		Set accounts = new HashSet(0);
		ConsumerAccount account = null;
		ConsumerAccountDAO accountDao = new ConsumerAccountDAO();
		ConsumerProfileDAO profileDao = new ConsumerProfileDAO();
		try
		{
			AccountSearchRequest searchCriteria = new AccountSearchRequest();
			searchCriteria.setCustId(Integer.valueOf(consumerId));

			accounts =
				accountDao.getAccount(searchCriteria, accountId, (String) null);
			// Should only return one account.
			Iterator iter = accounts.iterator();
			account = (ConsumerAccount) iter.next();
			//TODO currently the stored proc for getting accounts doesn't return the address
			//	so for now we handle separately.
			ConsumerAddress address =
				profileDao.getConsumerAddress(
					consumerId,
					account.getAddressId());
			if (address != null)
			{
				account.setAddressLine1(address.getAddressLine1());
				account.setAddressLine2(address.getAddressLine2());
				account.setCity(address.getCity());
				account.setIsoCountryCode(address.getIsoCountryCode());
				account.setPostalCode(address.getPostalCode());
				account.setState(address.getState());
				account.setZip4(address.getZip4());
				account.setAccountNumberTaintCode(
					TaintIndicatorType.getInstance(
						account.getAccountNumberTaintCode().toString()));
				account.setBankAbaTaintCode(
					TaintIndicatorType.getInstance(
						account.getBankAbaTaintCode().toString()));
				account.setCreditCardBinTaintCode(
					TaintIndicatorType.getInstance(
						account.getCreditCardBinTaintCode().toString()));
			}
			accountDao.commit();
			profileDao.commit();
		} catch (DataSourceException e)
		{
			accountDao.rollback();
			profileDao.rollback();
			throw e;
		} catch (Exception e)
		{
			accountDao.rollback();
			profileDao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			accountDao.close();
			profileDao.close();
		}
		return account;
	}

	public Map getAccountTypeDescriptions() throws DataSourceException
	{
		synchronized (accountTypeDescriptions)
		{
			if (accountTypeDescriptions.isEmpty())
			{
				ConsumerAccountDAO dao = new ConsumerAccountDAO();
				try
				{
					accountTypeDescriptions = dao.getAccountTypeDescriptions();
					dao.commit();
				} catch (DataSourceException e)
				{
					dao.rollback();
					throw e;
				} catch (Exception e)
				{
					dao.rollback();
					throw new EMGRuntimeException(e);
				} finally
				{
					dao.close();
				}
			}
		}
		return Collections.unmodifiableMap(accountTypeDescriptions);
	}

	public String getAccountTypeDescription(ConsumerAccountType accountType)
		throws DataSourceException
	{
		String description = null;
		if (accountType != null)
		{
			description =
				(String) getAccountTypeDescriptions().get(
					accountType.getCode());
		}
		return description;
	}

	public ConsumerCreditCardAccount updateConsumerCreditCardAccount(ConsumerCreditCardAccount account)
		throws DataSourceException
	{
		ConsumerCreditCardAccount updatedAccount = null;
		ConsumerAccountDAO dao = new ConsumerAccountDAO();
		try
		{
			updatedAccount = dao.updateCreditCardAccount(account);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Exception e)
		{
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			dao.close();
		}
		return updatedAccount;
	}

	public void updateCreditCardAccountType(
		String userId,
		int acctId,
		String acctType,
		String typeCode)
		throws DataSourceException
	{
		ConsumerAccountDAO dao = new ConsumerAccountDAO();
		try
		{
			dao.updateCreditCardAccountType(userId, acctId, acctType, typeCode);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Exception e)
		{
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			dao.close();
		}
	}

	public ConsumerBankAccount updateConsumerBankAccount(ConsumerBankAccount account)
		throws DataSourceException
	{
		ConsumerBankAccount updatedAccount = null;
		ConsumerAccountDAO dao = new ConsumerAccountDAO();
		try
		{
			updatedAccount = dao.updateBankAccount(account);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Exception e)
		{
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			dao.close();
		}
		return updatedAccount;
	}

	public ConsumerAccount deleteAccount(ConsumerAccount account)
		throws DataSourceException
	{
		Validate.notNull(account, "account must not be null");

		ConsumerAccount updatedAccount = null;
		ConsumerAccountDAO dao = new ConsumerAccountDAO();

		account.setStatus(AccountStatus.DELETED_DELETED);

		try
		{
			if (account.isBankAccount())
			{
				updatedAccount =
					dao.updateBankAccount((ConsumerBankAccount) account);
			} else if (account.isCardAccount())
			{
				updatedAccount =
					dao.updateCreditCardAccount(
						(ConsumerCreditCardAccount) account);
			} else
			{
				throw new IllegalArgumentException("account must be either a bank account or a credit card account");
			}
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Exception e)
		{
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			dao.close();
		}
		return updatedAccount;
	}

	public Map getAccountStatusDescriptions() throws DataSourceException
	{
		synchronized (accountStatusDescriptions)
		{
			if (accountStatusDescriptions.isEmpty())
			{
				ConsumerAccountDAO dao = new ConsumerAccountDAO();
				try
				{
					accountStatusDescriptions =
						dao.getAccountStatusDescriptions();
					dao.commit();
				} catch (DataSourceException e)
				{
					dao.rollback();
					throw e;
				} catch (Exception e)
				{
					dao.rollback();
					throw new EMGRuntimeException(e);
				} finally
				{
					dao.close();
				}
			}
		}
		return Collections.unmodifiableMap(accountStatusDescriptions);
	}

	public Map getAccountTypeDescriptionsBank() throws DataSourceException
	{
		Map bankDescriptions = new HashMap(getAccountTypeDescriptions());
		for (Iterator iter = bankDescriptions.keySet().iterator();
			iter.hasNext();
			)
		{
			String typeCode = (String) iter.next();
			ConsumerAccountType type =
				ConsumerAccountType.getInstance(typeCode);
			if (type.isBankAccount() == false)
			{
				iter.remove();
			}
		}
		return bankDescriptions;
	}

	public Map getAccountTypeDescriptionsCreditCard()
		throws DataSourceException
	{
		Map creditCardDescriptions = new HashMap(getAccountTypeDescriptions());
		for (Iterator iter = creditCardDescriptions.keySet().iterator();
			iter.hasNext();
			)
		{
			String typeCode = (String) iter.next();
			ConsumerAccountType type =
				ConsumerAccountType.getInstance(typeCode);
			if (type.isCardAccount() == false)
			{
				iter.remove();
			}
		}
		return creditCardDescriptions;
	}

	public UpdateAccountServiceResponse updateStatus(int accountId, ConsumerAccountType type,
			AccountStatus newStatus, String callerUserId)
			throws DataSourceException {
		Validate.notNull(type, "account type must not be null");
		Validate.notNull(newStatus, "newStatus must not be null");
		DefaultHttpClient httpClient = null;
		UpdateAccountServiceRequest request = new UpdateAccountServiceRequest();
		UpdateAccountServiceResponse response = new UpdateAccountServiceResponse();
		try {
			httpClient = new DefaultHttpClient();
			Gson gson = new GsonBuilder().create();
			request.setAccountId(String.valueOf(accountId));
			request.setAccountStatus(newStatus.getStatusCode());
			request.setAccountSubStatus(newStatus.getSubStatusCode());
			HttpPost postRequest = new HttpPost(EMTSharedContainerProperties
					.getAccountServiceURL().concat("/account/update"));
			String uacRequest = gson.toJson(request);
			LOGGER.info("Account Service URL : "+EMTSharedContainerProperties
					.getAccountServiceURL().concat("/account/update"));
			LOGGER.info("Account Service : Update Status JSON Request :: " + uacRequest);
			StringEntity input = new StringEntity(uacRequest);
			input.setContentType("application/json");
			postRequest.setEntity(input);
			HttpResponse httpResponse = httpClient.execute(postRequest);
			String responseJSONStr = EntityUtils.toString(httpResponse
					.getEntity());
			response = gson.fromJson(responseJSONStr,
					UpdateAccountServiceResponse.class);
		} catch (MalformedURLException e) {
			LOGGER.error("MalformedURLException in updateStatus for accountId {}"+request.getAccountId(),e);
		} catch (Exception e) {
			LOGGER.error("Exception occoured in updateStatus for accountId {}"+request.getAccountId(),e);
		} finally {
			httpClient.getConnectionManager().shutdown();
		}
		return response;
	}

	/*private ConsumerBankAccount updateStatusBankAccount(
		int accountId,
		AccountStatus newStatus,
		String callerUserId)
		throws DataSourceException
	{
		Validate.notNull(newStatus, "newStatus must not be null");
		ConsumerBankAccount oldAccount =
			getBankAccount(accountId, callerUserId);
		oldAccount.setStatus(newStatus);
		return (updateConsumerBankAccount(oldAccount));
	}

	private ConsumerCreditCardAccount updateStatusCreditCardAccount(
		int accountId,
		AccountStatus newStatus,
		String callerUserId)
		throws DataSourceException
	{
		Validate.notNull(newStatus, "newStatus must not be null");
		ConsumerCreditCardAccount oldAccount =
			getCreditCardAccount(accountId, callerUserId);
		oldAccount.setStatus(newStatus);
		return (updateConsumerCreditCardAccount(oldAccount));
	}*/

	public Map getAccountCommentReasons() throws DataSourceException
	{
		synchronized (accountCommentReasons)
		{
			if (accountCommentReasons.isEmpty())
			{
				ConsumerAccountDAO dao = new ConsumerAccountDAO();
				try
				{
					accountCommentReasons =
						Collections.unmodifiableMap(
							dao.getAccountCommentReasons());
					dao.commit();
				} catch (DataSourceException e)
				{
					dao.rollback();
					throw e;
				} catch (Exception e)
				{
					dao.rollback();
					throw new EMGRuntimeException(e);
				} finally
				{
					dao.close();
				}
			}
		}
		return accountCommentReasons;
	}

	public List getAccountCmnts(int accountId, String callerLoginId, String commentReasonCode)
		throws DataSourceException
	{
		List comments = null;
		ConsumerAccountDAO dao = new ConsumerAccountDAO();
		try
		{
			comments = dao.getCmnts(accountId, callerLoginId, commentReasonCode);
			dao.commit();
			Collections.sort(comments);
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Exception e)
		{
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			dao.close();
		}
		return (comments == null ? new ArrayList(0) : comments);
	}

	public int addAccountComment(AccountComment comment, String callerLoginId)
		throws DataSourceException
	{
		int newId;
		ConsumerAccountDAO dao = new ConsumerAccountDAO();
		try
		{
			newId = dao.insertComment(comment, callerLoginId);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Exception e)
		{
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			dao.close();
		}
		return newId;
	}

	public Collection getFilteredCCAccounts(
		String callerLoginId,
		String hashedText,
		String ccMask)
		throws DataSourceException
	{

		Collection ccAccounts = null;
		ConsumerAccountDAO dao = new ConsumerAccountDAO();
		try
		{
			ccAccounts =
				dao.getFilteredCCAccounts(callerLoginId, hashedText, ccMask);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Exception e)
		{
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			dao.close();
		}
		return (ccAccounts);
	}

	public Collection getFilteredBankAccounts(
		String callerLoginId,
		String abaNumber,
		String maskText)
		throws DataSourceException
	{

		Collection bankAccounts = null;
		ConsumerAccountDAO dao = new ConsumerAccountDAO();
		try
		{
			bankAccounts =
				dao.getFilteredBankAccounts(callerLoginId, abaNumber, maskText);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Exception e)
		{
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			dao.close();
		}
		return (bankAccounts);
	}

	public int getCreditCardCount(String userId, int id, String cardType)
		throws DataSourceException
	{
		ConsumerAccountDAO dao = new ConsumerAccountDAO();
		int creditCardCount = 0;
		try
		{
			creditCardCount = dao.getCreditCardCount(userId, id, cardType);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Exception e)
		{
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			dao.close();
		}
		return creditCardCount;
	}
}
