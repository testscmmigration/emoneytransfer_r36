package emgshared.services;

import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.SimpleCache;

public interface CacheService
{
	public SimpleCache getCache(String userID, String cacheName)
		throws DataSourceException, TooManyResultException;

	public void updateCache(String cacheName) throws DataSourceException;

}
