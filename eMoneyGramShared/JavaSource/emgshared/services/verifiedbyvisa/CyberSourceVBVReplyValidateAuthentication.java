package emgshared.services.verifiedbyvisa;

/**
 * 
 * @author G.R.Svenddal
 * 
 * This should be abstract enough to implement any service's vbvCheckEnroll service reply. But
 * only being familiar with CyberSource at the moment, it's probably going to be more specific
 * to them then it should be. If and when another service vendor is added, refactoring may (
 * probably will ) be needed.
 * 
 *  
 */

public class CyberSourceVBVReplyValidateAuthentication extends
        VBVReplyValidateAuthentication {

   // These booleans are business decisions and must be mutually exclusive.
    public boolean bdSuccess() {
      eval();
      return (decisionState == 1);
   }

    public boolean bdFailed() {
      eval();
      return (decisionState == 2);
   }

    public boolean bdAttemptsProtected() {
      eval();
      return (decisionState == 3);
   }

    public boolean bdNotProtected() {
      eval();
      return (decisionState == 4);
   }

    public boolean bdError() {
      eval();
      return (decisionState == 5);
   }

   private int decisionState = -1;

    private void eval() {
      if (decisionState != -1)
         return; // eval has run. No need to do it again.
      decisionState = 5; // start with "set error state"
        if (validateCoreReplyFields(this) == false) {
         VBVLogger.error("core ReplyValidateAuthentication field missing: " + getRequestID());
         return;// ERROR
      }
      if (getAuthenticationResult() == null)   return; // ERROR.
      if (getDecision().equals("ERROR"))       return; // ERROR
      if (getDecision().equals("ACCEPT")
            && getReasonCode().equals("100")
            && getCommerceIndicator() != null)
      {
         if (getCommerceIndicator().equals("vbv_attempted")
               && getEciRaw().equals("06"))
         {
            decisionState = 3; // ATTEMPTS PROTECTED
            return;
         }
         if (getAuthenticationResult().equals("0")
               && getEciRaw().equals("05"))
         {
            decisionState = 1; // SUCCESS
            return;
         }
      }
      if (getDecision().equals("REJECT") 
            && getReasonCode().equals("476")
            && getAuthenticationResult().equals("9"))
      {
         decisionState=2; // FAILED
         return;
      }
      return;
   }
}