package emgshared.services.verifiedbyvisa;

import org.apache.log4j.Logger;

import emgshared.dataaccessors.EMGSharedLogger;



/**
 * @author G.R.Svenddal
 * Created on Aug 2, 2005
 * VBVLogger.java
 * 
 * Special Logger for both sides. 
 * Assists in parsing log files for VBV analysis & discovery.
 * 
 *  */
public class VBVLogger
{
   private static final String LOG_KEY = "VBVLOG"; //$NON-NLS-1$

   private static final Logger log = EMGSharedLogger.getLogger(LOG_KEY);
 
   // helps to keep code less verbose
   public static void info(String message){
      log.info(message);
   }
   // me too.
   public static void error(String message){
      log.error(message);
   }
   // me three.
   public static void debug(String message){
      log.debug(message);
   }

   
   public static Logger getLogger()
   {
      return log;
   }
}
