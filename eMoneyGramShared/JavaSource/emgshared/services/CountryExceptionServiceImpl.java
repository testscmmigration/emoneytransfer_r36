package emgshared.services;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.moneygram.agentconnect1305.wsclient.CountryInfo;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

import shared.mgo.services.AgentConnectService;
import shared.mgo.services.MGOServiceFactory;



import emgshared.dataaccessors.CacheDAO;
import emgshared.dataaccessors.CountryExceptionDAO;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.CountryExceptionBean;
import emgshared.model.CountryMasterBean;
import emgshared.model.SimpleCache;
import emgshared.property.EMTSharedDynProperties;

class CountryExceptionServiceImpl implements CountryExceptionService {

	private static final String US_ONLY = "us_only";
	private static final String FIXED_RATE = "fixed_rate";
	private static final String ALL = "all";
//	private static final String COUNTRY_MASTER_STATUS_ACTIVE = "ACT";
//	private static final String COUNTRY_MASTER_STATUS_INACTIVE = "NAT";
	private static final CountryExceptionServiceImpl instance = new CountryExceptionServiceImpl();
	private static Logger logr =
		LogFactory.getInstance().getLogger(CountryExceptionServiceImpl.class);
	//release 6.4.3
	AgentConnectService agentConnectService = MGOServiceFactory.getInstance().getAgentConnectService();
	private CacheService cs = ServiceFactory.getInstance().getCacheService();

	private Collection allowedCountryList = null;
	private Collection allowedCaliforniaSameDayCountryList = null;
	private Collection allowedCaliforniaEconomyCountryList = null;
	private HashMap countryMasterList = null;
	private HashMap cntryExOverrideList = new HashMap();
	private long countryMasterLastRefreshed = 0;
	private Timestamp cntryExOverrideTimestamp = null;
	private final long refreshInterval = 30 * 1000 * 60;

	private CountryExceptionServiceImpl() {
		if (allowedCountryList == null) {
			try {
				allowedCountryList = cacheAllowedCountries();
				EMTSharedDynProperties dynProps = new EMTSharedDynProperties();
				// Build Same Day Country List
				allowedCaliforniaSameDayCountryList =
					BuildSpecialCountryList(dynProps.getSpecialRestriction_SameDayService().toLowerCase());
				// Build Economy Country List
				allowedCaliforniaEconomyCountryList =
					BuildSpecialCountryList(dynProps.getSpecialRestriction_EconomyService().toLowerCase());
			} catch (Exception e) {
				logr.error(e.getMessage(),e);
				throw new EMGRuntimeException(e);
			}
			Timer timer = new Timer(true);
			timer.schedule(new CacheAllowedCountryList(), refreshInterval , refreshInterval );
		}
	}

	public static final CountryExceptionServiceImpl getInstance() {
		return instance;
	}

	public void setCntryException(CountryExceptionBean excp) throws DataSourceException, ParseException {
		CountryExceptionDAO dao = new CountryExceptionDAO();
		CacheDAO cacheDao = new CacheDAO();
		try {
			dao.setCntryException(excp);
			cacheDao.updateCache(SimpleCache.COUNTRY_EXCEPTION_OVERRIDE);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} finally {
			dao.close();
		}
		return;
	}

	public void deleteCntryException(String rcvCntry, String sndCntry) throws DataSourceException {
		CountryExceptionDAO dao = new CountryExceptionDAO();
		CacheDAO cacheDao = new CacheDAO();
		try {
			dao.deleteCntryException(rcvCntry, sndCntry);
			cacheDao.updateCache(SimpleCache.COUNTRY_EXCEPTION_OVERRIDE);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} finally {
			dao.close();
		}
		return;
	}

	/**
	 * Send Country is dafulted to USA.
	 * @param receiveCountry
	 * @return
	 * @throws DataSourceException
	 */
	public CountryExceptionBean getCountryException(String receiveCountry) throws DataSourceException {

		return getCntryException(receiveCountry, "USA");
	}

	public CountryExceptionBean getCntryException(String receiveCountry, String sendCountry)
		throws DataSourceException {

		CountryExceptionBean selectedCountryExceptionBean;
		CountryExceptionDAO dao = new CountryExceptionDAO();
		try {
			selectedCountryExceptionBean = dao.getCntryException(receiveCountry, sendCountry);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} finally {
			dao.close();
		}
		return selectedCountryExceptionBean;
	}

	public synchronized List getCntryExceptions(String country) throws DataSourceException {
		List list;
		CountryExceptionDAO dao = new CountryExceptionDAO();
		HashMap cml = new HashMap();
		CountryMasterBean cmb = null;
		try {
			cml = this.getCountryMasterList();
			list = dao.getCntryExceptions(country);
			for (Iterator iter = list.iterator(); iter.hasNext();) {
				CountryExceptionBean ceb = (CountryExceptionBean) iter.next();
				try {
					cmb =(CountryMasterBean)cml.get(ceb.getRcvIsoCntryCode());
					ceb.setRcvCountryName(cmb.getCountryName());
					cmb =(CountryMasterBean)cml.get(ceb.getIsoCntryCode());
					ceb.setSndCountryName(cmb.getCountryName());
				} catch (Exception ignore) {
					ceb.setRcvCountryName(" ");
					ceb.setSndCountryName(" ");
				}
			}
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} catch (Exception ex) {
			dao.rollback();
			throw new EMGRuntimeException(ex);
		} finally {
			dao.close();
		}
		return list;
	}

	public Collection getAllowedCountries() {
		return allowedCountryList;
	}

	public Collection getAllowedSameDayCaliforniaCountries() {
		return allowedCaliforniaSameDayCountryList;
	}

	public Collection getAllowedEconomyCaliforniaCountries() {
		return allowedCaliforniaEconomyCountryList;
	}

	/**
	 * Gets Active Countries off the Master Country list and caches the results
	 * @throws Exception
	 */
	private void cacheCountryMaster() throws Exception {
		CountryExceptionDAO dao = new CountryExceptionDAO();
		try {
			//private Collection countryMasterList = null;
			// pass empty string and get Active and Inactive Countries - the complete list
			HashMap cml = dao.getCntryMasterList("");
			dao.commit();
			this.countryMasterList = cml;
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} catch (Exception ex) {
			dao.rollback();
			throw new EMGRuntimeException(ex);
		} finally {
			dao.close();
		}
		return;
	}

	private Collection cacheAllowedCountries() throws Exception {
		return agentConnectService.getActiveCountryList();
	}

	private Collection cacheAllowedCaliforniaCountries(boolean fixedRateOn) throws Exception {
		return agentConnectService.getFilteredCountryList("CA");
/*
		AgentConnectAccessor ac = new AgentConnectAccessor();
		Collection acListMod = new ArrayList(allowedCountryList);
		CountryCurrencyInfo[] cci = ac.getCountryCurrencyInfo();
		try {
			Iterator it = allowedCountryList.iterator();
			while (it.hasNext()) {
				CountryInfo ci = (CountryInfo) it.next();
				for (int i = 0; i < cci.length; i++) {
					if (ci.getCountryCode().equals(cci[i].getCountryCode())) {
						if (!cci[i].getPayoutCurrency().equals(ExchangeRateType.USD.getTypeCode())) {
							if (fixedRateOn) {
								if (cci[i].isIndicativeRateAvailable()) {
									acListMod.remove(AgentConnectAccessor.getCountry(ci.getCountryCode()));
								}
							} else {
								acListMod.remove(AgentConnectAccessor.getCountry(ci.getCountryCode()));
							}
						}
						break;
					}
				}
			}
		} catch (Exception e) {
			throw e;
		}
		return acListMod;
*/
	}
	protected class CacheAllowedCountryList extends TimerTask {
		public void run() {
			try {
				allowedCountryList = cacheAllowedCountries();
				EMTSharedDynProperties dynProps = new EMTSharedDynProperties();
				// Build Same Day Country List
				allowedCaliforniaSameDayCountryList =
					BuildSpecialCountryList(dynProps.getSpecialRestriction_SameDayService().toLowerCase());
				// Build Economy Country List
				allowedCaliforniaEconomyCountryList =
					BuildSpecialCountryList(dynProps.getSpecialRestriction_EconomyService().toLowerCase());
			} catch (Exception e) {
				throw new EMGRuntimeException(e);
			}
		}
	}

	private Collection BuildSpecialCountryList(String restriction) {
		/* If the resident lives in CA then they can only send to certain countries.
		 * These counties are defined in the Dynamic Properties which are stored in the db.
		 * The properties are as follows:
		 * 		usd_only = Only USA returned.
		 * 		usd_payout = Only countries who payout in USD will be returned.
		 * 		fixed_rate = Only countries who payout in USD and have indicative rate set to false will be returned.
		 *		all = All counties will be returned from the Country Exception list.
		 * Note:  These parameters are incrementally inclusive so each will include the previous.
		 */
		Collection c = new ArrayList();
		if (restriction.equals(US_ONLY)) {
			CountryInfo countryInfo = new CountryInfo();
			countryInfo.setCountryCode("USA");
			countryInfo.setCountryName("UNITED STATES");
			c.add(countryInfo);
		} else if (restriction.equals(ALL)) {
			c.addAll(allowedCountryList);
		} else {
			try {
				boolean fixedRateOn = false;
				if (restriction.equals(FIXED_RATE)) {
					fixedRateOn = true;
				}
				c = cacheAllowedCaliforniaCountries(fixedRateOn);
			} catch (Exception e) {
				throw new EMGRuntimeException(e);
			}
		}
		return c;
	}

	/* (non-Javadoc)
	 * @see emgshared.services.CountryExceptionService#getCountryMaster(java.lang.String)
	 * Pass in an ISO Country Code (3 chars) and if found on the Country Master list, return
	 * a CountryMasterBean object.  If the cache is old, not equal to today, then refresh it
	 */
	public CountryMasterBean getCountryMaster(String isoCountryCode) throws DataSourceException {
		try {
			checkCountryMasterRefresh();
			if (this.countryMasterList.containsKey(isoCountryCode.toUpperCase()))
				return (CountryMasterBean) this.countryMasterList.get(isoCountryCode.toUpperCase());
			else
				return null;
		} catch (Exception e) {
			throw new DataSourceException("Error Getting CountryMaster:", e);
		}
	}

	/*
	 * @see emgshared.services.CountryExceptionService#getCountryMasterList()
	 * Gets a HashMap of CountryMasterBean objects, the CountryMaster list
	 */
	public synchronized HashMap getCountryMasterList() throws DataSourceException {
		try {
			checkCountryMasterRefresh();
			return this.countryMasterList;
		} catch (Exception e) {
			throw new DataSourceException(e);
		}
	}

	/* (non-Javadoc)
	 * @see emgshared.services.CountryExceptionService#getCountryMasterListByStatus(java.lang.String)
	 * Gets a CountryMaster list - hashmap of CountryMasterBean objects by status "ACT", "NAT" or if "" then all
	 */
	public HashMap getCountryMasterListByStatus(String Status) throws DataSourceException {
		CountryExceptionDAO dao = new CountryExceptionDAO();
		HashMap cml = null;
		try {
			cml = dao.getCntryMasterList(Status);
			dao.commit();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} catch (Exception ex) {
			dao.rollback();
			throw new EMGRuntimeException(ex);
		} finally {
			dao.close();
		}
		return cml;
	}

	/**
	 * @throws DataSourceException
	 * Checks to see if the cache for the Country Master is old, if so then re-cache it.
	 */
	private void checkCountryMasterRefresh() throws DataSourceException {
		if ((this.countryMasterList == null) || ((System.currentTimeMillis() - this.countryMasterLastRefreshed) > this.refreshInterval ) ) {
			this.countryMasterLastRefreshed = System.currentTimeMillis();
			try {
				this.cacheCountryMaster();
			} catch (Exception e) {
				throw new DataSourceException("Error Getting CountryMaster", e);
			}
		}
		return;
	}


	public synchronized boolean isCountryExceptionOverride(String country) throws DataSourceException {
		validateCntryExOverrideCache();
		return this.cntryExOverrideList == null?false:this.cntryExOverrideList.containsKey(country);
	}

	/**
	 * Checks to see if the cache for the Override Country Exception is old, if so then refresh it.
	 */
	private void validateCntryExOverrideCache() {
        EMGSharedLogger.getLogger(this.getClass().getName().toString())
        	.debug("CountryExceptionOverride Cache: validating cache...");
        try {
        	SimpleCache cache = cs.getCache(null, SimpleCache.COUNTRY_EXCEPTION_OVERRIDE);
        	if (null == cache || null == cache.getName()) {
        		EMGSharedLogger.getLogger(this.getClass().getName().toString())
            		.info("No cache record for : " + SimpleCache.COUNTRY_EXCEPTION_OVERRIDE + ". So add one.");
        		cs.updateCache(SimpleCache.COUNTRY_EXCEPTION_OVERRIDE);
        		cache = cs.getCache(null, SimpleCache.COUNTRY_EXCEPTION_OVERRIDE);
        	}

        	EMGSharedLogger.getLogger(this.getClass().getName().toString())
            	.info("CountryExceptionOverride Cache Time : " + this.cntryExOverrideTimestamp);
        	EMGSharedLogger.getLogger(this.getClass().getName().toString())
            	.info("CountryExceptionOverride Database Time : " + cache.getTimeStamp());

        	if (this.cntryExOverrideTimestamp == null) {
        		EMGSharedLogger.getLogger(this.getClass().getName().toString())
                	.info("CountryExceptionOverride Cache : TimeStamp Null");
        		HashMap cntryExOverride = loadCntryExOverride();
        		if (cntryExOverride != null){
        			this.cntryExOverrideTimestamp = cache.getTimeStamp();
        			this.cntryExOverrideList = cntryExOverride;
        		}
        	} else {
        		if (!cache.getTimeStamp().equals(cntryExOverrideTimestamp)) {
        			EMGSharedLogger.getLogger(this.getClass().getName().toString())
        				.info("CountryExceptionOverride Cache : Cache obsolete");
            		HashMap cntryExOverride = loadCntryExOverride();
            		if (cntryExOverride != null){
            			this.cntryExOverrideTimestamp = cache.getTimeStamp();
            			this.cntryExOverrideList = cntryExOverride;
            		}
        		} else {
        			EMGSharedLogger.getLogger(this.getClass().getName().toString())
        				.info("CountryExceptionOverride Cache : Cache up-to-date");
        		}
        	}
        } catch (Exception e) {
        	EMGSharedLogger.getLogger(this.getClass().getName().toString())
        		.error("CountryExceptionService ERROR : validateCntryExOverrideCache() - " + e.getMessage());
        }
	}

	// get destinations countries that require manual review of transactions
	// regardless of scoring and/or premier status
    private HashMap loadCntryExOverride() {
        EMGSharedLogger.getLogger(this.getClass().getName().toString()).info(
                "Loading CountryExceptionOverride Cache...");

        HashMap cntryExOverride = null;
        try {
        	EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("Getting all country exceptions....");
        	HashMap ovrdCntryExcepts = new HashMap();
        	List countryExceptions = getCntryExceptions("USA");	// TODO review this hard-coded country and validate whether a country parameter is needed
        	EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("Got them!");
        	if (null != countryExceptions){
        		for (Iterator it=countryExceptions.iterator(); it.hasNext(); ){
        			CountryExceptionBean ceb = (CountryExceptionBean)it.next();
        			if (ceb.isManualReviewAllowed()){
        				ovrdCntryExcepts.put(ceb.getRcvIsoCntryCode(), "");
        			}
        		}
        	}
        	cntryExOverride = ovrdCntryExcepts;
            EMGSharedLogger.getLogger(this.getClass().getName().toString()).info(
                	"CountryExceptionOverride Cache successfully loaded!");
         } catch (Exception e) {
            EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(
                    "ERROR: Failed to loade CountryExceptionOverride Cache");
        }

        return cntryExOverride;
    }
    
    /*
     * Added for MBO-7556
	 * @see emgshared.services.CountryExceptionService#getCountryMasterList()
	 * Gets a HashMap of CountryMasterBean objects, the CountryMaster list
	 */
	public List getSendCountryCodes() throws DataSourceException {
		CountryExceptionDAO dao = new CountryExceptionDAO();
		List countryCodes = new ArrayList();
		try {
			countryCodes = dao.getSendCountryCodes();
		} catch (DataSourceException e) {
			dao.rollback();
			throw e;
		} finally {
			dao.close();
		}
		return countryCodes;
	}
}