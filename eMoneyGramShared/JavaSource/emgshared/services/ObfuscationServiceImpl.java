/*
 * Created on Jan 14, 2005
 *
 */
package emgshared.services;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.CertificateFactory;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;
import org.owasp.esapi.ESAPI;

import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.property.EMTSharedContainerProperties;
/**
 * @author T349
 *
 */
class ObfuscationServiceImpl implements ObfuscationService
{
	private static final ObfuscationService instance =
		new ObfuscationServiceImpl();

	private ObfuscationServiceImpl() {
	}

	public static final ObfuscationService getInstance() {
		return instance;
	}

	public String getCreditCardObfuscation(String creditCard) {
		return encryptRSA(creditCard,true);
	}

	public String getCreditCardHash(String creditCard) {
		return encryptSHA(creditCard + EMTSharedContainerProperties.getSecureHashSeed());
	}

	public String getBankAccountObfuscation(String bankAccount) {
		return encryptRSA(bankAccount,true);
	}

	public String getBankAccountHash(String bankAccount) {
		return encryptSHA(
			bankAccount + EMTSharedContainerProperties.getSecureHashSeed());
	}

	public String getBinObfuscation(String bin) {
		return encryptRSA(bin,true);
	}

	public String getBinHash(String bin) {
		return encryptSHA(bin + EMTSharedContainerProperties.getSecureHashSeed());
	}

	public String getPasswordObfuscation(String password) {
		return encryptSHA(password);
	}

	public String getBillerAccountObfuscation(String account) {
		return encryptRSA(account,true);
	}

	public String getAdditionalIdObfuscation(String additionalId) {
		return encryptRSA(additionalId,false);
	}

	private String encryptRSA(String textToEncrypt, boolean padding) {
		final String BouncyCastle="BC";

		String cypherOptions = "RSA/ECB/PKCS1Padding";

		if(!padding) {
			cypherOptions = "RSA/None/NoPadding";
		}

		try {
			// Add provider
			Security.addProvider(new BouncyCastleProvider());
		} catch (Exception e){
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(e.toString());
			throw new EMGRuntimeException(e);
		}

		PublicKey rsaPublicKey = null;

		try {

			FileInputStream fisCert = new FileInputStream(EMTSharedContainerProperties.getPublicRSAKey());
			java.security.cert.CertificateFactory cf = CertificateFactory.getInstance("X.509");
			java.security.cert.Certificate cert = cf.generateCertificate(fisCert);
			fisCert.close();
			rsaPublicKey = cert.getPublicKey();
		} catch (FileNotFoundException e) {
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(e.toString());
			throw new EMGRuntimeException(e);
		} catch (Exception e) {
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(e.toString());
			throw new EMGRuntimeException(e);
		}

		Cipher cp = null;

		try {
			// Generate RSA cipher
			cp = Cipher.getInstance(cypherOptions, BouncyCastle);

		} catch (NoSuchAlgorithmException nsae) {
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(nsae.toString());
			throw new EMGRuntimeException(nsae);
		} catch (NoSuchProviderException nspe) {
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(nspe.toString());
			throw new EMGRuntimeException(nspe);
		} catch (NoSuchPaddingException nspe) {
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(nspe.toString());
			throw new EMGRuntimeException(nspe);
		}

		try {

			cp.init(Cipher.ENCRYPT_MODE, rsaPublicKey);
			byte[] encryptedText = cp.doFinal(textToEncrypt.getBytes());

			return new String(Base64.encode(encryptedText));
		} catch (Exception e) {
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(e.toString());
			throw new EMGRuntimeException(e);
		}
	}

	private String encryptSHA(String text) {
		final String SHAAlgorithm = "SHA-1";
		try {
			MessageDigest sha = MessageDigest.getInstance(SHAAlgorithm);
			return hex(sha.digest(text.getBytes("UTF-8")));
		} catch (NoSuchAlgorithmException nsae) {
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).error(
				nsae.toString());
			return null;
		} catch (Exception e) {
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).error(
				e.toString());
			return null;
		}
	}
	public String hex(byte[] array) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < array.length; ++i) {
			sb.append(
				Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
		}
		return sb.toString();
	}
}