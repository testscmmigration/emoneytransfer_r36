/*
 * Created on Mar 9, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgshared.services;

import java.math.BigDecimal;

import emgshared.dataaccessors.AccountsDAO;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;

/**
 * @author T348
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AccountingServiceImpl implements AccountingService{

	private static final AccountingServiceImpl instance =
			new AccountingServiceImpl();

	private AccountingServiceImpl()
	{
	}

	public static final AccountingServiceImpl getInstance()
	{
		return instance;
	}

	public BigDecimal getLast30DayTotal(String userID, int custID) 
			throws DataSourceException
	{
		AccountsDAO dao=new AccountsDAO();
		BigDecimal bd=new BigDecimal(0);
		try
		{
			BigDecimal bd1=dao.getLast30DayTotal(userID, custID);
			//Database returns null, If profile doesn't have any past transactions
			if(bd1!=null) return bd1;
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Exception e)
		{
			dao.rollback();
			throw new EMGRuntimeException(e);
		} finally
		{
			dao.close();
		}
		return bd;
	}

}
