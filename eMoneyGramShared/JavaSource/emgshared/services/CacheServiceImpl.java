package emgshared.services;

import java.sql.SQLException;

import emgshared.dataaccessors.CacheDAO;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.SimpleCache;

class CacheServiceImpl implements CacheService
{
	private static final CacheServiceImpl instance = new CacheServiceImpl();

	private CacheServiceImpl()
	{
	}

	public static final CacheServiceImpl getInstance()
	{
		return instance;
	}

	public SimpleCache getCache(String userID, String cacheName)
		throws DataSourceException, TooManyResultException
	{

		SimpleCache sc = new SimpleCache();
		CacheDAO dao = new CacheDAO();
		try
		{
			sc = dao.getCache(userID, cacheName);
		} catch (DataSourceException e)
		{
			throw e;
		} catch (TooManyResultException e1)
		{
			throw e1;
		} catch (SQLException sqlex)
		{
			throw new EMGRuntimeException(sqlex);
		} finally
		{
			dao.close();
		}
		return sc;

	}

	public void updateCache(String cacheName) throws DataSourceException
	{

		CacheDAO dao = new CacheDAO();
		try
		{
			dao.updateCache(cacheName);
		} catch (DataSourceException e)
		{
			throw e;
		} finally
		{
			dao.close();
		}

	}

}