package emgshared.services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.dataaccessors.TransactionDAO;
import emgshared.exceptions.DataSourceException;


public class TransactionServiceImpl implements TransactionService
{

	private static final TransactionServiceImpl instance =
		new TransactionServiceImpl();

	private TransactionServiceImpl()
	{
	}

	public static final TransactionServiceImpl getInstance()
	{
		return instance;
	}

	public Map getHolidays(String userId) throws DataSourceException
	{
		TransactionDAO dao = new TransactionDAO();
		Map map = null;
		try
		{
			map = dao.getHolidays(userId);
			dao.commit();
		} catch (DataSourceException ex)
		{
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).warn(
				"Failed to auto insert credit card comment for transaction",
				ex);
			dao.rollbackAndIgnoreException();
		} finally
		{
			dao.close();
		}
		return map;
	}

	public Collection getTransactionTypes(int partnerSiteCode) throws DataSourceException
 {

		TransactionDAO dao = new TransactionDAO();
		Collection transactionTypes = new ArrayList();

		try {
			transactionTypes = dao.getTransactionTypes(partnerSiteCode);
			dao.commit();
		} catch (DataSourceException ex) {
			EMGSharedLogger
					.getLogger(this.getClass().getName().toString())
					.warn("Failed to auto insert credit card comment for transaction",
							ex);
			dao.rollbackAndIgnoreException();
		} catch (SQLException sqlex) {
			EMGSharedLogger
					.getLogger(this.getClass().getName().toString())
					.warn("Failed to auto insert credit card comment for transaction",
							sqlex);
			dao.rollbackAndIgnoreException();

		} finally {
			dao.close();
		}
		return transactionTypes;
	}

}
