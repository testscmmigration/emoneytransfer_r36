/*
 * Created on Mar 9, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgshared.dataaccessors;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import oracle.jdbc.OracleTypes;
import emgshared.exceptions.DataSourceException;

/**
 * @author T348
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AccountsDAO extends AbstractOracleDAO
{

	/**
	 	PROCEDURE prc_get_last_30_days_trans_cv
	   (iv_user_id                IN customer.cust_logon_id%TYPE,
		iv_call_type_code         IN process_log_a.call_type_code%TYPE,
		iv_cust_id                IN customer.cust_id%TYPE,          
		ov_prcs_log_id            OUT process_log_a.prcs_log_id%TYPE,
		ov_snd_tot_amt            OUT NUMBER
	   );
	*/
	public BigDecimal getLast30DayTotal(String userID, int custId)
		throws DataSourceException
	{

		CallableStatement cs = null;
		Connection conn = null;
		BigDecimal total;
		try
		{
			conn = getConnection();
			String storedProcName =
				"pkg_em_transactions.prc_get_last_30_days_trans_cv";

			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?) }");
			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, custId);
			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.NUMBER);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDebugEnabled())
			{
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).debug(
					storedProcName + " started: " + System.currentTimeMillis());
			}

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDebugEnabled())
			{
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).debug(
					storedProcName + " ended: " + System.currentTimeMillis());
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).debug(
					"dbLogId = " + dbLogId);
			}

			total = cs.getBigDecimal(paramIndex);
			logEndTime(dbLogId);

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}
		return total;
	}

}
