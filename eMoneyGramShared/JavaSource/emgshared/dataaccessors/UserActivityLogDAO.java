/*
 *  DAO that will get User Activity Event Types, insert User Activity logs,
 *  and query User Activity Logs.
 */
package emgshared.dataaccessors;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import oracle.jdbc.OracleTypes;
import java.sql.Connection;
import emgshared.exceptions.DataSourceException;
import emgshared.model.KeyColumns;
import emgshared.model.UserActivityLog;
import emgshared.model.UserActivityType;
/**
 * @author A119
 *  
 */
public class UserActivityLogDAO extends AbstractOracleDAO {


    public void insertUserActivityLog(String user, UserActivityLog ual)
            throws DataSourceException {
        CallableStatement cs = null;
        Connection conn = null;
        String storedProcName = "pkg_emg_type_stat_maint.prc_iu_usr_activity_log";
        StringBuilder sqlBuffer = new StringBuilder(64);
        sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?) }");
        try {
            conn = getConnection();
            cs = conn.prepareCall(sqlBuffer.toString());
            int paramIndex = 0;
            cs.setString(++paramIndex, user);
            cs.setString(++paramIndex, dbCallTypeCode);
            String theCode = ual.getUserActivityType().getUserActivityEventCode();
            cs.setString(++paramIndex, theCode);
            cs.setString(++paramIndex, ual.getUserId());
            cs.setString(++paramIndex, ual.getApplicationName());
            
            if ((ual.getIPAddress() == null) || (ual.getIPAddress().length() < 1))
                cs.setNull(++paramIndex, Types.VARCHAR);
            else
                cs.setString(++paramIndex, ual.getIPAddress());                
            cs.setDate(++paramIndex, new java.sql.Date(ual.getEventDate().getTime()));
            if ((ual.getDetailText() == null) || (ual.getDetailText().length() < 1))
                cs.setNull(++paramIndex, Types.VARCHAR);
            else
            {
                if (ual.getDetailText().length() > 2000)
                    cs.setString(++paramIndex, ual.getDetailText().substring(1,2000));
                else
                    cs.setString(++paramIndex, ual.getDetailText());
            }
            if (ual.getDataKeyValue() == null)
                cs.setNull(++paramIndex, Types.VARCHAR);
            else
                cs.setString(++paramIndex, ual.getDataKeyValue());
            cs.execute();
        } catch (SQLException e) {
            throw new DataSourceException(e);
        } finally {
            close(cs);
        }
        return;
    }

    public List getUserActivityTypes(String eventCode) throws DataSourceException, SQLException {
        ResultSet rs = null;
        ResultSet rs2 = null;
        CallableStatement cs = null;
        Connection conn = null;
        String storedProcName = "pkg_em_status_and_type.prc_get_usr_acty_event_type_cv";
        StringBuilder sqlBuffer = new StringBuilder(64);
        sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");
        List typeList = new ArrayList();
        try {
            conn = getConnection();
            cs = conn.prepareCall(sqlBuffer.toString());
            int paramIndex = 0;
            String userID = "EMGMGI";
            cs.setString(++paramIndex, userID);
            cs.setString(++paramIndex, dbCallTypeCode);
            if ((eventCode == null) || (eventCode.equals("")))
                cs.setNull(++paramIndex, Types.VARCHAR);
            else
                cs.setString(++paramIndex, eventCode);
            //  output parameters
            cs.registerOutParameter(++paramIndex, Types.INTEGER);
            cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
            int rsIndex = paramIndex;
            cs.execute();
            rs = (ResultSet) cs.getObject(rsIndex);
            String storedProcName2 = "pkg_em_status_and_type.prc_get_table_key_column_cv";
            while (rs.next()) {
                paramIndex = 0;
                String tableName = rs.getString("TBL_NAME");
                ArrayList keyColumnList = null;
                if ((tableName != null) && (tableName.length() > 1))
                {  // get the key column values if event has a table associated
	                StringBuilder sqlBuffer2 = new StringBuilder(64);
	                // call to get the key columns
	                sqlBuffer2.append("{ call " + storedProcName2 + "(?,?,?,?,?,?) }");
	                cs = conn.prepareCall(sqlBuffer2.toString());
	                cs.setString(++paramIndex, userID);
	                cs.setString(++paramIndex, dbCallTypeCode);
	                cs.setNull(++paramIndex, Types.VARCHAR);
	                cs.setString(++paramIndex, tableName);
	                //  output parameters
	                cs.registerOutParameter(++paramIndex, Types.INTEGER);
	                cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
	                cs.execute();
	                rs2 = (ResultSet) cs.getObject(paramIndex);
	                keyColumnList = new ArrayList();
	                int listIndex = 0;
	                while (rs2.next()) {
	                    KeyColumns kc = new KeyColumns();
	                    kc.setColumnName(rs2.getString("TBL_COL_NAME"));
	                    kc.setColumnSequenceNumber(rs2.getInt("COL_SEQ_NBR"));
	                    kc.setTableName(rs2.getString("TBL_NAME"));
	                    keyColumnList.add(listIndex, kc);
	                    listIndex++;
	                }
                }
                UserActivityType uat = new UserActivityType();
                uat.setTableName(tableName);
                uat.setUserActivityEventCode(rs.getString("USR_ACTY_EVNT_CODE"));
                uat.setUserActivityEventDesc(rs.getString("USR_ACTY_EVNT_DESC"));
                uat.setKeyColumns(keyColumnList);
                typeList.add(uat);
            }
        } catch (SQLException e) {
            throw new DataSourceException(e);
        } finally {
            close(rs);
            close(rs2);
            close(cs);
        }
        return typeList;
    }

}
