package emgshared.dataaccessors;

import java.lang.reflect.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;

import oracle.jdbc.OracleTypes;



import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.PropertyBean;
import emgshared.model.PropertyKey;
import emgshared.util.DateFormatter;

public class PropertyDAO extends AbstractOracleDAO {
	private static Logger logr = EMGSharedLogger.getLogger(
			PropertyDAO.class);

	/**
	 * @param propertyName
	 * @param userID
	 * @param castToType
	 *            - cast return object(s) to the table defined type.
	 * @return - if propertyName is null, returns a Map of Objects.
	 * @throws SQLException
	 * 
	 */
	public Object getPropertyObject(String propertyName, String userID,
			boolean castToType) {
		return CorePropertyObjectGetter(null, this, propertyName, userID,
				castToType);
	}

	/**
	 * A Getter for all uses. Works both from a PropertyDAO object ( by passing
	 * in the usually hidden <CODE>this</CODE> explicitly, or from bootstrap
	 * code.
	 * 
	 * @param conn
	 * @param dao
	 * @param propertyName
	 * @param userID
	 * @param castToType
	 * @return
	 */
	private static Object CorePropertyObjectGetter(Connection conn,
	// give me this
			PropertyDAO dao, // XOR give me this.
			String propertyName, String userID, boolean castToType) {
		Connection oConn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		Object returnThing = null;
		try {
			if (dao != null) {
				oConn = dao.getConnection();
			} else {
				oConn = conn;
			}

			String storedProcName = "pkg_em_dynamic_properties.prc_get_admin_property_cv ";
			cs = oConn.prepareCall("{ call " + storedProcName
					+ "(?,?,?,?,?,?,?,?) }");
			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, propertyName);
			cs.setString(++paramIndex, null);
			// admin_use_flag - filter. not used.
			cs.setString(++paramIndex, null);
			// multivalue_flag - filter. not used. TODO - implement seperate
			// getter for multivalue lines.
			cs.setString(++paramIndex, null);
			// data_type_code - filter. not used.

			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);

			if (logr.isDebugEnabled()) {
				logr.debug(storedProcName + " started: "
						+ System.currentTimeMillis());
			}
			/********************/
			cs.execute();
			/********************/
			if (logr.isDebugEnabled()) {
				logr.debug(storedProcName + " ended: "
						+ System.currentTimeMillis());
				long dbLogId = cs.getLong(dbLogIdIndex);
				logr.debug("dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(paramIndex);
			if (propertyName == null) { // we expect multiple rows
				Map rM = new HashMap();
				while (rs.next()) {
					String pName = rs.getString("PROP_NAME");
					String pVal = rs.getString("PROP_VAL");
					String pType = rs.getString("DATA_TYPE_CODE");
					String pMultiFlag = rs.getString("MULTIVALUE_FLAG");
					pMultiFlag = pMultiFlag == null ? "N" : pMultiFlag;
					if (castToType) {
						if ("Y".equalsIgnoreCase(pMultiFlag)) {
							rM.put(pName, addArray(rM.get(pName), pVal, pType));
						} else {
							rM.put(pName, castProperty(pVal, pType));
						}
					} else {
						rM.put(pName, pVal);
					}
				}
				returnThing = rM;
			} else {
				Object obj = null;
				while (rs.next()) // single named property
				{
					String pVal = rs.getString("PROP_VAL");
					String pType = rs.getString("DATA_TYPE_CODE");
					String pMultiFlag = rs.getString("MULTIVALUE_FLAG");
					pMultiFlag = pMultiFlag == null ? "N" : pMultiFlag;
					if (castToType) {
						if ("Y".equalsIgnoreCase(pMultiFlag)) {
							obj = addArray(obj, pVal, pType);
						} else {
							obj = castProperty(pVal, pType);
						}
					} else {
						obj = pVal;
					}

				}
				returnThing = obj;
			}
		} catch (Exception e) {
			logr.error("getProperty Exception. Returning null.", e);
			returnThing = null;
		} finally {
			if (dao == null) {
				try {
					rs.close();
				} catch (SQLException sqe) {
				}
				try {
					cs.close();
				} catch (SQLException sqe) {
				}
				try {
					oConn.rollback();
				} catch (Exception t) {
				}
			} else {
				dao.close(rs);
				dao.close(cs);
				dao.rollbackAndIgnoreException();
				dao.close();
			}
		}
		return returnThing;
	}

	private static Object addArray(Object obj, String pVal, String pType)
			throws NegativeArraySizeException, ClassNotFoundException {
		Object newObj = null;
		if ("char".equalsIgnoreCase(pType)) {
			int length = obj == null ? 1 : Array.getLength(obj) + 1;
			newObj = Array.newInstance(Class.forName("java.lang.String"),
					length);
			for (int i = 0; i < length - 1; i++) {
				Array.set(newObj, i, Array.get(obj, i));
			}
			Array.set(newObj, length - 1, pVal);
		} else if ("int".equalsIgnoreCase(pType)) {
			int length = obj == null ? 1 : Array.getLength(obj) + 1;
			newObj = Array.newInstance(int.class, length);
			for (int i = 0; i < length - 1; i++) {
				Array.setInt(newObj, i, Array.getInt(obj, i));
			}
			Array.setInt(newObj, length - 1, Integer.parseInt(pVal));
		} else if ("double".equalsIgnoreCase(pType)) {
			int length = obj == null ? 1 : Array.getLength(obj) + 1;
			newObj = Array.newInstance(double.class, length);
			for (int i = 0; i < length - 1; i++) {
				Array.setDouble(newObj, i, Array.getDouble(obj, i));
			}
			Array.setDouble(newObj, length - 1, Double.parseDouble(pVal));
		} else if ("float".equalsIgnoreCase(pType)) {
			int length = obj == null ? 1 : Array.getLength(obj) + 1;
			newObj = Array.newInstance(float.class, length);
			for (int i = 0; i < length - 1; i++) {
				Array.setFloat(newObj, i, Array.getFloat(obj, i));
			}
			Array.setFloat(newObj, length - 1, Float.parseFloat(pVal));
		} else {
			newObj = obj;
		}

		return newObj;
	}

	public static Map getProperties(String userID) {
		try {
			Map map = getPropertiesByKey(userID, null, false);
			return map;
		} catch (Exception e) {
			logr.error("getProperty Exception in getProperties.", e);
			throw new EMGRuntimeException(e);
		}
	}

	// if key is null, return all in a Map object, else return that key.
	public static Map getPropertiesByKey(String userID, PropertyKey pk,
			boolean returnMapKeyedByPropName) {
		PropertyDAO dao = new PropertyDAO();
		Connection conn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		Map map = null;

		try {
			DateFormatter df = new DateFormatter("dd/MMM/yyyy hh:mm a", true);
			String storedProcName = "pkg_em_dynamic_properties.prc_get_admin_property_cv ";
			conn = dao.getConnection();
			cs = conn.prepareCall("{ call " + storedProcName
					+ "(?,?,?,?,?,?,?,?) }");
			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			if (pk != null)
				cs.setString(++paramIndex, pk.getPropName());
			else
				cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);

			if (logr.isDebugEnabled()) {
				logr.debug(storedProcName + " started: "
						+ System.currentTimeMillis());
			}

			cs.execute();

			if (logr.isDebugEnabled()) {
				logr.debug(storedProcName + " ended: "
						+ System.currentTimeMillis());
				long dbLogId = cs.getLong(dbLogIdIndex);
				logr.debug("dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(paramIndex);

			while (rs.next()) {
				if (map == null) {
					map = new Hashtable();
				}

				String pType = rs.getString("DATA_TYPE_CODE");
				String pVal = rs.getString("PROP_VAL");
				pVal = (castProperty(pVal, pType)).toString();

				PropertyBean bean = new PropertyBean(new PropertyKey(
						rs.getString("PROP_NAME"), "Dynamic", pVal),
						rs.getString("PROP_DESC"),
						rs.getString("ADMIN_USE_FLAG"),
						rs.getString("MULTIVALUE_FLAG"), pType, df.format(rs
								.getTimestamp("CREATE_DATE")),
						rs.getString("CREATE_USERID"), df.format(rs
								.getTimestamp("LAST_UPDATE_DATE")),
						rs.getString("LAST_UPDATE_USERID"), true, false, false);
				// return a map keyed by Propname or by PropertyKey object
				if (!returnMapKeyedByPropName)
					map.put(bean.getPropKey(), bean);
				else
					map.put(bean.getPropKey().getPropName(), bean);
			}
		} catch (Exception e) {
			logr.error("getProperty Exception. Returning null.", e);
			throw new EMGRuntimeException(e);
		} finally {
			dao.close(rs);
			dao.close(cs);
			dao.close();
		}

		return map;
	}

	private static Object castProperty(String pVal, String pType)
			throws IllegalArgumentException {
		if (pType == null || pType.equalsIgnoreCase("char")) {
			return pVal;
		}
		if (pType.equalsIgnoreCase("int")) {
			return Integer.valueOf(pVal);
		}
		if (pType.equalsIgnoreCase("boolean")) {
			return new Boolean(pVal.equalsIgnoreCase("true")
					|| pVal.equalsIgnoreCase("yes")
					|| pVal.equalsIgnoreCase("active")
					|| pVal.equalsIgnoreCase("on")
					|| pVal.equalsIgnoreCase("youbetcha"));
		}
		if (pType.equalsIgnoreCase("float")) {
			return new Float(Float.parseFloat(pVal));
		}
		if (pType.equalsIgnoreCase("double")) {
			return Double.valueOf(Double.parseDouble(pVal));
		}
		throw new IllegalArgumentException(pType + " is not a valid pType");
	}

	public static void setPropertyValue(PropertyBean pb)
			throws DataSourceException {
		PropertyDAO dao = new PropertyDAO();
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName = "pkg_emg_type_stat_maint.prc_iu_admin_property";
		StringBuilder sqlBuffer = new StringBuilder(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?) }");

		try {
			conn = dao.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			// input parameters
			int parm = 0;
			cs.setString(++parm, pb.getPropKey().getPropName());
			cs.setString(++parm, pb.getPropDesc());
			cs.setString(++parm, pb.getPropKey().getPropVal());
			cs.setString(++parm, pb.getAdminUseFlag());
			cs.setString(++parm, pb.getMultivalueFlag());
			cs.setString(++parm, pb.getDataTypeCode());

			cs.execute();
			dao.commit();

		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			dao.close(cs);
			dao.close();
		}
		return;
	}

	/**
	 * Getter that doesn't use the superclass connection. NO TRANSACTION
	 * BEHAVIOUR -
	 * 
	 * @param conn
	 *            provide your own.
	 * @param userID
	 * @param contextLogger
	 *            most likely from the init Servlet.
	 * @return
	 */

	public static Map bootstrapPropertyGetter(Connection conn, String userID,
			Logger contextLogger) {
		Map rM = null;
		try {
			rM = (Map) CorePropertyObjectGetter(conn, null, null, userID, true);
			// cast to type = true.
			conn.close();
		} catch (SQLException e) {
			System.out
					.println("FAILED: Dynamic Properties BootstrapGetter - SQLException ");
			logr.error(e.getMessage(), e);
			contextLogger
					.error("FAILED: Dynamic Properties BootstrapGetter - SQLException",
							e);
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
			}
		}
		return rM;
	}

}
