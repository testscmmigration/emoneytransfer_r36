/*
 * Created on Jan 19, 2005
 *
 */
package emgshared.dataaccessors;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import java.sql.Connection;
import oracle.jdbc.driver.OracleTypes;
import emgshared.exceptions.DataSourceException;

/**
 * @author A131
 *
 */
public class QuestionDAO extends AbstractOracleDAO
{
	public Map getQuestions() throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		Map questions = new HashMap();

		try
		{
			conn = getConnection();

			String storedProcName =
				"pkg_em_customer_profile.prc_get_verify_question_cv";
			cs =
				conn.prepareCall(
					"{ call " + storedProcName + "(?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, "WEB");
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDebugEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDebugEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).debug(
					"dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(cursorIndex);
			long rowCount = 0;
			while (rs.next())
			{
				++rowCount;
				questions.put(
					Integer.valueOf(rs.getInt("VRFY_QUEST_ID")),
					rs.getString("VRFY_QUEST_TEXT"));
			}

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDebugEnabled())
			{
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).debug(
					storedProcName + " returned " + rowCount + " rows");
			}
			logEndTime(dbLogId,rowCount);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return questions;
	}
}
