/**
 * @author A131
 *
 * Logger class that returns an instance of the MoneyGram LogFactory class.
 * This class takes in the class file name so that it will appear in the log entry.
 */
package emgshared.dataaccessors;

import org.apache.log4j.Logger;

public class EMGSharedLogger
{
	public static Logger getLogger(String className)
	{
		return Logger.getLogger(className);
	}
	
	public static Logger getLogger(Class clazz)
	{
 	  	return Logger.getLogger(clazz);
	}
	
}
