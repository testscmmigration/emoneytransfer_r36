/*
 * Created on Jan 6, 2005
 *
 */
package emgshared.dataaccessors;

import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;
import oracle.sql.BLOB;


import com.moneygram.mgo.service.consumerValidationV2_1.RiskIndicator;

import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.exceptions.ParseDateException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.CodeDescription;
import emgshared.model.ConsumerAddress;
import emgshared.model.ConsumerDocument;
import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerIdentification;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileActivity;
import emgshared.model.ConsumerProfileComment;
import emgshared.model.ConsumerProfileSearchCriteria;
import emgshared.model.ConsumerProfileSearchView;
import emgshared.model.ConsumerStatus;
import emgshared.model.CustPremierType;
import emgshared.model.CustomerAuthenticationInfo;
import emgshared.model.DocumentType;
import emgshared.model.ElectronicTypeCode;
import emgshared.model.EmailStatus;
import emgshared.model.LexisNexisActivity;
import emgshared.model.LogonFailure;
import emgshared.model.NewConsumerProfile;
import emgshared.model.PhoneNumber;
import emgshared.model.PhoneType;
import emgshared.model.PurposeType;
import emgshared.model.ResidentStatusCode;
import emgshared.model.SearchCriteriaDTO;
import emgshared.model.TaintIndicatorType;
import emgshared.property.EMTSharedDynProperties;
import emgshared.util.Constants;
import emgshared.util.DateFormatter;
import emgshared.util.DateManager;
import emgshared.util.StringHelper;
import emgshared.util.SSNIndicatorEnum;
import emgshared.util.SSNValidIndicatorEnum;

/**
 * @author A131
 *
 */
public class ConsumerProfileDAO extends AbstractOracleDAO {
	
	private static Logger logger = EMGSharedLogger.getLogger(
			ConsumerProfileDAO.class);
	
	private static EMTSharedDynProperties dynProps = new EMTSharedDynProperties();
	private static final String ALL_PARTNER_SITE_ID = "All";
	private static final String USER_ID = "MGO";
	private static final String UNIQUE_CONSTRAINT_VIOLATED = "23000";

	public int insertProfile(NewConsumerProfile newConsumerProfile) throws DataSourceException {
		CallableStatement cs = null;
		Connection conn = null;
		int newId = -1;
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_iu_customer_profile";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?, ?,?,?,?,?," + "?,?,?,?,?, ?,?,?,?,?,"
					+ "?,?,?,?,?, ?,?,?,?,?," + "?,?,?,?,?, ?,?,?,?,?," + "?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, newConsumerProfile.getUserId());
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, 0);
			cs.setString(++paramIndex, newConsumerProfile.getEdirGuid());
			cs.setString(++paramIndex, newConsumerProfile.getUserId());
			cs.setString(++paramIndex, newConsumerProfile.getStatusCode());
			cs.setString(++paramIndex, newConsumerProfile.getSubStatusCode());
			cs.setString(++paramIndex, newConsumerProfile.getLastName());
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, newConsumerProfile.getFirstName());
			cs.setString(++paramIndex, newConsumerProfile.getMiddleName());
			cs.setString(++paramIndex, newConsumerProfile.getSsnLast4());
			cs.setDate(++paramIndex, new java.sql.Date(newConsumerProfile.getBirthdate().getTime()));
			cs.setString(++paramIndex, newConsumerProfile.getGuid());
			cs.setString(++paramIndex, newConsumerProfile.isAcceptPromotionalEmail() ? "Y" : "N");
			cs.setNull(++paramIndex, Types.VARCHAR); // auth status flag
			cs.setNull(++paramIndex, Types.DATE); // auth status date
			cs.setNull(++paramIndex, Types.INTEGER); // auth fail count
			cs.setString(++paramIndex, newConsumerProfile.getPassword());
			cs.setString(++paramIndex, newConsumerProfile.getAddressLine1());
			cs.setString(++paramIndex, newConsumerProfile.getAddressLine2());
			cs.setNull(++paramIndex, Types.VARCHAR); // adddress line 3		
			cs.setString(++paramIndex, newConsumerProfile.getCity());
			cs.setString(++paramIndex, newConsumerProfile.getState());
			cs.setString(++paramIndex, newConsumerProfile.getPostalCodePlusZip4());
			cs.setString(++paramIndex, newConsumerProfile.getIsoCountryCode());
			cs.setNull(++paramIndex, Types.VARCHAR); // addr_cnty_name
			// cs.setString(++paramIndex, newConsumerProfile.getCountyName());  // addr_cnty_name			
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, newConsumerProfile.getPhoneNumber());
			if (ConsumerProfile.HOME_PHONE_TYPE.equals(newConsumerProfile.getPhoneNumberType())) { //     iv_prm_phn_type_code          IN cust_phone.phn_type_code%TYPE,         --502115
				cs.setInt(++paramIndex, 1);
			} else if (ConsumerProfile.MOBILE_PHONE_TYPE.equals(newConsumerProfile.getPhoneNumberType())) {
				cs.setInt(++paramIndex, 2);
			} else {
				cs.setNull(++paramIndex, Types.INTEGER);
			}
			cs.setString(++paramIndex, newConsumerProfile.getPhoneNumberAlternate());
			// TODO In the future, users can have multiple emails.
			Collection emails = new HashSet();
			emails = newConsumerProfile.getEmails();
			ConsumerEmail consumerEmail = null;
			for (Iterator iter = emails.iterator(); iter.hasNext();) {
				consumerEmail = (ConsumerEmail) iter.next();
				cs.setString(++paramIndex, consumerEmail.getConsumerEmail());
			}
			if (dynProps.isEmailValidation()) {
				// Set to PND
				cs.setString(++paramIndex, EmailStatus.PENDING_VERIFICATION.toString());
			} else {
				// Set to ACT
				cs.setString(++paramIndex, EmailStatus.ACTIVE.toString());
			}
			cs.setInt(++paramIndex, Integer.parseInt(newConsumerProfile.getVerificationQuestionId()));
			cs.setString(++paramIndex, newConsumerProfile.getVerificationAnswer());
			cs.setString(++paramIndex, newConsumerProfile.getPhoneTaintCode().toString());
			cs.setString(++paramIndex, newConsumerProfile.getPhoneAlternateTaintCode().toString());
			cs.setString(++paramIndex, newConsumerProfile.getSsnTaintCode().toString());
			cs.setString(++paramIndex, newConsumerProfile.getEmailTaintCode().toString());
			cs.setString(++paramIndex, newConsumerProfile.getEmailDomainTaintCode().toString());
			cs.setString(++paramIndex, newConsumerProfile.getCreateIpAddrId());
			// customer premier code always starts with 'S'
			cs.setString(++paramIndex, "S");
			cs.setString(++paramIndex, newConsumerProfile.getLoyaltyPgmMembershipId());
			cs.setNull(++paramIndex, Types.VARCHAR);  // ov_prcs_log_id
			cs.setNull(++paramIndex, Types.VARCHAR); // ov_cust_id
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int newIdIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			newId = ((Integer) cs.getObject(newIdIndex)).intValue();
			logEndTime(dbLogId);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(cs);
		}
		return newId;
	}

	/**
	 * @param consumerProfile
	 *
	 *            Created on Jan 24, 2005
	 */
	public void updateProfile(ConsumerProfile consumerProfile, String updatedByUserId, String sourceSite) throws DataSourceException {
		CallableStatement cs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_iu_customer_profile";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?, ?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?,?,?,?,"
					+ "?,?,?,?,?, ?,?,?,?,?," + "?,?,?,?,?, ?,?,?,?,?," + "?,?,?,?,?, " + "?,?,?,?) }"); //53
			
			//cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?, ?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?,?,?,?,"
			//		+ "?,?,?,?,?, ?,?,?,?,?," + "?,?,?,?,?, ?,?,?,?,?," + "?,?) }");  //46

			int paramIndex = 0;
			cs.setString(++paramIndex, updatedByUserId); // iv_user_id                    IN customer.cust_logon_id%TYPE,
			cs.setString(++paramIndex, dbCallTypeCode); // iv_call_type_code             IN process_log_a.call_type_code%TYPE,
			cs.setInt(++paramIndex, 1); // 0 = insert; 1 = update // iv_action_code                IN INTEGER,
			if (consumerProfile.getEdirGuid() != null) {
				cs.setString(++paramIndex, consumerProfile.getEdirGuid()); // iv_edir_gu_id             IN customer.edir_gu_id%TYPE,
			} else {
				cs.setNull(++paramIndex, Types.VARCHAR); // iv_edir_gu_id             IN customer.edir_gu_id%TYPE,
			}
			cs.setString(++paramIndex, consumerProfile.getUserId()); // iv_cust_logon_id              IN customer.cust_logon_id%TYPE,
			cs.setString(++paramIndex, consumerProfile.getStatusCode()); // iv_cust_stat_code             IN customer.cust_stat_code%TYPE,
			cs.setString(++paramIndex, consumerProfile.getSubStatusCode()); // iv_cust_sub_stat_code         IN customer.cust_sub_stat_code%TYPE,
			cs.setString(++paramIndex, consumerProfile.getLastName()); // iv_cust_last_name             IN customer.cust_last_name%TYPE,
			if(consumerProfile.getSecondLastName()!=null && !consumerProfile.getSecondLastName().equals("")){
				cs.setString(++paramIndex, consumerProfile.getSecondLastName()); // iv_cust_matrnl_name           IN customer.cust_matrnl_name%TYPE,
			} else {
				cs.setNull(++paramIndex, Types.VARCHAR); // iv_cust_matrnl_name           IN customer.cust_matrnl_name%TYPE,
			}
			
			cs.setString(++paramIndex, consumerProfile.getFirstName()); // iv_cust_frst_name             IN customer.cust_frst_name%TYPE,
			cs.setString(++paramIndex, consumerProfile.getMiddleName()); // iv_cust_mid_name              IN customer.cust_mid_name%TYPE,
			cs.setString(++paramIndex, consumerProfile.getSsnLast4()); // iv_cust_ssn_mask_nbr          IN customer.cust_ssn_mask_nbr%TYPE,
			// cs.setNull(++paramIndex, Types.DATE); // birthdate
			cs.setDate(++paramIndex, new java.sql.Date(consumerProfile.getBirthdate().getTime())); // iv_cust_brth_date             IN customer.cust_brth_date%TYPE,
			cs.setString(++paramIndex, consumerProfile.getGuid()); // guid // iv_cust_gu_id                 IN customer.cust_gu_id%TYPE,
			cs.setString(++paramIndex, consumerProfile.isAcceptPromotionalEmail() ? "Y" : "N"); // iv_promo_email_code           IN cust_web_site_pref.promo_email_flag%TYPE, --542044
			cs.setString(++paramIndex, consumerProfile.isAuthStatusFlag() ? "Y" : "N"); // iv_prfl_auth_stat_flag        IN customer.prfl_auth_stat_flag%TYPE,
			java.util.Date authStatusDate = consumerProfile.getAuthStatusDate();
			cs.setTimestamp(++paramIndex, (authStatusDate == null ? null : new Timestamp(authStatusDate.getTime()))); // iv_prfl_auth_stat_date        IN customer.prfl_auth_stat_date%TYPE,
			cs.setInt(++paramIndex, consumerProfile.getAuthStatusFailCount()); // iv_prfl_auth_failr_cnt        IN customer.prfl_auth_failr_cnt%TYPE,
			cs.setNull(++paramIndex, Types.VARCHAR); // password // iv_pswd_text                  IN cust_password.pswd_text%TYPE,
			cs.setString(++paramIndex, consumerProfile.getAddressLine1()); // iv_addr_line1_text            IN cust_address.addr_line1_text%TYPE,
			cs.setString(++paramIndex, consumerProfile.getAddressLine2()); // iv_addr_line2_text            IN cust_address.addr_line2_text%TYPE,
			cs.setString(++paramIndex, consumerProfile.getAddressLine3()); // address line 3 // iv_addr_line3_text            IN cust_address.addr_line3_text%TYPE,
			cs.setString(++paramIndex, consumerProfile.getCity()); // iv_addr_city_name             IN cust_address.addr_city_name%TYPE,
			cs.setString(++paramIndex, consumerProfile.getState()); //     iv_addr_state_name            IN subdiv.iso_subdiv_code%TYPE,           --502115
			cs.setString(++paramIndex, consumerProfile.getPostalCodePlusZip4()); //     iv_addr_postal_code           IN cust_address.addr_postal_code%TYPE,
			cs.setString(++paramIndex, consumerProfile.getIsoCountryCode()); //     iv_addr_cntry_id              IN cust_address.addr_cntry_id%TYPE,
			cs.setString(++paramIndex, consumerProfile.getCountyName()); //     iv_addr_cnty_name         IN cust_address.addr_cnty_name%TYPE,      --502115
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, consumerProfile.getPhoneNumber()); //     iv_primary_phone              IN cust_phone.cust_ph_nbr%TYPE,
			if (ConsumerProfile.HOME_PHONE_TYPE.equals(consumerProfile.getPhoneNumberType())) { //     iv_prm_phn_type_code          IN cust_phone.phn_type_code%TYPE,         --502115
				cs.setInt(++paramIndex, 1);
			} else if (ConsumerProfile.MOBILE_PHONE_TYPE.equals(consumerProfile.getPhoneNumberType())) {
				cs.setInt(++paramIndex, 2);
			} else {
				cs.setNull(++paramIndex, Types.INTEGER);
			}

			if (consumerProfile.getPhoneNumberAlternate() != null) {
				cs.setString(++paramIndex, consumerProfile.getPhoneNumberAlternate()); //     iv_alternate_phone            IN cust_phone.cust_ph_nbr%TYPE,
				if (ConsumerProfile.HOME_PHONE_TYPE.equals(consumerProfile.getPhoneNumberAlternateType())) { //     iv_alt_phn_type_code          IN cust_phone.phn_type_code%TYPE,         --502115
					cs.setInt(++paramIndex, 1);
				} else if (ConsumerProfile.MOBILE_PHONE_TYPE.equals(consumerProfile.getPhoneNumberAlternateType())) {
					cs.setInt(++paramIndex, 2);
				} else {
					cs.setNull(++paramIndex, Types.INTEGER);
				}
			} else { // No alternative phone number
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.setNull(++paramIndex, Types.INTEGER);
			}
			Collection emails = new HashSet();
			emails = consumerProfile.getEmails();
			ConsumerEmail consumerEmail = null;
			if (emails.isEmpty()){
			    cs.setNull(++paramIndex, Types.VARCHAR); 
			    cs.setNull(++paramIndex, Types.VARCHAR);
			}else{
			for (Iterator iter = emails.iterator(); iter.hasNext();) {
				consumerEmail = (ConsumerEmail) iter.next();
				cs.setString(++paramIndex, consumerEmail.getConsumerEmail()); //     iv_email_addr                 IN cust_electronic_addr.cust_elec_addr_id%TYPE,
				cs.setString(++paramIndex, consumerEmail.getStatus().getStatusCode()); //     iv_elec_addr_stat_code        IN cust_electronic_addr.elec_addr_stat_code%TYPE,
			}
			}
			cs.setNull(++paramIndex, Types.INTEGER); //    iv_vrfy_quest_id              IN verify_question.vrfy_quest_id%TYPE,
			cs.setNull(++paramIndex, Types.VARCHAR); //     iv_vrfy_ans_text              IN cust_verification.vrfy_ans_text%TYPE,
			cs.setString(++paramIndex, consumerProfile.getPhoneTaintCode().toString()); //     iv_ph_blkd_code               IN cust_phone.ph_blkd_code%TYPE,
			cs.setString(++paramIndex, consumerProfile.getPhoneAlternateTaintCode().toString()); //     iv_alt_ph_blkd_code           IN cust_phone.ph_blkd_code%TYPE,
			cs.setString(++paramIndex, consumerProfile.getSsnTaintCode().toString()); //     iv_ssn_blkd_code              IN customer.ssn_blkd_code%TYPE,
			cs.setString(++paramIndex, consumerProfile.getEmailTaintCode().toString()); //     iv_elec_addr_blkd_code        IN cust_electronic_addr.elec_addr_blkd_code%TYPE,
			cs.setString(++paramIndex, consumerProfile.getEmailDomainTaintCode().toString()); //     iv_elec_addr_domn_blkd_code   IN cust_electronic_addr.elec_addr_domn_blkd_code%TYPE,
			cs.setString(++paramIndex, consumerProfile.getCreateIpAddrId()); //     iv_create_ip_addr_id          IN customer.create_ip_addr_id%TYPE,
			cs.setString(++paramIndex, consumerProfile.getCustPrmrCode()); //     iv_cust_prmr_code             IN customer.cust_prmr_code%TYPE,
			cs.setString(++paramIndex, consumerProfile.getLoyaltyPgmMembershipId()); //     iv_lylty_pgm_mbshp_id         IN customer.lylty_pgm_mbshp_id%TYPE,
			cs.setNull(++paramIndex, Types.VARCHAR); //     iv_cust_auto_enrl_flag        IN customer.cust_auto_enrl_flag%TYPE,
			cs.setString(++paramIndex, consumerProfile.getPartnerSiteId());
			if (consumerProfile.getRsaCustomerChallengeStatusCode()==0)
			{
				cs.setNull(++paramIndex,Types.VARCHAR);  // iv_scrty_chlng_stat_code in customer.SCRTY_CHLNG_STAT_CODE
			}
			else
			{
				cs.setInt(++paramIndex,consumerProfile.getRsaCustomerChallengeStatusCode());
			}
			
			cs.setInt(++paramIndex, consumerProfile.getId()); // Passing cust_id for MBO-1502
			//MBO-5464
			cs.setString(++paramIndex, consumerProfile.getSendLevel());
			cs.setString(++paramIndex, consumerProfile.getReceiveLevel());
			cs.setString(++paramIndex, consumerProfile.getResidentStatus());
			
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(cs);
		}
		return;
	}
	//MBO -2158 Email Age changes Added new method to save Email Age Score
	public void saveEmailAgeInfo(int custId, String respBsnsCode,String emailValueToSave,
			String callerLoginId) throws DataSourceException {

		CallableStatement cs = null;
		ResultSet rsConsumer = null;
		Connection conn = null;

		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_add_cust_frd_dtct_codes";
			cs = conn.prepareCall("{ call " + storedProcName
					+ "(?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, custId);
			cs.setString(++paramIndex, "EMAGE");
			cs.setString(++paramIndex, respBsnsCode);
			cs.setString(++paramIndex, emailValueToSave);
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.debug("dbLogId = " + dbLogId);

		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rsConsumer);
			close(cs);
		}
		return;

	}
	
	
	
	// MBO - 2158 changes Added new method to fetch the email age score 
	
		public String getEmailAgeInfo(ConsumerProfile customerProfile,String respBsnsCode) throws DataSourceException{
		
		String emailAgeResult = null;
		CallableStatement cs = null;
		ResultSet rsConsumer = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_get_cust_frd_dtct_codes_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, customerProfile.getUserId());
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, customerProfile.getId());
			cs.setString(++paramIndex, "EMAGE");
			cs.setString(++paramIndex, respBsnsCode);
			cs.registerOutParameter(++paramIndex, OracleTypes.FLOAT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramConsumerIndex = paramIndex;
			
			cs.execute();
			
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			
			rsConsumer = (ResultSet) cs.getObject(paramConsumerIndex);
			if (rsConsumer.next()) {
				emailAgeResult = rsConsumer.getString("FRD_DTCT_RESP_VAL");
			 EMGSharedLogger.getLogger("Email Age Info is " +emailAgeResult);
			
			}			
		}catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rsConsumer);
			close(cs);
		}
		return emailAgeResult;
	}

	public ConsumerProfile select(Integer consumerId, String callerLoginId, String consumerLogonId) throws DataSourceException {
		CallableStatement cs = null;
		ResultSet rsConsumer = null;
		Connection conn = null;
		ConsumerProfile consumerProfile = null;
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_get_customer_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			if (consumerId == null) {
				cs.setNull(++paramIndex, Types.INTEGER);
				cs.setString(++paramIndex, consumerLogonId);
			} else {
				cs.setInt(++paramIndex, consumerId.intValue());
				cs.setNull(++paramIndex, Types.VARCHAR);
			}
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			cs.registerOutParameter(++paramIndex, OracleTypes.FLOAT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramConsumerIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId, 1);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			rsConsumer = (ResultSet) cs.getObject(paramConsumerIndex);
			
			if (rsConsumer.next()) {
				int addressId = rsConsumer.getInt("addr_id");
				ConsumerAddress address = getConsumerAddress(rsConsumer.getInt("cust_id"), addressId);
				consumerProfile = createProfile(rsConsumer, address);
				Set emails = new HashSet();
				emails = selectElectronicAddress(consumerProfile.getId(), callerLoginId);
				consumerProfile.setEmails(emails);
				ConsumerEmail consumerEmail = null;
				boolean setEmailFromFirstRecord = false;			
			//MBO 2158 - Email Age changes
				String emailScore = getEmailAgeInfo(consumerProfile,"emailAgeScore");
				consumerProfile.setEmailScore(emailScore);
				//MBO-2587 - Email first seen date changes	 
				String emailFirstSeenDate= getEmailAgeInfo(consumerProfile,"emailFirstSeen");
				consumerProfile.setEmailFirstSeen(emailFirstSeenDate);
				
				//added for 3819
				String emailNameMatch= getEmailAgeInfo(consumerProfile,"emailAgeNameMatch");
				consumerProfile.setEmailAgeNameMatch(emailNameMatch);
				//ended
				
				//CPP-193 Start                    
	               int cnsmrPrflIdTypeCd= rsConsumer.getInt("CNSMR_PRFL_ID_TYPE_CODE");
	               consumerProfile.setCnsmrPrflIdTypeCd(cnsmrPrflIdTypeCd);
	            //CPP-193 End
				
				// added for MBO-5465
                String statusCode= rsConsumer.getString("cust_stat_code");
                consumerProfile.setCustStatusCode(statusCode);
                //ended
                
                //MBO-6804
                consumerProfile.setPhnCountryCode(rsConsumer.getString("DFLT_PHN_CNTRY_CODE"));
                //ended
				
				for (Iterator iter = emails.iterator(); iter.hasNext();) {
					consumerEmail = (ConsumerEmail) iter.next();
					if (consumerEmail.getEmailTaintCode().isBlocked()) {
						if (!consumerProfile.isEmailBlocked()) {
							consumerProfile.setEmailTaintCode(consumerEmail.getEmailTaintCode());
						}
					}
					if (consumerEmail.getEmailDomainTaintCode().isBlocked()) {
						if (!consumerProfile.isEmailDomainBlocked()) {
							consumerProfile.setEmailDomainTaintCode(consumerEmail.getEmailDomainTaintCode());
						}
					}
					// MBO-279 Replacing emailId for UserId only for guest profiles. 
					if ((consumerProfile.getUserId() == null || consumerProfile
							.getUserId().trim().equals(""))
							&& consumerProfile.getProfileType().equals(
									Constants.PROFILE_TYPE_GUEST)
							&& !setEmailFromFirstRecord) {
						consumerProfile.setUserId(consumerEmail
								.getConsumerEmail());
						setEmailFromFirstRecord = true;
					}

				}
				PhoneNumber[] phoneNumbers = selectPhoneNumbers(consumerProfile.getId(), callerLoginId);
				consumerProfile.setPhoneAlternateTaintCode(TaintIndicatorType.NOT_BLOCKED);
				for (int i = 0; i < phoneNumbers.length; i++) {
					PhoneNumber phoneNumber = phoneNumbers[i];
					PhoneType type = phoneNumber.getType();
					String phoneNumberType = phoneNumber.getPhoneNumberType();
					String phoneType = phoneNumberType == null ? "" :
						phoneNumberType.equals("HOME") ? ConsumerProfile.HOME_PHONE_TYPE : ConsumerProfile.MOBILE_PHONE_TYPE;
					if (PhoneType.PRIMARY.equals(type)) {
						consumerProfile.setPhoneNumber(phoneNumber.getPhoneNumber());
						consumerProfile.setPhoneNumberType(phoneType);
						if (!consumerProfile.isPhoneBlocked()) {
							phoneNumber.setPhoneTaintCode(TaintIndicatorType.getInstance(phoneNumber.getPhoneTaintCode().toString()));
							if (!consumerProfile.isPhoneOverridden()) {
								consumerProfile.setPhoneTaintCode(phoneNumber.getPhoneTaintCode());
							}
						}
						//MBO-7310
						consumerProfile.setPhoneCountryCode(phoneNumber.getCountryCode());
						consumerProfile.setPhoneDialingCode(phoneNumber.getCountryDialingCode());
					} else if (PhoneType.ALTERNATE.equals(type)) {
						consumerProfile.setPhoneNumberAlternate(phoneNumber.getPhoneNumber());
						consumerProfile.setPhoneNumberAlternateType(phoneType);
						if (!consumerProfile.isPhoneAlternateBlocked()) {
							phoneNumber.setPhoneTaintCode(TaintIndicatorType.getInstance(phoneNumber.getPhoneTaintCode().toString()));
							if (!consumerProfile.isPhoneAlternateOverridden()) {
								consumerProfile.setPhoneAlternateTaintCode(phoneNumber.getPhoneTaintCode());
							}
						}
						//MBO-7310
						consumerProfile.setPhoneCountryCode(phoneNumber.getCountryCode());
						consumerProfile.setPhoneDialingCode(phoneNumber.getCountryDialingCode());
					}
				}
				
					//MBO-7547: handling multiple id records
				List<ConsumerIdentification> identifications = selectConsumerIdentification(
						consumerProfile.getId(), callerLoginId);
				if (identifications != null && !identifications.isEmpty()) {
					for (ConsumerIdentification identification : identifications) {
						if (identification.getIdType().equals(
								ConsumerIdentification.TEMP_RESIDENT_ID)) {
							consumerProfile = populateIdFieldsToCustProfile(
									consumerProfile, identification, true);
						} else {
							if(StringUtils.isBlank(consumerProfile
									.getIdExtnlId()))
								consumerProfile = populateIdFieldsToCustProfile(
										consumerProfile, identification, false);
						}
					}
				}					
			}
		
	}catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rsConsumer);
			close(cs);
		}
		return consumerProfile;
	}
	
	//MBO-7547
	private ConsumerProfile populateIdFieldsToCustProfile(
			ConsumerProfile consumerProfile,
			ConsumerIdentification identification, boolean isTempResident) {
		if (null != consumerProfile && null != identification) {
			if (!isTempResident) {
				consumerProfile.setIdExtnlId(identification.getId());
				consumerProfile.setIdNumber(identification.getIdNumber());
				consumerProfile.setIdType(identification.getIdType());
				consumerProfile.setIdTypeFriendlyName(identification
						.getIdTypeFriendlyName());
				consumerProfile.setDocStatus(identification.getDocStatusCode());
				// MBO - 1800
				consumerProfile.setIdExpirationDate(identification
						.getIdExpirationDate());
				consumerProfile.setIdIssueCountry(identification
						.getIdIssueCountry());
				// MBO - 1800
				// MBO-4742 Starts Here
				consumerProfile.setMrzLine1(null != identification
						.getMrzLine1() ? identification.getMrzLine1() : null);
				consumerProfile.setMrzLine2(null != identification
						.getMrzLine2() ? identification.getMrzLine2() : null);
				consumerProfile.setMrzLine3(null != identification
						.getMrzLine3() ? identification.getMrzLine3() : null);
			} else {
				consumerProfile.setTempResidentNumber(identification
						.getIdNumber());
				consumerProfile.setTempResidentExpiry(identification
						.getIdExpirationDate());
			}
		}

		return consumerProfile;
	}

	public String[] selectConsumerQuestion(String userId, int custId, int verifyQuestId) throws DataSourceException {
		CallableStatement cs = null;
		ResultSet rsQuestion = null;
		Connection conn = null;
		String[] strArray = new String[2];
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_get_cust_verification_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, custId);
			if (verifyQuestId == 0) {
				cs.setNull(++paramIndex, Types.INTEGER);
			} else {
				cs.setInt(++paramIndex, verifyQuestId);
			}
			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramQuestionIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			rsQuestion = (ResultSet) cs.getObject(paramQuestionIndex);
			long count = 0;
			if (rsQuestion.next()) {
				count++;
				strArray[0] = rsQuestion.getString("vrfy_quest_text");
				strArray[1] = rsQuestion.getString("vrfy_ans_text");
			}
			logEndTime(dbLogId, count);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rsQuestion);
			close(cs);
		}
		return strArray;
	}

	public boolean isExistingUserId(String userId) throws DataSourceException {
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		boolean exists = false;
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_get_customer_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, userId);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.registerOutParameter(++paramIndex, Types.FLOAT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			rs = (ResultSet) cs.getObject(cursorIndex);
			exists = (rs.next());
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rs);
			close(cs);
		}
		return exists;
	}

	private ConsumerProfile loadProfile(CallableStatement cs, ConsumerProfile consumerProfile, int paramElecAddressIndex,
			int paramPhoneIndex) throws SQLException {
		ResultSet rsElecAddress = null;
		ResultSet rsPhoneNumber = null;
		try {
			rsElecAddress = (ResultSet) cs.getObject(paramElecAddressIndex);
			ElectronicTypeCode elecTypeCode = null;
			EmailStatus emailStatus = null;
			TaintIndicatorType emailTaintCode = null;
			TaintIndicatorType emailDomainTaintCode = null;
			ConsumerEmail consumerEmail = null;
			Set emails = new HashSet();
			while (rsElecAddress.next()) {
				if (rsElecAddress.getString("elec_addr_stat_code").equals(EmailStatus.ACTIVE.toString())
						|| rsElecAddress.getString("elec_addr_stat_code").equals(EmailStatus.PENDING_VERIFICATION.toString())) {
					emailStatus = EmailStatus.getInstance(rsElecAddress.getString("elec_addr_stat_code"));
					elecTypeCode = ElectronicTypeCode.getInstance(rsElecAddress.getString("elec_addr_type_code"));
					emailTaintCode = TaintIndicatorType.getInstance(rsElecAddress.getString("elec_addr_blkd_code"));
					emailDomainTaintCode = TaintIndicatorType.getInstance(rsElecAddress.getString("elec_addr_domn_blkd_code"));
					consumerEmail = new ConsumerEmail(rsElecAddress.getInt("cust_id"), rsElecAddress.getString("cust_elec_addr_id"),
							elecTypeCode, emailStatus, emailTaintCode, emailDomainTaintCode);
					emails.add(consumerEmail);
					if (emailTaintCode.isBlocked()) {
						if (!consumerProfile.isEmailBlocked()) {
							consumerProfile.setEmailTaintCode(emailTaintCode);
						}
					}
				}
			}
			consumerProfile.setEmails(emails);
			rsPhoneNumber = (ResultSet) cs.getObject(paramPhoneIndex);
			consumerProfile.setPhoneAlternateTaintCode(TaintIndicatorType.NOT_BLOCKED);
			while (rsPhoneNumber.next()) {
				if (rsPhoneNumber.getString("ph_type_code").equals(PhoneType.PRIMARY.getCode())) {
					consumerProfile.setPhoneNumber(rsPhoneNumber.getString("cust_ph_nbr"));
					if (!consumerProfile.isPhoneBlocked()) {
						if (!consumerProfile.isPhoneOverridden()) {
							consumerProfile.setPhoneTaintCode(TaintIndicatorType.getInstance(rsPhoneNumber.getString("ph_blkd_code")));
						}
					}
				} else if (rsPhoneNumber.getString("ph_type_code").equals(PhoneType.ALTERNATE.getCode())) {
					consumerProfile.setPhoneNumberAlternate(rsPhoneNumber.getString("cust_ph_nbr"));
					if (!consumerProfile.isPhoneAlternateBlocked()) {
						if (!consumerProfile.isPhoneAlternateOverridden()) {
							consumerProfile.setPhoneAlternateTaintCode(TaintIndicatorType.getInstance(rsPhoneNumber
									.getString("ph_blkd_code")));
						}
					}
				}
			}
		} finally {
			close(rsElecAddress);
			close(rsPhoneNumber);
			// Being passed in, should not be closed.
			// close(cs);
		}
		return consumerProfile;
	}

	private ConsumerProfile createProfile(ResultSet rsConsumer, ConsumerAddress consumerAddress) throws SQLException {
		ConsumerStatus status = ConsumerStatus.getInstance(rsConsumer.getString("cust_stat_code"), rsConsumer
				.getString("cust_sub_stat_code"));
		ConsumerProfile consumerProfile = new ConsumerProfile(rsConsumer.getInt("cust_id"), rsConsumer.getString("cust_ssn_mask_nbr"),
				status, consumerAddress, TaintIndicatorType.NOT_BLOCKED, TaintIndicatorType.NOT_BLOCKED, TaintIndicatorType.NOT_BLOCKED,
				TaintIndicatorType.NOT_BLOCKED, TaintIndicatorType.NOT_BLOCKED);
		
			consumerProfile.setUserId(rsConsumer.getString("cust_logon_id"));
		
		consumerProfile.setFirstName(rsConsumer.getString("cust_frst_name"));
		consumerProfile.setMiddleName(rsConsumer.getString("cust_mid_name"));
		consumerProfile.setLastName(rsConsumer.getString("cust_last_name"));
		consumerProfile.setSecondLastName(rsConsumer.getString("cust_matrnl_name"));
		consumerProfile.setPswdText(rsConsumer.getString("pswd_text"));
		consumerProfile.setBirthdate(rsConsumer.getTimestamp("cust_brth_date"));
		consumerProfile.setCreateDate(rsConsumer.getTimestamp("create_date"));
		consumerProfile.setAdaptiveAuthProfileCompleteDate(rsConsumer.getTimestamp("ADPTV_AUTHN_PRFL_CMPLT_DATE"));
		consumerProfile.setSourceDevice(rsConsumer.getString("src_dvc_bsns_code"));
		consumerProfile.setSourceApp(rsConsumer.getString("src_appln_bsns_code"));
		String eDirGuid = rsConsumer.getString("edir_gu_id");
		if (eDirGuid == null) {
			eDirGuid = "";
		}
		consumerProfile.setEdirGuid(eDirGuid);
		if (rsConsumer.getString("prfl_auth_stat_flag") != null && rsConsumer.getString("prfl_auth_stat_flag").toLowerCase().equals("y")) {
			consumerProfile.setAuthStatusFlag(true);
		} else {
			consumerProfile.setAuthStatusFlag(false);
		}
		consumerProfile.setAuthStatusDate(rsConsumer.getTimestamp("prfl_auth_stat_date"));
		consumerProfile.setAuthStatusFailCount(rsConsumer.getInt("prfl_auth_failr_cnt"));
		consumerProfile.setGuid(rsConsumer.getString("cust_gu_id"));
		String promoEmailCode = rsConsumer.getString("promo_email_code");
		if (promoEmailCode != null && promoEmailCode.equals("Y")) {
			consumerProfile.setAcceptPromotionalEmail(true);
		} else {
			consumerProfile.setAcceptPromotionalEmail(false);
		}
		consumerProfile.setSsnTaintCode(TaintIndicatorType.getInstance(rsConsumer.getString("ssn_blkd_code")));
		consumerProfile.setCreateIpAddrId(rsConsumer.getString("create_ip_addr_id"));
		consumerProfile.setCustPrmrCode(rsConsumer.getString("cust_prmr_code"));
		consumerProfile.setCustPrmrDate(rsConsumer.getTimestamp("prmr_code_update_date"));
		//consumerProfile.setLoyaltyPgmMembershipId(rsConsumer.getString("LYLTY_PGM_MBSHP_ID"));
		String bsns_code = rsConsumer.getString("BSNS_CODE");
		consumerProfile.setPartnerSiteId(bsns_code);
		String mstrTaint = rsConsumer.getString("CUST_BLKD_CODE");
		if (StringHelper.isNullOrEmpty(mstrTaint))
			consumerProfile.setConsumerMasterTaintCode(new TaintIndicatorType(TaintIndicatorType.NOT_BLOCKED.getStatusIndicator()));
		else
			consumerProfile.setConsumerMasterTaintCode(new TaintIndicatorType(mstrTaint));
		String gender = rsConsumer.getString("gndr_code");
		consumerProfile.setGender(gender == null ? "" : gender.equals("M") ? "Male" : "Female");
		//MBO-4925:Code change as part of spanish languange preference.
		consumerProfile.setPreferedLanguage( rsConsumer.getString("lang_tag_text"));
		//MBO-4925 Change ended
		//MBO-100:Show Profile Type on EMT Admin Consumer profile Page
		if (rsConsumer.getString("GST_CUST_PRFL_FLAG") != null && "N".equalsIgnoreCase(rsConsumer.getString("GST_CUST_PRFL_FLAG").trim())) {
			consumerProfile.setProfileType(Constants.PROFILE_TYPE_REGISTERED);
		} else {
			consumerProfile.setProfileType(Constants.PROFILE_TYPE_GUEST);
		}
		consumerProfile.setRsaCustomerChallengeStatusCode(rsConsumer.getInt("SCRTY_CHLNG_STAT_CODE"));
		//MBO-5464
		consumerProfile.setResidentStatus(rsConsumer.getString("resdnc_stat_bsns_code"));
		consumerProfile.setSendLevel(rsConsumer.getString("cust_max_snd_amt_lvl_bsns_code"));
		consumerProfile.setReceiveLevel(rsConsumer.getString("cust_max_rcv_amt_lvl_bsns_code"));
		consumerProfile.setCountryOfBirth(rsConsumer.getString("cust_brth_iso_cntry_code"));
		//MBO-6570
		consumerProfile.setNotificationSMSOptInFlag(rsConsumer.getString("oprn_notf_sms_opt_in_flag"));
		return consumerProfile;
	}

	public ConsumerAddress getConsumerAddress(int consumerId, int addressId) throws DataSourceException {
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		ConsumerAddress address = null;
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_get_cust_address_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setInt(++paramIndex, consumerId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, consumerId);
			cs.setInt(++paramIndex, addressId);
			cs.setString(++paramIndex,"N");
			//cs.setNull(++paramIndex, Types.VARCHAR); // stat code
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			rs = (ResultSet) cs.getObject(cursorIndex);
			Set addresses = populateAddresses(rs);
			// TODO what if more than one address returned?
			if (addresses.size() > 0) {
				address = (ConsumerAddress) addresses.iterator().next();
			}
			logEndTime(dbLogId, 1);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rs);
			close(cs);
		}
		return address;
	}
	//3269
	public List<ConsumerAddress> getConsumerAddressList(int consumerId) throws DataSourceException{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		final String active = "Y";  
		List<ConsumerAddress> consumerAddress = new ArrayList<ConsumerAddress>();
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_get_cust_address_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setInt(++paramIndex, consumerId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, consumerId);
			cs.setString(++paramIndex, null);
			//cs.setNull(++paramIndex, Types.VARCHAR);
			// stat code
			cs.setString(++paramIndex, active);
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			rs = (ResultSet) cs.getObject(cursorIndex);
			Set<ConsumerAddress> addresses = populateConsumerAddress(rs);
			// TODO what if more than one address returned?
			consumerAddress.addAll(addresses);
			logEndTime(dbLogId, 1);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rs);
			close(cs);
		}
		return consumerAddress;
	}

	public Set<ConsumerAddress> getConsumerAddressHistory(int consumerId,boolean isActive) throws DataSourceException {
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		Set<ConsumerAddress> addresses = null;
		String activeFlag = "";
		if(isActive){
			activeFlag = "Y";
		}else{
			activeFlag = "N";
		}
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_get_cust_address_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setInt(++paramIndex, consumerId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, consumerId);
			cs.setNull(++paramIndex, Types.BIGINT);
			cs.setString(++paramIndex, activeFlag);
       //	cs.setNull(++paramIndex, Types.VARCHAR); // stat code
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			rs = (ResultSet) cs.getObject(cursorIndex);
			addresses = populateAddresses(rs);
			logEndTime(dbLogId, 1);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rs);
			close(cs);
		}
		return addresses;
	}

	private Set<ConsumerAddress> populateAddresses(ResultSet rs) throws SQLException {
		Set<ConsumerAddress> addresses = new TreeSet<ConsumerAddress>();
		if (rs != null) {
			while (rs.next()) {
				String statusCode = null;
				java.util.Date addrEndDate = (java.util.Date) rs
						.getTimestamp("addr_end_date");
				try {
					if (null == addrEndDate
							|| DateManager.getCurrentDate().before(addrEndDate)) {
						statusCode = "ACT";
					} else {
						statusCode = "NAT";
					}
				} catch (Exception e) {
					logger.error(
							"Exception occured while findign Address Status with the Address End Date",
							e);
				}

				ConsumerAddress consumerAddress = new ConsumerAddress(
						rs.getInt("addr_id"), rs.getInt("cust_id"), statusCode,
						null, null);
				consumerAddress.setAddressLine1(rs.getString("addr_line1_text"));
				consumerAddress.setAddressLine2(rs.getString("addr_line2_text"));
				consumerAddress.setAddressLine3(rs.getString("addr_line3_text"));				
				consumerAddress.setCity(rs.getString("addr_city_name"));
				consumerAddress.setState(rs.getString("addr_state_name"));
				consumerAddress.setIsoCountryCode(rs.getString("addr_cntry_id"));				
				consumerAddress.setPostalCodePlusZip4(rs.getString("addr_postal_code"));		
				consumerAddress.setCreated(rs.getTimestamp("create_date"));
				consumerAddress.setCreatedBy(rs.getString("create_userid"));
				consumerAddress.setCountyName(rs.getString("addr_cnty_name"));
				consumerAddress.setProfileAddressInfo(rs.getString("curr_cust_prfl_addr_flag")); // Added for MBO-2781
				addresses.add(consumerAddress);
			}
		}
		return addresses;
	}
	
	private Set<ConsumerAddress> populateConsumerAddress(ResultSet rs) throws SQLException {
		Set<ConsumerAddress> addresses = new HashSet<ConsumerAddress>();
		if (rs != null) {
			while (rs.next()) {
				ConsumerAddress consumerAddress = new ConsumerAddress(rs.getInt("addr_id"), rs.getInt("cust_id"));
				consumerAddress.setAddressLine1(rs.getString("addr_line1_text"));
				consumerAddress.setAddressLine2(rs.getString("addr_line2_text"));
				consumerAddress.setAddressLine3(rs.getString("addr_line3_text"));				
				consumerAddress.setCity(rs.getString("addr_city_name"));
				consumerAddress.setState(rs.getString("addr_state_name"));
				consumerAddress.setPostalCodePlusZip4(rs.getString("addr_postal_code"));
				consumerAddress.setIsoCountryCode(rs.getString("addr_cntry_id"));
				consumerAddress.setCreated(rs.getTimestamp("create_date"));
				consumerAddress.setCreatedBy(rs.getString("create_userid"));
				consumerAddress.setCountyName(rs.getString("addr_cnty_name"));
				consumerAddress.setProfileAddressInfo(rs.getString("curr_cust_prfl_addr_flag"));
				addresses.add(consumerAddress);
			}
		}
		return addresses;
	}

	public int updatePassword(String userId, int custId, String password, String existingPassword, java.util.Date expirationDate)
			throws DataSourceException {
		CallableStatement cs = null;
		Connection conn = null;
		int updateCount = 0;
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_login_access.prc_iu_cust_password";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?,?, ?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, custId);
			cs.setString(++paramIndex, null);
			cs.setString(++paramIndex, password);
			cs.setDate(++paramIndex, null);
			if (expirationDate != null) {
				cs.setTimestamp(++paramIndex, new Timestamp(expirationDate.getTime()));
			} else {
				cs.setNull(++paramIndex, Types.DATE);
			}
			if (StringHelper.isNullOrEmpty(existingPassword)) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, existingPassword);
			}
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int updateCountIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			updateCount = ((Integer) cs.getObject(updateCountIndex)).intValue();
			logEndTime(dbLogId);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(cs);
		}
		return updateCount;
	}

	private Set selectElectronicAddress(int consumerId, String callerLoginId) throws DataSourceException {
		CallableStatement cs = null;
		ResultSet rsElectronicAddress = null;
		Connection conn = null;
		Set emails = new HashSet();
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_get_cust_elec_addr_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, consumerId);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramEmailIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis());
			}
			cs.execute();
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis());
			}
			rsElectronicAddress = (ResultSet) cs.getObject(paramEmailIndex);
			ElectronicTypeCode elecTypeCode = null;
			EmailStatus emailStatus = null;
			TaintIndicatorType emailTaintCode = null;
			TaintIndicatorType emailDomainTaintCode = null;
			ConsumerEmail consumerEmail = null;
			while (rsElectronicAddress.next()) {
				emailStatus = EmailStatus.getInstance(rsElectronicAddress.getString("elec_addr_stat_code"));
				elecTypeCode = ElectronicTypeCode.getInstance(rsElectronicAddress.getString("elec_addr_type_code"));
				emailTaintCode = TaintIndicatorType.getInstance(rsElectronicAddress.getString("Elec_Addr_Blkd_Code"));
				emailDomainTaintCode = TaintIndicatorType.getInstance(rsElectronicAddress.getString("Elec_Addr_Domn_Blkd_Code"));
				consumerEmail = new ConsumerEmail(rsElectronicAddress.getInt("cust_id"),
						rsElectronicAddress.getString("cust_elec_addr_id"), elecTypeCode, emailStatus, emailTaintCode, emailDomainTaintCode);
				emails.add(consumerEmail);
			}
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			if (conn != null) {
				try {
					logSQLDebug(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(this.getClass().getName().toString()).warn("Exception during sql debug logging", ignore);
				}
			}
			close(rsElectronicAddress);
			close(cs);
		}
		return emails;
	}

	private PhoneNumber[] selectPhoneNumbers(int consumerId, String callerLoginId) throws DataSourceException {
		Set phoneNumbers = new HashSet(2);
		CallableStatement cs = null;
		ResultSet rsPhoneNumbers = null;
		Connection conn = null;
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_get_cust_phone_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, consumerId);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramPhoneIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			rsPhoneNumbers = (ResultSet) cs.getObject(paramPhoneIndex);
			long count = 0;
			while (rsPhoneNumbers.next()) {
				count++;
				String number = rsPhoneNumbers.getString("cust_ph_nbr");
				String phoneTypeFlag = rsPhoneNumbers.getString("prm_phn_flag");
				String phoneType = rsPhoneNumbers.getString("bsns_code");
				PhoneType type = PhoneType.getInstance(phoneTypeFlag);
				PhoneNumber phoneNumber = new PhoneNumber(number, type, TaintIndicatorType.getInstance(rsPhoneNumbers.getString("ph_blkd_code")));
				phoneNumber.setPhoneNumberType(phoneType);
				phoneNumber.setCountryCode(rsPhoneNumbers.getString("cntry_cd"));
				phoneNumber.setCountryDialingCode(rsPhoneNumbers.getString("phn_cntry_code"));
				phoneNumbers.add(phoneNumber);
			}
			logEndTime(dbLogId, count);
		} catch (SQLException e) {
			EMGSharedLogger.getLogger(this.getClass()).error(
					e.getMessage(), e);
			throw new DataSourceException(e);
		} finally {
			close(rsPhoneNumbers);
			close(cs);
		}
		return (PhoneNumber[]) phoneNumbers.toArray(new PhoneNumber[0]);
	}

	private List<ConsumerIdentification> selectConsumerIdentification(int consumerId, String callerLoginId) throws DataSourceException {
		CallableStatement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_get_cust_extnl_ident_cv";
			statement = connection.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			statement.setString(++paramIndex, callerLoginId);
			statement.setString(++paramIndex, dbCallTypeCode);
			statement.setInt(++paramIndex, consumerId);
			statement.setNull(++paramIndex, Types.VARCHAR);
			statement.setNull(++paramIndex, Types.VARCHAR);
			statement.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			statement.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			statement.execute();
			//MBO-7547 - handle multiple id records for the customer
			List<ConsumerIdentification> consumerIdentifications = new ArrayList<ConsumerIdentification>();
			resultSet = (ResultSet) statement.getObject(paramIndex);
			while (resultSet.next()) {
				String idNumber = resultSet.getString("encrypt_extnl_id");
				String type = resultSet.getString("ident_doc_bsns_code");
				String id = resultSet.getString("cust_extnl_ident_id");
				String docStatusCode = resultSet.getString("bsns_code");
				//MBO-6091 Use ID label from DB in customer profile page
				String idTypeFriendlyName = resultSet.getString("ident_doc_bsns_desc");
				//MBO - 1800 ::::::::: starts
				String idIssueCountry = resultSet.getString("issu_iso_cntry_code")!= null ? resultSet.getString("issu_iso_cntry_code") : "";
				//Date idExpirationDate = resultSet.getDate("exp_date")!= null ? resultSet.getDate("exp_date") : new Date();
				Date idExpirationDate = resultSet.getDate("exp_date")!= null ? resultSet.getDate("exp_date") : null;
				//MBO-4742 Display all the MRZ lines of an ID 
				String mrzLine1 = null != resultSet
						.getString("encrypt_mrz_ln1_text") ? resultSet
						.getString("encrypt_mrz_ln1_text") : null;
				String mrzLine2 = null != resultSet
						.getString("encrypt_psprt_mrz_ln2_text") ? resultSet
						.getString("encrypt_psprt_mrz_ln2_text") : null;
				String mrzLine3 = null != resultSet
						.getString("encrypt_mrz_ln3_text") ? resultSet
						.getString("encrypt_mrz_ln3_text") : null;
				//MBO-4742 Ends Here
				ConsumerIdentification consumerIdentification = ConsumerIdentification.getConsumer(id,
						idNumber, type, docStatusCode, idExpirationDate,
						idTypeFriendlyName, idIssueCountry, mrzLine1, mrzLine2,
						mrzLine3);
				
				consumerIdentifications.add(consumerIdentification);
				//MBO - 1800 ::::::::: ends
			}
			return consumerIdentifications;
		} catch(SQLException e) {
			throw new DataSourceException("", e);
		} finally {
			close(resultSet);
			close(statement);
		}
	}

	public Map getConsumerStatusDescriptions() throws DataSourceException {
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		Map consumerStatusDescriptions = new HashMap();
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_status_and_type.prc_get_cust_sub_status_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			rs = (ResultSet) cs.getObject(cursorIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				String statusCode = rs.getString("CUST_STAT_CODE");
				String subStatusCode = rs.getString("CUST_SUB_STAT_CODE");
				String statusDesc = rs.getString("CUST_STAT_DESC");
				String subStatusDesc = rs.getString("CUST_SUB_STAT_DESC");
				ConsumerStatus status = ConsumerStatus.getInstance(statusCode, subStatusCode);
				if(!status.getSubStatusCode().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_7.getSubStatusCode())&&
						!status.getSubStatusCode().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_8.getSubStatusCode())&&
						!status.getSubStatusCode().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_9.getSubStatusCode())&&
						!status.getSubStatusCode().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_10.getSubStatusCode())){
					consumerStatusDescriptions.put(status, statusDesc + ": " +
							subStatusDesc);
				} else {
					consumerStatusDescriptions.put(status, subStatusCode + " - " +
							subStatusDesc);
				}
			}
			logEndTime(dbLogId, count);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rs);
			close(cs);
		}
		return consumerStatusDescriptions;
	}

	public Map getConsumerCommentReasons() throws DataSourceException {
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		Map consumerCommentReasons = new HashMap();
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_status_and_type.prc_get_cust_comment_reason_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			rs = (ResultSet) cs.getObject(cursorIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				String code = rs.getString("CUST_CMNT_REAS_CODE");
				String desc = rs.getString("CMNT_REAS_DESC");
				consumerCommentReasons.put(code, desc);
			}
			logEndTime(dbLogId, count);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rs);
			close(cs);
		}
		return consumerCommentReasons;
	}

	/**
	 * @param consumerId
	 * @param callerLoginId
	 * @return Created on Mar 2, 2005
	 */
	public List selectComments(int consumerId, String callerLoginId) throws DataSourceException {
		CallableStatement cs = null;
		ResultSet rsConsumerComments = null;
		Connection conn = null;
		List comments = new ArrayList();
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_comments.prc_get_cust_comment_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, consumerId);
			cs.setNull(++paramIndex, Types.INTEGER);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramConsumerIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			rsConsumerComments = (ResultSet) cs.getObject(paramConsumerIndex);
			long count = 0;
			while (rsConsumerComments.next()) {
				count++;
				ConsumerProfileComment comment = new ConsumerProfileComment();
				comment.setId(rsConsumerComments.getInt("cust_cmnt_id"));
				comment.setCustId(rsConsumerComments.getInt("cust_id"));
				comment.setReasonCode(rsConsumerComments.getString("cust_cmnt_reas_code"));
				comment.setReasonDescription(rsConsumerComments.getString("cmnt_reas_desc"));
				comment.setText(rsConsumerComments.getString("cust_cmnt_text"));
				comment.setCreatedBy(rsConsumerComments.getString("create_userid"));
				Timestamp ts = rsConsumerComments.getTimestamp("create_date");
				if (ts != null) {
					comment.setCreateDate(new java.util.Date(ts.getTime()));
				}
				comments.add(comment);
			}
			logEndTime(dbLogId, count);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rsConsumerComments);
			close(cs);
		}
		return comments;
	}

	public List getCustomerActivities(int consumerId, String callerLoginId, Integer activityLogCode, Date beginDate, Date endDate)
			throws DataSourceException {
		CallableStatement cs = null;
		ResultSet rsConsumerActivities = null;
		Connection conn = null;
		List activities = new ArrayList();
		try {
			conn = getConnection();
			String storedProcName = "pkg_mgo_cust_profile.prc_get_cust_acty";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, callerLoginId);
			cs.setInt(++paramIndex, consumerId);
			if (activityLogCode == null)
				cs.setNull(++paramIndex, Types.INTEGER);
			else
				cs.setInt(++paramIndex, activityLogCode);
			if (beginDate == null)
				cs.setNull(++paramIndex, Types.DATE);
			else
				cs.setDate(++paramIndex, beginDate);
			if (endDate == null)
				cs.setNull(++paramIndex, Types.DATE);
			else
				cs.setDate(++paramIndex, endDate);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramConsumerIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			rsConsumerActivities = (ResultSet) cs.getObject(paramConsumerIndex);
			long count = 0;
			while (rsConsumerActivities.next()) {
				count++;
				ConsumerProfileActivity acty = new ConsumerProfileActivity();
				acty.setCustId(consumerId);
				acty.setActivityBusinessDescription(rsConsumerActivities.getString("acty_log_bsns_desc"));
				acty.setActivityBusinessCategory(rsConsumerActivities.getString("acty_log_cat_bsns_desc"));
				acty.setActivityDate(rsConsumerActivities.getTimestamp("acty_date"));
				acty.setCreatedBy(rsConsumerActivities.getString("create_userid"));
				activities.add(acty);
			}
			logEndTime(dbLogId, count);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rsConsumerActivities);
			close(cs);
		}
		return activities;
	}

	public int insertConsumerActivityLog(ConsumerProfileActivity activity, String callerLoginId) throws DataSourceException {
		CallableStatement cs = null;
		Connection conn = null;
		int newId = -1;
		try {
			conn = getConnection();
			String storedProcName = "pkg_mgo_cust_profile.prc_insrt_cust_acty";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, callerLoginId);
			cs.setInt(++paramIndex, activity.getCustId());
			java.sql.Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
			cs.setTimestamp(++paramIndex, ts);
			cs.setInt(++paramIndex, activity.getActivityLogCode().intValue());
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int newIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			newId = ((Integer) cs.getObject(newIdIndex)).intValue();
			logEndTime(dbLogId);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(cs);
		}
		return newId;
	}

	public int insertComment(ConsumerProfileComment comment, String callerLoginId) throws DataSourceException {
		CallableStatement cs = null;
		Connection conn = null;
		int newId = -1;
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_comments.prc_insrt_cust_comment";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, comment.getCustId());
			cs.setString(++paramIndex, comment.getReasonCode());
			cs.setString(++paramIndex, comment.getText());
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int newIdIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			newId = ((Integer) cs.getObject(newIdIndex)).intValue();
			logEndTime(dbLogId);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(cs);
		}
		return newId;
	}

	/**
	 * @param custId
	 * @param ssnEncrypted
	 * @param callerLoginId
	 *
	 *            Created on Mar 3, 2005
	 */
	public void updateSsn(int custId, String ssnMask, String callerLoginId) throws DataSourceException {
		CallableStatement cs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_update_customer_ssn";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, custId);
			cs.setString(++paramIndex, ssnMask);
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(cs);
		}
		return;
	}

	/**
	 * @param custId
	 * @param ssnEncrypted
	 * @param callerLoginId
	 * @author vy79
	 *
	 *            Created on Oct 24, 2012
	 */
	public void updateCustDocumentStatus(ConsumerProfile cp, String newDocStatus) throws DataSourceException {
		CallableStatement cs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_upd_ident_doc_stat_code";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, cp.getUserId());
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, cp.getId());
			cs.setString(++paramIndex, newDocStatus);
			cs.registerOutParameter(++paramIndex, Types.FLOAT);
			int dbLogIdIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(cs);
		}
		return;
	}

	/**
	 * @param ConsumerEmail
	 *
	 *            Created on May 4, 2005
	 */
	public void updateElectronicEmail(String userId, ConsumerEmail consumerEmail) throws DataSourceException {
		CallableStatement cs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_iu_cust_elec_addr";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, consumerEmail.getConsumerEmail());
			cs.setInt(++paramIndex, consumerEmail.getConsumerId());
			cs.setString(++paramIndex, consumerEmail.getElectronicTypeCode().getTypeCode());
			cs.setString(++paramIndex, consumerEmail.getStatus().getStatusCode());
			cs.setString(++paramIndex, consumerEmail.getEmailTaintCode().getStatusIndicator());
			cs.setString(++paramIndex, consumerEmail.getEmailDomainTaintCode().getStatusIndicator());
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			logEndTime(dbLogId);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(cs);
		}
		return;
	}

	/**
	 * @param Consumer
	 *            Rewards/Loyalty fields
	 *
	 *            Created on July 16, 2009
	 */
	public void updateRewardsInfo(String userId, String dbCallTypeCode, int consumerId, String rewardsNumber, String consumerAutoEnrollFlag)
			throws DataSourceException {
		CallableStatement cs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_update_cust_rewards_info";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setInt(++paramIndex, consumerId);
			cs.setString(++paramIndex, rewardsNumber);
			cs.setString(++paramIndex, consumerAutoEnrollFlag);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			logEndTime(dbLogId);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(cs);
		}
		return;
	}

	public Collection searchConsumerProfile(ConsumerProfileSearchCriteria criteria, String userId, HashMap partnerSites)
			throws DataSourceException, TooManyResultException {
		Collection col = new ArrayList();
		CallableStatement cs = null;
		ResultSet rs = null;
		Connection conn = null;
		ConsumerProfileSearchView view = null;
		int maxCount = dynProps.getMaxDownloadTransactions();
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_get_adhoc_customer_cv";
			// TODO check if parameters are ok
//			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (criteria.getCustFrstName() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustFrstName());
			}

			if (criteria.getCustLastName() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustLastName());
			}

			if (criteria.getCustAddrLine1Text() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustAddrLine1Text());
			}

			if (criteria.getCustAddrCityName() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustAddrCityName());
			}

			if (criteria.getCustAddrStateName() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustAddrStateName());
			}

			if (criteria.getCustAddrPostalCode() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustAddrPostalCode());
			}

			if (criteria.getCustPswdText() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustPswdText());
			}

			if (criteria.getCustLogonId() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustLogonId());
			}

			if (criteria.getCustPhoneNumber() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustPhoneNumber());
			}

			if (criteria.getCustSSNMask() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustSSNMask());
			}

			if (criteria.getCustCCMask() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustCCMask());
			}

			if (criteria.getCustBankMask() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustBankMask());
			}

			if (criteria.getCustStatCode() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustStatCode());
			}

			if (criteria.getCustSubstatCode() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustSubstatCode());
			}

			if (criteria.getCustCreateDate() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
				Date tmpDate;
				try {
					tmpDate = new Date(df.parse(criteria.getCustCreateDate()).getTime());
				} catch (ParseDateException e1) {
					throw new EMGRuntimeException(e1);
				}
				cs.setDate(++paramIndex, tmpDate);
			}

			if (criteria.getCustCreateDateTo() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
				Date tmpDate;
				try {
					tmpDate = new Date(df.parse(criteria.getCustCreateDateTo()).getTime());
				} catch (ParseDateException e1) {
					throw new EMGRuntimeException(e1);
				}
				cs.setDate(++paramIndex, tmpDate);
			}

			if (criteria.getCustCreateIpAddrId() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustCreateIpAddrId());
			}

			if (criteria.getCustPrmrCode() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustPrmrCode());
			}

			if (criteria.getEDirectoryGuid() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getEDirectoryGuid());
			}

			if (StringHelper.isNullOrEmpty(criteria.getCustAdditionalIdType())) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustAdditionalIdType());
			}

			if (StringHelper.isNullOrEmpty(criteria.getCustAdditionalIdEncrypted())) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustAdditionalIdEncrypted());
			}

			if (StringHelper.isNullOrEmpty(criteria.getCustAdditionalIdMask())) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustAdditionalIdMask());
			}

			if (StringHelper.isNullOrEmpty(criteria.getCustAdditionalDocStatus())) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustAdditionalDocStatus());
			}

			if (criteria.getCustScndLastName() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustScndLastName());
			}
			
			if (criteria.getCustMiddleName() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, criteria.getCustMiddleName());
			}
			
			//added by Ankit Bhatt for MBO-128
			if (criteria.getProfileType() == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				System.out.println("criteria.getProfileType() " + criteria.getProfileType());
				cs.setString(++paramIndex, criteria.getProfileType());
			}
			//ended
			
			//Added for MBO-6291 
			if (null != criteria.getCustProgramId()) {
				if (criteria.getCustProgramId().equals(ALL_PARTNER_SITE_ID)) { // iv_src_web_site_bsns_cd
					cs.setNull(++paramIndex, Types.VARCHAR);
				} else {
					cs.setString(++paramIndex, criteria.getCustProgramId());
				}
			} else {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} 
			//ended
			
			//Added for MBO-6516
			if ((null != criteria.getCustCreateDate())&&(null != criteria.getCustCreateDateTo())){
				cs.setInt(++paramIndex, 15000);
			}else{
				cs.setInt(++paramIndex, 100);
			}
			//ended
			
			
			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramConsumerIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			rs = (ResultSet) cs.getObject(paramConsumerIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						"Result set retrieved: " + System.currentTimeMillis());
			}
			DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
			long cnt = 0;
			while (rs.next()) {
				view = new ConsumerProfileSearchView();
				view.setCustId(rs.getString("cust_id"));
				
				if (rs.getString("cust_logon_id") != null) {
					view.setCustLogonId(rs.getString("cust_logon_id"));
				} else {
					view.setCustLogonId(rs.getString("cust_elec_addr_id"));
				}
				
				view.setCustStatCode(rs.getString("cust_stat_code"));
				view.setCustStatDesc(rs.getString("cust_stat_desc"));
				view.setCustSubStatCode(rs.getString("cust_sub_stat_code"));
				view.setCustSubStatDesc(rs.getString("cust_sub_stat_desc"));
				view.setCustFrstName(rs.getString("cust_frst_name"));
				view.setCustLastName(rs.getString("cust_last_name"));
				view.setCustPhoneNbr(rs.getString("cust_ph_nbr"));
				// Added as part of MBO-738 by wx51
				view.setCustAddr(rs.getString("addr_line1_text"));
				view.setCustCity(rs.getString("addr_city_name"));
				view.setCustState(rs.getString("addr_state_name"));
				view.setCustPostal(rs.getString("addr_postal_code"));				
				// Change ended
				view.setCustSSNMask(rs.getString("cust_ssn_mask_nbr"));
				view.setCustBrthDate(df.format(rs.getTimestamp("cust_brth_date")));
				view.setCustCreateDate(df.format(rs.getTimestamp("create_date")));
				view.setCustCreateIpAddrId(rs.getString("create_ip_addr_id"));
				view.setCustPrmrCode(rs.getString("cust_prmr_code"));
				// Added as part of MBO-738 by wx51
				view.setProfileType(rs.getString("profile_type"));
				//Added as a part of MBO-2809
				view.setProfileTypeIndicator(rs.getString("CURR_CUST_PRFL_ADDR_FLAG"));
				if (view.getCustPrmrCode() == null) {
					view.setCustPrmrCode("");
				}
				if (rs.getString("cust_blkd_code").equals("B"))
					view.setTaintedText("Tainted");
				else
					view.setTaintedText("");
				
				String webSiteCode = "";
				if (rs.getString("prfl_create_src_web_site_code") != null) {
					webSiteCode = rs.getString("prfl_create_src_web_site_code");
					if (partnerSites.get(webSiteCode)!= null) {
						webSiteCode = (String)partnerSites.get(webSiteCode);
					}
				}
				view.setCustCreatSrcWebSite(webSiteCode);
				col.add(view);
				cnt++;
				if (cnt > maxCount + 1) {
					break;
				}
			}
			logEndTime(dbLogId, cnt);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						"Result set processed: " + System.currentTimeMillis());
			}
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						"Closing result set: " + System.currentTimeMillis());
			}
			close(rs);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						"Result set closed: " + System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						"Closing statement: " + System.currentTimeMillis());
			}
			close(cs);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						"Statement closed: " + System.currentTimeMillis());
			}
		}
		ConsumerProfileSearchView.sortFieldName = criteria.getSortBy();
		Collections.sort((List) col);
		return col;
	}

	/**
	 * @param custId
	 * @param callerLoginId
	 *
	 *            Created on Mar 28, 2005
	 */
	public void updateLastLoginDate(int custId, String callerLoginId) throws DataSourceException {
		CallableStatement cs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_login_access.prc_update_last_login_date";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, custId);
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(cs);
		}
		return;
	}

	public void addLogonFailure(LogonFailure logonFailure) throws DataSourceException {
		CallableStatement cs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_login_access.prc_insert_logon_failure";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, logonFailure.getUserId());
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, logonFailure.getLogonTrySeq().intValue());
			cs.setString(++paramIndex, logonFailure.isProfileDisabled() ? "1" : "0");
			cs.setString(++paramIndex, logonFailure.isBlockedIp() ? "1" : "0");
			cs.setString(++paramIndex, logonFailure.isBlockedSsn() ? "1" : "0");
			cs.setString(++paramIndex, logonFailure.isBlockedBank() ? "1" : "0");
			cs.setString(++paramIndex, logonFailure.isBlockedCc() ? "1" : "0");
			cs.setString(++paramIndex, logonFailure.isBlockedAba() ? "1" : "0");
			cs.setString(++paramIndex, logonFailure.isBlockedCcBin() ? "1" : "0");
			cs.setString(++paramIndex, logonFailure.isBlockedPhone() ? "1" : "0");
			cs.setString(++paramIndex, logonFailure.isBlockedAltPhone() ? "1" : "0");
			cs.setString(++paramIndex, logonFailure.isBlockedEmail() ? "1" : "0");
			cs.setString(++paramIndex, logonFailure.isBlockedEmailDomain() ? "1" : "0");
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(cs);
		}
	}

	public Collection getConsumerSubStatuses(boolean onlyActive) throws DataSourceException {
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		ArrayList codeDescriptions = new ArrayList();
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_status_and_type.prc_get_cust_sub_status_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, dbCallTypeCode);
			if (onlyActive)
				cs.setString(++paramIndex, "ACT");
			else
				cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			rs = (ResultSet) cs.getObject(cursorIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				CodeDescription cd = new CodeDescription(rs.getString("cust_sub_stat_code"), rs.getString("cust_sub_stat_desc"));
				codeDescriptions.add(cd);
			}
			logEndTime(dbLogId, count);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rs);
			close(cs);
		}
		return codeDescriptions;
	}

	public boolean isCustomerExist(int custId, String callerLoginId) throws DataSourceException {
		CallableStatement cs = null;
		ResultSet rsConsumer = null;
		Connection conn = null;
		boolean isExist = false;
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_get_customer_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, custId);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.registerOutParameter(++paramIndex, OracleTypes.FLOAT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramConsumerIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			rsConsumer = (ResultSet) cs.getObject(paramConsumerIndex);
			if (rsConsumer.next()) {
				isExist = true;
			}
			logEndTime(dbLogId);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rsConsumer);
			close(cs);
		}
		return isExist;
	}

	// used by quick search feature, allows for keying of the eDir guid,
	// searching for it and
	// returning the cust id for full customer retrieval.
	public int getCustIdByEdirGuid(String eDirectoryGuid, String callerLoginId) throws DataSourceException {
		CallableStatement cs = null;
		ResultSet rsConsumer = null;
		Connection conn = null;
		int returnCustId = 0;
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_get_cust_id_by_edir_guid";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, eDirectoryGuid);
			cs.registerOutParameter(++paramIndex, OracleTypes.FLOAT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int paramConsumerIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			returnCustId = cs.getInt(paramConsumerIndex);
			logEndTime(dbLogId);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rsConsumer);
			close(cs);
		}
		return returnCustId;
	}

	// custId passed in, ignore it from the dup list, cannot be a dup of itself.
	public boolean isDuplicateActiveCustomer(int custId, java.util.Date birthDate, String ssnMask, String custLastName, String addrLine1,
			String callerLoginId,String elecAddr,String isoCountryCode) throws DataSourceException {
		CallableStatement cs = null;
		ResultSet rsConsumerDups = null;
		Connection conn = null;
		boolean isDuplicateActiveCustomer = false;
		String includeGuestProfiles="Y";
		try {
			conn = getConnection();
			String storedProcName = "pkg_mgo_cust_profile.prc_get_active_cust_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setDate(++paramIndex, new java.sql.Date(birthDate.getTime()));
			cs.setString(++paramIndex, custLastName);
			cs.setString(++paramIndex, addrLine1);
			cs.setNull(++paramIndex, Types.VARCHAR);  // postal code
			cs.setString(++paramIndex, elecAddr); // electronic address
			cs.setString(++paramIndex, isoCountryCode); //     iv_addr_cntry_id 
			cs.setString(++paramIndex, includeGuestProfiles); // guestProfile flag
			cs.setNull(++paramIndex, Types.VARCHAR); //phone number
			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramConsumerIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			rsConsumerDups = (ResultSet) cs.getObject(paramConsumerIndex);
			if (rsConsumerDups.next()) {
				int dupCustId = rsConsumerDups.getInt("cust_id");
				if (dupCustId != custId)
					isDuplicateActiveCustomer = true;
			}
			logEndTime(dbLogId);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rsConsumerDups);
			close(cs);
		}
		return isDuplicateActiveCustomer;
	}

	public List getCustPremierTypes(String callerLoginId) throws DataSourceException {
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		ArrayList prmrTypes = new ArrayList();
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_status_and_type.prc_get_premier_cust_type_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			long count = 0;
			rs = (ResultSet) cs.getObject(cursorIndex);
			while (rs.next()) {
				count++;
				CustPremierType cpt = new CustPremierType(rs.getString("cust_prmr_code"), rs.getString("cust_prmr_desc"));
				prmrTypes.add(cpt);
			}
			logEndTime(dbLogId, count);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rs);
			close(cs);
		}
		return prmrTypes;
	}

	/*
	 * PACKAGE : pkg_em_status_and_type PROCEDURE prc_get_string_purpose_type_cv
	 * (iv_user_id IN customer.cust_logon_id%TYPE, iv_call_type_code IN
	 * process_log_a.call_type_code%TYPE, iv_strng_prps_code IN
	 * string_purpose_type.strng_prps_code%TYPE, ov_prcs_log_id OUT
	 * process_log_a.prcs_log_id%TYPE, ov_string_purpose_type_cv OUT
	 * string_purpose_type_cv_type );
	 */
	public List getPurposeTypes(String callerLoginId) throws DataSourceException {
		ArrayList purposeTypes = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		// DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
		String storedProcName = "pkg_em_status_and_type.prc_get_string_purpose_type_cv";
		StringBuilder sqlBuffer = new StringBuilder(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");
		try {
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());
			int paramIndex = 0;
			// input parameters
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setNull(++paramIndex, Types.VARCHAR);
			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;
			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);
			// int cnt = 0;
			while (rs.next()) {
				PurposeType type = new PurposeType();
				type.setPurposeTypeCode(rs.getString("STRNG_PRPS_CODE"));
				type.setPurposeTypeDesc(rs.getString("STRNG_PRPS_DESC"));
				purposeTypes.add(type);
			}
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			if (rs != null)
				close(rs);
			if (cs != null)
				close(cs);
		}
		return purposeTypes;
	}

	/**
	 * @param custId
	 * @param blockCode
	 * @param callerLoginId
	 */
	public void updateCustBlkdCode(int custId, String blockCode, String callerLoginId) throws DataSourceException {
		CallableStatement cs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_update_cust_blkd_code ";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, custId);
			cs.setString(++paramIndex, blockCode);
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(cs);
		}
		return;
	}
	
	public List<CustomerAuthenticationInfo> getCustomerAuthenticationInformation(int custId) throws DataSourceException{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		List<CustomerAuthenticationInfo> customerAuthenticationInfoList = new ArrayList<CustomerAuthenticationInfo>();
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_customer_profile.prc_get_cust_ident_vrfy_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?) }");
			int paramIndex = 0;
//			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, custId);
			cs.registerOutParameter(++paramIndex, Types.FLOAT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
		
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			rs = (ResultSet) cs.getObject(cursorIndex);

//			if(detail){
//				List<CustomerAuthenticationInfo> validateCustomerInfoList = (ArrayList<CustomerAuthenticationInfo>)populateValidateCustomerInfoList(rs);
////				result.setValidateCustomerInfo(validateCustomerInfoList);
//			} else {
			
				if (rs != null) {
					while(rs.next()) {
						CustomerAuthenticationInfo customerAuthenticationInfo=new CustomerAuthenticationInfo();
						customerAuthenticationInfo.setCustId(rs.getLong("cust_id"));
						customerAuthenticationInfo.setAuthnId(rs.getString("authn_id"));
						customerAuthenticationInfo.setAuthnDate(rs.getDate("create_date"));
						customerAuthenticationInfo.setScoreNbr(rs.getString("score_nbr"));
						customerAuthenticationInfo.setDeciBandText(rs.getString("deci_band_text"));
						if(rs.getTimestamp("create_date")!=null){
							SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm a");
							String formattedDate = formatter.format(rs.getTimestamp("create_date"));
							customerAuthenticationInfo.setAuthDateDisplay(formattedDate);
						}
						customerAuthenticationInfoList.add(customerAuthenticationInfo);
						
//						result.setRemarksText(rs.getString("remarks_text"));
//						result.setStatText(rs.getString("stat_text"));
//						result.setChkTypeCode(rs.getString("chk_type_code"));
//						result.setChkTypeDesc(rs.getString("chk_type_desc"));
//						result.setRsltCode(rs.getString("rslt_code"));
//						result.setRsltDesc(rs.getString("rslt_desc"));
//						result.setRsltSvrtyText(rs.getString("rslt_svrty_text"));
						//break;
					}
//				} 
			}
			logEndTime(dbLogId, 1);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rs);
			close(cs);
		}
		return customerAuthenticationInfoList;
	}
	
	private ArrayList<CustomerAuthenticationInfo> populateValidateCustomerInfoList(ResultSet rs) throws SQLException {
		ArrayList<CustomerAuthenticationInfo> result = new ArrayList<CustomerAuthenticationInfo>();
		if (rs != null) {
			while (rs.next()) {
				CustomerAuthenticationInfo listItem= new CustomerAuthenticationInfo();
				listItem.setCustId(rs.getLong("cust_id"));
				listItem.setAuthnId(rs.getString("authn_id"));
				listItem.setAuthnDate(rs.getDate("authn_date"));
				listItem.setScoreNbr(rs.getString("score_nbr"));
				listItem.setDeciBandText(rs.getString("deci_band_text"));
//				listItem.setRemarksText(rs.getString("remarks_text"));
//				listItem.setStatText(rs.getString("stat_text"));
//				listItem.setChkTypeCode(rs.getString("chk_type_code"));
//				listItem.setChkTypeDesc(rs.getString("chk_type_desc"));
//				listItem.setRsltCode(rs.getString("rslt_code"));
//				listItem.setRsltDesc(rs.getString("rslt_desc"));
//				listItem.setRsltSvrtyText(rs.getString("rslt_svrty_text"));

				result.add(listItem);
			}
		}
		return result;
	}

	/*
	 * Method to fetch lexis nexis activity log
	 * 
	 * @param SearchCriteriaDTO throws DataSourceException
	 */
	public List<LexisNexisActivity> getLexisNexisActivityLogs(
			SearchCriteriaDTO searchDTO) throws DataSourceException {
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		List<LexisNexisActivity> activityList = new ArrayList<LexisNexisActivity>();
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_cust_frd_dtct.prc_get_cust_srch_api_resp_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, searchDTO.getLogonId());
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, searchDTO.getCustId());
			cs.registerOutParameter(++paramIndex, Types.FLOAT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.debug(
								storedProcName
										+ " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.debug(
								storedProcName
										+ " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.debug("dbLogId = " + dbLogId);
			}
			rs = (ResultSet) cs.getObject(cursorIndex);
			if (rs != null) {
				boolean bpsFound = false;
				LexisNexisActivity activity = new LexisNexisActivity();
				while (rs.next()) {
					if (rs.getInt("cust_srch_type_code") !=2 && !bpsFound) {
						activity.setId(rs.getInt("cust_srch_api_resp_id"));
						activity.setActivityDate(rs.getTimestamp("create_date"));
						activity.setUniqueId(rs.getString("svc_prvdr_cust_id"));
						activity.setSsnIndicator(String.valueOf(rs.getInt("ssn_match_rslt_type_code")));
						activity.setSsnValidityIndCode(String.valueOf(rs
								.getString("ssn_valid_indctr_bsns_code")));
						activity.setSsnValidityIndDesc(rs
								.getString("ssn_valid_indctr_bsns_desc"));
						activity.setSearchType(String.valueOf(rs.getInt("cust_srch_type_code")));
						bpsFound = true;
					}
					if(null!=rs.getString("high_risk_indctr_bsns_code") && rs.getInt("cust_srch_type_code") == 1){
						RiskIndicator riskIndicator = new RiskIndicator();
						riskIndicator.setCode(rs
								.getString("high_risk_indctr_bsns_code"));
						riskIndicator.setDescription(rs
								.getString("high_risk_indctr_bsns_desc"));
						activity.getHighRiskIndicator().add(riskIndicator);
					}
					if(rs.getInt("cust_srch_type_code") == 2){
						LexisNexisActivity relativeActivity = new LexisNexisActivity();
						relativeActivity.setId(rs.getInt("cust_srch_api_resp_id"));
						relativeActivity.setActivityDate(rs.getTimestamp("create_date"));
						relativeActivity.setSearchType(String.valueOf(rs.getInt("cust_srch_type_code")));
						relativeActivity.setUniqueId(rs.getString("svc_prvdr_cust_id"));
						activityList.add(relativeActivity);
					}
				}
				if(null != activity && null != activity.getActivityDate())
					activityList.add(activity);
				
			}
			logEndTime(dbLogId, 1);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rs);
			close(cs);
		}
		return activityList;
	}
	
	public void insertLexisNexisActivityLog(ConsumerProfile consumerProfile,LexisNexisActivity activity) throws DataSourceException {
		CallableStatement cs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_cust_frd_dtct.prc_add_cust_srch_api_resp";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, consumerProfile.getUserId());
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setNull(++paramIndex,OracleTypes.FLOAT);
			cs.setInt(++paramIndex,activity.getCustId());
			cs.setString(++paramIndex,activity.getSearchType());
			cs.setString(++paramIndex,activity.getUniqueId()); //Unique customer Id
			cs.setString(++paramIndex,activity.getSsnValidityIndCode());  //SSN Validity Indicator
			if(null != activity.getSsnIndicator())
				cs.setInt(++paramIndex,SSNIndicatorEnum.getIdValueByCode(activity.getSsnIndicator())); //SSN Indicator (Flag)
			else
				cs.setNull(++paramIndex,Types.INTEGER);
			cs.setString(++paramIndex,activity.getResponseXML());
			cs.setString(++paramIndex,activity.getRiskIndicator());
			//output parameters
			cs.registerOutParameter(++paramIndex, OracleTypes.FLOAT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, Types.FLOAT);
			//int newIdIndex = paramIndex;
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).info("Input Params for proc in insertLexNex " 
							+ " \nuser id : "+consumerProfile.getUserId() 
							+ " \ncust id : "+ activity.getCustId() 
							+ " \nSearchType : "+ activity.getSearchType() 
							+ " \nUniqueId : "+ activity.getUniqueId() 
							+ " \nSsnValidityIndCode : "+activity.getSsnValidityIndCode()
							+ " \nSsnIndicator : "+activity.getSsnIndicator()
							+ " \nXML Response : \n"+activity.getResponseXML()
							+ " \nRiskIndicator : "+activity.getRiskIndicator()
							+ "\nconsumerprofileId : "+consumerProfile.getId()); // can be removed in later releases after R36
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			logEndTime(dbLogId);
		} catch (SQLException e) {
			logger.error("SQL Error occurred during BPS/RLT Search insertion:"
					+ e.getMessage());
			if (e.getSQLState().equals(UNIQUE_CONSTRAINT_VIOLATED)) {
				retryInsertLexNex(consumerProfile, activity);
			} else {
				throw new DataSourceException(e);
			}
		} catch (Exception e) {
				logger.error("Error occurred during BPS/RLT Search insertion:"
						+ e.getMessage());
		} finally {
			close(cs);
		}
	}
	
	public void retryInsertLexNex(ConsumerProfile consumerProfile,
			LexisNexisActivity activity) throws DataSourceException {
		CallableStatement cs = null;
		Connection conn = null;
		SearchCriteriaDTO searchDTO = new SearchCriteriaDTO();
		int searchId = 0;
		searchDTO.setLogonId(consumerProfile.getUserId());
		searchDTO.setCustId(activity.getCustId());
		List<LexisNexisActivity> listActivity = getLexisNexisActivityLogs(searchDTO);
		if (null != listActivity) {
			searchId = listActivity.get(0).getId();
		}
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_cust_frd_dtct.prc_add_cust_srch_api_resp";
			cs = conn.prepareCall("{ call " + storedProcName
					+ "(?,?,?,?,?,?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, consumerProfile.getUserId());
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, searchId); // pass iv_cust_srch_api_resp_id to update the value
			cs.setInt(++paramIndex, activity.getCustId());
			cs.setString(++paramIndex, activity.getSearchType());
			cs.setString(++paramIndex, activity.getUniqueId()); // Unique customer Id
			cs.setString(++paramIndex, activity.getSsnValidityIndCode()); // SSN Validity Indicator
			if (null != activity.getSsnIndicator())
				cs.setInt(++paramIndex, SSNIndicatorEnum
						.getIdValueByCode(activity.getSsnIndicator())); // SSN Indicator (Flag)
			else
				cs.setNull(++paramIndex, Types.INTEGER);
			cs.setString(++paramIndex, activity.getResponseXML());
			cs.setString(++paramIndex, activity.getRiskIndicator());
			cs.registerOutParameter(++paramIndex, OracleTypes.FLOAT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, Types.FLOAT);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.info("Input Params for proc in retryInsertLexNex "
							+ " \nuser id : " + consumerProfile.getUserId()
							+ "\nsearchId : " + searchId
							+ " \ncust id : " + activity.getCustId()
							+ " \nSearchType : " + activity.getSearchType()
							+ " \nUniqueId : " + activity.getUniqueId()
							+ " \nSsnValidityIndCode : " + activity.getSsnValidityIndCode()
							+ " \nSsnIndicator : " + activity.getSsnIndicator()
							+ " \nXML Response : \n" + activity.getResponseXML() 
							+ " \nRiskIndicator : " + activity.getRiskIndicator()); // can be removed in later releases after R36
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.debug(storedProcName
								+ " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.debug(storedProcName
								+ " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.debug("dbLogId = " + dbLogId);
			}
			logEndTime(dbLogId);
		} catch (SQLException e) {
			logger.error("Error occurred during BPS/RLT Search retryInsertLexNex:"
					+ e.getMessage());
			throw new DataSourceException(e);
		} finally {
			close(cs);
		}

	}
	
	/**
	 * 
	 * returns the BPS response returned from CVS stored as string in DB 
	 * @param searchId
	 * @param userId
	 * @return
	 * @throws DataSourceException
	 */
	public String getBPSResponse(Long searchId,String userId) throws DataSourceException{
		String BPSResponse = "";
		ResultSet resultSet = null;
		CallableStatement callableStatement = null;
		Connection connection = null;
		try {
			connection = getConnection();
			String storedProcName = "pkg_em_cust_frd_dtct.prc_get_cust_api_resp_xml_cv";
			callableStatement = connection.prepareCall("{ call " + storedProcName + "(?,?,?,?,?) }");
			int paramIndex = 0;
			callableStatement.setString(++paramIndex, userId);
			callableStatement.setString(++paramIndex, dbCallTypeCode);
			callableStatement.setLong(++paramIndex,searchId);
			callableStatement.registerOutParameter(++paramIndex, Types.FLOAT);
			int dbLogIdIndex = paramIndex;
			callableStatement.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).info("Input Params for proc in getBPSSearch "
					+"\n searchId : "+searchId);// can be removed later
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
		
			callableStatement.execute();
			long dbLogId = callableStatement.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			resultSet = (ResultSet) callableStatement.getObject(cursorIndex);
			if (resultSet != null) {
				while(resultSet.next()){
					BPSResponse = resultSet.getString("resp_xml_text");
				}
			}			
		} catch (SQLException e) {
			logger.error("handling getBPSSearchResults failed "
					+ e.getMessage());
			throw new DataSourceException(e);
		} finally {
			close(resultSet);
			close(callableStatement);
		}	
		return BPSResponse;
	}
	
	// 
	/**
	 * MBO - 3150
	 * 
	 * @param custId
	 * return number of days since the current profile address of customer got changed.
	 */
	public int getNumberOfDaysSinceCurrentAddressChanged(int custId) throws DataSourceException{
		int noOfDaysSinceAddressLastChanged = 0;
		ResultSet resultSet = null;
		CallableStatement callableStatement = null;
		Connection connection = null;
		final String userId = "EMT";
		connection = getConnection();
		String storedProcName = "pkg_em_customer_profile.prc_get_days_last_prf_addr_chg";
		try {
			callableStatement = connection.prepareCall("{ call " + storedProcName + "(?,?,?,?,?) }");
		
		int paramIndex = 0;
		callableStatement.setString(++paramIndex, userId);
		callableStatement.setString(++paramIndex, dbCallTypeCode);
		callableStatement.setInt(++paramIndex,custId);
		callableStatement.registerOutParameter(++paramIndex, Types.FLOAT);
		int dbLogIdIndex = paramIndex;
		callableStatement.registerOutParameter(++paramIndex, Types.INTEGER);
		int cursorIndex = paramIndex;
		
		if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
					storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
		}
	
		callableStatement.execute();
		long dbLogId = callableStatement.getLong(dbLogIdIndex);
		if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
					storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
		}
		noOfDaysSinceAddressLastChanged =  callableStatement.getInt(cursorIndex);
		
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}finally {
			close(resultSet);
			close(callableStatement);
		}
		return noOfDaysSinceAddressLastChanged;
	}
	
	/**
	 * MBO - 5468
	 * 
	 * @param consumerId
	 * returns the list of documents that have been uploaded for the particular consumerId.
	 * @param callerLoginId 
	 */
	public List<ConsumerDocument> getConsumerDocumentList(int consumerId, String callerLoginId)
			throws DataSourceException {
		ResultSet resultSet = null;
		CallableStatement callableStatement = null;
		Connection connection = null;
		connection = getConnection();
		String storedProcName = "pkg_em_customer_profile.prc_get_cust_upld_doc_cv";
		List<ConsumerDocument> consumerdocumentList = new ArrayList<ConsumerDocument>();
		try {
			callableStatement = connection.prepareCall("{ call "
					+ storedProcName + "(?,?,?,?,?) }");

			int paramIndex = 0;
			callableStatement.setString(++paramIndex, callerLoginId);
			callableStatement.setString(++paramIndex, dbCallTypeCode);
			callableStatement.setInt(++paramIndex, consumerId);
			callableStatement.registerOutParameter(++paramIndex, Types.FLOAT);
			int dbLogIdIndex = paramIndex;
			callableStatement.registerOutParameter(++paramIndex,
					OracleTypes.CURSOR);
			int cursorIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.debug(
								storedProcName
										+ " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			callableStatement.execute();
			long dbLogId = callableStatement.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.debug(
								storedProcName
										+ " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.debug("dbLogId = " + dbLogId);
			}
			resultSet = (ResultSet) callableStatement.getObject(cursorIndex);
			if (null!= resultSet) {
				while (resultSet.next()) {
					ConsumerDocument docDet = new ConsumerDocument();
					docDet.setCustomerId(resultSet.getLong("CUST_ID"));
					// MBO-7890
					Timestamp ts = resultSet.getTimestamp("CREATE_DATE");
					if (null != ts) {
						docDet.setDocCreatedTime(new java.util.Date(ts
								.getTime()));
					}
					docDet.setDocImgFileName(resultSet
							.getString("DOC_IMG_FILE_NAME"));
					docDet.setDocImgId(resultSet.getInt("CUST_UPLD_DOC_IMG_ID"));
					docDet.setDocTypeCode(DocumentType.getCodeByName(resultSet
							.getString("CUST_DOC_TYPE_BSNS_CODE")));
					consumerdocumentList.add(docDet);
				}
			}
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(resultSet);
			close(callableStatement);
		}
		return consumerdocumentList;
	}

	/**
	 * MBO - 5468
	 * 
	 * @param custDocId
	 * returns the image that has been uploaded corresponding to the image id that has been passed.
	 */
	public ConsumerDocument getDoc(int custDocId) throws DataSourceException {
		ResultSet resultSet = null;
		CallableStatement callableStatement = null;
		Connection connection = null;
		final String userId = "EMT";
		connection = getConnection();
		Blob img = null;
		String storedProcName = "pkg_em_customer_profile.prc_get_cust_doc_img_cv";
		ConsumerDocument consumerDocument = new ConsumerDocument();
		try {
			callableStatement = connection.prepareCall("{ call "
					+ storedProcName + "(?,?,?,?,?) }");

			int paramIndex = 0;
			callableStatement.setString(++paramIndex, userId);
			callableStatement.setString(++paramIndex, dbCallTypeCode);
			callableStatement.setInt(++paramIndex, custDocId);
			callableStatement.registerOutParameter(++paramIndex, Types.FLOAT);
			int dbLogIdIndex = paramIndex;
			callableStatement.registerOutParameter(++paramIndex,
					OracleTypes.CURSOR);
			int cursorIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.debug(
								storedProcName
										+ " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			callableStatement.execute();
			long dbLogId = callableStatement.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.debug(
								storedProcName
										+ " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.debug("dbLogId = " + dbLogId);
			}
			resultSet = (ResultSet) callableStatement.getObject(cursorIndex);
			if (null != resultSet) {
				while (resultSet.next()) {
					img = resultSet.getBlob("DOC_IMG_BLOB");
					consumerDocument.setDocImgBlob(img.getBytes(1,
							(int) img.length()));
				}
			}
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(resultSet);
			close(callableStatement);
		}
		return consumerDocument;
	}
	
	/**
	 * MBO - 6161
	 * 
	 * @param callerLoginId
	 *            returns the list of resident statuses to which we are
	 *            appending "Not specified" option.
	 */
	public List getResdentStatTypes(String callerLoginId)
			throws DataSourceException {
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		ArrayList resdCodes = new ArrayList();
		try {
			conn = getConnection();
			String storedProcName = "pkg_em_status_and_type.prc_get_resdnc_stat_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.debug(
								storedProcName + " started: "
										+ System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			long count = 0;
			rs = (ResultSet) cs.getObject(cursorIndex);
			ResidentStatusCode resdNS = new ResidentStatusCode(0,
					"Not Specified");
			resdCodes.add(resdNS);
			while (rs.next()) {
				count++;
				ResidentStatusCode resd = new ResidentStatusCode(
						rs.getInt("resdnc_stat_code"),
						rs.getString("resdnc_stat_bsns_code"));
				resdCodes.add(resd);
			}
			logEndTime(dbLogId, count);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rs);
			close(cs);
		}
		return resdCodes;
	}
	
	/**
	 * CAN-56
	 * 
	 * @param CountryCode
	 *             returns the list of States/Provinces from DB.
	 */
	public Map<String, String> getStatesProvinces(String countryCode) throws DataSourceException{
		
		Map<String, String> statesProvincesList = new HashMap<String, String>();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection connection = null;
		connection = getConnection();
		String storedProcName = "pkg_em_biller_and_country.prc_get_cntry_subdiv";
		
		try {
			cs = connection.prepareCall("{ call "
					+ storedProcName + "(?,?,?,?,?) }");
			
			int paramIndex = 0;
			cs.setString(++paramIndex, USER_ID);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, countryCode);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			cs.registerOutParameter(++paramIndex, Types.FLOAT);
			int dbLogIdIndex = paramIndex-1;
			int cursorIndex = paramIndex;
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(cursorIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
						storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("dbLogId = " + dbLogId);
			}
			rs = (ResultSet) cs.getObject(dbLogIdIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				String subDivCode = rs.getString("ISO_SUBDIV_CODE");
				String subDivCodeName = rs.getString("ISO_SUBDIV_CODE_NAME");
				statesProvincesList.put(subDivCode, subDivCodeName);
			}
			logEndTime(dbLogId, count);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rs);
			close(cs);
		}
		return statesProvincesList;
	}
	}