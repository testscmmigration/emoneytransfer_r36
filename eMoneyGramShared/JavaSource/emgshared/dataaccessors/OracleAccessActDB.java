package emgshared.dataaccessors;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import emgshared.exceptions.DataSourceException;

public class OracleAccessActDB {
	public static final String E_MONEYGRAM_RESOURCE_ACT_REF = "java:comp/env/jdbc/eMoneyGramACTDB";
	private static final Logger LOGGER = EMGSharedLogger
			.getLogger(OracleAccessActDB.class.getName().toString());
	private static DataSource dataSource;
	private static InitialContext initialContext;
	private static Connection connection;
	private static Statement statement;
	private static ResultSet resultSet;

	static {
		dataSource = getDataSource(E_MONEYGRAM_RESOURCE_ACT_REF);
	}

	private OracleAccessActDB() {
	}

	public static Connection getConnection() throws DataSourceException {
		Connection conn = null;

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("In JndiDataSourceHelper: getting MGT connection from jndi datasource: "
					+ E_MONEYGRAM_RESOURCE_ACT_REF + "...");
		}
		try {
			conn = dataSource.getConnection();
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}

		return conn;
	}

	public static DataSource getDataSource() {
		return dataSource;
	}

	private static DataSource getDataSource(String dataSourceName) {
		DataSource ds = null;
		try {

			if (null == initialContext) {
				initialContext = new InitialContext();
			}
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Getting Connection: " + dataSourceName + "...");
				LOGGER.debug(Thread.currentThread().getName() + ": "
						+ "Getting Connection: " + dataSourceName + "...");
			}
			ds = (DataSource) initialContext.lookup(dataSourceName);
		} catch (NamingException ne) {
			LOGGER.error(
					Thread.currentThread().getName()
							+ ": "
							+ "JndiDataSourceHelper: getConnection: NamingException - ",
					ne);
		} catch (Exception e) {

			LOGGER.error("Exception in JndiDataSource: " + e.getMessage());
			LOGGER.error(Thread.currentThread().getName() + ": "
					+ "JndiDataSourceHelper: getConnection: Exception: ", e);
		}
		return ds;
	}

	public static void close(Connection conn) throws DataSourceException {
		connection = conn;
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException ignore) {
				LOGGER.error(OracleAccessActDB.class.getName(), ignore);
				throw new DataSourceException(ignore);
			} finally {
				connection = null;
			}
		}
	}

	public static void close(Statement stmt) throws DataSourceException {
		statement = stmt;
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException ignore) {
				LOGGER.error(E_MONEYGRAM_RESOURCE_ACT_REF
						+ " - Error: Close Statement failed - '"
						+ ignore.getMessage() + "'");
				throw new DataSourceException(ignore);
			} finally {
				statement = null;
			}
		}
	}

	public static void close(ResultSet rs) throws DataSourceException {
		resultSet = rs;
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException ignore) {
				LOGGER.error(E_MONEYGRAM_RESOURCE_ACT_REF
						+ " - Error: Close ResultSet failed - '"
						+ ignore.getMessage() + "'");
				throw new DataSourceException(ignore);
			} finally {
				resultSet = null;
			}
		}
	}

}