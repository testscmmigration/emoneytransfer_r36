package emgshared.cache;

import java.io.Serializable;

import com.moneygram.common.cache.Cache;
import com.moneygram.common.cache.CacheElement;
import com.moneygram.common.cache.CacheException;
import com.moneygram.common.cache.CacheManager;
import com.moneygram.common.cache.CacheManagerFactory;

@SuppressWarnings("serial")
public class EMTCacheElement implements Serializable {

	/**
	 * Returns the cacheElement value (usually a Map)
	 */
	public Serializable getCacheElementValue(String cacheName, Serializable key)
			throws CacheException {
		Serializable result = null;
		try {
			CacheManager cacheManager = CacheManagerFactory
					.getCacheManagerInstance();
			Cache cache = cacheManager.getCache(cacheName);
			if (cache != null) {
				CacheElement cacheElement = cache.getElement(key);
				if (cacheElement != null) {
					result = cacheElement.getValue();
				}
			}
		} catch (Exception ex) {
			throw new CacheException("Error retreiving element from cache: "
					+ ex.getMessage());
		}
		return result;
	}

}
