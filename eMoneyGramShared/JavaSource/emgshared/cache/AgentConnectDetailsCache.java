package emgshared.cache;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;

import shared.mgo.dto.MGOAgentProfile;
import shared.mgo.dto.MGOPaymentOption;
import shared.mgo.dto.MGOProduct;

import com.moneygram.common.cache.Cache;
import com.moneygram.common.cache.CacheElement;
import com.moneygram.common.cache.CacheElementFactory;
import com.moneygram.common.cache.CacheException;
import com.moneygram.common.cache.CacheManager;
import com.moneygram.common.cache.CacheManagerFactory;


import com.moneygram.mgo.service.config_v3_4.AgentInfo;
import com.moneygram.mgo.service.config_v3_4.GetSourceSiteInfoResponse;
import com.moneygram.mgo.service.config_v3_4.MGOProductType;
import com.moneygram.mgo.service.config_v3_4.PaymentInfo;

import emgshared.cache.locator.EMTSharedSourceSiteCacheService;
import emgshared.cache.locator.EMTSharedSourceSiteCacheServiceImpl;
import emgshared.dataaccessors.EMGSharedLogger;

public class AgentConnectDetailsCache implements CacheElementFactory {
	private static final Logger LOG =EMGSharedLogger.getLogger(
			AgentConnectDetailsCache.class);	
	
	Map<String,Map> agentInfoMap = new HashMap<String,Map>();
	
	public Map<String,Map> initAgentConnectCache(String sourceSiteId) throws Exception {		
		MGOAgentProfile agentProfile = null;
		LOG.info("Going to init agent connect cache for source site " + ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTML(sourceSiteId)));
		EMTSharedSourceSiteCacheService cacheService = new EMTSharedSourceSiteCacheServiceImpl();		
		GetSourceSiteInfoResponse response = cacheService.sourceSiteDetails(sourceSiteId);
		if(null!=response && null!=response.getCountryTransactionTypeInfo() && 
				response.getCountryTransactionTypeInfo().length >0 ) {
			Map<String,MGOAgentProfile> productTypeACMap = new HashMap<String,MGOAgentProfile>();
			LOG.info("Sourcesite response got and will be processed now");
			for(int i=0;i<response.getCountryTransactionTypeInfo().length;i++) {
				MGOProductType mgoProductType = response.getCountryTransactionTypeInfo()[i].getTransactionType();
				AgentInfo agentInfo = response.getCountryTransactionTypeInfo()[i].getAgentInfo();
				PaymentInfo paymentInfo = response.getCountryTransactionTypeInfo()[i].getPaymentInfo();
				String scoreFlag = response.getCountryTransactionTypeInfo()[i].getScoreFlag();
				String scoreMerchantId = response.getCountryTransactionTypeInfo()[i].getScoringMerchantId();
				
				agentProfile = new MGOAgentProfile();
				agentProfile.setAgentId(agentInfo.getAgentId());
				agentProfile.setAgentSequence(agentInfo.getSeqNbr());
				agentProfile.setToken(agentInfo.getToken());
				agentProfile.setMGOAgentIdentifier(sourceSiteId); //need to check
				agentProfile.setProfileId(null!=agentInfo.getUnitProfileId() && !agentInfo.getUnitProfileId().isEmpty() ?
								Integer.valueOf(agentInfo.getUnitProfileId()) : 0);
				
				System.out.println("mgoProductType.getValue() " + mgoProductType.getValue());
				System.out.println("agent Id " + agentProfile.getAgentId());
				System.out.println("agent Seq " + agentProfile.getAgentSequence());
				System.out.println("agent Token " + agentProfile.getToken());
				System.out.println("agentInfo.getUnitProfileId() " + agentInfo.getUnitProfileId());
				
				MGOProduct product = new MGOProduct();
				product.setProductName(mgoProductType.getValue());
				product.setEnabled(true);
				
				MGOPaymentOption paymentOption = new MGOPaymentOption();
				paymentOption.setGlobalCollectMerchantId(paymentInfo.getPaymentMerchantId());
				paymentOption.setPaymentOptionName(paymentInfo.getProviderName());
				List<MGOPaymentOption> lstPaymentOptions = new ArrayList<MGOPaymentOption>();
				lstPaymentOptions.add(paymentOption);
				product.setMgoPaymentOptions(lstPaymentOptions);
				agentProfile.setMgoProduct(product);
				
				//This map will contain Product Type wise AC objects
				//For Example, EPSEND, DSSEND, MGSEND will be keys
				productTypeACMap.put(mgoProductType.getValue(), agentProfile);				
			}
			//This Map will have all the Product Types wise Map
			//for given source site Id. 
			agentInfoMap.put(sourceSiteId,productTypeACMap);
			this.createElement(sourceSiteId);
		}
		//LOG.info("AC initialized and returning map is " + agentInfoMap);
		return agentInfoMap;
	}
	public CacheElement createElement(Serializable key) throws Exception {
		try {
			LOG.info("going to create cache element of AC details for " + ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTML(key.toString())));
			CacheManager cacheManager = CacheManagerFactory
					.getCacheManagerInstance();
			Cache cache = cacheManager
					.getCache(CacheConstants.AGENT_CONNECT_DETAILS);
			CacheElement cacheElement = null;
			cacheElement = new CacheElement(key, (Serializable) agentInfoMap);
			cache.putElement(cacheElement);

			return cacheElement;
		} catch (Exception e) {
			throw new CacheException(e);
		}
	}

}
