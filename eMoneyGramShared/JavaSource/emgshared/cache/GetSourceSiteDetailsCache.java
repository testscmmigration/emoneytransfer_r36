package emgshared.cache;

import java.io.Serializable;


import java.net.URL;

import com.moneygram.common.cache.Cache;
import com.moneygram.common.cache.CacheElement;
import com.moneygram.common.cache.CacheElementFactory;
import com.moneygram.common.cache.CacheException;
import com.moneygram.common.cache.CacheManager;
import com.moneygram.common.cache.CacheManagerFactory;
import com.moneygram.mgo.common_v1.ProcessingInstruction;
import com.moneygram.mgo.common_v1.RequestHeader;
import com.moneygram.mgo.service.config_v3_4.GetSourceSiteInfoRequest;
import com.moneygram.mgo.service.config_v3_4.GetSourceSiteInfoResponse;
import com.moneygram.mgo.service.config_v3_4.MGOConfigService_v3_4SoapBindingStub;
import com.moneygram.mgo.service.config_v3_4.ServiceAction;
import org.apache.log4j.Logger;

import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.property.EMTSharedContainerProperties;

public class GetSourceSiteDetailsCache implements CacheElementFactory {
	private static final Logger LOG = EMGSharedLogger.getLogger(
			GetSourceSiteDetailsCache.class);
	GetSourceSiteInfoResponse response = new GetSourceSiteInfoResponse();

	public GetSourceSiteInfoResponse initCacheSourceSiteId(String sourceSite)
			throws Exception {
		try {
			GetSourceSiteInfoRequest request = new GetSourceSiteInfoRequest();
			request.setSourceSite(sourceSite);
			RequestHeader header = new RequestHeader();

			ProcessingInstruction processingInstruction = new ProcessingInstruction();
			processingInstruction.setAction(ServiceAction._getSourceSiteInfo_v3_4);
			processingInstruction.setReadOnlyFlag(false);

			header.setProcessingInstruction(processingInstruction);
			request.setHeader(header);
			response = new MGOConfigService_v3_4SoapBindingStub(new URL(
					EMTSharedContainerProperties.getConfigServiceUrl()), null)
					.getSourceSiteInfo(request);
			this.createElement(sourceSite);

		} catch (Exception e) {
			LOG.error("Exception in initCacheSourceSiteId", e);
			throw e;
		}

		return response;

	}

	public CacheElement createElement(Serializable key) throws Exception {
		try {
			CacheManager cacheManager = CacheManagerFactory
					.getCacheManagerInstance();
			Cache cache = cacheManager
					.getCache(CacheConstants.SOURCE_SITE_DETAILS);
			CacheElement cacheElement = null;
			cacheElement = new CacheElement(key, (Serializable) response);
			cache.putElement(cacheElement);

			return cacheElement;
		} catch (Exception e) {
			throw new CacheException(e);
		}
	}

}
