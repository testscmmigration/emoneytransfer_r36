package emgshared.cache.locator;

import java.util.Collection;


import shared.mgo.dto.MGOAgentProfile;

import com.moneygram.mgo.service.config_v3_4.GetSourceSiteInfoResponse;

public interface EMTSharedSourceSiteCacheService {
	/*public Collection sourceSiteId(String status) throws Exception;*/

	public GetSourceSiteInfoResponse sourceSiteDetails(String sourceSite)
			throws Exception;
	
	//MBO-6101
	public MGOAgentProfile getACDetails(String sourceSiteId, String tranType) throws Exception;
	//ended
}
