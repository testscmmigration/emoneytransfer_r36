package emgshared.cache.locator;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import shared.mgo.dto.MGOAgentProfile;


import com.moneygram.mgo.service.config_v3_4.GetSourceSiteInfoResponse;

import emgshared.cache.AgentConnectDetailsCache;
import emgshared.cache.CacheConstants;
import emgshared.cache.EMTCacheElement;
import emgshared.cache.GetSourceSiteDetailsCache;
import emgshared.dataaccessors.EMGSharedLogger;

public class EMTSharedSourceSiteCacheServiceImpl implements EMTSharedSourceSiteCacheService {
	private static final Logger LOG = EMGSharedLogger.getLogger(
			EMTSharedSourceSiteCacheServiceImpl.class);
	private static final EMTSharedSourceSiteCacheService _instance = new EMTSharedSourceSiteCacheServiceImpl();

	EMTCacheElement emtCache = new EMTCacheElement();

	/*@SuppressWarnings("unchecked")
	public Collection sourceSiteId(String status) throws Exception {
		Collection<PartnerSite> partSites = null;
		try {
			partSites = (Collection<PartnerSite>) emtCache
					.getCacheElementValue(CacheConstants.SOURCE_SITE_IDS_CACHE,
							status);
			if (null != partSites) {
				LOG.info("Getting the source site id from Cache");
			} else {
				GetSourceSiteIdsCache sourceSiteCache = new GetSourceSiteIdsCache();
				partSites = sourceSiteCache.initCacheSourceSiteId(status);
				LOG.info("Getting the source site id from DB as Cache Element is null");
			}
		} catch (CacheException e) {
			throw new EMGRuntimeException(e.getMessage(), e);
		}

		return partSites;

	}*/

	public static EMTSharedSourceSiteCacheService instance() {
		return _instance;
	}

	public GetSourceSiteInfoResponse sourceSiteDetails(String sourceSite)
			throws Exception {
		GetSourceSiteInfoResponse response = (GetSourceSiteInfoResponse) emtCache
				.getCacheElementValue(CacheConstants.SOURCE_SITE_DETAILS,
						sourceSite);
		if (null != response) {
			LOG.info("GetSourceSiteInfoResponse retrived from cache element");
		} else {
			GetSourceSiteDetailsCache siteDetailsCache = new GetSourceSiteDetailsCache();
			response = siteDetailsCache.initCacheSourceSiteId(sourceSite);
			LOG.info("GetSourceSiteInfoResponse retrived from hitting API as Cache Element is null");
		}
		return response;
	}
	
	//MBO-6101
	public MGOAgentProfile getACDetails(String sourceSiteId, String tranType) throws Exception {
		LOG.info("Inside getACDetails - going to check from Cahce service");
		Map<String,Map> agentInfoMap = (HashMap<String,Map>) emtCache
		.getCacheElementValue(CacheConstants.AGENT_CONNECT_DETAILS,
				sourceSiteId);
		
		MGOAgentProfile mgoAgentProfile = null;
		if(null!=agentInfoMap) {
			LOG.info("Agent Info for Source Site is retrived from cache element");
			if(agentInfoMap.containsKey(sourceSiteId)) {
				Map<String,MGOAgentProfile> productTypeACMap =  agentInfoMap.get(sourceSiteId);				
				if(null!=productTypeACMap && productTypeACMap.containsKey(tranType)) {
					mgoAgentProfile = productTypeACMap.get(tranType);
				}
			}
		}
		else {
			LOG.info("Agent Info for Source Site is NOT available in cache");
			AgentConnectDetailsCache acCache = new AgentConnectDetailsCache();			
			agentInfoMap = acCache.initAgentConnectCache(sourceSiteId);
			if(agentInfoMap.containsKey(sourceSiteId)) {
				Map<String,MGOAgentProfile> productTypeACMap =  agentInfoMap.get(sourceSiteId);
				if(null!=productTypeACMap && productTypeACMap.containsKey(tranType)) {
					mgoAgentProfile = productTypeACMap.get(tranType);
				}
			}			
		}
		LOG.info("returning agent profile object with agentId " + mgoAgentProfile.getAgentId());
		return mgoAgentProfile;		
	}
	//ended
}
