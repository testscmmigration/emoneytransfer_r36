package emgshared.cache;

public class CacheConstants {
	public static final String SOURCE_SITE_IDS_CACHE = "SourceSiteIds";
	public static final String SOURCE_SITE_DETAILS = "SourceSiteDetails";
	
	//MBO-6101
	public static final String AGENT_CONNECT_DETAILS = "AgentConnectDetails";
	//ended
	public static final String DAILING_CODE_INFO = "DailingCodeInfo";

}
