package shared.mgo.services;

import java.math.BigDecimal;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import shared.mgo.dto.MGOAgentProfile;
import shared.mgo.exceptions.ACRuntimeException;

import com.moneygram.agentconnect1305.wsclient.AgentConnect;
import com.moneygram.agentconnect1305.wsclient.AgentConnectProxy;
import com.moneygram.agentconnect1305.wsclient.AgentConnectServiceLocator;
import com.moneygram.agentconnect1305.wsclient.BillerSearchRequest;
import com.moneygram.agentconnect1305.wsclient.BillerSearchResponse;
import com.moneygram.agentconnect1305.wsclient.BpValidationRequest;
import com.moneygram.agentconnect1305.wsclient.BpValidationResponse;
import com.moneygram.agentconnect1305.wsclient.CodeTableRequest;
import com.moneygram.agentconnect1305.wsclient.CodeTableResponse;
import com.moneygram.agentconnect1305.wsclient.CommitTransactionRequest;
import com.moneygram.agentconnect1305.wsclient.CommitTransactionResponse;
import com.moneygram.agentconnect1305.wsclient.CountryCurrencyInfo;
import com.moneygram.agentconnect1305.wsclient.CountryInfo;
import com.moneygram.agentconnect1305.wsclient.CurrencyInfo;
import com.moneygram.agentconnect1305.wsclient.DeliveryOptionInfo;
import com.moneygram.agentconnect1305.wsclient.DetailLookupRequest;
import com.moneygram.agentconnect1305.wsclient.DetailLookupResponse;
import com.moneygram.agentconnect1305.wsclient.FQDOsForCountryRequest;
import com.moneygram.agentconnect1305.wsclient.FQDOsForCountryResponse;
import com.moneygram.agentconnect1305.wsclient.FeeLookupRequest;
import com.moneygram.agentconnect1305.wsclient.FeeLookupResponse;
import com.moneygram.agentconnect1305.wsclient.GetFQDOByCustomerReceiveNumberRequest;
import com.moneygram.agentconnect1305.wsclient.GetFQDOByCustomerReceiveNumberResponse;
import com.moneygram.agentconnect1305.wsclient.GetFieldsForProductRequest;
import com.moneygram.agentconnect1305.wsclient.GetFieldsForProductResponse;
import com.moneygram.agentconnect1305.wsclient.ProductType;
import com.moneygram.agentconnect1305.wsclient.ProductVariant;
import com.moneygram.agentconnect1305.wsclient.ProfileRequest;
import com.moneygram.agentconnect1305.wsclient.ProfileResponse;
import com.moneygram.agentconnect1305.wsclient.ReceiveCountryRequirementsInfo;
import com.moneygram.agentconnect1305.wsclient.ReceiveCountryRequirementsRequest;
import com.moneygram.agentconnect1305.wsclient.ReceiveCountryRequirementsResponse;
import com.moneygram.agentconnect1305.wsclient.Request;
import com.moneygram.agentconnect1305.wsclient.SearchType;
import com.moneygram.agentconnect1305.wsclient.SendReversalRequest;
import com.moneygram.agentconnect1305.wsclient.SendReversalResponse;
import com.moneygram.agentconnect1305.wsclient.SendReversalType;
import com.moneygram.agentconnect1305.wsclient.SendValidationRequest;
import com.moneygram.agentconnect1305.wsclient.SendValidationResponse;
import com.moneygram.agentconnect1305.wsclient.StateProvinceInfo;
import com.moneygram.agentconnect1305.wsclient.ThirdPartyType;
import com.moneygram.agentconnect1305.wsclient.SendReversalReasonCode;

import emgshared.dataaccessors.EMGSharedLogger;


/**
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public abstract class AbstractAgentConnectAccessor {

	private static CodeTableResponse codeTableResponse;
	private static CodeTableResponse codeTableResponseForRefresh;
	private static ReceiveCountryRequirementsResponse receiveCountryRequirementsResponse;
	private static Hashtable countries;
	private static List sortedCountries;
	private AgentConnect agentConnect;
	private AgentConnectServiceLocator agentConnectService;

	// shared props
	final public static String API_VER_PROP_KEY = "AC4APIVersion";
	final public static String CLIENT_VER_PROP_KEY = "AC4ClientVersion";
	final public static String ADDRESS_PROP_KEY = "AC4Address";
	final public static String NAMESPACE_URI_PROP_KEY = "AC4NamespaceURI";
	final public static String LOCAL_PART_PROP_KEY = "AC4LocalPart";
	final public static String SERVICE_NAME_PROP_KEY = "AC4WSDDServiceName";
	final public static String RETRY_COUNT = "AC4RetryCount";
	final public static String RETRY_WAIT = "AC4RetryWaitTime";
	final public static String TIME_OUT = "A4CTimeout";

	private Integer profileId;
	private String agentId;
	private String agentSequence;
	private String token;
	private static String apiVersion;
	private static String clientApiVersion;
	private static String address;
	private static String nameSpaceURI;
	private static String localPart;
	private static String serviceName;
	private static int retryCount = 3;
	private static long retryWaitTime = 15000;
	private static int timeout = 30000;

	//
	private static boolean initialized = false;
	//
	private static Logger logger = EMGSharedLogger.getLogger(
			AbstractAgentConnectAccessor.class);

	/**
	 * 
	 * @throws IllegalStateException
	 */
	protected AbstractAgentConnectAccessor(MGOAgentProfile mgoAgtProfile) {
		// if (!isInitialized())
		// new IllegalStateException("AgentConnect is not initialized.");

		profileId = mgoAgtProfile.getProfileId();
		agentId = mgoAgtProfile.getAgentId();
		agentSequence = mgoAgtProfile.getAgentSequence();
		token = mgoAgtProfile.getToken();
	}

	/**
	 * 
	 * @param props
	 * @throws ACRuntimeException
	 */
	protected AbstractAgentConnectAccessor(Properties props,
			MGOAgentProfile mgoAgtProfile) {
		try {
			logger.info("Intializing AgentConnect ...");
			initOnce(props, mgoAgtProfile);
			setCodeTableResponse();
			//setReceiveCountryRequirementsResponse();
			setInitialized(true);
			logger.info("Done Intializing AgentConnect");
		} catch (Exception e) {
			throw new ACRuntimeException(e.toString());
		}
	}

	public CountryCurrencyInfo[] getCountryCurrencyInfo() {
		return getCodeTableResponse().getCountryCurrencyInfo();
	}

	public CodeTableResponse getCodeTableResponse() {
		if (codeTableResponse == null) {
			try {
				setCodeTableResponse();
			} catch (Exception e) {
				System.err
						.println("Problems restoring code table response. Errors expected: "
								+ e.getMessage());
				logger.error(
						"Problems restoring code table response. Errors expected",
						e);
			}
		}
		return codeTableResponse;
	}

	public void refreshCodeTablesFromAgentConnect() throws Exception {
		try {
			setCodeTableResponse();
		} catch (Exception e) {
			logger.error("Error Refreshing Code Tables", e);
			throw e;
		}

	}

	private synchronized void setCodeTableResponse() throws Exception {
		logger.info("Requesting AgentConnect for codeTable ...");

		CodeTableRequest req = new CodeTableRequest();
		req = (CodeTableRequest) setHeader(req);
		// FIXME - S26 - remove following logging
		logger.error("req.apiversion = " + req.getApiVersion()
				+ ";clientsoftvers = " + req.getClientSoftwareVersion());
		int count = 0;
		while (count < retryCount) {
			try {
				logger.error("Refreshing/Building Code Tables from Agent Connect 13.05 "
						+ System.currentTimeMillis());
				refreshCodeTableResponse(req);
				logger.error("Done Refreshing/Building Code Tables from Agent Connect  13.05 "
						+ System.currentTimeMillis());
				break;
			} catch (Exception e) {
				wait(retryWaitTime);
				count++;
				logger.warn("getCodeTable()- Attempt : " + count + " Failed.",
						e);
				System.err.println("getCodeTable()- Attempt : " + count
						+ " Failed: " + e.getMessage());
				logger.error("refreshCodeTableResponse failed", e);
				if (count == retryCount) {
					throw (e);
				}
				continue;
			}
		}
	}

	private void refreshCodeTableResponse(CodeTableRequest req)
			throws RemoteException, Error, Exception {
		
		codeTableResponseForRefresh = new AgentConnectProxy(address)
				.codeTable(req);
		codeTableResponse = codeTableResponseForRefresh;
	}

	// Commented as a part of UCP-340
	/*public FQDOsForCountryResponse getFQDOsForCountryResponse(String countryCode)
			throws Exception {
		FQDOsForCountryRequest req = new FQDOsForCountryRequest();
		req = (FQDOsForCountryRequest) setHeader(req);
		req.setReceiveCountry(countryCode);
		return getAgentConnect().getFQDOsForCountry(req);
	}*/

	public DeliveryOptionInfo[] getDeliveryOptionInfo() {
		return getCodeTableResponse().getDeliveryOptionInfo();
	}

	public CountryInfo[] getCountryInfo() {
		return getCodeTableResponse().getCountryInfo();
	}

	public CurrencyInfo[] getCurrencyInfo() {
		return getCodeTableResponse().getCurrencyInfo();
	}

	public ReceiveCountryRequirementsResponse getReceiveCountryRequirementsResponse() {
		return receiveCountryRequirementsResponse;
	}

	public StateProvinceInfo[] getStateProvinceInfo() {
		return getCodeTableResponse().getStateProvinceInfo();
	}

	private boolean isInitialized() {
		return initialized;
	}

	private synchronized void setInitialized(boolean init) {
		initialized = init;
	}

	private synchronized void initOnce(Properties props,
			MGOAgentProfile mgoAgtProfile) {
		// FIXME: S26 - logging remove
		logger.error("S26:initOnce called...");
		// ***********************************
		// unique parameters for each agent
		profileId = mgoAgtProfile.getProfileId();
		agentId = mgoAgtProfile.getAgentId();
		System.out.println("HELLO" + mgoAgtProfile.getAgentId());
		agentSequence = mgoAgtProfile.getAgentSequence();
		System.out.println(mgoAgtProfile.getAgentSequence());
		System.out.println(mgoAgtProfile.getToken());
		token = mgoAgtProfile.getToken();

		// shared parameters for all agents
		apiVersion = props.getProperty(API_VER_PROP_KEY, "1");	
		clientApiVersion = props.getProperty(CLIENT_VER_PROP_KEY, "1");
		address = props.getProperty(ADDRESS_PROP_KEY);
		if (address == null) {
			Exception e = new Exception("test");
			logger.error(e.getMessage(),e);
		}
		nameSpaceURI = props.getProperty(NAMESPACE_URI_PROP_KEY);
		localPart = props.getProperty(LOCAL_PART_PROP_KEY);
		serviceName = props.getProperty(SERVICE_NAME_PROP_KEY);
		retryCount = (int) toLong(props.getProperty(RETRY_COUNT), retryCount);
		retryWaitTime = toLong(props.getProperty(RETRY_WAIT), retryWaitTime);
		timeout = (int) toLong(props.getProperty(TIME_OUT), timeout);
	}

	private long toLong(String val, long defVal) {
		try {
			return Long.parseLong(val);
		} catch (Exception e1) {
		}

		return defVal;
	}

	protected Request setHeader(Request req) {
		req.setUnitProfileID(profileId);
		req.setAgentID(agentId);
		req.setAgentSequence(agentSequence);
		req.setApiVersion(apiVersion);
		req.setClientSoftwareVersion(clientApiVersion);
		req.setToken(token);
		req.setTimeStamp(Calendar.getInstance());
		// FIXME: S26 - remove logging and setting
		if (req.getApiVersion() == null) {
			req.setApiVersion("1");
			System.out
					.println("***S26 - AbstractAgentConnectAccessor.setHeader - apiVersion is null***");
		}
		if (req.getClientSoftwareVersion() == null) {
			req.setClientSoftwareVersion("1");
			System.out
					.println("***S26 - AbstractAgentConnectAccessor.setHeader - clientSoftwareVersion is null***");
		}
		// *************************************************
		return req;
	}

	public AgentConnect getAgentConnect() throws Exception {
		if (agentConnect == null) {
			agentConnect = new AgentConnectProxy(address);
		}
		return agentConnect;
	}

	/**
	 * @return
	 */
	public String getAgentId() {
		return agentId;
	}

	/*private synchronized void setReceiveCountryRequirementsResponse()
			throws Exception {

		logger.info("Requesting AgentConnect for Code Table ... ");
		ReceiveCountryRequirementsRequest req = new ReceiveCountryRequirementsRequest();
		req = (ReceiveCountryRequirementsRequest) setHeader(req);
		receiveCountryRequirementsResponse = getAgentConnect()
				.receiveCountryRequirements(req);
	}*/

	protected void logSOAPMessages() {
		try {
			logger.debug("SOAP Request : \n"
					+ agentConnectService.getCall().getMessageContext()
							.getRequestMessage().getSOAPPartAsString());

			logger.debug("SOAP Response : \n"
					+ agentConnectService.getCall().getMessageContext()
							.getResponseMessage().getSOAPPartAsString());
		} catch (Exception t) {
			logger.warn("logSOAPMessages Failed - " + t.getMessage());
		}
	}

	public DetailLookupResponse detailLookup(String refNbr) throws Exception {
		logger.info("Requesting AgentConnect for detailLookup ... ");
		DetailLookupRequest req = new DetailLookupRequest();
		req = (DetailLookupRequest) setHeader(req);
		req.setReferenceNumber(refNbr);
		DetailLookupResponse resp = getAgentConnect().detailLookup(req);
		//
		logSOAPMessages();
		return resp;
	}

	public SendReversalResponse sendReversal(String refNbr, BigDecimal sndAmt,
			BigDecimal feeAmt, String sendCurrency,
			SendReversalType reversalType) throws Exception {
		logger.info("Requesting AgentConnect for sendReversal ... ");
		SendReversalRequest req = new SendReversalRequest();
		req = (SendReversalRequest) setHeader(req);
		req.setReferenceNumber(refNbr);
		req.setReversalType(reversalType);
		req.setFeeRefund("Y");
		req.setSendAmount(sndAmt);
		req.setFeeAmount(feeAmt);
		req.setSendCurrency(sendCurrency);
		req.setSendReversalReason(SendReversalReasonCode.MS_NOT_USED);
		SendReversalResponse resp = getAgentConnect().sendReversal(req);
		//
		logSOAPMessages();
		return resp;
	}

	private String truncate(String str, int length) {
		if (str == null)
			return str;
		if (str.length() > length)
			return str.substring(0, length);
		else
			return str;
	}

	public static CountryInfo getCountry(String cid) {
		if ((countries == null) || (countries.get(cid) == null))
			return null;
		return (CountryInfo) countries.get(cid);
	}

	public ReceiveCountryRequirementsInfo[] getRequirementsInfo()
			throws Exception {

		ArrayList list = new ArrayList();

		ReceiveCountryRequirementsRequest req = new ReceiveCountryRequirementsRequest();
		req = (ReceiveCountryRequirementsRequest) setHeader(req);

		ReceiveCountryRequirementsResponse resp = getAgentConnect()
				.receiveCountryRequirements(req);
		ReceiveCountryRequirementsInfo[] rcri = resp
				.getReceiveCountryRequirementsInfo();
		return rcri;
	}

	public FeeLookupResponse feeLookup(BigDecimal amount, String recCountry,
			ProductType type, String recCurrency, String deliveryOption,
			String rewardsNumber, String agencyId) throws Exception {
		logger.info("Requesting AgentConnect for feeLookup ... ");
		FeeLookupRequest req = new FeeLookupRequest();
		req = (FeeLookupRequest) setHeader(req);
		// req.setReceiveAgentID(agencyId);
		req.setReceiveCode(agencyId);
		if ((rewardsNumber != null) && (!rewardsNumber.equals("")))
			req.setMgiRewardsNumber(rewardsNumber);
		req.setDeliveryOption(deliveryOption);
		req.setProductType(type);
		req.setReceiveCurrency(recCurrency);
		req.setAmountExcludingFee(amount);
		req.setReceiveCountry(recCountry);
		FeeLookupResponse resp = getAgentConnect().feeLookup(req);
		logSOAPMessages();
		return resp;
	}

	/*public FeeLookupResponse bpfeeLookup(BigDecimal amount, String recCountry,
			ProductType type, String recCurrency, String deliveryOption,
			String rewardsNumber, String agencyId) throws Exception {
		logger.info("Requesting AgentConnect for feeLookup ... ");
		FeeLookupRequest req = new FeeLookupRequest();
		req = (FeeLookupRequest) setHeader(req);
		// req.setReceiveAgentID(agencyId);
		req.setReceiveCode(agencyId);
		if ((rewardsNumber != null) && (!rewardsNumber.equals("")))
			req.setMgiRewardsNumber(rewardsNumber);
		// req.setDeliveryOption(deliveryOption);
		req.setProductType(type);
		req.setProductVariant(ProductVariant.EP);
		req.setReceiveCurrency(recCurrency);
		req.setAmountExcludingFee(amount);
		req.setReceiveCountry(recCountry);
		FeeLookupResponse resp = getAgentConnect().feeLookup(req);
		logSOAPMessages();
		return resp;
	}*/

	public ProfileResponse getProfile() throws Exception {
		logger.info("Requesting AgentConnect for profile ... ");
		ProfileRequest req = new ProfileRequest();
		req = (ProfileRequest) setHeader(req);
		ProfileResponse resp = getAgentConnect().profile(req);
		logSOAPMessages();
		return resp;
	}

	public SendValidationResponse sendValidation(SendValidationRequest req)
			throws Exception {
		logger.info("Requesting AgentConnect for sendValidation ... ");
		req = (SendValidationRequest) setHeader(req);
		SendValidationResponse resp = getAgentConnect().sendValidation(req);
		logSOAPMessages();
		return resp;
	}

	// Commented as a part of UCP-340
	/*public BpValidationResponse bpValidation(BpValidationRequest req)
			throws Exception {
		logger.info("Requesting AgentConnect for bpValidation ... ");
		req = (BpValidationRequest) setHeader(req);
		BpValidationResponse resp = getAgentConnect().bpValidation(req);
		logSOAPMessages();
		return resp;

	}

	public BillerSearchResponse billerSearch(String recCode) throws Exception {
		logger.info("Requesting AgentConnect for billerSearchRequest ... ");
		BillerSearchRequest req = new BillerSearchRequest();
		req = (BillerSearchRequest) setHeader(req);
		req.setSearchType(SearchType.CODE);
		req.setProductVariant(ProductVariant.EP);
		req.setReceiveCode(recCode);
		BillerSearchResponse resp = getAgentConnect().billerSearch(req);
		logSOAPMessages();
		return resp;
	}

	public GetFQDOByCustomerReceiveNumberResponse getFQDOByCustomerReceiveNumber(
			GetFQDOByCustomerReceiveNumberRequest req, String rrn)
			throws Exception {
		logger.info("Requesting AgentConnect for getFQDOByCustomerReceiveNumber ... ");
		req = (GetFQDOByCustomerReceiveNumberRequest) setHeader(req);
		req.setMgCustomerReceiveNumber(rrn);
		GetFQDOByCustomerReceiveNumberResponse resp = getAgentConnect()
				.getFQDOByCustomerReceiveNumber(req);
		logSOAPMessages();
		return resp;
	}*/

	public CommitTransactionResponse commitTransaction(
			CommitTransactionRequest req) throws Exception {
		logger.info("Requesting AgentConnect for commitTransaction ... "
				+ System.currentTimeMillis());
		req = (CommitTransactionRequest) setHeader(req);
		CommitTransactionResponse resp = new CommitTransactionResponse();
		boolean respFlag = false;
		try {
			resp = getAgentConnect().commitTransaction(req);
			// Added by wr39(Mrugesh) for MGO-7405 story
			if (null == resp.getReferenceNumber()
					|| "".equals(resp.getReferenceNumber())) {
				respFlag = true;
				throw new com.moneygram.agentconnect1305.wsclient.Error();
			}
			// Ended by wr39(Mrugesh) for MGO-7405 story
			logSOAPMessages();
			logger.info("Done Requesting AgentConnect for commitTransaction ... "
					+ System.currentTimeMillis());
		} catch (com.moneygram.agentconnect1305.wsclient.Error e) {
			// Added by wr39(Mrugesh) for MGO-7405 story
			if (respFlag) {
				logger.info("AgentConnect returned empty ReferenceNumber in commitTransaction response");
			}
			// Ended by wr39(Mrugesh) for MGO-7405 story
			logger.info("Error Requesting AgentConnect for commitTransaction ... "
					+ System.currentTimeMillis() + e.getLocalizedMessage());
			logSOAPMessages();
			//removed Explicit error codes(7003,7004) check
			if (null != e.getErrorCode()) {
				throw e;
			} else {
				throw new Exception(e);
			}
		} catch (Exception e) {
			logger.info("Error Requesting AgentConnect for commitTransaction ... RETRYING"
					+ System.currentTimeMillis() + e.getLocalizedMessage());
			// Added by wr39(Mrugesh) for MGO-6698 story
			try {
				logger.info("Requesting AgentConnect for commitTransaction ... "
						+ System.currentTimeMillis());
				resp = getAgentConnect().commitTransaction(req);
				logSOAPMessages();
				logger.info("Done Requesting AgentConnect for commitTransaction ... "
						+ System.currentTimeMillis());
			} catch (Exception ex) {
				logger.info("Error On AgentConnect RETRY for commitTransaction ... "
						+ System.currentTimeMillis() + e.getLocalizedMessage());
				logSOAPMessages();
				throw new Exception(ex);

			}
		}
		// Ended by wr39(Mrugesh) for MGO-6698 story
		return resp;
	}

	/*public GetFieldsForProductResponse getFieldsForProduct(
			String rcvCountryCode, String rcvCurrencyCode, String rcvAgentID,
			String deliveryOption, String consumerId, BigDecimal amount)
			throws Exception {

		logger.info("Requesting AgentConnect for getFieldsForProduct ... ");
		GetFieldsForProductRequest req = new GetFieldsForProductRequest();
		req = (GetFieldsForProductRequest) setHeader(req);
		req.setProductType(ProductType.SEND);
		req.setDeliveryOption(deliveryOption);
		req.setReceiveCountry(rcvCountryCode);
		req.setReceiveCurrency(rcvCurrencyCode);
		req.setReceiveAgentID(rcvAgentID);
		req.setThirdPartyType(ThirdPartyType.NONE);
		req.setConsumerId(consumerId);
		req.setAmount(amount);
		GetFieldsForProductResponse resp = getAgentConnect()
				.getFieldsForProduct(req);
		logSOAPMessages();
		return resp;
	}*/

}
