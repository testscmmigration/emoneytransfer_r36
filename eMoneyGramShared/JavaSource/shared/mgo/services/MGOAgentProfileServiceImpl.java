package shared.mgo.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;


import com.moneygram.mgo.service.config_v3_4.client.ConfigurationServiceClient;
import com.moneygram.ree.lib.Config;

import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.util.ResourceConfigFactory;

import shared.mgo.dto.MGOAgentProfile;
import shared.mgo.dto.MGOProduct;
import shared.mgo.exceptions.MGOProfileNotFoundException;

public class MGOAgentProfileServiceImpl implements MGOAgentProfileService {

	private static final String CONTAINER_RESOURCE_URI = "java:comp/env/rep/EMTAdminResourceReference";
	private static HashMap mgoAgentProfileCache = new HashMap();
	private static HashMap mgoAgentProfileCacheByAgtId = new HashMap();
	private static long lastCacheUpdate = System.currentTimeMillis();
	private static final String MGO_CONFIGURATION_SERVICE_URL = "MGO_CONFIGURATION_SERVICE_URL";
	private static final String MGO_CONFIGURATION_SERVICE_TIMEOUT = "MGO_CONFIGURATION_SERVICE_TIMEOUT";
	private static final String MGO_PROFILES_FILE_NAME = "PROFILES_FILE_NAME";
	// cache for for 8 hours
	private static long refreshDelay = 8 * 60 * 60 * 1000;
	// for testing ... private static long refreshDelay = 3*1000;

	private static ResourceConfigFactory resourceConfigFactory = new ResourceConfigFactory();
	private static Config mgoConfig;
	static Logger logger = EMGSharedLogger.getLogger(
    		MGOAgentProfileServiceImpl.class);
	public static ResourceConfigFactory getResourceConfigFactory() {
		return resourceConfigFactory;
	}

	public static void setResourceConfigFactory(
			emgshared.util.ResourceConfigFactory factory) {
		resourceConfigFactory = factory;
	}

	private static final MGOAgentProfileServiceImpl instance = new MGOAgentProfileServiceImpl();

	private MGOAgentProfileServiceImpl() {
	}

	public static final MGOAgentProfileServiceImpl getInstance() {
		return instance;
	}

	// pass in an MGOAgentProfile object with the keys specified the the actual
	// object will be returned.
	public MGOAgentProfile getMGOProfile(String mgoAgentIdentifier,
			String mgoProductName) throws MGOProfileNotFoundException {
		long currentTime = System.currentTimeMillis();
		if (((currentTime - lastCacheUpdate) > refreshDelay)
				|| (mgoAgentProfileCache.isEmpty())) {
			try {
				initAgentProfileCache();
			} catch (Exception e) {
				throw new MGOProfileNotFoundException(
						"MGO Profile Service call failure.  Unable to find MGO Agent Profile Information.",
						e);
			}
		}
		MGOAgentProfile returnProfile = new MGOAgentProfile();
		try {
			if ((mgoAgentIdentifier == null) || (mgoAgentIdentifier.equals("")))
				throw new MGOProfileNotFoundException(
						"No Agent Identifier passed.");
			if ((mgoProductName == null) || (mgoProductName.equals("")))
				throw new MGOProfileNotFoundException(
						"No Agent Product passed.");
			String key = mgoAgentIdentifier + mgoProductName;
			key = key.toUpperCase();
			if (mgoAgentProfileCache.containsKey(key))
				returnProfile = (MGOAgentProfile) mgoAgentProfileCache.get(key);
		} catch (Exception e) {
			throw new MGOProfileNotFoundException(
					"Exception in getMGOProfile. " + e.getLocalizedMessage(), e);
		}
		return returnProfile;
	}

	// pass in an MGOAgentProfile object with the keys specified the the actual
	// object will be returned.
	public MGOAgentProfile getMGOProfileByAgtId(String mgoAgentId,
			String mgoProductName) throws MGOProfileNotFoundException {
		long currentTime = System.currentTimeMillis();
		if (((currentTime - lastCacheUpdate) > refreshDelay)
				|| (mgoAgentProfileCache.isEmpty())) {
			try {
				initAgentProfileCache();
			} catch (Exception e) {
				throw new MGOProfileNotFoundException(
						"MGO Profile Service call failure.  Unable to find MGO Agent Profile Information.",
						e);
			}
		}
		MGOAgentProfile returnProfile = new MGOAgentProfile();
		try {
			if ((mgoAgentId == null) || (mgoAgentId.equals("")))
				throw new MGOProfileNotFoundException("No Agent Id passed.");
			if ((mgoProductName == null) || (mgoProductName.equals("")))
				throw new MGOProfileNotFoundException(
						"No Agent Product passed.");
			String key = mgoAgentId + mgoProductName;
			key = key.toUpperCase().trim();
			if (mgoAgentProfileCacheByAgtId.containsKey(key))
				returnProfile = (MGOAgentProfile) mgoAgentProfileCacheByAgtId
						.get(key);
		} catch (Exception e) {
			throw new MGOProfileNotFoundException(
					"Exception in getMGOProfileByAgtId. "
							+ e.getLocalizedMessage(), e);
		}
		return returnProfile;
	}

	public void initAgentProfileCache() throws Exception {

		String SERVICE_URL = "";
		int timeout = 30000;
		String CONFIG_FILE_NAME = "";
		HashMap tempCache = new HashMap();
		HashMap tempCacheByAgtId = new HashMap();
		List profileList = null;

		try {

			CONFIG_FILE_NAME = getContextResourceVariable(MGOAgentProfileServiceImpl.MGO_PROFILES_FILE_NAME);
			SERVICE_URL = getContextResourceVariable(MGOAgentProfileServiceImpl.MGO_CONFIGURATION_SERVICE_URL);
			timeout = Integer
					.parseInt(getContextResourceVariable(MGOAgentProfileServiceImpl.MGO_CONFIGURATION_SERVICE_TIMEOUT));
			ConfigurationServiceClient client = new ConfigurationServiceClient(
					SERVICE_URL, timeout);
			client.setFileName(CONFIG_FILE_NAME);
			profileList = client.getAllAgentConfigurations();
		} catch (Exception e) {
			// if refresh of cache fails, just keep what we have
			if ((profileList == null) || (profileList.isEmpty()))
				throw e;
			else {
				logger.error(e.getMessage(),e);
				return;
			}
		}
		Iterator it = profileList.iterator();
		int profileCount = 0;
		while (it.hasNext()) {
			com.moneygram.mgo.service.config_v3_4.MGOAgentProfile mgoProf = (com.moneygram.mgo.service.config_v3_4.MGOAgentProfile) it
					.next();
			MGOAgentProfile mgoProfConv = new MGOAgentProfile();
			mgoProfConv.setAgentId(mgoProf.getAgentId());
			mgoProfConv.setAgentSequence(mgoProf.getAgentSequence());
			mgoProfConv.setMGOAgentIdentifier(mgoProf.getMGOAgentIdentifier()
					.toUpperCase().trim());
			mgoProfConv.setMgoAgentProfileDescription(mgoProf
					.getMgAgentProfileDescription());
			com.moneygram.mgo.service.config_v3_4.MGOProduct mgoProd = mgoProf
					.getMgoProduct();
			MGOProduct mgoProdConv = new MGOProduct();
			mgoProdConv.setEnabled(mgoProd.getEnabled());
			mgoProdConv.setProductName(mgoProd.getProductName().toUpperCase()
					.trim());
			mgoProdConv.setMgoPaymentOptions(mgoProf.getMgoProduct()
					.getMgoPaymentOptions());
			mgoProfConv.setMgoProduct(mgoProdConv);
			String agtTokenVariableName = mgoProfConv.getMGOAgentIdentifier()
					+ "_" + mgoProfConv.getMgoProduct().getProductName()
					+ "_password";
			agtTokenVariableName = agtTokenVariableName.replaceAll(" ", "_");
			String token = getContextResourceVariable(agtTokenVariableName);
			// FIXME: Added this for S26 walmart cuz missing properties on
			// weekend
			if (token == null || token.length() < 1) {
				token = "TEST";
			}
			mgoProfConv.setToken(token);
			mgoProfConv.setMgoProduct(mgoProdConv);
			mgoProfConv.setProfileId(mgoProf.getProfileId());
			String key = mgoProf.getMGOAgentIdentifier()
					+ mgoProf.getMgoProduct().getProductName();
			tempCache.put(key.toUpperCase().trim(), mgoProfConv);
			key = mgoProf.getAgentId()
					+ mgoProf.getMgoProduct().getProductName();
			tempCacheByAgtId.put(key.toUpperCase().trim(), mgoProfConv);
			profileCount++;
		}
		if (profileCount == 0)
			return;
		synchronized (mgoAgentProfileCache) {
			lastCacheUpdate = System.currentTimeMillis();
			mgoAgentProfileCache = tempCache;
		}
		synchronized (mgoAgentProfileCacheByAgtId) {
			mgoAgentProfileCacheByAgtId = tempCacheByAgtId;
		}
	}

	private static String getContextResourceVariable(String variableName) {
		String returnString = "";
		try {
			if (mgoConfig == null) {
				mgoConfig = getResourceConfigFactory()
						.createResourceConfiguration(CONTAINER_RESOURCE_URI);
			}
			returnString = (String) mgoConfig.getAttribute(variableName);
			System.out.println("variableName = " + variableName
					+ " returnString = " + returnString);
		} catch (Exception exception) {
			logger.error(exception.getMessage(),exception);
		}
		return returnString;
	}

	// pass "ALL" to siteidentifier to get all Profiles for all sites. else,
	// pass in your
	// site id to get all those applicable to your site. Example "MGO".
	public ArrayList getAllMGOProfiles(String siteIdentifier)
			throws MGOProfileNotFoundException {

		Iterator it = mgoAgentProfileCache.keySet().iterator();
		ArrayList returnList = new ArrayList();
		while (it.hasNext()) {
			String key = (String) it.next();
			boolean addIt = false;
			if (siteIdentifier.equalsIgnoreCase("ALL"))
				addIt = true;
			else if (key.startsWith(siteIdentifier.toUpperCase()))
				addIt = true;
			if (addIt) {
				MGOAgentProfile mgap = (MGOAgentProfile) mgoAgentProfileCache
						.get(key);
				returnList.add(mgap);
			}
		}
		return returnList;
	}
}
