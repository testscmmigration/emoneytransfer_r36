/*
 *
 */
package shared.mgo.services;

/**
 * @author A119
 *
 */
public class MGOServiceFactory
{
	private static final MGOServiceFactory instance = new MGOServiceFactory();

	private MGOServiceFactory()
	{
	}

	public static final MGOServiceFactory getInstance()
	{
		return instance;
	}

	public AgentConnectService getAgentConnectService()
	{
		return AgentConnectServiceImpl.getInstance();
	}

	public MGOAgentProfileService getMGOAgentProfileService()
	{
		return MGOAgentProfileServiceImpl.getInstance();
	}
}
