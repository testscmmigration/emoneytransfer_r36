/*
 * Created on Dic 5, 2012
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package shared.mgo.services;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import shared.mgo.dto.MGOAgentProfile;
import shared.mgo.dto.MGOCountry;
import shared.mgo.dto.MGOProductFieldInfo;

import com.moneygram.agentconnect1305.wsclient.CountryCurrencyInfo;
import com.moneygram.agentconnect1305.wsclient.CurrencyInfo;
import com.moneygram.agentconnect1305.wsclient.DeliveryOptionInfo;
import com.moneygram.agentconnect1305.wsclient.FeeLookupResponse;
import com.moneygram.agentconnect1305.wsclient.GetFQDOByCustomerReceiveNumberResponse;

/**
 * @author VY79
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface AgentConnectService {
	
	MGOCountry getCountry(String countryCode) throws Exception;
	
	void refreshMasterFromAgentConnect() throws Exception;
	
	void refreshExceptionCountry() throws Exception;
	
	Collection getStateProvinceInfo(String countryCode) throws Exception;
	
 //	Collection getFQDOInfo(String countryCode) throws Exception;

	Collection getCountryCurrencyInfo(String countryCode) throws Exception;

	Map getDeliveryOptionInfo() throws Exception;
	
	DeliveryOptionInfo getDeliveryOptionInfo(int deliveryOptionId) throws Exception;

	DeliveryOptionInfo getDeliveryOptionInfo(String deliveryOptionCode) throws Exception;
	
	List getCntryExceptions() throws Exception;	
	
	Collection getActiveCountryList() throws Exception;
	
	/*FeeLookupResponse epFeeLookup(MGOAgentProfile mgoAgentProfile ,String agencyId, String amount) throws Exception;
	
	FeeLookupResponse mgFeeLookup(MGOAgentProfile mgoAgentProfile ,BigDecimal amount, String recCountry,String rewardsNumber) throws Exception;
	
	FeeLookupResponse mgFeeLookup(MGOAgentProfile mgoAgentProfile ,BigDecimal amount, String recCountry, String currency,String rewardsNumber) throws Exception;
	
	FeeLookupResponse mgFeeLookup(MGOAgentProfile mgoAgentProfile ,BigDecimal amount, String recCountry, String recCurrency, String deliveryOption,String rewardsNumber) throws Exception;
	*/		
	Collection getFilteredCountryList(String state) throws Exception;
	
	Collection getEconomyCountryList(String originationStateCode) throws Exception;
	
	Collection getSameDayCountryList(String originationStateCode) throws Exception;
	
	Map getCountries() throws Exception;
	
	Collection getReceiveCurrencyInfo(String countryCode) throws Exception;
		
	Collection getServiceOptionInfos(String countryCode) throws Exception;

	CurrencyInfo getCurrencyInfo(String currencyCode) throws Exception;
	
	CountryCurrencyInfo getCountryCurrencyInfo(String countryCode, String currencyCode, String deliveryOptionCode) throws Exception;
	
	Collection getReceiveCountryRequirementsList(String receiveCountry) throws Exception;
	
	/*boolean isRRNumberFound(MGOAgentProfile mgoAgentProfile,String rrn) throws Exception; 
	
	GetFQDOByCustomerReceiveNumberResponse getRRNInfo(MGOAgentProfile mgoAgentProfile,String rrn) throws Exception;
	
	MGOProductFieldInfo getFieldsByProduct(MGOAgentProfile mgoAgentProfile ,String rcvCountryCode, String rcvCurrencyCode,
		   String rcvAgentID, String deliveryOption, String consumerId,BigDecimal amount) throws Exception;
	
	MGOProductFieldInfo getFieldsByProduct(MGOAgentProfile mgoAgentProfile,String rcvCountryCode, String rcvCurrencyCode,String deliveryOption,String consumerId,BigDecimal amount) throws Exception;*/
}
