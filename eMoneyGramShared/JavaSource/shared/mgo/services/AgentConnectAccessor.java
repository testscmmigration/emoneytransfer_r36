package shared.mgo.services;

import java.util.Properties;

import shared.mgo.dto.MGOAgentProfile;



public class AgentConnectAccessor extends AbstractAgentConnectAccessor {
	public final static String PROFILE_ID_PROP_KEY = "ACProfileId";
	public final static String AGENT_ID_PROP_KEY = "ACAgentId";
	public final static String AGENT_SEQ_PROP_KEY = "ACAgentSequence";
	public final static String TOKEN_PROP_KEY = "ACtoken";
	
	public AgentConnectAccessor(MGOAgentProfile mgoAgentProfile) {
		super(mgoAgentProfile);
	}

	public AgentConnectAccessor(Properties props,MGOAgentProfile mgoAgentProfile) {
		super(props, mgoAgentProfile);
	}	

}
