package shared.mgo.exceptions;
/**
 *  Thrown when an MGO Profile is not found for the request.  
 */
public class MGOProfileNotFoundException extends Exception {
	public MGOProfileNotFoundException()
	{
		super();
	}

	public MGOProfileNotFoundException(String s)
	{
		super(s);
	}

	public MGOProfileNotFoundException(Throwable rootCause)
	{
		super(rootCause);
	}

	public MGOProfileNotFoundException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
