package shared.mgo.dto;

public class MGOPaymentOption {

	private String paymentOptionName;
	private String paymentOptionService;
	private String paymentOptionEnabled;
	private Double maxPaymentOptionAmount;
	private String globalCollectMerchantId;

	public Double getMaxPaymentOptionAmount() {
		return maxPaymentOptionAmount;
	}
	public void setMaxPaymentOptionAmount(Double maxPaymentOptionAmount) {
		this.maxPaymentOptionAmount = maxPaymentOptionAmount;
	}
	public String getPaymentOptionService() {
		return paymentOptionService;
	}
	public void setPaymentOptionService(String paymentOptionService) {
		this.paymentOptionService = paymentOptionService;
	}
	public String getPaymentOptionName() {
		return paymentOptionName;
	}
	public void setPaymentOptionName(String paymentOptionName) {
		this.paymentOptionName = paymentOptionName;
	}
	public String getPaymentOptionEnabled() {
		return paymentOptionEnabled;
	}
	public void setPaymentOptionEnabled(String paymentOptionEnabled) {
		this.paymentOptionEnabled = paymentOptionEnabled;
	}
	public String getGlobalCollectMerchantId() {
		return globalCollectMerchantId;
	}
	public void setGlobalCollectMerchantId(String globalCollectMerchantId) {
		this.globalCollectMerchantId = globalCollectMerchantId;
	}

}
