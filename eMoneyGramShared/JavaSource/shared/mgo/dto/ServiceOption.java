/*
 * Copyright (c) 2004 MoneyGram Payment Systems, Inc. All rights reserved.
 * 
 * Created on Feb 14, 2005
 */
package shared.mgo.dto;

import java.io.Serializable;

/**
 * @author Sanjeeva Kodimala
 * 
 */
public class ServiceOption implements Serializable
{
	private int seqId;
	private int id;
	private String code;
	private String name;
	private String emtServiceOptionName;
	private boolean directedSend = false;
	//Added by Jawahar 
    private String countryCode;
    private String sendCurrency;
    private String localCurrency;
    private String receiveCurrency;
    private boolean indicativeRateAvailable;
	/**
	 * @return Returns the agentManaged.
	 */
	public String getAgentManaged() {
		return agentManaged;
	}
	/**
	 * @param agentManaged The agentManaged to set.
	 */
	public void setAgentManaged(String agentManaged) {
		this.agentManaged = agentManaged;
	}
	/**
	 * @return Returns the countryCode.
	 */
	public String getCountryCode() {
		return countryCode;
	}
	/**
	 * @param countryCode The countryCode to set.
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	/**
	 * @return Returns the indicativeRateAvailable.
	 */
	public boolean isIndicativeRateAvailable() {
		return indicativeRateAvailable;
	}
	/**
	 * @param indicativeRateAvailable The indicativeRateAvailable to set.
	 */
	public void setIndicativeRateAvailable(boolean indicativeRateAvailable) {
		this.indicativeRateAvailable = indicativeRateAvailable;
	}
	/**
	 * @return Returns the localCurrency.
	 */
	public String getLocalCurrency() {
		return localCurrency;
	}
	/**
	 * @param localCurrency The localCurrency to set.
	 */
	public void setLocalCurrency(String localCurrency) {
		this.localCurrency = localCurrency;
	}
	/**
	 * @return Returns the mgManaged.
	 */
	public String getMgManaged() {
		return mgManaged;
	}
	/**
	 * @param mgManaged The mgManaged to set.
	 */
	public void setMgManaged(String mgManaged) {
		this.mgManaged = mgManaged;
	}
	/**
	 * @return Returns the receiveAgentAbbr.
	 */
	public String getReceiveAgentAbbr() {
		return receiveAgentAbbr;
	}
	/**
	 * @param receiveAgentAbbr The receiveAgentAbbr to set.
	 */
	public void setReceiveAgentAbbr(String receiveAgentAbbr) {
		this.receiveAgentAbbr = receiveAgentAbbr;
	}
	/**
	 * @return Returns the receiveAgentID.
	 */
	public String getReceiveAgentID() {
		return receiveAgentID;
	}
	/**
	 * @param receiveAgentID The receiveAgentID to set.
	 */
	public void setReceiveAgentID(String receiveAgentID) {
		this.receiveAgentID = receiveAgentID;
	}
	/**
	 * @return Returns the receiveCurrency.
	 */
	public String getReceiveCurrency() {
		return receiveCurrency;
	}
	/**
	 * @param receiveCurrency The receiveCurrency to set.
	 */
	public void setReceiveCurrency(String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}
	/**
	 * @return Returns the sendCurrency.
	 */
	public String getSendCurrency() {
		return sendCurrency;
	}
	/**
	 * @param sendCurrency The sendCurrency to set.
	 */
	public void setSendCurrency(String sendCurrency) {
		this.sendCurrency = sendCurrency;
	}
    private String receiveAgentID;
    private String receiveAgentAbbr;
    private String mgManaged;
    private String agentManaged;	
    
	/**
	 * @return Returns the seqId.
	 */
	public int getSeqId() {
		return seqId;
	}
	/**
	 * @param seqId The seqId to set.
	 */
	public void setSeqId(int seqId) {
		this.seqId = seqId;
	}
	
	

	/**
	 * @return
	 */
	public String getCode()
	{
		return code;
	}

	/**
	 * @return
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @return
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param string
	 */
	public void setCode(String string)
	{
		code = string;
	}

	/**
	 * @param i
	 */
	public void setId(int i)
	{
		id = i;
	}

	/**
	 * @param string
	 */
	public void setName(String string)
	{
		name = string;
	}

    /**
     * @return Returns the emtServiceOptionName.
     */
    public String getEmtServiceOptionName() {
        return emtServiceOptionName;
    }
    /**
     * @param emtServiceOptionName The emtServiceOptionName to set.
     */
    public void setEmtServiceOptionName(String string) {
        this.emtServiceOptionName = string;
    }
    /**
     * @return Returns the directedSend.
     */
    public boolean isDirectedSend() {
        return directedSend;
    }
    /**
     * @param directedSend The directedSend to set.
     */
    public void setDirectedSend(boolean value) {
        this.directedSend = value;
    }
}
