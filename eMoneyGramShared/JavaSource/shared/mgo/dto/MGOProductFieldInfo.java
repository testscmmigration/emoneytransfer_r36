/*
 * Created on Oct 5, 2006
 *
 */
package shared.mgo.dto;

import java.util.Map;

/**
 * @author T004
 *
 */
public class MGOProductFieldInfo{
    private String rrnVisibility;
    private boolean lastName2Required;
    private boolean messageField1Allowed;
    private String deliveryOption;
    private String receiveCountry;
    private String receiveCurrency;
    private String receiveAgentID;
    private Map productFieldInfo;
    
    
    
	/**
	 * @return Returns the deliveryOption.
	 */
	public String getDeliveryOption() {
		return deliveryOption;
	}
	/**
	 * @param deliveryOption The deliveryOption to set.
	 */
	public void setDeliveryOption(String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}
	/**
	 * @return Returns the lastName2Required.
	 */
	public boolean isLastName2Required() {
		return lastName2Required;
	}
	/**
	 * @param lastName2Required The lastName2Required to set.
	 */
	public void setLastName2Required(boolean lastName2Required) {
		this.lastName2Required = lastName2Required;
	}
	/**
	 * @return Returns the productFieldInfo.
	 */
	public Map getProductFieldInfo() {
		return productFieldInfo;
	}
	/**
	 * @param productFieldInfo The productFieldInfo to set.
	 */
	public void setProductFieldInfo(Map productFieldInfo) {
		this.productFieldInfo = productFieldInfo;
	}
	/**
	 * @return Returns the receiveCountry.
	 */
	public String getReceiveCountry() {
		return receiveCountry;
	}
	/**
	 * @param receiveCountry The receiveCountry to set.
	 */
	public void setReceiveCountry(String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	/**
	 * @return Returns the rrnVisibility.
	 */
	public String getRrnVisibility() {
		return rrnVisibility;
	}
	/**
	 * @param rrnVisibility The rrnVisibility to set.
	 */
	public void setRrnVisibility(String rrnVisibility) {
		this.rrnVisibility = rrnVisibility;
	}
	/**
	 * @return Returns the receiveAgentID.
	 */
	public String getReceiveAgentID() {
		return receiveAgentID;
	}
	/**
	 * @param receiveAgentID The receiveAgentID to set.
	 */
	public void setReceiveAgentID(String receiveAgentID) {
		this.receiveAgentID = receiveAgentID;
	}
	/**
	 * @return Returns the receiveCurrency.
	 */
	public String getReceiveCurrency() {
		return receiveCurrency;
	}
	/**
	 * @param receiveCurrency The receiveCurrency to set.
	 */
	public void setReceiveCurrency(String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}
	/**
	 * @return Returns the messageField1Allowed.
	 */
	public boolean isMessageField1Allowed() {
		return messageField1Allowed;
	}
	/**
	 * @param messageField1Allowed The messageField1Allowed to set.
	 */
	public void setMessageField1Allowed(boolean messageField1Allowed) {
		this.messageField1Allowed = messageField1Allowed;
	}	
}
