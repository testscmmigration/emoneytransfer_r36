/*
 * Created on Aug 31, 2006
 *
 */
package shared.mgo.dto;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author T004
 *
 */
public class MGOCountry implements Comparable, Serializable
{
    private String countryCode;
    private String countryName;
    private String countryLegacyCode;
    private boolean sendActive;
    private boolean receiveActive;
    private boolean directedSendCountry;
//    private boolean mgDirectedSendCountry;
    private boolean multiCurrencyCountry;
    private String baseReceiveCurrency;
    
    private boolean esCaliforniaAllowed;
    private boolean mgCaliforniaAllowed;
    private boolean emtDisabled;

    private Collection fQDOList;
    private Collection stateProvinceList;
    private Collection countryCurrencyList;
    private Collection deliveryOptionList;
    private Collection receiveCurrencyList;
    private Collection serviceOptionList;
    
	private String sndAllowFlag;
	private float sndMaxAmtNbr;  
	private float sndThrldWarnAmtNbr;
	
	public static String sortBy = "countryName";
	public static int sortSeq = 1;
	
	/**
	 * @return Returns the sortBy.
	 */
	public static String getSortBy() {
		return sortBy;
	}
	/**
	 * @param sortBy The sortBy to set.
	 */
	public static void setSortBy(String sortBy) {
		MGOCountry.sortBy = sortBy;
	}
	/**
	 * @return Returns the sortSeq.
	 */
	public static int getSortSeq() {
		return sortSeq;
	}
	/**
	 * @param sortSeq The sortSeq to set.
	 */
	public static void setSortSeq(int sortSeq) {
		MGOCountry.sortSeq = sortSeq;
	}
	/**
	 * @return Returns the countryCode.
	 */
	public String getCountryCode() {
		return countryCode;
	}
	/**
	 * @param countryCode The countryCode to set.
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	/**
	 * @return Returns the countryLegacyCode.
	 */
	public String getCountryLegacyCode() {
		return countryLegacyCode;
	}
	/**
	 * @param countryLegacyCode The countryLegacyCode to set.
	 */
	public void setCountryLegacyCode(String countryLegacyCode) {
		this.countryLegacyCode = countryLegacyCode;
	}
	
	/**
	 * @return Returns the countryName truncated to 15 chars
	 */
	public String getCountryNameAbbr() {
	    if (countryName.length() > 18)
			return countryName.substring(0,17);
	    else
	        return countryName;
	}
	/**
	 * @return Returns the countryName.
	 */
	public String getCountryName() {
		return countryName;
	}
	/**
	 * @param countryName The countryName to set.
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	/**
	 * @return Returns the countryCurrencyList.
	 */
	public Collection getCountryCurrencyList() {
		return countryCurrencyList;
	}
	/**
	 * @param countryCurrencyList The countryCurrencyList to set.
	 */
	public void setCountryCurrencyList(Collection countryCurrencyList) {
		this.countryCurrencyList = countryCurrencyList;
	}
	/**
	 * @return Returns the directedSendCountry.
	 */
	public boolean isDirectedSendCountry() {
		return directedSendCountry;
	}
	/**
	 * @param directedSendCountry The directedSendCountry to set.
	 */
	public void setDirectedSendCountry(boolean directedSendCountry) {
		this.directedSendCountry = directedSendCountry;
	}

	/**
	 * @return Returns the emtDisabled.
	 */
	public boolean isEmtDisabled() {
		return emtDisabled;
	}
	/**
	 * @param emtDisabled The emtDisabled to set.
	 */
	public void setEmtDisabled(boolean emtDisabled) {
		this.emtDisabled = emtDisabled;
	}
	/**
	 * @return Returns the esCaliforniaAllowed.
	 */
	public boolean isEsCaliforniaAllowed() {
		return esCaliforniaAllowed;
	}
	/**
	 * @param esCaliforniaAllowed The esCaliforniaAllowed to set.
	 */
	public void setEsCaliforniaAllowed(boolean esCaliforniaAllowed) {
		this.esCaliforniaAllowed = esCaliforniaAllowed;
	}
	/**
	 * @return Returns the fQDOList.
	 */
	public Collection getFQDOList() {
		return fQDOList;
	}
	/**
	 * @param list The fQDOList to set.
	 */
	public void setFQDOList(Collection list) {
		fQDOList = list;
	}
	/**
	 * @return Returns the mgCaliforniaAllowed.
	 */
	public boolean isMgCaliforniaAllowed() {
		return mgCaliforniaAllowed;
	}
	/**
	 * @param mgCaliforniaAllowed The mgCaliforniaAllowed to set.
	 */
	public void setMgCaliforniaAllowed(boolean mgCaliforniaAllowed) {
		this.mgCaliforniaAllowed = mgCaliforniaAllowed;
	}
	/**
	 * @return Returns the receiveActive.
	 */
	public boolean isReceiveActive() {
		return receiveActive;
	}
	/**
	 * @param receiveActive The receiveActive to set.
	 */
	public void setReceiveActive(boolean receiveActive) {
		this.receiveActive = receiveActive;
	}
	/**
	 * @return Returns the sendActive.
	 */
	public boolean isSendActive() {
		return sendActive;
	}
	/**
	 * @param sendActive The sendActive to set.
	 */
	public void setSendActive(boolean sendActive) {
		this.sendActive = sendActive;
	}
	/**
	 * @return Returns the stateProvinceList.
	 */
	public Collection getStateProvinceList() {
		return stateProvinceList;
	}
	/**
	 * @param stateProvinceList The stateProvinceList to set.
	 */
	public void setStateProvinceList(Collection stateProvinceList) {
		this.stateProvinceList = stateProvinceList;
	}

	/**
	 * @return Returns the baseReceiveCurrency.
	 */
	public String getBaseReceiveCurrency() {
		return baseReceiveCurrency;
	}
	/**
	 * @param baseReceiveCurrency The baseReceiveCurrency to set.
	 */
	public void setBaseReceiveCurrency(String baseReceiveCurrency) {
		this.baseReceiveCurrency = baseReceiveCurrency;
	}	
	
	public int compareTo(Object o)
	{
		MGOCountry acc = (MGOCountry) o;

		if (MGOCountry.sortBy.equalsIgnoreCase("countryName"))
		{
			return (countryName.compareToIgnoreCase(acc.countryName)) * sortSeq;
		}

		if (MGOCountry.sortBy.equalsIgnoreCase("countryCode"))
		{
			return (countryCode.compareToIgnoreCase(acc.countryCode)) * sortSeq;
		}

		return 0;
	}
	
	/**
	 * @return Returns the deliveryOptionList.
	 */
	public Collection getDeliveryOptionList() {
		return deliveryOptionList;
	}
	/**
	 * @param deliveryOptionList The deliveryOptionList to set.
	 */
	public void setDeliveryOptionList(Collection deliveryOptionList) {
		this.deliveryOptionList = deliveryOptionList;
	}
	/**
	 * @return Returns the multiCurrencyCountry.
	 */
	public boolean isMultiCurrencyCountry() {
		return multiCurrencyCountry;
	}
	/**
	 * @param multiCurrencyCountry The multiCurrencyCountry to set.
	 */
	public void setMultiCurrencyCountry(boolean multiCurrencyCountry) {
		this.multiCurrencyCountry = multiCurrencyCountry;
	}
	/**
	 * @return Returns the sndAllowFlag.
	 */
	public String getSndAllowFlag() {
		return sndAllowFlag;
	}
	/**
	 * @param sndAllowFlag The sndAllowFlag to set.
	 */
	public void setSndAllowFlag(String sndAllowFlag) {
		this.sndAllowFlag = sndAllowFlag;
	}
	/**
	 * @return Returns the sndMaxAmtNbr.
	 */
	public float getSndMaxAmtNbr() {
		return sndMaxAmtNbr;
	}
	/**
	 * @param sndMaxAmtNbr The sndMaxAmtNbr to set.
	 */
	public void setSndMaxAmtNbr(float sndMaxAmtNbr) {
		this.sndMaxAmtNbr = sndMaxAmtNbr;
	}
	/**
	 * @return Returns the sndThrldWarnAmtNbr.
	 */
	public float getSndThrldWarnAmtNbr() {
		return sndThrldWarnAmtNbr;
	}
	/**
	 * @param sndThrldWarnAmtNbr The sndThrldWarnAmtNbr to set.
	 */
	public void setSndThrldWarnAmtNbr(float sndThrldWarnAmtNbr) {
		this.sndThrldWarnAmtNbr = sndThrldWarnAmtNbr;
	}
	/**
	 * @return Returns the receiveCurrencyList.
	 */
	public Collection getReceiveCurrencyList() {
		return receiveCurrencyList;
	}
	/**
	 * @param receiveCurrencyList The receiveCurrencyList to set.
	 */
	public void setReceiveCurrencyList(Collection receiveCurrencyList) {
		this.receiveCurrencyList = receiveCurrencyList;
	}
	/**
	 * @return Returns the serviceOptionList.
	 */
	public Collection getServiceOptionList() {
		return serviceOptionList;
	}
	/**
	 * @param serviceOptionList The serviceOptionList to set.
	 */
	public void setServiceOptionList(Collection serviceOptionList) {
		this.serviceOptionList = serviceOptionList;
	}
	
	 
}
