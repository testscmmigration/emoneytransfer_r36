package emgadm.dataaccessors;

import java.util.List;

import emgshared.exceptions.DataSourceException;
import emgadm.exceptions.InactiveUserException;
import emgadm.exceptions.InvalidUserException;
import emgadm.exceptions.LockedUserException;
import emgadm.exceptions.UserAuthenticationException;
import emgadm.exceptions.UserNotFoundException;
import emgadm.model.Role;
import emgadm.model.UserProfile;
import emgadm.model.UserQueryCriterion;

/**
 * @author A132
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface UserProfileManager
{
	public static final String HIDDEN_PASSWORD = "********"; //$NON-NLS-1$
	public UserProfile authenticate(String userId, String password) throws DataSourceException, UserAuthenticationException, LockedUserException, InvalidUserException;
	public void updateUser(UserProfile modifyUser, List<Role> newRole) throws DataSourceException, UserNotFoundException;
	public void updateEmailPassword(String userID, String newPassword) throws DataSourceException, UserNotFoundException;
	public UserProfile getUser(String uid) throws DataSourceException, InvalidUserException, UserNotFoundException;
	public UserProfile getUserByUserId(String guid) throws DataSourceException, InvalidUserException;
	public List<UserProfile> findHollowUsers(UserQueryCriterion uqc) throws DataSourceException;
	public boolean isLoginAuthorized(UserProfile up) throws InactiveUserException, DataSourceException;
	//public boolean isPasswordExpired(String userId) throws DataSourceException, UserNotFoundException, UserAuthenticationException;
}
