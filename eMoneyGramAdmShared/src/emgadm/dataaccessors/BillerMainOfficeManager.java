package emgadm.dataaccessors;

import java.util.Collection;
import java.util.List;

import emgshared.exceptions.DataSourceException;

public interface BillerMainOfficeManager
{
	abstract Collection getMainOffices() throws DataSourceException;
	abstract List getLocations(String agentId) throws DataSourceException;
}
