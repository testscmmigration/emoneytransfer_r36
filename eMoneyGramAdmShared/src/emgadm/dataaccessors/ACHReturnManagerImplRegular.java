package emgadm.dataaccessors;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.apache.log4j.Logger;

import oracle.jdbc.OracleTypes;
import emgadm.exceptions.TooManyResultException;
import emgadm.model.AchReturn;
import emgadm.model.AchReturnSearchRequest;
import emgadm.model.AchReturnStatus;
import emgadm.model.AchReturnType;
import emgadm.util.StringHelper;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.dataaccessors.OracleAccess;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.property.EMTSharedDynProperties;

class ACHReturnManagerImplRegular implements ACHReturnManager {

	private static final EMTSharedDynProperties dynProps = new EMTSharedDynProperties();
	public static final String dbCallTypeCode = "WEB";
	private static final ACHReturnManager _instance = new ACHReturnManagerImplRegular();
	private static final Logger LOG = EMGSharedLogger.getLogger(
			ACHReturnManagerImplRegular.class);

	private ACHReturnManagerImplRegular() {
	}

	static ACHReturnManager instance() {
		return _instance;
	}

	
	public Collection getAchReturnQueue(String userId, AchReturnSearchRequest acsr)
			throws DataSourceException, SQLException, TooManyResultException {
		
		Collection transactions = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		int maxDownloadTransactions = dynProps.getMaxDownloadTransactions();


		String storedProcName = "pkg_em_transactions.prc_get_emg_ach_returns_cv";
		StringBuilder sqlBuffer = new StringBuilder(64);
		sqlBuffer.append("{ call " + storedProcName
				+ "(?,?,?,?,?,?, ?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			//  input parameters
			int parm = 0;
			
			cs.setString(++parm, userId);
			cs.setString(++parm, dbCallTypeCode);
			
			if (StringHelper.isNullOrEmpty(acsr.getAchEffDateBegin())) {
				cs.setNull(++parm, Types.VARCHAR);
			} else {
				cs.setString(++parm, acsr.getAchEffDateBegin());
			}
			
			if (StringHelper.isNullOrEmpty(acsr.getAchEffDateEnd())) {
				cs.setNull(++parm, Types.VARCHAR);
			} else {
				cs.setString(++parm, acsr.getAchEffDateEnd());
			}
			
			if (StringHelper.isNullOrEmpty(acsr.getAchTranType())) {
				cs.setNull(++parm, Types.VARCHAR);
			} else {
				cs.setString(++parm, acsr.getAchTranType());
			}
			
			if (StringHelper.isNullOrEmpty(acsr.getAchTranStatusCode())) {
				cs.setNull(++parm, Types.VARCHAR);
			} else {
				cs.setString(++parm, acsr.getAchTranStatusCode());
			}
			
			cs.registerOutParameter(++parm, Types.INTEGER);
			int dbLogIdIndex = parm;
			cs.registerOutParameter(++parm, OracleTypes.CURSOR);

			cs.execute();
			rs = (ResultSet) cs.getObject(parm);

			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.debug("dbLogId = " + dbLogId);

			int cnt = 0;

			while (rs.next()) {
				AchReturn achReturn = new AchReturn();
				
				achReturn.setAchReturnTranId(rs.getInt("ACH_RTN_TRAN_ID"));
				achReturn.setReturnStatCode(rs.getString("ACH_RTN_TRAN_STAT_CODE"));
				achReturn.setReturnStatDesc(rs.getString("ACH_RTN_TRAN_STAT_DESC"));
				
				if (rs.getString("EMG_TRAN_ID")==null) {
					achReturn.setTranId(null);
				} else {
					achReturn.setTranId(Integer.valueOf(rs.getInt("EMG_TRAN_ID")));
				}
				if (rs.getString("MIC_DEP_VLDN_ID")==null) {
					achReturn.setMicroTranId(null);
				} else {
					achReturn.setMicroTranId(Integer.valueOf(rs.getInt("MIC_DEP_VLDN_ID")));
				}
				achReturn.setReturnTypeDesc(rs.getString("ACH_PRVDR_RTN_TYPE_DESC"));
				achReturn.setReturnReasonDesc(rs.getString("ACH_PRVDR_RTN_REAS_DESC"));
				achReturn.setCreditAmount(rs.getDouble("ACH_PRVDR_CR_AMT"));
				achReturn.setDebitAmount(rs.getDouble("ACH_PRVDR_DR_AMT"));
				achReturn.setEffectiveDate(rs.getDate("ACH_PRVDR_TRAN_EFF_DATE"));
				achReturn.setIndividualName(rs.getString("ACH_PRVDR_INDVDL_ID"));
				achReturn.setLastUpdateDate(rs.getDate("LAST_UPDATE_DATE"));
				achReturn.setLastUpdateUserId(rs.getString("LAST_UPDATE_USERID"));

				transactions.add(achReturn);
				
				if (cnt++ > maxDownloadTransactions) {
					throw new TooManyResultException(
							"Too many results and maximum download transaction is "
									+ maxDownloadTransactions);
				}
			}
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

		return transactions;
	}

	public Collection getAchReturnStatus(String userId) throws DataSourceException {

		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		ArrayList achReturnStatuses = new ArrayList();

		try {
			conn = OracleAccess.getConnection();
			String storedProcName = "pkg_em_status_and_type.prc_get_ach_return_status_cv";
			cs = conn.prepareCall("{ call " + storedProcName
					+ "(?,?, ?, ?,?) }");
			
			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);

			cs.setNull(++paramIndex, Types.VARCHAR);

			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramTransactionIndex = paramIndex;

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.debug(
								storedProcName + " started: "
										+ System.currentTimeMillis());
			}
			cs.execute();
			
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.debug(
								storedProcName + " ended: "
										+ System.currentTimeMillis());
				long dbLogId = cs.getLong(dbLogIdIndex);
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.debug("dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(paramTransactionIndex);

			while (rs.next()) {
				AchReturnStatus achReturnStatus = new AchReturnStatus();
				achReturnStatus.setAchReturnStatCode(rs.getString("ACH_RTN_TRAN_STAT_CODE"));
				achReturnStatus.setAchReturnStatDesc(rs.getString("ACH_RTN_TRAN_STAT_DESC"));
				achReturnStatuses.add(achReturnStatus);
			}
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return achReturnStatuses;
	}
	
	public AchReturn getAchReturn(String userId, int achReturnTranId) {

		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		AchReturn achReturn = new AchReturn();

		String storedProcName = "pkg_em_transactions.prc_get_emg_ach_return_cv";
		StringBuilder sqlBuffer = new StringBuilder(64);
		sqlBuffer.append("{ call " + storedProcName
				+ "(?,?,?, ?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			//  input parameters
			int parm = 0;
			
			cs.setString(++parm, userId);
			cs.setString(++parm, dbCallTypeCode);
			cs.setInt(++parm, achReturnTranId);
			
			cs.registerOutParameter(++parm, Types.INTEGER);
			int dbLogIdIndex = parm;
			cs.registerOutParameter(++parm, OracleTypes.CURSOR);

			cs.execute();
			rs = (ResultSet) cs.getObject(parm);

			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.debug("dbLogId = " + dbLogId);

			while (rs.next()) {
				achReturn.setAchReturnTranId(rs.getInt("ACH_RTN_TRAN_ID"));
				achReturn.setFileControlSequenceNumber(rs.getInt("ACH_RTN_FILE_CNTL_SEQ_NBR"));
				achReturn.setReturnStatCode(rs.getString("ACH_RTN_TRAN_STAT_CODE"));
				achReturn.setReturnStatDesc(rs.getString("ACH_RTN_TRAN_STAT_DESC"));
				
				if (rs.getString("EMG_TRAN_ID")==null) {
					achReturn.setTranId(null);
				} else {
					achReturn.setTranId(Integer.valueOf(rs.getInt("EMG_TRAN_ID")));
				}
				if (rs.getString("MIC_DEP_VLDN_ID")==null) {
					achReturn.setMicroTranId(null);
				} else {
					achReturn.setMicroTranId(Integer.valueOf(rs.getInt("MIC_DEP_VLDN_ID")));
				}
				
				achReturn.setAsOfDate(rs.getDate("ACH_PRVDR_AS_OF_DATE"));
				achReturn.setFileId(rs.getString("ACH_PRVDR_FILE_ID"));
				achReturn.setCompanyId(rs.getString("ACH_PRVDR_CO_ID"));

				achReturn.setSettlementBank(rs.getString("ACH_PRVDR_SETTLMT_ABA_NBR"));
				achReturn.setSettlementAccount(rs.getString("ACH_PRVDR_SETTLMT_ACCT_NBR"));
				achReturn.setReturnTypeCode(rs.getString("ACH_PRVDR_RTN_TYPE_CODE"));
				achReturn.setAccountHolderName(rs.getString("ACCT_HOLDR_NAME"));
				achReturn.setEffectiveDate(rs.getDate("ACH_PRVDR_TRAN_EFF_DATE"));
				
				achReturn.setDescriptiveDate(rs.getString("ACH_PRVDR_DESC_DATE"));
				achReturn.setBankNo(rs.getString("CUST_BANK_ABA_NBR"));
				achReturn.setAccountNo(rs.getString("CUST_ACCT_NBR"));
				achReturn.setReturnTraceNo(rs.getString("ACH_PRVDR_RTN_TRC_NBR"));
				
				achReturn.setAccountType(rs.getString("ACH_PRVDR_ACCT_TYPE_CODE"));
				achReturn.setReturnReasonCode(rs.getString("ACH_PRVDR_RTN_REAS_CODE"));
				achReturn.setOriginalTraceNo(rs.getString("ACH_PRVDR_ORIG_TRC_NBR"));
				achReturn.setSearchResult(rs.getString("ACH_PRVDR_SRCH_RSLT_DESC"));
				
				achReturn.setReturnTypeDesc(rs.getString("ACH_PRVDR_RTN_TYPE_DESC"));
				achReturn.setReturnReasonDesc(rs.getString("ACH_PRVDR_RTN_REAS_DESC"));
				achReturn.setCreditAmount(rs.getDouble("ACH_PRVDR_CR_AMT"));
				achReturn.setDebitAmount(rs.getDouble("ACH_PRVDR_DR_AMT"));
				achReturn.setIndividualName(rs.getString("ACH_PRVDR_INDVDL_ID"));
				achReturn.setLastUpdateDate(rs.getDate("LAST_UPDATE_DATE"));
				achReturn.setLastUpdateUserId(rs.getString("LAST_UPDATE_USERID"));
			}
		} catch (Exception e) {
			throw new EMGRuntimeException(e);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

		return achReturn;
	}


	public int setAchReturnStatus(String userId, int achTranId, String status) {
		Double rc = Double.valueOf(-1);
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName = "pkg_em_transactions.prc_update_ach_transaction";
		StringBuilder sqlBuffer = new StringBuilder(64);
		sqlBuffer.append("{ call " + storedProcName
				+ "(?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			//  input parameters
			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, achTranId); 
			cs.setString(++paramIndex, status); 

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.FLOAT);
			int outParamIndex = paramIndex;

			cs.execute();

			long dbLogId = cs.getLong(outParamIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.debug("dbLogId = " + dbLogId);

			rc = (Double) cs.getObject(outParamIndex);
			safeCommit(conn);

		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				LOG.error("Error occoured during commit.....");
			}
			throw new EMGRuntimeException(e);
		} finally {
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return rc.intValue();

	}

	private void safeCommit(java.sql.Connection conn) {
		try {
			conn.commit();
		} catch (Exception e) {
			LOG.error("WARN: Commit was not possible. This might be possible and acceptable in Java EE environments: " + e.getMessage());
		}
	}

	public Collection getAchTranTypes() throws DataSourceException, SQLException {
		return Arrays.asList( AchReturnType.ALL_TYPES );
	}
	

}
