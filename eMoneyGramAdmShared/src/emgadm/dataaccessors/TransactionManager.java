package emgadm.dataaccessors;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import emgadm.exceptions.TooManyResultException;
import emgadm.model.AchReturn;
import emgadm.model.ChargebackTrackingSearchRequest;
import emgadm.model.ChargebackTransactionHeader;
import emgadm.model.ConsumerProfileDashboard;
import emgadm.model.CustAddress;
import emgadm.model.GlAccount;
import emgadm.model.PartnerSite;
import emgadm.model.TranAction;
import emgadm.model.TranType;
import emgshared.exceptions.DataSourceException;
import emgshared.model.ConsumerProfile;
import emgshared.model.EMTTransactionType;
import emgshared.model.MicroDeposit;
import emgshared.model.Transaction;
import emgshared.model.TransactionSearchRequest;
import emgshared.model.TransactionStatus;
import emgshared.services.CyberDMData;
import emgshared.services.ScoringData;

public interface TransactionManager {
	Transaction getTransaction(int tranId);

	int setTransactionOwnership(int tranId, String userId);

	int setTransactionStatus(int tranId, String status, String subStatus, String userId);

	int setTransactionStatus(int tranId, TransactionStatus status, String userId);

	void updateTransaction(Transaction tran, String userId);

	void updateTransaction(Transaction tran, String userId, String actionConfId, Float postAmount);

	void updateTransaction(Transaction tran, String userId, String actionConfId, Float postAmount, String subReasonCode);

	Collection getTransactionQueue(TransactionSearchRequest tsr) throws DataSourceException, SQLException, TooManyResultException;

	Collection getTransactionQueueDocStatus(TransactionSearchRequest tsr) throws DataSourceException, SQLException, TooManyResultException;

	Collection getTransactionComments(int tranId) throws DataSourceException, SQLException;

	Collection getTransactionCommentReasons() throws DataSourceException, SQLException;

	int setTransactionComment(int tranId, String reasonCode, String comment, String userId) throws DataSourceException, SQLException;

	Collection getTranStatusCodes() throws DataSourceException, SQLException;

	Collection getTranSubStatusCodes() throws DataSourceException, SQLException;

	Collection<TranType> getTranTypes(int partnerSiteCode) throws DataSourceException, SQLException;

	Collection getProcessUsers() throws DataSourceException, SQLException;

	Collection getAccountTypes() throws DataSourceException, SQLException;

	Collection getJournalEntries(int tranId, String beginDate, String endDate) throws DataSourceException, SQLException,
			TooManyResultException;

	List getTransactions(TransactionSearchRequest searchCriteria, String callerLoginId) throws DataSourceException, TooManyResultException;

	String getConsumerEmail(int custId) throws DataSourceException, SQLException;

	String getCCAuthConfId(int tranId, String reasonCode) throws DataSourceException, SQLException;

	List getTranActions(int tranId, String logonId) throws DataSourceException, SQLException;

	int[] getQueueCounts() throws DataSourceException, SQLException;

	List getIpHistorySummaryList(String searchLogonId, String logonId) throws DataSourceException, SQLException;

	List getIpHistoryDetailList(String searchLogonId, String logonId) throws DataSourceException, SQLException;

	List getLogonIpConsumerList(String ipAddress, String logonId) throws DataSourceException, SQLException;

	CustAddress getCustAddress(int addressId) throws DataSourceException;

    List<GlAccount> getGLAccounts(String userId) throws DataSourceException, SQLException;

	void insertGLAdjustment(TranAction glAdjustment, int agentId, String callerLoginId) throws DataSourceException;

	List getVBVBeans(int tranId);

	void updateTransactionType(String userId, EMTTransactionType tranType) throws DataSourceException;

	boolean isScoringConfigurationEditable(String userId, int ScoreConfigId) throws DataSourceException;

	boolean canTakeoverFromSysOwner(Transaction tran);

	boolean checkASPTran(Transaction tran, String userId);

	Transaction getTransactionForAutoProcess(String userId,
				String tranStatCode, String tranSubStatCode, String csrUserId,
				Date createDateTime,  String partnerSiteId, String tranTypeCode,Date updateDateTime)
			throws DataSourceException;

	Collection getDeadTransactions();

	int[] getLimboASPTransactions();

	ConsumerProfileDashboard getConsumerProfileDashboard(ConsumerProfile consumerProfile, String userId);

	List getTranReasons(String userId, String tranStatCode, String tranSubStatCode) throws DataSourceException;

	public List getTransactionsWithCmnts(TransactionSearchRequest searchCriteria, String callerLoginId) throws DataSourceException;

	Collection getChargebackStatuses() throws DataSourceException, SQLException;

	Collection getGenderTypes() throws DataSourceException, SQLException;

	Collection getCustomerContactMethodTypes() throws DataSourceException, SQLException;

	Collection getChargebackDenialTypes() throws DataSourceException, SQLException;

	ChargebackTransactionHeader getChargebackTransactionHeader(String userId, int tranId) throws DataSourceException, SQLException;

	Collection getChargebackComments(String userId, int tranId) throws DataSourceException, SQLException;

	Collection getChargebackActions(String userId, int tranId) throws DataSourceException, SQLException;

	Collection getChargebackProviderCase(String userId, int tranId) throws DataSourceException, SQLException;

	void insertChargebackTranComment(String userId, int tranId, String comment) throws DataSourceException;

	void insertChargebackProviderCase(String userId, int tranId, String denialCode, String providerCaseId, Date originatorStatementDate)
			throws DataSourceException;

	void insertUpdateChargebackTransaction(String userId, int tranId, String chargebackStatusCode, Date chargebackTrackingDate,
			String genderType, String customerContactMethodFlag, String emailConsistentFlag, String phoneVerifiedFlag,
			String customerVerbalContactFlag, String unsolicitPhoneCallFlag, String customerProfileValidationFlag,
			String sendReceiverDistance, String linkedCustomerProfiles) throws DataSourceException;

	void updateFileControl(int iFileControlSeqNbr, int iRecCnt, int iSuccessCount, int iFailCount) throws DataSourceException;

	int insertFileControl(String userId, String processName) throws DataSourceException;

	Collection getLatestFileControl(String processName) throws DataSourceException;

	void deletePhoneAreaCode() throws DataSourceException;

	void insertPhoneAreaCode(String userId, String countryCode, String areaCode, String stateName) throws DataSourceException;

	void insertEmployeeRecord(String userId, String empLastName, String empFirstName, String empMidName, String ssnMask,
			int fileCtrlNumber, String empStatCode) throws DataSourceException;

	void inactivateEmployeeList(String userId) throws DataSourceException;

	void insertAchReturn(String userId, int fileControlSeqNbr, AchReturn achReturn) throws DataSourceException;

	Collection getChargebackTrackingQueue(String userId, ChargebackTrackingSearchRequest searchRequest) throws DataSourceException,
			SQLException, TooManyResultException;

	MicroDeposit getMicroDepTrans(String userId, int microValidationId) throws DataSourceException, SQLException;

	Collection getAllTranSubStatus() throws DataSourceException, SQLException;

	public List getTransactionsForVelocityCheck(Integer consumerId, String callerLoginId) throws Exception;

	public Collection<PartnerSite> getPartnerSiteIds(String status) throws DataSourceException, SQLException;

	public Transaction setMerchantId(Transaction tran);

	public void saveTransactionFraudDetectCodes(String userId, int tranId, String type, String codesList, String providerCode);
	
	//added by Ankit Bhatt for 285
	public CyberDMData getCybersourceDMData(int tranId,String userId) throws Exception;
	
	
	//ended
	//EMT-2516
	public ScoringData getActimizeTriggeredRules (int tranId, Date createDateTime) throws SQLException;

}