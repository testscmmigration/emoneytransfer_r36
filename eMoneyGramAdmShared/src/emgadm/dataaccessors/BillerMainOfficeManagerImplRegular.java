package emgadm.dataaccessors;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import oracle.jdbc.OracleTypes;
import emgadm.model.Agent;
import emgadm.model.AgentBuilder;
import emgadm.util.StringHelper;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.dataaccessors.OracleAccess;
import emgshared.exceptions.DataSourceException;

public class BillerMainOfficeManagerImplRegular implements
        BillerMainOfficeManager {

    private static BillerMainOfficeManager _instance = new BillerMainOfficeManagerImplRegular();

    private BillerMainOfficeManagerImplRegular() {
    }

    static BillerMainOfficeManager instance() {
        return _instance;
    }

    public Collection getMainOffices() throws DataSourceException {
        try {
            return getAgents(null, Agent.MAIN_OFFICE_LEVEL);
        } catch (SQLException e) {
            EMGSharedLogger.getLogger(this.getClass().getName().toString())
                    .error(e.getMessage(), e);
            throw new DataSourceException(e.getMessage());
        }
    }

    public List getLocations(String mainOfficeAgentId)
            throws DataSourceException {
        try {
            return getAgents(mainOfficeAgentId, Agent.LOCATION_LEVEL);
        } catch (SQLException e) {
            EMGSharedLogger.getLogger(this.getClass().getName().toString())
                    .error(e.getMessage(), e);
            throw new DataSourceException(e.getMessage());
        }
    }

    private List getAgents(String agentId, int hierarchyLevel)
            throws SQLException, DataSourceException {
        List agents = new ArrayList();
        ResultSet rs = null;
        CallableStatement cs = null;
        Connection conn = null;

        boolean searchForMainOffices = hierarchyLevel == 1;
        String storedProcName = searchForMainOffices ? "pkg_em_agent_list.prc_get_mainoffice_id_cv"
                : "pkg_em_agent_list.prc_get_location_agent_cv";
        StringBuilder sqlBuffer = new StringBuilder(64);
        sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

        try {
            conn = OracleAccess.getConnection();
            cs = conn.prepareCall(sqlBuffer.toString());
            int paramIndex = 0;
            int cursorParamIndex = 0;

            cs.setNull(++paramIndex, Types.VARCHAR);
            cs.setNull(++paramIndex, Types.VARCHAR);

            if (StringHelper.isNullOrEmpty(agentId)) {
                cs.setNull(++paramIndex, Types.INTEGER);
            } else {
                cs.setInt(++paramIndex, Integer.parseInt(agentId));
            }

            cs.registerOutParameter(++paramIndex, Types.INTEGER);
            cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
            cursorParamIndex = paramIndex;

            cs.execute();

            rs = (ResultSet) cs.getObject(cursorParamIndex);

            while (rs.next()) {
                if (searchForMainOffices
                        || (!searchForMainOffices && !StringHelper
                                .isNullOrEmpty(rs.getString("agt_rcv_agency")))) {
                    AgentBuilder agentBuilder = new AgentBuilder();
                    agentBuilder.setAgentId(rs.getString("agent_id"));
                    agentBuilder.setHierarchyLevel(hierarchyLevel);
                    agentBuilder.setAgentParentID(rs
                            .getString("parent_agent_id"));
                    agentBuilder.setAgentCity(rs.getString("city"));
                    agentBuilder.setAgentName(rs.getString("agent_name"));
                    agentBuilder.setStatus(rs.getString("agent_status"));
                    agentBuilder.setReceiveCode(rs.getString("agt_rcv_agency"));
                    if (!searchForMainOffices) {
                        agentBuilder.setAgentState(rs
                                .getString("state_province"));
                    } else {
                        agentBuilder.setAgentState("");
                    }
                    agents.add(agentBuilder.buildAgent());
                    agentBuilder = null;
                }
            }
        } finally {
            OracleAccess.close(rs);
            OracleAccess.close(cs);
            OracleAccess.close(conn);
        }

        return agents;
    }
}
