package emgadm.dataaccessors;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;



import emgadm.model.ApplicationCommand;
import emgadm.model.CommandFactory;
import emgadm.model.Program;
import emgadm.model.ProgramFactory;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;

public class CommandManager {
	private static List<ApplicationCommand> allCommands = new ArrayList<ApplicationCommand>();
	private static List<String> commandNames = new ArrayList<String>();
	private static List<Program> allPrograms = new ArrayList<Program>();
	private static final Logger LOG = EMGSharedLogger.getLogger(
			CommandManager.class);

	private static String getChildElementValue(Element parentElement, String childName) {
		try {
			NodeList lstNmElmntLst = parentElement.getElementsByTagName(childName);
			if (lstNmElmntLst.getLength() > 0) {
				Element lstNmElmnt = (Element) lstNmElmntLst.item(0);
				NodeList lstNm = lstNmElmnt.getChildNodes();
				if ((lstNm.getLength() > 0) && (lstNm.item(0) != null)) {
					return lstNm.item(0).getNodeValue();
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return "";
	}

	private static void retreiveCommands() throws DataSourceException {
		InputStream inputStream = null;
		try {
			inputStream = CommandManager.class.getClassLoader().getResourceAsStream("/emtCommands.xml");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			//MBO-0000 fix for Veracode static scan - Starts
			dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
            dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            //MBO-0000 fix for Veracode static scan - Ends
			Document doc = dbf.newDocumentBuilder().parse(new InputSource(inputStream)); 
			//DocumentBuilder db = dbf.newDocumentBuilder();
			doc.getDocumentElement().normalize();
			readCommands(doc);
			readPrograms(doc);
		} catch (Exception e) {
			LOG.error("Error: Loading Application commands from XML",e);
			throw new DataSourceException(e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    LOG.error(e.getMessage());
                }
            }
        }
	}

	private static void readCommands(Document doc) {
		NodeList nodeLst = doc.getElementsByTagName("command");
		for (int s = 0; s < nodeLst.getLength(); s++) {
			Node fstNode = nodeLst.item(s);
			if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
				Element commandElement = (Element) fstNode;
				String menuLevel = getChildElementValue(commandElement, "menuLevel", "0");
				String id = getChildElementValue(commandElement, "id");
				ApplicationCommand command = CommandFactory.createCommand(getChildElementValue(
						commandElement, "displayName"),
						getChildElementValue(commandElement, "menuGroup"), id,
						menuLevel, "true", // allow internal (always for emtAdmin)
						getChildElementValue(commandElement, "authReq", "true"),
						getChildElementValue(commandElement, "description"),
						!"0".equals(menuLevel));
				commandNames.add(id);
				allCommands.add(command);
			}
		}
	}

	private static String getChildElementValue(Element commandElement, String subElementName, String defaultValue) {
		String value = getChildElementValue(commandElement, subElementName);
		return "".equals(value) ? defaultValue : value;
	}

	private static void readPrograms(Document doc) {
		NodeList nodeLst = doc.getElementsByTagName("program");
		for (int s = 0; s < nodeLst.getLength(); s++) {
			Node fstNode = nodeLst.item(s);
			if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
				Element programElement = (Element) fstNode;
				Program program = ProgramFactory.createProgram(getChildElementValue(programElement, "id"),
						getChildElementValue(programElement, "name"), getChildElementValue(programElement, "description"));
				allPrograms.add(program);
			}
		}
	}

	public static List<ApplicationCommand> getCommands() throws DataSourceException {
		synchronized (allCommands) {
			if (allCommands.isEmpty()) {
				retreiveCommands();
			}
			return allCommands;
		}
	}

	public static List<Program> getPrograms() throws DataSourceException {
		synchronized (allPrograms) {
			if (allPrograms.isEmpty()) {
				retreiveCommands();
			}
			return allPrograms;
		}
	}

	public static ApplicationCommand getCommandById(String guid) throws DataSourceException, InvalidGuidException {
		ApplicationCommand appCommand = null;
		for (Iterator<ApplicationCommand> iter = getCommands().iterator(); iter.hasNext();) {
			appCommand = iter.next();
			if (appCommand.getId().equals(guid)) {
				return appCommand;
			}
		}
		if (appCommand == null) {
			throw new InvalidGuidException("Application Command with cn=" + guid + " does not exist.");
		}
		return appCommand;
	}

	public static ApplicationCommand getCommandByCommandName(String command) throws DataSourceException {
		ApplicationCommand appCommand = null;
		for (Iterator<ApplicationCommand> iter = getCommands().iterator(); iter.hasNext();) {
			appCommand = iter.next();
			if (appCommand.getId().equals(command)) {
				return appCommand;
			}
		}
		return appCommand;
	}

	public static boolean contains(String command) throws DataSourceException {
		return getCommandNames().contains(command);
	}

	/**
	 * @return
	 */
	public static List<String> getCommandNames() {
		return commandNames;
	}

	/**
	 * @param list
	 */
	public void setCommandNames(List<String> list) {
		commandNames = list;
	}
}
