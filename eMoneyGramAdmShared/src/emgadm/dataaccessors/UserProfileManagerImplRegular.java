package emgadm.dataaccessors;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.naming.NameClassPair;
import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;

import org.owasp.esapi.ESAPI;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

import emgadm.constants.EMoneyGramAdmLDAPKeys;
import emgadm.exceptions.InactiveUserException;
import emgadm.exceptions.InvalidUserException;
import emgadm.exceptions.UserAuthenticationException;
import emgadm.exceptions.UserNotFoundException;
import emgadm.model.Role;
import emgadm.model.UserProfile;
import emgadm.model.UserProfileFactory;
import emgadm.model.UserQueryCriterion;
import emgadm.property.EMTAdmContainerProperties;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;

class UserProfileManagerImplRegular extends AbstractUserProfileManagerImpl {
	private static final UserProfileManager _instance = new UserProfileManagerImplRegular();
	
	private UserProfileManagerImplRegular() {
	}

	static UserProfileManager getInstance() {
		return _instance;
	}

	public boolean isLoginAuthorized(UserProfile up)
			throws InactiveUserException, DataSourceException {
		boolean authorized = true;

		return (authorized);
	}

	public void updateUser(UserProfile modifyUser, List<Role> newRoles)
			throws UserNotFoundException, DataSourceException {
		DirContext dirContext = null;
		try {
			String ldapKey = EMoneyGramAdmLDAPKeys.USER_ID
					+ "=" + modifyUser.getUID(); //$NON-NLS-1$ 
			String context = EMTAdmContainerProperties.getLDAP_USER_CONTEXT();
			dirContext = JNDIManager.getInitialContext(context);
			ModificationItem item;
			Collection updates = new ArrayList();

			// First we strip all existing roles since we're going to replace
			// the whole set
			item = new ModificationItem(DirContext.REMOVE_ATTRIBUTE,
					new BasicAttribute(EMoneyGramAdmLDAPKeys.USER_ROLES));

			updates.add(item);

			Attributes existingAttributes = dirContext.getAttributes(ldapKey);

			NamingEnumeration ane = existingAttributes.getAll();
			while (ane.hasMore()) {
				Attribute attr = (Attribute) ane.next();
				String attrType = attr.getID();
				if (attrType.equals(EMoneyGramAdmLDAPKeys.USER_ROLES)) {
					updates.addAll(getRoleMods(attr, newRoles));
				}
			}

			try {
				if (updates.size() > 0) {
					// System.out.println("Context: " +
					// dirContext.getNameInNamespace());
					ModificationItem[] itemArray = (ModificationItem[]) updates
							.toArray(new ModificationItem[0]);
					dirContext.modifyAttributes(EMoneyGramAdmLDAPKeys.CN + "="
							+ modifyUser.getUID(), itemArray);
				}
			} catch (Exception e) {
				EMGSharedLogger.getLogger(this.getClass()).error(
						e.getMessage(), e);
			}

		} catch (NameNotFoundException e) {
			EMGSharedLogger.getLogger(this.getClass()).error(e.getMessage(), e);
			throw new UserNotFoundException(e.getMessage());
		} catch (NamingException e) {
			EMGSharedLogger.getLogger(this.getClass()).error(e.getMessage(), e);
			throw new DataSourceException(e.getMessage());
		} finally {
			JNDIManager.releaseDirContext(dirContext);
		}
	}

	private List<ModificationItem> getRoleMods(Attribute attr,
			List<Role> newRoles) throws NamingException {
		ModificationItem item;
		NamingEnumeration values = attr.getAll();
		List<ModificationItem> updates = new ArrayList<ModificationItem>();

		while (values.hasMore()) {
			Object value = values.nextElement();
			if (value instanceof String) {
				String strValue = (String) value;
				if (strValue.endsWith(EMTAdmContainerProperties
						.getLDAP_INTERNAL_ROLE_APPLICATION_CONTEXT())) {
					for (Role newRole : newRoles) {
						item = new ModificationItem(
								DirContext.ADD_ATTRIBUTE,
								new BasicAttribute(
										EMoneyGramAdmLDAPKeys.USER_ROLES,
										EMoneyGramAdmLDAPKeys.CN
												+ "="
												+ newRole
												+ ","
												+ EMTAdmContainerProperties
														.getLDAP_INTERNAL_ROLE_APPLICATION_CONTEXT()));
						updates.add(item);
					}
				} else {
					item = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							new BasicAttribute(
									EMoneyGramAdmLDAPKeys.USER_ROLES, value));
					updates.add(item);
				}
			}
		}
		return updates;
	}

	public void updatePassword(String userID, String newPassword)
			throws DataSourceException, UserNotFoundException {
		DirContext dirContext = JNDIManager
				.getInitialContext(EMTAdmContainerProperties
						.getLDAP_USER_CONTEXT());
		try {
			DirContext userContext = (DirContext) dirContext
					.lookup(EMoneyGramAdmLDAPKeys.USER_ID + "=" + userID); //$NON-NLS-1$

			try {
				Attributes attrs = new BasicAttributes(true);

				userContext.modifyAttributes(
						"", DirContext.REPLACE_ATTRIBUTE, attrs); //$NON-NLS-1$
			} finally {
				JNDIManager.releaseDirContext(userContext);
			}
		} catch (NameNotFoundException e) {
			EMGSharedLogger.getLogger(this.getClass()).error(e.getMessage(), e);
			throw new UserNotFoundException(e.getMessage());
		} catch (NamingException e) {
			EMGSharedLogger.getLogger(this.getClass()).error(e.getMessage(), e);
			throw new DataSourceException(e.getMessage());
		} finally {
			JNDIManager.releaseDirContext(dirContext);
		}
	}

	public void updateEmailPassword(String userID, String newPassword)
			throws DataSourceException, UserNotFoundException {
		DirContext dirContext = JNDIManager
				.getInitialContext(EMTAdmContainerProperties
						.getLDAP_USER_CONTEXT());
		try {
			DirContext userContext = (DirContext) dirContext
					.lookup(EMoneyGramAdmLDAPKeys.USER_ID + "=" + userID); //$NON-NLS-1$

			try {
				Attributes attrs = new BasicAttributes(true);

				userContext.modifyAttributes(
						"", DirContext.REPLACE_ATTRIBUTE, attrs); //$NON-NLS-1$
			} finally {
				JNDIManager.releaseDirContext(userContext);
			}
		} catch (NameNotFoundException e) {
			EMGSharedLogger.getLogger(this.getClass()).error(e.getMessage(), e);
			throw new UserNotFoundException(e.getMessage());
		} catch (NamingException e) {
			EMGSharedLogger.getLogger(this.getClass()).error(e.getMessage(), e);
			throw new DataSourceException(e.getMessage());
		} finally {
			JNDIManager.releaseDirContext(dirContext);
		}
	}

	public UserProfile getUser(String uid) throws InvalidUserException,
			DataSourceException, UserNotFoundException {
		Map m = getLDAPUser(uid);
		return (getUser(m));
	}

	private UserProfile getUser(Map m) throws InvalidUserException,
			DataSourceException {
		UserProfile up = UserProfileFactory.createUserProfile((String) m
				.get(EMoneyGramAdmLDAPKeys.USER_ID));

		try {
			up.setGuid((String) (m.get(EMoneyGramAdmLDAPKeys.USER_GUID)));
			up.setFirstName((String) (m
					.get(EMoneyGramAdmLDAPKeys.USER_FIRST_NAME)));
			up.setLastName((String) m.get(EMoneyGramAdmLDAPKeys.USER_LAST_NAME));
			up.setEmailAddress((String) m.get(EMoneyGramAdmLDAPKeys.USER_EMAIL));
			up.setRoles(RoleManager.getEmtRoles(m
					.get(EMoneyGramAdmLDAPKeys.USER_ROLES)));
		} catch (InvalidGuidException e) {
			EMGSharedLogger.getLogger(this.getClass()).error(e.getMessage(), e);
			throw new InvalidUserException(e.getMessage());
		}
		return up;
	}

	public UserProfile getUserByUserId(String userId)
			throws InvalidUserException, DataSourceException {
		UserProfile user = null;
		DirContext dirContext = JNDIManager
				.getInitialContext(EMTAdmContainerProperties
						.getLDAP_USER_CONTEXT());
		try {
			SearchControls searchControls = new SearchControls();
			searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			NamingEnumeration namingEnumeration = dirContext.search("", "(&("
					+ EMoneyGramAdmLDAPKeys.USER_ID + "=" + userId + "))",
					searchControls);

			NameClassPair pair;
			Map map = null;
			while (namingEnumeration.hasMore()) {
				pair = (NameClassPair) namingEnumeration.next();

				if (pair.getName() == null) {
					continue;
				}
				DirContext userContext = (DirContext) dirContext.lookup(pair
						.getName());
				try {
					map = getMap(userContext.getAttributes("")); //$NON-NLS-1$

					user = UserProfileFactory.createUserProfile((String) map
							.get(EMoneyGramAdmLDAPKeys.USER_ID));
					user.setFirstName((String) map
							.get(EMoneyGramAdmLDAPKeys.USER_FIRST_NAME));
					user.setLastName((String) map
							.get(EMoneyGramAdmLDAPKeys.USER_LAST_NAME));
					user.setEmailAddress((String) map
							.get(EMoneyGramAdmLDAPKeys.USER_EMAIL));
					user.setRoles(RoleManager.getEmtRoles(map
							.get(EMoneyGramAdmLDAPKeys.USER_ROLES)));
					user.setGuid((String) map
							.get(EMoneyGramAdmLDAPKeys.USER_GUID));

				} catch (InvalidGuidException e) {
					EMGSharedLogger.getLogger(this.getClass()).error(
							e.getMessage(), e);
					throw new InvalidUserException(e.getMessage());
				} finally {
					JNDIManager.releaseDirContext(userContext);
				}
			}
		} catch (NamingException e) {
			EMGSharedLogger.getLogger(this.getClass()).error(e.getMessage(), e);
			throw new DataSourceException(e.getMessage());
		} finally {
			JNDIManager.releaseDirContext(dirContext);
		}
		return user;
	}

	public List<UserProfile> findHollowUsers(UserQueryCriterion uqc)
			throws DataSourceException {
		List<UserProfile> users = new ArrayList<UserProfile>();
		DirContext dirContext = JNDIManager
				.getInitialContext(EMTAdmContainerProperties
						.getLDAP_USER_CONTEXT());
		try {
			SearchControls searchControls = new SearchControls();
			searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String filter = uqc.getLDAPFilter();
			NamingEnumeration namingEnumeration = dirContext
					.search("", ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTML(filter)), searchControls); //$NON-NLS-1$
			NameClassPair pair;
			Map map = null;
			while (namingEnumeration.hasMore()) {
				pair = (NameClassPair) namingEnumeration.next();

				if (pair.getName() == null) {
					continue;
				}

				DirContext userContext = (DirContext) dirContext.lookup(pair
						.getName());
				try {
					map = getMap(userContext.getAttributes("")); //$NON-NLS-1$

					String userId = (String) map
							.get(EMoneyGramAdmLDAPKeys.USER_ID);
					if (userId == null)
						continue;
					UserProfile user = UserProfileFactory
							.createUserProfile(userId);
					Object givenName = map
							.get(EMoneyGramAdmLDAPKeys.USER_FIRST_NAME);
					if (givenName instanceof String) {
						user.setFirstName((String) givenName);
					} else {
						List firstName = (List) givenName;
						user.setFirstName((String) firstName.get(0));
					}
					user.setLastName((String) map
							.get(EMoneyGramAdmLDAPKeys.USER_LAST_NAME));
					user.setEmailAddress((String) map
							.get(EMoneyGramAdmLDAPKeys.USER_EMAIL));
					user.setRoles(RoleManager.getEmtRoles(map
							.get(EMoneyGramAdmLDAPKeys.USER_ROLES)));
					user.setGuid((String) map
							.get(EMoneyGramAdmLDAPKeys.USER_GUID));

					users.add(user);
				} catch (Exception e) {
					EMGSharedLogger.getLogger(this.getClass()).warn(
							e.getMessage(), e);
				}
			}
		}  catch (Exception e) {
			EMGSharedLogger.getLogger(this.getClass()).error(e.getMessage(), e);
			throw new DataSourceException(e.getMessage());
		} finally {
			JNDIManager.releaseDirContext(dirContext);
		}
		return users;
	}

	private Map getLDAPUser(String id) throws DataSourceException,
			UserNotFoundException {
		DirContext dirContext = JNDIManager
				.getInitialContext(EMTAdmContainerProperties
						.getLDAP_USER_CONTEXT());
		try {
			DirContext userContext = (DirContext) dirContext
					.lookup(EMoneyGramAdmLDAPKeys.USER_ID + "=" + id); //$NON-NLS-1$
			try {
				Map m = getMap(userContext.getAttributes("")); //$NON-NLS-1$
				return m;
			} finally {
				JNDIManager.releaseDirContext(userContext);
			}
		} catch (NameNotFoundException e) {
			EMGSharedLogger.getLogger(this.getClass()).error(e.getMessage(), e);
			throw new UserNotFoundException(e.getMessage());
		} catch (NamingException e) {
			EMGSharedLogger.getLogger(this.getClass()).error(e.getMessage(), e);
			throw new DataSourceException(e.getMessage());
		} finally {
			JNDIManager.releaseDirContext(dirContext);
		}
	}

	//MBO-14232: Remove Password Expired logic in EMTAdmin Login to remove dependency on Ping directory
	/*public boolean isPasswordExpired(String userId) throws DataSourceException,
			UserNotFoundException, UserAuthenticationException {
		DirContext dirContext = JNDIManager
				.getInitialContext(EMTAdmContainerProperties
						.getLDAP_USER_CONTEXT());
		try {
			DirContext userContext = (DirContext) dirContext
					.lookup(EMoneyGramAdmLDAPKeys.USER_ID + "=" + userId);
			try {
				Map m = getMap(userContext.getAttributes(
						"", new String[] { "passwordExpirationTime" })); //$NON-NLS-1$
				String expirationTime = (String) m
						.get("passwordExpirationTime");
				if (expirationTime == null) { // for locked accounts, we cannot
												// get the expiration date (or
												// any other attributes)
					return false;
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
				try {
					Date passwordExpirationDate = sdf.parse(expirationTime);
					return passwordExpirationDate.before(new Date());
				} catch (ParseException e) {
					EMGSharedLogger.getLogger(this.getClass()).error(e.getMessage(), e);
				}

				return false;
			} finally {
				JNDIManager.releaseDirContext(userContext);
			}
		} catch (NameNotFoundException e) {
			EMGSharedLogger.getLogger(this.getClass()).error(e.getMessage(), e);
			throw new UserAuthenticationException(e.getMessage());
		} catch (NamingException e) {
			EMGSharedLogger.getLogger(this.getClass()).error(e.getMessage(), e);
			throw new DataSourceException(e.getMessage());
		} finally {
			JNDIManager.releaseDirContext(dirContext);
		}
		// return false;
	}*/

	// private ModificationItem getModificationItemForSingleValueStringItem(
	// Attributes existingAttributes,
	// String attributeId,
	// String attributeValue)
	// throws NamingException
	// {
	// if (existingAttributes == null)
	// {
	//			String errorMessage = I18NHelper.getFormattedMessage("exception.required.param", new Object[] { "existingAttributes" }); //$NON-NLS-1$ //$NON-NLS-2$
	// throw new NullPointerException(errorMessage);
	// }
	//
	// if (attributeId == null)
	// {
	//			String errorMessage = I18NHelper.getFormattedMessage("exception.required.param", new Object[] { "attributeId" }); //$NON-NLS-1$ //$NON-NLS-2$
	// throw new NullPointerException(errorMessage);
	// }
	//
	// ModificationItem item = null;
	// Attribute existingAttribute = existingAttributes.get(attributeId);
	// if (existingAttribute == null)
	// {
	// if (attributeValue != null)
	// {
	// item =
	// new ModificationItem(
	// DirContext.ADD_ATTRIBUTE,
	// new BasicAttribute(attributeId, attributeValue));
	// }
	// } else
	// {
	// String existingValue = (String) existingAttribute.get(0);
	// if (StringHelper.compareToIgnoreCase(existingValue, attributeValue)
	// != 0)
	// {
	//				item = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute(attributeId, attributeValue == null ? "" : attributeValue)); //$NON-NLS-1$
	// }
	// }
	// return (item);
	// }

	// private ModificationItem[] getModificationItemsForSingleValueStringItem(
	// Attributes existingAttributes,
	// String attributeId,
	// String oldValue,
	// String newValue)
	// throws NamingException
	// {
	// if (existingAttributes == null)
	// {
	//				String errorMessage = I18NHelper.getFormattedMessage("exception.required.param", new Object[] { "existingAttributes" }); //$NON-NLS-1$ //$NON-NLS-2$
	// throw new NullPointerException(errorMessage);
	// }
	//
	// if (attributeId == null)
	// {
	//				String errorMessage = I18NHelper.getFormattedMessage("exception.required.param", new Object[] { "attributeId" }); //$NON-NLS-1$ //$NON-NLS-2$
	// throw new NullPointerException(errorMessage);
	// }
	//
	// ModificationItem item = null;
	// Attribute existingAttribute = existingAttributes.get(attributeId);
	// item = new ModificationItem(DirContext.ADD_ATTRIBUTE, new
	// BasicAttribute(attributeId, newValue));
	// if (existingAttribute == null)
	// {
	// {
	//					item = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute(attributeId, newValue == null ? "" : newValue)); //$NON-NLS-1$
	// }
	// }
	// return (item);
	// }

	// private ModificationItem getModificationItemForMultiValueAttribute(
	// Attributes existingAttributes,
	// String attributeId,
	// ArrayList values)
	// throws NamingException
	// {
	// if (existingAttributes == null)
	// {
	//			String errorMessage = I18NHelper.getFormattedMessage("exception.required.param", new Object[] { "existingAttributes" }); //$NON-NLS-1$ //$NON-NLS-2$
	// throw new NullPointerException(errorMessage);
	// }
	//
	// if (attributeId == null)
	// {
	//			String errorMessage = I18NHelper.getFormattedMessage("exception.required.param", new Object[] { "attributeId" }); //$NON-NLS-1$ //$NON-NLS-2$
	// throw new NullPointerException(errorMessage);
	// }
	//
	// ModificationItem item = null;
	// Attribute attr = new BasicAttribute(attributeId);
	// String roleID;
	// for (int i = 0; i < values.size(); i++)
	// {
	// roleID = (String) values.get(i);
	// attr.add(roleID);
	// }
	//
	// try
	// {
	// item = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, attr);
	// } catch (Exception e)
	// {
	// e.printStackTrace();
	// }
	// return (item);
	// }

}
