/*
 * Created on Mar 17, 2004
 *
 */
package emgadm.dataaccessors;

import emgadm.cache.locator.EMTDailingCodeInfoCacheService;
import emgadm.cache.locator.EMTDailingCodeInfoCacheServiceImpl;
import emgadm.cache.locator.EMTSourceSiteCacheService;
import emgadm.cache.locator.EMTSourceSiteCacheServiceImpl;

/**
 * @author A131
 *
 */
public class ManagerFactory
{
	private ManagerFactory() {
	}

	public static UserProfileManager createUserManager() {
		return UserProfileManagerImplRegular.getInstance();
	}

	public static BillerMainOfficeManager createBillerMainOfficeManager() {
		return BillerMainOfficeManagerImplRegular.instance();
	}

	public static TransactionManager createTransactionManager() {
		return TransactionManagerImplRegular.instance();
	}

	public static ACHReturnManager createACHReturnManager() {
		return ACHReturnManagerImplRegular.instance();
	}
	
	public static EMTSourceSiteCacheService createEMTSourceSiteCacheManager() {
		return EMTSourceSiteCacheServiceImpl.instance();
	}
	
	public static EMTDailingCodeInfoCacheService createEMTDailingCodeInfoCacheManager(){
		return EMTDailingCodeInfoCacheServiceImpl.instance();
	}

}
