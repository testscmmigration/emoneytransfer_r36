package emgadm.dataaccessors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import emgadm.model.ApplicationCommand;
import emgadm.model.Role;
import emgadm.model.UserProfile;
import emgshared.exceptions.DataSourceException;

public class MenuManager {
	private MenuManager(){
	}
		
	private static HashMap<String, List<ApplicationCommand>> menuMap = new HashMap<String, List<ApplicationCommand>>();

	public static HashMap<String, List<ApplicationCommand>> retrieveMenu()
			throws DataSourceException {
		if (menuMap.isEmpty()) {
			for (ApplicationCommand command : CommandManager.getCommands()) {
				if (command.isMenuItem()) {
					addToMenu(command);
				}
			}
		}
		return menuMap;
	}

	private static void addToMenu(ApplicationCommand menuItem) {
		List<ApplicationCommand> menu = menuMap.get(menuItem.getMenuGroupName());
		if (menu == null) {
			menu = new ArrayList<ApplicationCommand>();
		}
		menu.add(menuItem);
		menuMap.put(menuItem.getMenuGroupName(), menu);
	}

	public static String getMenu(Role role)
			throws DataSourceException {
		StringBuilder buffer = new StringBuilder();
		HashMap<String, List<ApplicationCommand>> map = retrieveMenu();

		Set<String> names = map.keySet();
		int ctr = 0;
		boolean isMenuAdded = false;
		for (String name : names) {
			List<ApplicationCommand> commands = map.get(name);

			int intCtr = 0;
			int currentMainCtr = 0;
			for (ApplicationCommand command : commands) {
				if (role.contains(command)) {
					if (!isMenuAdded) {
						isMenuAdded = true;
						if (!"REPORTS".equalsIgnoreCase(name)) {  //$NON-NLS-1$
							buffer.append("oCMenu.makeMenu('top" + ctr + "','','&nbsp;" + name + "','','') \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
							currentMainCtr = ctr;
						} else {
							currentMainCtr = 0;
						}
					}
					buffer.append("oCMenu.makeMenu('" + currentMainCtr + "sub" + intCtr + "','top" + currentMainCtr + "','" + command.getDisplayName() + "','/" + command.getId() + ".do') \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
					intCtr++;
				}
			}
			ctr++;
			isMenuAdded = false;
		}
		return buffer.toString();
	}

	public static String getMenu(UserProfile user) throws DataSourceException {
		StringBuilder buffer = new StringBuilder("");
		HashMap<String, List<ApplicationCommand>> map = retrieveMenu();

		Set<String> names = map.keySet();
		int ctr = 0;
		boolean isMenuAdded = false;
		String [] menuNames=new String[5];
		
		for(String name : names){
			if("System Admin".equalsIgnoreCase(name)){
				menuNames[0]=name;
			}else if("Reporting".equalsIgnoreCase(name)){
				menuNames[1]=name;
			}else if("User Admin".equalsIgnoreCase(name)){
				menuNames[2]=name;
			}else if("Dashboard".equalsIgnoreCase(name)){
				menuNames[3]=name;
			}else if("Consumer".equalsIgnoreCase(name)){
				menuNames[4]=name;
			}
		}
		
		for (String name : menuNames) {
			List<ApplicationCommand> commands = map.get(name);

			int intCtr = 0;
			int currentMainCtr = 0;
			for (ApplicationCommand command : commands) {
				if (user.hasPermission(command.getId())) {
					if (!isMenuAdded) {
						isMenuAdded = true;
						if ("Dashboard".equalsIgnoreCase(name)) {
							buffer.append("oCMenu.makeMenu('top" + ctr + "','','&nbsp;" + name + "','/showTransQueue.do') \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
							currentMainCtr = ctr;
                        } else if (!"REPORTS".equalsIgnoreCase(name)) {  //$NON-NLS-1$
							buffer.append("oCMenu.makeMenu('top" + ctr + "','','&nbsp;" + name + "','','') \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
							currentMainCtr = ctr;
						} else {
							currentMainCtr = 0;
						}
					}
					
					if (!"Dashboard".equalsIgnoreCase(name)) {
						buffer.append("oCMenu.makeMenu('" + currentMainCtr + "sub" + intCtr + "','top" + currentMainCtr + "','" + command.getDisplayName() + "','/" + command.getId() + ".do') \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
					}
					intCtr++;
				}
			}
			ctr++;
			isMenuAdded = false;
		}
		return buffer.toString();
	}

	public static String getEndMenu()
			throws DataSourceException {
		StringBuilder buffer = new StringBuilder();
		int ctr = 99;
		buffer.append("oCMenu.makeMenu('top" + ctr + "','','&nbsp;Logoff','/logoff.do','') \n"); //$NON-NLS-1$//$NON-NLS-2$
		buffer.append("oCMenu.construct()"); //$NON-NLS-1$
		return buffer.toString();
	}

}
