package emgadm.util;

import com.moneygram.mgo.service.config_v3_4.GetSourceSiteInfoResponse;

import com.moneygram.mgo.service.config_v3_4.TransactionTypeInfo;


import emgshared.cache.locator.EMTSharedSourceSiteCacheService;
import emgshared.cache.locator.EMTSharedSourceSiteCacheServiceImpl;
import emgshared.model.TransactionType;

/**
 * @version 1.0
 * @author wx51 
 * This class has common method which can be called wherever
 * merchant id is needed
 */
public class MerchantIdHelper {

	public String merchantIdInfo(String partnerSiteId, String transactionType,
			String merchantType) throws Exception {
		EMTSharedSourceSiteCacheService cacheService = new EMTSharedSourceSiteCacheServiceImpl();
		String Mid = null;
		GetSourceSiteInfoResponse response = null;
		response = cacheService.sourceSiteDetails(partnerSiteId);
		TransactionTypeInfo[] typeInfo = response
				.getCountryTransactionTypeInfo();
		if (null == transactionType) {
			transactionType = TransactionType.MONEY_TRANSFER_SEND_CODE;
		}
		if (null != merchantType) {
			for (TransactionTypeInfo tp : typeInfo) {
				if (merchantType.equals(tp.getPaymentInfo().getProviderName())
						&& transactionType.equals(tp.getTransactionType()
								.getValue())) {
					Mid = tp.getPaymentInfo().getPaymentMerchantId();
				}

			}
		} else {
			for (TransactionTypeInfo tp : typeInfo) {
				if (transactionType.equals(tp.getTransactionType().getValue())) {
					Mid = tp.getScoringMerchantId();
				}

			}
		}
		return Mid;
	}
}
