package emgadm.util;

import java.util.Locale;

import org.apache.log4j.Logger;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

import emgshared.dataaccessors.EMGSharedLogger;



public class PhoneConversionUtil {
	private static Logger LOGGER = EMGSharedLogger.getLogger(EMTEnvironmentInitializer.class);
	
	public static String formatPhoneNumber(String phoneNumberRaw,
			String userSelectedLocale) {
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		PhoneNumber phoneNumber = null;
		String numberformatted = null;
		try {
			if ("DE".equals(userSelectedLocale.substring(3, 5))) {
				phoneNumber = phoneUtil.parse(phoneNumberRaw,
						Locale.GERMANY.getCountry());
			} else if ("en-GB".equals(userSelectedLocale)) {
				phoneNumber = phoneUtil.parse(phoneNumberRaw,
						Locale.UK.getCountry());
			} else {
				phoneNumber = phoneUtil.parse(phoneNumberRaw,
						Locale.US.getCountry());
			}
			LOGGER.debug("User entered PhoneNumber=" + phoneNumber);
			numberformatted = phoneUtil.format(phoneNumber,
					PhoneNumberUtil.PhoneNumberFormat.E164);
			
		} catch (Exception e) {
			LOGGER.error("Error in PhoneNumber Formatting:" 
					+ e.getMessage(), e);
			numberformatted = phoneNumberRaw;
		}

		return numberformatted;
	}
}
