package emgadm.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import shared.mgo.dto.MGOAgentProfile;
import shared.mgo.dto.MGOProduct;
import shared.mgo.services.MGOServiceFactory;


import com.moneygram.security.ldap.LdapProperty;


import emgadm.dataaccessors.CommandManager;
import emgadm.dataaccessors.ManagerFactory;
import emgadm.dataaccessors.TransactionManager;
import emgadm.model.ApplicationCommand;
import emgadm.model.DeadTransaction;
import emgadm.model.PartnerSite;
import emgadm.property.EMTAdmAppProperties;
import emgadm.property.EMTAdmContainerProperties;
import emgadm.services.NotificationAccess;
import emgadm.services.NotificationAccessImpl;
import emgshared.cache.locator.EMTSharedSourceSiteCacheService;
import emgshared.cache.locator.EMTSharedSourceSiteCacheServiceImpl;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerProfile;
import emgshared.model.MessageCacheBean;
import emgshared.model.Transaction;
import emgshared.property.EMTSharedContainerProperties;
import emgshared.property.EMTSharedDynProperties;
import emgshared.property.EMTSharedEnvProperties;
import emgshared.property.EMTVersionProperties;
import emgshared.property.PropertyLoader;
import emgshared.services.ServiceFactory;
import emgshared.services.TransactionService;
import emgshared.services.UserActivityLogService;
import emgshared.services.backgroundprocess.MasterInformationLongTermRefreshRun;
import emgshared.services.backgroundprocess.MasterInformationShortTermRefreshRun;


public class EMTEnvironmentInitializer {
	private static final String CONTAINER_RESOURCE_URI = "java:comp/env/rep/EMTAdminResourceReference";
    protected static Logger log = EMGSharedLogger.getLogger(EMTEnvironmentInitializer.class);

	public static void initEnvironment(ServletContext servletContext) {
		notify("Classpath = " + System.getProperty("java.class.path"));

        /** ******************* CONTAINER PROPERTIES *********************** */
        PropertyLoader containerPropertyLoader = loadContainerProperties();

        /** ***************** AGENT CONNECT INITIALIZATION ***************** */
        initAgentConnectAccessors(containerPropertyLoader);

        /** ******************* DYNAMIC PROPERTIES ************************* */
        loadDynamicProperties();

        /** ***************** MISCELLANEOUS ******************************** */
        loadSubReasonTableInfo();

        loadUserEventTypes();

        loadCountryObject(servletContext);

        loadApplicationCommands(servletContext);

        loadHolidayInfo(servletContext);

        loadCustomerPremierCode(servletContext);
        //mbo-4766
        loadParnterSiteId(servletContext);
        loadPurposeTypes(servletContext);
        //MBO-6161
        loadResidencyStatus(servletContext);
        loadAdminMessages();

        /** ****************** Last but not least... ****************************** */
//This is now handled by EJB timer bean
//        String cr = "/" + ((servletContext != null) ? servletContext.getServletContextName() : "eMoneyGramAdm");
//        if (EMTAdmDynProperties.getDPO(null).isBGProcessAutoStartup()) {
//            BackgroundProcessMaster.startBackgroundProcessing(
//                    EMTAdmContainerProperties.getLoopbackServer(),
//                    EMTAdmContainerProperties.getLoopbackHost(),cr);
//        }

        Timer timer = new Timer(true);
        timer.schedule(new MasterInformationShortTermRefreshRun(), 0, 30*60*1000);
        timer.schedule(new MasterInformationLongTermRefreshRun(), 0, 6*60*60*1000);
        timer.schedule(new DenyExpiredTrans(), 10 * 60 * 1000, 2 * 60 * 1000);

        //		timer.schedule(new CheckMessageRefresh(), 2 * 60 * 1000, 2 * 60 * 1000);
	}
	
	/**
	 * MBO - 6161
	 * 
	 * @param servletContext
	 *            returns the list of resident statuses from DB and loads it to
	 *            a list on startup of server.
	 */
	private static void loadResidencyStatus(ServletContext servletContext) {
		try {
			// Gets Residency Statuses from the table.
			System.out.println(new Date() + " LOADING: Resident status Codes.");
			log.info("LOADING: Resident status Codes.");
			List residentStatusCodes = emgshared.services.ServiceFactory
					.getInstance().getConsumerProfileService()
					.getResidencyStatusTypes("System");
			safeSetContextAttribute(servletContext, "resdStatCodes",
					residentStatusCodes);
			System.out.println(new Date() + " SUCCESS: Resident status Codes");
			log.info(new Date() + " SUCCESS: Resident status Codes.");
		} catch (Exception e) {
			System.out
					.println(new Date()
							+ " FAILED: Resident status Codes Not Loaded.  Error: "
							+ e);
			log.error(new Date()
					+ " FAILED: Resident status Codes Not Loaded.  Error: " + e);
			notify("Halting Startup");
			throw new RuntimeException("Halting Startup", e);
		}
	}

	private static void initAgentConnectAccessors(PropertyLoader containerPropertyLoader) {
		boolean haltStartupFlag;
		//MBO-6101
		/*haltStartupFlag = EMTSharedContainerProperties.initAgentConnectAccessors(containerPropertyLoader
                .getRawProperties(), log, true);*/
		log.info("Going to initialize AgentConnect");
		EMTSharedSourceSiteCacheService cacheService = new EMTSharedSourceSiteCacheServiceImpl();
		MGOAgentProfile mgoAgentProfile = null;
		try {
			 mgoAgentProfile = cacheService.getACDetails("MGO", MGOProduct.SAME_DAY);
		} catch (Exception e) {
			log.error("Error while initialize Agent Profile object",e);
		}
		
		//ended
		haltStartupFlag = EMTSharedContainerProperties.initAgentConnectAccessors(containerPropertyLoader
                .getRawProperties(), log, true,mgoAgentProfile);

		//ended
        if (haltStartupFlag) {
            notify("Halting Startup");
            throw new RuntimeException("Halting Startup");
        }
	}

	private static void loadAdminMessages() {
		// Gets the admin messages
        System.out.println(new Date() + " LOADING: Admin Message Information.");
        log.info("LOADING: Admin Message Information.");
        try {
            MessageCacheBean.getInstance();
            System.out.println(new Date()
                    + " SUCCESS: Admin Message Information Loaded");
        } catch (Exception e) {
            System.out.println(new Date()
                    + " FAILED: Admin Message Information Not Loaded.  Error: "
                    + e);
            log.error(new Date()
                    + " FAILED: Admin Message Information Not Loaded.  Error: "
                    + e);
            notify("Halting Startup");
            throw new RuntimeException("Halting Startup", e);
        }
	}

	private static void loadPurposeTypes(ServletContext servletContext) {
		try {
            // Gets the negative purpose type table.
            System.out
                    .println(new Date() + " LOADING: Purpose Types.");
            log.info("LOADING: Customer Purpose Types.");
            List purposeTypes = emgshared.services.ServiceFactory
                    .getInstance().getConsumerProfileService()
					.getPurposeTypes("System");
           	safeSetContextAttribute(servletContext, "custPurposeTypes", purposeTypes);
            System.out.println(new Date()
                    + " SUCCESS: Customer Purpose Types");
            log.info(new Date() + " SUCCESS: Purpose Types.");
        } catch (Exception e) {
            System.out.println(new Date()
                    + " FAILED: Purpose Types Not Loaded.  Error: " + e);
            log.error(new Date()
                    + " FAILED: Purpose Types Not Loaded.  Error: " + e);
            notify("Halting Startup");
            throw new RuntimeException("Halting Startup", e);
        }
	}

	private static void loadCustomerPremierCode(ServletContext servletContext) {
		try {
            // Gets the Customer Premier Code table.
            System.out.println(new Date() + " LOADING: Customer Premier Codes.");
            log.info("LOADING: Customer Premier Codes.");
            List custPrmrCodes = emgshared.services.ServiceFactory
                    .getInstance().getConsumerProfileService()
                    .getCustPremierTypes("System");
            safeSetContextAttribute(servletContext, "custPrmrCodes", custPrmrCodes);
            System.out.println(new Date()
                    + " SUCCESS: Customer Premier Codes");
            log.info(new Date() + " SUCCESS: Customer Premier Codes.");
        } catch (Exception e) {
            System.out.println(new Date()
                    + " FAILED: Customer Premier Codes Not Loaded.  Error: " + e);
            log.error(new Date()
                    + " FAILED: Customer Premier Codes Not Loaded.  Error: " + e);
            notify("Halting Startup");
            throw new RuntimeException("Halting Startup", e);
        }
	}
//MBO-4766
	private static void loadParnterSiteId(ServletContext servletContext) {
		try {
            // Gets the Customer Premier Code table.
             log.info("LOADING: partnerSiteIds Codes.");
             java.util.Collection<PartnerSite> partnerSiteIds = ManagerFactory.createTransactionManager().getPartnerSiteIds("B");
            safeSetContextAttribute(servletContext, "partnerSiteIds", partnerSiteIds);
            log.info(new Date() + " SUCCESS: partnerSiteIds Codes.");
        } catch (Exception e) {
             log.error(new Date()
                    + " FAILED: partnerSiteIds Codes Not Loaded.  Error: " + e);
            notify("Halting Startup");
            throw new RuntimeException("Halting Startup", e);
        }
	}
	
	
	
	private static void loadHolidayInfo(ServletContext servletContext) {
		try {
            // Gets the Holiday Information.
            System.out.println(new Date() + " LOADING: Holiday Information.");
            log.info("LOADING: Holiday Information.");
            ServiceFactory sf = ServiceFactory.getInstance();
            TransactionService ts = sf.getTransactionService();
            safeSetContextAttribute(servletContext, "holidayMap",
                    ts.getHolidays("emgwww"));
            System.out.println(new Date()
                    + " SUCCESS: Holiday Information Loaded");
            log.info(new Date() + " SUCCESS: Holiday Information Loaded.");
        } catch (Exception e) {
            System.out.println(new Date()
                    + " FAILED: Holiday Information Not Loaded.  Error: " + e);
            log.error(new Date()
                    + " FAILED: Holiday Information Not Loaded.  Error: " + e);
            notify("Halting Startup");
            throw new RuntimeException("Halting Startup", e);
        }
	}

	private static void loadApplicationCommands(ServletContext servletContext) {
		//  cache application commands
        try {
            System.out.println(new Date() + " LOADING: Command Map.");
            log.info("LOADING: Command Map.");
            Map<String, ApplicationCommand> commandMap = new HashMap<String, ApplicationCommand>();
            Iterator<ApplicationCommand> iter = CommandManager.getCommands().iterator();
            while (iter.hasNext()) {
                ApplicationCommand cmd = iter.next();
                commandMap.put(cmd.getId(), cmd);
            }
			safeSetContextAttribute(servletContext, "commandMap", commandMap);
            System.out.println(new Date() + " SUCCESS: Command Map.");
            log.info("SUCCESS: Command Map.");
        } catch (Exception e) {
            System.out.println(new Date() + " FAILED: Command Map.");
            log.error("FAILED: Command Map.", e);
            notify("Halting Startup");
            throw new RuntimeException("Halting Startup", e);
        }
	}

	private static void loadCountryObject(ServletContext servletContext) {
		//  cache country object (CountryInfo) collection
        try {
	        MGOServiceFactory.getInstance().getAgentConnectService().refreshMasterFromAgentConnect();
	        String name = "versionLabel";
			String value = EMTVersionProperties.getBuildString() + " ("
			        + EMTSharedEnvProperties.getEnvironment() + ")";
			safeSetContextAttribute(servletContext, name, value);
	    } catch (Exception e) {
	        System.out.println(new Date() + " FAILED: Countries Map.");
	        log.error("FAILED: Countries Map.", e);
	        notify("Halting Startup");
            throw new RuntimeException(e);
	    }
	}

	private static void safeSetContextAttribute(ServletContext servletContext, String name,
			Object value) {
		if (servletContext != null) {
			servletContext.setAttribute(name, value);
		}
	}

	private static void loadUserEventTypes() {
		// cache the User Event Types, it will automatically, but force it at startup
        try {
            ServiceFactory sf = ServiceFactory.getInstance();
            UserActivityLogService uals = sf.getUserActivityLogService();
            uals.getAllUserActivityTypes();
            System.out.println(new Date()
                    + " SUCCESS:  User Activity Type Cache Information is loaded");
            log.info(new Date() + " SUCCESS: User Activity Type Cache Information is loaded");
        } catch (Exception e) {
            System.out.println(new Date()
                    + " FAILED: User Activity Type Cache Not Loaded.  Error: " + e);
            log.error(new Date()
                    + " FAILED: User Activity Type Cache Not Loaded.  Error: " + e);
            notify("Halting Startup");
            throw new RuntimeException("Halting Startup", e);
        }
	}

	private static void loadSubReasonTableInfo() {
		try {
            // Gets the Sub-Reason table information and caches
            System.out.println(new Date() + " LOADING: Transaction Sub-Reason Information.");
            log.info("LOADING: Transaction Sub-Reason Information.");
            ServiceFactory sf = ServiceFactory.getInstance();
            // by calling this service will force the initial cache, subsequent
            // calls will only recache after a determined amount of time
            // ************ don't remove this line  **************
            sf.getTransSubReasonService();
            // ************ forces an initial cache of the sub-reasons  **************
            System.out.println(new Date()
                    + " SUCCESS: Transaction Sub-Reason Information is loaded");
            log.info(new Date() + " SUCCESS: Transaction Sub-Reason is Loaded.");
        } catch (Exception e) {
            System.out.println(new Date()
                    + " FAILED: Transaction Sub-Reason Information Not Loaded.  Error: " + e);
            log.error(new Date()
                    + " FAILED: Transaction Sub-Reason Information Not Loaded.  Error: " + e);
            notify("Halting Startup");
            throw new RuntimeException("Halting Startup", e);
        }
	}

	private static void loadDynamicProperties() {
		List<String> de = EMTSharedDynProperties
                .bootstrapLoadAndValidate(log);
        if (de == null || de.size() > 0) {
        	System.err.println("About to HALT: =================================================");
        	for (String msg : de) {
        		System.err.println(msg);
        	}
        	System.err.println("================================================================");
        	notify("FAILURE: Admin Dynamic Properties Validation Could not be loaded");
            throw new RuntimeException("Startup Halted.");
        } else {
            notify("SUCCESS: Admin Dynamic Properties Loaded and Validated.");
        }
        if (de != null && de.size() > 0) {
            for (int i = 0; i < de.size(); i++) {
                notify(de.get(i));
            }
        }
	}

	private static PropertyLoader loadContainerProperties() {
		notify("LOADING: EMT Admin Container Properties.");

        PropertyLoader containerPropertyLoader = PropertyLoader.getInstance(CONTAINER_RESOURCE_URI,
                "EMT Admin Container ", log);

        if (containerPropertyLoader == null)
            throw new RuntimeException("Startup Halted");

        EMTAdmContainerProperties.initialize(containerPropertyLoader);
        LdapProperty.initialize(containerPropertyLoader.getRawProperties());
        EMTSharedContainerProperties.initialize(containerPropertyLoader);
        EMTAdmAppProperties.initialize(containerPropertyLoader);

		if (!containerPropertyLoader.isValidated()) {
			System.out.println("FAILURE: Admin Container Properties Validation Error");
			containerPropertyLoader.dumpErrors(true);
			throw new RuntimeException("Startup Halted.");
		}

		if (!LdapProperty.isValid()) {
			System.out.println("FAILURE: Admin Ldap Properties Validation Error");
			LdapProperty.dumpErrors();
			throw new RuntimeException("Startup Halted.");
		}

		notify("SUCCESS: Admin Container Properties Loaded and Validated.");
		return containerPropertyLoader;
	}

    private static void notify(String message) {
        log.error(message);
//        System.out.println((new Date()).toString() + " " + message);
    }

    protected static class DenyExpiredTrans extends TimerTask {
        public void run() {
            log.debug("Begin deny expired transactions process ...");

            emgadm.services.TransactionService tranService = emgadm.services.ServiceFactory
                    .getInstance().getTransactionService();

            TransactionManager tm = ManagerFactory.createTransactionManager();
            NotificationAccess na = new NotificationAccessImpl();

            Iterator<DeadTransaction> iter = tranService.denyExpiredTrans().iterator();

            while (iter.hasNext()) {
                DeadTransaction et = iter.next();

                Transaction trn = tm.getTransaction(et.getTranId());

                ConsumerProfile profile = null;
				try {
					profile = emgshared.services.ServiceFactory.getInstance()
					.getConsumerProfileService().getConsumerProfile(Integer.valueOf(trn.getCustId()), trn.getSndCustLogonId(), "");
				} catch (DataSourceException e) {
					log.error("Error getting consumer profile ...", e);
				}
        		if (profile != null) {
        			String preferedlanguage = profile.getPreferedLanguage();
        			ConsumerEmail consumerEmail = null;
        			// Loop sends out email to each address.
        			for (Iterator<ConsumerEmail> iter1 = profile.getEmails().iterator(); iter1.hasNext();) {
        				consumerEmail = iter1.next();
        				na.notifyDeniedTransaction(trn, consumerEmail,preferedlanguage);
        			}
        		}
            }

            log.debug("Done deny expired transactions process ...");
            log.debug("Begin update limbo ASP transactions process ...");
            tranService.updateLimboASPTrans();
            log.debug("Done update limbo ASP transactions process ...");
        }
    }

    //	protected class CheckMessageRefresh extends TimerTask
    //	{
    //		public void run()
    //		{
    //			log.diagnostic(
    //				"Begin checking need for Admin Message Refresh ...");
    //
    //			Map msgMap =
    //				(Map) getServletContext().getAttribute("adminMsgs");
    //			CacheService cs = ServiceFactory.getInstance().getCacheService();
    //			SimpleCache sc = null;
    //			try
    //			{
    //				sc = cs.getCache("emgwww", SimpleCache.ADMIN_MESSAGE_CODE);
    //			} catch (Exception e1)
    //			{
    //				throw new EMGRuntimeException(e1);
    //			}
    //
    //			if (!sc.getTimeStamp().equals(msgMap.get("CacheTimestamp")))
    //			{
    //				MessageService ms =
    //					ServiceFactory.getInstance().getMessageService();
    //				try
    //				{
    //					getServletContext().setAttribute(
    //						"adminMsgs",
    //						ms.getActiveUserMessages("emgwww"));
    //				} catch (DataSourceException e)
    //				{
    //					log.diagnostic(
    //						"Admin Message Refresh failed ...");
    //					throw new EMGRuntimeException(e);
    //				}
    //			}
    //			log.diagnostic(
    //				"Done Admin Message Refresh process ...");
    //		}
    //	}

}
