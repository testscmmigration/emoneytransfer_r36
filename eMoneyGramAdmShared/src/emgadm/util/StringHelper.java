/*
 * Created on Feb 6, 2004
 *
 */
package emgadm.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import emgadm.property.EMTAdmContainerProperties;
import emgadm.services.DecryptionService;

/**
 * This class contains 'convenient' static methods
 * for working with Strings.  There isn't any reason
 * to instantiate an object of this type so the
 * construtor has been made private.
 * For reused purposes, this class also extends the 
 * StringHelper in the Shared project.
 * 
 * @author A131
 *
 */
public class StringHelper extends emgshared.util.StringHelper {
	public static final String emptyString = "";

    private StringHelper() {
    }
	
	//3514
	/**
	 * inserts hypens in the indexes mentioned in indexArray 
	 * @param entity
	 * @param intArray
	 * @return
	 */
	public static String formatStringWithHyphen(String entity,int[] indexArray) {
		final char hyphen = '-';
		char[] a = entity.toCharArray();
		StringBuilder formattedEntity = new StringBuilder();
		for (int i = 0 ; i < a.length; i++) {
			for(int j = 0 ; j < indexArray.length; j++){
				if (i == indexArray[j]) {
					formattedEntity.append(hyphen);
				}
			}
			formattedEntity.append(a[i]);
		}
		entity = formattedEntity.toString();
		return entity;
	}
	//3514
    public static boolean internalNetworkIDInvalidFormat(String userID) {
		boolean invalidNetworkID = false; // a999
        if (StringHelper.isNullOrEmpty(userID) == false) {
            for (int i = 0; i < userID.length(); i++) {
				char c = userID.charAt(i);
                if (i == 0) {
                    if (!Character.isLetter(c)) {
						invalidNetworkID = true;
						break;
                    }
                } else {
                    if (!Character.isDigit(c)) {
                        invalidNetworkID = true;
                        break;
                    }
				}
            }
        } else {
			invalidNetworkID = true; // empty field
		}

		return invalidNetworkID;
	}

    public static boolean invalidEmailAddress(String email) {
		boolean invalidEmail = false;
        if (email.indexOf("@moneygram.com") < 0) {
            invalidEmail = true;
        }
        if (email.indexOf("@") < 1) { // @cannot be in first spot
            invalidEmail = true;
        }
		return invalidEmail;
	}

//	public static String genRandomPassword(int length)
//	{		
//		String charset = "23456789abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
//		String charsetNumber = "123456789";
//		String charsetLowerCase = "abcdefghijklmnpqrstuvwxyz";
//		String charsetUpperCase = "ABCDEFGHJKLMNPQRSTUVWXYZ";
//		Random rand = new Random(System.currentTimeMillis());
//	    StringBuilder sb = new StringBuilder();
//	    boolean oneNumber = false;
//	    boolean oneLowerCase = false;
//	    boolean oneUpperCase = false;
//	    int count=0;
//	    while(true) {
//	    	count++;
//		    for (int i = 0; i < length; i++) {
//		    	int pos = rand.nextInt(charset.length());
//		    	
//		    	sb.append(charset.charAt(pos));
//		    	if (charsetNumber.indexOf(charset.charAt(pos)) >-1) oneNumber = true;
//		    	if (charsetLowerCase.indexOf(charset.charAt(pos)) >-1) oneLowerCase = true;
//		    	if (charsetUpperCase.indexOf(charset.charAt(pos)) >-1) oneUpperCase = true;
//		    }
//		    if (count>99) break; // take this one if maxed out at 99;
//		    if (oneNumber && oneLowerCase & oneUpperCase) break; 
//		    else
//		    	sb = new StringBuilder();
//	    }
//	    return sb.toString();
//	}
    public static String genRandomPassword(int length)
            throws NoSuchAlgorithmException {
        String charset = "23456789abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
		String charsetNumber = "123456789";
		String charsetLowerCase = "abcdefghijklmnpqrstuvwxyz";
		String charsetUpperCase = "ABCDEFGHJKLMNPQRSTUVWXYZ";
		SecureRandom secRand=SecureRandom.getInstance("SHA1PRNG");
	    StringBuilder sb = new StringBuilder();
	    boolean oneNumber = false;
	    boolean oneLowerCase = false;
	    boolean oneUpperCase = false;
	    int count=0;
	    while(true) {
	    	count++;
		    for (int i = 0; i < length; i++) {
		    	int pos = secRand.nextInt(charset.length());
		    	
		    	sb.append(charset.charAt(pos));
		    	if (charsetNumber.indexOf(charset.charAt(pos)) >-1) oneNumber = true;
		    	if (charsetLowerCase.indexOf(charset.charAt(pos)) >-1) oneLowerCase = true;
		    	if (charsetUpperCase.indexOf(charset.charAt(pos)) >-1) oneUpperCase = true;
		    }
		    if (count>99) break; // take this one if maxed out at 99;
		    if (oneNumber && oneLowerCase & oneUpperCase) break; 
		    else
		    	sb = new StringBuilder();
	    }
	    return sb.toString();
	}
	
	/*public static String generateHash(String text) {
		MessageDigest md;
		final String DigestAlgorithm ="SHA-256";
		try {
			md = MessageDigest.getInstance(DigestAlgorithm);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("MD5 hash algorith not found.", e);
		}
		md.update(text.getBytes(), 0, text.length());
		return new BigInteger(1, md.digest()).toString(16);
	}*/

	public static String maskCc(String creditCardNumber) {
		return creditCardNumber.substring(0, 6) + "******" + creditCardNumber.substring(12);
	}
	
	public static String getCcBin(String creditCardNumber) {
		return creditCardNumber.substring(0, 6);
	}
	
	public static String decryptIt(String encryptedString,Date date){
    	try {
    		DecryptionService ds = emgadm.services.ServiceFactory.getInstance().getDecryptionService();

    		/* FIXME The transaction service encrypts external ids (e.g. passport)
    		 * with out padding, but send customer's photo id with padding. 
    		 * Refer to TransactionDAO, and ConsumerDAO in transaction service 
    		 * project. This is inconsistent as both ids. are considerd additional
    		 * ids and should be encrypted with the same Cipher class options (
    		 * (translation options). decryptAdditionalId uses no padding,
    		 * while decryptSSN does. Once this inconsistency is resolved can 
    		 * use decryptedAdditionalId(encryptedString)
    		 */
    	    // String decryptedString = ds.decryptAdditionalId(encryptedString);
    		/*MBO-3510 Defect Fix*/
    		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    		Date decryptSyncStartDate = null;
    		String decryptedString = null;
    		try {
    			decryptSyncStartDate = format.parse(EMTAdmContainerProperties
    					.getDecryptSyncStartDate());
    		} catch (ParseException parseException) {
    			System.err.println("failed to get Devrypt Start Date. " + parseException.getMessage());
    		}

    		if (decryptSyncStartDate.before(date)) {
    			decryptedString = ds.decryptAdditionalId(encryptedString);
    		}else{
    			decryptedString = ds.decryptSSN(encryptedString);		
    		}

    		
    		return decryptedString;
    	} catch (Exception e){
    		System.err.println("The decryption failed. " + e.getMessage());
    		return "";
    	}
	}
}
