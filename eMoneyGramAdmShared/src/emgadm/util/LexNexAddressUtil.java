package emgadm.util;

import java.util.Arrays;

import com.moneygram.mgo.service.consumerValidationV2_1.Address;
import com.moneygram.mgo.service.consumerValidationV2_1.Person;
import com.moneygram.mgo.service.consumerValidationV2_1.PersonSearchResponse;

import emgshared.model.ConsumerAddress;
import emgshared.util.StringHelper;

/**
 * This class carries utility methods to manipulate Addresses we receive from
 * Lexis Nexis.
 */
public class LexNexAddressUtil {

	private static final String SPACE = " ";

	/**
	 * This method compares given Address with the list of Addresses from
	 * LexNex, and finds the Person who has the matching address.
	 * 
	 * @param Address
	 *            reqAddress
	 * @param PersonSearchResponse
	 *            response
	 * @return Person
	 */
	public static Person findPersonWithMatchingAddr(ConsumerAddress reqAddress,
			PersonSearchResponse response) {
		Person selectedPerson = null;

		if (null != reqAddress && null != response) {
			String reqAddressLine1 = reqAddress.getAddressLine1();

			// Create string array by splitting Request AddressLine1 based on
			// Space, and Sort it.
			String[] reqAddressArray = null;
			if (null != reqAddressLine1) {
				reqAddressArray = reqAddressLine1.trim().split(SPACE);
				Arrays.sort(reqAddressArray);
			}

			if (null != response.getPersons()
					&& response.getPersons().length > 0) {
				// Iterate through each Person and compare its addresses with
				// the given address until it finds match.
				for (int personIndex = 0; personIndex < response.getPersons().length; personIndex++) {
					Address resAddress = (response.getPersons()[personIndex])
							.getAddress();
					if (null != resAddress) {
						// Compare City, State and PostalCode strings first. If
						// anyone doesn't match, break the loop and return null
						// selectedPerson.
						if ((StringHelper.compareToIgnoreCase(
								resAddress.getCity(), reqAddress.getCity())) != 0) {
							continue;
						}
						if ((StringHelper.compareToIgnoreCase(
								resAddress.getState(), reqAddress.getState())) != 0) {
							continue;
						}
						if ((StringHelper.compareToIgnoreCase(
								resAddress.getPostalCode(),
								reqAddress.getPostalCode())) != 0) {
							continue;
						}

						// Compare AddressLine1 if every other Address elements
						// match. Construct String array of AddressLine1,
						// similar to Request AddressLine1 array.
						String resAddressLine1 = resAddress.getAddressLine1();
						String[] resAddressArray = null;
						if (null != resAddressLine1) {
							resAddressArray = resAddressLine1.split(SPACE);
							Arrays.sort(resAddressArray);
						}

						// Compare both arrays and if both match, return Person
						// of the matched address.
						boolean isSameAddress = compareStringArrays(
								reqAddressArray, resAddressArray);
						if (isSameAddress) {
							selectedPerson = response.getPersons()[personIndex];
							break;
						}
					}
				}
			}
		}

		return selectedPerson;
	}

	/**
	 * This method compare given 2 String Arrays and finds if they are matching.
	 * 
	 * @param array1
	 * @param array2
	 * @return True if Array1 is same as Array2 irrespective of its elements
	 *         order; Otherwise False.
	 */
	public static boolean compareStringArrays(String[] array1, String[] array2) {

		// Return True if both Arrays are null, False if either one of the Array
		// is null.
		if (null == array1) {
			if (null == array2)
				return true;
			else
				return false;
		} else if (null == array2) {
			return false;
		}

		// Return False if length of the Arrays are not same.
		if (array1.length != array2.length) {
			return false;
		} else {
			// Compare each array element and return True if all the elements
			// match.
			for (int arrayIndex = 0; arrayIndex < array1.length; arrayIndex++) {
				if (!(array1[arrayIndex].trim()
						.equalsIgnoreCase(array2[arrayIndex].trim()))) {
					return false;
				}
			}
			return true;
		}
	}
}
