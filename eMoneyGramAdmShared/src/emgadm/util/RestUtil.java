package emgadm.util;

import org.springframework.http.HttpStatus;

public class RestUtil {

	public static boolean isError(HttpStatus status) {
		return HttpStatus.Series.CLIENT_ERROR.equals(status.series())
				|| HttpStatus.Series.SERVER_ERROR.equals(status.series());
	}
	
	public static boolean isSuccess(HttpStatus status) {
		return HttpStatus.Series.SUCCESSFUL.equals(status.series());
	}
}
