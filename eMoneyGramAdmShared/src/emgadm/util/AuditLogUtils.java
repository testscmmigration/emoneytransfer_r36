/*
 * Created on Apr 2, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.util;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;

//  import emgadm.dataaccessors.EMoneyGramAdmLogger;

/**
 * @author A132
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AuditLogUtils {
	public static final String ADHOC_TRAN_REPORT = "ATR"; //$NON-NLS-1$
	public static final String ACCT_ADMIN_BUSINESS = "AAB"; //$NON-NLS-1$
	public static final String ACCT_ADMIN_SECURITY = "AAS"; //$NON-NLS-1$
	public static final String DAILY_TRAN_REPORT = "DTR"; //$NON-NLS-1$
//	public static final String EXTERNAL_USER_ADD = "EUA"; //$NON-NLS-1$
//	public static final String EXTERNAL_USER_EDIT = "EUE"; //$NON-NLS-1$
	public static final String INTERNAL_USER_ADD = "IUA"; //$NON-NLS-1$
	public static final String INTERNAL_USER_EDIT = "IUE"; //$NON-NLS-1$
	public static final String INACTIVE_USERS_REPORT = "IUR"; //$NON-NLS-1$
	public static final String LOGIN_FAILURE = "LF"; //$NON-NLS-1$
	public static final String LOGIN_INACTIVE = "LFD"; //$NON-NLS-1$
	public static final String LOGIN_EXCESSIVE_FAILURE = "LFE"; //$NON-NLS-1$
	public static final String LOGIN_SUCCESS = "LS"; //$NON-NLS-1$
	public static final String PASSWORD_CHANGE = "PWC"; //$NON-NLS-1$
	public static final String REALTIME_TRAN_REPORT = "RTR"; //$NON-NLS-1$
	public static final String SYSTEM_ERROR = "SE"; //$NON-NLS-1$
	public static final String SUMMARY_REPORT = "SR"; //$NON-NLS-1$
	public static final String TRANS_DETAIL_VIEW = "TDV"; //$NON-NLS-1$

	private static final String PARAM_LIST = "_PARAM_LIST"; //$NON-NLS-1$
	private static final String DETAIL = "_DETAIL"; //$NON-NLS-1$

	private static final Properties p = new Properties();

	static {
		new AuditLogUtils();
	}

    private AuditLogUtils() {
		InputStream is = getClass().getClassLoader().getResourceAsStream("emgadm/resources/AuditLog.properties"); //$NON-NLS-1$
        try {
			p.load(is);
        } catch (IOException e) {
			//  EMoneyGramAdmLogger.getLogger().error(e.getMessage(), e);
			System.out.println("Error: IO exception when open the audit log - '" + e.getMessage() + "'");
        } finally {
            try {
				is.close();
            } catch (IOException ignore) {
            }
		}
	}

    public static String getCommand(String uri) {
		String command = null;
        if (!StringHelper.isNullOrEmpty(uri)) {
			int startIndex = uri.lastIndexOf("/") + 1; //$NON-NLS-1$
			int endIndex = uri.lastIndexOf("."); //$NON-NLS-1$
            if (endIndex < startIndex) {
                endIndex = uri.length();
            }
			command = uri.substring(startIndex, endIndex);
		}
		return command == null ? "" : command; //$NON-NLS-1$
	}

    public static String getParamList(String command, String action,
            Object[] objects) {
		String parmList = null;
		String pattern = p.getProperty(command + "." + action + PARAM_LIST); //$NON-NLS-1$
        if (!StringHelper.isNullOrEmpty(pattern)) {
            try {
                parmList = new MessageFormat(pattern).format(objects); //$NON-NLS-1$
            } catch (Exception ignore) {
            }
        }
		return parmList == null ? "" : parmList; //$NON-NLS-1$
	}

    public static String getDetails(String command, String action) {
		return p.getProperty(command + "." + action + DETAIL, ""); //$NON-NLS-1$ //$NON-NLS-2$
	}
}
