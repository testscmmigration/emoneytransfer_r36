package emgadm.util;

import java.math.BigInteger;

public class AgentConnectUtil {

	public AgentConnectUtil() {
	}
	
    public boolean hasGlobalAggregateError(Exception ex){
        boolean hasGlobalAggregateError = false;
        if(ex instanceof com.moneygram.agentconnect1305.wsclient.Error){
            com.moneygram.agentconnect1305.wsclient.Error error = (com.moneygram.agentconnect1305.wsclient.Error) ex;
            BigInteger errorCode = error.getErrorCode();
            if (errorCode != null) {
                int code = error.getErrorCode().intValue();
                hasGlobalAggregateError = isGlobalAggregateErrorCode(code);
            }
        }
        return hasGlobalAggregateError;
       
    }

	private boolean isGlobalAggregateErrorCode(int code) {
		if(code>=7000&&code<=7999){
			return true;
		}
		return false;
	}
}
