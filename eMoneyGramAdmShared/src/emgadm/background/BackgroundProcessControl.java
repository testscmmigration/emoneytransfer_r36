package emgadm.background;

public class BackgroundProcessControl {

	protected static int lastProcessed = 0;

	protected static int masterProcessedCount = 0;

	protected static int masterProcessExceptionCount = 0;

	public static int getMasterProcessExceptionCount() {
		return masterProcessExceptionCount;
	}

	public static void setMasterProcessExceptionCount(int masterProcessExceptionCount) {
		BackgroundProcessControl.masterProcessExceptionCount = masterProcessExceptionCount;
	}

	public static int getLastTranProcessed() {
		return lastProcessed;
	}

	public static int getTotalProcessedSinceStartup() {
		return masterProcessedCount;
	}
}
