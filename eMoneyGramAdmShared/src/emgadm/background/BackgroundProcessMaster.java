package emgadm.background;



import org.apache.log4j.Logger;

import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.property.EMTSharedContainerProperties;
import emgshared.property.EMTSharedDynProperties;

/**
 * Singleton thread that calls the Struts BackgroundProcessAction periodically.
 * Will sleep ( throttle timeout setting ) if last check returned no work.
 * @author GRS
 *
 */

public class BackgroundProcessMaster implements Runnable {

    private static final Logger LOG = EMGSharedLogger.getLogger(BackgroundProcessMaster.class);
    private static Thread _masterThread = null;
    private static BackgroundProcessMaster _masterRunnable = null;
    private static int bpMasterThrottle;
    private static Object lockObj = new Object();
    private static volatile boolean shutdownFlag = false;   
	private static String contextRoot = "";
    /**
     * Factory Method, except you can't get a reference
     * to my private singleton.
     */

    public static void startBackgroundProcessing(String loopbackServer, String loopbackHost, String contextRoot) {
    	BackgroundProcessMaster.contextRoot = contextRoot;
        synchronized (lockObj){
           LOG.info("Call to Starte the Background Master process");
           if (_masterThread != null){
                LOG.info("startBackgroundProcessing() called, but previous thread not finished.");
                return;
           }

           EMTSharedDynProperties dProp = new EMTSharedDynProperties();

           // first time the myPriority is called, the bootstrap properties are used,
           // afterward the 3 fields are fresh read from the database.
           bpMasterThrottle = dProp.getBGProcessMasterThrottle();
           int myPriority = dProp.getBGProcessMasterPriority();
           // limit the range of priority values.
			if (myPriority > Thread.MAX_PRIORITY) {
				myPriority = Thread.MAX_PRIORITY;
			}
			if (myPriority < Thread.MIN_PRIORITY) {
				myPriority = Thread.MIN_PRIORITY;
			}	

           LOG.info("startBGP: mT:" + bpMasterThrottle + " mP:" + myPriority);

           // Create a new master, cut the old loose.
           _masterRunnable = new BackgroundProcessMaster();
           _masterRunnable.runSwitch = true;
           _masterThread = new Thread(_masterRunnable);

           try {
                _masterThread.setPriority(myPriority);
                _masterThread.setName("BackgroundProcessMasterThread");
                LOG.info("Attempting to start the master thread");
                if ( shutdownFlag ){
                	return; // abort.
                }	
                _masterThread.start();
                LOG.info("Master thread started");
            } catch (Exception e) {
                LOG.error("startup exception", e);
            }
        }
    }
    /**
     * called by the admin application.
     */
    public static void stopAutoProcessing() {
        synchronized (lockObj) {
			if (_masterThread == null) {
				LOG.info("Background Stop called, but master == null");
				return;
			}
			if (_masterRunnable == null) {
				// this really should never happen. Assertion.
				LOG.info("BackgroundMaster _masterThread not null, _masterRunnable is null. ?");
				_masterThread = null;
				return;
			}
            _masterRunnable.runSwitch = false;
            try {
                _masterThread.join(30000); // allow 30 seconds to end. 
            } catch (InterruptedException ie) {
                LOG.error("stopAutoProc() join interrupted", ie);
            }
            if (_masterThread != null && _masterThread.isAlive()) {
                LOG.info("stopAutoProc() failed after 15 seconds");
            } else {
                _masterThread = null;
                _masterRunnable = null;
            }
        }
     } 
    /**
     * called by the BackgroundProcessShutdownListener
    * @return
     */
    static void shutdownAutoProcessing() {

        shutdownFlag = true;
        if ( _masterRunnable == null ) {
        	return;
        }
        _masterRunnable.runSwitch = false;

        if (_masterRunnable.isSleeping && _masterThread != null) {
            _masterThread.interrupt();
        }
        if (_masterThread != null) {
            try {
                LOG.info("Waiting 60 seconds for background process to complete work.");
                _masterThread.join(60000); // 60 seconds allowance to complete any processing.
            } catch (InterruptedException ie) {
            }
        }
        if (_masterThread != null) {
            LOG.info("Background Processing Master Thread refusing to die.");
        }
        // as if this will make any difference at this point.
        _masterThread = null;
        _masterRunnable = null;
    }
    
    
    //  **************** REPORTING STATICS ***************
    public static boolean isRunning() {

        if (_masterThread == null){
            return false;
        }    
        if (_masterThread.isAlive()){
            return true;        	
        }
        return false;
    }

    // switch for master continue.
    private volatile boolean runSwitch = false;
    // set when the master is sleeping w/o work. ( long sleep )
    private volatile boolean isSleeping = false;

 
    
    
    /**
     *  MAIN PROCESS LOOP 
     *  sleep a bit, look for available transactions, process if found.
     *  repeat.
     */
	public void run() {
      LOG.info("BPM thread start");
        int backGroundExceptions = 0;
        int maxErrors = EMTSharedContainerProperties.getMaxBackGroundProcessorErrors();
        while (runSwitch) {
			try {
				BackGroundProcessorRequest bgpr = new BackGroundProcessorRequest();			
				bgpr.setContextRoot(BackgroundProcessMaster.contextRoot);
				bgpr.setRequestProcessingParameter("transaction");
				bgpr.setRequestResultCode(0);
				try {
					bgpr = BackgroundProcessInitiator.getInstance().executeBackGroundRequest(bgpr);
				} catch (Exception hce) {
					LOG.error("Background Processor Exception ", hce);
					runSwitch = false;
					continue;
				}
				int retCode=999;
				try {
					retCode = bgpr.getRequestResultCode();
				} catch (Exception e) {
				}
				switch (retCode) {
					case 0: {
						LOG.info("Background Processed Transaction sucessfully");
						Thread.sleep(2000);
						break;
					}
					case 200: {
						LOG.info("No work to do in BackgroundProcessor");
						Thread.sleep(30000);
						break;
					}
					case 201: {
						LOG.info("Background processor is busy, waiting");
						Thread.sleep(5000);
						break;
					}
					case 400: {
						LOG.info("Invalid Parm to BackgroundProcessor");
						Thread.sleep(30000);
						break;
					}
					case 500: {
						backGroundExceptions++;
						if (backGroundExceptions>maxErrors) {
								runSwitch = false; // too many errors, stop now
						} else {
							LOG.info("Exception in BackgroundProcessor, sleep and try again.");
							Thread.sleep(30000);
						}
						break;
					}		
				default: {
						LOG.info("Invalid Backgroundprocessor response:" + bgpr.getRequestResultCode());
						backGroundExceptions++;
						if (backGroundExceptions>maxErrors){ 
								runSwitch = false; // too many errors, stop now
						}
						break;
					}
				}  // switch statement
			} catch (Exception e) {
				LOG.error("ProcessMaster main loop severe exception, halting the processor.",e);
				runSwitch = false;
				break; 
			}
		}

		// TODO - verify that this is neccesary. Life cycle of Thread and Runnable objects?
		_masterThread = null;
		_masterRunnable = null;

	}

}
