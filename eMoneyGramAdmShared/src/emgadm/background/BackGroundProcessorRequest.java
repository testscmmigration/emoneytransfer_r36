package emgadm.background;

public class BackGroundProcessorRequest {

	private String requestProcessingParameter;
	private int requestResultCode = -1;
	private String contextRoot;
	
	public String getRequestProcessingParameter() {
		return requestProcessingParameter;
	}
	public void setRequestProcessingParameter(String requestProcessingParameter) {
		this.requestProcessingParameter = requestProcessingParameter;
	}
	public int getRequestResultCode() {
		return requestResultCode;
	}
	public void setRequestResultCode(int requestResultCode) {
		this.requestResultCode = requestResultCode;
	}
	public String getContextRoot() {
		return contextRoot;
	}
	public void setContextRoot(String contextRoot) {
		this.contextRoot = contextRoot;
	}
	

}
