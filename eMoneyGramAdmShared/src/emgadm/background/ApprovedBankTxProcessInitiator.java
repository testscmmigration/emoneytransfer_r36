package emgadm.background;

import java.util.Date;

import javax.servlet.ServletException;


import org.apache.log4j.Logger;

import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;

public class ApprovedBankTxProcessInitiator {
	private static final ApprovedBankTxProcessInitiator instance = new ApprovedBankTxProcessInitiator();
	public static final int RESPONSE_PROCESSED = 0;
	public static final int RESPONSE_CODE_NO_WORK = 200;
	public static final int RESPONSE_CODE_BUSY = 201;
	public static final int RESPONSE_BAD_PARM = 400;
	public static final int RESPONSE_INTERNAL_EXCEPTION = 500;
		
	private volatile Object lockObj = null;
	private ApprovedBankTxProcessWorker bob = null; // the Builder, of course. 

	private static final Logger LOG = EMGSharedLogger.getLogger(ApprovedBankTxProcessInitiator.class);

	private ApprovedBankTxProcessInitiator() {
		// singleton
	}

	public static final ApprovedBankTxProcessInitiator getInstance() {
		return instance;
	}

	public ApprovedBankTxProcessorRequest executeRequest(
			ApprovedBankTxProcessorRequest bgpr) throws Exception {
		String act = bgpr.getRequestProcessingParameter();
		if ((act == null) || !"transaction".equals(act)) {
			LOG.error(this.getClass().getName()
					+ " Error in (ApprovedBankTx)BackGround Processor, invalid request parameter.");
			bgpr.setRequestResultCode(RESPONSE_BAD_PARM);
			return bgpr;
		}

		if (bob != null) {
			LOG.debug("(ApprovedBankTx)Worker busy.");
			bgpr.setRequestResultCode(RESPONSE_CODE_BUSY);
			return bgpr;
		}

		if (lockObj == null) {
			init();
		}

		synchronized (lockObj) {
			try {
				if (bob != null) {
					LOG.error("(ApprovedBankTx)sync block entered w/bob not null.");
					bgpr.setRequestResultCode(RESPONSE_CODE_BUSY);
					return bgpr;
				}

				LOG.debug("(ApprovedBankTx)Back Ground Initiator looking for work.");

				// Execute!
				try {
					Date dateUntil = new Date(System.currentTimeMillis() - bgpr.getMinimumTimeToProcess());
					bob = ApprovedBankTxProcessWorker.getWorker(dateUntil); // getWorker
				} catch (DataSourceException dse) {
					LOG.error("(ApprovedBankTx)Datasource Exception creating Worker. ", dse);
					bob = null;
					bgpr.setRequestResultCode(RESPONSE_CODE_NO_WORK);
					return bgpr;
				}

				if (bob == null) {
					LOG.debug("(ApprovedBankTx)Background Initiator, no work to do.");
					bgpr.setRequestResultCode(RESPONSE_CODE_NO_WORK);
					return bgpr;
				}

				// Method!
				bob.processApprovedTransactions(bgpr.getContextRoot());
				LOG.info("completed processApprovedBankTx(). updating stats.");
				BackgroundProcessControl.lastProcessed = bob.myTrans.getEmgTranId();
				BackgroundProcessControl.masterProcessedCount++;
				BackgroundProcessControl.setMasterProcessExceptionCount(BackgroundProcessControl
						.getMasterProcessExceptionCount() + 1);
				bgpr.setRequestResultCode(RESPONSE_PROCESSED);
				LOG.info("(ApprovedBankTx)Processed Tran ID:" + bob.myTrans.getEmgTranId());
				return bgpr;
			} catch (Exception e) {
				LOG.error("(ApprovedBankTx)Background Process Exception: ", e);
				bgpr.setRequestResultCode(RESPONSE_INTERNAL_EXCEPTION);
				return bgpr;
			} finally {
				bob = null;
			}
		}
	}

	public void init() throws ServletException {
		lockObj = new Object();
		
	}
}