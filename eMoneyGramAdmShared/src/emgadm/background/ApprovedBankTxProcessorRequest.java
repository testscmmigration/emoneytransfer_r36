package emgadm.background;

public class ApprovedBankTxProcessorRequest {

	private String requestProcessingParameter;
	private int requestResultCode = -1;
	private String contextRoot;
	private long minimumTimeToProcess;
	
	public String getRequestProcessingParameter() {
		return requestProcessingParameter;
	}
	public void setRequestProcessingParameter(String requestProcessingParameter) {
		this.requestProcessingParameter = requestProcessingParameter;
	}
	public int getRequestResultCode() {
		return requestResultCode;
	}
	public void setRequestResultCode(int requestResultCode) {
		this.requestResultCode = requestResultCode;
	}
	public String getContextRoot() {
		return contextRoot;
	}
	public void setContextRoot(String contextRoot) {
		this.contextRoot = contextRoot;
	}
	public long getMinimumTimeToProcess() {
		return minimumTimeToProcess;
	}
	public void setMinimumTimeToProcess(long minimumTimeToProcess) {
		this.minimumTimeToProcess = minimumTimeToProcess;
	}
	

}
