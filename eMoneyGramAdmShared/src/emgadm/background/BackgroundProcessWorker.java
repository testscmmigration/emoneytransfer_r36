package emgadm.background;



import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;


import com.moneygram.service.moneygramscoringservice_v3.Decision;
import com.moneygram.service.moneygramscoringservice_v3.DecisionAndScoreResponse;

import emgadm.common.CyberSourceScoring;
import emgadm.dataaccessors.ManagerFactory;
import emgadm.dataaccessors.TransactionManager;
import emgadm.exceptions.InvalidRRNException;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.property.EMTAdmAppProperties;
import emgadm.property.EMTAdmContainerProperties;
import emgadm.services.NotificationAccess;
import emgadm.services.NotificationAccessImpl;
import emgadm.services.ServiceFactory;
import emgadm.services.TransactionScoring;
import emgadm.services.TransactionService;
import emgadm.services.iplookup.melissadata.client.IPLocatorService;
import emgshared.dataaccessors.ConsumerProfileDAO;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.dataaccessors.PropertyDAO;
import emgshared.dataaccessors.TransactionDAO;
import emgshared.exceptions.AgentConnectException;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.DuplicateActiveProfileException;
import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerStatus;
import emgshared.model.EMTTransactionType;
import emgshared.model.IPDetails;
import emgshared.model.PropertyBean;
import emgshared.model.PropertyKey;
import emgshared.model.ScoringSingleton;
import emgshared.model.Transaction;
import emgshared.model.TransactionComment;
import emgshared.model.TransactionScoreResult;
import emgshared.model.TransactionStatus;
import emgshared.model.TransactionType;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.ConsumerProfileService;
import emgshared.services.CountryExceptionService;
import emgshared.services.ScoringService;
import emgshared.util.Constants;

public class BackgroundProcessWorker {
	private static final Logger LOG  = EMGSharedLogger.getLogger(BackgroundProcessWorker.class);
	private static String CSRID = null;
	private static String CSRID_PREFX = null;
	private static final String USER_ID = "emgwww";
	private static final String PREMIER_AUTOMATED_PROCESS_CODE = "PRE";
	private static final String SCORING_AUTOMATED_PROCESS_CODE = "ASP";
	private static final String VELOCITY_FAILURE_PROCESS_CODE = "VEL";
	private static final String COUNTRY_EXCEPTION_PROCESS_CODE = "CEX";
	private static final String APP = TransactionStatus.APPROVED_CODE;
	private static final String NOF = TransactionStatus.NOT_FUNDED_CODE;
	
	private static String contextRoot = "";
	// added by Ankit Bhatt for 403
	private static final int MAX_ATTEMPT_CYBERDM_SCORING = 3;
	// ended

	// added for MBO-1050
	private static final String bankFundingAccType = "BANK-CHK";
	// ended
	static {
		
		CSRID_PREFX = EMTAdmAppProperties.getSystemUserIdPrefix();
		try {
			LOG.debug("try to get localhost name");
			java.net.InetAddress localMachine = java.net.InetAddress
					.getLocalHost();
			String serverId = localMachine.getHostName().toUpperCase()
					.replaceAll(".AD.MONEYGRAM.COM", "");
			CSRID = CSRID_PREFX + serverId;
		} catch (java.net.UnknownHostException uhe) {
			LOG.error("static init exception while looking up server name",
					uhe);
			CSRID = CSRID_PREFX + " <unknown> ";
		}
		LOG.debug("CSRID set to " + CSRID);
	}

	/**
	 * Factory Method.
	 * 
	 * @return loaded worker with transaction, if no work to do, returns null.
	 * @throws DataSourceException
	 */
	protected static BackgroundProcessWorker getWorker()
			throws DataSourceException {
		LOG.debug("isWorkToDo? call getTransactionForAutoProcess() ");
		BackgroundProcessWorker newBob = new BackgroundProcessWorker();
		// this bean is not fully populated.
		// only tranId and CSR fields have data.

		newBob.myTrans = newBob.ts.getTransactionForAutoProcess(USER_ID,
				TransactionStatus.AUTOMATIC_SCORING_PROCESS_CODE,
				TransactionStatus.NOT_FUNDED_CODE, CSRID, null, null, null,null);

		if (newBob.myTrans == null) {
			//MBO-1920 Reprocess APP/CCA Transaction
			int timeInterval = 0;
			if(!EMTAdmContainerProperties.getReprocessTransactionInterval().trim().isEmpty()) {
				timeInterval = Integer.parseInt(EMTAdmContainerProperties.getReprocessTransactionInterval()); // in minutes
			}	
			if(timeInterval > 0){
				BackgroundProcessWorker worker = new BackgroundProcessWorker();
				Calendar now = Calendar.getInstance();
				now.add(Calendar.MINUTE,-timeInterval);
				String intervalTime = (new SimpleDateFormat("MM/dd/yyyy h:mm:ss a")).format(now.getTime());
				DateFormat formatter =  new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
				try {
					Date updateDate = formatter.parse(intervalTime);
					worker.myTrans = newBob.ts.getTransactionForAutoProcess(USER_ID,
							 TransactionStatus.APPROVED_CODE,TransactionStatus.CC_AUTH_CODE, CSRID, null, null, null,updateDate);
					
				} catch (ParseException e) {
					LOG.error("Exception occoured while formatting time",e);
				}
				if (worker.myTrans == null) {
					return null;
				} else {
					worker.myTrans = ManagerFactory.createTransactionManager()
							.getTransaction(worker.myTrans.getEmgTranId());
					worker.ts = ServiceFactory.getInstance()
							.getTransactionService(
									worker.myTrans.getPartnerSiteId());

					worker.processApprovedTransactions(worker.myTrans);
				}
            } else {
				return null;
			}	
            // MBO-1920 Ends Here
        } else {

		// get a fully populated Transaction bean.
			newBob.myTrans = ManagerFactory.createTransactionManager()
			.getTransaction(newBob.myTrans.getEmgTranId());
			// Hard coding for the Melissa service call
			if(!TransactionStatus.APPROVED_CODE.equalsIgnoreCase(newBob.myTrans.getTranStatCode())){
				IPDetails ipDetails = getIPDetailsFromLookupService(newBob.myTrans
						.getSndCustIPAddrId());
				newBob.myTrans.setIpDetails(ipDetails);
			}
			newBob.tm.updateTransaction(newBob.myTrans, CSRID);

			// getting correct localized transaction service instance
			newBob.ts = ServiceFactory.getInstance().getTransactionService(
					newBob.myTrans.getPartnerSiteId());
			LOG.debug("Work to do - myTrans.getEmgTranId():"
					+ newBob.myTrans.getEmgTranId());
			return newBob;
		}
		return null;
	}
	
	//MBO-1920 Reprocess APP/CCA Transaction
	protected void processApprovedTransactions(Transaction transaction){
		try {
			//MBO-3720 condition added to check for EP transactions
			if (TransactionType.EXPRESS_PAYMENT_SEND_CODE.equals(
					transaction.getEmgTranTypeCode())) {
				LOG.error("Calling processExpressPayTransaction method");
				ts.processExpressPayTransaction(transaction.getEmgTranId(),
						CSRID, APP, TransactionStatus.CC_AUTH_CODE, contextRoot);
			} else {
				LOG.error("Calling processMoneyGramTransaction method");
				ts.processMoneyGramTransaction(transaction.getEmgTranId(),
						CSRID, APP, TransactionStatus.CC_AUTH_CODE, contextRoot);
			}	
		} catch (TransactionAlreadyInProcessException e) {
			LOG.error("Transaction already in process",e);
		} catch (TransactionOwnershipException e) {
			LOG.error("Transaction Ownersip Exception", e);
		} catch (AgentConnectException e) {
			LOG.error("Agent Connect Exception",e);
		} catch (InvalidRRNException e) {
			LOG.error("Invalid RRN",e);
		} catch (DataSourceException e) {
			LOG.error("DateSource Exception",e);
		} catch (SQLException e) {
			LOG.error("SQL Exception",e);
		}
	}
	//MBO-1920 Ends Here

	private BackgroundProcessWorker() {
	}

	private TransactionService ts = ServiceFactory.getInstance()
			.getTransactionService();
	private TransactionScoring tscore = ServiceFactory.getInstance()
			.getTransactionScoring();
	private TransactionManager tm = ManagerFactory.createTransactionManager();
	private ConsumerProfileService cps = emgshared.services.ServiceFactory
			.getInstance().getConsumerProfileService();
	private CountryExceptionService ces = emgshared.services.ServiceFactory
			.getInstance().getCountryExceptionService();
	private EMTSharedDynProperties dProp = new EMTSharedDynProperties();
	protected Transaction myTrans = null;

	public void scoreAndProcessTransaction(final String contextRoot) {

		LOG.debug("Enter scoreAndProcessTransaction.");

		try {
			// change status to FFS/NOF
			// if an Exception occurs the transaction will be accessible
			tm.setTransactionStatus(myTrans.getEmgTranId(),
					TransactionStatus.FORM_FREE_SEND_CODE,
					// use fixed fields because the transaction bean is
					// incomplete.
					TransactionStatus.NOT_FUNDED_CODE, CSRID);
			// If it's in the queue, we always score. It wouldn't be there
			// otherwise.
			// this also saves the score in the database.
			Map<String, Object> transactionScoreMap = tscore
					.getTransactionScore(CSRID, myTrans.getEmgTranId());
			// now in FFS/NOF and scored. Shall we do more? ( autoProcess, i.e.
			// autoApprove? )
			EMTTransactionType tt = new EMTTransactionType();
			tt.setEmgTranTypeCode(myTrans.getEmgTranTypeCode());
			tt.setPartnerSiteId(myTrans.getPartnerSiteId());
			tt.setEmgTranTypeCodeLabel(myTrans.getEmgTranTypeCode());
			ConsumerProfile cp = cps.getConsumerProfile(
					Integer.valueOf(myTrans.getCustId()), CSRID, USER_ID);
			// check to see if the customer is a premier customer
			boolean premierCustomer = "P"
					.equalsIgnoreCase(cp.getCustPrmrCode());
			dProp.checkForRefreshDynProperties(CSRID);
			// see if Premier Automation Prop is on
			boolean premierEPAutomation = dProp.isPrmrEPAllowed();
			boolean premierESAutomation = dProp.isPrmrESAllowed();
			boolean premierMGAutomation = dProp.isPrmrMTAllowed();
			boolean premierDSAutomation = dProp.isPrmrDSAllowed();
			// default to premierAutomated=false
			boolean premierAutomated = false;
			// get the trans type
			boolean isDSTran = myTrans.getEmgTranTypeCode().equals(
					TransactionType.DELAY_SEND_CODE);
			boolean isEPTran = myTrans.getEmgTranTypeCode().equals(
					TransactionType.EXPRESS_PAYMENT_SEND_CODE);
			boolean isESTran = myTrans.getEmgTranTypeCode().equals(
					TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE);
			boolean isMGTran = myTrans.getEmgTranTypeCode().equals(
					TransactionType.MONEY_TRANSFER_SEND_CODE);

			// if P2P and Premier Prop on and Premier Cust, then automate.
			if (isMGTran && premierMGAutomation && premierCustomer) {
				premierAutomated = true;
			}	
			// if EP and Premier Prop on and Premier Cust, then automate.
			if (isEPTran && premierEPAutomation && premierCustomer) {
				premierAutomated = true;
			}
			// if ES and Premier Prop on and Premier Cust, then automate.
			if (isESTran && premierESAutomation && premierCustomer) {
				premierAutomated = true;
			}
			// if DS and Premier Prop on and Premier Cust, then automate.
			if (isDSTran && premierDSAutomation && premierCustomer) {
				premierAutomated = true;
			}
			if (!premierAutomated
					&& ScoringSingleton.getInstance().isAutoApprove(tt) == false) {
				return; // no. get outta here. ( after doing finally{}... )
			}	
			String src = ((TransactionScoreResult) transactionScoreMap
					.get("Transaction Score Result")).getResultCode(); // scoring
																		// result
																		// code.
			if (!premierAutomated && "REJ".equals(src)) {
				tm.setTransactionStatus(myTrans.getEmgTranId(),
						TransactionStatus.DENIED_CODE,
						TransactionStatus.NOT_FUNDED_CODE, CSRID);
				LOG.debug("Decision: Reject Transaction:"
						+ myTrans.getEmgTranId());
				try {
					// Send deny email.
					Transaction tns = tm.getTransaction(myTrans.getEmgTranId());
					sendDenyEmail(tns);
				} catch (Exception e) {
					LOG.error("Send Email failed for tran#:"
							+ myTrans.getEmgTranId());
				}
				return;
			}

			// Check for Destination Country Override
			if (!isEPTran
					&& ces.isCountryExceptionOverride(myTrans
							.getRcvISOCntryCode())) {
				// this destination country requires manual review of the
				// transaction regardless of scoring and premier status
				myTrans.setTranRvwPrcsCode(COUNTRY_EXCEPTION_PROCESS_CODE);
				myTrans.setTranStatCode(TransactionStatus.FORM_FREE_SEND_CODE);
				myTrans.setTranSubStatCode(TransactionStatus.NOT_FUNDED_CODE);
				tm.updateTransaction(myTrans, CSRID);
				LOG.debug("Decision: Country Exception - Manual Review needed:"
						+ myTrans.getEmgTranId());
				return;
			}
		
			if (!premierAutomated && "REVW".equals(src)) {
				LOG.debug("Decision: Review Transaction "
						+ myTrans.getEmgTranId());
				return;
			}
			// if it's ready to automate ...
			if ((premierAutomated || "APRV".equals(src)) && !isVelocityOKToAutomate(myTrans)) {
				// check to see if it fails velocity test ...
					myTrans.setTranRvwPrcsCode(VELOCITY_FAILURE_PROCESS_CODE);
					myTrans.setTranStatCode(TransactionStatus.FORM_FREE_SEND_CODE);
					myTrans.setTranSubStatCode(TransactionStatus.NOT_FUNDED_CODE);
					tm.updateTransaction(myTrans, CSRID);
					LOG.debug("Decision: Velocity Failure - Manual Review needed:"
							+ myTrans.getEmgTranId());
					return;
			}

			// passed Velocity and is Premier or it scored well. Send it out.
			if (premierAutomated || "APRV".equals(src)) {
				// AutoProcess the transaction.
				LOG.debug("Processing Transaction:" + myTrans.getEmgTranId());
				ts.setTransactionStatus(myTrans.getEmgTranId(), APP, NOF, CSRID);
				myTrans.setTranStatCode(APP);
				myTrans.setTranSubStatCode(NOF);
				if (premierAutomated) {
					myTrans.setTranRvwPrcsCode(PREMIER_AUTOMATED_PROCESS_CODE);
				} else {
					myTrans.setTranRvwPrcsCode(SCORING_AUTOMATED_PROCESS_CODE);
				}	
				tm.updateTransaction(myTrans, CSRID);
				List<String> tranProcessError = null;
				try {
					/** EXPRESS PAYMENT * */
					if (isEPTran) {
						tranProcessError = ts.processExpressPayTransaction(
								myTrans.getEmgTranId(), CSRID, APP, NOF,
								contextRoot);
					} else if (isESTran) {  /** ECONOMY SEND **/
						ServiceFactory
								.getInstance()
								.getTransactionService()
								.sendEconomyServicePendingEmail(
										tm.getTransaction(myTrans
												.getEmgTranId()), USER_ID);
					} else if (isMGTran) {
					/** PERSON TO PERSON - MONEYGRAM SEND - SAME DAY * */
						tranProcessError = ts.processMoneyGramTransaction(
								myTrans.getEmgTranId(), CSRID, APP, NOF,
								contextRoot);
					} else if (isDSTran) {
					/** AFFILIATE PROGRAM - DELAYED SEND - FOUR HOURS * */
						tranProcessError = ts.approveDelayedSendTransaction(
								myTrans.getEmgTranId(), CSRID, APP, NOF,
								contextRoot);
					} else if (!isDSTran && !isEPTran && !isESTran && !isMGTran) {
					/** UNKNOWN TRANS TYPE * */
						LOG.error("can't recognize transaction type. tran:"
								+ myTrans.getEmgTranId() + " type code:"
								+ myTrans.getEmgTranTypeCode());
					}
				} catch (Exception e) {
					LOG.error(
							"transaction process exception, myTrans.getEmgTranId():"
									+ myTrans.getEmgTranId(), e);
				}
				if (tranProcessError != null) {
					String errsInLine = "";
					for (int i = 0; i < tranProcessError.size(); i++) {
						errsInLine += (String) tranProcessError.get(i) + "  ";
					}
					LOG.error(myTrans.getEmgTranTypeCode() + " transaction #:"
							+ myTrans.getEmgTranId()
							+ " has returned error codes:" + errsInLine);
				}
			} else {
				LOG.error("unknown transaction score result code: " + src
						+ " for tran:" + myTrans.getEmgTranId()
						+ "  cannot process.");
			}
		} catch (Exception sqe) {
			LOG.error(
					"Exception in worker. myTrans.getEmgTranId():"
							+ myTrans.getEmgTranId(), sqe);
		} finally {
			// RELEASE OWNERSHIP ( LOCK )
			try {
				LOG.debug("call releaseOwnership() - tranId:"
						+ myTrans.getEmgTranId());
				ts.releaseOwnership(myTrans.getEmgTranId(), CSRID);
			} catch (TransactionAlreadyInProcessException taip) {
				LOG.error(
						"releaseOwnership Exception - tranId:"
								+ myTrans.getEmgTranId(), taip);
			} catch (TransactionOwnershipException toe) {
				LOG.error(
						"releaseOwnership Exception - tranId:"
								+ myTrans.getEmgTranId(), toe);
			}
		}
		LOG.debug("worker finished. tran:" + myTrans.getEmgTranId());
		LOG.debug("Exit scoreAndProcessTransaction.");

		return;
	}


	public void scoreMSSAndProcessTransaction(final String contextRoot)
			throws Exception {

		String emgTranTypeCode = myTrans.getEmgTranTypeCode();
		boolean isDSTran = emgTranTypeCode
				.equals(TransactionType.DELAY_SEND_CODE);
		boolean isEPTran = emgTranTypeCode
				.equals(TransactionType.EXPRESS_PAYMENT_SEND_CODE);
		boolean isESTran = emgTranTypeCode
				.equals(TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE);
		boolean isMGTran = emgTranTypeCode
				.equals(TransactionType.MONEY_TRANSFER_SEND_CODE);
		Transaction transaction = tm.getTransaction(myTrans.getEmgTranId());
		ConsumerProfileService consumerProfileService = emgshared.services.ServiceFactory
				.getInstance().getConsumerProfileService();
		CyberSourceScoring cyberSourceScoring = new CyberSourceScoring();
		// added by Ankit Bhatt for 403
		int nbrAttempts = 0;
		String errorCode = "";
		// ended by Ankit Bhatt
		try {

			ConsumerProfile consumerProfile = consumerProfileService
					.getConsumerProfile(Integer.valueOf(myTrans.getCustId()),
							CSRID, USER_ID);

			// get the results from scoring service
			emgshared.services.ServiceFactory serviceFactory = emgshared.services.ServiceFactory
					.getInstance();
			ScoringService scoringService = serviceFactory.getScoringService();
			// Invoking the ScoringService
			DecisionAndScoreResponse decisionAndScoreResponse = null;
			Decision cyberDMDecision = null;
			// added by Ankit Bhatt for 403
			while (nbrAttempts < MAX_ATTEMPT_CYBERDM_SCORING) {

				try {

					decisionAndScoreResponse = cyberSourceScoring
							.getCyberSourceDecision(transaction,
									consumerProfile, scoringService, CSRID, tm);

				} catch (Exception exception) {
					LOG.error("Error in invoking the scroing service",
							exception);

					// MBO-1442
					// When EmtAdmin recieve review score from Cybersource DM,
					// send mail to User

					sendDMReviewEmail(myTrans);
					// MBO-1142
					//check should be done ..bcoz if first time fails and exception is thrown it will come out of loop.
					if(nbrAttempts == (MAX_ATTEMPT_CYBERDM_SCORING - 1)){
						throw exception;
					//will be caught at outer catch when the trial is third time and failed.
					}
				}
				// Decision response from the scoring service is returned.
				cyberDMDecision = decisionAndScoreResponse.getDecision();
				LOG.info("error object from response is "
						+ decisionAndScoreResponse.getErrors());
				// If valid response, then break the loop
				if (decisionAndScoreResponse != null && cyberDMDecision != null
						&& cyberDMDecision.getValue() != null
						&& decisionAndScoreResponse.getErrors() == null) {
					LOG.info("Valid response - either accept/reject/review - has been got fromCyberDM, breaking while loop");
					break;
				}

				if (cyberDMDecision != null
						&& Decision._reject.equalsIgnoreCase(cyberDMDecision.getValue())
						&& decisionAndScoreResponse.getErrors() != null
						&& decisionAndScoreResponse.getErrors().getError() != null
						&& decisionAndScoreResponse.getErrors().getError().getCanRetry()) {
					errorCode = insertComment(nbrAttempts,
							decisionAndScoreResponse);
				} else {// if canRetry = false and error code is there, throw
						// exception to catch in generic exception
					if (decisionAndScoreResponse.getErrors() != null
							&& decisionAndScoreResponse.getErrors().getError() != null) {
						errorCode = decisionAndScoreResponse.getErrors()
								.getError().getReasonCode();

					}
					break;
				}

				LOG.info("nbrAttempts value in scoreMSSAndProcessTransaction is "
						+ nbrAttempts);
				nbrAttempts++;
			}
			LOG.debug("cyberDMDecision   -> " + cyberDMDecision.getValue());

			cyberSourceScoring.updateScoreAndAllComments(
					decisionAndScoreResponse, transaction, consumerProfile,
					scoringService, cyberDMDecision, isEPTran, isMGTran);

			if (cyberDMDecision != null) {
				if (cyberDMDecision.getValue().equals(Decision._reject)) {

					transaction.setTranRvwPrcsCode("ARJ");
					tm.updateTransaction(transaction, CSRID);
					updateTransactionReject(transaction,
							consumerProfileService, consumerProfile);
					return;
				}

				if (cyberDMDecision.getValue().equals(Decision._review)) {
					LOG.debug("Decision: Review Transaction "
							+ myTrans.getEmgTranId()+" ::: for consumer Id"+consumerProfile.getId());
					
					// MBO-1442
					// When EmtAdmin recieve review score from Cybersource DM,
					// send mail to User

					sendDMReviewEmail(myTrans);
					// MBO-1142
					/*
					 * MBO 2158 If Decision is Review Check if profile has
					 * EmailAge Score, if it dose not have call Email Age Score
					 * and update score in DB
					 */
					ConsumerProfileDAO dao = new ConsumerProfileDAO();
                    CyberSourceScoring  scoring = new CyberSourceScoring();
					if (consumerProfile.getEmailScore() == null || consumerProfile
							.getEmailScore().isEmpty()) {
						LOG.debug("calling email age for consumer Id "
								+ consumerProfile.getId()
								+ ":::: for review state and profile created date "+consumerProfile.getCreateDate());
						Map<String,Object> resultMap  = scoring.getEmailAgeInfo(consumerProfile,transaction.getSndCustIPAddrId());
						String score = resultMap.get("score")!=null ? resultMap.get("score").toString() : null;
						String firstSeenDate = resultMap.get("formatedFirstSeenDate")!=null ? 
								resultMap.get("formatedFirstSeenDate").toString() : null;
						 consumerProfile.setEmailScore(score);
						 // Code Fix for Production SD 4745273
						 
				          consumerProfile.setEmailFirstSeen(firstSeenDate);
				          LOG.info("Email Age Scoring done successfully for tran id"+consumerProfile.getId()+
				        		  "Score"+ESAPI.encoder().encodeForHTMLAttribute(score));
						//added for 3819
				          String nameMatchCode = resultMap.get("nameMatchCode")!=null ? resultMap.get("nameMatchCode").toString() : null;
				          LOG.info("Email Age name match flag in BGProcessor " + ESAPI.encoder().encodeForHTMLAttribute(nameMatchCode));
				          //ended 3819
						 try {
								dao.saveEmailAgeInfo(consumerProfile.getId(), "emailAgeScore", score,
										consumerProfile.getUserId());
								// Code Fix for Production SD 4745273
								dao.saveEmailAgeInfo(consumerProfile.getId(),"emailFirstSeen", firstSeenDate,
										consumerProfile.getUserId());
								//added for 3819
								dao.saveEmailAgeInfo(consumerProfile.getId(),"emailAgeNameMatch", nameMatchCode,
										consumerProfile.getUserId());
								//ended 3819
							} catch (DataSourceException dataSourceException) {
								
								LOG.error("Call to Save Email Age Info  failed for tran id"+consumerProfile.getId() ,dataSourceException);
							} 
							
					// MBO 2158 Email Age changes Ends
					return;
				}
				}

				if (cyberDMDecision.getValue().equals(Decision._accept)) {
					updateTransactionApproved(contextRoot, isDSTran, isEPTran,
							isESTran, isMGTran, consumerProfileService,
							consumerProfile);
				} else {
					LOG.error("unknown transaction score result code: "
							+ " for tran:" + myTrans.getEmgTranId()
							+ "  cannot process.");

					// MBO-1442
					// When EmtAdmin recieve review score from Cybersource DM,
					// send mail to User
                 /*MBO -3671  F&F_'transaction submitted' email sent twice commenting below line 
					as sendDM Review mail is already sent in Review block*/
					//sendDMReviewEmail(myTrans);
					// MBO-1142
				}
			}

			// ended by Ankit Bhatt
        } catch (Exception exception) { // added by Ankit Bhatt for generic Exception
			//Added for MBO-3883 if an Exception occurs the transaction will be accessible
			tm.setTransactionStatus(myTrans.getEmgTranId(),
					TransactionStatus.FORM_FREE_SEND_CODE,
					TransactionStatus.NOT_FUNDED_CODE, CSRID);
			
			LOG.error(
					"exception in scoreMSSAndProcessTransaction method of BackgroundProcessor ",
					exception);
			// check if Max attempt exception has been thrown from code above,
			// no need to insert new comment here.
			if (nbrAttempts >= 2) {
				LOG.info("Max attempets for CyberDM score have been exceeded, comment has been inserted in EMTAdmin for Tran Id"+myTrans.getEmgTranId());
            } else {  // if any other exception occured then max attemts, insert comment
				LOG.info("Exception occred in Backgroundprocessworker - scoreMSSandProcessTransaction method");

				TransactionDAO transactionDAO = new TransactionDAO();
				String commentText = "Error while scoring in CyberDM";
				if (!"".equals(errorCode)) {
					commentText += " ," + errorCode;
				}
				TransactionComment transactionComment = new TransactionComment();
				transactionComment.setTranId(myTrans.getEmgTranId());
				transactionComment.setReasonCode("OTH");
				transactionComment.setText(commentText);
				transactionDAO.insertComment(transactionComment, CSRID);
				transactionDAO.commit();
			}
		} finally {

			try {
				LOG.debug("call releaseOwnership() - tranId:"
						+ myTrans.getEmgTranId());
				ts.releaseOwnership(myTrans.getEmgTranId(), CSRID);
			} catch (TransactionAlreadyInProcessException transactionAlreadyInProcessException) {
				LOG.error(
						"releaseOwnership Exception - tranId:"
								+ myTrans.getEmgTranId(),
						transactionAlreadyInProcessException);
			} catch (TransactionOwnershipException transactionOwnershipException) {
				LOG.error(
						"releaseOwnership Exception - tranId:"
								+ myTrans.getEmgTranId(),
						transactionOwnershipException);
			}
		}

	}

	/**
	 * This method is used to send mail to User if Review status received from
	 * Cybersource DM as part of MBO-1442
	 * 
	 * @param tran
	 * @throws Exception
	 */
	private void sendDMReviewEmail(Transaction tran) throws Exception {
//Additional logic added to if condition as part of MBO-3749
//Changed the logic as part of MBO-3315
		if (tran.getTranAppVersionNumber() >= 2.0
				// Manual score mail should not send to WAP.
				&& !tran.getPartnerSiteId().equalsIgnoreCase(
						Constants.WAP_PARTNER_SITE_ID)
				|| (Constants.VERSION_WAP_NXT >= tran.getTranAppVersionNumber() && Constants.WAP_PARTNER_SITE_ID
						.equalsIgnoreCase(tran.getPartnerSiteId()))) {
			ConsumerProfile profile = emgshared.services.ServiceFactory
					.getInstance()
					.getConsumerProfileService()
					.getConsumerProfile(Integer.valueOf(tran.getCustId()),
							tran.getSndCustLogonId(), "");
			if (profile != null) {
				String preferedlanguage = profile.getPreferedLanguage();
				ConsumerEmail consumerEmail = null;
				// Loop sends out email to each address.
				NotificationAccess na = new NotificationAccessImpl();
				for (Iterator iter = profile.getEmails().iterator(); iter
						.hasNext();) {
					consumerEmail = (ConsumerEmail) iter.next();
					LOG.debug("Send Review Status mail for Transaction ID="
							+ tran.getEmgTranId());
					na.notifyReviewTransaction(tran, consumerEmail,
							preferedlanguage);
				}
			}
		}
	}

	private String insertComment(int nbrAttempts,
			DecisionAndScoreResponse decisionAndScoreResponse)
			throws DataSourceException, Exception {
		String errorCode;
		LOG.info("CyberDM score decision value comes as reject and canRetry is true");
		errorCode = decisionAndScoreResponse.getErrors().getError()
				.getReasonCode();

		TransactionDAO transactionDAO = new TransactionDAO();
		String commentText = "Error in Cyber DM scoring was encountered "
				+ errorCode;
		// logic to add "retry will be attempted" for 2 times.
		if (nbrAttempts < 2) {
			commentText += ",retry will be attempted.";
		} else if (nbrAttempts >= 2) {
			commentText += ",Max number of CyberDM scoring attempts have been exceeded.";
		}
		LOG.info("Decision is reject and comment to be inserted is "
				+ commentText);
		TransactionComment transactionComment = new TransactionComment();
		transactionComment.setTranId(myTrans.getEmgTranId());
		transactionComment.setReasonCode("OTH");
		transactionComment.setText(commentText);
		transactionDAO.insertComment(transactionComment, CSRID);
		transactionDAO.commit();

		// if Max attempts have been done and comment has been stored, throw
		// exception
		// to release transaction and return out
		/*
		 * if (nbrAttempts >= 2) { LOG.info(
		 * "Max attempt for cyberdm score has been reached, throwing exception to release txn."
		 * );
		 * 
		 * tm.setTransactionStatus(myTrans.getEmgTranId(),
		 * TransactionStatus.FORM_FREE_SEND_CODE,
		 * TransactionStatus.NOT_FUNDED_CODE, CSRID);
		 * 
		 * throw new Exception("CyberDM score MAx attempt have been exceeded.");
		 * }
		 */

		// nbrAttempts++;
		return errorCode;
	}

	private void updateTransactionApproved(final String contextRoot,
			boolean isDSTran, boolean isEPTran, boolean isESTran,
			boolean isMGTran, ConsumerProfileService consumerProfileService,
			ConsumerProfile consumerProfile)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException, DataSourceException,
			DuplicateActiveProfileException, AgentConnectException, Exception,
			InvalidRRNException, SQLException {
		// AutoProcess the transaction.
		LOG.debug("Processing Transaction:" + myTrans.getEmgTranId());
		ts.setTransactionStatus(myTrans.getEmgTranId(), APP, NOF, CSRID);
		myTrans.setTranStatCode(APP);
		myTrans.setTranSubStatCode(NOF);
		myTrans.setTranRvwPrcsCode(SCORING_AUTOMATED_PROCESS_CODE);
		tm.updateTransaction(myTrans, CSRID);

		// MBO-396
		// If profile SCRTY_CHLNG_STAT_CODE was "CHALLENGED" AND MGOSS response
		// is 'ACCEPT', this profile status should be changed to 'VERIFIED'
		if (consumerProfile.getRsaCustomerChallengeStatusCode() == ConsumerStatus.SECURITY_CHALLENGE_STATUS_CODE_CHALLENGE) {
			consumerProfile
					.setRsaCustomerChallengeStatusCode(ConsumerStatus.SECURITY_CHALLENGE_STATUS_CODE_VERIFIED);
			consumerProfileService.updateConsumerProfile(consumerProfile, true,
					USER_ID);
		}
		// MBO-396 - Ends
		List<String> tranProcessError = null;
		// changed for MBO-1050
		/** EXPRESS PAYMENT * */
		if (isEPTran && myTrans.getSndAcctTypeCode().startsWith("CC")) {
			tranProcessError = ts.processExpressPayTransaction(
					myTrans.getEmgTranId(), CSRID, APP, NOF, contextRoot);
		} else if (isESTran) {
		/** ECONOMY SEND * */
			ServiceFactory
					.getInstance()
					.getTransactionService()
					.sendEconomyServicePendingEmail(
							tm.getTransaction(myTrans.getEmgTranId()), USER_ID);
		} else if (isMGTran) {
		/** PERSON TO PERSON - MONEYGRAM SEND - SAME DAY * */
			tranProcessError = ts.processMoneyGramTransaction(
					myTrans.getEmgTranId(), CSRID, APP, NOF, contextRoot);
		} else if (isDSTran
				|| (isEPTran && bankFundingAccType.equalsIgnoreCase(myTrans
						.getSndAcctTypeCode()))) {
		/** AFFILIATE PROGRAM - DELAYED SEND - FOUR HOURS * */
		// condition changed for MBO-1050
		  tranProcessError = ts.approveDelayedSendTransaction(
					myTrans.getEmgTranId(), CSRID, APP, NOF, contextRoot);
		} else if (!isDSTran && !isEPTran && !isESTran && !isMGTran) {
		/** UNKNOWN TRANS TYPE * */
			LOG.error("can't recognize transaction type. tran:"
					+ myTrans.getEmgTranId() + " type code:"
					+ myTrans.getEmgTranTypeCode());
		}

		if (tranProcessError != null) {
			String errsInLine = "";
			for (int i = 0; i < tranProcessError.size(); i++)
				errsInLine += (String) tranProcessError.get(i) + "  ";
			LOG.error(myTrans.getEmgTranTypeCode() + " transaction #:"
					+ myTrans.getEmgTranId() + " has returned error codes:"
					+ errsInLine);
		}
	}

	private void updateTransactionReject(Transaction transaction,
			ConsumerProfileService consumerProfileService,
			ConsumerProfile consumerProfile) throws DataSourceException,
			DuplicateActiveProfileException, Exception {
		tm.setTransactionStatus(myTrans.getEmgTranId(),
				TransactionStatus.DENIED_CODE,
				TransactionStatus.NOT_FUNDED_CODE, CSRID);
		LOG.debug("Decision: Reject Transaction:" + myTrans.getEmgTranId());
		// MBO-396
		// If profile SCRTY_CHLNG_STAT_CODE was "CHALLENGED" AND MGOSS response
		// is 'DENY', this profile status should be changed to 'FAILED'
		if (consumerProfile.getRsaCustomerChallengeStatusCode() == ConsumerStatus.SECURITY_CHALLENGE_STATUS_CODE_CHALLENGE) {
			consumerProfile
					.setRsaCustomerChallengeStatusCode(ConsumerStatus.SECURITY_CHALLENGE_STATUS_CODE_FAILED);
			consumerProfileService.updateConsumerProfile(consumerProfile, true,
					USER_ID);
		}
		// MBO-396 - Ends
		// Send deny email.
		sendDenyEmail(transaction);
	}

	// this method will apply the automation velocity filter logic.
	// even if a trans scores well and/or the profile is Premier, the velocity
	// check will prevent a compromised profile from automating more than
	// defined "X" trans in
	// a defined "Y" period of time. "X" and "Y" are re-read each time this
	// method executes
	// to get the lastest settings ... no caching of these settings.
	private boolean isVelocityOKToAutomate(Transaction tran) {
		PropertyKey pk = new PropertyKey();
		pk.setPropName(EMTSharedDynProperties.MAXIMUMNUMBERAUTOMATEDPERPERIOD);
		Map propMap = PropertyDAO.getPropertiesByKey(CSRID, pk, true);
		PropertyBean pb = (PropertyBean) propMap.get(pk.getPropName());
		Integer maxNumAutoPerPeriod = Integer.valueOf(pb.getPropKey().getPropVal());
		pk.setPropName(EMTSharedDynProperties.AUTOMATIONPERIODINHRS);
		propMap = PropertyDAO.getPropertiesByKey(CSRID, pk, true);
		pb = (PropertyBean) propMap.get(pk.getPropName());
		Integer automationPeriodHrs = Integer.valueOf(pb.getPropKey().getPropVal());
		long chkBackInMilliSecs = automationPeriodHrs.intValue() * 60 * 60 * 1000;
		Integer consID = Integer.valueOf(tran.getSndCustId());
		List transactions;
		int recentTransMeetingCriteriaCount = 0;
		try {
			transactions = tm.getTransactionsForVelocityCheck(consID, CSRID);
			// careful, this procedure returns sparsely populated transactions
			// for efficiency reasons
			for (Iterator iter = transactions.iterator(); iter.hasNext();) {
				Transaction transaction = (Transaction) iter.next();
				// don't look at the current one ...
				if (transaction.getEmgTranId() != tran.getEmgTranId()) {
					// limit to transactions in the created in the last "Y"
					// seconds.
					if ((System.currentTimeMillis() - transaction
							.getCreateDate().getTime()) < chkBackInMilliSecs) {
						recentTransMeetingCriteriaCount++;
					}
				}
			}
		} catch (Exception e1) {
			return false;
		}
		if (recentTransMeetingCriteriaCount >= maxNumAutoPerPeriod.intValue()) {
			return false;
		} else {
			return true;
		}	
	}

	// TODO Remove hardcoded resource file.
	// private static final String resourceFile =
	// "emgadm.resources.ApplicationResources";

	// This is cutNpaste under the gun from TransactionDetailAction.
	// refactor in a smart way, please.
	private void sendDenyEmail(Transaction tran) throws Exception {
		ConsumerProfile profile = emgshared.services.ServiceFactory
				.getInstance()
				.getConsumerProfileService()
				.getConsumerProfile(Integer.valueOf(tran.getCustId()),
						tran.getSndCustLogonId(), "");
		if (profile != null) {
			String preferedlanguage = profile.getPreferedLanguage();
			ConsumerEmail consumerEmail = null;
			// Loop sends out email to each address.
			NotificationAccess na = new NotificationAccessImpl();
			for (Iterator iter = profile.getEmails().iterator(); iter.hasNext();) {
				consumerEmail = (ConsumerEmail) iter.next();
				na.notifyDeniedTransaction(tran, consumerEmail,
						preferedlanguage);
			}
		}
	}

	private static IPDetails getIPDetailsFromLookupService(String ipAddress) {

		LOG.debug("Enter getIPDetailsFromLookupService.");

		// check PERFORM_IP_LOOKUP_FLAG is true .
		IPDetails ipDetails = new IPDetails();
		boolean isIpLookuponScoring = true;
		if (EMTAdmContainerProperties.isPerformIPLookUpFlag()) {

			ipDetails = IPLocatorService.getIPDetailsFromLookupService(
					ipAddress, isIpLookuponScoring);
		}

		LOG.debug("Exit getIPDetailsFromLookupService.");

		return ipDetails;
	}

}
