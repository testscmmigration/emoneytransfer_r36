package emgadm.background;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;



import emgadm.dataaccessors.ManagerFactory;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.property.EMTAdmAppProperties;
import emgadm.services.ServiceFactory;
import emgadm.services.TransactionService;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.model.Transaction;
import emgshared.model.TransactionStatus;

public class ApprovedBankTxProcessWorker {
	private static Logger LOG = null;
	private static String CSRID = null;
	private static String CSRID_PREFX = null;
	private static final String USER_ID = "emgwww";
	private static final String APP = TransactionStatus.APPROVED_CODE;
	private static final String NOF = TransactionStatus.NOT_FUNDED_CODE;
	private TransactionService ts = ServiceFactory.getInstance().getTransactionService();
	protected Transaction myTrans = null;
	// temporary fix until stored procedures are changed


	static {
		LOG = EMGSharedLogger.getLogger("BackgroundProcessWorker");
		CSRID_PREFX = EMTAdmAppProperties.getSystemUserIdPrefix();
		try {
			LOG.debug("(approved transaction processor)try to get localhost name");
			java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
			String serverId = localMachine.getHostName().toUpperCase().replaceAll(".AD.MONEYGRAM.COM", "");
			CSRID = CSRID_PREFX + serverId;
		} catch (java.net.UnknownHostException uhe) {
			LOG.error("static init exception while looking up server name", uhe);
			CSRID = CSRID_PREFX + " <unknown> ";
		}
		LOG.debug("CSRID set to " + CSRID);
	}

	/**
	 * Factory Method.
	 *
	 * @return loaded worker with transaction, if no work to do, returns null.
	 * @throws DataSourceException
	 */
	protected static ApprovedBankTxProcessWorker getWorker(Date createDateTime) throws DataSourceException {
		LOG.debug("isWorkToDo? find approved bank transactions not processed after delivery time (4 hours, initially)");
		ApprovedBankTxProcessWorker newBob = new ApprovedBankTxProcessWorker();
		// this bean is not fully populated.
		// only tranId and CSR fields have data.

		// Get next approved bank transaction (APP/BKS) created after specified date (initially, for longer than 4 hours)
		//changes for MBO-1050
		newBob.myTrans = newBob.ts.getTransactionForAutoProcess(USER_ID, TransactionStatus.APPROVED_CODE,
				TransactionStatus.BANK_SETTLED_CODE, CSRID, createDateTime,
				null, null,null); 
		//changes end
		if (newBob.myTrans == null) {
			return null;
		}

		// Get a fully populated Transaction bean.
		newBob.myTrans = ManagerFactory.createTransactionManager().getTransaction(newBob.myTrans.getEmgTranId());
		LOG.debug("Work to do (approved transaction processor) - myTrans.getEmgTranId(): " + newBob.myTrans.getEmgTranId());
		return newBob;
	}

	private ApprovedBankTxProcessWorker() {
	}

	public void processApprovedTransactions(final String contextRoot) {
		List<String> tranProcessError = null;
		try {
			tranProcessError = ts.processDelayedSendTransaction(myTrans.getEmgTranId(), CSRID, APP, NOF, contextRoot);
		} catch (Exception sqe) {
			LOG.error("Exception in approved transaction process worker. myTrans.getEmgTranId():" + myTrans.getEmgTranId(), sqe);
		} finally {
			// RELEASE OWNERSHIP ( LOCK )
			try {
				LOG.debug("call (approved transaction processor) releaseOwnership() - tranId:" + myTrans.getEmgTranId());
				ts.releaseOwnership(myTrans.getEmgTranId(), CSRID);
			} catch (TransactionAlreadyInProcessException taip) {
				LOG.error("releaseOwnership (approved transaction processor) Exception - tranId:" + myTrans.getEmgTranId(), taip);
			} catch (TransactionOwnershipException toe) {
				LOG.error("releaseOwnership (approved transaction processor) Exception - tranId:" + myTrans.getEmgTranId(), toe);
			}
		}
		if (tranProcessError != null) {
			String errsInLine = "";
			for (int i = 0; i < tranProcessError.size(); i++) {
				errsInLine += (String) tranProcessError.get(i) + "  ";
			}
			LOG.error(myTrans.getEmgTranTypeCode() + " transaction #:" + myTrans.getEmgTranId() + " has returned error codes:"
					+ errsInLine);
		}
		LOG.debug("(approved transaction processor) worker finished. tran:" + myTrans.getEmgTranId());
		return;
	}
	
}