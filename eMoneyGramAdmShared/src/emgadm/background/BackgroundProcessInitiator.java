package emgadm.background;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;

import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;

public class BackgroundProcessInitiator {
	private static final BackgroundProcessInitiator instance = new BackgroundProcessInitiator();
	public static final int RESPONSE_PROCESSED = 0;
	public static final int RESPONSE_CODE_NO_WORK = 200;
	public static final int RESPONSE_CODE_BUSY = 201;
	public static final int RESPONSE_BAD_PARM = 400;
	public static final int RESPONSE_INTERNAL_EXCEPTION = 500;
		
	private Object lockObj = null;
	private BackgroundProcessWorker bob = null; // the Builder, of course. 

	private static final Logger LOG =  EMGSharedLogger.getLogger(BackgroundProcessInitiator.class);
	private BackgroundProcessInitiator(){
	}

	public static final BackgroundProcessInitiator getInstance(){
		return instance;
	}

	public BackGroundProcessorRequest executeBackGroundRequest(
			BackGroundProcessorRequest request) throws Exception {
		String act = request.getRequestProcessingParameter();
		if (act == null || !("transaction").equals(act)){
			LOG.error(this.getClass().getName()+ " Error in BackGround Processor, invalid request parameter.");			
			request.setRequestResultCode(RESPONSE_BAD_PARM);
			return request;
		}
		if (bob != null) {
			LOG.debug("Worker busy.");
			request.setRequestResultCode(RESPONSE_CODE_BUSY);
			return request;
		}

        if (lockObj == null) {
            init();
        }
		synchronized (lockObj)	{
			try	{
				if (bob != null) {
					LOG.error("sync block entered w/bob not null.");
					request.setRequestResultCode(RESPONSE_CODE_BUSY);
					return request;
				}

				LOG.debug("Back Ground Initiator looking for work.");
				try {
					bob = BackgroundProcessWorker.getWorker();
				} catch (DataSourceException dse) {
					LOG.error("Datasource Exception creating Worker. ", dse);
					bob = null;
					request.setRequestResultCode(RESPONSE_CODE_NO_WORK);
					return request;
				}

				if (bob == null) {
					LOG.debug("Background Initiator, no work to do.");
					request.setRequestResultCode(RESPONSE_CODE_NO_WORK);
					return request;
				}

								
				if (bob.myTrans.getTranAppVersionNumber() < 2.0) {
					bob.scoreAndProcessTransaction(request.getContextRoot());
				} else {
					bob.scoreMSSAndProcessTransaction(request.getContextRoot());
				}				
				//MBO-461 EMT Admin for version scoring routing changes ends
				
				LOG.info("completed scoreAndProcessTransaction(). updating stats.");
				BackgroundProcessControl.lastProcessed =	bob.myTrans.getEmgTranId();
				BackgroundProcessControl.masterProcessedCount++;
				BackgroundProcessControl.setMasterProcessExceptionCount(BackgroundProcessControl.getMasterProcessExceptionCount()+1);
				request.setRequestResultCode(RESPONSE_PROCESSED);
				LOG.info("Processed Tran ID:" + bob.myTrans.getEmgTranId());
				return request;
			} catch (Exception e) {
				LOG.error("Background Process Exception: ", e);
				request.setRequestResultCode(RESPONSE_INTERNAL_EXCEPTION);
				return request;
			} finally {
				bob = null;
			}
		}
	}

	public void init() throws ServletException {
		lockObj = new Object();
		
	}

	
}
