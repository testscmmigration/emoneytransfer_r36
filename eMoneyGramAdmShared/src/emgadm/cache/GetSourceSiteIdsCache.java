package emgadm.cache;

import java.io.Serializable;
import java.util.Collection;

import org.apache.log4j.Logger;

import com.moneygram.common.cache.Cache;
import com.moneygram.common.cache.CacheElement;
import com.moneygram.common.cache.CacheElementFactory;
import com.moneygram.common.cache.CacheException;
import com.moneygram.common.cache.CacheManager;
import com.moneygram.common.cache.CacheManagerFactory;


import emgadm.dataaccessors.ManagerFactory;
import emgadm.model.PartnerSite;
import emgshared.cache.CacheConstants;
import emgshared.dataaccessors.EMGSharedLogger;

public class GetSourceSiteIdsCache implements CacheElementFactory {
	private static final Logger LOG = EMGSharedLogger.getLogger(
			GetSourceSiteIdsCache.class);
	Collection<PartnerSite> partSites = null;

	public Collection initCacheSourceSiteId(String status) throws Exception {
		try {
			partSites = ManagerFactory.createTransactionManager()
					.getPartnerSiteIds(status);
			LOG.info("Creating the Cache Element for Source Site Id from DB");
			this.createElement(status);
		} catch (Exception e) {
			throw e;
		}

		return partSites;

	}

	public CacheElement createElement(Serializable key) throws Exception {
		try {
			CacheManager cacheManager = CacheManagerFactory
					.getCacheManagerInstance();
			Cache cache = cacheManager
					.getCache(CacheConstants.SOURCE_SITE_IDS_CACHE);
			CacheElement cacheElement = null;
			cacheElement = new CacheElement(key, (Serializable) partSites);
			cache.putElement(cacheElement);

			return cacheElement;
		} catch (Exception e) {
			throw new CacheException(e);
		}
	}

}
