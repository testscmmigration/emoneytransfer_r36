package emgadm.cache;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.struts.util.LabelValueBean;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.moneygram.common.cache.Cache;
import com.moneygram.common.cache.CacheElement;
import com.moneygram.common.cache.CacheElementFactory;
import com.moneygram.common.cache.CacheException;
import com.moneygram.common.cache.CacheManager;
import com.moneygram.common.cache.CacheManagerFactory;

import emgadm.property.EMTAdmContainerProperties;
import emgadm.services.osl.OSLException;
import emgshared.cache.CacheConstants;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.model.CountryCodeOSLResponse;
import emgshared.model.CountryDialingInfo;

public class DailingCodeInfoCache implements CacheElementFactory {
	private static final Logger LOG = EMGSharedLogger.getLogger(
			DailingCodeInfoCache.class);
	List countryDialingCode = new ArrayList();
	
	public List initCacheDailingCodeInfo(String key) throws Exception {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		Gson gson = new GsonBuilder().create();
		try {

			HttpPost postRequest = new HttpPost(
					EMTAdmContainerProperties.getOSLURLCountryDiallingCode());

			CountryCodeOSLResponse countryCodeBean = new CountryCodeOSLResponse();
			JSONObject ob = new JSONObject(countryCodeBean);
			StringEntity input = new StringEntity(ob.toString());
			input.setContentType("application/json");
			postRequest.setEntity(input);
			HttpResponse response = httpClient.execute(postRequest);
			String responseJSONStr = EntityUtils.toString(response.getEntity());
			countryCodeBean = gson.fromJson(responseJSONStr,
					CountryCodeOSLResponse.class);
			for (CountryDialingInfo dialingInfo : countryCodeBean
					.getCountryDialingInfo()) {

				if (!"null".equalsIgnoreCase(dialingInfo.getCountryCode())
						&& !"null".equalsIgnoreCase(dialingInfo
								.getCountryDialingCode())) {
					StringBuilder sb = new StringBuilder();
					sb.append(dialingInfo.getCountryCode());
					sb.append("-");
					sb.append(dialingInfo.getCountryDialingCode());
					countryDialingCode.add(new LabelValueBean(sb.toString(), sb
							.toString()));	
				}
			}
			LOG.info("Creating the Cache Element for Dailing Code Info from OSL");
			this.createElement(key);
		} catch (MalformedURLException e) {
			LOG.error("MalformedURLException in getCountryCodeInfo", e);
			throw new OSLException("Wrong URL to invoke OSL service", e);

		} catch (IOException e) {
			LOG.error("IOException in getCountryCodeInfo", e);
			throw new OSLException(e);

		} catch (JSONException e) {
			LOG.error("JSONException in getCountryCodeInfo", e);
			throw new OSLException(e);
		} finally {
			httpClient.getConnectionManager().shutdown();
		}
		return countryDialingCode;
	}
	
	public CacheElement createElement(Serializable key) throws Exception {
		try {
			CacheManager cacheManager = CacheManagerFactory
					.getCacheManagerInstance();
			Cache cache = cacheManager
					.getCache(CacheConstants.DAILING_CODE_INFO);
			CacheElement cacheElement = null;
			cacheElement = new CacheElement(key,
					(Serializable) countryDialingCode);
			cache.putElement(cacheElement);

			return cacheElement;
		} catch (Exception e) {
			throw new CacheException(e);
		}
	}
	  
       
}
