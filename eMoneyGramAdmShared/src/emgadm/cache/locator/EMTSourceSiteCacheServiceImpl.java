package emgadm.cache.locator;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.moneygram.common.cache.CacheException;

import emgadm.cache.GetSourceSiteIdsCache;
import emgadm.model.PartnerSite;
import emgshared.cache.CacheConstants;
import emgshared.cache.EMTCacheElement;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.EMGRuntimeException;

public class EMTSourceSiteCacheServiceImpl implements EMTSourceSiteCacheService {
	private static final Logger LOG = EMGSharedLogger.getLogger(
			EMTSourceSiteCacheServiceImpl.class);
	private static final EMTSourceSiteCacheService _instance = new EMTSourceSiteCacheServiceImpl();

	EMTCacheElement emtCache = new EMTCacheElement();

	@SuppressWarnings("unchecked")
	public Collection sourceSiteId(String status) throws Exception {
		Collection<PartnerSite> partSites = null;
		try {
			partSites = (Collection<PartnerSite>) emtCache
					.getCacheElementValue(CacheConstants.SOURCE_SITE_IDS_CACHE,
							status);
			if (null != partSites) {
				LOG.info("Getting the source site id from Cache");
			} else {
				GetSourceSiteIdsCache sourceSiteCache = new GetSourceSiteIdsCache();
				partSites = sourceSiteCache.initCacheSourceSiteId(status);
				LOG.info("Getting the source site id from DB as Cache Element is null");
			}
		} catch (CacheException e) {
			throw new EMGRuntimeException(e.getMessage(), e);
		}

		return partSites;

	}

	public static EMTSourceSiteCacheService instance() {
		return _instance;
	}

	}
