package emgadm.cache.locator;

import java.util.Collection;

public interface EMTSourceSiteCacheService {
	public Collection sourceSiteId(String status) throws Exception;
	
	//MBO-6817 Moved this logic to emgShared project
	/*public GetSourceSiteInfoResponse sourceSiteDetails(String sourceSite)
			throws Exception;
	
	//MBO-6101
	public MGOAgentProfile getACDetails(String sourceSiteId, String tranType) throws Exception;
	//ended
*/}
