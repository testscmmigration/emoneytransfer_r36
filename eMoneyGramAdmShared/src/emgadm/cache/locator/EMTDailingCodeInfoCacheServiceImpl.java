package emgadm.cache.locator;

import java.util.List;

import org.apache.log4j.Logger;

import com.moneygram.common.cache.CacheException;

import emgadm.cache.DailingCodeInfoCache;
import emgshared.cache.CacheConstants;
import emgshared.cache.EMTCacheElement;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.CountryCodeOSLResponse;

public class EMTDailingCodeInfoCacheServiceImpl implements EMTDailingCodeInfoCacheService {
	private static final Logger LOG = EMGSharedLogger.getLogger(
			EMTDailingCodeInfoCacheServiceImpl.class);
	private static final EMTDailingCodeInfoCacheService _instance = new EMTDailingCodeInfoCacheServiceImpl();
	EMTCacheElement emtCache = new EMTCacheElement();
	
	@SuppressWarnings("unchecked")
	public List dailingCodeInfo(String status) throws Exception {
		List<CountryCodeOSLResponse> dailingCode = null;
		try {
			dailingCode = (List<CountryCodeOSLResponse>) emtCache
					.getCacheElementValue(CacheConstants.DAILING_CODE_INFO,
							status);
			if (null != dailingCode) {
				LOG.info("Getting the dailing code info from Cache");
			}else{
				DailingCodeInfoCache dailingCodeInfo = new DailingCodeInfoCache();
				dailingCode = dailingCodeInfo.initCacheDailingCodeInfo(status);
				LOG.info("Getting the dailing code info from OSL as Cache Element is null");
			}
		}catch (CacheException e) {
			throw new EMGRuntimeException(e.getMessage(), e);
		}
	 return dailingCode;
	}
	
	public static EMTDailingCodeInfoCacheService instance() {
		return _instance;
	}

}
