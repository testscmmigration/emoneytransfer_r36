package emgadm.common;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.SocketTimeoutException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.owasp.esapi.ESAPI;


import com.moneygram.common_v1.Header;
import com.moneygram.common_v1.ProcessingInstruction;
import com.moneygram.service.moneygramscoringservice_v3.AfsInfoCode;
import com.moneygram.service.moneygramscoringservice_v3.Bank;
import com.moneygram.service.moneygramscoringservice_v3.Card;
import com.moneygram.service.moneygramscoringservice_v3.CurrencyAmount;
import com.moneygram.service.moneygramscoringservice_v3.Decision;
import com.moneygram.service.moneygramscoringservice_v3.DecisionAndScoreRequest;
import com.moneygram.service.moneygramscoringservice_v3.DecisionAndScoreResponse;
import com.moneygram.service.moneygramscoringservice_v3.Id;
import com.moneygram.service.moneygramscoringservice_v3.Receiver;
import com.moneygram.service.moneygramscoringservice_v3.Sender;
import com.moneygram.service.moneygramscoringservice_v3.TriggeredRule;

import emgadm.dataaccessors.TransactionManager;
import emgadm.model.EPartnerSite;
import emgadm.model.VBVViewBean;
import emgadm.property.EMTAdmContainerProperties;
import emgadm.services.DecryptionService;
import emgadm.services.emailage.EmailAgeProxy;
import emgadm.services.emailage.EmailAgeProxyImpl;
import emgadm.services.mgoscoringservice.MGOScoringServiceProxyImpl;
import emgadm.util.MerchantIdHelper;
import emgshared.dataaccessors.AccountsDAO;
import emgshared.dataaccessors.ConsumerProfileDAO;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.model.AccountComment;
import emgshared.model.ChargeBackScoreData;
import emgshared.model.ConsumerAccount;
import emgshared.model.ConsumerCreditCardAccount;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerStatus;
import emgshared.model.CustomerAuthenticationInfo;
import emgshared.model.DeliveryOption;
import emgshared.model.Transaction;
import emgshared.model.TransactionScore;
import emgshared.model.TransactionStatus;
import emgshared.model.TransactionType;
import emgshared.property.EMTSharedContainerProperties;
import emgshared.services.ConsumerAccountService;
import emgshared.services.ConsumerAccountServiceImpl;
import emgshared.services.ConsumerProfileService;
import emgshared.services.PCIService;
import emgshared.services.ScoringService;
import emgshared.services.ServiceFactory;
import emgshared.util.Constants;

public class CyberSourceScoring {

	private String userId = null;
	private String acceptCode = "ACCEPT";
	private Transaction myTrans = null;
	private final static Logger LOG = EMGSharedLogger.getLogger(
			CyberSourceScoring.class);

	private TransactionManager tm = null;
	MerchantIdHelper merchant = new MerchantIdHelper();

	public DecisionAndScoreResponse getCyberSourceDecision(
			Transaction transaction, ConsumerProfile consumerProfile,
			ScoringService scoringService, String csrId,
			TransactionManager transactionManager) throws Exception {

		userId = csrId;
		myTrans = transaction;
		tm = transactionManager;

		MGOScoringServiceProxyImpl mgoScoringServiceProxyImpl = new MGOScoringServiceProxyImpl();
		DecisionAndScoreResponse decisionAndScoreResponse = null;
		decisionAndScoreResponse = mgoScoringServiceProxyImpl
				.getDecisionAndScore(createScoringServiceRequest(transaction,
						consumerProfile, scoringService));

		return decisionAndScoreResponse;
	}

	public void updateScoreAndAllComments(
			DecisionAndScoreResponse decisionAndScoreResponse,
			Transaction transaction, ConsumerProfile consumerProfile,
			ScoringService scoringService, Decision cyberDMDecision,
			boolean isEPTran, boolean isMGTran) throws Exception {

		transaction.setTranStatCode(TransactionStatus.FORM_FREE_SEND_CODE);
		transaction.setTranSubStatCode(TransactionStatus.NOT_FUNDED_CODE);
		tm.updateTransaction(transaction, userId,
				decisionAndScoreResponse.getVendorRequestId(), null);
		// MBO-450 ends

		// Log for response received from Scoring service
		LOG.debug("MGOScoringService response received for transaction Id : "
				+ myTrans.getEmgTranId() + "  With decision value as : "
				+ cyberDMDecision);

		updateTransactionScore(transaction, consumerProfile, scoringService,
				decisionAndScoreResponse, cyberDMDecision);

		// MBO-821 Persist ProfileName in DB
		saveTransactionFraudDetectCodes(transaction.getEmgTranId(),
				"profileName", decisionAndScoreResponse.getActiveProfileName(),
				null);
		saveTransactionFraudDetectCodes(transaction.getEmgTranId(),
				"deviceSmartId", decisionAndScoreResponse
						.getDeviceFingerprintInfo().getSmartID(), null);
		saveTransactionFraudDetectCodes(transaction.getEmgTranId(),
				"deviceFingerPrint", decisionAndScoreResponse
						.getDeviceFingerprintInfo().getHashValue(), null);
		// MBO-821 Persist ProfileName in DB - Ends

		addAccountComment(isEPTran, isMGTran, transaction,
				decisionAndScoreResponse);

		// MBO-284 Transaction Comments

		// afsFactorCode
		StringBuilder afsFactorCodeCmnt = updateAfsFactorCode(decisionAndScoreResponse);

		// infoCodes [addressInfoCode, identityInfoCode, internetInfoCode,
		// phoneInfoCode, suspiciousInfoCode, velocityInfoCode]
		AfsInfoCode[] arrayOfAfsInfoCodes = decisionAndScoreResponse
				.getArrayOfAfsInfoCodes();

		StringBuilder infoCodesBuffer = new StringBuilder();

		updateSubScoringinfoComment(decisionAndScoreResponse,
				afsFactorCodeCmnt, infoCodesBuffer);

		updateDeviceFingerprintComment(decisionAndScoreResponse);

		updateTriggeredRule(decisionAndScoreResponse);

		updateCreditCardType(transaction.getSndCustAcctId(),
				decisionAndScoreResponse.getCardScheme(),
				transaction.getCustId());
	}

	/**
	 * @param sndCustAcctId
	 * @param cardScheme
	 * @param custId
	 * @throws DataSourceException
	 */
	private void updateCreditCardType(int sndCustAcctId, String cardScheme,
			int custId) throws DataSourceException {

		emgshared.services.ServiceFactory sharedServiceFactory = emgshared.services.ServiceFactory
				.getInstance();
		ConsumerAccountService accountService = sharedServiceFactory
				.getConsumerAccountService();

		ConsumerAccount consumerAccount = accountService.getAccountByAccountId(
				custId, sndCustAcctId);

		if (consumerAccount.getAccountType().isCardAccount()) {
			ConsumerCreditCardAccount account = (ConsumerCreditCardAccount) consumerAccount;
			if (account.getCreditOrDebitCode() == null
					|| "U".equalsIgnoreCase(account.getCreditOrDebitCode())) {

				if (cardScheme != null
						&& (cardScheme.contains("Credit") || cardScheme
								.contains("credit"))) {

					accountService.updateCreditCardAccountType(userId,
							sndCustAcctId, account.getAccountType().getCode(),
							"C");
				} else if (cardScheme != null
						&& (cardScheme.contains("Debit") || cardScheme
								.contains("debit"))) {
					accountService.updateCreditCardAccountType(userId,
							sndCustAcctId, account.getAccountType().getCode(),
							"D");
				}

			}
		}
	}

	private void saveTransactionFraudDetectCodes(int tranId, String type,
			String codeList, String providerCode) {

		try {
			tm.saveTransactionFraudDetectCodes(userId, myTrans.getEmgTranId(),
					type, codeList, providerCode);
		} catch (Exception e) {
			LOG.error("Exception in persisting " + type + " for Transaction : "
					+ tranId, e);
		}

	}

	private void addAccountComment(boolean isEPTran, boolean isMGTran,
			Transaction transaction,
			DecisionAndScoreResponse decisionAndScoreResponse)
			throws DataSourceException {
		// MBO-284 Credit Card Comments
		String binCountry = decisionAndScoreResponse.getBinCountry();
		String cardScheme = decisionAndScoreResponse.getCardScheme();
		String cardIssuer = decisionAndScoreResponse.getCardIssuer();

		ConsumerAccountService accountService = emgshared.services.ServiceFactory
				.getInstance().getConsumerAccountService();

		List acctCmntsList = accountService.getAccountCmnts(
				transaction.getSndCustAcctId(), userId, "CARD");

		if ((isMGTran || isEPTran)
				&& (binCountry != null || cardScheme != null || cardIssuer != null)
				&& (acctCmntsList.isEmpty())) {
			AccountComment comment = new AccountComment();
			comment.setAccountId(transaction.getSndCustAcctId());
			comment.setReasonCode("CARD");
			comment.setText("binCountry=" + binCountry + ", cardScheme="
					+ cardScheme + ", cardIssuer=" + cardIssuer);
			accountService.addAccountComment(comment, userId);
		}
	}

	private StringBuilder updateAfsFactorCode(
			DecisionAndScoreResponse decisionAndScoreResponse) {
		String[] afsFactorCodes = decisionAndScoreResponse.getAfsFactorCodes();
		StringBuilder afsFactorCodeCmnt = new StringBuilder();
		if (afsFactorCodes != null && afsFactorCodes.length > 0) {
			String prefix = "";
			for (String afsFactorCode : afsFactorCodes) {
				afsFactorCodeCmnt.append(prefix);
				prefix = "^";
				afsFactorCodeCmnt.append(afsFactorCode);
			}

			// afsFactorCode DB save
			StringBuilder afsFactorCode = new StringBuilder();
			String prefixComma = "";
			for (String factorCode : afsFactorCodes) {
				afsFactorCode.append(prefixComma);
				prefixComma = ",";
				afsFactorCode.append(factorCode);
			}
			saveTransactionFraudDetectCodes(myTrans.getEmgTranId(),
					"afsFactorCode", afsFactorCode.toString(), null);

		}

		return afsFactorCodeCmnt;
	}

	private void updateTransactionScore(Transaction transaction,
			ConsumerProfile consumerProfile, ScoringService scoringService,
			DecisionAndScoreResponse decisionAndScoreResponse,
			Decision cyberDMDecision) throws DataSourceException {
		// Update the emg_tran_score table with the score and the decision from
		// the response
		if (transaction != null) {
			TransactionScore transactionScore = new TransactionScore();
			transactionScore.setEmgTranId(transaction.getEmgTranId());
			transactionScore.setScoreCnfgId(0);
			if (decisionAndScoreResponse.getAfsResult() != null) {
				transactionScore.setTranScoreNbr(Integer
						.parseInt(decisionAndScoreResponse.getAfsResult()));
			}
			if (Decision._accept.equals(cyberDMDecision.getValue())) {
				transactionScore.setSysAutoRsltCode("APRV");
			} else if (Decision._review.equals(cyberDMDecision.getValue())) {
				transactionScore.setSysAutoRsltCode("REVW");
			} else if (Decision._reject.equals(cyberDMDecision.getValue())) {
				transactionScore.setSysAutoRsltCode("REJ");
			}
			scoringService.setEmgTranScore(consumerProfile.getUserId(),
					transactionScore);
		}
	}

	private void updateTriggeredRule(
			DecisionAndScoreResponse decisionAndScoreResponse)
			throws DataSourceException {
		TriggeredRule[] trigRules = decisionAndScoreResponse
				.getTriggeredRules();
		StringBuilder rulesBuf = new StringBuilder();

		if (trigRules != null) {
			for (TriggeredRule TriggeredRule : trigRules) {
				int ruleNum = TriggeredRule.getNumber();
				rulesBuf.append(ruleNum + "_decision="
						+ TriggeredRule.getDecision());
				rulesBuf.append("," + ruleNum + "_evaluation="
						+ TriggeredRule.getEvaluation());
				rulesBuf.append("," + ruleNum + "_name="
						+ TriggeredRule.getName());

				LOG.debug("Triggered Rule Comment:" + rulesBuf.toString());

				saveTransactionFraudDetectCodes(myTrans.getEmgTranId(),
						"triggeredRule", rulesBuf.toString(), null);
				// Updated as part of MBO-6839
				if (!acceptCode.equalsIgnoreCase(TriggeredRule.getDecision())) {
					StringBuilder triggeredRuleComment = new StringBuilder();
					triggeredRuleComment.append("CyberDMTriggeredRule:"
							+ "RuleName=" + TriggeredRule.getName());
					LOG.debug("CyberDMDeviceInfo Comment:"
							+ triggeredRuleComment.toString());

					try {
						tm.setTransactionComment(myTrans.getEmgTranId(), "OTH",
								triggeredRuleComment.toString(), userId);
					} catch (SQLException e) {
						LOG.error(
								"Exception in persisting DeviceFingerPrintInfo Comment",
								e);
					}
				}
			}
		}
	}

	private void updateDeviceFingerprintComment(
			DecisionAndScoreResponse decisionAndScoreResponse)
			throws DataSourceException, SQLException {
		try {
			// Device Fingerprint Comment
			String trueIPAddress = null != decisionAndScoreResponse
					.getDeviceFingerprintInfo().getTrueIPAddress() ? decisionAndScoreResponse
					.getDeviceFingerprintInfo().getTrueIPAddress() : "none";
			String trueIPAddressCity = null != decisionAndScoreResponse
					.getDeviceFingerprintInfo().getTrueIPAddressCity() ? decisionAndScoreResponse
					.getDeviceFingerprintInfo().getTrueIPAddressCity() : "none";
			String smartIDConfidenceLevel = null != decisionAndScoreResponse
					.getDeviceFingerprintInfo().getSmartIDConfidenceLevel() ? decisionAndScoreResponse
					.getDeviceFingerprintInfo().getSmartIDConfidenceLevel()
					: "none";
			String smartID = null != decisionAndScoreResponse
					.getDeviceFingerprintInfo().getSmartID() ? decisionAndScoreResponse
					.getDeviceFingerprintInfo().getSmartID() : "none";

			StringBuilder deviceFingerprintComment = new StringBuilder();
			// Changed as part of MBO-4500
			deviceFingerprintComment.append("CyberDMDeviceInfo:"
					+ "trueIPAddress=" + trueIPAddress + ",trueIPAddressCity="
					+ trueIPAddressCity + ",smartIDConfidenceLevel="
					+ smartIDConfidenceLevel + ",smartID=" + smartID);

			LOG.debug("CyberDMDeviceInfo Comment:"
					+ deviceFingerprintComment.toString());

			try {
				tm.setTransactionComment(myTrans.getEmgTranId(), "OTH",
						deviceFingerprintComment.toString(), userId);
			} catch (SQLException e) {
				LOG.error(
						"Exception in persisting DeviceFingerPrintInfo Comment",
						e);
			}
		} catch (Exception e) {
			LOG.error("Exception in updateDeviceFingerprintComment", e);
		}
	}

	private void updateSubScoringinfoComment(
			DecisionAndScoreResponse decisionAndScoreResponse,
			StringBuilder afsFactorCodeCmnt, StringBuilder infoCodesBuffer)
			throws DataSourceException, SQLException {
		try {
			String reasonCode = null;
			if (null != decisionAndScoreResponse.getErrors()) {
				if (null != decisionAndScoreResponse.getErrors().getError()) {
					if (null != decisionAndScoreResponse.getErrors().getError()
							.getReasonCode()) {
						reasonCode = decisionAndScoreResponse.getErrors()
								.getError().getReasonCode();
					}
				}
			} else {
				reasonCode = "none";
			}

			// Scoring Info Comment
			// MBO-1934 starts.
			StringBuilder scoringInfoComment = new StringBuilder();
			scoringInfoComment
					.append("DMScoreInfo:" + "rCode=" + reasonCode
							+ ",activeProfileName="
							+ decisionAndScoreResponse.getActiveProfileName()
							+ ",afsFactorCode=" + afsFactorCodeCmnt.toString())
					.append(infoCodesBuffer != null ? ", " : " ")
					.append(infoCodesBuffer != null ? infoCodesBuffer
							.toString() : " ");

			// MBO-1934 ends.
			String subScoringinfoComment = null;
			if (scoringInfoComment.toString().length() > 254) {
				subScoringinfoComment = scoringInfoComment.toString()
						.substring(0, 254);
			} else {
				subScoringinfoComment = scoringInfoComment.toString();
			}
			LOG.debug("DMScoreInfo Comment:" + subScoringinfoComment);
			try {
				tm.setTransactionComment(myTrans.getEmgTranId(), "OTH",
						subScoringinfoComment, userId);
			} catch (SQLException e) {
				LOG.error("Exception in persisting Scoring Info Comment", e);
			}
		} catch (Exception e) {
			LOG.error("Exception in updateSubScoringinfoComment", e);
		}
	}

	@SuppressWarnings("unchecked")
	private DecisionAndScoreRequest createScoringServiceRequest(
			Transaction transaction, ConsumerProfile consumerProfile,
			ScoringService scoringService) throws DataSourceException {

		LOG.debug("Enter createScoringServiceRequest.");
		boolean flagPrmrCode = false;
		DecisionAndScoreRequest decisionAndScoreRequest = new DecisionAndScoreRequest();

		// Code added for MBO - 1311 adding new MDD fields for UK and DE
		ConsumerProfileService profileService = ServiceFactory.getInstance()
				.getConsumerProfileService();
		List vbvList = tm.getVBVBeans(myTrans.getEmgTranId());
		List<CustomerAuthenticationInfo> customerAuthenticationInfo = profileService
				.getCustomerAuthInfo(transaction.getCustId());
		// Ends Here
		// MBO-2321 changes Starts here
		List<ChargeBackScoreData> cbsList = new ArrayList();
		try {
			LOG.debug("Enter call for CB score and version parameters ");
			cbsList = scoringService.getChargeBackScore(myTrans.getEmgTranId());
		} catch (Exception exception) {
			LOG.error(
					"Exception occurs while calling the GetChargeback method of scoringService",
					exception);
		}
		// MBO-2321 changes ends here

		ProcessingInstruction processingInstruction = new ProcessingInstruction();
		processingInstruction.setAction("getDecisionAndScore");
		processingInstruction.setReturnErrorsAsException(false);
		processingInstruction.setRollbackTransaction(false);

		Header header = new Header();
		header.setProcessingInstruction(processingInstruction);
		decisionAndScoreRequest.setHeader(header);

		// MBO - 2158 EMAIL AGE Integration changes Starts
		ConsumerProfileDAO dao = new ConsumerProfileDAO();
		LOG.debug("Email Age Scoring - going to be checked Tran id"
				+ myTrans.getEmgTranId());
		LOG.debug("for consumer Id" + consumerProfile.getId());
		LOG.debug("application version ::::"
				+ myTrans.getTranAppVersionNumber());
		LOG.debug("application name:::" + myTrans.getPartnerSiteId());
		
		String nameMatchCode = null;
		if ((consumerProfile.getEmailScore() == null || consumerProfile
                .getEmailScore().isEmpty())
                && isNewProfile(consumerProfile.getCreateDate())) {
            LOG.debug("Conditions satisfied to call email Age for"
                    + consumerProfile.getId());
            String score = null;
            
            String ipAddress = transaction.getSndCustIPAddrId();                    
            Map<String,Object> resultMap = null;            
            resultMap = getEmailAgeInfo(consumerProfile,ipAddress); 
            
            //added for 3819
            if(null!= resultMap && null!= resultMap.get("nameMatchCode")){
                nameMatchCode = resultMap.get("nameMatchCode").toString();
            }
            
            if(null!= resultMap && null!= resultMap.get("score")){
                score = resultMap.get("score").toString();
            }
            consumerProfile.setEmailScore(score);
            String firstSeenDate = null;
            if(null!= resultMap && null!= resultMap.get("formatedFirstSeenDate")){
                firstSeenDate = resultMap.get("formatedFirstSeenDate").toString();
            }
            consumerProfile.setEmailFirstSeen(firstSeenDate);
            //ended 3819
			LOG.debug("createScoringServiceRequest: Email Age Scoring done successfully:consumer Id"
					+ consumerProfile.getId()
					+ "score"
					+ ESAPI.encoder().encodeForHTMLAttribute(score));
			LOG.info("createScoringServiceRequest: Email Age name match code "
					+ ESAPI.encoder().encodeForHTMLAttribute(nameMatchCode));
			try {
				dao.saveEmailAgeInfo(consumerProfile.getId(), "emailAgeScore",
						score, consumerProfile.getUserId());
				dao.saveEmailAgeInfo(consumerProfile.getId(), "emailFirstSeen",
						firstSeenDate, consumerProfile.getUserId());
				// added for 3819
				dao.saveEmailAgeInfo(consumerProfile.getId(),
						"emailAgeNameMatch", nameMatchCode,
						consumerProfile.getUserId());
				// ended 3819
			} catch (DataSourceException dataSourceException) {

				LOG.error("Call to Save Email Age Info  failed.",
						dataSourceException);
			}
			// Set the score obtained in DM Request
			decisionAndScoreRequest.setEmailAgeScore(score);
			
			 //added for 8371
            consumerProfile.setEmailAgeNameMatch(nameMatchCode);
            decisionAndScoreRequest.setEmailAgeNameMatch(nameMatchCode);
 
		} else {
			LOG.debug("Email Age Scoring: User id"
                    + ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTML(consumerProfile.getUserId())) + "Score"
                    + ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTML(consumerProfile.getEmailScore())));
			decisionAndScoreRequest.setEmailAgeScore(consumerProfile
					.getEmailScore());
			decisionAndScoreRequest.setEmailAgeNameMatch(consumerProfile.getEmailAgeNameMatch());

		}

		// MBO 2158 changes Ends
		// Assigning the Merchant Id from Container Property for US and WAP
		//MBO-7330 defect fix-Changing condition for EP Send Code
		LOG.debug("transaction.getPartnerSiteId() "
				+ transaction.getPartnerSiteId());
		try {
			if (consumerProfile.getIsoCountryCode().equalsIgnoreCase(
					Constants.COUNTRY_CODE_USA) && transaction
					.getEmgTranTypeCode()
					.equalsIgnoreCase(TransactionType.MONEY_TRANSFER_SEND_CODE)) {

				decisionAndScoreRequest.setMerchantID(merchant.merchantIdInfo(
						transaction.getPartnerSiteId(),
						transaction.getEmgTranTypeCode(), null));
				populateMGOSSRequestForMGOAffliliates(decisionAndScoreRequest,
						cbsList, tm);

			} else {
				decisionAndScoreRequest.setMerchantID(merchant.merchantIdInfo(
						transaction.getPartnerSiteId(),
						transaction.getEmgTranTypeCode(), null));
				if (!vbvList.isEmpty()) {
					VBVViewBean vvb = (VBVViewBean) vbvList.get(0);
					decisionAndScoreRequest.setStatusID(vvb.getAuthCmrcCode());
					decisionAndScoreRequest.setEciRawcode(vvb.getEciRawCode());
					if (vvb.getVbvTranId().trim().length() > 0) {
						decisionAndScoreRequest.setCavv(true);
					} else {
						decisionAndScoreRequest.setCavv(false);
					}
				}
				if (!customerAuthenticationInfo.isEmpty()) {
					CustomerAuthenticationInfo customerAuthInfo = customerAuthenticationInfo
							.get(0);
					decisionAndScoreRequest.setDecisionBand(customerAuthInfo
							.getDeciBandText());
					decisionAndScoreRequest.setGbgScore(Integer
							.parseInt(customerAuthInfo.getScoreNbr()));
				}
			}

		} catch (Exception e) {
			LOG.error("Exception in retriving merchant id", e);
		}
		
		// MBO-3149 MDD Fields added for passing Biller account number that is
		// funded by express account
		if (transaction.getRcvAgcyAcctEncryp() != null) {
			DecryptionService decryptionService = emgadm.services.ServiceFactory
					.getInstance().getDecryptionService();
			decisionAndScoreRequest.setBillerAccountNumber(decryptionService
					.decryptBankAccountNumber(transaction
							.getRcvAgcyAcctEncryp()));
		}
		// MBO-3149 Ends Here
		// MBO-3150 starts here
		int numberOfDaysSinceAddressChange = profileService
				.getNumberOfDaysSinceAddressChanged(transaction.getCustId());
		LOG.info("MDD field set 41::" + numberOfDaysSinceAddressChange);
		decisionAndScoreRequest
				.setNbrDaysLastAddressChg(numberOfDaysSinceAddressChange);

		final String userId = "EMT";
		java.util.Set<emgshared.model.ConsumerAddress> addressSet = (java.util.Set<emgshared.model.ConsumerAddress>) profileService
				.getConsumerAddressHistory(transaction.getCustId(), userId,
						true);
		LOG.info("MDD field set 42::" + addressSet.size());
		decisionAndScoreRequest.setNbrActiveAddresses(addressSet.size());

		// MBO - 3150 && MBO - 3269 ends here
		decisionAndScoreRequest.setMGIReferenceCode(transaction.getEmgTranId());
		decisionAndScoreRequest.setCustomerId(transaction.getCustId());
		Sender sender = new Sender();
		sender.setFirstName(consumerProfile.getFirstName());
		sender.setLastName(consumerProfile.getLastName());
		sender.setStreet1(consumerProfile.getAddressLine1());
		sender.setCity(consumerProfile.getCity());
		sender.setState(consumerProfile.getState());
		sender.setPostalCode(consumerProfile.getPostalCode());
		sender.setCountry2CharCode(getTwoDigitCode(transaction
				.getSndISOCntryCode().trim()));
		sender.setEmail(consumerProfile.getUserId());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(consumerProfile.getBirthdate());
		sender.setDob(calendar);
		sender.setPhoneNumber(consumerProfile.getPhoneNumber());
		sender.setMiddleName(consumerProfile.getMiddleName());
		sender.setSecondLastName(consumerProfile.getSecondLastName());
		sender.setIpAddress(transaction.getSndCustIPAddrId().trim()); // MBO-1561

		CurrencyAmount currencyAmount = new CurrencyAmount();
		// MGO-728
		currencyAmount.setTotalAmount(transaction.getSndTotAmt().floatValue());
		currencyAmount.setFaceAmount(transaction.getSndFaceAmt().floatValue());
		currencyAmount.setFeeAmount(transaction.getSndFeeAmt().floatValue());
		// ended
		currencyAmount.setCurrency(EPartnerSite.USA.getPartnerSiteCurrency());
		decisionAndScoreRequest.setAmount(currencyAmount);

		Receiver receiver = new Receiver();
		receiver.setFirstName(transaction.getRcvCustFrstName());
		receiver.setLastName(transaction.getRcvCustLastName());
		receiver.setMiddleName(transaction.getRcvCustMidName());
		receiver.setSecondLastName(transaction.getRcvCustMatrnlName());
		receiver.setState(transaction.getRcvCustAddrStateName());
		receiver.setCountry3CharCode(transaction.getRcvCustAddrISOCntryCode());

		// MBO-199: Assign country code from RcvISOCntryCode for EP type
		// transactions
		if (transaction.getEmgTranTypeCode().equalsIgnoreCase(
				TransactionType.EXPRESS_PAYMENT_SEND_CODE)) {
			receiver.setCountry3CharCode(transaction.getRcvISOCntryCode());
		}

		try {
			DecryptionService decryptionService = emgadm.services.ServiceFactory
					.getInstance().getDecryptionService();

			emgshared.services.ServiceFactory sharedServiceFactory = emgshared.services.ServiceFactory
					.getInstance();
			ConsumerAccountService accountService = sharedServiceFactory
					.getConsumerAccountService();
			ConsumerAccount consumerAccount = accountService
					.getAccountByAccountId(transaction.getCustId(),
							transaction.getSndCustAcctId());

			// modified code to check acct type
			if (consumerAccount.getAccountType().isCardAccount()) {

				Card card = new Card();
				PCIService pciService = emgshared.services.ServiceFactory
						.getInstance().getPCIService();
				card.setAccountNumber(pciService.retrieveCardNumber(
						transaction.getSndCustAcctId(), true));
				for (ConsumerCreditCardAccount consumerCreditCardAccount : consumerProfile
						.getCardAccounts()) {
					if (consumerCreditCardAccount.getId() == transaction
							.getSndCustAcctId()) {
						card.setExpirationMonth(consumerCreditCardAccount
								.getExpireMonth());
						card.setExpirationYear(consumerCreditCardAccount
								.getExpireYear());
						break;
					}
				}
				/* MBO-5370 Changes Starts */
				if (transaction.getDlvrOptnId() == DeliveryOption.BANK_DEPOSIT_ID
						&& transaction.getRcvAcctHashNbr() != null) {
					decisionAndScoreRequest
							.setReceiverAccountNumberHashed(transaction
									.getRcvAcctHashNbr());
					decisionAndScoreRequest.setReceiverAgentID(transaction
							.getRcvAgentId());
				}
				/* MBO-5370 Change Ends */
				decisionAndScoreRequest.setCard(card);
			} else {

				String encryptedAccountNumber = accountService
						.getEncryptedAccountNumber(
								transaction.getSndCustAcctId(),
								consumerProfile.getUserId());
				if (encryptedAccountNumber != null
						&& !"".equals(encryptedAccountNumber.trim())) {
					String decryptedAccountNumber = decryptionService
							.decryptBankAccountNumber(encryptedAccountNumber);
					Bank bank = new Bank();
					bank.setAccountNumber(decryptedAccountNumber);// Account
																	// Number
					bank.setRoutingNumber(transaction.getBankAbaNbr());// Routing
																		// Number
					// MBO-1717
					if (transaction.getDlvrOptnId() == DeliveryOption.BANK_DEPOSIT_ID
							&& transaction.getRcvAcctHashNbr() != null) {
						decisionAndScoreRequest
								.setReceiverAccountNumberHashed(transaction
										.getRcvAcctHashNbr());
						decisionAndScoreRequest.setReceiverAgentID(transaction
								.getRcvAgentId());
					}
					decisionAndScoreRequest.setBankDetail(bank);
				}

			}

			if (transaction.getSndCustPhotoId() != null) {
				Id photoId = new Id();
				photoId.setNumber(transaction.getSndCustPhotoId());
				photoId.setIssueState(transaction.getSndCustPhotoIdStateCode());
				photoId.setIssueCountry(transaction
						.getSndCustPhotoIdCntryCode());
				sender.setPhotoId(photoId);
			}

		} catch (Exception exception) {
			// TODO Auto-generated catch block
			LOG.error("call to decryptionService failed.", exception);
		}

		decisionAndScoreRequest.setSender(sender);

		decisionAndScoreRequest
				.setPasswordChangeInLast24Hours(scoringService
						.isPasswordChangedWithin24Hours(userId,
								transaction.getCustId()));
		decisionAndScoreRequest.setNbrNegativeTransactions(Long
				.valueOf(scoringService.getBadTranCount(userId,
						transaction.getCustId())));
		// commenting below line to set Integer as a part of MBO-728
		// decisionAndScoreRequest.setNbrPositiveTransactions(new
		// Long(scoringService.getSuccessfulTranCount(CSRID,
		// transaction.getCustId())));
		// Commenting below MGISessionId population temporarily for avoiding the
		// response status as REJECT from cybersource.
		// MBO-651, MGI Session Id is changed to tran_sess_beg_id.
		decisionAndScoreRequest.setDmSessionId(transaction
				.getTranSessionBeginId());
		decisionAndScoreRequest.setReceiver(receiver);
		decisionAndScoreRequest.setAVSResultCodes(transaction.getAvsResult());
		// MBO-5620

		int rsaCustStutsCode = consumerProfile
				.getRsaCustomerChallengeStatusCode();
		if (ConsumerStatus.SECURITY_CHALLENGE_STATUS_CODE_VERIFIED == rsaCustStutsCode) {
			decisionAndScoreRequest.setRsaSecurityChallengeStatus(true);
		} else {
			decisionAndScoreRequest.setRsaSecurityChallengeStatus(false);
		}
		// ended 5620
		// As part of 12933 story change for Scoring service , the Prmr code is
		// boolean, so doing the required change below.
		if (consumerProfile.getCustPrmrCode() != null
				&& "P".equalsIgnoreCase(consumerProfile.getCustPrmrCode())) {
			flagPrmrCode = true;
		}
		decisionAndScoreRequest.setPremierCode(flagPrmrCode);
		// MBO-199: Add biller code to the request
		decisionAndScoreRequest.setBillerReceiveCode(transaction
				.getRcvAgcyCode());

		// MBO-728
		decisionAndScoreRequest.setDeliveryOption(transaction.getDlvrOptnId());

		if ("Y".equalsIgnoreCase(transaction.getGuestTransactionFlag())) {
			decisionAndScoreRequest.setIsGuestTransaction(true);
		} else {
			decisionAndScoreRequest.setIsGuestTransaction(false);
		}
		List<Transaction> tranList = scoringService.getSuccessfulTranCount(
				userId, transaction.getCustId());

		AccountsDAO accountDAO = new AccountsDAO();
		BigDecimal totLast30Days = null;
		try {
			totLast30Days = accountDAO.getLast30DayTotal(userId,
					transaction.getCustId());
			if (totLast30Days != null) {
				decisionAndScoreRequest.setTotalFaceAmountInLast30Days(Float
						.valueOf(String.valueOf(totLast30Days)));
			}
		} catch (Exception exception) {
			LOG.error("Exception occurs in getting last 30 days total");
		}

		int nbrDeniedTxns = scoringService.getDeniedTranCount(userId,
				transaction.getCustId());
		decisionAndScoreRequest.setNbrDeniedTransactions(nbrDeniedTxns);

		ConsumerAccountService consumerService = ConsumerAccountServiceImpl
				.getInstance();
		int nbrPaymentAccounts = 0;
		try {
			nbrPaymentAccounts = consumerService.getCreditCardCount(userId,
					transaction.getCustId(), "ALL");
			decisionAndScoreRequest.setNbrPaymentAccounts(nbrPaymentAccounts);
		} catch (Exception exception) {
			LOG.error("Exception occurs in getting Credit Card Txns count",
					exception);
		}

		long nbrPositiveTxns = 0;
		int nbrTransToSameCountry = 0;
		int nbrTransToSameCountryReceiver = 0;
		long daysSincelastTxn = -1;
		String rcvCountryOnLastSend = null;
		long daysSinceLastSentTxn = -1;

		for (Transaction tran : tranList) {
			if (!"EPSEND".equalsIgnoreCase(tran.getEmgTranTypeCode())) {
				if ("SEN".equalsIgnoreCase(tran.getTranStatDesc())) {
					nbrPositiveTxns++;
					if (transaction.getRcvCustAddrISOCntryCode().equals(
							tran.getRcvCustAddrISOCntryCode())) {
						nbrTransToSameCountry++;
						if (transaction.getRcvCustFrstName().trim()
								.equalsIgnoreCase(tran.getRcvCustFrstName())
								&& transaction
										.getRcvCustLastName()
										.trim()
										.equalsIgnoreCase(
												tran.getRcvCustLastName())) {
							nbrTransToSameCountryReceiver++;
						}
					}

					if (rcvCountryOnLastSend == null) {
						rcvCountryOnLastSend = tran
								.getRcvCustAddrISOCntryCode();
						decisionAndScoreRequest
								.setRcvCountryOnLastSend(rcvCountryOnLastSend);
					}

					if (daysSinceLastSentTxn == -1) {
						long createdDate = tran.getCreateDate().getTime();
						long now = Calendar.getInstance().getTime().getTime();
						daysSinceLastSentTxn = (now - createdDate) / 86400000;
						decisionAndScoreRequest.setDaysSinceLastSentTxn(Integer
								.valueOf(String.valueOf(daysSinceLastSentTxn)));
					}
				}
				if (daysSincelastTxn == -1
						&& tran.getEmgTranId() != transaction.getEmgTranId()) {
					long createdDate = tran.getCreateDate().getTime();
					long now = Calendar.getInstance().getTime().getTime();
					daysSincelastTxn = (now - createdDate) / 86400000;
					decisionAndScoreRequest.setDaysSinceLastTxn(Integer
							.valueOf(String.valueOf(daysSincelastTxn)));
				}
			}
		}
		decisionAndScoreRequest.setNbrPositiveTransactions(nbrPositiveTxns);
		decisionAndScoreRequest
				.setNbrTransSentToSameCountry(nbrTransToSameCountry);
		decisionAndScoreRequest
				.setNbrTransSentToSameCountryReceiver(nbrTransToSameCountryReceiver);
		decisionAndScoreRequest
				.setAgeOfProfile(calculateAgeOfProfile(consumerProfile
						.getCreateDate()));

		LOG.debug("Exit createScoringServiceRequest.");

		return decisionAndScoreRequest;

	}

	// MBO 2158 Email Age service Integration
	private boolean isNewProfile(Date profileCreatedDate) {
		LOG.debug("Entering isNewProfile() check---->" + profileCreatedDate);
		boolean isNewProfile = false;
		DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		Date emailAgeStartDate = null;
		try {
			emailAgeStartDate = format.parse(EMTAdmContainerProperties
					.getEmailAgeStartDate());
		} catch (ParseException parseException) {
			LOG.error("Exception in Parsing Email Age Start Date",
					parseException);
		}

		if (profileCreatedDate.after(emailAgeStartDate)) {
			isNewProfile = true;
			LOG.debug("Email age: Profile is a new profile");
		} else {
			LOG.debug("Profile is old. Hence skipping email age call");
		}

		return isNewProfile;
	}

	// MBO 2158 - Get Email Age Score

	public Map<String, Object> getEmailAgeInfo(ConsumerProfile consumerProfile,
			String ipAddress) {

		String result = "";
		String score = null;
		String errorCode = null;
		String errDesc = null;

		EmailAgeProxy emailAgeService = new EmailAgeProxyImpl();
		Map<String, Object> resultMap = null;

		try {

			result = emailAgeService.getEmailAgeScore(consumerProfile,
					ipAddress);
			LOG.info("result value in Cybersource Scoring is " + ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTML(result)));
			//added for 2896
			resultMap = EmailAgeProxyImpl
					.processResult(consumerProfile, result);
			if (resultMap != null && !resultMap.isEmpty()) {
				// age = resultMap.get("age")!=null ?
				// resultMap.get("age").toString() : null;
				score = resultMap.get("score") != null ? resultMap.get("score")
						.toString() : null;
				// formatedFirstSeenDate =
				// resultMap.get("formatedFirstSeenDate")!=null ?
				// resultMap.get("formatedFirstSeenDate").toString() : null;
				errorCode = resultMap.get("errorCode") != null ? resultMap.get(
						"errorCode").toString() : null;
				errDesc = resultMap.get("errorDesc") != null ? resultMap.get(
						"errorDesc").toString() : null;
			}
			// ended
			if ((null != errorCode && !"0".equals(errorCode) && !errorCode
					.isEmpty()) || (null != errDesc && !errDesc.isEmpty())) {
				throw new Exception();
			}

		} catch (IOException ioException) {
			// MBO -2879
			LOG.error("Email Age Exception occured for emailId : "
					+ consumerProfile.getUserId() + " ipAddress : " + ipAddress);
			LOG.error("Email Age exception Occured", ioException);
			// MBO -2879
			if (ioException instanceof SocketTimeoutException) {
				String errMessage = "Email Age Error - Timeout Occured";
				// MBO-2896
				handleEmailAgeException(errMessage, score);
				// ended
			}
		} catch (Exception emailAgeException) {
			// Handled TimeOut Exception and Exceptions with and without
			// error code.
			// MBO -2879
			LOG.error("Email Age Exception occured for emailId : "
					+ consumerProfile.getUserId() + " ipAddress : " + ipAddress);
			LOG.error("Email Age exception Occured", emailAgeException);

			String errorMessage = null;

			if (null != errorCode && !"0".equals(errorCode)
					&& !errorCode.isEmpty()) {
				errorMessage = "Email Age Error - errorcode = " + errorCode;

			} else {
				errorMessage = "Email Age Error - Exception Occured ";

			}
			// MBO-2896
			handleEmailAgeException(errorMessage, score);
			// ended
		}

		return resultMap;
	}

	// MBO-2896 - Code refactor
	private void handleEmailAgeException(String errMessage, String score) {
		try {
			tm.setTransactionComment(myTrans.getEmgTranId(), "OTH", errMessage,
					score);
		} catch (DataSourceException dataSourceException) {
			LOG.error("Exception in persisting Email Scoring Info Comment",
					dataSourceException);

		} catch (SQLException sqlException) {
			LOG.error("Exception in persisting Email Scoring Info Comment",
					sqlException);
		}

	}

	// ended 2896

	private long calculateAgeOfProfile(Date profileCreatedDate) {

		Calendar createdDate = Calendar.getInstance();
		createdDate.setTime(profileCreatedDate);

		return TimeUnit.MILLISECONDS.toDays(Calendar.getInstance().getTime()
				.getTime()
				- createdDate.getTime().getTime());
	}

	private String getTwoDigitCode(String threeDigitCode) {

		if ("GBR".equalsIgnoreCase(threeDigitCode)) {
			return "GB";
		} else if ("DEU".equalsIgnoreCase(threeDigitCode)) {
			return "DE";
		} else {
			return "US";
		}

	}
	
	private void populateMGOSSRequestForMGOAffliliates(DecisionAndScoreRequest decisionAndScoreRequest, List<ChargeBackScoreData> cbsList, TransactionManager tm ) throws DataSourceException{
		final String scoreLabel = "CUSTOM MODEL SCORE:";
		final String CB_SCORE_A = "cbScoreA= ";
		final String CB_SCORE_A_VERSION = "cbScoreAVersion= ";
		final String CB_SCORE_B = "cbScoreB= ";
		final String CB_SCORE_B_VERSION = "cbScoreBVersion= ";
		if (!cbsList.isEmpty()) {
			ChargeBackScoreData cbs = (ChargeBackScoreData) (cbsList)
					.get(0);
			//Setting Score A
			decisionAndScoreRequest.setCbScoreA(String.valueOf(cbs
					.getCbScoreA()));
			saveTransactionFraudDetectCodes(myTrans.getEmgTranId(),
					"cbScoreA", decisionAndScoreRequest.getCbScoreA(), Constants.PROVIDER_CODE_MGI);
			decisionAndScoreRequest.setCbScoreAVersion(String.valueOf(cbs
					.getCbScoreAVersion()));
			saveTransactionFraudDetectCodes(myTrans.getEmgTranId(),
					"cbScoreAVersion",
					decisionAndScoreRequest.getCbScoreAVersion(), Constants.PROVIDER_CODE_MGI);
			//Setting Score B
			decisionAndScoreRequest.setCbScoreB(String.valueOf(cbs
					.getCbScoreB()));
			saveTransactionFraudDetectCodes(myTrans.getEmgTranId(),
					"cbScoreB", decisionAndScoreRequest.getCbScoreB(), Constants.PROVIDER_CODE_MGI);
			decisionAndScoreRequest.setCbScoreBVersion(String.valueOf(cbs
					.getCbScoreBVersion()));
			saveTransactionFraudDetectCodes(myTrans.getEmgTranId(),
					"cbScoreBVersion",
					decisionAndScoreRequest.getCbScoreBVersion(), Constants.PROVIDER_CODE_MGI);
			//setting cbScoreB details MBO-6271
			try {
				StringBuilder commentBuilder = new StringBuilder();
				commentBuilder.append(scoreLabel);
				commentBuilder.append(CB_SCORE_A+String.valueOf(cbs.getCbScoreA())+";");
				commentBuilder.append(CB_SCORE_A_VERSION+String.valueOf(cbs.getCbScoreAVersion())+";");
				commentBuilder.append(CB_SCORE_B+String.valueOf(cbs.getCbScoreB())+";");
				commentBuilder.append(CB_SCORE_B_VERSION+String.valueOf(cbs.getCbScoreBVersion())+";");

				String comment = commentBuilder.toString();
				tm.setTransactionComment(myTrans.getEmgTranId(), "OTH",
						comment, userId);

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				LOG.error("Exception in persisting Scoring Info Comment",
						e);
			}
			// MBO - 2595 Display the statistical custom model score in EMT
			// Admin
		}
	}
}
