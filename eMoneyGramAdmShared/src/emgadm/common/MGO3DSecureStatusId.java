package emgadm.common;

public enum MGO3DSecureStatusId {

	ACCEPT(new int[]{600, 800}, "3D_SECURE_ACCEPT","Accept"),
	REJECT(new int[]{60, 180, 230}, "3D_SECURE_REJECT","Reject"),
	TIMEOUT(new int[]{172, 175, 220}, "3D_SECURE_TIME_OUT","Timeout");
	
	private int[] codes;
    private String status;
    private String statusMessage;
	
	private MGO3DSecureStatusId(int[] codes, String status, String statusMessage) {
		this.codes = codes.clone();
		this.status = status;
		this.statusMessage = statusMessage;
	}
	
	public int[] getCodes() {
		return codes;
	}

	public void setCodes(int[] codes) {
		this.codes = codes.clone();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public static MGO3DSecureStatusId fromCode(int code) throws java.lang.IllegalArgumentException {
        for (MGO3DSecureStatusId e : MGO3DSecureStatusId.values()) {
            int[] c = e.getCodes();
            for (int i = 0; i < c.length; i++) {
				if(code == c[i]){
					return e;
				}
			}
        }
        throw new java.lang.IllegalArgumentException();
    }

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}



	
	
}
