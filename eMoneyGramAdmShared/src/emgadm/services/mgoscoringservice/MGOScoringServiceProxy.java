package emgadm.services.mgoscoringservice;

import com.moneygram.service.moneygramscoringservice_v3.DecisionAndScoreRequest;
import com.moneygram.service.moneygramscoringservice_v3.DecisionAndScoreResponse;

public interface MGOScoringServiceProxy {

	public DecisionAndScoreResponse getDecisionAndScore(
			DecisionAndScoreRequest decisionAndScoreRequest) throws Exception;
}