package emgadm.services.mgoscoringservice;

import java.net.URL;

import org.apache.commons.lang.StringUtils;

import com.moneygram.service.moneygramscoringservice_v3.DecisionAndScoreRequest;
import com.moneygram.service.moneygramscoringservice_v3.DecisionAndScoreResponse;
import com.moneygram.service.moneygramscoringservice_v3.MoneygramScoringPortType_v3;
import com.moneygram.service.moneygramscoringservice_v3.MoneygramScoringServiceBinding_v3Stub;

import emgadm.property.EMTAdmContainerProperties;
import emgadm.services.ProxyException;
import emgshared.dataaccessors.EMGSharedLogger;

public class MGOScoringServiceProxyImpl implements MGOScoringServiceProxy {

	private MoneygramScoringServiceBinding_v3Stub mgoScoringServiceClient = null;
	

	private static final int DEFAULT_TIMEOUT = 15000;

	private static final MGOScoringServiceProxy instance = new MGOScoringServiceProxyImpl();

	public static final MGOScoringServiceProxy getInstance() {
		return instance;
	}

	public MGOScoringServiceProxyImpl() {
	}

	// we need to add the client jar class here along with calling the URL from
	// container property.

	private MoneygramScoringPortType_v3 getMGOScoringServiceClient()
			throws ProxyException {
		if (mgoScoringServiceClient != null) {
			return mgoScoringServiceClient;
		} else {
			synchronized (MGOScoringServiceProxyImpl.class) {
				try {
					URL url = new URL(
							EMTAdmContainerProperties.getMGOScoringServiceUrl());
					String timeOut = EMTAdmContainerProperties
							.getMGOScoringServiceTimeout();
					int timeout = DEFAULT_TIMEOUT;
					if (StringUtils.isNumeric(timeOut)) {
						timeout = Integer.parseInt(timeOut);
					} else {
						EMGSharedLogger.getLogger(
								this.getClass().getName().toString()).warn(
								"Invalid timeout value for MGOScoringService: "
										+ timeOut + ". Using default value of "
										+ timeout);
					}
					// here we need to pass the URL from our side to the service
					// to hit it.
					// mgoScoringServiceClient = new moneygramscoringservice_v3
					mgoScoringServiceClient = new MoneygramScoringServiceBinding_v3Stub(url, null);
					
					return mgoScoringServiceClient;
				} catch (Exception e) {
					throw new ProxyException(
							"Failed to create MGOScoringService:",e);
				}
			}
		}
	}

	public DecisionAndScoreResponse getDecisionAndScore(
			DecisionAndScoreRequest decisionAndScoreRequest) throws Exception {
		// TODO Auto-generated method stub
		DecisionAndScoreResponse response = new DecisionAndScoreResponse();
		response = getMGOScoringServiceClient().getDecisionAndScore(
				decisionAndScoreRequest);

		return response;
	}

}
