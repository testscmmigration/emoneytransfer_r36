package emgadm.services.emailage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import emgshared.dataaccessors.EMGSharedLogger;



public class EmailAgeTest {
  
	private static Logger logger = EMGSharedLogger.getLogger(EmailAgeTest.class);

		/**
		 * @param args
		 */
		public static void emailTest(String[] args) {
			

			String email = "test2158@mail.com";//"andyd2mgo19@mailinator.com"; //Ex: a@yahoo.com
			String accountSId = "o";//"A8173D42EC894A21A54557D076A9D18F"; //Get it from Settings->API Key Info
			String authToken = "AD2CB9CB666B4182B74285B1D0EC45D9"; //Get it from Settings->API Key Info
			String resultFormat = "xml";
			String requestBaseUrl ="https://sandbox.emailage.com/EmailAgeValidator/";
			String user_email = null;  //used to force department level rules.  This would be an MGI user email address
									   //for example, MGO may have one like mgoonline@moneygram.com & kiosk may have mgokiosk@moneygram.com
			                           //if not passed, EMailAge will use company level rules
			String ipAddress = "66.87.71.185";
			
			try {
				String validResult = validEmail(email
				,ipAddress
				,accountSId
				,authToken
				,resultFormat
				,requestBaseUrl
				,user_email);
				System.out.println(validResult);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
		
		public static String validEmail(String email, String ipAddress, String accountSId,
				String authToken, String resultFormat, String requestBaseUrl,
				String user_email) throws IOException {
			String oriUrl = requestBaseUrl + "?format=" + resultFormat;
			if (user_email != null && user_email.trim().length() > 0) {
				oriUrl = oriUrl + "&user_email=" + user_email;
			}
			
			oriUrl = oriUrl + "&firstname=" + "Andy";
			oriUrl = oriUrl + "&lastname=" + "Pavlish";
			oriUrl = oriUrl + "&billcity=" + "Minneapolis";
			//only pass for USA:
			    oriUrl = oriUrl + "&billregion=" + "MN";
			oriUrl = oriUrl + "&billcountry=" + "USA";
			oriUrl = oriUrl + "&phone=" + "7634289043";

			
			String requestUrl = OAuth.getUrl("POST", null, oriUrl, accountSId,
					authToken);

			URL url = new URL(requestUrl);
			URLConnection conn = url.openConnection();
			conn.setReadTimeout(5000); //in milliseconds
			conn.setRequestProperty("Content-Language", "en-US");
			conn.setRequestProperty("Charset", "utf-8");
			conn.setDoOutput(true);
			OutputStreamWriter writer = new OutputStreamWriter(
					conn.getOutputStream(), "utf-8");

			String query;
			if (ipAddress == null || ipAddress.length() > 15) {
				//ipv6 not supported & results will be incorrect
				query = email;
			} else {
				query = email + "+" + ipAddress;
			}
			// write parameters
			writer.write(query);
			writer.flush();

			// Get the response
			StringBuilder answer = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream(), "utf-8"));

			String line;
			while ((line = reader.readLine()) != null) {
				answer.append(line);
			}
		String	answer1 = answer.toString();
		if(answer1.contains("EAScore")){
			String score = answer1.substring(	answer1.indexOf("<EAScore>")+9, answer1.indexOf("</EAScore>"));
		}
		else{
			System.out.println("Score is Null");
		}
			String errorCode = answer1.substring(answer1.indexOf("<errorCode>")+11, answer1.indexOf("</errorCode>"));
			String errDesc = answer1.substring(answer1.indexOf("<description>")+13, answer1.indexOf("</description>"));
			writer.close();
			reader.close();
		//System.out.println("EmailScore IS:" + score);
		System.out.println("Error Code IS:" + errorCode);
		System.out.println("Error Description IS:" + errDesc);
		
		String date = "01/09/2015";
		String date2 ="01/10/2015";
		 DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date date1 = (Date) format.parse(date);
			Date date3 = (Date) format.parse(date2);
			if(date1.after(date3)){
				System.out.println("New Profile");
			}
			else
			System.out.println("Old Profile");
			
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
			return answer.toString();
		}


	}
	
	

