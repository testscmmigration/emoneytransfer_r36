package emgadm.services.emailage;

import emgshared.model.ConsumerProfile;

public interface EmailAgeProxy {
	
	public  String getEmailAgeScore(ConsumerProfile consumerProfile,String ipAddress) throws Exception;
	

}
