package emgadm.services.emailage;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;
import org.springframework.beans.propertyeditors.PatternEditor;

import com.cybersource.security.util.Base64;// need to chk if import is right

import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.property.EMTSharedContainerProperties;

import emgadm.services.TransactionServiceGlobalCollect;

 public class OAuth {
	@SuppressWarnings("unused")
	private static Random _Random = new Random();
	private static String _UnreservedChars = EMTSharedContainerProperties.getUnReservedCharacters();
	public final static String HMACSHA1 = "HMAC-SHA1";
	private static Logger LOGGER = EMGSharedLogger.getLogger(TransactionServiceGlobalCollect.class);
	
	public static String getUrl(String method, String hashAlgorithm,
			String url, String consumerKey, String consumerSecret) {
		if (method == null)
			method = "GET";
		if (hashAlgorithm == null)
			hashAlgorithm = HMACSHA1;
		double nowDates = (new Date()).getTime(); // from 1970.1.1

		String timestamp = String.valueOf(Math.floor(nowDates));
		String nonce = GetRandomString(10);

		int index = url.indexOf('?');
		String querystring;
		if (index == -1) {
			querystring = "";
		} else {
			querystring = url.substring(index + 1);
			url = url.substring(0, index);
		}

		String[] splString = querystring.split("&");
		Map<String, String> query = new HashMap<String, String>();
		for (int i = 0; i < splString.length; i++) {
			int in = splString[i].indexOf("=");
			if (in == -1)
				query.put(splString[i], null);
			else {
				String key = splString[i].substring(0, in);
				String value = splString[i].substring(in + 1);
				query.put(key, value);
			}
		}

		// query.put("oauth_version", "1.0");
		// query.put("oauth_timestamp", timestamp);
		// query.put("oauth_signature_method", "HMAC-SHA1");
		// query.put("oauth_nonce", nonce);
		// query.put("oauth_consumer_key", consumerKey);
		StringBuilder sb = new StringBuilder();
		for (Object key : query.keySet()) {
			sb.append(((String) key).concat(query.get(key) == null ? "" : "="
					+ query.get(key))
					+ "&");
		}
		sb.append("oauth_consumer_key=" + consumerKey + "&");
		sb.append("oauth_nonce=" + nonce + "&");
		sb.append("oauth_signature_method=" + "HMAC-SHA1" + "&");
		sb.append("oauth_timestamp=" + timestamp + "&");
		sb.append("oauth_version=" + 1.0);
		querystring = sb.toString();

		String data = "";
		try {
			data = method.toUpperCase().concat("&")
					.concat(URLEncoder.encode(url, "utf-8")).concat("&")
					.concat(URLEncoder.encode(querystring, "utf-8"));
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			LOGGER.error(e1.getMessage(),e1);
		}

		String sig = "";
		try {
			sig = URLEncoder.encode(
					new String(Base64.encode(HmacSHA1Encrypt(data,
							consumerSecret + "&")), "ASCII"), "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			LOGGER.error(e.getMessage(),e);
		} catch (RuntimeException e) {
			// TODO Auto-generated catch block
			LOGGER.error(e.getMessage(),e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error(e.getMessage(),e);
		}
		return url.concat("?").concat(querystring).concat("&oauth_signature=")
				.concat(sig);
	}

	private static String GetRandomString(int length) {
		StringBuilder result = new StringBuilder();
		SecureRandom random;
		try {
			random = SecureRandom.getInstance("SHA1PRNG");
			boolean specialChar = validateUnreservedChar(_UnreservedChars);
			if (specialChar) {

				for (int i = 0; i < length; i++)
					// System.out.println(random.nextInt(100));
					result.append(_UnreservedChars.charAt(random.nextInt(25)));
			}
		} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				LOGGER.error(e.getMessage(),e);
		}     catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error occured while retreving the unReservedChar "+e.getMessage(), e);   
		}
		return result.toString();
	}

	// veracode fix 8137 "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~";
	
		public static boolean validateUnreservedChar(String specialCharacter) {
		Matcher matcher = null;
		Pattern pattern = Pattern.compile("[a-zA-Z0-9-_.~]*");
		if (null != specialCharacter) {
			matcher = pattern.matcher(specialCharacter);
		}
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}
	}

	
	public static byte[] HmacSHA1Encrypt(String encryptText, String encryptKey)
			throws Exception {
		Mac mac = null;
		String hmacsha1Key = EMTSharedContainerProperties.getHmacsha1Key();
		boolean hmchshaKey = validateKey(hmacsha1Key);
		try {
			if (hmchshaKey) {
				byte[] keybytes = encryptKey.getBytes("UTF-8");
				mac = Mac.getInstance(hmacsha1Key);
				Key key = new SecretKeySpec(keybytes, hmacsha1Key);
				mac.init(key);
			}
		} catch (Exception e) {
			LOGGER.error("Error occured while retreving the HMAC-SHA key "+e.getMessage(), e);
		}

		byte[] text = encryptText.getBytes("UTF-8");
		return mac.doFinal(text);
	}
	
	public static boolean validateKey(String key) {
		Matcher matcher = null;
		Pattern pattern = Pattern.compile("[a-zA-Z0-9-]*");
		if (null != key) {
			matcher = pattern.matcher(key);
		}
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}
	}
		
}
