package emgadm.services.emailage;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;



import emgadm.property.EMTAdmContainerProperties;
import emgadm.util.PhoneConversionUtil;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.model.ConsumerProfile;
import emgshared.util.Constants;
import emgadm.services.customvalidation.CustomValidator;

public class EmailAgeProxyImpl implements EmailAgeProxy {

	//MBO-2896 - Code refactoring
	private static final Logger logr = EMGSharedLogger.getLogger("EmailAgeProxyImpl");
	//ended
	/**
	 * @param args
	 */
	public String getEmailAgeScore(ConsumerProfile consumerProfile,
			String ipAddress) throws Exception {

		String validResult = null;
		String email = consumerProfile.getUserId();
		logr.debug("getEmailAgeScore: email passed is - "+email);
		logr.debug("for consumer Id :::"+consumerProfile.getId());
		String accountSId = EMTAdmContainerProperties.getEmailAgeAccountSID();
		String authToken = EMTAdmContainerProperties.getEmailAgeAuthToken();
		String resultFormat = Constants.RESULT_FORMAT;
		String requestBaseUrl = EMTAdmContainerProperties.getEmailAgeURL();
		String user_email = null;
		validResult = validateEmail(email, ipAddress, accountSId, authToken,
				resultFormat, requestBaseUrl, user_email, consumerProfile);
		
		return validResult;
	}

	public String validateEmail(String email, String ipAddress,
			String accountSId, String authToken, String resultFormat,
			String requestBaseUrl, String user_email,
			ConsumerProfile consumerProfile) throws Exception {
		String oriUrl = requestBaseUrl + "?format=" + resultFormat;
		String customerParms = null;
		if (user_email != null && user_email.trim().length() > 0) {
			customerParms = customerParms + "&user_email=" + user_email;
			
		}
		logr.info("locale passed for conversion:::"+consumerProfile.getPreferedLanguage());
// Commenting fname, lname and city for fixing Defect MBO - 2898
		customerParms = customerParms + "&firstname=" + encodeParam(consumerProfile.getFirstName());
		customerParms = customerParms + "&lastname=" + encodeParam(consumerProfile.getLastName());
		customerParms = customerParms + "&billcity=" + encodeParam(consumerProfile.getCity());
		if("USA".equals(consumerProfile.getIsoCountryCode())){
			customerParms = customerParms + "&billregion=" +consumerProfile.getState() ;
		}
		customerParms = customerParms + "&billcountry=" +consumerProfile.getIsoCountryCode();
		// Commenting phone as spaces in phone is causing the emailage to not work
		//customerParms = customerParms + "&phone=" + consumerProfile.getPhoneNumber();
		String phoneNumberWithDiallingCode = (new StringBuilder(
				consumerProfile.getPhoneDialingCode()).append(consumerProfile
				.getPhoneNumber())).toString();
		String formattedPhoneNumber = PhoneConversionUtil.formatPhoneNumber(
				phoneNumberWithDiallingCode,
				consumerProfile.getPreferedLanguage());
		if (null != formattedPhoneNumber) {
			if (isPhoneNumberValid(formattedPhoneNumber))
				logr.debug("formatted Phone Number" + formattedPhoneNumber);
		}
		customerParms = customerParms + "&phone=" + formattedPhoneNumber;
        //Added encoder to replace the space in url with (%20,%26.etc.) for MBO-3823
				
		oriUrl = oriUrl + customerParms;
		String requestUrl = OAuth.getUrl("POST", null, oriUrl, accountSId,
				authToken);
		if (null != requestUrl) {
			if (isURLValid(requestUrl))
				logr.debug("URL for email age being passed as " + requestUrl);
		}
		URL url = new URL(requestUrl);
		URLConnection conn = url.openConnection();
		conn.setReadTimeout(Integer.parseInt(EMTAdmContainerProperties
				.getEmailAgeServiceTimeout())); // in milliseconds
		conn.setRequestProperty("Content-Language", "en-US");
		conn.setRequestProperty("Charset", "utf-8");
		conn.setDoOutput(true);
		OutputStreamWriter writer = new OutputStreamWriter(
				conn.getOutputStream(), "utf-8");

		String query;
		if (ipAddress == null || ipAddress.length() > 15) {
			// ipv6 not supported & results will be incorrect
			query = email;
		} else {
			query = email + "+" + ipAddress;
		}
		// write parameters
		writer.write(query);
		writer.flush();

		// Get the response
		StringBuilder answer = new StringBuilder();
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				conn.getInputStream(), "utf-8"));

		String line;
		while ((line = reader.readLine()) != null) {
			answer.append(line);
		}
		writer.close();
		reader.close();
		
		return answer.toString();
	}
	
	//MBO-2896
	public static Map<String,Object> processResult(ConsumerProfile consumerProfile,String result) throws Exception {
		Map<String, Object> resultMap = new HashMap<String,Object>();
		String score = null;		
		String formatedFirstSeenDate=null;
		String age="";
		
		//added for 3819
		logr.info("Result value in processResult is " + ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTMLAttribute(result)));
		String nameMatchCode = null;
		//ended
		
		if (result.contains("EAScore")) {
			score = generateScore(result);
			logr.debug("Email Age Scoring done successfully - "
					+ consumerProfile.getId()+ " -Score is- " + ESAPI.encoder().encodeForHTMLAttribute(score));
		}
		// MBO-2587 Email first seen date change
		if (result.contains("emailAge")) {
			age = generateEmailAge(result);
			if (age.isEmpty()) {
				if (result.contains("firstVerificationDate")) {					
					formatedFirstSeenDate = generateFormattedFirstSeenDate(result);
					logr.debug("Email first verification date received successfully"
							+ formatedFirstSeenDate
							+ " "
							+ consumerProfile.getId());
				}
				
			}
			else {
				Calendar dateFirstSeen = Calendar.getInstance();
				dateFirstSeen.setTime((new SimpleDateFormat(
						"yyyy-MM-dd'T'HH:mm:ss")).parse(age));
				formatedFirstSeenDate = (new SimpleDateFormat(
						" yyyy-MM-dd HH:mm:SS")).format(dateFirstSeen
						.getTime());
				logr.debug("Email age date received successfully"
						+ formatedFirstSeenDate + " "
						+ consumerProfile.getId());					
			}
		}
		
		//added for 3819
		logr.info("Going to check namematch tag in EmailAgeproxyimpl");
		if (result.contains("namematch")) {			
			nameMatchCode = generateNameMatch(result);
			logr.info("name match code in email proxy impl is " + ESAPI.encoder().encodeForHTMLAttribute(nameMatchCode));
		}
		//ended
		resultMap.put("score",score);
		resultMap.put("age",age);
		resultMap.put("formatedFirstSeenDate",formatedFirstSeenDate);
		resultMap.put("nameMatchCode", nameMatchCode);
		generateErrorCode(resultMap,result);		
		return resultMap;
	}
	
	private static String generateScore(String result){
		return result.substring(result.indexOf("<EAScore>") + 9,
				result.indexOf("</EAScore>"));
	}
	
	private static String generateEmailAge(String result){
		return result.substring(result.indexOf("<emailAge>") + 10,
				result.indexOf("</emailAge>"));
	}
	
	private static String generateFormattedFirstSeenDate(String result) throws Exception {
		String fistSeenDate = result.substring(
				result.indexOf("<firstVerificationDate>") + 23,
				result.indexOf("</firstVerificationDate>"));
		Calendar dateFirstSeen = Calendar.getInstance();
		dateFirstSeen.setTime((new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss")).parse(fistSeenDate));
		String formatedFirstSeenDate = (new SimpleDateFormat(
				" yyyy-MM-dd HH:mm:SS")).format(dateFirstSeen
				.getTime());
		return formatedFirstSeenDate;
	}
	
	private static Map<String,Object> generateErrorCode (Map<String,Object> resultMap,String result) throws Exception {
		String errorCode = result.substring(
				result.indexOf("<errorCode>") + 11, result.indexOf("</errorCode>"));
		
		String errDesc = result.substring(result.indexOf("<description>") + 13,
						result.indexOf("</description>"));
		
		resultMap.put("errorCode",errorCode);
		resultMap.put("errorDesc", errDesc);
		return resultMap;
	}
	
	//added for 3819
	private static String generateNameMatch (String result) throws Exception {
		return result.substring(
				result.indexOf("<namematch>") + 11,
				result.indexOf("</namematch>"));
		
	}
	
	public String encodeParam(String param) throws Exception {
		String encodeValue = null;
		if(null!=param && !param.isEmpty()) {
			encodeValue = URLEncoder.encode(param,"UTF-8");
		}
		return encodeValue;
	}
	//ended
	//ended

	
	private static boolean isPhoneNumberValid(String phoneNumber) {

		Matcher matcher = null;
		Pattern pattern = Pattern.compile("[0-9]*");
		if (null != phoneNumber) {
			matcher = pattern.matcher(phoneNumber);
		}
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}

	}
	
	private static boolean isURLValid(String URL) {
		Matcher matcher = null;
		Pattern pattern = Pattern.compile("[a-zA-Z0-9-?./_&=$]*");
		if (null != URL) {
			matcher = pattern.matcher(URL);
		}
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}
	}

}
