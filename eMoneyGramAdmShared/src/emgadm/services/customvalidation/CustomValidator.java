package emgadm.services.customvalidation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomValidator {
	private static final String MOBILE = "M";
	private static final String HOME = "H";
	
	public static boolean isUnreservedCharValid(String specialCharacter) {
		Matcher matcher = null;
		Pattern pattern = Pattern.compile("[a-zA-Z0-9-_.~]*");
		if (null != specialCharacter) {
			matcher = pattern.matcher(specialCharacter);
		}
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isKeyValid(String key) {
		Matcher matcher = null;
		Pattern pattern = Pattern.compile("[a-zA-Z0-9-]*");
		if (null != key) {
			matcher = pattern.matcher(key);
		}
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}
	}
	
	
	public boolean isThisDateValid(String dateToValidate, String dateFromat){

		if(dateToValidate == null){
			return false;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);

		try {

			//if not valid, it will throw ParseException
			Date date = sdf.parse(dateToValidate);
			System.out.println(date);

		} catch (ParseException e) {

			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	public boolean isPhoneNumberValid(String phoneNumber){

		Matcher matcher = null;
		Pattern pattern = Pattern.compile("[0-9]*");
		if (null != phoneNumber) {
			matcher = pattern.matcher(phoneNumber);
		}
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}
	
	}

	public boolean isURLValid(String URL) {
		Matcher matcher = null;
		Pattern pattern = Pattern.compile("[a-zA-Z0-9-?./_&=$]*");
		if (null != URL) {
			matcher = pattern.matcher(URL);
		}
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}

	}
	
	public boolean isPhoneTypeValid(String phoneType) {
		if (phoneType.equals(MOBILE) || phoneType.equals(HOME)) {
			return true;
		} else {
			return false;
		}
	}
	
	//MBO-9993
	private static boolean isAlphaBlockerValid(String name){
		Matcher matcher = null;
//		Pattern pattern = Pattern.compile("[!-~ ]*"); 
		Pattern pattern = Pattern.compile("[0-9a-zA-Z-_ ]*"); 
		if (null != name) {
			matcher = pattern.matcher(name);
		}
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}

	}
	
//	public static void main(String args[]){
//		String name = " - ";
//		if(isAlphaBlockerValid(name)){
//			System.out.println("=================");
//		}
//		else {
//			System.out.println("false");
//		}
//	}
}
