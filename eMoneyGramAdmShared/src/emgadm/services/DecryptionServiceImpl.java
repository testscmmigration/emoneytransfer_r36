/*
 * Created on Feb 7, 2005
 *
 */
package emgadm.services;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.spec.PKCS8EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

import emgadm.property.EMTAdmContainerProperties;
import emgshared.exceptions.EMGRuntimeException;

/**
 * @author A131
 *
 */
public class DecryptionServiceImpl implements DecryptionService
{
	private static final DecryptionService instance =
		new DecryptionServiceImpl();

	public static final DecryptionService getInstance() {
		return instance;
	}

	private DecryptionServiceImpl() {
		// Add provider
		Security.addProvider(new BouncyCastleProvider());
	}

	public String decryptSSN(String encryptedSsn) {
		return decryptRSA(encryptedSsn, true);
	}

	public String decryptCreditCardNumber(String encryptedCreditCardNumber) {
		return decryptRSA(encryptedCreditCardNumber, true);
	}

	public String decryptCreditCardBinNumber(String encryptedCreditCardBinNumber) {
		return decryptRSA(encryptedCreditCardBinNumber, true);
	}

	public String decryptBankAccountNumber(String encryptedBankAccountNumber) {
		return decryptRSA(encryptedBankAccountNumber, true);
	}

	public String decryptAdditionalId(String encryptedAdditionalId) {
	    return decryptRSA(encryptedAdditionalId, false);
	}


	private String decryptRSA(String encryptedValue, boolean padding) {

		String cypherOptions = "RSA/ECB/PKCS1Padding";
        final String BouncyCastle ="BC";
		if(!padding) {
			cypherOptions = "RSA/None/NoPadding";
		}

		//TODO give more thought to exception handling.
		try {
			FileInputStream fisKey =
				new FileInputStream(EMTAdmContainerProperties.getPrivateKeyFile());

			byte[] encKey = new byte[fisKey.available()];
			fisKey.read(encKey);
			fisKey.close();

			PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(encKey);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA", BouncyCastle);
			PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);

			Cipher cp = null;
			cp = Cipher.getInstance(cypherOptions, BouncyCastle);
			cp.init(Cipher.DECRYPT_MODE, privateKey);

			byte[] encryptedText = Base64.decode(encryptedValue);

			return new String(cp.doFinal(encryptedText));
		} catch (FileNotFoundException e) {
			// No Such File Exception
			throw new EMGRuntimeException(e);
		} catch (IOException e) {
			// IO Exception
			throw new EMGRuntimeException(e);
		} catch (NoSuchAlgorithmException e) {
			// RSA cipher not found in bouncycastle provider
			throw new EMGRuntimeException(e);
		} catch (NoSuchProviderException e) {
			// Provider bouncycastle not found
			throw new EMGRuntimeException(e);
		} catch (NoSuchPaddingException e) {
			// Invalid padding
			throw new EMGRuntimeException(e);
		} catch (Exception e) {
			// General Exception
			throw new EMGRuntimeException(e);
		}
	}
}
