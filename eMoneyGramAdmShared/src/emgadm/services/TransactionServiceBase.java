package emgadm.services;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;

import shared.mgo.dto.MGOAgentProfile;
import shared.mgo.dto.MGOProduct;
import shared.mgo.dto.MGOProductFieldInfo;
import shared.mgo.services.AbstractAgentConnectAccessor;
import shared.mgo.services.AgentConnectAccessor;
import shared.mgo.services.AgentConnectService;
import shared.mgo.services.MGOAgentProfileService;
import shared.mgo.services.MGOServiceFactory;

import com.moneygram.acclient.BillPaymentSendRequest;
import com.moneygram.acclient.LegalIdType;
import com.moneygram.acclient.MoneyGramSendRequest;
import com.moneygram.acclient.PhotoIdType;
import com.moneygram.acclient.ProductVariant;
import com.moneygram.agentconnect1305.wsclient.BillerInfo;
import com.moneygram.agentconnect1305.wsclient.BpValidationRequest;
import com.moneygram.agentconnect1305.wsclient.BpValidationResponse;
import com.moneygram.agentconnect1305.wsclient.CommitTransactionRequest;
import com.moneygram.agentconnect1305.wsclient.CommitTransactionResponse;
import com.moneygram.agentconnect1305.wsclient.Error;
import com.moneygram.agentconnect1305.wsclient.SendReversalResponse;
import com.moneygram.agentconnect1305.wsclient.SendReversalType;
import com.moneygram.agentconnect1305.wsclient.TextTranslation;
import com.moneygram.common_v1.Header;
import com.moneygram.common_v1.ProcessingInstruction;
import com.moneygram.mgo.service.config_v3_4.GetSourceSiteInfoResponse;
import com.moneygram.mgo.service.config_v3_4.TransactionTypeInfo;
import com.moneygram.service.moneygramscoringservice_v3.Action;

import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.dataaccessors.ManagerFactory;
import emgadm.dataaccessors.TransactionManager;
import emgadm.dataaccessors.UserProfileManager;
import emgadm.exceptions.BankPaymentServiceException;
import emgadm.exceptions.CreditCardRefundException;
import emgadm.exceptions.InvalidRRNException;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.model.CustAddress;
import emgadm.model.DeadTransaction;
import emgadm.model.GlAccount;
import emgadm.model.Notification;
import emgadm.model.TranAction;
import emgadm.model.TranComment;
import emgadm.model.UserProfile;
import emgadm.model.UserQueryCriterion;
import emgadm.property.EMTAdmAppProperties;
import emgadm.property.EMTAdmContainerProperties;
import emgadm.services.bankpayment.AdjustmentInquiryRequestMessage;
import emgadm.services.bankpayment.AdjustmentInquiryResponseMessage;
import emgadm.services.bankpayment.BankPaymentProxy;
import emgadm.services.bankpayment.BankPaymentProxyImpl;
import emgadm.services.bankpayment.StatusInquiryRequestMessage;
import emgadm.services.bankpayment.StatusInquiryResponseMessage;
import emgadm.services.mgoscoringservice.MGOScoringServiceProxyImpl;
import emgadm.services.osl.OSLException;
import emgadm.services.osl.OSLProxyImpl;
import emgadm.services.processing.UpdateTransactionCommand;
import emgadm.sysmon.EventListConstant;
import emgadm.util.MerchantIdHelper;
import emgadm.util.StringHelper;
import emgshared.cache.locator.EMTSharedSourceSiteCacheService;
import emgshared.cache.locator.EMTSharedSourceSiteCacheServiceImpl;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.AgentConnectException;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.AdminMessage;
import emgshared.model.BillerLimit;
import emgshared.model.ConsumerAccount;
import emgshared.model.ConsumerAccountType;
import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileSearchCriteria;
import emgshared.model.DeliveryOption;
import emgshared.model.Transaction;
import emgshared.model.TransactionStatus;
import emgshared.model.TransactionType;
import emgshared.property.EMTSharedContainerProperties;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.CommentService;
import emgshared.services.ConsumerAccountService;
import emgshared.services.ConsumerProfileService;
import emgshared.services.MessageService;
import emgshared.services.PCIService;
import emgshared.util.Constants;
import emgshared.util.DateFormatter;
import emgshared.util.I18NHelper;
import emgshared.util.MailHelper;
import eventmon.eventmonitor.EventMonitor;

abstract class TransactionServiceBase implements TransactionService {

	protected static final TransactionManager tranManager = ManagerFactory
			.createTransactionManager();
	protected static final MailService mailService = ServiceFactory
			.getInstance().getMailService();
	protected static final ConsumerAccountService accountService = emgshared.services.ServiceFactory
			.getInstance().getConsumerAccountService();
	protected static final ConsumerProfileService profileService = emgshared.services.ServiceFactory
			.getInstance().getConsumerProfileService();
	protected static final PCIService pciService = emgshared.services.ServiceFactory
			.getInstance().getPCIService();
	protected static final DecryptionService decryptService = emgadm.services.ServiceFactory
			.getInstance().getDecryptionService();
	protected static final MessageService messageService = emgshared.services.ServiceFactory
			.getInstance().getMessageService();
	protected static final Set transactionsInProcess = new HashSet();
	protected static final String resourceFile = "emgadm.resources.ApplicationResources";
	// private static final String ccAuthConfReasonCode = "28";
	protected static final String ccFeeConfReasonCode = "131";
	protected static final String ccFundedConfReasonCode = "140";

	protected static final int PAYMENT_STATUS_PENDING = 600;
	protected static final int PAYMENT_STATUS_READY = 800;
	protected static final int PAYMENT_STATUS_PROCESSED = 900;

	protected static final String DECISION_ACCEPT = "accept";
	protected static final String DECISION_REJECT = "reject";
	// https://mgonline.atlassian.net/browse/AUS-56- GB Group Display- AUS
	//return transaction object based ConfigServiceSourceSite - CyberSource.getSourceSite->countryTransactionTypeInfo->paymentInfo->providerName
	protected static final String GLOBAL_COLLECT = "Global Collect";
	protected static final String CYBER_SOURCE = "Cybersource";

	// added for MBO-1050
	private static final String bankFundingAccType = "BANK-CHK";
	// ended

	private static final Logger log = EMGSharedLogger
			.getLogger(TransactionServiceBase.class);

	protected EMTSharedDynProperties eadp = new EMTSharedDynProperties();

	public static TransactionService getInstance(String partnerSiteId) {

		String paymentProvider = null;
		if (null != partnerSiteId && !partnerSiteId.equalsIgnoreCase("")){
			paymentProvider = getPaymentProvider(partnerSiteId);
			log.info("paymentProvider:" + paymentProvider);
		}
		log.info("partnerSiteId:"+partnerSiteId);
		log.info("paymentProvider:"+paymentProvider);
		if (EMTAdmContainerProperties.getUseGCForProcessing()
				|| (null != paymentProvider && paymentProvider
						.equalsIgnoreCase(GLOBAL_COLLECT))) {
			log.info("TransactionServiceBase-->getInstance()-->returning GlobalCollect");
		    log.info("TransactionServiceBase-->getInstance()-->returning GlobalCollect->UseGCForProcessing");
			return TransactionServiceGlobalCollect.getInstance();
		} 
		else if (partnerSiteId!=null && null!= paymentProvider && paymentProvider .equalsIgnoreCase(GLOBAL_COLLECT)) {
			log.info("TransactionServiceBase-->getInstance()-->returning GlobalCollect");
	
			return TransactionServiceGlobalCollect.getInstance();
		} else if((null != paymentProvider && paymentProvider
						.equalsIgnoreCase(CYBER_SOURCE))){
			log.info("TransactionServiceBase-->getInstance()-->returning cybersource");

			return TransactionServiceCyberSource.getInstance();
		}
		else
		{
			// if config service is not return either Global Collect or Cyber Source, will return null;
			// if config service doen't returns value it falls to null
			return TransactionServiceCyberSource.getInstance();
		}
			
	}

	/**
	 * This method will replace the getInstance method. This method will return
	 * the instance of cybersource or global collect transaction according the
	 * container property USE_GC_for_processig
	 * 
	 * @param partnerSiteId
	 * @return
	 */
	public static TransactionService getInstanceGlobalCollectFlag() {

		boolean useGlobalCollecforProcessing = EMTAdmContainerProperties
				.getUseGCForProcessing();
		if (useGlobalCollecforProcessing) {
			return TransactionServiceGlobalCollect.getInstance();
		} else {
			return TransactionServiceCyberSource.getInstance();
		}
	}

	/**
	 * This method will replace the getInstance method. This method will return
	 * the instance of cybersource or global collect transaction according the
	 * container property USE_GC_for_processig
	 * 
	 * @param partnerSiteId
	 * @return
	 */
	public static TransactionService getInstanceTmp(Integer pymtSrvProvCode,
			String partnerSiteId) {

		if (pymtSrvProvCode != null) {
			if (partnerSiteId
					.equals(EMoneyGramAdmApplicationConstants.MGOUK_PARTNER_SITE_ID)
					|| partnerSiteId
							.equals(EMoneyGramAdmApplicationConstants.MGODE_PARTNER_SITE_ID)
					|| pymtSrvProvCode == EMoneyGramAdmApplicationConstants.PYMT_PROVIDER_CODE_GLOBAL_COLLECT) {
				return TransactionServiceGlobalCollect.getInstance();
			} else {
				return TransactionServiceCyberSource.getInstance();
			}
		} else {
			return TransactionServiceCyberSource.getInstance();
		}
	}

	private static boolean isSameDay(Calendar a, Calendar b) {
		return (a.get(Calendar.YEAR) == b.get(Calendar.YEAR)
				&& a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a
				.get(Calendar.DAY_OF_MONTH) == b.get(Calendar.DAY_OF_MONTH));
	}

	protected TransactionServiceBase() {
		super();
	}

	public void requestOwnership(final int tranId, final String userId)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				TransactionManager manager = ManagerFactory
						.createTransactionManager();
				manager.setTransactionOwnership(tranId, userId);
				return null;
			}
		};
		updateTransaction(tranId, userId, command, true, false);
	}

	public void takeOverOwnership(final int tranId, final String userId)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				TransactionManager manager = ManagerFactory
						.createTransactionManager();
				manager.setTransactionOwnership(tranId, userId);
				return null;
			}
		};
		updateTransaction(tranId, userId, command, false, false);
	}

	public void releaseOwnership(final int tranId, final String userId)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				TransactionManager manager = ManagerFactory
						.createTransactionManager();
				manager.setTransactionOwnership(tranId, null);
				return null;
			}
		};
		updateTransaction(tranId, userId, command);
	}

	public void setTransactionStatus(final int tranId, final String tranStatus,
			final String fundStatus, final String userId)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				TransactionManager manager = ManagerFactory
						.createTransactionManager();
				manager.setTransactionStatus(tranId, tranStatus, fundStatus,
						userId);
				return null;
			}
		};
		updateTransaction(tranId, userId, command, true, true);
	}

	public void recordLossAdjustment(final int tranId, final float amount,
			final String userId) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		recordManualAdjustment(tranId, amount,
				TransactionStatus.MANUAL_RECORD_LOSS_REVENUE, userId);
	}

	public void recordFeeAdjustment(final int tranId, final float amount,
			final String userId) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		recordManualAdjustment(tranId, amount,
				TransactionStatus.MANUAL_RECORD_FEE_REVENUE, userId);
	}

	public void recordConsumerRefundOrRemittanceAdjustment(final int tranId,
			final float amount, final String userId)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		recordManualAdjustment(tranId, amount,
				TransactionStatus.MANUAL_RECORD_CONSUMER_REFUND_OR_REMITTANCE,
				userId);
	}

	public void recordMgActivityAdjustment(final int tranId,
			final float amount, final String userId)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		recordManualAdjustment(tranId, amount,
				TransactionStatus.MANUAL_RECORD_MG_ACTIVITY, userId);
	}

	private void recordManualAdjustment(final int tranId, final float amount,
			final TransactionStatus transientStatus, final String userId)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				Transaction tran = tranManager.getTransaction(tranId);
				if (tran == null) {
					throw new IllegalArgumentException(
							"unable to retrieve transaction with id = "
									+ tranId);
				}
				if (!tran.isManualAdjustable()) {
					throw new IllegalStateException("transaction id = "
							+ tranId + " is not in a manually adjustable state");
				}
				TransactionManager manager = ManagerFactory
						.createTransactionManager();
				// Change the status so the stored procedure code records
				// ledger entries properly. For the moment, we call
				// update transaction as opposed to set transaction status
				// because set transaction status does not accept an amount.
				tran.setStatus(transientStatus);
				manager.updateTransaction(tran, userId, null, new Float(amount));
				// Change the status to generic manual because the
				// preceding status is considered a transient status.
				tran.setStatus(TransactionStatus.MANUAL_MANUAL);
				manager.setTransactionStatus(tran.getEmgTranId(),
						tran.getStatus(), userId);
				return null;
			}
		};
		updateTransaction(tranId, userId, command, true, true);
	}

	public void returnFee(final int tranId, final float amount,
			final String userId) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		moveToManualWithAdjustment(tranId, new Float(amount), userId);
	}

	public void collectFee(final int tranId, final float amount,
			final String userId) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		moveToManualWithAdjustment(tranId, new Float(amount), userId);
	}

	public void returnFunding(final int tranId, final float amount,
			final String userId) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		moveToManualWithAdjustment(tranId, new Float(amount), userId);
	}

	private void moveToManualWithAdjustment(final int tranId,
			final Float amount, final String userId)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				Transaction tran = tranManager.getTransaction(tranId);
				if (tran == null) {
					throw new IllegalArgumentException(
							"unable to retrieve transaction with id = "
									+ tranId);
				}
				TransactionManager manager = ManagerFactory
						.createTransactionManager();
				tran.setStatus(TransactionStatus.MANUAL_MANUAL);
				manager.updateTransaction(tran, userId, null, amount);
				return null;
			}
		};
		updateTransaction(tranId, userId, command, true, true);
	}

	/*
	 * currently the resubmission and the charging of fee is performed manually.
	 * this method is simply getting the accounting entries in sync with what
	 * has been done manually.
	 */
	public void resubmitAch(final int tranId, final Float fee,
			final String userId) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				Transaction tran = tranManager.getTransaction(tranId);
				if (tran == null) {
					throw new IllegalArgumentException(
							"unable to retrieve transaction with id = "
									+ tranId);
				}
				if (fee != null) {
					tran.setTranSubStatCode(TransactionStatus.FEE_CODE);
					tranManager.updateTransaction(tran, userId, null, fee);
				}
				tran.setTranSubStatCode(TransactionStatus.ACH_SENT_CODE);
				tranManager.setTransactionStatus(tran.getEmgTranId(),
						tran.getStatus(), userId);
				return null;
			}
		};
		updateTransaction(tranId, userId, command, true, true);
	}

	public ArrayList approveDelayedSendTransaction(int tranId,
			String callerLoginId, String tranStat, String fundStat,
			String contextRoot) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException, AgentConnectException,
			InvalidRRNException, DataSourceException, SQLException {
		ArrayList<String> errors = new ArrayList<String>();

		// Get funding (CC Capture or ACH Waiting) with status of SEN/CCA
		if ((TransactionStatus.FORM_FREE_SEND_CODE.equals(tranStat)
				|| TransactionStatus.APPROVED_CODE.equals(tranStat) || TransactionStatus.PENDING_CODE
				.equals(tranStat))
				&& TransactionStatus.NOT_FUNDED_CODE.equals(fundStat)) {
			try {
				fundDelayedSends(tranId, callerLoginId, contextRoot);
			} catch (BankPaymentServiceException e) {
				errors.add("error.bankpayment.posttoaccount");
				return errors;
			}
		}
		return null; // all is well.
	}

	protected void sendPersonToPersonSuccessEmail(Transaction tran,
			ConsumerAccountType accountType) throws Exception {
		ConsumerAccount account;
		if (accountType.isCardAccount()) {
			account = accountService.getCreditCardAccount(
					tran.getSndCustAcctId(), tran.getSndCustLogonId());
		} else {
			account = accountService.getBankAccount(tran.getSndCustAcctId(),
					tran.getSndCustLogonId());
		}
		if (account != null) {
			ConsumerProfile profile = profileService.getConsumerProfile(
					Integer.valueOf(account.getConsumerId()),
					tran.getSndCustLogonId(), "");
			if (profile != null) {
				String preferedlanguage = profile.getPreferedLanguage();
				ConsumerEmail consumerEmail = null;
				// Loop sends out email to each address.
				NotificationAccess na = new NotificationAccessImpl();
				for (Iterator<ConsumerEmail> iter = profile.getEmails()
						.iterator(); iter.hasNext();) {
					consumerEmail = iter.next();
					 //MBO-5319 - Modified Method signature to use Profile object, will be useful in future if any
					  //value needs to be fetched from Profile
					boolean sendSMS = true;
					na.notifySentTransaction(tran, consumerEmail,
							preferedlanguage,profile, sendSMS);
					//ended 5319
				}
			}
		}
	}

	/**
	 * 
	 * @param request
	 * @param trans
	 */

	public void sendEconomyServicePendingEmail(Transaction tran, String userId)
			throws Exception {
		ConsumerAccount account = accountService.getBankAccount(
				tran.getSndCustAcctId(), tran.getSndCustLogonId());
		if (account != null) {
			ConsumerProfile profile = profileService.getConsumerProfile(
					Integer.valueOf(account.getConsumerId()),
					tran.getSndCustLogonId(), "");
			if (profile != null) {
				String preferedlanguage = profile.getPreferedLanguage();
				ConsumerEmail consumerEmail = null;
				// Loop sends out email to each address.
				NotificationAccess na = new NotificationAccessImpl();
				for (Iterator iter = profile.getEmails().iterator(); iter
						.hasNext();) {
					consumerEmail = (ConsumerEmail) iter.next();
					na.notifyPendingTransaction(tran, consumerEmail,
							preferedlanguage);
				}
			}
			TransactionManager manager = ManagerFactory
					.createTransactionManager();
			manager.setTransactionStatus(tran.getEmgTranId(),
					TransactionStatus.APPROVED_CODE,
					TransactionStatus.ACH_WAITING_CODE, userId);
		}
	}

	public void sendEconomyServiceAchReturnEmail(Transaction tran, String userId)
			throws Exception {
		ConsumerAccount account = accountService.getBankAccount(
				tran.getSndCustAcctId(), tran.getSndCustLogonId());
		if (account != null) {
			ConsumerProfile profile = profileService.getConsumerProfile(
					Integer.valueOf(account.getConsumerId()),
					tran.getSndCustLogonId(), "");
			if (profile != null) {
				String preferedlanguage = profile.getPreferedLanguage();
				ConsumerEmail consumerEmail = null;
				// Loop sends out email to each address.
				NotificationAccess na = new NotificationAccessImpl();
				for (Iterator<ConsumerEmail> iter = profile.getEmails()
						.iterator(); iter.hasNext();) {
					consumerEmail = iter.next();
					na.notifyACHReturnTransaction(tran, consumerEmail,
							preferedlanguage);
				}
			}
			TransactionManager manager = ManagerFactory
					.createTransactionManager();
			manager.setTransactionStatus(tran.getEmgTranId(),
					tran.getTranStatCode(),
					TransactionStatus.ACH_RETURNED_CODE, userId);
			if (TransactionStatus.APPROVED_CODE.equals(tran.getTranStatCode())) {
				manager.setTransactionStatus(tran.getEmgTranId(),
						TransactionStatus.CANCELED_CODE,
						TransactionStatus.ACH_RETURNED_CODE, userId);
			}
		}
	}

	protected void sendCreditCardFailEmail(Transaction tran) throws Exception {
		ConsumerProfile profile = profileService.getConsumerProfile(
				Integer.valueOf(tran.getCustId()), tran.getSndCustLogonId(), "");
		if (profile != null) {
			String preferedlanguage = profile.getPreferedLanguage();
			ConsumerEmail consumerEmail = null;
			// Loop sends out email to each address.
			NotificationAccess na = new NotificationAccessImpl();
			for (Iterator iter = profile.getEmails().iterator(); iter.hasNext();) {
				consumerEmail = (ConsumerEmail) iter.next();
				na.notifyCreditCardFailTransaction(tran, consumerEmail,
						preferedlanguage);
			}
		}
	}

	/**
	 * Calls updateTransaction with verifyOwnership and verifyStrictOwnership
	 * set to true.
	 * 
	 * @param transId
	 * @param userId
	 * @param command
	 * @return
	 * @throws TransactionAlreadyInProcessException
	 * @throws TransactionOwnershipException
	 * 
	 *             Created on Mar 1, 2005
	 */
	protected Object updateTransaction(int transId, String userId,
			UpdateTransactionCommand command)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		return updateTransaction(transId, userId, command, true, true);
	}

	/**
	 * 
	 * @param tranId
	 * @param userId
	 *            - id of user requesting update
	 * @param command
	 * @param verifyOwnership
	 *            - true implies transaction must not be owned by someone other
	 *            than the user requesting the update, i.e. owner can be empty.
	 * @param verifyStrictOwnership
	 *            - true implies transaction must be owned by user requesting
	 *            udpate. valid only when verifyOwnership is true.
	 * @return
	 * @throws TransactionAlreadyInProcessException
	 * @throws TransactionOwnershipException
	 * 
	 *             Created on Mar 1, 2005
	 */
	protected Object updateTransaction(int tranId, String userId,
			UpdateTransactionCommand command, boolean verifyOwnership,
			boolean verifyStrictOwnership)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		Integer transIdKey = Integer.valueOf(tranId);
		synchronized (transactionsInProcess) {
			if (transactionsInProcess.contains(transIdKey)) {
				String currentOwner;
				try {
					currentOwner = getCurrentOwner(tranId);
				} catch (Exception ex) {
					currentOwner = "unavailable";
					throw new TransactionAlreadyInProcessException(
							"transaction is already in process", currentOwner,
							ex);
				}
				throw new TransactionAlreadyInProcessException(
						"transaction is already in process", currentOwner);
			}
			transactionsInProcess.add(transIdKey);
		}
		try {
			if (verifyOwnership) {
				String currentOwner = getCurrentOwner(tranId);
				boolean owned = StringUtils.isNotBlank(currentOwner);
				boolean ownerMatch = StringUtils.equalsIgnoreCase(userId,
						currentOwner);
				if (verifyStrictOwnership) {
					if (!owned  || !ownerMatch ) {
						throw new TransactionOwnershipException(
								"Transaction is owned by " + currentOwner,
								currentOwner);
					}
				} else {
					if (owned  && !ownerMatch ) {
						throw new TransactionOwnershipException(
								"Transaction is owned by " + currentOwner,
								currentOwner);
					}
				}
			}
			return command.execute();
		} finally {
			synchronized (transactionsInProcess) {
				transactionsInProcess.remove(transIdKey);
			}
		}
	}

	private String getCurrentOwner(int tranId) {
		TransactionManager transactionManager = ManagerFactory
				.createTransactionManager();
		Transaction tran = transactionManager.getTransaction(tranId);
		return (tran == null ? "" : tran.getCsrPrcsUserid());
	}

	protected void acknowledgementFailEmail(int tranId, String refNumber,
			Throwable throwable, String userId) throws Exception {
		UserProfile up = getUserProfile(userId);
		String userName = userId;
		userName = up == null ? userName : up.getFirstName() + " "
				+ up.getLastName() + " (" + userId + ")";
		Locale locale = Locale.getDefault();
		Object[] params = new Object[4];
		String label = "messages.mail.ac.acknowledge.failed.body";
		params[0] = tranId + "";
		params[1] = refNumber;
		params[2] = userName;
		String dump = emgshared.util.StringHelper
				.getExceptionDumpMsg(throwable);
		params[3] = dump;
		String body = I18NHelper.getFormattedMessage(label, params,
				resourceFile, locale);
		String subject = I18NHelper.getFormattedMessage(
				"messages.mail.ac.acknowledge.failed.subject", resourceFile,
				locale);
		// get error code and apend it to the subject line
		if (dump.indexOf("<ac:errorCode>") > 0) {
			int i = dump.indexOf("<ac:errorCode>") + 14;
			int j = dump.indexOf("</ac:errorCode>");
			subject = subject + " (Error Code: " + dump.substring(i, j).trim()
					+ ")";
		}
		mailService.sendTransactionMail(
				EMTSharedContainerProperties.getMailACTo(), subject, body);
	}

	protected boolean sendAgentConnectFailMail(String message,
			Throwable throwable, String userId) {
		UserProfile up = getUserProfile(userId);
		String userName = "";
		if (up != null) {
			userName = up.getFirstName() + " " + up.getLastName() + " ("
					+ userId + ")";
		}
		Locale locale = Locale.getDefault();
		Object[] params = new Object[2];
		String label = "messages.mail.ac.failed.body";
		params[0] = userName;
		String dump = emgshared.util.StringHelper
				.getExceptionDumpMsg(throwable);
		params[1] = message + dump;
		String body = I18NHelper.getFormattedMessage(label, params,
				resourceFile, locale);
		String subject = I18NHelper.getFormattedMessage(
				"messages.mail.ac.failed.subject", resourceFile, locale);
		// get error code and apend it to the subject line
		if (dump.indexOf("<ac:errorCode>") > 0) {
			int i = dump.indexOf("<ac:errorCode>") + 14;
			int j = dump.indexOf("</ac:errorCode>");
			subject = subject + " (Error Code: " + dump.substring(i, j).trim()
					+ ")";
		}
		return MailHelper.sendMail(EMTSharedContainerProperties.getMailHost(),
				EMTSharedContainerProperties.getMailFrom(),
				EMTSharedContainerProperties.getMailACTo(), subject, body);
	}

	protected boolean addMGOGlobalAggErrorToTranComment(Exception e,
			int tranId, String userId) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		String msg = sw.toString();
		try {
			pw.close();
			sw.close();
		} catch (Exception e0) {
		}
		UserProfile up = getUserProfile(userId);
		String userName = userId;
		userName = up == null ? userName : up.getFirstName() + " "
				+ up.getLastName() + " (" + userId + ")";

		StringBuilder out = new StringBuilder();
		Error error = (Error) e.getCause();
		//Added the below string for ease of search in DB
		out.append("GlobalAgg Error:");
		//ended
		out.append(error.getErrorCode());
		out.append("-" + error.getSubErrorCode());
		out.append(" " + error.getErrorString());
		msg = out.toString();
		if (msg.length() > 255) {
			msg = msg.substring(0, 254);
		}
		try {
			tranManager.setTransactionComment(tranId, "OTH", msg, "emgwww");
		} catch (Exception e1) {
			throw new EMGRuntimeException(e1);
		}
		return true;
	}

	protected boolean addAgentConnectErrorToTranComment(Throwable t,
			int tranId, String userId) {
		String className = "emgshared.exceptions.AgentConnectException";
		if (!t.getClass().getName().equals(className)) {
			return false;
		}
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		String msg = sw.toString();
		try {
			pw.close();
			sw.close();
		} catch (Exception e) {
		}
		UserProfile up = getUserProfile(userId);
		String userName = userId;
		userName = up == null ? userName : up.getFirstName() + " "
				+ up.getLastName() + " (" + userId + ")";
		StringBuilder out = new StringBuilder("User: " + userName
				+ " **AgentConnect Error**  ");
		int i, j;
		// get error code
		if (t instanceof Error) {
			Error error = (Error) t;
			out.append("Error Code: " + error.getErrorCode());
			out.append("Error String: " + error.getErrorString());
			out.append("Sub-Error Code: " + error.getSubErrorCode());
			SimpleDateFormat sdf1 = new SimpleDateFormat();
			sdf1.applyPattern("dd/MM/yyyy HH:mm:ss");
			if (error.getTimeStamp() != null) {
				String dateTime = sdf1.format(error.getTimeStamp().getTime());
				out.append("Timestamp: " + dateTime);
			}
			out.append("Detail String: " + error.getDetailString());
		} else {
			if (msg.indexOf("<ac:errorCode>") > 0) {
				i = msg.indexOf("<ac:errorCode>") + 14;
				j = msg.indexOf("</ac:errorCode>");
				out.append("Error Code: " + msg.substring(i, j).trim() + ", ");
			}
			// get error string
			if (msg.indexOf("<ac:errorString>") > 0) {
				i = msg.indexOf("<ac:errorString>") + 16;
				j = msg.indexOf("</ac:errorString>");
				out.append("Error String: " + msg.substring(i, j).trim() + ", ");
			}
			// get sub error code
			if (msg.indexOf("<ac:subErrorCode>") > 0) {
				i = msg.indexOf("<ac:subErrorCode>") + 17;
				j = msg.indexOf("</ac:subErrorCode>");
				out.append("Sub-Error Code: " + msg.substring(i, j).trim()
						+ ", ");
			}
			// get timestamp
			if (msg.indexOf("<ac:timeStamp>") > 0) {
				i = msg.indexOf("<ac:timeStamp>") + 14;
				j = msg.indexOf("</ac:timeStamp>");
				out.append("Timestamp: " + msg.substring(i, j).trim() + ", ");
			}
			// get detail string
			if (msg.indexOf("<ac:detailString>") > 0) {
				i = msg.indexOf("<ac:detailString>") + 17;
				j = msg.indexOf("</ac:detailString>");
				out.append("Detail String: " + msg.substring(i, j).trim());
			}
		}
		msg = out.toString();
		if (msg.length() > 255) {
			msg = msg.substring(0, 254);
		}
		try {
			tranManager.setTransactionComment(tranId, "OTH", msg, "emgwww");
		} catch (Exception e) {
			throw new EMGRuntimeException(e);
		}
		return true;
	}

	public void recordCcChargeback(final int tranId, final float amount,
			final String userId, final String fundCode,
			final String subReasonCode)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				Transaction tran = tranManager.getTransaction(tranId);
				if (tran == null) {
					throw new IllegalArgumentException(
							"unable to retrieve transaction with id = "
									+ tranId);
				}
				// basic checking to see if Chargeback is allowed now.
				if (!((tran.getTranSubStatCode()
						.equalsIgnoreCase(TransactionStatus.WRITE_OFF_LOSS_CODE))
						|| (tran.getTranSubStatCode()
								.equalsIgnoreCase(TransactionStatus.RECOVERY_OF_LOSS_CODE)) || (tran
						.getTranSubStatCode()
						.equalsIgnoreCase(TransactionStatus.CC_SALE_CODE)))) {
					throw new IllegalStateException("transaction id = "
							+ tranId + " is not in a Record Chargeback State");
				}
				TransactionManager manager = ManagerFactory
						.createTransactionManager();
				tran.setTranSubStatCode(fundCode);
				manager.updateTransaction(tran, userId, null,
						new Float(amount), subReasonCode);
				return null;
			}
		};
		updateTransaction(tranId, userId, command, true, true);
	}

	// method that can be used for simple status transitions where
	// a sub-reason is also needed.
	public void confirmStatusChangeWithSubReason(final int tranId,
			final String userId, final String statusCode,
			final String subStatusCode, final String subReasonCode)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				Transaction tran = tranManager.getTransaction(tranId);
				if (tran == null) {
					throw new IllegalArgumentException(
							"unable to retrieve transaction with id = "
									+ tranId);
				}
				TransactionManager manager = ManagerFactory
						.createTransactionManager();
				if (!StringHelper.isNullOrEmpty(statusCode))
					tran.setTranStatCode(statusCode);
				if (!StringHelper.isNullOrEmpty(subStatusCode))
					tran.setTranSubStatCode(subStatusCode);
				manager.updateTransaction(tran, userId, null, null,
						subReasonCode);
				return null;
			}
		};
		updateTransaction(tranId, userId, command, true, true);
		// TODO need to add code to update the sub-reason
	}

	
	protected CommitTransactionRequest buildCommitTransactionRequest(
			Transaction tran) throws Exception {
		CommitTransactionRequest req = new CommitTransactionRequest();
		req.setMgiTransactionSessionID(tran.getMgiTransactionSessionID());
		
		//MBO-7461: Unable to release Execution Pending Held Reason for Bill Pay transactions - starts
		if (null!= tran.getEmgTranTypeCode() && ("EPSEND".equalsIgnoreCase(tran.getEmgTranTypeCode())))
		{
			req.setProductType(com.moneygram.agentconnect1305.wsclient.ProductType.BP);
		}
		else{
			req.setProductType(com.moneygram.agentconnect1305.wsclient.ProductType.SEND);
		}
		//MBO-7461: Unable to release Execution Pending Held Reason for Bill Pay transactions - ends
		
		//req.setProductType(com.moneygram.agentconnect1305.wsclient.ProductType.SEND);
		req.setStateRegulatorVersion(tran.getRegulationVersion());

		return req;
	}

	/*
	 * private String buildFullName(String firstName, String middleName, String
	 * lastName) { if (!StringHelper.isNullOrEmpty(middleName)) { return
	 * firstName + " " + middleName + ". " + lastName; } else { return firstName
	 * + " " + lastName; } }
	 */

	public List<GlAccount> getGLAccounts(String userId) throws Exception {
		TransactionManager transactionManager = ManagerFactory
				.createTransactionManager();
		List<GlAccount> glAccounts = transactionManager.getGLAccounts(userId);

		return glAccounts;
	}

	// Below lines are commented as part of UCP-340
	/*protected BigDecimal getNewExchangeRate(
			AbstractAgentConnectAccessor ac, String receiveCountry,
			String sendCountry, int dlvrOptnId, String receiveCurrency,
			String receivePayoutCurrency, String agencyId, String rewardsNumber) {
		com.moneygram.agentconnect1305.wsclient.FeeLookupResponse resp = null;
		try {
			AgentConnectService acs = MGOServiceFactory.getInstance()
					.getAgentConnectService();
			com.moneygram.agentconnect1305.wsclient.DeliveryOptionInfo dlvrOptn = acs
					.getDeliveryOptionInfo(dlvrOptnId);
			resp = ac.feeLookup(new BigDecimal(1), receiveCountry,
					com.moneygram.agentconnect1305.wsclient.ProductType.SEND,
					receiveCurrency, dlvrOptn.getDeliveryOption(), agencyId,
					rewardsNumber);
		} catch (Exception e) {
			return null;
		}

		*//**
		 * WAP and MGO (US) partner sites will always retrieve the valid
		 * exchange rate value.
		 *//*
		if (sendCountry.equals("USA")
				&& !receiveCurrency.equals(receivePayoutCurrency)) {
			return resp.getFeeInfo(0).getEstimatedExchangeRate();
		} else {
			*//**
			 * Money is being sent from another country (site) such as 'MGO DE',
			 * MGOUK, and so retrieve the estimated exchange rate.
			 *//*
			return resp.getFeeInfo(0).getValidExchangeRate();
		}
	}*/

	protected com.moneygram.agentconnect1305.wsclient.FeeLookupResponse feeLookupCall(
			AbstractAgentConnectAccessor ac, Transaction tran) {
		com.moneygram.agentconnect1305.wsclient.FeeLookupResponse resp = null;
		try {
			AgentConnectService acs = MGOServiceFactory.getInstance()
					.getAgentConnectService();
			com.moneygram.agentconnect1305.wsclient.DeliveryOptionInfo dlvrOptn = acs
					.getDeliveryOptionInfo(tran.getDlvrOptnId());
			resp = ac
					.feeLookup(
							tran.getSndFaceAmt(),
							tran.getRcvISOCntryCode(),
							com.moneygram.agentconnect1305.wsclient.ProductType.SEND,
							tran.getRcvISOCrncyId(),
							dlvrOptn.getDeliveryOption(),
							tran.getLoyaltyPgmMembershipId(),
							tran.getRcvAgentId() + "");
		} catch (Exception e) {
			return null;
		}
		return resp;
	}

	/*protected com.moneygram.agentconnect1305.wsclient.FeeLookupResponse bpfeeLookupCall(
			AbstractAgentConnectAccessor ac, Transaction tran) {
		com.moneygram.agentconnect1305.wsclient.FeeLookupResponse resp = null;
		try {
			AgentConnectService acs = MGOServiceFactory.getInstance()
					.getAgentConnectService();
			com.moneygram.agentconnect1305.wsclient.DeliveryOptionInfo dlvrOptn = acs
					.getDeliveryOptionInfo(tran.getDlvrOptnId());
			resp = ac.bpfeeLookup(tran.getSndFaceAmt(),
					tran.getRcvISOCntryCode(),
					com.moneygram.agentconnect1305.wsclient.ProductType.BP,
					tran.getRcvISOCrncyId(), dlvrOptn.getDeliveryOption(),
					tran.getLoyaltyPgmMembershipId(), tran.getRcvAgcyCode()
							+ "");
		} catch (Exception e) {
			return null;
		}
		return resp;
	}*/

	private UserProfile getUserProfile(String userId) {
		UserProfile up = null;
		UserProfileManager upm = ManagerFactory.createUserManager();
		try {
			up = upm.getUser(userId);
		} catch (Exception e) {
		}
		return up;
	}

	public Transaction getTransactionForAutoProcess(String userId,
			String tranStatCode, String tranSubStatCode, String csrUserId,
			Date createDateTime, String partnerSiteId, String tranTypeCode, Date updateDateTime)
			throws DataSourceException {
		TransactionManager transactionManager = ManagerFactory
				.createTransactionManager();
		return transactionManager.getTransactionForAutoProcess(userId,
				tranStatCode, tranSubStatCode, csrUserId, createDateTime, partnerSiteId, 
				 tranTypeCode,updateDateTime);
	}

	public Collection denyExpiredTrans() {
		TransactionManager manager = ManagerFactory.createTransactionManager();
		Collection deadTranList = manager.getDeadTransactions();
		Iterator iter = deadTranList.iterator();
		while (iter.hasNext()) {
			DeadTransaction dt = (DeadTransaction) iter.next();
			manager.setTransactionStatus(dt.getTranId(), "DEN", "NOF", "system");

			// If the deadTranList has the application number as .nxt and status
			// as DEN, sub status as NOF then call cyberupdateAPI.

			}

		return deadTranList;
	}

	public void updateLimboASPTrans() {
		TransactionManager manager = ManagerFactory.createTransactionManager();
		int[] tranList = (int[]) manager.getLimboASPTransactions();
		for (int i = 0; i < tranList.length; i++) {
			manager.setTransactionStatus(tranList[i], "FFS", "NOF", "system");
		}
	}

	protected void sendAdminMessage(String processUserId, String message) {
		AdminMessage amb = new AdminMessage();
		amb.setAdminMsgTypeCode("System Error");
		amb.setMsgPrtyCode("9");
		amb.setMsgBegDate(new Date(System.currentTimeMillis()));
		amb.setMsgThruDate(new Date(System.currentTimeMillis() + 24L * 60L
				* 60L * 1000L));
		amb.setMsgText(message);
		String userPrefix = EMTAdmAppProperties.getSystemUserIdPrefix();
		List userList = new ArrayList();
		// if it's a background processor, send message to all users
		// for the role specified; otherwise, just the processing user
		if (processUserId.startsWith(userPrefix)) {
			UserProfileManager upm = ManagerFactory.createUserManager();
			UserQueryCriterion criteria = new UserQueryCriterion();
			// currently, the role id is 'Manager'
			criteria.setRoleId(EMTAdmAppProperties.getRoleIdMsgSendTo());
			Iterator iter;
			try {
				iter = upm.findHollowUsers(criteria).iterator();
			} catch (DataSourceException e) {
				throw new EMGRuntimeException(e);
			}
			while (iter.hasNext()) {
				UserProfile user = (UserProfile) iter.next();
				userList.add(user.getUID());
			}
		} else {
			userList.add(processUserId);
		}
		String[] users = new String[userList.size()];
		int i = 0;
		Iterator iter = userList.iterator();
		while (iter.hasNext()) {
			users[i++] = (String) iter.next();
		}
		try {
			messageService.insertAdminMessage(processUserId, amb, users);
		} catch (DataSourceException e) {
			// just catch this and log it, just a message failure
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.error("Error Inserting Admin Message:", e);
		}
	}

	protected String getMessageResource(String key, String[] params) {
		Locale locale = Locale.getDefault();
		MessageResources msgRes = MessageResources
				.getMessageResources("emgadm.resources.ApplicationResources");
		MessageFormat formatter = new MessageFormat(msgRes.getMessage(key));
		formatter.setLocale(locale);
		return formatter.format(params);
	}

	public boolean sendConsumerAdhocSearchTraceMail(
			ConsumerProfileSearchCriteria cpsc, String userId) {
		UserProfile up = getUserProfile(userId);
		String userName = "";
		if (up != null) {
			userName = up.getFirstName() + " " + up.getLastName() + " ("
					+ userId + ")";
		}
		Locale locale = Locale.getDefault();
		Object[] params = new Object[2];
		String label = "messages.mail.consumer.search.body";
		params[0] = userName;
		StringBuilder criteria = new StringBuilder();
		if (cpsc.getCustLogonId() != null)
			criteria.append("Cust Logon Id : " + cpsc.getCustLogonId() + "\n");
		if (cpsc.getCustFrstName() != null)
			criteria.append("Cust First Name : " + cpsc.getCustFrstName()
					+ "\n");
		if (cpsc.getCustLastName() != null)
			criteria.append("Cust Last Name : " + cpsc.getCustLastName() + "\n");
		if (cpsc.getCustPhoneNumber() != null)
			criteria.append("Cust Phone Number : " + cpsc.getCustPhoneNumber()
					+ "\n");
		if (cpsc.getCustSSNMask() != null)
			criteria.append("Cust SSN Mask : " + cpsc.getCustSSNMask() + "\n");
		if (cpsc.getCustCCMask() != null)
			criteria.append("Cust CC Mask : " + cpsc.getCustCCMask() + "\n");
		if (cpsc.getCustBankMask() != null)
			criteria.append("Cust Bank Mask : " + cpsc.getCustBankMask() + "\n");
		if (cpsc.getCustAddrLine1Text() != null)
			criteria.append("Cust Addr Line 1 : " + cpsc.getCustAddrLine1Text()
					+ "\n");
		if (cpsc.getCustFrstName() != null)
			criteria.append("Cust Addr City Name : "
					+ cpsc.getCustAddrCityName() + "\n");
		if (cpsc.getCustAddrCityName() != null)
			criteria.append("Cust Addr State Name : "
					+ cpsc.getCustAddrStateName() + "\n");
		if (cpsc.getCustAddrPostalCode() != null)
			criteria.append("Cust Addr Postal Code : "
					+ cpsc.getCustAddrPostalCode() + "\n");
		if (cpsc.getCustCreateDate() != null)
			criteria.append("Cust Create Date : " + cpsc.getCustCreateDate()
					+ "\n");
		if (cpsc.getCustCreateIpAddrId() != null)
			criteria.append("Cust Create IP Address : "
					+ cpsc.getCustCreateIpAddrId() + "\n");
		if (cpsc.getCustPrmrCode() != null)
			criteria.append("Cust Premier Code : " + cpsc.getCustPrmrCode()
					+ "\n");
		if (cpsc.getCustStatCode() != null)
			criteria.append("Cust Status Code : " + cpsc.getCustStatCode()
					+ "\n");
		if (cpsc.getCustSubstatCode() != null)
			criteria.append("Cust Sub Status Code : "
					+ cpsc.getCustSubstatCode() + "\n");
		params[1] = criteria.toString();
		String body = I18NHelper.getFormattedMessage(label, params,
				resourceFile, locale);
		String subject = I18NHelper.getFormattedMessage(
				"messages.mail.consumer.search.subject", resourceFile, locale);
		return MailHelper.sendMail(EMTSharedContainerProperties.getMailHost(),
				EMTSharedContainerProperties.getMailFrom(),
				EMTSharedContainerProperties.getMailACTo(), subject, body);
	}

	protected String getTransactionUrl(String contextRoot) {
		return contextRoot + "/transactionDetail.do";
	}

	public void doCreditCardReversal(int tranId, String userId,
			String contextRoot) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		// implement in subclass if needed

	}

	public void doCreditCardPartialRefund(int tranId, double refundAmount,
			String userId, String contextRoot)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException, CreditCardRefundException {
		// UK only method

	}


	protected void fundDelayedSends(final int tranId, final String userId,
			final String contextRoot)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException, BankPaymentServiceException {

		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				final Transaction tran = tranManager.getTransaction(tranId);
				if (tran == null) {
					throw new IllegalArgumentException(
							"unable to retrieve transaction with id = "
									+ tranId);
				}

				final int tranId = tran.getEmgTranId();

				try {
					long beginTime = System.currentTimeMillis();
					BankPaymentProxy proxy = BankPaymentProxyImpl.getInstance();
					StatusInquiryRequestMessage rqst = new StatusInquiryRequestMessage();
					rqst.setClientTraceId(tran.getTCInternalTransactionNumber());
					rqst.setProcessorTraceId(tran
							.getTCProviderTransactionNumber());
					StatusInquiryResponseMessage resp = proxy
							.statusInquiry(rqst,tran);

					EventMonitor.getInstance().recordEvent(
							EventListConstant.BANK_PAYMENT_SERVICE_POST,
							beginTime, System.currentTimeMillis());

					boolean success = false;
					if (resp.getResponseCode()
							.equalsIgnoreCase(
									BankPaymentProxyImpl.BANK_PAYMENT_RESP_POST_ACCOUNT_SUCCESS)) {
						// Change from FFS/NOF to APP/NOF
						if (tran.getStatus()
								.getStatusCode()
								.equalsIgnoreCase(
										TransactionStatus.FORM_FREE_SEND_CODE)
								|| tran.getStatus()
										.getStatusCode()
										.equalsIgnoreCase(
												TransactionStatus.PENDING_CODE)) {
							tran.setTranStatCode(TransactionStatus.APPROVED_CODE);
							tranManager.updateTransaction(tran, userId, null,
									null);
						}
						// Change from APP/NOF to APP/BKS
						tran.setTranSubStatCode(TransactionStatus.BANK_SETTLED_CODE);
						tranManager.updateTransaction(tran, userId,
								resp.getApprovalCode(), null);
						success = true;
					}

					CommentService commentService = emgshared.services.ServiceFactory
							.getInstance().getCommentService();
					commentService.insertBankPaymentServiceResponseComment(
							"STATUS", Integer.valueOf(tranId), success, userId,
							resp.getResponseCode(), resp.getApprovalCode(),
							resp.getDenialRecNbr(), resp.getAchStatus(),
							tran.getTCProviderTransactionNumber(),
							tran.getTCInternalTransactionNumber());

					if (!success) {
						return "BankPaymentService posting call unsuccessful";
					}

				} catch (Exception e) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).error(
							"Failed to post to BankPaymentService:", e);
					CommentService commentService = emgshared.services.ServiceFactory
							.getInstance().getCommentService();
					commentService.insertBankPaymentServiceResponseComment(
							"STATUS", Integer.valueOf(tranId), false, userId, null,
							null, null, null,
							tran.getTCProviderTransactionNumber(),
							tran.getTCInternalTransactionNumber());
					return "BankPaymentService Exception: " + e.getMessage();
				}

				return null;
			}
		};
		Object obj = updateTransaction(tranId, userId, command);
		if (obj != null) {
			throw new BankPaymentServiceException((String) obj);
		}
	}

	public ArrayList processDelayedSendTransaction(int tranId,
			String callerLoginId, String tranStat, String fundStat,
			String contextRoot) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException, AgentConnectException,
			InvalidRRNException, DataSourceException, SQLException {

		ArrayList<String> errors = new ArrayList<String>();
		// Do AgentConnect MoneyGram Send with status of APP/CCA
		TransactionManager tm = ManagerFactory.createTransactionManager();
		Transaction tran = tm.getTransaction(tranId);
		if (TransactionStatus.APPROVED_CODE.equals(tran.getTranStatCode())
				&& TransactionStatus.BANK_SETTLED_CODE.equals(tran
						.getTranSubStatCode())) {
			//code changed for MBO-1050
			try {
				if (TransactionType.DELAY_SEND_CODE.equals(tran.getEmgTranTypeCode())) {
					  sendPersonToPerson(tranId, callerLoginId, false);       
					} else {
					  sendExpressPayment(tranId, callerLoginId, contextRoot);
					}
					tran = tm.getTransaction(tranId);
				} catch (AgentConnectException e) {
					// add admin message
					String[] params = { String.valueOf(tran.getEmgTranId()),
					    tran.getLgcyRefNbr(), getTransactionUrl(contextRoot) };
					String errorKey = TransactionType.DELAY_SEND_CODE.equals(tran.getEmgTranTypeCode()) 
					? "error.person.to.person.send.fail.notice" : "error.express.pay.send.fail.notice";
					sendAdminMessage(
					    tran.getCsrPrcsUserid(),
					    getMessageResource(
					        errorKey,
					        params));

			//code changes end for MBO-1050
				errors.add("error.agent.connect.failed");
				tran.setTranStatCode(TransactionStatus.ERROR_CODE);
				tran.setSndTranDate(new Date());
				tm.updateTransaction(tran, callerLoginId, null, null);
				return errors;
			}
		}
		return null; // all is well.
	}

	protected abstract void fundExpressPayment(int tranId, String authConfCode,
			String callerLoginId, String contextRoot)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException;

	public ArrayList processEconomyServiceTransaction(int tranId,
			String callerLoginId, String tranStat, String fundStat,
			String contextRoot) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException, AgentConnectException,
			InvalidRRNException, DataSourceException, SQLException {
		ArrayList elr = new ArrayList<String>();
		Transaction tran;
		tran = ManagerFactory.createTransactionManager().getTransaction(tranId);
		if (tran.isSendable()) {
			try {
				sendEconomyService(tranId, callerLoginId);
			} catch (AgentConnectException e) {
				// add admin message
				String[] params = { String.valueOf(tran.getEmgTranId()),
						tran.getLgcyRefNbr(), getTransactionUrl(contextRoot) };
				sendAdminMessage(
						tran.getCsrPrcsUserid(),
						getMessageResource(
								"error.economy.service.send.fail.notice",
								params));
				elr.add("error.agent.connect.failed");
				return elr;
			}
		}
		return null;
	}

	public void refundPersonToPerson(final int tranId, final String userId)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException, AgentConnectException,
			EMGRuntimeException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				final TransactionManager tranManager = ManagerFactory
						.createTransactionManager();
				final Transaction tran = tranManager.getTransaction(tranId);
				if (tran == null) {
					throw new IllegalArgumentException(
							"unable to retrieve transaction with id = "
									+ tranId);
				}
				if (!tran.isRefundable()) {
					throw new IllegalStateException("transaction id = "
							+ tranId + " is not in a refundable state.");
				}
				// int confirmationNumber = tran.getFormFreeConfNbr();
				try {
					Date before = new Date(System.currentTimeMillis());
					long beginTime = System.currentTimeMillis();
					
					//MBO-6101
					/*MGOAgentProfileService maps = MGOServiceFactory
							.getInstance().getMGOAgentProfileService();
					String agtid = String.valueOf(tran.getSndAgentId());*/
					//code for 1054  - getting DSSEND AC where Tran Type is EPSEND
					String tranTypeCode = tran.getEmgTranTypeCode();
					if("EPSEND".equalsIgnoreCase(tran.getEmgTranTypeCode()) 
							&& bankFundingAccType.equalsIgnoreCase(tran.getSndAcctTypeCode())) {
						//System.out.println("Tran is EPSEND Bank funded, so changing Agent to DSSEND for MBO 1054");
							tranTypeCode = "DSSEND";
					}
					/*MGOAgentProfile mgoAgentProfile = maps
							.getMGOProfileByAgtId(agtid,
									tranTypeCode);*/
					EMGSharedLogger.getLogger(this.getClass()).info("inside refundPersonToPerson - getting AC details from tran proc");
					MGOAgentProfile mgoAgentProfile = new MGOAgentProfile();
					mgoAgentProfile.setAgentId(tran.getAgentId());
					mgoAgentProfile.setAgentSequence(tran.getAgentSeqNo());
					mgoAgentProfile.setProfileId(tran.getAgentUnitProfileId()!=null ? Integer.valueOf(tran.getAgentUnitProfileId()) : 0);
					mgoAgentProfile.setToken(tran.getAgentToken());
					MGOProduct mgoProduct = new MGOProduct();
					mgoProduct.setProductName(tranTypeCode);
					mgoAgentProfile.setMgoProduct(mgoProduct);
					//ended 6101
					
					AgentConnectAccessor agentConnectAccessor = new AgentConnectAccessor(
							mgoAgentProfile);
					// Try 'C' (Cancel) reversal type first. AC will return
					// 605/3401 error/suberror if txn is in state
					// if 'R' (Refund) type is needed - if so, then do 'R' type
					// reversal
					
					//As per MBO-9384,we are sending AgentConnect SendReversalRequest with only "R" ReversalType.
					try {
						SendReversalResponse sendReversalResponse = agentConnectAccessor.sendReversal(
								String.valueOf(tran.getLgcyRefNbr()),
								tran.getSndFaceAmt(), tran.getSndFeeAmt(),
								tran.getSndISOCrncyId(), SendReversalType.R);
						if(null != sendReversalResponse){
							ConsumerAccountType primaryAccountType = ConsumerAccountType
							.getInstance(tran.getSndAcctTypeCode());
							refundPersonToPersonSuccessEmail(tran,
									primaryAccountType);
						}
					} catch (Error err) {
						EMGSharedLogger.getLogger(
								this.getClass().getName().toString()).error(
								"SendReversal Failed for tranId"+tran.getEmgTranId()+"legacy reference number"+ tran.getLgcyRefNbr()+"Error Code is"  
										+ err.getErrorCode()
										+"SubErrorCode is"
										+ err.getSubErrorCode());
						throw err;
					} catch (Exception e1) {
						addAgentConnectErrorToTranComment(e1, tranId, userId);
						String msg = "In Program: TransactionServiceImpl\n"
								+ "In Method: refundPersonToPerson()\n"
								+ "AC call started at: "
								+ before
								+ "\n"
								+ "AC call aborted at: "
								+ new Date(System.currentTimeMillis())
								+ "\n"
								+ "AC Method called: sendReversal() with a transaction id of "
								+ tran.getEmgTranId()
								+ " and the legacy reference number of "
								+ tran.getLgcyRefNbr() + "\n\n";
						EMGSharedLogger.getLogger(
								this.getClass().getName().toString()).error(
								"send Economy Service failed.\n\n" + msg, e1);
						sendAgentConnectFailMail(msg, e1, userId);
						throw new AgentConnectException(e1);
					} finally {
						EventMonitor.getInstance().recordEvent(
								EventListConstant.AC_SEND_REVERSAL, beginTime,
								System.currentTimeMillis());
					}
					// FIXME S26 - set flag to indicate whether cancel cuz NSF
					// for Bank(Telecheck) or customer initiated
					// refund/cancellation
					// This is for email logic to know what type of email to
					// send
					

					if (!tran.getEmgTranTypeCode().equalsIgnoreCase(
							TransactionType.DELAY_SEND_CODE)) {
						// set the transaction status to cancel
						tran.setTranStatCode(TransactionStatus.CANCELED_CODE);
						tran.setTranStatDate(new Date());
						tranManager.updateTransaction(tran, userId);

						// set the fund status to ACH Cancel, ACH or CC Refund
						// Request
						if (tran.getTranSubStatCode().equalsIgnoreCase(
								TransactionStatus.ACH_WAITING_CODE)) {
							tran.setTranSubStatCode(TransactionStatus.ACH_CANCEL_CODE);
						} else if (tran.getTranSubStatCode().equalsIgnoreCase(
								TransactionStatus.ACH_SENT_CODE)) {
							tran.setTranSubStatCode(TransactionStatus.ACH_REFUND_REQUESTED_CODE);
						} else if (tran.getTranSubStatCode().equalsIgnoreCase(
								TransactionStatus.CC_SALE_CODE)) {
							tran.setTranSubStatCode(TransactionStatus.CC_REFUND_REQUESTED_CODE);
						}
						tran.setTranStatDate(new Date());
						tranManager.updateTransaction(tran, userId);

					} else {
						// Handle DelayedSends status/substatus changes
						// SEN/BKS will go to SEN/BRR after AC cancel - it is
						// odd, but will go to CXL/BRR
						// after Bank reversal
						if (tran.getTranStatCode().equalsIgnoreCase(
								TransactionStatus.SENT_CODE)
								&& tran.getTranSubStatCode().equalsIgnoreCase(
										TransactionStatus.BANK_SETTLED_CODE)) {
							tran.setTranSubStatCode(TransactionStatus.BANK_REFUND_REQUEST_CODE);
						} else {
							tran.setTranStatCode(TransactionStatus.CANCELED_CODE);
						}
						tran.setTranStatDate(new Date());
						tranManager.updateTransaction(tran, userId);
					}

					/*ConsumerAccountType primaryAccountType = ConsumerAccountType
							.getInstance(tran.getSndAcctTypeCode());
					try {
						refundPersonToPersonSuccessEmail(tran,
								primaryAccountType);
					} catch (Exception e) {
						// Email failed to get sent.
						// Not a serious error, log and move on.
						EMGSharedLogger.getLogger(
								this.getClass().getName().toString()).error(
								"Failed to send Transaction Email:", e);
					}*/
				} catch (Throwable t) {
					if (addAgentConnectErrorToTranComment(t, tranId, userId)) {
						EMGSharedLogger
								.getLogger(this.getClass().getName().toString())
								.error("refundPersonToPerson AgentConnect reversal failed.",
										t);
						throw new EMGRuntimeException(t);
					} else {
						EMGSharedLogger.getLogger(
								this.getClass().getName().toString()).error(
								"refundPersonToPerson failed.", t);
						throw new EMGRuntimeException(t);
					}
				}
				return null;
			}
		};
		updateTransaction(tranId, userId, command);
	}

	public void recoveryOfLoss(final int tranId, final float amount,
			final float recoveredAmt, final String userId,
			final String fundCode, final String subReasonCode)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				Transaction tran = tranManager.getTransaction(tranId);
				if (tran == null) {
					throw new IllegalArgumentException(
							"unable to retrieve transaction with id = "
									+ tranId);
				}
				if (!tran.getTranStatCode().equalsIgnoreCase(
						TransactionStatus.SENT_CODE)
						|| !tran.getTranSubStatCode().equalsIgnoreCase(
								TransactionStatus.WRITE_OFF_LOSS_CODE)) {
					throw new IllegalStateException("transaction id = "
							+ tranId + " is not in a recovery of loss state");
				}
				TransactionManager manager = ManagerFactory
						.createTransactionManager();
				// Change the status so the stored procedure code records
				// ledger entries properly. For the moment, we call
				// update transaction as opposed to set transaction status
				// because set transaction status does not accept an amount.
				tran.setTranSubStatCode(fundCode);
				manager.updateTransaction(tran, userId, null,
						new Float(amount), subReasonCode);
				// int remain =
				// Math.round((totalAmt - amount - recoveredAmt) * 100F);
				// Change the status back to write off loss because the
				// preceding status is considered a transient status.
				// if (remain > 0)
				// {
				tran.setTranSubStatCode(TransactionStatus.WRITE_OFF_LOSS_CODE);
				manager.setTransactionStatus(tran.getEmgTranId(),
						tran.getStatus(), userId);
				// }
				return null;
			}
		};
		updateTransaction(tranId, userId, command, true, true);
		// TODO need to add code to update the sub-reason
	}

	public void doBankReversal(final int tranId, final String userId,
			final String contextRoot)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				final Transaction tran = tranManager.getTransaction(tranId);
				if (tran == null) {
					throw new IllegalArgumentException(
							"unable to retrieve transaction with id = "
									+ tranId);
				}

				final int tranId = tran.getEmgTranId();

//				if (!tran.getStatus().getSubStatusCode()
//						.equalsIgnoreCase(TransactionStatus.BANK_SETTLED_CODE)
//						|| isWithinValidTimeFrame(tran, userId)) {
					// DO Adjustment/ Bank reversal
				
				
				// if CXL/BRR, don't need to check if within valid timeframe,
				// otherwise check to see if cancel is done same day as approved
				// AND check the time to make sure it qualifies
			if ( (tran.getStatus().getSubStatusCode().equalsIgnoreCase(TransactionStatus.BANK_REFUND_REQUEST_CODE) &&
			    tran.getStatus().getStatusCode().equalsIgnoreCase(TransactionStatus.CANCELED_CODE))
			    || isWithinValidTimeFrame(tran, userId)) {
				
				
				
				
					final double amount = tran.getSndTotAmt().doubleValue();

					AdjustmentInquiryResponseMessage resp = null;
					try {
						BankPaymentProxy proxy = BankPaymentProxyImpl
								.getInstance();
						AdjustmentInquiryRequestMessage message = new AdjustmentInquiryRequestMessage();
						message.setClientTraceId(tran
								.getTCInternalTransactionNumber());
						message.setProcessorTraceId(tran
								.getTCProviderTransactionNumber());
						message.setTotalAmmount(tran.getSndTotAmt().toString());
						message.setTransactionType(BankPaymentProxyImpl.BANK_PAYMENT_ADJUSTMENT_TYPE);
						long beginTime = System.currentTimeMillis();
						resp = proxy.adjustmentInquiry(message,tran);
						EventMonitor
								.getInstance()
								.recordEvent(
										EventListConstant.BANK_PAYMENT_SERVICE_ADJUSTMENT,
										beginTime, System.currentTimeMillis());
						boolean success = false;
						if (resp.getResponseCode()
								.equalsIgnoreCase(
										BankPaymentProxyImpl.BANK_PAYMENT_RESP_ADUSTMENT_SUCCESS)) {
							success = true;
						}

						CommentService commentService = emgshared.services.ServiceFactory
								.getInstance().getCommentService();
						commentService.insertBankPaymentServiceResponseComment(
								"ADJUSTMENT", Integer.valueOf(tranId), success,
								userId, resp.getResponseCode(), null,
								resp.getDenialRecNbr(), resp.getAchStatus(),
								tran.getTCProviderTransactionNumber(),
								tran.getTCInternalTransactionNumber());

					} catch (Exception e) {
						CommentService commentService = emgshared.services.ServiceFactory
								.getInstance().getCommentService();
						commentService.insertBankPaymentServiceResponseComment(
								"ADJUSTMENT", Integer.valueOf(tranId), false,
								userId, null, null, null, null,
								tran.getTCProviderTransactionNumber(),
								tran.getTCInternalTransactionNumber());
					}
					// Note: If errors with Bank reversal, it's ok. EOC will
					// handle after viewing Funding reports

				} else {
					try {
						tranManager
								.setTransactionComment(
										tranId,
										"OTH",
										"TELECHECK - Skipping adjustment it may be in ACH processing at Telecheck.",
										userId);
					} catch (Exception e) {
						EMGSharedLogger
								.getLogger(this.getClass().getName().toString())
								.warn("Failed to log comment about skipping adjustment.",
										e);
					}
				}
				return null;
			}
		};
		updateTransaction(tranId, userId, command);
	}

	private boolean isWithinValidTimeFrame(Transaction transaction,
			String userId) {

		List tas;
		try {
			tas = tranManager
					.getTranActions(transaction.getEmgTranId(), userId);
		} catch (Exception e) {
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.error("Failed to get tran actions:", e);
			return false;
		}
		Iterator iter = tas.iterator();
		TranAction ta = null;
		while (iter.hasNext()) {
			ta = (TranAction) iter.next();
			if (ta.getTranStatCode().equalsIgnoreCase(
					TransactionStatus.APPROVED_CODE)
					&& ta.getTranSubStatCode().equalsIgnoreCase(
							TransactionStatus.BANK_SETTLED_CODE)) {
				break;
			}
		}

		if (ta == null
				|| !ta.getTranStatCode().equalsIgnoreCase(
						TransactionStatus.APPROVED_CODE)
				|| !ta.getTranSubStatCode().equalsIgnoreCase(
						TransactionStatus.BANK_SETTLED_CODE)) {
			return false;
		}

		Calendar approvedDateTime = Calendar.getInstance();
		approvedDateTime.setTime(ta.getTranActionDate());

		// final TimeZone CST = TimeZone.getTimeZone("CST");
		Calendar lowerBoundLimit = Calendar.getInstance();
		Calendar upperBoundLimit = Calendar.getInstance();

		lowerBoundLimit = DateFormatter.setCalendarTimeFromHHMMString(
				eadp.getDsStartAchWindow(), lowerBoundLimit);
		upperBoundLimit = DateFormatter.setCalendarTimeFromHHMMString(
				eadp.getDsEndAchWindow(), upperBoundLimit);

		Calendar now = Calendar.getInstance();

		boolean sameDay = isSameDay(approvedDateTime, now);
		if (!sameDay) {
			return false;
		}

		// if it is now prior to start of processing window, can reverse today's
		// approved items
		if (now.getTime().getTime() < lowerBoundLimit.getTime().getTime()) {
			return true;
		}

		// if it is between today's processing window, don't process anything
		if (now.getTime().getTime() >= lowerBoundLimit.getTime().getTime()
				&& now.getTime().getTime() <= upperBoundLimit.getTime()
						.getTime()) {
			return false;
		}

		// if is after end of processing window and transaction approved after
		// processing window can reverse
		if (now.getTime().getTime() >= upperBoundLimit.getTime().getTime()
				&& approvedDateTime.getTime().getTime() >= upperBoundLimit
						.getTime().getTime()) {
			return true;
		}

		return false;
	}

	public void refundPersonToPersonSuccessEmail(Transaction tran,
			ConsumerAccountType primaryAccountType) throws Exception {
		ConsumerAccount account;
		if (primaryAccountType.isCardAccount()) {
			account = accountService.getCreditCardAccount(
					tran.getSndCustAcctId(), tran.getSndCustLogonId());
		} else {
			account = accountService.getBankAccount(tran.getSndCustAcctId(),
					tran.getSndCustLogonId());
		}
		if (account != null) {
			ConsumerProfile profile = profileService.getConsumerProfile(
					Integer.valueOf(account.getConsumerId()),
					tran.getSndCustLogonId(), "");
			if (profile != null) {
				ConsumerEmail consumerEmail = null;
				// Loop sends out email to each address.
				NotificationAccessImpl na = new NotificationAccessImpl();
				try { //MBO-6990
					Notification emailNotification = na
							.createNotificationRequest( tran, NotificationAccessImpl.EMAIL_NOTIFY_MSG_TYPE_TRAN_CANCEL, profile);
					OSLProxyImpl.sendNotification(emailNotification);
					log.info("Txn Cancelled email sent successfully !  Tran Id :: "+ tran.getEmgTranId() + "Cust Id" + profile.getId());
				} catch (OSLException e) {
					log.error("Error while sending email", e);
				}	
			}
		}
	}

	public void sendEconomyService(final int tranId, final String userId)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException, InvalidRRNException,
			AgentConnectException {
		final TransactionManager tranManager = ManagerFactory
				.createTransactionManager();
		final Transaction tran = tranManager.getTransaction(tranId);
		UpdateTransactionCommand command1305 = new UpdateTransactionCommand() {
			public Object execute() {
				if (tran == null) {
					throw new IllegalArgumentException(
							"unable to retrieve transaction with id = "
									+ tranId);
				}
				if (!tran.isSendable()) {
					throw new IllegalStateException("transaction id = "
							+ tranId + " is not in a sendable state.");
				}
				CommitTransactionRequest req = null;
				CommitTransactionResponse resp = null;

				Date before = new Date(System.currentTimeMillis());
				AgentConnectAccessor agentConnectAccessor = null;
				try {
					//Removed RRN check as part of MBO-5147
					//MBO-6101
					/*MGOAgentProfileService maps = MGOServiceFactory
							.getInstance().getMGOAgentProfileService();
					String agtid = String.valueOf(tran.getSndAgentId());
					MGOAgentProfile mgoAgentProfile = maps
							.getMGOProfileByAgtId(agtid,
									tran.getEmgTranTypeCode());*/
					EMGSharedLogger
					.getLogger(
							this.getClass().getName()
									.toString()).info("Inside sendEconomyService method, getting AC conf Tran proc details");
					MGOAgentProfile mgoAgentProfile = new MGOAgentProfile();
					mgoAgentProfile.setAgentId(tran.getAgentId());
					mgoAgentProfile.setAgentSequence(tran.getAgentSeqNo());
					mgoAgentProfile.setProfileId(tran.getAgentUnitProfileId()!=null ? Integer.valueOf(tran.getAgentUnitProfileId()) : 0);
					mgoAgentProfile.setToken(tran.getAgentToken());
					MGOProduct mgoProduct = new MGOProduct();
					mgoProduct.setProductName(tran.getEmgTranTypeCode());
					mgoAgentProfile.setMgoProduct(mgoProduct);
					//6101 ended
					agentConnectAccessor = new AgentConnectAccessor(
							mgoAgentProfile);
					long beginTime = System.currentTimeMillis();
					try {
						req = buildCommitTransactionRequest(tran);
						if (req.getToken() == null
								|| req.getToken().length() < 1) {
							req.setToken("TEST");
						}
						try {
							EMGSharedLogger
									.getLogger(
											this.getClass().getName()
													.toString())
									.debug("EMT Agent Connect v.13.05 ...START...\n\n");
							resp = agentConnectAccessor
									.commitTransaction(req);
							EMGSharedLogger
									.getLogger(
											this.getClass().getName()
													.toString())
									.debug("EMT Agent Connect v.13.05 ...END...\n\n");
						} catch (Exception e) {
							EMGSharedLogger
									.getLogger(
											this.getClass().getName()
													.toString())
									.debug("EMT Agent Connect v.13.05 ...ERROR...\n\n");
							acknowledgementFailEmail(
									tranId,
									resp == null ? "" : resp
											.getReferenceNumber(), e, userId);
							throw new AgentConnectException(e);
						}

					} catch (Exception e1) {
						EMGSharedLogger.getLogger(
								this.getClass().getName().toString()).error(
								"response failed.\n\n", e1);
						throw new AgentConnectException(e1);
					} finally {
						EventMonitor.getInstance().recordEvent(
								EventListConstant.ES_AC_SEND, beginTime,
								System.currentTimeMillis());
					}
					String referenceNumber = resp.getReferenceNumber();
					tran.setLgcyRefNbr(referenceNumber);
					tran.setSndTranDate(new Date());

					// S29a Add three minute phone number and three minute pin
					// number
					if (tran.getDlvrOptnId() == DeliveryOption.WILL_CALL_ID
							|| tran.getDlvrOptnId() == DeliveryOption.ONLY_AT_ID) {
						tran.setThreeMinuteFreePhoneNumber(resp
								.getTollFreePhoneNumber());
						tran.setThreeMinuteFreePinNumber(resp
								.getFreePhoneCallPIN());
					}
					//MGO-12802 Changes starts, commenting the below if condition to avoid updating the new exchange rate while approving the transaction
					/*if (!"USA".equalsIgnoreCase(tran.getRcvISOCntryCode())) {
						BigDecimal exRate = null;
						exRate = getNewExchangeRate(
								agentConnectAccessor,
								tran.getRcvISOCntryCode(),
								tran.getSndISOCntryCode(),
								tran.getDlvrOptnId(), tran.getRcvISOCrncyId(),
								tran.getRcvPayoutISOCrncyId(), null,
								tran.getLoyaltyPgmMembershipId());
						// set to new foreign exchange rate
						if (exRate != null) {
							tran.setSndFxCnsmrRate(exRate);
							if (!tran.getRcvISOCrncyId().equals(
									tran.getRcvPayoutISOCrncyId())) {

							} else {
							}
						}
					}*/
					String refNbr = resp
							.getPartnerConfirmationNumber();
					if (!StringHelper.isNullOrEmpty(refNbr)) {
						tran.setRcvAgentRefNbr(refNbr);
					}
					ConsumerAccountType accountType = ConsumerAccountType
							.getInstance(tran.getSndAcctTypeCode());
					tran.setTranStatCode(TransactionStatus.SENT_CODE);
					tran.setSndTranDate(new Date());
					tran.setTranAvailabilityDate(resp
							.getExpectedDateOfDelivery());
					tranManager.updateTransaction(tran, userId, null, null);
					try {
						sendEconomyServiceSuccessEmail(tran, accountType);
					} catch (Exception e) {
						// Email failed to get sent.
						// Not a serious error, log and move on.
						EMGSharedLogger.getLogger(
								this.getClass().getName().toString()).error(
								"Failed to send Transaction Email:", e);
					}
				} catch (Throwable t) {
					addAgentConnectErrorToTranComment(t, tranId, userId);
					String msg = "In Program: TransactionServiceImpl\n"
							+ "In Method: sendEconomyService()\n"
							+ "AC call started at: "
							+ before
							+ "\n"
							+ "AC call aborted at: "
							+ new Date(System.currentTimeMillis())
							+ "\n"
							+ "AC Method called: moneyGramSend() with a transaction id of "
							+ tran.getEmgTranId()
							+ " and the request object is as follows:\n\n"
							+ req;
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).error(
							"send Economy Service failed.\n\n" + msg, t);
					sendAgentConnectFailMail(msg, t, userId);
					return t;
				}
				return null;
			}
		};
		// Object obj = updateTransaction(tranId, userId,
		// !isMgiTranId(tran)?command76:command1305);
		Object obj = updateTransaction(tranId, userId, command1305);
		if (obj != null) {
			throw new AgentConnectException((Throwable) obj);
		}
	}

	private void sendEconomyServiceSuccessEmail(Transaction tran,
			ConsumerAccountType accountType) throws Exception {
		ConsumerAccount account;
		if (accountType.isCardAccount()) {
			account = accountService.getCreditCardAccount(
					tran.getSndCustAcctId(), tran.getSndCustLogonId());
		} else {
			account = accountService.getBankAccount(tran.getSndCustAcctId(),
					tran.getSndCustLogonId());
		}
		if (account != null) {
			ConsumerProfile profile = profileService.getConsumerProfile(
					Integer.valueOf(account.getConsumerId()),
					tran.getSndCustLogonId(), "");
			if (profile != null) {
				String preferedlanguage = profile.getPreferedLanguage();
				ConsumerEmail consumerEmail = null;
				// Loop sends out email to each address.
				NotificationAccess na = new NotificationAccessImpl();
				for (Iterator<ConsumerEmail> iter = profile.getEmails()
						.iterator(); iter.hasNext();) {
					consumerEmail = iter.next();
					 //MBO-5319 - Modified Method signature to use Profile object, will be useful in future if any
					  //value needs to be fetched from Profile
					boolean sendSMS = true;
					na.notifySentTransaction(tran, consumerEmail,
							preferedlanguage,profile, sendSMS);
					//ended 5319
				}
			}
		}
	}

	public void sendExpressPayment(final int tranId, final String userId,
			final String contextRoot)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException, AgentConnectException {
		final TransactionManager tranManager = ManagerFactory
				.createTransactionManager();
		final Transaction tran = tranManager.getTransaction(tranId);

		UpdateTransactionCommand command1305 = new UpdateTransactionCommand() {
			public Object execute() {
				if (tran == null) {
					throw new IllegalArgumentException(
							"unable to retrieve transaction with id = "
									+ tranId);
				}
				if (!tran.isSendable()) {
					throw new IllegalStateException("transaction id = "
							+ tranId + " is not in a sendable state.");
				}
				com.moneygram.agentconnect1305.wsclient.FeeLookupResponse feeResponse = null;
				BpValidationRequest bpRequest = null;
				BpValidationResponse bpResponse = null;
				CommitTransactionRequest req = null;
				CommitTransactionResponse response = null;
				Date before = new Date(System.currentTimeMillis());
				try {
					
					//MBO-6101
					/*MGOAgentProfileService maps = MGOServiceFactory
							.getInstance().getMGOAgentProfileService();
					String agtid = String.valueOf(tran.getSndAgentId());*/
					//code for 1050  - getting DSSEND AC where Tran Type is EPSEND
					String tranTypeCode = tran.getEmgTranTypeCode();
					if("EPSEND".equalsIgnoreCase(tran.getEmgTranTypeCode()) 
							&& bankFundingAccType.equalsIgnoreCase(tran.getSndAcctTypeCode())) {
						//System.out.println("Tran is EPSEND Bank funded, so changing Agent to DSSEND");
							tranTypeCode = "DSSEND";
					}
					/*MGOAgentProfile mgoAgentProfile = maps
							.getMGOProfileByAgtId(agtid,
									tranTypeCode);*/
					EMGSharedLogger.getLogger(this.getClass()).info("inside sendExpressPayment - getting AC details from tran proc");
					MGOAgentProfile mgoAgentProfile = new MGOAgentProfile();
					mgoAgentProfile.setAgentId(tran.getAgentId());
					mgoAgentProfile.setAgentSequence(tran.getAgentSeqNo());
					mgoAgentProfile.setProfileId(tran.getAgentUnitProfileId()!=null ? Integer.valueOf(tran.getAgentUnitProfileId()) : 0);
					mgoAgentProfile.setToken(tran.getAgentToken());
					MGOProduct mgoProduct = new MGOProduct();
					mgoProduct.setProductName(tranTypeCode);
					mgoAgentProfile.setMgoProduct(mgoProduct);
					//ended 6101
					AgentConnectAccessor ac = new AgentConnectAccessor(
							mgoAgentProfile);
					long beginTime = System.currentTimeMillis();
					try {

						// The AC response will return an exception if there is
						// one
						// Send an email and then, throw an exception
						// add admin message
						try {
							// Commented as a part of UCP-340
							/*if(tran.getTranAppVersionNumber()<=2.0){//MBO-5881
								System.out.println("********** BP Validated for Tran ID : "+tranId);
								feeResponse = bpfeeLookupCall(ac, tran);
								tran.setMgiTransactionSessionID(feeResponse
										.getFeeInfo()[0]
										.getMgiTransactionSessionID());
								bpRequest = buildBpValidationRequest(ac, tran,
										userId);
								bpResponse = ac.bpValidation(bpRequest);
							}*/
							req = buildCommitTransactionRequest(tran);
							try {
								response = ac.commitTransaction(req);
							} catch (Exception r) {
								EMGSharedLogger
										.getLogger(
												this.getClass().getName()
														.toString())
										.error("send Express Commit Transcation failed retrying.. Transcation Id"
												+ tran.getEmgTranId());
								// Retry for RTS Real time, if error is 4200
								response = ac.commitTransaction(req);
							}
						} catch (Exception e) {
							String[] params = {
									String.valueOf(tran.getEmgTranId()),
									response == null ? "" : response.getReferenceNumber(),
									getTransactionUrl(contextRoot) };
							sendAdminMessage(
									tran.getCsrPrcsUserid(),
									getMessageResource(
											"error.express.pay.send.fail.notice",
											params));
							// send email
							acknowledgementFailEmail(
									tranId,
									response == null ? "" : response
											.getReferenceNumber(), e, userId);
							throw new AgentConnectException(e);
						}
					} catch (Exception e1) {
						EMGSharedLogger.getLogger(
								this.getClass().getName().toString()).error(
								"response failed.\n\n", e1);
						throw new AgentConnectException(e1);
					} finally {
						EventMonitor.getInstance().recordEvent(
								EventListConstant.EP_AC_SEND, beginTime,
								System.currentTimeMillis());
					}
					String referenceNumber = response.getReferenceNumber();
					tran.setReceiptTextInfoEng(getReceiptTextInfo(
							response.getReceiptTextInfo(), "ENG"));
					tran.setReceiptTextInfoSpa(getReceiptTextInfo(
							response.getReceiptTextInfo(), "SPA"));
					tran.setLgcyRefNbr(referenceNumber);
					tran.setSndTranDate(new Date());
					tran.setTranStatCode(TransactionStatus.SENT_CODE);
					tranManager.updateTransaction(tran, userId);
					tranManager.setTransactionStatus(tranId,
							tran.getTranStatCode(), tran.getTranSubStatCode(),
							userId);
					try {
						sendExpressPaymentSuccessMail(tran,
								ConsumerAccountType.getInstance(tran
										.getSndAcctTypeCode()));
					} catch (Exception e) {
						// Email failed to get sent. Not a serious error, log
						// and move on.
						EMGSharedLogger.getLogger(
								this.getClass().getName().toString()).error(
								"Failed to send Transaction Email:", e);
					}
				} catch (Throwable t) {
					addAgentConnectErrorToTranComment(t, tranId, userId);
					String msg = "In Program: TransactionServiceImpl\n"
							+ "In Method: sendExpressPayment()\n"
							+ "AC call started at: "
							+ before
							+ "\n"
							+ "AC call aborted at: "
							+ new Date(System.currentTimeMillis())
							+ "\n"
							+ "AC Method called: expressPaySend() with the transaction id of "
							+ tran.getEmgTranId() + "\n\n";
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).error(
							"send Express Payment failed.\n\n" + msg, t);
					sendAgentConnectFailMail(msg, t, userId);
					return t;
				}
				return null;
			}
		};
		// Object obj = updateTransaction(tranId, userId,
		// !isMgiTranId(tran)?command76:command1305);
		Object obj = updateTransaction(tranId, userId, command1305);
		if (obj != null) {
			throw new AgentConnectException((Throwable) obj);
		}
	}

	/*protected BpValidationRequest buildBpValidationRequest(
			AgentConnectAccessor ac, Transaction tran, String userId)
			throws Exception {
		BpValidationRequest req = new BpValidationRequest();
		req.setAmount(tran.getSndFaceAmt());
		req.setReceiveCode(tran.getRcvAgcyCode());
		DecryptionService decryptionService = emgadm.services.ServiceFactory
				.getInstance().getDecryptionService();
		String encryptedAccountNumber = tran.getRcvAgcyAcctEncryp();
		String decryptedAccountNumber = null;
		if (encryptedAccountNumber == null) { // it's always null when the
			// rcvAgcyAcctEncryp is a PAN
			decryptedAccountNumber = pciService.retrieveCardNumber(
					tran.getFormFreeConfNbr(), false);
		} else { // Not PAN, so use old decrypt method
			decryptedAccountNumber = decryptionService
					.decryptBankAccountNumber(encryptedAccountNumber);
		}
		req.setBillerAccountNumber(StringHelper.truncate(
				decryptedAccountNumber, 20));
		if (isBillerdoubleAccountNumber(ac, tran)) {
			req.setValidateAccountNumber(StringHelper.truncate(
					decryptedAccountNumber, 20));
		}
		req.setDestinationCountry("USA");
		req.setSenderFirstName(StringHelper.truncate(tran.getSndCustFrstName(),
				14));
		req.setSenderLastName(StringHelper.truncate(tran.getSndCustLastName(),
				20));
		req.setProductVariant(com.moneygram.agentconnect1305.wsclient.ProductVariant.EP);
		req.setSendCurrency(tran.getSndISOCrncyId());
		req.setReceiveCurrency(tran.getRcvISOCrncyId());
		TransactionManager tm = ManagerFactory.createTransactionManager();
		CustAddress addr = tm.getCustAddress(tran.getSndCustAddrId());
		String addrText = addr.getAddrLine1Text();
		if (!StringHelper.isNullOrEmpty(addr.getAddrLine2Text())) {
			addrText = addrText + " " + addr.getAddrLine2Text();
		}
		if (!StringHelper.isNullOrEmpty(addr.getAddrLine3Text())) {
			addrText = addrText + " " + addr.getAddrLine3Text();
		}
		req.setSenderAddress(StringHelper.truncate(addrText, 30));
		req.setSenderCity(StringHelper.truncate(addr.getAddrCityName(), 20));
		req.setSenderState(StringHelper.truncate(addr.getAddrStateName(), 2));
		req.setSenderCountry(addr.getAddrCntryId());
		req.setAccountNumberRetryCount(new BigInteger("2")); // Nissan Biller -
																// to always
																// pass a �2� in
																// the
																// transaction
																// attempt
																// counter,
																// regardless of
																// the actually
																// attempt
																// number
		req.setSenderZipCode(StringHelper.truncate(addr.getAddrPostalCode(), 10));
		ConsumerProfile profile = profileService.getConsumerProfile(
				Integer.valueOf(tran.getSndCustId()), userId, "");
		if (!StringHelper.isNullOrEmpty(profile.getPhoneNumber())) {
			req.setSenderHomePhone(StringHelper.truncate(
					profile.getPhoneNumber(), 16));
		}
		req.setMessageField1(StringHelper.truncate(tran.getSndMsg1Text(), 40));
		req.setMessageField2(StringHelper.truncate(tran.getSndMsg2Text(), 33));
		req.setSenderOccupation(tran.getSndCustOccupationText());
		req.setSenderDOB(profile.getBirthdate());
		req.setSenderLegalIdNumber(tran.getSndCustLegalId());
		if (tran.getSndCustLegalIdTypeCode() != null) {
			com.moneygram.agentconnect1305.wsclient.LegalIdType lit = com.moneygram.agentconnect1305.wsclient.LegalIdType
					.fromString(tran.getSndCustLegalIdTypeCode());
			req.setSenderLegalIdType(lit);
		}
		// 4.0.0 release UAT defect MBO-3510 - MGO does not collect ID info for
		// Bill Pay txn, so it may not required to send it to AgentConnect.
		req.setSenderPhotoIdNumber(tran.getSndCustPhotoId());
		req.setSenderPhotoIdState(tran.getSndCustPhotoIdStateCode());
		req.setSenderPhotoIdCountry(tran.getSndCustPhotoIdCntryCode());		 
		
		if (tran.getSndCustPhotoIdTypeCode() != null) {
			
			com.moneygram.agentconnect1305.wsclient.PhotoIdType pid = com.moneygram.agentconnect1305.wsclient.PhotoIdType
					.fromString(EMoneyGramAdmApplicationConstants.photoIdStringToIdType.get(tran.getSndCustPhotoIdTypeCode()));
			req.setSenderPhotoIdType(pid);
		}
		if (tran.getRcvAgentId() > 0) {
			req.setReceiveAgentID(String.valueOf(tran.getRcvAgentId()));
		}

		if (!StringUtility.isNullOrEmpty(tran.getRcvCustLastName())
				|| !StringUtility.isNullOrEmpty(tran.getRcvCustFrstName())) {
			req.setReceiverLastName(tran.getRcvCustLastName());
			req.setReceiverFirstName(tran.getRcvCustFrstName());
		}
		req.setFormFreeStaging(false);
		req.setMgiTransactionSessionID(tran.getMgiTransactionSessionID());
		if (tran.getRcvAgentId() > 0)
			req.setReceiveAgentID(tran.getRcvAgentId() + "");
		else
			req.setReceiveAgentID(null);
		req.setPrimaryReceiptLanguage("ENG");
		req.setSecondaryReceiptLanguage("SPA");
		return req;
	}*/

	// Commented as a part of UCP-340
	/*protected boolean isBillerdoubleAccountNumber(
			AgentConnectAccessor ac, Transaction tran) {
		com.moneygram.agentconnect1305.wsclient.BillerSearchResponse resp = null;
		boolean isdoubleBiller = false;
		try {
			resp = ac.billerSearch(tran.getRcvAgcyCode());
			if (resp != null) {
				BillerInfo billerDetails[] = resp.getBillerInfo();
				if (billerDetails == null || billerDetails.length > 1)
					return false;
				for (int i = 0; i < billerDetails.length; i++) {
					isdoubleBiller = billerDetails[i]
							.getDoubleAcctNumberEntryFlag().booleanValue();
				}
			}
		} catch (Exception e) {
			EMGSharedLogger
					.getLogger(this.getClass().getName().toString())
					.error("Failed to find the biller is double account or not",
							e);
			return isdoubleBiller;
		}
		return isdoubleBiller;
	}*/

	private BillPaymentSendRequest getBillPaySendRequest(Transaction tran,
			String userId) throws Exception {
		BillPaymentSendRequest req = new BillPaymentSendRequest();
		req.setAmount(tran.getSndFaceAmt());
		req.setReceiveCode(tran.getRcvAgcyCode());
		BillerLimit bl = emgshared.services.ServiceFactory.getInstance()
				.getBillerLimitService().getBillerLimit(tran.getRcvAgcyCode());
		DecryptionService decryptionService = emgadm.services.ServiceFactory
				.getInstance().getDecryptionService();
		String encryptedAccountNumber = tran.getRcvAgcyAcctEncryp();
		String decryptedAccountNumber = null;
		if (encryptedAccountNumber == null) { // it's always null when the
			// rcvAgcyAcctEncryp is a PAN
			decryptedAccountNumber = pciService.retrieveCardNumber(
					tran.getFormFreeConfNbr(), false);
		} else { // Not PAN, so use old decrypt method
			decryptedAccountNumber = decryptionService
					.decryptBankAccountNumber(encryptedAccountNumber);
		}
		req.setBillerAccountNumber(StringHelper.truncate(
				decryptedAccountNumber, 20));
		req.setReceiveCountry("USA");
		req.setSenderFirstName(StringHelper.truncate(tran.getSndCustFrstName(),
				14));
		req.setSenderLastName(StringHelper.truncate(tran.getSndCustLastName(),
				20));
		req.setProductVariant(ProductVariant.EP);
		req.setSendCurrency(tran.getSndISOCrncyId());
		req.setReceiveCurrency(tran.getRcvISOCrncyId());
		TransactionManager tm = ManagerFactory.createTransactionManager();
		CustAddress addr = tm.getCustAddress(tran.getSndCustAddrId());
		String addrText = addr.getAddrLine1Text();
		if (!StringHelper.isNullOrEmpty(addr.getAddrLine2Text())) {
			addrText = addrText + " " + addr.getAddrLine2Text();
		}
		if (!StringHelper.isNullOrEmpty(addr.getAddrLine3Text())) {
			addrText = addrText + " " + addr.getAddrLine3Text();
		}
		req.setSenderAddress(StringHelper.truncate(addrText, 30));
		req.setSenderCity(StringHelper.truncate(addr.getAddrCityName(), 20));
		req.setSenderState(StringHelper.truncate(addr.getAddrStateName(), 2));
		req.setSenderCountry(addr.getAddrCntryId());
		req.setTransactionAttemptNumber(new BigInteger("2")); // Nissan Biller -
																// to always
																// pass a �2� in
																// the
																// transaction
																// attempt
																// counter,
																// regardless of
																// the actually
																// attempt
																// number
		req.setSenderZipCode(StringHelper.truncate(addr.getAddrPostalCode(), 10));
		ConsumerProfile profile = profileService.getConsumerProfile(
				Integer.valueOf(tran.getSndCustId()), userId, "");
		if (!StringHelper.isNullOrEmpty(profile.getPhoneNumber())) {
			req.setSenderHomePhone(StringHelper.truncate(
					profile.getPhoneNumber(), 16));
		}
		req.setMessageField1(StringHelper.truncate(tran.getSndMsg1Text(), 40));
		req.setMessageField2(StringHelper.truncate(tran.getSndMsg2Text(), 33));
		req.setSenderOccupation(tran.getSndCustOccupationText());
		req.setSenderDOB(profile.getBirthdate());
		req.setSenderLegalIdNumber(tran.getSndCustLegalId());
		if (tran.getSndCustLegalIdTypeCode() != null) {
			LegalIdType lit = LegalIdType.fromString(tran
					.getSndCustLegalIdTypeCode());
			req.setSenderLegalIdType(lit);
		}
		req.setSenderPhotoIdNumber(tran.getSndCustPhotoId());
		req.setSenderPhotoIdState(tran.getSndCustPhotoIdStateCode());
		req.setSenderPhotoIdCountry(tran.getSndCustPhotoIdCntryCode());
		if (tran.getSndCustPhotoIdTypeCode() != null) {
			PhotoIdType pid = PhotoIdType.fromString(tran
					.getSndCustPhotoIdTypeCode());
			req.setSenderPhotoIdType(pid);
		}
		if (tran.getRcvAgentId() > 0) {
			req.setReceiveAgentID(String.valueOf(tran.getRcvAgentId()));
		} else {
			req.setReceiveAgentID(bl.getId());
		}
		return req;
	}

	private void sendExpressPaymentSuccessMail(Transaction tran,
			ConsumerAccountType primaryAccountType) throws Exception {
		ConsumerAccount account;
		if (primaryAccountType.isCardAccount()) {
			account = accountService.getCreditCardAccount(
					tran.getSndCustAcctId(), tran.getSndCustLogonId());
		} else {
			account = accountService.getBankAccount(tran.getSndCustAcctId(),
					tran.getSndCustLogonId());
		}
		if (account != null) {
			ConsumerProfile profile = profileService.getConsumerProfile(
					Integer.valueOf(account.getConsumerId()),
					tran.getSndCustLogonId(), "");
			if (profile != null) {
				String preferedlanguage = profile.getPreferedLanguage();
				ConsumerEmail consumerEmail = null;
				// Loop sends out email to each address.
				NotificationAccess na = new NotificationAccessImpl();
				for (Iterator iter = profile.getEmails().iterator(); iter
						.hasNext();) {
					consumerEmail = (ConsumerEmail) iter.next();
					na.notifyExpressPaymentTransaction(tran, consumerEmail,
							preferedlanguage);
				}
			}
		}
	}

	protected boolean isMgiTranId(Transaction tran) {
		if (tran.getMgiTransactionSessionID() != null
				&& !tran.getMgiTransactionSessionID().equals("")) {
			return true;
		}
		return false;
	}

	private String getReceiptTextInfo(TextTranslation[] rcptTextInfoa,
			String language) {
		if (rcptTextInfoa != null) {
			for (TextTranslation t : rcptTextInfoa) {
				if (language.equalsIgnoreCase(t.getLongLanguageCode())) {
					return t.getTextTranslation();
				}
			}
		}
		return null;

	}

	// https://mgonline.atlassian.net/browse/AUS-56- GB Group Display- AUS
	// by calling ConfigServiceSourceSite
	private static String getPaymentProvider(String sourceSite) {

		log.info("ShowCustomerProfileAction->Start of getPaymentProvider()->sourceSite:"
				+ sourceSite);
		String paymentProviderName = null;
		// com.moneygram.mgo.service.config_v3_2.NameValuePairType[] dynData =
		// (com.moneygram.mgo.service.config_v3_2.NameValuePairType[])
		// sourceSite
		// .getDynamicData();
		GetSourceSiteInfoResponse getSourceSiteInfoResponse = sourceSiteDetails(sourceSite);
		log.info(ToStringBuilder.reflectionToString(sourceSite));
		com.moneygram.mgo.service.config_v3_4.TransactionTypeInfo[] transactionTypeInfoArray = getSourceSiteInfoResponse
				.getCountryTransactionTypeInfo();
		log.info(transactionTypeInfoArray.length);
		if (transactionTypeInfoArray.length > 0) {
			ArrayList<com.moneygram.mgo.service.config_v3_4.TransactionTypeInfo> transactionTypeInfoAL = new ArrayList<TransactionTypeInfo>(
					Arrays.asList(transactionTypeInfoArray));
			for (TransactionTypeInfo transactionTypeInfo : transactionTypeInfoAL) {
				if (transactionTypeInfo.getTransactionType().toString()
						.equalsIgnoreCase("MGSEND")) {
					paymentProviderName = transactionTypeInfo.getPaymentInfo()
							.getProviderName();
					break;
				}
			}
		}
		log.info("ShowCustomerProfileAction->End of getPaymentProvider():->Payment ProviderName:"
				+ paymentProviderName);
		return paymentProviderName;
	}

	/**
	 * This method calls the cache implementation for GetSourceSiteInfo API of
	 * Configuration Service.
	 * 
	 * @param partnerSite
	 * @throws Exception
	 */
	private static GetSourceSiteInfoResponse sourceSiteDetails(
			String partnerSite) {
		EMTSharedSourceSiteCacheService proxy = EMTSharedSourceSiteCacheServiceImpl
				.instance();
		GetSourceSiteInfoResponse getSourceSiteInfoResponse = new GetSourceSiteInfoResponse();
		try {
			getSourceSiteInfoResponse = proxy.sourceSiteDetails(partnerSite);
		} catch (Exception e) {
			log.error("Exception in calling the Configuration service", e);
		}
		return getSourceSiteInfoResponse;
	}
}