/*
 * Created on Jan 6, 2005
 *
 */
package emgadm.services;

/**
 * @author A131
 *
 */
public class ServiceFactory {
	private static final ServiceFactory instance = new ServiceFactory();

	private ServiceFactory() {
	}

	public static final ServiceFactory getInstance() {
		return instance;
	}

	public DecryptionService getDecryptionService() {
		return DecryptionServiceImpl.getInstance();
	}

	public SecuredCreditCardService getSecuredCreditCardService() {
		return SecuredCreditCardService.getInstance();
	}

	public TransactionService getTransactionService() {
		return getTransactionService(null);
	}

	public TransactionService getTransactionService(String partnerSiteId) {
		return TransactionServiceBase.getInstance(partnerSiteId);
	}

	/**
	 * Temp method to replace the getInstance without the partnerSiteId parameter
	 * @param partnerSiteId
	 * @return
	 */
	public TransactionService getTransactionServiceTmp() {
		return TransactionServiceBase.getInstanceGlobalCollectFlag();
	}

	/**
	 * Get the transaction service according the paymentprovidercode
	 * @param pymtSrvsProCode
	 * @param partnerSiteId TODO
	 * @return
	 */
	public TransactionService getTransactionServicePymtProvider(Integer pymtSrvsProCode, String partnerSiteId) {
		return TransactionServiceBase.getInstanceTmp(pymtSrvsProCode,partnerSiteId);
	}

	public TransactionScoring getTransactionScoring() {
		return TransactionScoringImpl.getInstance();
	}

	public MailService getMailService() {
		return MailServiceImpl.getInstance();
	}

	public DVSService getDVSService() {
		return DVSServiceImpl.getInstance();
	}

	public SecurityService getSecurityService() {
		return SecurityServiceImpl.getInstance();
	}
	
	public ConsumerServiceProxy getConsumerServiceProxy() {
		return ConsumerServiceProxyImpl.getInstance();
	}
}
