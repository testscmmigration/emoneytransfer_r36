package emgadm.services.bankpayment;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.moneygram.bankpayment.BankPaymentService_v1.AdjustmentRequest;
import com.moneygram.bankpayment.BankPaymentService_v1.AdjustmentResponse;
import com.moneygram.bankpayment.BankPaymentService_v1.PostToAccountRequest;
import com.moneygram.bankpayment.BankPaymentService_v1.PostToAccountResponse;
import com.moneygram.bankpayment.BankPaymentService_v1.TransactionType;
import com.moneygram.bankpayment.client.BankPaymentClient;
import com.moneygram.bankpayment.common_v1.Header;
import com.moneygram.bankpayment.common_v1.ProcessingInstruction;


import emgadm.exceptions.BankPaymentServiceException;
import emgadm.property.EMTAdmContainerProperties;
import emgadm.services.ProxyException;
import emgadm.util.MerchantIdHelper;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.model.Transaction;
import emgshared.util.Constants;

public class BankPaymentProxyImpl implements BankPaymentProxy 
{
	public static final String BANK_PAYMENT_RESP_POST_ACCOUNT_SUCCESS = "OK";
	public static final String BANK_PAYMENT_RESP_ADUSTMENT_SUCCESS = "26";
	public static final String BANK_PAYMENT_ADJUSTMENT_TYPE = "R"; //R= adjustment, V=void  use 'R' for now
	
	private static final Logger log = EMGSharedLogger.getLogger(BankPaymentProxyImpl.class);
	private static final int DEFAULT_TIMEOUT = 15000;
    private BankPaymentClient bankPaymentClient = null;
    public static final String DSSEND = "DSSEND";
    //MBO-5058
    MerchantIdHelper merchant = new MerchantIdHelper();
    public static final String Cybersource ="Cybersource";
    private static final BankPaymentProxy instance =	new BankPaymentProxyImpl();

	public static final BankPaymentProxy getInstance()
	{
		return instance;
	}

	private BankPaymentProxyImpl()
	{		
	}
	
    @SuppressWarnings("unused")
	private BankPaymentClient getBankPaymentClient() throws ProxyException 
    {
        if(bankPaymentClient != null)
        {
            return bankPaymentClient;
        }
        else
        {
            synchronized (BankPaymentProxyImpl.class) {
	            try 
	            {
	               	String url = EMTAdmContainerProperties.getBankPaymentServiceUrl();
	            	String to = EMTAdmContainerProperties.getBankPaymentServiceTimeout();
	                int timeout = DEFAULT_TIMEOUT;
	                if (StringUtils.isNumeric(to)) {
	                    timeout = Integer.parseInt(to);
	                } else {
	                	EMGSharedLogger.getLogger(this.getClass().getName().toString()).warn(
						"Invalid timeout value for BankPaymentService: "+ to +". Using default value of "+ timeout);
	                }
	                bankPaymentClient = new BankPaymentClient();
	                bankPaymentClient.setWebServiceUrl(url);
	                bankPaymentClient.setTimeout(timeout);                
	                return bankPaymentClient;
	            } 
	            catch(Exception e) 
	            {
	                throw new ProxyException("Failed to create BankPaymentClient:" + e.getMessage());
	            }
            }
        }
    }

//	public AuthAccountResponse authAcct(AuthAccountRequest authAccountRequest) throws RemoteException, ProxyException {
//		AuthAccountResponse bankPaymentResponse = null;
//		try {
//			bankPaymentResponse = getBankPaymentClient()
//				.authAccount(authAccountRequest);
//			
//		} catch (Exception e) {
//			throw new ProxyException(
//					"Call to authAccount failed", e);
//		}
//		return bankPaymentResponse;
//	}
	
//	public AdjustmentResponse refundAcct(AdjustmentRequest ajustmentRequest) throws RemoteException, ProxyException {
//		AdjustmentResponse bankPaymentResponse = null;
//		try {
//			bankPaymentResponse = getBankPaymentClient()
//				.adjustment(ajustmentRequest);
//			
//		} catch (Exception e) {
//			throw new ProxyException(
//					"Call to adjustment failed", e);
//		};
//		
//		// return
//		return bankPaymentResponse;
//	}

//	public PostToAccountResponse postToAcct(PostToAccountRequest postToAccountRequest) throws ProxyException {
//		PostToAccountResponse bankPaymentResponse = null;
//		try {
//			bankPaymentResponse = getBankPaymentClient()
//				.postToAccount(postToAccountRequest);
//			
//		} catch (Exception e) {
//			throw new ProxyException(
//					"Call to postToAccount failed", e);
//		};
//		
//		// return
//		return bankPaymentResponse;
//	}

	/**
	 * Status Enquiry
	 * @throws Exception 
	 */
	/*
	 * statusInquiry 
	 */
	public StatusInquiryResponseMessage statusInquiry(StatusInquiryRequestMessage sIRM, Transaction tran) throws Exception {
		// Building the SaleRequest
		PostToAccountRequest request = new PostToAccountRequest();
		// Response
		PostToAccountResponse response = null;
		StatusInquiryResponseMessage statusResponse = null;
		
		// Header
		Header header = new Header();
			// Processing instruction
			ProcessingInstruction proc= new ProcessingInstruction();
				proc.setAction("postToAccount");
				proc.setReturnErrorsAsException(Boolean.FALSE);
				proc.setRollbackTransaction(Boolean.FALSE.booleanValue());
			//Set ProcessingInstruction to Header node
			header.setProcessingInstruction(proc);
		//Set Header
		request.setHeader(header);
		
		// clientTraceId
		request.setClientTraceId(sIRM.getClientTraceId());
		
		// processorTraceId
		request.setProcessorTraceId(sIRM.getProcessorTraceId());
		//MerchanId for DSSEND transactions for MGO and WAP
		
		request.setMerchantId(merchant.merchantIdInfo(tran.getPartnerSiteId(),
				tran.getEmgTranTypeCode(), Cybersource));
		log.info("PostToAccountRequest" + request.getMerchantId());
			
		try {
			response = getBankPaymentClient().postToAccount(request);
			
		} catch (Exception e) {
			throw e;
		}
		
		// Convert
		if (response.getError() != null) {
			String error = "Error posting to Telecheck; error code = " + response.getError().getErrorCode() + ";errormsg = " + response.getError().getErrorMessage();
			throw new BankPaymentServiceException(error);
		}
		statusResponse = new StatusInquiryResponseMessage();
		statusResponse.setAchStatus(response.getAchStatus());
		statusResponse.setApprovalCode(response.getApprovalCode());
		statusResponse.setProcessorDateTime(response.getProcessorDateTime().getTime());
		statusResponse.setProcessorTraceId(response.getProcessorTraceId());
		statusResponse.setResponseCode(response.getResponseCode());
		
		// Return
		return statusResponse;
	}

	/**
	 * Adjustment Enquiry
	 * @throws Exception 
	 */
	/*
	 * adjustmentInquiry 
	 */
	public AdjustmentInquiryResponseMessage adjustmentInquiry(AdjustmentInquiryRequestMessage sIRM, Transaction tran) throws Exception {
		// Building the SaleRequest
		AdjustmentRequest adjustmentRequest = new AdjustmentRequest();
		// Response
		AdjustmentResponse response = null;
		AdjustmentInquiryResponseMessage adjustmentResponse = null;
		
		// Header
		Header header = new Header();
			// Processing instruction
			ProcessingInstruction proc= new ProcessingInstruction();
				proc.setAction("adjustment");
				proc.setReturnErrorsAsException(Boolean.FALSE);
				proc.setRollbackTransaction(Boolean.FALSE.booleanValue());
			//Set ProcessingInstruction to Header node
			header.setProcessingInstruction(proc);
		//Set Header
		adjustmentRequest.setHeader(header);
		
		// clientTraceId
		adjustmentRequest.setClientTraceId(sIRM.getClientTraceId());
		
		// processorTraceId
		adjustmentRequest.setProcessorTraceId(sIRM.getProcessorTraceId());
		
		adjustmentRequest.setTotalAmmount(new BigDecimal(sIRM.getTotalAmmount()));
		
		//always pass 'REFUND' type 'R' - not voids
		adjustmentRequest.setTransactionType(TransactionType.REFUND);
		
		//MerchanId for DSSEND transactions for MGO
		if(Constants.MGO_PARTNER_SITE_ID.equals(tran.getPartnerSiteId()) && DSSEND.equals(tran.getEmgTranTypeCode())){
			//MBO-5058
			adjustmentRequest.setMerchantId(merchant.merchantIdInfo(tran.getPartnerSiteId(), tran.getEmgTranTypeCode(), Cybersource));
			log.debug("AdjustmentRequest" +adjustmentRequest.getMerchantId());
		}
						
		try {
			response = getBankPaymentClient().adjustment(adjustmentRequest);
			
		} catch (Exception e) {
			throw e;
		}
		
		
		// Convert
		adjustmentResponse = new AdjustmentInquiryResponseMessage();
		adjustmentResponse.setAchStatus(response.getAchStatus());
		adjustmentResponse.setProcessorDateTime(response.getProcessorDateTime().getTime());
		adjustmentResponse.setProcessorTraceId(response.getProcessorTraceId());
		adjustmentResponse.setResponseCode(response.getResponseCode());
		
		// Return
		return adjustmentResponse;
	}
}
