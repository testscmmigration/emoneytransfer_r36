package emgadm.services.bankpayment;

import java.io.Serializable;
import java.rmi.Remote;
import java.util.Date;

public class StatusInquiryResponseMessage implements Remote, Serializable {

	private static final long serialVersionUID = 1L;
    private String responseCode;
    private String achStatus;
    private String processorTraceId;
    private Date processorDateTime;
    private String approvalCode;
    private String denialRecNbr;
    
	public String getDenialRecNbr() {
		return denialRecNbr;
	}
	public void setDenialRecNbr(String denialRecNbr) {
		this.denialRecNbr = denialRecNbr;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getAchStatus() {
		return achStatus;
	}
	public void setAchStatus(String achStatus) {
		this.achStatus = achStatus;
	}
	public String getProcessorTraceId() {
		return processorTraceId;
	}
	public void setProcessorTraceId(String processorTraceId) {
		this.processorTraceId = processorTraceId;
	}
	public Date getProcessorDateTime() {
		return processorDateTime;
	}
	public void setProcessorDateTime(Date processorDateTime) {
		this.processorDateTime = processorDateTime;
	}
	public String getApprovalCode() {
		return approvalCode;
	}
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}
}
