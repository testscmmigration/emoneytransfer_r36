package emgadm.services;

import java.util.List;

import emgadm.dataaccessors.InvalidGuidException;
import emgadm.model.ApplicationCommand;
import emgadm.model.Program;
import emgadm.model.Role;
import emgadm.model.UserProfile;
import emgshared.exceptions.DataSourceException;

public interface SecurityService {
	List<ApplicationCommand> getMenuOptions(UserProfile user) throws DataSourceException;
	List<Program> getAuthorizedProgramsByUserAndCommand(UserProfile user, ApplicationCommand command);

	List<ApplicationCommand> getAllApplicationCommands() throws DataSourceException;
	List<Role> getAllRoles() throws DataSourceException, InvalidGuidException;
	List<Program> getAllPrograms() throws DataSourceException;
	List<Program> getAuthorizedProgramsByRoleAndCommand(Role role, ApplicationCommand command);	

	boolean hasPermission(UserProfile user, ApplicationCommand command, Program program);
	List<Role> getRolesByProgram(Program program) throws DataSourceException, InvalidGuidException;
}
