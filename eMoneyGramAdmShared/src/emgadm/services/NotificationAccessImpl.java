package emgadm.services;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.InitialContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.springframework.jms.core.JmsTemplate;

import com.moneygram.common.jms.JMSWriterCache;
import com.moneygram.common.jms.MessageWriter;
import com.moneygram.mgo.service.config_v3_4.GetSourceSiteInfoResponse;

import emgadm.model.Notification;
import emgadm.model.NotificationType;
import emgadm.property.EMTAdmContainerProperties;
import emgadm.services.osl.OSLException;
import emgadm.services.osl.OSLProxyImpl;
import emgadm.util.StringHelper;
import emgshared.cache.locator.EMTSharedSourceSiteCacheService;
import emgshared.cache.locator.EMTSharedSourceSiteCacheServiceImpl;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerProfile;
import emgshared.model.DeliveryOption;
import emgshared.model.Transaction;
import emgshared.model.TransactionType;
import emgshared.util.Constants;

public class NotificationAccessImpl implements NotificationAccess {
  private static final String TRANSACTION_PENDING_MORE_INFO_NEEDED = "Transaction_Pending";
	private static final String NOTIFICATION_TYPE_EMAIL = "Email";
	private static final Logger log = EMGSharedLogger.getLogger(NotificationAccessImpl.class);
	public static final String NOTIFICATIONCONNECTIONFACTORY = "CONSUMER.NOTIFICATION.MSGING.REQ.QCF";
	public static final String NOTIFICATIONQUEUE = "CONSUMER.NOTIFICATION.MSGING.REQ";
	public static final String EMTQUEUE = "EMT.TO.MGOTXP.REQ";
	public static final String EMTQCONNECTIONFACTORY = "EMT.TO.MGOTXP.REQ.QCF";
	public static final String EMTQUEUEPREFIX = "jms/";
	private static final String SOURCE_SYSTEM = "MGO";
	
	/*
	 * TODO: These vars are hard-coded,but should be dynamic depending on the
	 * affiliate, the mechanism to make them dynamic is yet to be determined.
	 * Could be in the profiles.xml document, a database table, or possibly WAS
	 * resource environment variables.
	 */
	private static final String AFF_SENDER_EMAIL_ADDRESS = "no-reply.walmartmgo@moneygram.com";

	private static final String SENDER_EMAIL_ADDRESS = "no-reply.moneygramonline@moneygram.com";
	private static final String CONSUMER_SITE_URL = "https://www.moneygram.com/moneygramonline";
	private static final String CONSUMER_SITE_URL_UK = "https://www.moneygram.co.uk";
	private static final String HELP_LINE_NUMBER = "1-800-922-7146";
	private static final String HELP_LINE_NUMBER_DE = "0-800-606-6019";
	private static final String EMAILHEADER_LOGO_LINK = "http://www.moneygram.com/html/emailHeader.gif";
	private static final String AFFILIATE_NAME = "MoneyGram";
	private static final String AFFILIATE_SITE_NAME = "MoneyGramOnline";

	//SITES INDENTIFIERS
	public static final String SITEIDENTIFIER = "MGO";
	public static final String SITEIDENTIFIER_UK = "MGOUK";
    public static final String SITEIDENTIFIER_INLANE = "INLANE";
    public static final String SITEIDENTIFIER_WAP = "WAP"; // TODO is WAP correct?
    public static final String SITEIDENTIFIER_DE = "MGODE"; // TODO is WAP correct?
    //Added for MBO-6412
    public static final String SITEIDENTIFIER_ZAF = "MGOZAF"; 
    
  //MBO-6416
    public static final String SITEIDENTIFIER_DIGICEL = "DIGICUSA";

    //DELIVERY OPTIONS
    public static final String MGSEND = "MGSEND";
	public static final String DSSEND = "DSSEND";
	public static final String AFF_10_MINUTES ="10-Minute Transfer";
	public static final String AFF_4_HOURS ="4-Hour Transfer";

	/*
	 * TODO: needs to be dynamic at some point, admin site has no access to the
	 * user locale, it'll need to be stored when we go international (with the
	 * tran or with the profile??)
	 */

	// MESSAGE TYPES
	private static final String MSG_TYPE_TRNSND = "TRNSND";
	private static final String MSG_TYPE_TRNSNP = "TRNSNP";
	private static final String MSG_TYPE_PENDTRN = "PENTRN";
	private static final String MSG_TYPE_EXPAYTRN = "EXPTRN";
	private static final String MSG_TYPE_CCFAILTRN = "CCFTRN";
	private static final String MSG_TYPE_REFP2PTRN = "RPPTRN";
	private static final String MSG_TYPE_DENIEDTRN = "DENTRN";
	private static final String MSG_TYPE_ACHRETTRN = "ACHTRN";
	private static final String MSG_TYPE_NEEDINFOTRN = "INFTRN";
	private static final String AFF_MSG_TYPE_TRNSND = "ASNDTR";
	private static final String AFF_MSG_TYPE_PENDTRN = "APENTR";
	private static final String AFF_MSG_TYPE_CCFAILTRN = "ACCFTR";
	private static final String AFF_MSG_TYPE_REFP2PTRN = "ARPPTR";
	private static final String AFF_MSG_TYPE_DENIEDTRN = "ADENTR";
	private static final String AFF_MSG_TYPE_ACHRETTRN = "AACHTR";
	private static final String AFF_MSG_TYPE_NEEDINFOTRN = "AINFTR";
	private static final String MSG_TYPE_REVIEWTRN = "TRNSUB";//MBO-1442
	private static final String MSG_TYPE_MTAAS_REVIEWTRN = "TRSUBW"; //MBO-3619
	
	//added for MBO-6163	
	private static final String DIGICEL_MSG_TYPE_NEEDINFOTRN = "INFTRD";
	private static final String DIGICEL_MSG_TYPE_DENIEDTRN = "DENTRD";
	private static final String DIGICEL_MSG_TYPE_REFP2PTRN = "RPPTRD";
	private static final String DIGICEL_MSG_TYPE_CCFAILTRN = "CCFTRD";
	//ended
	
	//added for MBO-6180 Mail trigger for JUVO Bill pay
	private static final String DIGICEL_MSG_TYPE_EXPTRD = "EXPTRD";
	//ended
	
	// Added for new MTAAS .NXT emails requirement in MBO-3619
	private static final String AFF_MSG_TYPE_TRNSND_NXT = "SNDTRW";
	private static final String AFF_MSG_TYPE_NEEDINFOTRN_NXT = "INFTRW";
	private static final String AFF_MSG_TYPE_CCFAILTRN_NXT = "CCFTRW";
	private static final String AFF_MSG_TYPE_REFP2PTRN_NXT = "RPPTRW";
	private static final String AFF_MSG_TYPE_DENIEDTRN_NXT = "DENTRW";
	private static final String MSG_TYPE_EXPAYTRN_NXT = "EXPTRW";
	
	 // ID RELATED MESSAGE TYPES
    private static final String ID_UPLOAD = "ID_UP";
    private static final String ID_PENDING_STATUS = "ID_PS";
    private static final String ID_DENIED_STATUS = "ID_DS";
    private static final String ID_APPROVED_WITH_TX = "IDAPWT";

	// MESSAGE KEYS
	private static final String MSG_KEY_CONSUMER_SITE_URL = "consumerSiteURL";
	private static final String MSG_KEY_SENDER_NAME = "senderName";
	private static final String MSG_KEY_SENDER_FIRST_NAME = "firstName";
	private static final String MSG_KEY_RECEIVER_NAME = "receiverName";
	private static final String MSG_KEY_SEND_DATE = "sendDate";
	private static final String MSG_KEY_CONTACT_NUMBER = "contactNumber";
	private static final String MSG_KEY_THREE_MINUTE_PHONE_NBR = "threeMinutePhoneNbr";
	private static final String MSG_KEY_THREE_MINUTE_PIN_NBR = "threeMinutePinNbr";
	private static final String MSG_KEY_BILLER_NAME = "billerName";
	private static final String MSG_KEY_AFFILIATE_LOGO = "affiliateLogo";
	private static final String MSG_KEY_AFFILIATE_NAME = "affiliateName";
	private static final String MSG_KEY_AFFILIATE_SITE_NAME = "affiliateSiteName";
	private static final String MSG_KEY_SEND_AMOUNT = "sendAmount";
	private static final String MSG_KEY_FUNDING_TYPE = "fundingType";
	private static final String MSG_KEY_WAP_RECEIVE_PROVINCE = "province";
	private static final String MSG_KEY_AUTH_NUMBER ="authNumber";
	private static final NumberFormat df = new DecimalFormat("#,##0.00");
	private static final String MSG_KEY_SEND_CURRENCY ="sendCurrency";//MBO-1442
	
	private static final String MSG_KEY_REF_NUMBER ="referenceNbr";
	

	//MBO-5319
	//English language code for US Transaction - taken from language_tag_master table
	private static final String TXN_LANG_ENG_TAG_ID = "en-US";
	private static final String TXN_LANG_SPA_TAG_ID = "es-US";
	
	protected static final String resourceFile = "emgadm.resources.ApplicationResources";
	private static JmsTemplate jmsTemplate=null;
	
	private static final String WAP_ENG_SMS_TEXT = "msg.wap.eng.text";
	private static final String WAP_SPA_SMS_TEXT = "msg.wap.spa.text";
	private static final String NXT_ENG_SMS_TEXT = "msg.nxt.eng.text";
	private static final String NXT_SPA_SMS_TEXT = "msg.nxt.spa.text";
	//6146
	private static final String DIGICEL_ENG_SMS_TEXT = "msg.digicel.eng.txt";
	private static final String DIGICEL_SPA_SMS_TEXT = "msg.digicel.spa.txt";
	
	private static final String NOTIFICATION_SERVICE_FUNCTION_ID = "SendNotification1607";
	private static final String NOTIFICATION_SEND_CLIENT_SYSTEM_ID = "MGO";
	private static final String NOTIFICATION_MGI_API_ID = "Unknown";
	private static final String NOTIFICATION_POE_TYPE_ID = "Unknown";
	//ended
	
	//Added for MBO-6412
	private static final String ZAF_COUNTRY_DIALINGCODE = "27";
	private static final String US_COUNTRY_DIALINGCODE = "1";
	
	//MBO-6789
	public static final int SMS_SUPPORT_TRAN_VERSION = 2;
	public static final String SMS_NOTIFY_MSG_TYPE_TRAN_SENT="Send_Money";
	public static final String SMS_NOTIFY_MSG_TYPE_PROFILE_APP="Profile_Approved";
	public static final String SMS_NOTIFY_MSG_TYPE_PROFILE_DEC="Profile_Declined";
	public static final String SMS_NOTIFICATION_TYPE ="SMS";
	
	//MBO-6990 start
	public static final String EMAIL_NOTIFICATION_TYPE ="EMAIL";
	public static final String EMAIL_NOTIFY_MSG_TYPE_TRAN_CANCEL = "Cancel_Confirmation";
	//MBO-6990 start
	
	
	private static final int[] HOME_DELIVERY_OPTIONS = {6,7,8};
	//EMT-1799
	private static final int[] ACCOUNT_DEPOSIT_OPTIONS = {9,10,19};
	private static final int[] CASH_PICKUP_DELIVERY_OPTIONS = {0,11};
	
	
	public void sendRegenEmails(Transaction transaction, String preferedLanguage, ConsumerProfile profile) {
		//notifySentTransaction method replacement for regen emails so that email can be sent to only internal email box
		//and not to sender and receiver.
		notifySender(transaction, preferedLanguage, profile);
	}
	public void notifySentTransaction(Transaction transaction,
			ConsumerEmail consumerEmail, String preferedLanguage,ConsumerProfile profile,boolean smsFlag) {
		  
		//TODO: Remove below ATP call  and add a method to send mail to sender through OSL/SALESFORCE.
		//      If sender notifications are not switching to SALESFORCE in R24, remove receiver email notifications
		//      in ATP to avoid duplicate email notifications.
		/*sendNotificationMessage(MSG_TYPE_TRNSND, transaction, consumerEmail,
				preferedLanguage);*/
		
		// Added method for Sender Email thro OSL/SALESFORCE 
		notifySender(transaction,preferedLanguage,profile);
		
		
		//Call to OSL for sender email notifications
		//TODO - Enable below method after removing ATP call and fine tune to include notification event type for send transaction
		if(null != transaction.getReceiverEmailAddress()){
			sendMailToTranReceiver(transaction, preferedLanguage, profile);
		}
						
		boolean isSMSEnabled = EMTAdmContainerProperties.isSendSMS();	
		
		log.info("SMS Enabled flag in container prop " + isSMSEnabled);
		
		//This flag will check at first only - if SMS to be sent for this partnersiteid.
		boolean isSiteEligibleForSMS = SITEIDENTIFIER_WAP.equals(transaction.getPartnerSiteId())
										|| SITEIDENTIFIER.equals(transaction.getPartnerSiteId())
										|| SITEIDENTIFIER_DIGICEL.equals(transaction.getPartnerSiteId());
		
		boolean tranAppVersion = transaction.getTranAppVersionNumber() >= SMS_SUPPORT_TRAN_VERSION;
		
		//if smsFlag flag is true then only SMS will be sent, it will be false in Regenerate Email
		//MBO6570 EMT Admin - Simple SMS message to be sent based on the opt in/opt out preference of the consumer for transactional sms
		if (isSMSEnabled && tranAppVersion && smsFlag  && isSiteEligibleForSMS
				&& ("Y").equalsIgnoreCase(profile.getNotificationSMSOptInFlag())) {
			String phoneType = null;
			phoneType = profile.getPhoneNumberType();		
			log.info("customer phone type " + phoneType);
				if(isMobilePhoneType(profile, phoneType) && transaction.getDlvrOptnId() == DeliveryOption.WILL_CALL_ID) {	
				log.info(" Transcation " + transaction.getEmgTranId() + " is of type " + transaction.getEmgTranTypeCode());
				boolean isMGTran = transaction.getEmgTranTypeCode().equals(TransactionType.MONEY_TRANSFER_SEND_CODE);
				boolean isDSTran = transaction.getEmgTranTypeCode().equals(TransactionType.DELAY_SEND_CODE);
				log.info("Is this MGSEND " + isMGTran);
				log.info("Is this DSSEND " + isDSTran);
				if(isMGTran || isDSTran) {
					log.info("SMS logic Start for " + transaction.getEmgTranId());
					log.info("transaction.getPartnerSiteId() " + transaction.getPartnerSiteId());
					log.info("transaction.getTranAppVersionNumber()  " + transaction.getTranAppVersionNumber());
					log.info("transaction.getTranLanguage() " + transaction.getTranLanguage());
					try{ //MBO-6789
							Notification smsNotification = createNotificationRequest( transaction ,SMS_NOTIFY_MSG_TYPE_TRAN_SENT  ,profile);
							OSLProxyImpl.sendNotification(smsNotification);	
							log.info(" Transcation SMS sent successfully ! , Tran Id :: "+ transaction.getEmgTranId()); // ended MBO-6789
						}catch (OSLException e) {						
							log.error("Error while sending SMS Exception",e);
						}	 
				 } else {
					 log.info("Txn is not MGSEND or DSSEND or Dlvry option is not WILL_CALL, so no SMS will be sent");
				 }				
				} else {
					log.info("Either Phone number not available or phone number type is NOT mobile for Txn# " + transaction.getEmgTranId());
				}
		}
		//ended 5319
	}
	
	// Added for EMT-3530
	private void notifySender(Transaction transaction, String preferedLanguage, ConsumerProfile profile){
		String transferSentEmailTemplate = null;
		if(ArrayUtils.contains(HOME_DELIVERY_OPTIONS,transaction.getDlvrOptnId())){
			 transferSentEmailTemplate = NotificationType.Transfer_Sent_Home_Delivery.name();
		//EMT-3538 : e-mail to be sent for CASH_PICKUP delivery option from OSL instead of ATP
		}else if(ArrayUtils.contains(CASH_PICKUP_DELIVERY_OPTIONS,transaction.getDlvrOptnId())){
			 transferSentEmailTemplate = NotificationType.Transfer_Sent_Cash_PickUp.name();				
		}else if(ArrayUtils.contains(ACCOUNT_DEPOSIT_OPTIONS, transaction.getDlvrOptnId())){
			//EMT-3551
			transferSentEmailTemplate = NotificationType.Transfer_Sent_Account_Deposit.name();
		}
		sendMailToTranSender(transferSentEmailTemplate,transaction,preferedLanguage,profile.getId());
	}
	
	private void sendMailToTranSender(String messageType,Transaction transaction,
			String preferedLanguage, int consumerId) {
		Notification emailNotificationForSender = new Notification();
		constructEmailRequestForSentTrans(transaction,
				preferedLanguage,consumerId, emailNotificationForSender);
		emailNotificationForSender.setEmailId(transaction.getSndCustLogonId());
		emailNotificationForSender.setNotificationType(messageType);		
		try {
			OSLProxyImpl.sendNotification(emailNotificationForSender);
		} catch (OSLException oslException) {
			log.warn("sendMailToTranSender - An Exception occurred during sending email notification : " + oslException.getMessage());
		}
	}

	/**
	 * sending mail to receiver based on receive options
	 * @param transaction
	 * @param preferedLanguage
	 * @param profile
	 */
	private void sendMailToTranReceiver(Transaction transaction,
			String preferedLanguage, ConsumerProfile profile) {		
		Notification emailNotificationForRec = new Notification();
		constructEmailRequestForSentTrans(transaction,
				preferedLanguage, profile.getId(), emailNotificationForRec);
		emailNotificationForRec.setEmailId(transaction.getReceiverEmailAddress());		
		if(ArrayUtils.contains(HOME_DELIVERY_OPTIONS,transaction.getDlvrOptnId())){
			emailNotificationForRec.setNotificationType(NotificationType.Receiver_Home_Delivery.name()); 							
		}else if(ArrayUtils.contains(ACCOUNT_DEPOSIT_OPTIONS,transaction.getDlvrOptnId())){    // If the delivery option is CardDeposit or AccountDeposit
			emailNotificationForRec.setNotificationType(NotificationType.Receiver_Account_Deposit.name());
		}else if(ArrayUtils.contains(CASH_PICKUP_DELIVERY_OPTIONS,transaction.getDlvrOptnId())){
			emailNotificationForRec.setNotificationType(NotificationType.Receiver_Cash_PickUp.name());
		}
		
		try {
			OSLProxyImpl.sendNotification(emailNotificationForRec);
		} catch (OSLException oslException) {
			log.warn("sendMailToTranReceiver  -  An Exception occurred during sending email notification : " + oslException.getMessage());
		}
	}
	

	private void constructEmailRequestForSentTrans(
			Transaction transaction, String preferedLanguage,
			int consumerId, Notification emailNotificationForRec) {
		emailNotificationForRec.setLanguagePreference(preferedLanguage);		
		emailNotificationForRec.setNotificationMethod(NOTIFICATION_TYPE_EMAIL);
		//EMT-3530,3538,1665 - Salesforce partner Id fixed as a part of MBO-12255
		emailNotificationForRec.setSourceSite(SITEIDENTIFIER);
		if(null != transaction.getPartnerSiteId() && (!transaction.getPartnerSiteId().startsWith(SITEIDENTIFIER))){
			emailNotificationForRec.setSourceSite(transaction.getPartnerSiteId());
		}
		emailNotificationForRec.setTranRefNum(transaction.getLgcyRefNbr());		
		emailNotificationForRec.setTranId(String.valueOf(transaction.getEmgTranId()));		
		emailNotificationForRec.setFirstName(transaction.getSndCustFrstName());
		emailNotificationForRec.setLastName(transaction.getSndCustLastName());
		emailNotificationForRec.setConsumerId(String.valueOf(consumerId));
	}

	private boolean isMobilePhoneType(ConsumerProfile profile, String phoneType) {
		return null!=profile.getPhoneNumber() && null!=phoneType && 
				phoneType.equals(ConsumerProfile.MOBILE_PHONE_TYPE);
	}

	public void notifyPendingTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		sendNotificationMessage(MSG_TYPE_PENDTRN, transaction, consumerEmail,preferedLanguage);
	}

	public void notifyExpressPaymentTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		EMGSharedLogger.getLogger(this.getClass().getName().toString()).info("Inside NotificationAccessImpl-notifyExpressPaymentTransaction");
		//1667
		sendMailToTranSender(NotificationType.Express_Payment_Processing.name(),transaction, preferedLanguage, consumerEmail.getConsumerId());
	}

	public void notifyCreditCardFailTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		Notification emailNotificationForCCFail = new Notification();
		constructEmailRequestForSentTrans(transaction, preferedLanguage, consumerEmail.getConsumerId(), emailNotificationForCCFail);
		emailNotificationForCCFail.setEmailId(consumerEmail.getConsumerEmail());
		emailNotificationForCCFail.setNotificationType(NotificationType.Card_Declined.name());
		try {
			OSLProxyImpl.sendNotification(emailNotificationForCCFail);
		} catch (OSLException e) {
			log.warn("notifyCreditCardFailTransaction:  An Exception occurred during sending email notification " + e.getMessage());
		}
	}

	public void notifyRefundPersonToPersonTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		sendNotificationMessage(MSG_TYPE_REFP2PTRN, transaction, consumerEmail,preferedLanguage);
	}

	public void notifyDeniedTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		
		sendMailToTranSender(NotificationType.Transaction_Declined.name(), transaction, preferedLanguage, transaction.getCustId());
	}

	public void notifyACHReturnTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		sendNotificationMessage(MSG_TYPE_ACHRETTRN, transaction, consumerEmail,preferedLanguage);
	}

	public void notify48hoursIdUpload(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		sendNotificationMessage(ID_UPLOAD, transaction, consumerEmail,preferedLanguage);
	}

   	public void notifyIdPendingStatus(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		sendNotificationMessage(ID_PENDING_STATUS, transaction, consumerEmail,preferedLanguage);
	}

   	public void notifyIdDeniedStatus(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		sendNotificationMessage(ID_DENIED_STATUS, transaction, consumerEmail,preferedLanguage);
	}

   	
   	public void notifyApprovedWithTX(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		sendNotificationMessage(ID_APPROVED_WITH_TX, transaction, consumerEmail,preferedLanguage);
	}

	public void notifyNeedInfoTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		Notification notification = new Notification();
		notification.setNotificationType(TRANSACTION_PENDING_MORE_INFO_NEEDED);
		notification.setConsumerId(String.valueOf(consumerEmail.getConsumerId()));
		notification.setEmailId(consumerEmail.getConsumerEmail());
		notification.setSourceSite(transaction.getPartnerSiteId());
		notification.setFirstName(transaction.getSndCustFrstName());
		notification.setLastName(transaction.getSndCustLastName());
		notification.setNotificationMethod(NOTIFICATION_TYPE_EMAIL);
		notification.setLanguagePreference(transaction.getTranLanguage());
		notification.setTranId(String.valueOf(transaction.getEmgTranId()));
		try {
			OSLProxyImpl.sendNotification(notification);
		} catch (OSLException e) {
			log.warn("notifyNeedInfoTransaction - An Exception occurred during sending email notification : " + e.getMessage());
			//FIXME: don't know if there are not subsequent steps needed! 
						//may be, user who initiated this need to know 'bout this failure? 
						//May be, system might need to retry or user need to retry? 
		}
		//sendNotificationMessage(MSG_TYPE_NEEDINFOTRN, transaction, consumerEmail,preferedLanguage);
	}

	private void sendNotificationMessage(String messageType, Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		Session session=null;
		Connection conn=null;
		
		try {
			if (useReceiptQueue(messageType, transaction)) {
				InitialContext ctx=new InitialContext();
				ConnectionFactory connFactory=(javax.jms.ConnectionFactory)ctx.lookup(EMTQUEUEPREFIX+EMTQCONNECTIONFACTORY);
				Queue queue=(Queue) ctx.lookup(EMTQUEUEPREFIX+EMTQUEUE);
				conn=connFactory.createConnection();
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).info("[sendNotificationMessage] created connection to EMTQUEUEPREFIX+EMTQUEUE");
				session=conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
				MessageProducer msgProducer=session.createProducer(queue);
				
				MapMessage mapMessage = session.createMapMessage();
                mapMessage.setLong("transactionId", transaction.getEmgTranId());
                // For guest transaction, transaction.getSndCustLogonId() will be null.so getting emailID from consumerEmail.getConsumerEmail()
                if("Y".equalsIgnoreCase(transaction.getGuestTransactionFlag().trim())){
                	mapMessage.setString("userLoginId", consumerEmail.getConsumerEmail());
                }else{
                	mapMessage.setString("userLoginId", transaction.getSndCustLogonId());
                }
                mapMessage.setString("transactionLanguageCode", preferedLanguage);
                mapMessage.setString("partnerSiteId", transaction.getPartnerSiteId());
                
                EMGSharedLogger.getLogger(this.getClass().getName().toString()).info("transaction.getSndCustId  -->" + transaction.getSndCustId());
                
                mapMessage.setInt("customerId", transaction.getSndCustId());
                EMGSharedLogger.getLogger(this.getClass().getName().toString()).info("going to send params to ATP for message type " + messageType);
                msgProducer.send(mapMessage);    
                EMGSharedLogger.getLogger(this.getClass().getName().toString()).info("Message pushed to ATP successfully");
			} else {
				StringBuilder messageBuffer = new StringBuilder();
				messageBuffer.append("<MessageInfo>");
				messageBuffer.append(getMessageType(messageType, transaction,preferedLanguage));
				messageBuffer.append(getServices(consumerEmail.getConsumerEmail(), transaction));
				messageBuffer.append(getAdmEmailContentParms(transaction));
				messageBuffer.append("</MessageInfo>");
				String message = messageBuffer.toString();
				writeMessageToQueue(message);
			}									
		} catch (Exception exception) {
			 log.error("Error sending email: " + messageType, exception);
		} finally {
			try {
				log.info("Inside finally block of sendNotificationMessage");
				if(session!=null)
				 session.close();
				if(conn!=null)
				conn.close();
			} catch (Exception e) {
				  log.error("Error closing session and connection"+ EMTQUEUEPREFIX+EMTQCONNECTIONFACTORY + "MessageType: " + messageType, e);
			}
			
		}
	}
	
	private boolean useReceiptQueue(String messageType, Transaction transaction) {

		EMGSharedLogger.getLogger(this.getClass().getName().toString()).info("Inside useReceiptQue function");
	  return EMTAdmContainerProperties.isUseReceiptQueue() &&
	      (messageType.equals(MSG_TYPE_TRNSNP) ||
	       messageType.equals(MSG_TYPE_TRNSND) ||
	       messageType.equals(MSG_TYPE_EXPAYTRN)||
	       messageType.equals(MSG_TYPE_EXPAYTRN_NXT));
    }

	private String getAdmEmailContentParms(Transaction transaction) throws Exception {
		SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM d hh:mm:ss a z yyyy");
		String tranDateFormatted = String.valueOf(dateFormat.format(transaction.getSndTranDate()));
		String senderName = buildFullName(transaction.getSndCustFrstName(), null, transaction.getSndCustLastName());
		String receiverName = buildFullName(transaction.getRcvCustFrstName(), transaction.getRcvCustMidName(), transaction
				.getRcvCustLastName());
		String siteUrl = CONSUMER_SITE_URL;
		if (transaction.getPartnerSiteId().equals(SITEIDENTIFIER_UK)) {
			siteUrl = CONSUMER_SITE_URL_UK;
		}
		String helpLineNumber = HELP_LINE_NUMBER;
		if(transaction.getPartnerSiteId().equals(SITEIDENTIFIER_DE)){
			helpLineNumber = HELP_LINE_NUMBER_DE;
		}
		StringBuilder messageBuffer = new StringBuilder();
		try {
			messageBuffer.append("<contentParameters>");
			messageBuffer.append(getContentParameter(MSG_KEY_SEND_DATE, tranDateFormatted));
			messageBuffer.append(getContentParameter(MSG_KEY_RECEIVER_NAME, receiverName));
			messageBuffer.append(getContentParameter(MSG_KEY_SENDER_NAME, senderName));
			messageBuffer.append(getContentParameter(MSG_KEY_SENDER_FIRST_NAME, transaction.getSndCustFrstName()));
			//messageBuffer.append(getContentParameter(MSG_KEY_CONSUMER_SITE_URL, CONSUMER_SITE_URL));
			messageBuffer.append(getContentParameter(MSG_KEY_CONSUMER_SITE_URL, siteUrl));
			messageBuffer.append(getContentParameter(MSG_KEY_CONTACT_NUMBER, helpLineNumber));
			if (transaction.getEmgTranTypeCode().equalsIgnoreCase(
					TransactionType.EXPRESS_PAYMENT_SEND_CODE)) {
				messageBuffer.append(getContentParameter(MSG_KEY_BILLER_NAME,transaction.getRcvAgentName()));
				messageBuffer.append(getContentParameter(MSG_KEY_RECEIVER_NAME,""));
			} else {
				messageBuffer.append(getContentParameter(MSG_KEY_BILLER_NAME,""));
			}
			messageBuffer.append(getContentParameter(MSG_KEY_AFFILIATE_LOGO, EMAILHEADER_LOGO_LINK));
			messageBuffer.append(getContentParameter(MSG_KEY_AFFILIATE_NAME, AFFILIATE_NAME));
			messageBuffer.append(getContentParameter(MSG_KEY_SEND_AMOUNT, df.format(transaction.getSndFaceAmt().doubleValue())));
			messageBuffer.append(getContentParameter(MSG_KEY_FUNDING_TYPE, getFundingType(transaction)));
			messageBuffer.append(getContentParameter(MSG_KEY_AFFILIATE_SITE_NAME, AFFILIATE_SITE_NAME));
			messageBuffer.append(getContentParameter(MSG_KEY_WAP_RECEIVE_PROVINCE, getReceiveProvince(transaction)));
			messageBuffer.append(getContentParameter(MSG_KEY_THREE_MINUTE_PHONE_NBR, transaction.getThreeMinuteFreePhoneNumber()));
			messageBuffer.append(getContentParameter(MSG_KEY_THREE_MINUTE_PIN_NBR, transaction.getThreeMinuteFreePinNumber()));
			
			if (transaction.getLgcyRefNbr() != null
					&& !transaction.getLgcyRefNbr().equals("")) {

				messageBuffer.append(getContentParameter(MSG_KEY_REF_NUMBER,String.valueOf(transaction.getLgcyRefNbr())));
				messageBuffer.append(getContentParameter(MSG_KEY_AUTH_NUMBER,""));
			} else {

				messageBuffer.append(getContentParameter(MSG_KEY_AUTH_NUMBER,String.valueOf(transaction.getEmgTranId())));
				messageBuffer.append(getContentParameter(MSG_KEY_REF_NUMBER,""));
			}

			messageBuffer.append(getContentParameter(MSG_KEY_SEND_CURRENCY, transaction.getSndISOCrncyId()));//MBO-1442

			messageBuffer.append("</contentParameters>");
		} catch (Exception e) {
			 log.error("error in getAdmEmailContentParms:" + e.getLocalizedMessage());
			throw e;
		}
		return messageBuffer.toString();
	}

	private String getReceiveProvince(Transaction tran) {
		if (tran.getRcvISOCntryCode().equals("USA")) {
			return tran.getIntndDestStateProvinceCode();
		} else if (tran.getPartnerSiteId().equals("WAP")){
			return "Puerto Rico";
		} else {
			return tran.getRcvISOCntryCode();
		}
	}

	private String getFundingType(Transaction transaction)
	{
		String mgoProductType = transaction.getEmgTranTypeDesc();
		if (isAffiliateSite(transaction))
		{
			if(mgoProductType.equals(MGSEND))
			{
				return AFF_10_MINUTES;
			}
			else if( mgoProductType.equals(DSSEND))
			{
				return AFF_4_HOURS;
			}
		}
		return mgoProductType;
	}

	private String getContentParameter(String key, String value) {
		StringBuilder contentParameter = new StringBuilder();
		contentParameter.append("<contentParameter>");
		contentParameter.append("<name>");
		contentParameter.append(key);
		contentParameter.append("</name>");
		contentParameter.append("<value>");
		contentParameter.append(value);
		contentParameter.append("</value>");
		contentParameter.append("</contentParameter>");
		return contentParameter.toString();
	}

	private boolean isAffiliateSite(Transaction transaction) {		
		//MBO-6075
		EMTSharedSourceSiteCacheService cacheService = new EMTSharedSourceSiteCacheServiceImpl();
		GetSourceSiteInfoResponse sourceSiteResponse = new GetSourceSiteInfoResponse();
		
		try {
			sourceSiteResponse = cacheService.sourceSiteDetails(transaction
					.getPartnerSiteId());
		} catch (Exception exception) {
			log.error("Exception in getting Source Site Details from Cache ", exception);
		}
		
		boolean isAffiliatedSite = (sourceSiteResponse!=null && sourceSiteResponse.getCountryInfo()!=null) ?
				sourceSiteResponse.getCountryInfo().getIsPartnerSite() : false;
		log.info("isAffiliated Site from Source response is " + isAffiliatedSite);
		//ended		
		return isAffiliatedSite;
    }

	private String getMessageType(String messageTypeCode, Transaction transaction, String preferedLanguage)
    {
    	/* TODO: MGO database doesn't store the users locale.  Once MGO goes multi-lingual or international,
    	 * the locale will need to be stored with the consumer profile or potentially with each transaction.
    	 */
       // MGOLocale currentLocale = mgoConsumer.getMGOLocale();
    	if (isAffiliateSite(transaction))
    	{
    		String oldMsgType = messageTypeCode;
    		// Added transaction parameter to get the tranAppVersion for new MTAAS .NXT emails requirement in MBO-3619
    		messageTypeCode = getAffiliateMessageType(transaction, messageTypeCode); 
    		 log.debug("Affiliate Program - Old MessageType: " + oldMsgType + ", New MessageType: " + messageTypeCode);
    	}
    	//locale should eventually be stored in DB, but for now either use en-US(MGO USA) or en-GB (MGO UK)
    	String locale = preferedLanguage;
    	String country = "USA";
    	if (transaction.getPartnerSiteId().equals(SITEIDENTIFIER_UK)) {
    		country = "GBR";
    	}
    	if (transaction.getPartnerSiteId().equals(SITEIDENTIFIER_DE)) {
    		country = "DEU";
    	}
        StringBuilder messageType = new StringBuilder();
        messageType.append("<messageType>");
            messageType.append("<type>");
            messageType.append(messageTypeCode);
            messageType.append("</type>");
            messageType.append("<subtype>1</subtype>");
            messageType.append("<sourceSystem>" + SOURCE_SYSTEM + "</sourceSystem>");
            messageType.append("<countryCode>");
            //messageType.append(currentLocale.getISO3Country());
           // messageType.append("USA");
            messageType.append( country );
            messageType.append("</countryCode>");
            messageType.append("<languageCode>");
            //messageType.append(currentLocale.getISO5Language());
            //messageType.append("en-US");
            messageType.append(locale);
            messageType.append("</languageCode>");
        messageType.append("</messageType>");
        return messageType.toString();
    }

	// Added additional logic for new MTAAS .NXT email's requirement in MBO-3619
	
	private String getAffiliateMessageType(Transaction tran, String msgType) {

		  if(Constants.DIGICEL_SITE_IDENTIFIER.equalsIgnoreCase(tran.getPartnerSiteId())) {
				log.debug("Inside NotificationAccess for Juvo site id");										
				if (msgType.equals(MSG_TYPE_DENIEDTRN)) {
					return DIGICEL_MSG_TYPE_DENIEDTRN;
				}
				if (msgType.equals(MSG_TYPE_NEEDINFOTRN)) {
					return DIGICEL_MSG_TYPE_NEEDINFOTRN;
				}				
				if (msgType.equals(MSG_TYPE_REFP2PTRN)) {
					return DIGICEL_MSG_TYPE_REFP2PTRN;
				}
				if (msgType.equals(MSG_TYPE_CCFAILTRN)) {
					return DIGICEL_MSG_TYPE_CCFAILTRN;
				}
				if (msgType.equals(MSG_TYPE_EXPAYTRN)) {//MBO-6180 Mail trigger for DIGICUSA Bill pay
					return DIGICEL_MSG_TYPE_EXPTRD;
				}
				System.out.println("Returning from Juvo with message Type " + msgType);
				return msgType;
			} else if (!Constants.WAP_PARTNER_SITE_ID.equalsIgnoreCase(tran.getPartnerSiteId())
				|| (Constants.WAP_PARTNER_SITE_ID.equalsIgnoreCase(tran.getPartnerSiteId()) && 
						tran.getTranAppVersionNumber() == Constants.VERSION_NBR_WAP)){
			if (msgType.equals(MSG_TYPE_TRNSND)) {
				return AFF_MSG_TYPE_TRNSND;
			}
			if (msgType.equals(MSG_TYPE_TRNSNP)) {
				return AFF_MSG_TYPE_TRNSND;
			}
			if (msgType.equals(MSG_TYPE_PENDTRN)) {
				return AFF_MSG_TYPE_PENDTRN;
			}
			if (msgType.equals(MSG_TYPE_CCFAILTRN)) {
				return AFF_MSG_TYPE_CCFAILTRN;
			}
			if (msgType.equals(MSG_TYPE_REFP2PTRN)) {
				return AFF_MSG_TYPE_REFP2PTRN;
			}
			if (msgType.equals(MSG_TYPE_DENIEDTRN)) {
				return AFF_MSG_TYPE_DENIEDTRN;
			}
			if (msgType.equals(MSG_TYPE_ACHRETTRN)) {
				return AFF_MSG_TYPE_ACHRETTRN;
			}
			if (msgType.equals(MSG_TYPE_NEEDINFOTRN)) {
				return AFF_MSG_TYPE_NEEDINFOTRN;
			}
			if (msgType.equals(MSG_TYPE_EXPAYTRN)) {
				return MSG_TYPE_EXPAYTRN;
			}			
			return msgType;
		} else {												
			 log.info("Enter into MTaaS Email message checks in getAffiliateMessageType method ");
			
			if (msgType.equals(MSG_TYPE_TRNSND)) {
				return AFF_MSG_TYPE_TRNSND_NXT;
			}
			if (msgType.equals(MSG_TYPE_TRNSNP)) {
				return AFF_MSG_TYPE_TRNSND_NXT;
			}
			if (msgType.equals(MSG_TYPE_CCFAILTRN)) {
				return AFF_MSG_TYPE_CCFAILTRN_NXT;
			}
			if (msgType.equals(MSG_TYPE_REFP2PTRN)) {
				return AFF_MSG_TYPE_REFP2PTRN_NXT;
			}
			if (msgType.equals(MSG_TYPE_DENIEDTRN)) {
				return AFF_MSG_TYPE_DENIEDTRN_NXT;
			}
			if (msgType.equals(MSG_TYPE_NEEDINFOTRN)) {
				return AFF_MSG_TYPE_NEEDINFOTRN_NXT;
			}
			if (msgType.equals(MSG_TYPE_EXPAYTRN)) {
				return MSG_TYPE_EXPAYTRN_NXT;
			}
			return msgType;
		}
	}

	private String getServices(String receiverEmailAddress, Transaction transaction) {
		StringBuilder services = new StringBuilder();
		services.append("<services>");
		services.append("<service type=\"E-MAIL\">");
		services.append("<sender>");
		// services.append(MGOServiceUtil.getContextResourceVariable(
		// MGOServiceUtil.SENDEREMAILADDRESS));
		if (isAffiliateSite(transaction)) {
			services.append(AFF_SENDER_EMAIL_ADDRESS);
		}
		else {
			services.append(SENDER_EMAIL_ADDRESS);
		}
		services.append("</sender>");
		services.append("<receiver>");
		services.append(receiverEmailAddress);
		services.append("</receiver>");
		services.append("</service>");
		services.append("</services>");
		return services.toString();
	}
	
	private void writeMessageToQueue(String message) throws Exception {
		if (log.isDebugEnabled()) {
			 log.debug("writeMessageToQueue: message=" + StringUtils.left(message, 1000) + "... length=" + message.length());
		}
		try {
			MessageWriter writer=null;
			writer= JMSWriterCache.getInstance().getWriter(NOTIFICATIONCONNECTIONFACTORY, NOTIFICATIONQUEUE, false);	
			
			writer.writeTextMessage(message);
		} catch (Exception e) {
			log.error("Error in writing to the Message Queue: " + e.getLocalizedMessage());
			Exception msgException = new Exception("Error in writing to the Message Queue:" + e.getLocalizedMessage());
			throw msgException;
		}
	}

	private String buildFullName(String firstName, String middleName, String lastName) {
		if (!StringHelper.isNullOrEmpty(middleName)) {
			return firstName + " " + middleName + " " + lastName;
		} else {
			return firstName + " " + lastName;
		}
	}
	
	/**
	 * Send message to Notification Service to trigger mail for Cybersource DM Review status
	 * @param
	 * 
	 * MBO-1442
	 */
	public void notifyReviewTransaction(Transaction transaction,
			ConsumerEmail consumerEmail, String preferedLanguage) {
		// Added additional logic for new MTAAS .NXT email's requirement in MBO-3619
		if (!Constants.WAP_PARTNER_SITE_ID.equalsIgnoreCase(transaction.getPartnerSiteId())
				|| (Constants.WAP_PARTNER_SITE_ID.equalsIgnoreCase(transaction.getPartnerSiteId()) && 
						transaction.getTranAppVersionNumber() == Constants.VERSION_NBR_WAP)){		
			sendNotificationMessage(MSG_TYPE_REVIEWTRN, transaction,
					consumerEmail, preferedLanguage);
		} else {			
			log.info(" This"
					+ MSG_TYPE_MTAAS_REVIEWTRN
					+ " message type will pass to trigger MTaaS email when a money transfer gets put into Manual Review while processing ");
			sendNotificationMessage(MSG_TYPE_MTAAS_REVIEWTRN, transaction,
					consumerEmail, preferedLanguage);
		}
	}

	public static JmsTemplate getJmsTemplate() {
		return jmsTemplate;
	}

	public static void setJmsTemplate(JmsTemplate jmsTemplate) {
		NotificationAccessImpl.jmsTemplate = jmsTemplate;
	}
	
	/**
	 * 
	 * @param tran
	 * @param notificationMsgType
	 * @param profile
	 * @return
	 */
	//MBO-6789 
	public Notification createNotificationRequest(Transaction tran,
			String notificationMsgType, ConsumerProfile profile) {

		Notification notification = new Notification();
		String generalphonenumber = EMTAdmContainerProperties
				.getGeneralPhoneNumber();
		

		if (null != notificationMsgType
				&& notificationMsgType.equals(SMS_NOTIFY_MSG_TYPE_TRAN_SENT)) {

			notification.setNotificationType(notificationMsgType);
			notification.setSourceSite(tran.getPartnerSiteId());
			notification.setLanguagePreference(tran.getTranLanguage());
			notification.setNotificationMethod(SMS_NOTIFICATION_TYPE);
			notification.setPhoneNumber(profile.getPhoneNumber());
			notification.setCountryDialingCode(profile.getPhnCountryCode());
			notification.setTranId(tran.getLgcyRefNbr());
		} else if (null != notificationMsgType
				&& notificationMsgType.equals(SMS_NOTIFY_MSG_TYPE_PROFILE_APP)) {

			notification.setNotificationType(notificationMsgType);
			notification.setSourceSite(profile.getPartnerSiteId());
			notification.setLanguagePreference(profile.getPreferedLanguage());
			notification.setNotificationMethod(SMS_NOTIFICATION_TYPE);
			notification.setPhoneNumber(profile.getPhoneNumber());			
			notification.setCountryDialingCode(profile.getPhnCountryCode());			
			notification.setGeneralPhoneNumber(generalphonenumber);

		} else if (null != notificationMsgType
				&& notificationMsgType.equals(SMS_NOTIFY_MSG_TYPE_PROFILE_DEC)) {

			notification.setNotificationType(notificationMsgType);
			notification.setSourceSite(profile.getPartnerSiteId());
			notification.setLanguagePreference(profile.getPreferedLanguage());
			notification.setNotificationMethod(SMS_NOTIFICATION_TYPE);
			notification.setPhoneNumber(profile.getPhoneNumber());
			notification.setCountryDialingCode(profile.getPhnCountryCode());
			notification.setGeneralPhoneNumber(generalphonenumber);
		}// MBO-6990 start
		else if (null != notificationMsgType
				&& notificationMsgType
						.equals(EMAIL_NOTIFY_MSG_TYPE_TRAN_CANCEL)) {
			notification.setNotificationType(notificationMsgType);
			notification.setNotificationMethod(EMAIL_NOTIFICATION_TYPE);
			notification.setTranId(String.valueOf(tran.getEmgTranId()));
			notification.setConsumerId(String.valueOf(tran.getCustId()));
			notification.setSourceSite(tran.getPartnerSiteId());
			notification.setLanguagePreference(tran.getTranLanguage());
			ConsumerEmail consumerEmail = null;
			for (Iterator iter = profile.getEmails().iterator(); iter.hasNext();) {
				consumerEmail = (ConsumerEmail) iter.next();
			}
			notification.setEmailId(consumerEmail.getConsumerEmail());
			notification.setFirstName(profile.getFirstName());
			notification.setLastName(profile.getLastName());
		}
		// MBO-6990 end
		
		return notification;

	} // Ended MBO-6789
}
