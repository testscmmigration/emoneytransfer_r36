
package emgadm.services;

import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerProfile;
import emgshared.model.Transaction;
public interface NotificationAccess {

	public void sendRegenEmails(Transaction transaction, String preferedLanguage, ConsumerProfile profile);
	//MBO-5319 - Modified Method signature to use Profile object, will be useful in future if any value needs to be fetched from Profile
	public void notifySentTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage,ConsumerProfile profile,boolean smsFlag);
	//ended 5319
	public void notifyPendingTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
	public void notifyExpressPaymentTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
	public void notifyCreditCardFailTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
	public void notifyRefundPersonToPersonTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
	public void notifyDeniedTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
	public void notifyACHReturnTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
	public void notifyNeedInfoTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
	public void notify48hoursIdUpload(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
   	public void notifyIdPendingStatus(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
   	public void notifyIdDeniedStatus(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
   	public void notifyApprovedWithTX(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
   	public void notifyReviewTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);

}
