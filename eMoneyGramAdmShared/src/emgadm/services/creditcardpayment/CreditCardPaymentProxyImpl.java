package emgadm.services.creditcardpayment;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


import com.moneygram.common_v1.ClientHeader;
import com.moneygram.common_v1.Header;
import com.moneygram.common_v1.ProcessingInstruction;
import com.moneygram.service.CreditCardPaymentService_v2.AuthenticationIndicator;
import com.moneygram.service.CreditCardPaymentService_v2.AuthorizationRequest;
import com.moneygram.service.CreditCardPaymentService_v2.AuthorizationResponse;
import com.moneygram.service.CreditCardPaymentService_v2.BaseCreditCardPaymentRequest;
import com.moneygram.service.CreditCardPaymentService_v2.BaseCreditCardPaymentResponse;
import com.moneygram.service.CreditCardPaymentService_v2.BusinessRules;
import com.moneygram.service.CreditCardPaymentService_v2.CCPError;
import com.moneygram.service.CreditCardPaymentService_v2.Card;
import com.moneygram.service.CreditCardPaymentService_v2.CardType;
import com.moneygram.service.CreditCardPaymentService_v2.CurrencyAmount;
import com.moneygram.service.CreditCardPaymentService_v2.FinishPaymentRequest;
import com.moneygram.service.CreditCardPaymentService_v2.FinishPaymentResponse;
import com.moneygram.service.CreditCardPaymentService_v2.GlobalCollectRequest;
import com.moneygram.service.CreditCardPaymentService_v2.PostTransactionRequest;
import com.moneygram.service.CreditCardPaymentService_v2.PostTransactionResponse;
import com.moneygram.service.CreditCardPaymentService_v2.RefundTransactionRequest;
import com.moneygram.service.CreditCardPaymentService_v2.RefundTransactionResponse;
import com.moneygram.service.CreditCardPaymentService_v2.SetPaymentRequest;
import com.moneygram.service.CreditCardPaymentService_v2.SetPaymentResponse;
import com.moneygram.service.CreditCardPaymentService_v2.StatusRequest;
import com.moneygram.service.CreditCardPaymentService_v2.StatusResponse;
import com.moneygram.service.CreditCardPaymentService_v2.client.CreditCardPaymentServiceClient;

import emgadm.property.EMTAdmContainerProperties;
import emgadm.services.ProxyException;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.model.ConsumerAddress;
import emgshared.model.ConsumerCreditCardAccount;
import emgshared.model.ConsumerProfile;
import emgshared.model.CreditCard;
import emgshared.model.Transaction;
import emgshared.property.EMTSharedContainerProperties;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.ConsumerAccountService;
import emgshared.services.ConsumerProfileService;
import emgshared.services.PCIService;

public class CreditCardPaymentProxyImpl implements CreditCardPaymentProxy {
	private static Logger logger = EMGSharedLogger.getLogger(
			CreditCardPaymentProxyImpl.class);
	// MBO-1702 - Changing action names to _v2 as a CCPS upgradation part
	public static final String GLOBAL_COLLECT_ACTION_AUTHORIZE = "authorize_v2";
	public static final String GLOBAL_COLLECT_ACTION_STATUS = "orderStatus_v2";
	public static final String GLOBAL_COLLECT_ACTION_REFUND = "refundTransaction_v2";
	public static final String GLOBAL_COLLECT_ACTION_POST = "postTransaction_v2";

	// added for 1702
	public static final String SET_PAYMENT_ACTION = "setPayment_v2";
	public static final String FINISH_PAYMENT_ACTION = "finishPayment_v2";
	// ended

	private EMTSharedDynProperties eadp = new EMTSharedDynProperties();
	private static final PCIService pciService = emgshared.services.ServiceFactory
			.getInstance().getPCIService();

	private static class SingletonContainer {
		public static final CreditCardPaymentProxy INSTANCE = new CreditCardPaymentProxyImpl();
	}

	private CreditCardPaymentProxyImpl() {
	}

	private static final int DEFAULT_TIMEOUT = 15000;
	private CreditCardPaymentServiceClient creditCardPaymentClient;

	public static CreditCardPaymentProxy getInstance() throws ProxyException {
		return SingletonContainer.INSTANCE;
	}

	private CreditCardPaymentServiceClient getCreditCardPaymentClient()
			throws ProxyException {
		if (creditCardPaymentClient != null) {
			return creditCardPaymentClient;
		} else {
			synchronized (CreditCardPaymentProxyImpl.class) {
				try {
					String url = EMTAdmContainerProperties
							.getCreditCardPaymentServiceUrl();
					String to = EMTAdmContainerProperties
							.getCreditCardPaymentServiceTimeout();
					int timeout = DEFAULT_TIMEOUT;
					if (StringUtils.isNumeric(to)) {
						timeout = Integer.parseInt(to);
					} else {
						EMGSharedLogger.getLogger(
								this.getClass().getName().toString()).warn(
								"Invalid timeout value for CreditCardPaymentService: "
										+ to + ". Using default value of "
										+ timeout);
					}
					creditCardPaymentClient = new CreditCardPaymentServiceClient(
							url, timeout);
					return creditCardPaymentClient;
				} catch (Exception e) {
					throw new ProxyException(
							"Failed to create CreditCardPaymentClient", e);
				}
			}
		}
	}

	private AuthorizationResponse authorize(AuthorizationRequest request)
			throws Exception {
		return getCreditCardPaymentClient().authorize(request);
	}

	public AuthorizationResponse authorize(Transaction transaction,
			String callerLoginId) throws Exception {

		int accountId = transaction.getSndCustAcctId();
		double amount = transaction.getSndTotAmt().doubleValue();
		String currency = transaction.getSndISOCrncyId();
		String merchantId = transaction.getMccPartnerProfileId();
		int tranId = transaction.getEmgTranId();

		emgshared.services.ServiceFactory sharedServiceFactory = emgshared.services.ServiceFactory
				.getInstance();
		ConsumerProfileService profileService = sharedServiceFactory
				.getConsumerProfileService();
		ConsumerAccountService accountService = sharedServiceFactory
				.getConsumerAccountService();

		ConsumerCreditCardAccount account = accountService
				.getCreditCardAccount(accountId, callerLoginId);
		ConsumerProfile consumerProfile = profileService.getConsumerProfile(
				account.getConsumerId(), callerLoginId, null);

		String decryptedAccountNumber = pciService.retrieveCardNumber(
				account.getId(), true);
		CreditCard creditCard = new CreditCard(decryptedAccountNumber, null,
				account.getExpireMonth(), account.getExpireYear());
		ConsumerAddress billingAddress = account.getBillingAddress();

		AuthorizationRequest request = new AuthorizationRequest();

		// MBO-1702 - New field added for Auth Call
		request.setAuthenticationIndicator(AuthenticationIndicator.value1);
		// ended
		CurrencyAmount currencyAmount = new CurrencyAmount();
		currencyAmount.setAmount(new Float(amount));
		currencyAmount.setCurrency(currency);
		request.setAmount(currencyAmount);

		BusinessRules businessRules = new BusinessRules();
		if (!eadp.isCcAVSTrans()) {
			businessRules.setDeclineAVSFlags("");
			businessRules.setIgnoreAVSResult(true);
		} else {
			businessRules.setDeclineAVSFlags(EMTSharedContainerProperties
					.getAvsDeclineFlags());
			businessRules.setIgnoreAVSResult(false);
		}
		request.setBusinessRules(businessRules);

		String cardTypeAccount = account.getCreditOrDebitCode();

		Card card = new Card();
		card.setAccountNumber(creditCard.getAccountNumber());
		// Set the card type field according the cardTypeAccount variable
		if (cardTypeAccount.equals(ConsumerCreditCardAccount.CREDIT_CODE))
			card.setCardType(CardType.credit);
		else if (cardTypeAccount.equals(ConsumerCreditCardAccount.DEBIT_CODE))
			card.setCardType(CardType.debit);
		else if (cardTypeAccount.equals(ConsumerCreditCardAccount.UNKNOWN_CODE))
			card.setCardType(CardType.unknown);

		card.setExpirationMonth(creditCard.getExpireMonth());
		card.setExpirationYear(creditCard.getExpireYear());
		card.setCardBrandType(account.getAccountType().getCode());
		card.setFullName(consumerProfile.getFirstName() + " "
				+ consumerProfile.getMiddleName() + " "
				+ consumerProfile.getLastName());
		request.setCard(card);

		Long orderId = Long.valueOf(tranId);

		request.setMerchantOrderId(orderId);

		request.setStreet1(billingAddress.getAddressLine1());
		request.setStreet2(billingAddress.getAddressLine2());
		request.setCity(billingAddress.getCity());
		request.setState(billingAddress.getState());
		request.setPostalCode(billingAddress.getPostalCode());

		request.setCountry(billingAddress.getIsoCountryCode());
		request.setCountryCode2Char(billingAddress.getIsoCountryCode()
				.substring(0, 2)); // FIXME place correct country code

		request.setCustomerId(String.valueOf(consumerProfile.getId()));
		request.setFirstName(consumerProfile.getFirstName());
		request.setLastName(consumerProfile.getLastName());

		Header header = new Header();
		ProcessingInstruction pi = new ProcessingInstruction();
		pi.setAction(GLOBAL_COLLECT_ACTION_AUTHORIZE);
		pi.setReturnErrorsAsException(true);
		header.setProcessingInstruction(pi);
		request.setHeader(header);

		request.setMerchantID(merchantId);
		request.setMGIReferenceCode(String.valueOf(tranId));

		// MBO-1702 - Authorize call needs partnersiteid mandatory now
		request.setPartnerSiteId(transaction.getPartnerSiteId());
		// ended
		request.setPerformCVVAVS(false);
		AuthorizationResponse resp = authorize(request);
		String text = null;
		try {
			logger.info("auth response text " + resp.getFormattedResponseText());
			text = CreditCardPaymentServiceClient.getGCFormattedResponseText(
					request, resp, 255);
			logger.info("auth response formatted text " + text);
		} catch (Exception e) {
			text = "ERROR formatting resp data";
			EMGSharedLogger
					.getLogger(this.getClass().getName().toString())
					.error("ERROR Formatting CCPS authorize resp data for txn comment:",
							e);
		}
		// formatting method requires request/response and request is build in
		// this lower level method. Get
		// the formatted text and store it in an unused response header field.
		if (resp.getHeader().getClientHeader() == null) {
			resp.getHeader().setClientHeader(new ClientHeader());
		}
		resp.getHeader().getClientHeader().setClientSessionID(text);

		return resp;
		// return authorize(request);
	}

	private StatusResponse status(StatusRequest request) throws Exception {
		return getCreditCardPaymentClient().status(request);
	}

	public StatusResponse status(String captureRequestId, long accountId,
			long effortId, int tranId, String merchantId, String currency)
			throws Exception {
		// Last argument, order id, is set to -1 to identify Tx as non-maestro
		return status(captureRequestId, accountId, effortId, tranId,
				merchantId, currency, -1);
	}

	public StatusResponse status(String captureRequestId, long accountId,
			long effortId, int tranId, String merchantId, String currency,
			long orderId) throws Exception {
		// Method argument orderId is '-1' when Tx is non-Maestro

		StatusRequest request = new StatusRequest();

		GlobalCollectRequest globalCollect = new GlobalCollectRequest();
		globalCollect.setEffortId((orderId == -1) ? Long.valueOf(effortId)
				: Long.valueOf(1));
		globalCollect.setOrderid((orderId == -1) ? Long.valueOf(accountId)
				: Long.valueOf(orderId));
		if (orderId != -1) {
			globalCollect.setAttemptId(Long.valueOf(1));
		}
		request.setGlobalCollect(globalCollect);

		Header header = new Header();
		ClientHeader clientHeader = new ClientHeader();
		header.setClientHeader(clientHeader);
		ProcessingInstruction processingInstruction = new ProcessingInstruction();
		processingInstruction.setAction(GLOBAL_COLLECT_ACTION_STATUS);
		processingInstruction.setReturnErrorsAsException(true);
		header.setProcessingInstruction(processingInstruction);
		request.setHeader(header);

		request.setMerchantID(merchantId);
		request.setMGIReferenceCode(String.valueOf(tranId));

		StatusResponse resp = status(request);
		String text = null;
		try {
			text = CreditCardPaymentServiceClient.getGCFormattedResponseText(
					request, resp, 255);
		} catch (Exception e) {
			text = "ERROR formatting resp data";
			EMGSharedLogger
					.getLogger(this.getClass().getName().toString())
					.error("ERROR Formatting CCPS Status resp data for txn comment:",
							e);
		}
		// formatting method requires request/response and request is build in
		// this lower level method. Get
		// the formatted text and store it in an unused response header field.
		if (resp.getHeader().getClientHeader() == null) {
			resp.getHeader().setClientHeader(new ClientHeader());
		}
		resp.getHeader().getClientHeader().setClientSessionID(text);

		return resp;
		// return status(request);
	}

	private PostTransactionResponse postTransaction(
			PostTransactionRequest request) throws Exception {
		return getCreditCardPaymentClient().postTransaction(request);
	}

	public PostTransactionResponse postTransaction(int accountId,
			double amount, String currency, String callerLoginId,
			String merchantId, long effortId, int tranId) throws Exception {
		// Last argument is set to -1 to identify Tx as non-maestro
		return postTransaction(accountId, amount, currency, callerLoginId,
				merchantId, effortId, tranId, -1);
	}

	// changing method definition from int orderId to long orderId for MBO-1704
	public PostTransactionResponse postTransaction(int accountId,
			double amount, String currency, String callerLoginId,
			String merchantId, long effortId, int tranId, long orderId)
			throws Exception {
		// Method argument orderId is '-1' when Tx is non-Maestro

		emgshared.services.ServiceFactory sharedServiceFactory = emgshared.services.ServiceFactory
				.getInstance();
		ConsumerProfileService profileService = sharedServiceFactory
				.getConsumerProfileService();
		ConsumerAccountService accountService = sharedServiceFactory
				.getConsumerAccountService();

		ConsumerCreditCardAccount account = accountService
				.getCreditCardAccount(accountId, callerLoginId);
		ConsumerProfile consumerProfile = profileService.getConsumerProfile(
				account.getConsumerId(), callerLoginId, null);

		String decryptedAccountNumber = pciService.retrieveCardNumber(
				account.getId(), true);
		CreditCard creditCard = new CreditCard(decryptedAccountNumber, null,
				account.getExpireMonth(), account.getExpireYear());
		ConsumerAddress billingAddress = account.getBillingAddress();

		PostTransactionRequest request = new PostTransactionRequest();

		CurrencyAmount currencyAmount = new CurrencyAmount();
		currencyAmount.setAmount(new Float(amount));
		currencyAmount.setCurrency(currency);

		request.setAmount(currencyAmount);

		// Card type account
		String cardTypeAccount = account.getCreditOrDebitCode();

		Card card = new Card();
		// Set the Card type enumerated vale
		card.setAccountNumber(creditCard.getAccountNumber());
		if (cardTypeAccount.equals(ConsumerCreditCardAccount.CREDIT_CODE))
			card.setCardType(CardType.credit);
		else if (cardTypeAccount.equals(ConsumerCreditCardAccount.DEBIT_CODE))
			card.setCardType(CardType.debit);
		else if (cardTypeAccount.equals(ConsumerCreditCardAccount.UNKNOWN_CODE))
			card.setCardType(CardType.unknown);

		// card.setCardType(account.getAccountType().getCode());
		card.setExpirationMonth(creditCard.getExpireMonth());
		card.setExpirationYear(creditCard.getExpireYear());
		card.setFullName(consumerProfile.getFirstName() + " "
				+ consumerProfile.getMiddleName() + " "
				+ consumerProfile.getLastName());
		card.setCardBrandType(account.getAccountType().getCode());

		request.setCard(card);

		GlobalCollectRequest globalCollect = new GlobalCollectRequest();
		// vy79
		// Apparently is the same logic but It should remain equal in case of
		// future modifications
		// for CC or Maestro that do not apply to the other product
		globalCollect.setEffortId((orderId == -1) ? Long.valueOf(effortId)
				: Long.valueOf(1));
		globalCollect.setOrderid((orderId == -1) ? Long.valueOf(tranId) : Long.valueOf(
				orderId));
		if (orderId != -1) {
			globalCollect.setAttemptId(Long.valueOf(1));
		}
		request.setGlobalCollect(globalCollect);

		Header header = new Header();
		ProcessingInstruction pi = new ProcessingInstruction();

		pi.setAction(GLOBAL_COLLECT_ACTION_POST);
		pi.setReturnErrorsAsException(true);

		header.setProcessingInstruction(pi);
		request.setHeader(header);

		request.setMerchantID(merchantId);
		request.setMGIReferenceCode(String.valueOf(tranId));

		PostTransactionResponse resp = postTransaction(request);
		String text = null;
		try {
			text = CreditCardPaymentServiceClient.getGCFormattedResponseText(
					request, resp, 255);
		} catch (Exception e) {
			text = "ERROR formatting resp data";
			EMGSharedLogger
					.getLogger(this.getClass().getName().toString())
					.error("ERROR Formatting CCPS PostTransaction resp data for txn comment:",
							e);
		}
		// formatting method requires request/response and request is build in
		// this lower level method. Get
		// the formatted text and store it in an unused response header field.
		if (resp.getHeader().getClientHeader() == null) {
			resp.getHeader().setClientHeader(new ClientHeader());
		}
		resp.getHeader().getClientHeader().setClientSessionID(text);

		return resp;
		// return postTransaction(request);
	}

	private RefundTransactionResponse refundTransaction(
			RefundTransactionRequest request) throws Exception {
		return getCreditCardPaymentClient().refundTransaction(request);
	}

	public RefundTransactionResponse refundTransaction(String captureRequestId,
			double amount, String currency, long accountId, String merchantId,
			long effortId, long tranId) throws Exception {
		// Last argument is set to -1 to identify Tx as non-maestro
		return refundTransaction(captureRequestId, amount, currency, accountId,
				merchantId, effortId, tranId, -1);
	}

	// changing method definition from int orderId to long as part of 1704
	public RefundTransactionResponse refundTransaction(String captureRequestId,
			double amount, String currency, long accountId, String merchantId,
			long effortId, long tranId, long orderId) throws Exception {
		RefundTransactionRequest request = new RefundTransactionRequest();
		// Method argument orderId is '-1' when Tx is non-Maestro
		CurrencyAmount currencyAmount = new CurrencyAmount();
		currencyAmount.setAmount(new Float(amount));
		currencyAmount.setCurrency(currency);
		request.setAmount(currencyAmount);

		GlobalCollectRequest globalCollect = new GlobalCollectRequest();
		globalCollect.setEffortId((orderId == -1) ? Long.valueOf(effortId)
				: Long.valueOf(1));
		globalCollect.setOrderid((orderId == -1) ? Long.valueOf(accountId)
				: Long.valueOf(orderId));
		if (orderId != -1) {
			globalCollect.setAttemptId(Long.valueOf(1));
		}
		request.setGlobalCollect(globalCollect);

		request.setMerchantID(merchantId);
		request.setMGIReferenceCode(String.valueOf(tranId));

		Header header = new Header();

		ClientHeader clientHeader = new ClientHeader();
		header.setClientHeader(clientHeader);
		ProcessingInstruction processingInstruction = new ProcessingInstruction();
		processingInstruction.setAction(GLOBAL_COLLECT_ACTION_REFUND);
		processingInstruction.setReturnErrorsAsException(true);
		header.setProcessingInstruction(processingInstruction);

		request.setHeader(header);

		RefundTransactionResponse resp = refundTransaction(request);
		String text = null;
		try {
			text = CreditCardPaymentServiceClient.getGCFormattedResponseText(
					request, resp, 255);
		} catch (Exception e) {
			text = "ERROR formatting resp data";
			EMGSharedLogger
					.getLogger(this.getClass().getName().toString())
					.error("ERROR Formatting CCPS RefundTransaction resp data for txn comment:",
							e);
		}
		// formatting method requires request/response and request is build in
		// this lower level method. Get
		// the formatted text and store it in an unused response header field.
		if (resp.getHeader().getClientHeader() == null) {
			resp.getHeader().setClientHeader(new ClientHeader());
		}
		resp.getHeader().getClientHeader().setClientSessionID(text);

		return resp;
		// return refundTransaction(request);
	}

	// MBO-1702
	public SetPaymentResponse doSetPayment(String merchantId, String currency,
			double amount, long orderId, long effortId, String callerLoginId,
			int accountId) throws Exception {
		SetPaymentResponse response = null;
		System.out.println("Inside CCPS Proxy Impl - doSetPayment");
		SetPaymentRequest request = new SetPaymentRequest();
		Header header = new Header();
		ClientHeader clientHeader = new ClientHeader();
		header.setClientHeader(clientHeader);
		ProcessingInstruction processingInstruction = new ProcessingInstruction();
		processingInstruction.setAction(SET_PAYMENT_ACTION);
		processingInstruction.setReturnErrorsAsException(true);
		header.setProcessingInstruction(processingInstruction);
		request.setHeader(header);

		emgshared.services.ServiceFactory sharedServiceFactory = emgshared.services.ServiceFactory
				.getInstance();
		ConsumerProfileService profileService = sharedServiceFactory
				.getConsumerProfileService();
		ConsumerAccountService accountService = sharedServiceFactory
				.getConsumerAccountService();
		ConsumerCreditCardAccount account = accountService
				.getCreditCardAccount(accountId, callerLoginId);

		String decryptedAccountNumber = pciService.retrieveCardNumber(
				account.getId(), true);
		CreditCard creditCard = new CreditCard(decryptedAccountNumber, null,
				account.getExpireMonth(), account.getExpireYear());

		ConsumerProfile consumerProfile = profileService.getConsumerProfile(
				account.getConsumerId(), callerLoginId, null);
		request.setMerchantID(merchantId);
		Card card = new Card();
		card.setFullName(consumerProfile.getFirstName() + " "
				+ consumerProfile.getLastName());
		card.setAccountNumber(creditCard.getAccountNumber());
		card.setExpirationMonth(creditCard.getExpireMonth());
		card.setExpirationYear(creditCard.getExpireYear());
		card.setCvv(creditCard.getCvv());
		card.setCardBrandType(account.getAccountType().getCode());
		String cardTypeAccount = account.getCreditOrDebitCode();

		if (cardTypeAccount.equals(ConsumerCreditCardAccount.CREDIT_CODE))
			card.setCardType(CardType.credit);
		else if (cardTypeAccount.equals(ConsumerCreditCardAccount.DEBIT_CODE))
			card.setCardType(CardType.debit);
		else if (cardTypeAccount.equals(ConsumerCreditCardAccount.UNKNOWN_CODE))
			card.setCardType(CardType.unknown);
		System.out.println("Card details set");

		CurrencyAmount cardAmount = new CurrencyAmount();
		cardAmount.setCurrency(currency);
		cardAmount.setAmount(new Float(amount));

		request.setCard(card);
		request.setAmount(cardAmount);

		GlobalCollectRequest gcReq = new GlobalCollectRequest();
		gcReq.setOrderid(orderId);
		gcReq.setEffortId(effortId);
		gcReq.setAttemptId(Long.valueOf(1));

		request.setGlobalCollect(gcReq);

		response = setPayment(request);
		String comment = "";
		/*
		 * try { text =
		 * CreditCardPaymentServiceClient.getGCFormattedResponseText(request,
		 * response, 255); System.out.println("Test response from CCPS is " +
		 * text); } catch(Exception e) { text = "ERROR formatting resp data";
		 * EMGSharedLogger
		 * .getLogger(this.getClass().getName().toString()).error(
		 * "ERROR Formatting CCPS RefundTransaction resp data for txn comment:",
		 * e); }
		 */
		// formatting method requires request/response and request is build in
		// this lower level method. Get
		// the formatted text and store it in an unused response header field.
		comment = getFormattedTextForTranComment(request, response);
		/*if (response.getHeader().getClientHeader() == null) {
			response.getHeader().setClientHeader(new ClientHeader());
		}*/
		//response.getHeader().getClientHeader().setClientSessionID(comment);
		response.setFormattedResponseText(comment);
		return response;
	}

	public SetPaymentResponse setPayment(SetPaymentRequest request)
			throws Exception {
		return getCreditCardPaymentClient().setPayment(request);
	}

	public FinishPaymentResponse doFinishPayment(String merchantId,
			long orderId, String parnerSiteId, long effortId) throws Exception {
		FinishPaymentResponse response = null;
		System.out.println("Inside CCPS Proxy Impl - doFinishPayment");
		FinishPaymentRequest request = new FinishPaymentRequest();
		Header header = new Header();
		ClientHeader clientHeader = new ClientHeader();
		header.setClientHeader(clientHeader);
		ProcessingInstruction processingInstruction = new ProcessingInstruction();
		processingInstruction.setAction(FINISH_PAYMENT_ACTION);
		processingInstruction.setReturnErrorsAsException(true);
		header.setProcessingInstruction(processingInstruction);
		request.setHeader(header);

		request.setMerchantID(merchantId);

		GlobalCollectRequest gcReq = new GlobalCollectRequest();
		gcReq.setOrderid(orderId);
		gcReq.setEffortId(effortId);
		gcReq.setAttemptId(Long.valueOf(1));

		request.setGlobalCollect(gcReq);

		request.setPartnerSiteId(parnerSiteId);

		response = finishPayment(request);
		String comment = "";

		comment = getFormattedTextForTranComment(request, response);
		response.setFormattedResponseText(comment);
		/*
		 * try { text =
		 * CreditCardPaymentServiceClient.getGCFormattedResponseText(request,
		 * response, 255); System.out.println("Test response from CCPS is " +
		 * text); } catch(Exception e) { text = "ERROR formatting resp data";
		 * EMGSharedLogger
		 * .getLogger(this.getClass().getName().toString()).error(
		 * "ERROR Formatting CCPS RefundTransaction resp data for txn comment:",
		 * e); }
		 */
		// formatting method requires request/response and request is build in
		// this lower level method. Get
		// the formatted text and store it in an unused response header field.
//		if (response.getHeader().getClientHeader() == null) {
//			response.getHeader().setClientHeader(new ClientHeader());
//		}commented for MBO-4001
//		response.getHeader().getClientHeader().setClientSessionID(text);

		return response;
	}

	public FinishPaymentResponse finishPayment(FinishPaymentRequest request)
			throws Exception {
		return getCreditCardPaymentClient().finishPayment(request);
	}

	// MBO-4001 added to build formatted comment for transaction comments
	// section for SetPayment and
	// finish payment
	/**
	 * @param BaseCreditCardPaymentRequest
	 * @param BaseCreditCardPaymentResponse
	 * 
	 */
	private static String getFormattedTextForTranComment(
			BaseCreditCardPaymentRequest baseCreditCardPaymentRequest,
			BaseCreditCardPaymentResponse baseCreditCardPaymentResponse)
			throws Exception {
		logger.debug("getFormattedTextForTranComment--->");
		StringBuilder commentBuilder = new StringBuilder();
		
		if (baseCreditCardPaymentRequest instanceof SetPaymentRequest) {
			SetPaymentRequest setPaymentRequest = (SetPaymentRequest) baseCreditCardPaymentRequest;
			commentBuilder.append(COMMENT_POSTING_DESC);
			commentBuilder.append(COMMENT_VERTICAL_LINE);
			commentBuilder.append(baseCreditCardPaymentResponse.getDecision()
					.getValue().toUpperCase());
			commentBuilder.append(appendDetail(COMMENT_ORDERID_DESC,
					Long.toString(setPaymentRequest.getGlobalCollect()
							.getOrderid())));
			commentBuilder.append(appendDetail(COMMENT_EFFORTID_DESC, Long
					.toString(setPaymentRequest.getGlobalCollect()
							.getEffortId())));
			commentBuilder.append(COMMENT_VERTICAL_LINE);
			commentBuilder.append(setPaymentRequest.getAmount().getAmount());
			commentBuilder.append(COMMENT_OPENBRACKET);
			commentBuilder.append(setPaymentRequest.getAmount().getCurrency());
			commentBuilder.append(COMMENT_CLOSEBRACKET);
		} else if (baseCreditCardPaymentRequest instanceof FinishPaymentRequest) {
			FinishPaymentRequest finishPaymentRequest = (FinishPaymentRequest) baseCreditCardPaymentRequest;
			commentBuilder.append(COMMENT_AUTH_DESC);
			commentBuilder.append(COMMENT_VERTICAL_LINE);
			commentBuilder.append(baseCreditCardPaymentResponse.getDecision()
					.getValue().toUpperCase());
			commentBuilder.append(appendDetail(COMMENT_ORDERID_DESC,Long.toString(finishPaymentRequest
					.getGlobalCollect().getOrderid())))	;
		} else {
			throw new Exception();
		}
		
		if (baseCreditCardPaymentResponse.getError() != null) {
			commentBuilder.append(appendDetail(COMMENT_ERRORCODE_DESC,
					baseCreditCardPaymentResponse.getError()
					.getErrorCode()));
			commentBuilder.append(appendDetail(COMMENT_ERRORDESCR_DESC,
					baseCreditCardPaymentResponse.getError().getErrorMessage()));			
		}else if (baseCreditCardPaymentResponse.getErrors() != null) {
			CCPError[] ccpErrors = baseCreditCardPaymentResponse.getErrors();
			if (ccpErrors.length > 0) {
				commentBuilder.append(appendDetail(COMMENT_ERRORCODE_DESC,
						ccpErrors[0].getReasonCode()));
				commentBuilder.append(appendDetail(COMMENT_ERRORDESCR_DESC,
						ccpErrors[0].getErrorMessage()));
			}
		}
	    if (commentBuilder.length() > 255) {
	    	commentBuilder.delete(255, commentBuilder.length());
	    }		    	
	    logger.debug("<----getFormattedTextForTranComment");
		return commentBuilder.toString();

	}

	// for comments of PATTERN |DESC=VALUE MBO-4001
	private static String appendDetail(String desc, String value) {
		StringBuilder builder = new StringBuilder();
		builder.append(COMMENT_VERTICAL_LINE).append(desc)
				.append(COMMENT_EQUAL).append(value);
		return builder.toString();
	}
}
