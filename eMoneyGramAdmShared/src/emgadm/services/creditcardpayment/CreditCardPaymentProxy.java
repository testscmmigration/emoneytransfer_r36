package emgadm.services.creditcardpayment;


import com.moneygram.service.CreditCardPaymentService_v2.AuthorizationResponse;
import com.moneygram.service.CreditCardPaymentService_v2.CardType;
import com.moneygram.service.CreditCardPaymentService_v2.FinishPaymentResponse;
import com.moneygram.service.CreditCardPaymentService_v2.PostTransactionResponse;
import com.moneygram.service.CreditCardPaymentService_v2.RefundTransactionResponse;
import com.moneygram.service.CreditCardPaymentService_v2.SetPaymentResponse;
import com.moneygram.service.CreditCardPaymentService_v2.StatusResponse;

import emgshared.model.Transaction;


public interface CreditCardPaymentProxy  {


	public static final String COMMENT_POSTING_DESC = "GC Posting";
	public static final String COMMENT_ORDERID_DESC = "OrderID";
	public static final String COMMENT_EFFORTID_DESC = "EffortId";
	public static final String COMMENT_AUTH_DESC = "GC Auth";
	public static final String COMMENT_VERTICAL_LINE = "\u007C"; 
	public static final String COMMENT_EQUAL = "\u003D";
	public static final String COMMENT_ERRORCODE_DESC = "ErrorCode";
	public static final String COMMENT_ERRORDESCR_DESC = "ErrorDescr";
	public static final String COMMENT_OPENBRACKET = "\u0028";
	public static final String COMMENT_CLOSEBRACKET = "\u0029";
	public AuthorizationResponse authorize(Transaction transaction, String callerLoginId) throws Exception;



	public StatusResponse status(String captureRequestId, long accountId,
			long effortId, int tranId, String merchantId, String currency) throws Exception;

	public StatusResponse status(String captureRequestId, long accountId,
	        long effortId, int tranId, String merchandId, String currency,
	        long orderId) throws Exception;

	public PostTransactionResponse postTransaction(int accountId, double amount,
			String currency, String callerLoginId,
			String merchantId, long effortId, int tranId) throws Exception;

	//changing method definition from int orderId to long orderId for MBO-1704
	public PostTransactionResponse postTransaction(int accountId, double amount,
			String currency, String callerLoginId,
			String merchantId, long effortId, int tranId, long orderId) throws Exception;

	public RefundTransactionResponse refundTransaction(String captureRequestId, double amount,
			String currency, long accountId, String merchantId, long effortId, long internalTranNumber) throws Exception;

	//changing method definiition to change orderId from int to long as part of 1704
	public RefundTransactionResponse refundTransaction(String captureRequestId, double amount,
			String currency, long accountId, String merchantId, long effortId, long internalTranNumber, long orderId) throws Exception;
	
	//MBO-1702
	public SetPaymentResponse doSetPayment(String merchantId, String currency, double amount, long orderId, long effortId, 
			String callerLoginId,int accountId) throws Exception;
	
	public FinishPaymentResponse doFinishPayment(String merchantId, long orderId, String partnerSiteId, long effortId) throws Exception;
	//ended 1702
}
