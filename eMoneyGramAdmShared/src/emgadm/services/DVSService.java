/*
 * Created on May 3, 2010
 *
 */
package emgadm.services;

public interface DVSService {

    /**
     * Validates the consumer using DVS.
     * @param 
     * @return consumer with error messages filled.
     * @throws ProxyException
     */
	//commenting since it found not used MBO-5198 
   // public Map validateConsumer(ConsumerProfile consumer) throws Exception;
}
