package emgadm.services;

import com.moneygram.mgo.service.consumer_v4_0_7_2.GetConsumerIDApprovalStatusResponse;
import com.moneygram.mgo.service.consumer_v4_0_7_2.GetConsumerIDImageResponse;
import com.moneygram.mgo.service.consumer_v4_0_7_2.GetConsumerIDUploadDynamicDataResponse;
import com.moneygram.mgo.service.consumer_v4_0_7_2.GetConsumerProfileResponse;
import com.moneygram.mgo.service.consumer_v4_0_7_2.UpdateConsumerProfileRequest;
import com.moneygram.mgo.service.consumer_v4_0_7_2.UpdateConsumerProfileResponse;



public interface ConsumerServiceProxy {
	
	public GetConsumerIDApprovalStatusResponse getConsumerIDApprovalStatusResponse(long customerId, String partnerSiteId) throws Exception;
	public GetConsumerIDImageResponse getConsumerIDImageResponse(long customerId, String partner) throws Exception;
	public UpdateConsumerProfileResponse updateConsumerProfile(
			UpdateConsumerProfileRequest updateConsumerProfileRequest,
			String parentSiteId) throws Exception;
	public boolean hasUploadedImage(String customerId,String partnerSiteId) throws Exception;
	
	
	public GetConsumerProfileResponse getConsumerProfile(Long consumerProfileId, String sourceSiteId)throws Exception;
	
	public GetConsumerIDUploadDynamicDataResponse getConsumerIDUploadData(String mgiIdentDocUploadRefId)throws Exception;
	
}
