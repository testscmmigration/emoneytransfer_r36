package emgadm.services;

import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;

import org.owasp.esapi.ESAPI;

import emgadm.dataaccessors.JNDIManager;
import emgadm.model.UserQueryCriterion;
import emgadm.property.EMTAdmContainerProperties;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;

public class RoleValidationServiceImpl implements RoleValidationService{
/*
 * Method to check permission to see the user details for logged in user
 * @param userID String.(Selecte User Id)
 * 
 * 
 * */
	public boolean checkUserAuthorize(String userID)
			throws Exception {
		boolean ret = Boolean.FALSE;
		UserQueryCriterion criteria = new UserQueryCriterion(userID, "", "", "", "", true);
		DirContext dirContext =	JNDIManager.getInitialContext(EMTAdmContainerProperties.getLDAP_USER_CONTEXT());
		try
		{
			SearchControls searchControls = new SearchControls();
			searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String filter = criteria.getLDAPFilter();
			NamingEnumeration namingEnumeration = dirContext.search(
					"",
					ESAPI.encoder().decodeForHTML(
							ESAPI.encoder().encodeForHTML(filter)),
					searchControls);// if it returns value then have permission 
			NameClassPair pair;
			if(namingEnumeration.hasMore()){
				pair = (NameClassPair) namingEnumeration.next();
				if (pair.getName() != null)	{
					ret = Boolean.TRUE;
				}
			}
		} catch (Exception e)	{
			EMGSharedLogger.getLogger(this.getClass()).error(e.getMessage(), e);
			throw new DataSourceException(e.getMessage());
		} finally {
			JNDIManager.releaseDirContext(dirContext);
		}
		return ret;
	}

	
}
