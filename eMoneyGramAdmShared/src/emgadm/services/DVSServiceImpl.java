/*
 * Created on May 3, 2010
 *
 */
package emgadm.services;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.moneygram.dvs.Consumer;
import com.moneygram.dvs.ConsumerValidationRequest;
import com.moneygram.dvs.DataValidationException;
import com.moneygram.dvs.PersonalID;
import com.moneygram.dvs.Version;
import com.moneygram.dvs.client.DVSClient;

import emgadm.property.EMTAdmContainerProperties;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.model.ConsumerProfile;
import emgshared.util.StringHelper;

/**
 * 
 * DVS Proxy Implementation.
 *<div>
 *<table>
 * <tr><th>Copyright:</th><td>Copyright (c) 2010</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2011/06/11 22:48:23 $ </td><tr><td>
 * @author   </td><td>$Author: uw25 $ </td>
 *</table>
 *</div>
 */
public class DVSServiceImpl implements DVSService {
    private static final int DEFAULT_TIMEOUT = 15000;
    private static final String DVS_SENDER_ID = "sender";
    private static final String DVS_RECEIVER_ID = "receiver";
    private static final String DVS_PROFILE_ID = "profile";

    private static DVSClient dvsClient = null;
    private static final DVSService instance =	new DVSServiceImpl();

	public static final DVSService getInstance()
	{
		return instance;
	}

	private DVSServiceImpl()
	{		
	}
	//commenting since it found not used MBO-5198 
   /* private DVSClient getDVSClient() throws Exception {
        if (dvsClient == null) {
            synchronized (DVSServiceImpl.class) {
                if (dvsClient == null) {
                    String url = null;
                    try {
                    	url = EMTAdmContainerProperties.getDVSUrl();
                    	String to = EMTAdmContainerProperties.getDVSTimeout();
                        int timeOut = DEFAULT_TIMEOUT;
                        if (StringUtils.isNumeric(to)) {
                            timeOut = Integer.parseInt(to);
                        } else {
                        	EMGSharedLogger.getLogger(this.getClass().getName().toString()).warn(
        					"Invalid timeout value for DVS: "+ to +". Using default value of "+ timeOut);
                        }
                        dvsClient = new DVSClient(url, timeOut);
                        if (EMGSharedLogger.getLogger(this.getClass()).isInfoEnabled()) {
                        	EMGSharedLogger.getLogger(this.getClass()).info("getDVSClient: created DVS client using url="+ url +" timeout="+ timeOut);
                        }
                    } catch (Exception e) {
                        throw new Exception("Failed to create DVS Client using url="+ url, e);
                    }
                }                
            }
        }
        return dvsClient;
    }

    *//**
     * Validates the consumer using DVS.
     * @param consumer
     * @return consumer with error messages filled.
     * @throws ProxyException
     */
	//commenting since it found not used MBO-5198 
	/*
    public Map validateConsumer(ConsumerProfile consumer) throws Exception {
        DVSClient client = getDVSClient();
        Map errors = new HashMap();
        //populate DVS request
        ConsumerValidationRequest request = createDVSRequest(consumer);
        try {
            client.validateConsumers(request);
        } catch (DataValidationException e) {
            //process DVS validation errors
            errors = DVSClient.processDVSException(e);
        } catch (Exception e) {
            throw new Exception("Failed to prcess DVS request", e);
        }
        return errors;
    }
*/
    private static ConsumerValidationRequest createDVSRequest(ConsumerProfile consumer) {
        ConsumerValidationRequest request = new ConsumerValidationRequest();
        request.setVersion(Version.v1);
        request.setLogGuid(consumer.getEdirGuid());
        Consumer dvsSender = null;
        if (consumer != null) {
            dvsSender = new Consumer();
            dvsSender.setValidationID( DVS_PROFILE_ID);
            dvsSender.setFirstName(consumer.getFirstName());
            dvsSender.setLastName(consumer.getLastName());
            dvsSender.setMiddleName(consumer.getMiddleName());
            dvsSender.setAddressLine1(consumer.getAddressLine1());
            dvsSender.setAddressLine2(consumer.getAddressLine2());
            dvsSender.setCity(consumer.getCity());
            dvsSender.setPostalCode(consumer.getPostalCode());
            dvsSender.setStateProvince(consumer.getState());
            dvsSender.setIsoCountryCode(consumer.getIsoCountryCode());
            dvsSender.setHomePhoneNumber(consumer.getPhoneNumber());
            if (!StringHelper.isNullOrEmpty(consumer.getPhoneNumberAlternate()))
            	dvsSender.setCellPhoneNumber(consumer.getPhoneNumberAlternate());
            dvsSender.setBirthDate(consumer.getBirthdate());

            PersonalID personalID1 = new PersonalID();
            personalID1.setPersonalIDType(DVSClient.convertPersonalIDType("SSN"));
            //Removing personal ID from DVS request as part of SSN changes
            
           /* personalID1.setPersonalIDNumber(consumer.getSsnLast4());
            dvsSender.setPersonalID1(personalID1 );*/
        }
        // receiver not populated in Admin site, only used to validate Send Profile changes at this time
        Consumer[] consumers =  new Consumer[]{dvsSender};
        request.setConsumers(consumers);
        return request;
    }
}
