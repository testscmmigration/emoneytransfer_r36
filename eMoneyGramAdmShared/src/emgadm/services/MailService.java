/*
 * Created on Jan 14, 2005
 *
 */
package emgadm.services;

/**
 * @author T349
 *
 */
public interface MailService
{

	public boolean sendTransactionMail(String emailTo,
		 String subject, String body);

}
