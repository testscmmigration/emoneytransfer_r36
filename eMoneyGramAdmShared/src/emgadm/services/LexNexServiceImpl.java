package emgadm.services;

import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date; 
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.axis.MessageContext;
import org.apache.axis.description.TypeDesc;
import org.apache.axis.encoding.SerializationContext;
import org.apache.axis.encoding.ser.BeanSerializer;
import org.apache.axis.server.AxisServer;
import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.xml.sax.helpers.AttributesImpl;


import com.moneygram.mgo.service.consumerValidationV2_1.Person;
import com.moneygram.mgo.service.consumerValidationV2_1.PersonSearchResponse;
import com.moneygram.mgo.service.consumerValidationV2_1.RelativeSearchResponse;
import com.moneygram.mgo.service.consumerValidationV2_1.RiskIndicator;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.DateConverter;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.mapper.MapperWrapper;

import emgadm.model.BPSAddress;
import emgadm.model.BPSResultUnit;
import emgadm.model.BasicPersonDetails;
import emgadm.model.PersonComparator;
import emgadm.services.consumervalidation.ConsumerValidationProxy;
import emgadm.services.consumervalidation.ConsumerValidationProxyImpl;
import emgshared.dataaccessors.ConsumerProfileDAO;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.ConsumerProfile;
import emgshared.model.LexisNexisActivity;
import emgshared.services.ConsumerProfileService;
import emgshared.util.Constants;
import emgshared.util.SSNIndicatorEnum;
import emgshared.util.SSNValidIndicatorEnum;

public class LexNexServiceImpl implements LexNexService {

	private static Logger logger = EMGSharedLogger.getLogger(
			LexNexServiceImpl.class);
	private static final LexNexService instance = new LexNexServiceImpl();
	protected static final ConsumerProfileService profileService = emgshared.services.ServiceFactory
			.getInstance().getConsumerProfileService();

	private LexNexServiceImpl() {
	}

	public static final LexNexService getInstance() {
		return instance;
	}

	/**
	 * @author aip8 Call to Consumer Validation Proxy to get Person Search
	 *         Response object and persist into DB
	 * 
	 * @param ConsumerProfile
	 *            , CustId, ipAddress
	 * @throws Exception 
	 * 
	 */
	public void getPersonSearch(ConsumerProfile consumerProfile, int custId,
			String ipAddress) throws Exception {
		ConsumerValidationProxy consumerValidationProxy = ConsumerValidationProxyImpl
				.getInstance();
		PersonSearchResponse response;
		try {
			response = consumerValidationProxy.personSearch(consumerProfile,
					ipAddress);
			LexisNexisActivity activity = new LexisNexisActivity();
			if (null != response && null != response.getPersons()) {
				/*Person person = LexNexAddressUtil.findPersonWithMatchingAddr(
				consumerProfile.getAddress(), response);*/
				Person person = response.getPersons()[0];		
				logger.debug("person" + person);
				if (null != person)
					activity = getActivityObject(person, response, custId);
			}
			activity.setCustId(custId);
			activity.setSearchType(Constants.BPS);
			insertLexisNexisActivityLog(consumerProfile, activity);
		} catch (DataSourceException e) {
			logger.error("Error while occoured in person search Impl:"
					+ e.getRootCause());
			throw new RuntimeException(e);
		}

	}

	/**
	 * @author aip8 Method to form LexisNexisActivity Object from CVS Response
	 * 
	 * @param Person
	 *            , PersonSearchResponse, custId
	 * 
	 * @return LexisNexisActivity
	 */
	public LexisNexisActivity getActivityObject(Person person,
			PersonSearchResponse response, int custId) {
		LexisNexisActivity activity = new LexisNexisActivity();
		try {
			activity.setCustId(custId);
			if (null != person.getHighRiskIndicators())
				activity.setRiskIndicator(getHighRiskIndList(person
						.getHighRiskIndicators()));
			if (null != person.getUniqueId()) {
				activity.setResponseXML(serializeResponseObject(response, true,
						true));
				activity.setUniqueId(person.getUniqueId());
			}
			if (null != person.getSsnInfo()
					&& null != person.getSsnInfo().getSsnIndicator()) {
				activity.setSsnIndicator(SSNIndicatorEnum.getDescValue(person
						.getSsnInfo().getSsnIndicator()));
			}
			if (null != person.getSsnInfo()
					&& null != person.getSsnInfo().getSsnValidityIndicator())
				activity.setSsnValidityIndCode(SSNValidIndicatorEnum.getCodeValue(person.getSsnInfo()
						.getSsnValidityIndicator()));
		} catch (Exception e) {
			throw new RuntimeException("Exception thrown while marshalling" + e);
		}
		return activity;
	}

	/**
	 * @author aip8 Method to get list of high risk indicator values from array
	 *         of RiskIndicators
	 * 
	 * @param Person
	 * @return String of High Risk Indicators
	 */
	public String getHighRiskIndList(RiskIndicator[] riskIndicatorArr) {

		StringBuilder strBuilder = new StringBuilder();
		int j = 1;
		int arrLength = riskIndicatorArr.length;
		for (int i = 0; i < arrLength; i++) {
			strBuilder.append(riskIndicatorArr[i].getCode());
			if (j < arrLength)
				strBuilder.append(",");
			j++;
		}
		return strBuilder.toString();
	}

	/**
	 * @author aip8 Method to serialize Person Search Response Object into XML
	 *         Used Axis Marshaller to serialize Java Response Object into XML
	 * @param PersonSearchResponse
	 * 
	 * @return XML String
	 */
	public String serializeResponseObject(Object responseObj,
			boolean removeNamespaces, boolean prettyPrint)
			throws SecurityException, IllegalArgumentException,
			NoSuchMethodException, IllegalAccessException,
			InvocationTargetException {
		StringWriter outStr = new StringWriter();
		TypeDesc typeDesc = getAxisTypeDesc(responseObj);
		QName qname = typeDesc.getXmlType();
		String lname = qname.getLocalPart();
		if (lname.startsWith(">") && lname.length() > 1)
			lname = lname.substring(1);
		qname = removeNamespaces ? new QName(lname) : new QName(
				qname.getNamespaceURI(), lname);
		AxisServer server = new AxisServer();
		BeanSerializer ser = new BeanSerializer(responseObj.getClass(), qname,
				typeDesc);
		SerializationContext ctx = new SerializationContext(outStr,
				new MessageContext(server));
		ctx.setSendDecl(false);
		ctx.setDoMultiRefs(false);
		ctx.setPretty(prettyPrint);
		try {
			ser.serialize(qname, new AttributesImpl(), responseObj, ctx);
		} catch (final Exception e) {
			logger.error("Error occured while serializing response object to XML"
					+ e);
		}
		String xml = outStr.toString();
		if (removeNamespaces) {
			xml = xml.replaceAll(" xmlns[:=].*?\".*?\"", "")
					.replaceAll(" xsi[:=].*\".*?\"", "")
					.replaceAll(" soapenc[:=].*?\".*?\"", "")
					.replaceAll("ns[0-9]{1}[:=]", "");
		}
//		xml = edu.stanford.nlp.util.XMLUtils.escapeAttributeXML(xml);
		return (xml);
	}

	@SuppressWarnings({ "rawtypes" })
	public static TypeDesc getAxisTypeDesc(final Object obj)
			throws SecurityException, NoSuchMethodException,
			IllegalArgumentException, IllegalAccessException,
			InvocationTargetException {
		final Class objClass = obj.getClass();
		final Method methodGetTypeDesc = objClass.getMethod("getTypeDesc",
				new Class[] {});
		final TypeDesc typeDesc = (TypeDesc) methodGetTypeDesc.invoke(obj,
				new Object[] {});
		return (typeDesc);
	}

	/**
	 * @author aip8 Method call to Consumer DAO for persisting Lexis Nexis
	 *         Activity into DB
	 * 
	 * @param ConsumerProfile
	 *            , LexisNexisActivity
	 */
	public void insertLexisNexisActivityLog(ConsumerProfile consumerProfile,
			LexisNexisActivity activity) throws DataSourceException {
		ConsumerProfileDAO dao = new ConsumerProfileDAO();
		try {
			dao.insertLexisNexisActivityLog(consumerProfile, activity);
			dao.commit();
		} catch (DataSourceException e) {
			throw e;
		} catch (Exception e) {
			dao.rollback();
			throw new EMGRuntimeException(e);
		}finally {
			dao.close();
		}
	}

	/**
	 * generation of XStream instance which avoids unnecessary tags and takes
	 * collections.
	 * 
	 * 
	 * @return XStream
	 * @throws Exception
	 */
	public static XStream getXstreamObject() throws Exception {
		XStream xStream = new XStream(new StaxDriver()) {
			@Override
			protected MapperWrapper wrapMapper(MapperWrapper next) {
				return new MapperWrapper(next) {
					@Override
					public boolean shouldSerializeMember(Class definedIn,
							String fieldName) {
						if (definedIn == Object.class) {
							return false;
						} else {
							return super.shouldSerializeMember(definedIn,
									fieldName);
						}
					}
				};
			}
		};
		try {
			String formatDefault = "yyyy-MM-dd HH:mm:ss.S z";
			String format = "yyyy-MM-dd";
			String[] formats = { format };
			xStream.registerConverter(new DateConverter(formatDefault, formats));
			xStream.alias("PersonSearchResponse",
					emgadm.model.PersonSearchResponse.class);
			xStream.aliasField("Persons",
					emgadm.model.PersonSearchResponse.class, "persons");
			xStream.alias("ssnInfo", emgadm.model.SSNinfo.class);
			xStream.alias("Persons", emgadm.model.Persons.class);
			xStream.alias("Person", emgadm.model.Person.class);
			xStream.alias("address", emgadm.model.BPSAddress.class);
			xStream.alias("phone", emgadm.model.BPSPhoneDetails.class);
			xStream.aliasField("phone", emgadm.model.Person.class,
					"phoneDetails");
			xStream.addImplicitCollection(emgadm.model.Persons.class,
					"personList", "Person", emgadm.model.Person.class);
			xStream.addImplicitCollection(emgadm.model.Person.class,
					"phoneDetails", "phone", emgadm.model.BPSPhoneDetails.class);
		} catch (Exception exception) {
			logger.error("Stream instance generation failed"
					+ exception.getMessage());
			throw new EMGRuntimeException(exception);
		}
		return xStream;
	}

	/**
	 * 
	 * to fetch the XML FROM DB and convert to objects
	 */

	public LinkedHashMap<String, BPSResultUnit> getBPSSearchResults(
			Long searchId, String userId) throws Exception {
		LinkedHashMap<String, BPSResultUnit> map = new LinkedHashMap<String, BPSResultUnit>();
		try {
			String bpsResponse = profileService
					.getBPSResponse(searchId, userId);
			// String bpsResponse = getString();
			emgadm.model.PersonSearchResponse personSearchResponse = new emgadm.model.PersonSearchResponse();

			XStream xStream = getXstreamObject();
			if (bpsResponse != null) {
				personSearchResponse = (emgadm.model.PersonSearchResponse) xStream
						.fromXML(bpsResponse);
			} else {
				throw new EMGRuntimeException();
			}
//			String bpsResponseAgain = xStream.toXML(personSearchResponse);
//			logger.info("the response fetched from DB" + bpsResponseAgain);
			List<emgadm.model.Person> personList = null != personSearchResponse
					.getPersons() ? personSearchResponse.getPersons()
					.getPersonList() : null;
			if (null != personList) {
				Collections.sort(personList, new PersonComparator());
				Collections.reverse(personList);
				LinkedHashSet<String> ssnSet = sortSSNSet(personList);
				Iterator<emgadm.model.Person> personListIterator = personList
						.iterator();
				while (personListIterator.hasNext()) {
					emgadm.model.Person person = (emgadm.model.Person) personListIterator
							.next();
					if (null != person.getAddress()) {
						person.getAddress().setFirstSeenDate(
								person.getDateFirstSeen());
						person.getAddress().setLastSeenDate(
								person.getDateLastSeen());

						if (null != person.getPhoneDetails()
								&& !person.getPhoneDetails().isEmpty()) {
							person.getAddress().setBpsPhoneDetails(
									person.getPhoneDetails());
						}
					}
					organiseBPSDisplay(map, ssnSet, person);
				}
			}
		} catch (DataSourceException exc) {
			throw new EMGRuntimeException(exc.getRootCause());
		} catch (Exception exception) {
			logger.error("handling getBPSSearchResults failed "
					+ exception.getMessage());
			throw new EMGRuntimeException(exception);

		}
		return map;
	}

	/**
	 * 
	 * in order to set the fetched details into the display beans
	 * 
	 * @param map
	 * @param ssnSet
	 * @param person
	 */
	private static void organiseBPSDisplay(
			LinkedHashMap<String, BPSResultUnit> map,
			LinkedHashSet<String> ssnSet, emgadm.model.Person person) {
		try {
			Iterator<String> ssnIterator = ssnSet.iterator();
			while (ssnIterator.hasNext()) {
				String ssnNumber = (String) ssnIterator.next();
				String parsedDob = formatDateOfBirth(person.getDob());
				emgadm.model.BasicPersonDetails basicPersonDetails = new emgadm.model.BasicPersonDetails(
						person.getFirstName(), person.getGender(),
						person.getMiddleName(), person.getLastName(),
						person.getAge(), parsedDob, person.getDeceased(),
						person.getPrefix());
				emgadm.model.BPSAddress bpsAddressForDisplay = new BPSAddress(
						person.getAddress());
				if (null != person.getSsnInfo()
						&& (person.getSsnInfo().getSsnNumber())
								.equals(ssnNumber)) {
					if (map.containsKey(person.getSsnInfo().getSsnNumber())) {
						BPSResultUnit displayBean = map.get(person.getSsnInfo()
								.getSsnNumber());
						LinkedList<BasicPersonDetails> personListInMap = displayBean
								.getPersonList();
						personListInMap.add(basicPersonDetails);
						LinkedList<emgadm.model.BPSAddress> addressListInMap = displayBean
								.getAddressList();
						addressListInMap.add(bpsAddressForDisplay);
					} else {
						LinkedList<BasicPersonDetails> personListInMap = new LinkedList<BasicPersonDetails>();
						personListInMap.add(basicPersonDetails);
						LinkedList<emgadm.model.BPSAddress> addressList = new LinkedList<emgadm.model.BPSAddress>();
						addressList.add(bpsAddressForDisplay);
						emgadm.model.SSNinfo ssnInfo = new emgadm.model.SSNinfo(
								person.getSsnInfo());
						BPSResultUnit displayBean = new BPSResultUnit(
								person.getUniqueId(), addressList,
								personListInMap, ssnInfo);
						map.put(ssnNumber, displayBean);
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("organise BPS data failed ", e);
			throw new EMGRuntimeException(e);
		}
	}

	// added tpo be used in future for formatting the Date of Birth and other
	// such date fields
	private static String formatDateOfBirth(String dateOfBirth) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
		SimpleDateFormat dateFormatInResponse = new SimpleDateFormat(
				"yyyy-MM-dd");
		Date dob = null;
		String parsedDob = "";
		if (dateOfBirth != null && !"".equals(dateOfBirth)) {
			try {
				dob = dateFormatInResponse.parse(dateOfBirth);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				logger.error("formatDateOfBirth  failed");
				throw new EMGRuntimeException(e);
			}

			if (dob != null) {
				parsedDob = sdf.format(dob);
			}
		}
		return parsedDob;
	}

	/**
	 * in order to arrange the SSN set to avoid duplicate sections
	 * 
	 * @param personList
	 * @return
	 */
	private static LinkedHashSet<String> sortSSNSet(
			List<emgadm.model.Person> personList) {
		Iterator<emgadm.model.Person> iterator1 = personList.iterator();
		LinkedHashSet<String> ssnSet = new LinkedHashSet<String>();
		while (iterator1.hasNext()) {
			emgadm.model.Person person = iterator1.next();
			ssnSet.add((null == person.getSsnInfo() || null == person.getSsnInfo().getSsnNumber()) ? "" : person
					.getSsnInfo().getSsnNumber());
		}
		return ssnSet;
	}

	/**
	 * @author aip8
	 * 
	 *         Call to Consumer Validation Proxy to get Person Search Response
	 *         object and persist into DB
	 * 
	 * @param ConsumerProfile
	 *            , CustId, ipAddress
	 * 
	 */
	public void getRelativeSearch(ConsumerProfile consumerProfile, int custId,
			String uniqueId, String ipAddress) {
		
		if(null == consumerProfile){
			logger.error("Consumer Profile cannot be null.");
			throw new EMGRuntimeException("ConsumerProfile cannot be null");
		}
		
		ConsumerValidationProxy consumerValidationProxy = ConsumerValidationProxyImpl
				.getInstance();
		RelativeSearchResponse response;
		try {

			response = consumerValidationProxy.relativeSearch(consumerProfile,
					uniqueId, ipAddress);
			LexisNexisActivity activity = new LexisNexisActivity();
			activity.setCustId(custId);
			activity.setSearchType(Constants.RLS);

			if (null != response) {
				activity.setResponseXML(serializeResponseObject(response, true,
						true));
				if (null != response.getRelatives()) {
					activity.setUniqueId(uniqueId);
				}
			}
			insertLexisNexisActivityLog(consumerProfile, activity);
		} catch (Exception e) {
			logger.error("Error occoured in relative search Impl:"
					+ e.getMessage());
			throw new RuntimeException(e);
		}
	}
	
	
	

	/**
	 * to instantiate XStream with repect to expected response xml stored in DB
	 * 
	 * @return
	 * @throws Exception
	 */
	public static XStream getXstreamRelativeSearchObject() throws Exception {
		XStream xStream = new XStream(new StaxDriver()) {
			@Override
			protected MapperWrapper wrapMapper(MapperWrapper next) {
				return new MapperWrapper(next) {
					@Override
					public boolean shouldSerializeMember(Class definedIn,
							String fieldName) {
						if (definedIn == Object.class) {
							return false;
						} else {
							return super.shouldSerializeMember(definedIn,
									fieldName);
						}
					}
				};
			}
		};
		try {
			xStream.alias("RelativeSearchResponse",
					emgadm.model.relativessearch.RelativeSearchResponse.class);
			xStream.aliasField("Relatives",
					emgadm.model.relativessearch.RelativeSearchResponse.class,
					"relatives");
			xStream.alias("Relatives",
					emgadm.model.relativessearch.Relatives.class);
			xStream.alias("Associates",
					emgadm.model.relativessearch.Associates.class);
			xStream.aliasField("Associates",
					emgadm.model.relativessearch.RelativeSearchResponse.class,
					"associates");
			// inside relatives
			xStream.addImplicitCollection(
					emgadm.model.relativessearch.Relatives.class,
					"relativesList", "Relative",
					emgadm.model.relativessearch.Relative.class);
			xStream.alias("relativeIdentity",
					emgadm.model.relativessearch.Identity.class);
			xStream.aliasField("RelativeAddresses",
					emgadm.model.relativessearch.Relative.class,
					"relativeAddresses");
			xStream.alias("RelativeAddresses",
					emgadm.model.relativessearch.RelativeAddresses.class);
			xStream.addImplicitCollection(
					emgadm.model.relativessearch.RelativeAddresses.class,
					"relativeAddressList", "RelativeAddress",
					emgadm.model.relativessearch.BaseAddress.class);
			xStream.alias("ssnInfo", emgadm.model.SSNinfo.class);
			xStream.alias("address", emgadm.model.relativessearch.Address.class);
			xStream.aliasField("Relatives",
					emgadm.model.relativessearch.Relative.class, "relatives");
			// inside associates
			xStream.addImplicitCollection(
					emgadm.model.relativessearch.Associates.class,
					"associateList", "Associate",
					emgadm.model.relativessearch.Associate.class);
			xStream.alias("associateIdentity",
					emgadm.model.relativessearch.Identity.class);
			xStream.aliasField("AssociateAddresses",
					emgadm.model.relativessearch.Associate.class,
					"associateAddresses");
			xStream.alias("AssociateAddresses",
					emgadm.model.relativessearch.AssociateAddresses.class);
			xStream.addImplicitCollection(
					emgadm.model.relativessearch.AssociateAddresses.class,
					"associateAddressList", "AssociateAddress",
					emgadm.model.relativessearch.BaseAddress.class);

		} catch (Exception exception) {
			logger.error("Stream instance generation failed"
					+ exception.getMessage());
			throw new EMGRuntimeException(exception);
		}
		return xStream;
	}

	
	/**
	 * gets the relatives search response from DB
	 * 
	 * @param searchId
	 * @param userId
	 */
	public emgadm.model.relativessearch.RelativeSearchResponse getRelativesSearchResults(
			Long searchId, String userId) throws Exception {
		emgadm.model.relativessearch.RelativeSearchResponse relativeSearchResponse = null;
		
		String responseXML="";
		try {
			responseXML = profileService.getBPSResponse(searchId, userId);
		} catch (DataSourceException e) {
			// TODO Auto-generated catch block
			logger.error("Error occurred during relative search "+e);
		}
		logger.debug(ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTML(responseXML)));
		XStream xStream;
		if (!"".equals(responseXML)) {
			try {
				xStream = getXstreamRelativeSearchObject();
				relativeSearchResponse = (emgadm.model.relativessearch.RelativeSearchResponse) xStream
						.fromXML(responseXML);
			} catch (Exception exception) {
				throw new EMGRuntimeException(exception);
			}
		}
		return relativeSearchResponse;
	}
}
