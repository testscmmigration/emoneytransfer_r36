package emgadm.services.processing;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.util.MessageResources;

import emgadm.dataaccessors.ManagerFactory;
import emgadm.dataaccessors.TransactionManager;
import emgadm.dataaccessors.UserProfileManager;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.model.UserProfile;
import emgadm.model.UserQueryCriterion;
import emgadm.property.EMTAdmAppProperties;
import emgadm.services.NotificationAccess;
import emgadm.services.NotificationAccessImpl;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.AdminMessage;
import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerProfile;
import emgshared.model.Transaction;
import emgshared.services.ConsumerProfileService;
import emgshared.services.MessageService;

public abstract class VendorOperation {

	protected static final HashSet<Integer> transactionsInProcess = new HashSet<Integer>();
	protected TransactionManager transactionManager = ManagerFactory.createTransactionManager();


	public abstract Transaction execute(int tranId, String userId, String contextRoot) throws TransactionAlreadyInProcessException, TransactionOwnershipException;

	public abstract boolean isTransactionStatusValid(Transaction transaction);

	protected Transaction updateTransaction(int transId, String userId, UpdateTransactionCommand command)
	throws TransactionAlreadyInProcessException, TransactionOwnershipException {
		return updateTransaction(transId, userId, command, true, true);
	}

	protected Transaction updateTransaction(int tranId, String userId, UpdateTransactionCommand command, boolean verifyOwnership,
			boolean verifyStrictOwnership) throws TransactionAlreadyInProcessException, TransactionOwnershipException {
		Integer transIdKey = Integer.valueOf(tranId);
		synchronized (transactionsInProcess) {
			if (transactionsInProcess.contains(transIdKey)) {
				String currentOwner;
				try {
					currentOwner = getCurrentOwner(tranId);
				} catch (Exception exception) {
					currentOwner = "unavailable";
					throw new TransactionAlreadyInProcessException("transaction is already in process", currentOwner, exception);
				}
				throw new TransactionAlreadyInProcessException("transaction is already in process", currentOwner);
			}
			transactionsInProcess.add(transIdKey);
		}
		try {
			if (verifyOwnership) {
				String currentOwner = getCurrentOwner(tranId);
				boolean owned = StringUtils.isNotBlank(currentOwner);
				boolean ownerMatch = StringUtils.equalsIgnoreCase(userId, currentOwner);
				if (verifyStrictOwnership) {
					if (!owned  || !ownerMatch ) {
						throw new TransactionOwnershipException("Transaction is owned by " + currentOwner, currentOwner);
					}
				} else {
					if (owned  && !ownerMatch ) {
						throw new TransactionOwnershipException("Transaction is owned by " + currentOwner, currentOwner);
					}
				}
			}
			return (Transaction) command.execute();
		} finally {
			synchronized (transactionsInProcess) {
				transactionsInProcess.remove(transIdKey);
			}
		}
	}

	protected String getCurrentOwner(int tranId) {
		Transaction tran = transactionManager.getTransaction(tranId);
		return (tran == null ? "" : tran.getCsrPrcsUserid());
	}

	protected void sendCreditCardFailEmail(Transaction tran) throws Exception {
		ConsumerProfileService profileService = emgshared.services.ServiceFactory.getInstance().getConsumerProfileService();
		ConsumerProfile profile = profileService.getConsumerProfile(Integer.valueOf(tran.getCustId()), tran.getSndCustLogonId(), "");
		if (profile != null) {
			String preferedlanguage = profile.getPreferedLanguage();
			ConsumerEmail consumerEmail = null;
			// Loop sends out email to each address.
			NotificationAccess na = new NotificationAccessImpl();
			for (Iterator iter = profile.getEmails().iterator(); iter.hasNext();) {
				consumerEmail = (ConsumerEmail) iter.next();
				na.notifyCreditCardFailTransaction(tran, consumerEmail,preferedlanguage);
			}
		}
	}

	protected String getMessageResource(String key, String[] params) {
		Locale locale = Locale.getDefault();
		MessageResources msgRes = MessageResources.getMessageResources("emgadm.resources.ApplicationResources");
		MessageFormat formatter = new MessageFormat(msgRes.getMessage(key));
		formatter.setLocale(locale);
		return formatter.format(params);
	}

	protected void sendAdminMessage(String processUserId, String message) {
		MessageService messageService = emgshared.services.ServiceFactory.getInstance().getMessageService();
		AdminMessage amb = new AdminMessage();
		amb.setAdminMsgTypeCode("System Error");
		amb.setMsgPrtyCode("9");
		amb.setMsgBegDate(new Date(System.currentTimeMillis()));
		amb.setMsgThruDate(new Date(System.currentTimeMillis() + 24L * 60L * 60L * 1000L));
		amb.setMsgText(message);
		String userPrefix = EMTAdmAppProperties.getSystemUserIdPrefix();
		List userList = new ArrayList();
		// if it's a background processor, send message to all users
		// for the role specified; otherwise, just the processing user
		if (processUserId.startsWith(userPrefix)) {
			UserProfileManager upm = ManagerFactory.createUserManager();
			UserQueryCriterion criteria = new UserQueryCriterion();
			// currently, the role id is 'Manager'
			criteria.setRoleId(EMTAdmAppProperties.getRoleIdMsgSendTo());
			Iterator iter;
			try {
				iter = upm.findHollowUsers(criteria).iterator();
			} catch (DataSourceException e) {
				throw new EMGRuntimeException(e);
			}
			while (iter.hasNext()) {
				UserProfile user = (UserProfile) iter.next();
				userList.add(user.getUID());
			}
		} else {
			userList.add(processUserId);
		}
		String[] users = new String[userList.size()];
		int i = 0;
		Iterator iter = userList.iterator();
		while (iter.hasNext()) {
			users[i++] = (String) iter.next();
		}
		try {
			messageService.insertAdminMessage(processUserId, amb, users);
		} catch (DataSourceException e) {
			// just catch this and log it, just a message failure
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Error Inserting Admin Message:", e);
		}
	}


}
