package emgadm.services.processing;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


import com.moneygram.service.CreditCardPaymentService_v2.FinishPaymentResponse;
import com.moneygram.service.CreditCardPaymentService_v2.AuthorizationResponse;
import com.moneygram.service.CreditCardPaymentService_v2.CardType;
import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.services.creditcardpayment.CreditCardPaymentProxyImpl;
import emgadm.sysmon.EventListConstant;
import emgadm.util.MerchantIdHelper;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.BillerLimit;
import emgshared.model.ConsumerCreditCardAccount;
import emgshared.model.Transaction;
import emgshared.model.TransactionStatus;
import emgshared.model.TransactionType;
import emgshared.property.EMTSharedContainerProperties;
import emgshared.services.CommentService;
import emgshared.services.ConsumerAccountService;
import emgshared.services.CreditCardServiceResponse;
import emgshared.services.CreditCardServiceResponseImpl;
import eventmon.eventmonitor.EventMonitor;

public class GlobalCollectAuthorize extends VendorOperation {
	
	//MBO-1702 : CCPS V2 dont have Decision class
	private final String decision_accept = "accept";
	private final String decision_reject = "reject";
	private static Logger logger = EMGSharedLogger.getLogger(GlobalCollectAuthorize.class);
	//ended
	//MBO-5058
	private final String GC= "Global Collect";
	public Transaction execute(final int tranId, final String userId, final String contextRoot)
	throws TransactionAlreadyInProcessException, TransactionOwnershipException {

		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Transaction execute(){
				Transaction tran = transactionManager.getTransaction(tranId);
				/*
				 * Start definitions Card type identify process
				 */
				int accountId = tran.getSndCustAcctId();
				String callerLoginId = userId;
				emgshared.services.ServiceFactory sharedServiceFactory = emgshared.services.ServiceFactory.getInstance();
				ConsumerAccountService accountService = sharedServiceFactory.getConsumerAccountService();
				ConsumerCreditCardAccount account = null;
				try {
					account = accountService.getCreditCardAccount(accountId, callerLoginId);
				} catch (DataSourceException e1) {
					// TODO Handle Correct Exception
				}

				String cardTypeAccount = account.getCreditOrDebitCode();
				/*
				 * Finish definions for Card type identify process
				 */
				if(isTransactionStatusValid(tran)) {

				String TRANSACTION_DETAIL_URL = contextRoot + "/transactionDetail.do";
				double amount = tran.getSndTotAmt().doubleValue();
				tran = setMerchantId(tran);
				AuthorizationResponse authorizationResponse = null;
				long beginTime = System.currentTimeMillis();
				//MBO-1702
				if (tran.getTranAppVersionNumber()<2.00) {
				//ended
				try {
					authorizationResponse = CreditCardPaymentProxyImpl.getInstance().authorize(tran, userId);
					CardType cardTypeResp = authorizationResponse.getCardType();
					//Try to update the card type field on data base
					if(cardTypeAccount.equals(ConsumerCreditCardAccount.UNKNOWN_CODE) &&
							(cardTypeResp.equals(CardType.debit) || cardTypeResp.equals(CardType.credit))){

						accountService.updateCreditCardAccountType(userId, account.getId(),
								account.getAccountType().getCode(), getCardTypeCode(cardTypeResp));
					}
				} catch (Exception e) {
					throw new EMGRuntimeException(e);
				}

				CreditCardServiceResponse serviceResponse = constructServiceResponse(authorizationResponse);

				EventMonitor.getInstance().recordEvent(EventListConstant.CREDIT_CARD_AUTH,
						beginTime, System.currentTimeMillis());

				CommentService commentService = emgshared.services.ServiceFactory.getInstance().getCommentService();

				commentService.insertTransactionComment(tran.getEmgTranId(), userId,
						(serviceResponse != null) ? serviceResponse.getProcessorFormattedRespText() : "",
								EMoneyGramAdmApplicationConstants.COMMENT_CODE_OTH);

				if (serviceResponse.isAccepted()) {
					tran.setTranSubStatCode(TransactionStatus.CC_AUTH_CODE);
					tran.setTCInternalTransactionNumber(String.valueOf(tranId));
					tran.setTranSeqNumber(serviceResponse.getEffortId());

					transactionManager.updateTransaction(tran, userId, StringUtils.trimToNull(serviceResponse.getIdentifier()), null);

				} else if (serviceResponse.isReTriable()) {
					String[] params1 = { String.valueOf(tran.getEmgTranId()),
							serviceResponse.getReason(), TRANSACTION_DETAIL_URL };
					sendAdminMessage(tran.getCsrPrcsUserid(), getMessageResource(
							"error.cc.auth.fail.notice", params1));
				} else {
					if(serviceResponse.getProcessorFormattedRespText() == null) {
						String[] params1 = { String.valueOf(tran.getEmgTranId()),
								"Network Error: No response received from Credit Card processor",
								TRANSACTION_DETAIL_URL };
						sendAdminMessage(tran.getCsrPrcsUserid(), getMessageResource(
								"error.cc.auth.fail.notice", params1));
					}

					try {
						sendCreditCardFailEmail(tran);
					} catch (Exception e) {
						EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(
								"Failed to send Transaction Email:", e);
					}

					tran.setTranSubStatCode(TransactionStatus.CC_FAILED_CODE);
					transactionManager.updateTransaction(tran, userId, StringUtils
							.trimToNull(serviceResponse.getIdentifier()), null);
					tran.setTranStatCode(TransactionStatus.ERROR_CODE);
					transactionManager.updateTransaction(tran, userId, StringUtils
							.trimToNull(serviceResponse.getIdentifier()), null);
				   }
				}
				//MBO-1702
				else if (tran.getTranAppVersionNumber() >= 2.00) {
						FinishPaymentResponse finishPaymentResponse = null;				
						try {		
							long orderId = tranId;
							String strOrderId = tran.getTCProviderTransactionNumber();
		            		System.out.println("Order Id for Txn is " + strOrderId);
					    	if(null!=strOrderId && !"".equals(strOrderId.trim())) {
					    	  orderId = Long.valueOf(strOrderId);
					    	}
					    	
					    	//MBO-4001 existing code havin TCProviderTransactionNumber value 
					    	//in order ID sent as an arguement in proxy
							finishPaymentResponse = CreditCardPaymentProxyImpl.getInstance().doFinishPayment(tran.getMccPartnerProfileId(), 
									orderId, tran.getPartnerSiteId(), Long.valueOf(1));
							//commnting below logic as response of finish payment dont include card type
							/*CardType cardTypeResp = authorizationResponse.getCardType();
							//Try to update the card type field on data base
							if(cardTypeAccount.equals(ConsumerCreditCardAccount.UNKNOWN_CODE) &&
									(cardTypeResp.equals(CardType.debit) || cardTypeResp.equals(CardType.credit))){
		*/
								/*accountService.updateCreditCardAccountType(userId, account.getId(),
										account.getAccountType().getCode(), cardTypeAccount);*/
							//commented  MBO-4001
//							}
						} catch (Exception e) {
							throw new EMGRuntimeException(e);
						}

						//CreditCardServiceResponse serviceResponse = constructServiceResponse(authorizationResponse);

						EventMonitor.getInstance().recordEvent(EventListConstant.CREDIT_CARD_AUTH,
								beginTime, System.currentTimeMillis());

						CommentService commentService = emgshared.services.ServiceFactory.getInstance().getCommentService();
						commentService
								.insertTransactionComment(
										tran.getEmgTranId(),
										userId,
										finishPaymentResponse
												.getFormattedResponseText(),
										EMoneyGramAdmApplicationConstants.COMMENT_CODE_OTH);

						if (decision_accept.equalsIgnoreCase(finishPaymentResponse.getDecision().toString())) {
							tran.setTranSubStatCode(TransactionStatus.CC_AUTH_CODE);
							tran.setTCInternalTransactionNumber(String.valueOf(tranId));
							tran.setTranSeqNumber(Long.valueOf(1)); //setting effort id to 1 as response not contains

							transactionManager.updateTransaction(tran, userId, null, null);

						} else if (finishPaymentResponse.getErrors()!=null && finishPaymentResponse.getErrors()[0]!=null
								&& finishPaymentResponse.getErrors()[0].getCanRetry()) {
							String[] params1 = { String.valueOf(tran.getEmgTranId()),
									finishPaymentResponse.getErrors()[0].getErrorMessage(), TRANSACTION_DETAIL_URL };
							sendAdminMessage(tran.getCsrPrcsUserid(), getMessageResource(
									"error.cc.auth.fail.notice", params1));
						} else {
							/*if(finishPaymentResponse.getFormattedResponseText() == null) {
								String[] params1 = { String.valueOf(tran.getEmgTranId()),
										"Network Error: No response received from Credit Card processor",
										TRANSACTION_DETAIL_URL };
								sendAdminMessage(tran.getCsrPrcsUserid(), getMessageResource(
										"error.cc.auth.fail.notice", params1));
							}*/

							try {
								sendCreditCardFailEmail(tran);
							} catch (Exception e) {
								EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(
										"Failed to send Transaction Email:", e);
							}

							tran.setTranSubStatCode(TransactionStatus.CC_FAILED_CODE);
							transactionManager.updateTransaction(tran, userId, null, null);
							tran.setTranStatCode(TransactionStatus.ERROR_CODE);
							transactionManager.updateTransaction(tran, userId, null , null);
						}
				}
				
				//ended

				return tran;
			}
				throw new IllegalStateException("transaction id = " + tran.getEmgTranId()
						+ " is not in valid state");
			}
		};
		return updateTransaction(tranId, userId, command);
	}

	/**
	 * Get the Card Code according the CardType Response
	 * @param cardType
	 * @return
	 */
	private String getCardTypeCode(CardType cardType){
		if(cardType.equals(CardType.credit))
			return ConsumerCreditCardAccount.CREDIT_CODE;
		else if(cardType.equals(CardType.debit))
			return ConsumerCreditCardAccount.DEBIT_CODE;
		else
			return null;

	}

	private CreditCardServiceResponse constructServiceResponse(AuthorizationResponse authorizationResponse) {

		String requestId = null;
		String decision = null;
		String reasonCode = null;
		String processorTraceNbr = null;
		String processorRespTxt = null;
		long effortId = 0L;

		if (authorizationResponse == null) {
			decision = decision_reject;
		} else if(authorizationResponse.getErrors() != null
				&& authorizationResponse.getErrors().length > 0) {
			reasonCode = authorizationResponse.getErrors()[0].getReasonCode();
			processorRespTxt = authorizationResponse.getHeader().getClientHeader().getClientSessionID();
			decision = decision_reject;
		} else {
			decision = authorizationResponse.getDecision().getValue();
			requestId = authorizationResponse.getVendorRequestId();
			processorRespTxt = authorizationResponse.getHeader().getClientHeader().getClientSessionID();
			if(authorizationResponse.getGlobalCollect() != null
					&& authorizationResponse.getGlobalCollect().getGcResponse() != null
					&& authorizationResponse.getGlobalCollect().getGcResponse().getEffortId() != null) {
				effortId = authorizationResponse.getGlobalCollect().getGcResponse().getEffortId();
			}
			if (authorizationResponse.getGlobalCollect() != null
					&& authorizationResponse.getGlobalCollect().getGcResponse() != null) {
				processorTraceNbr = authorizationResponse.getGlobalCollect().getGcResponse().getPaymentReference();
			}
		}

		if (decision.equalsIgnoreCase(decision_reject)
				|| (authorizationResponse.getErrors() != null
						&& authorizationResponse.getErrors().length > 0)) {
			StringBuilder errorText = new StringBuilder(128);
			errorText.append("Credit card authorization failed.");
			errorText.append("emgTrackingValue = ");
			errorText.append("insert emg tracking value here");
			errorText.append(".  Request id = ");
			errorText.append(requestId);
			errorText.append(".  Decision = ");
			errorText.append(decision);
			errorText.append(".  Reason code = ");
			errorText.append(reasonCode);
			errorText.append(".");
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(errorText.toString());
		}

		CreditCardServiceResponse response = new CreditCardServiceResponseImpl(requestId, decision, reasonCode);
		logger.info("process response to be set for Txn " + processorRespTxt);
		response.setProcessorFormattedRespText(processorRespTxt);
		response.setProcessorTraceNbr(processorTraceNbr);
		response.setEffortId(effortId);

		return response;
	}

	private  Transaction setMerchantId(Transaction tran) {
		//if (StringHelper.isNullOrEmpty(tran.getMccPartnerProfileId())) {
			// if MCC profile is not set, figure it out now.
			MerchantIdHelper merchant = new MerchantIdHelper();
			if (tran.getEmgTranTypeCode().equalsIgnoreCase(TransactionType.EXPRESS_PAYMENT_SEND_CODE)) {
				BillerLimit billerLimit = new BillerLimit();
				try {
					billerLimit = emgshared.services.ServiceFactory.getInstance().getBillerLimitService().getBillerLimit(tran.getRcvAgcyCode());
				} catch (Exception e) {
					throw new IllegalArgumentException("Biller MCC code could not be retrieved for transaction = " + tran.getEmgTranId());
				}
				// change here to corresponding global collect merchantid
				if(billerLimit.getPaymentProfileId().matches("\\d+")) {
					tran.setMccPartnerProfileId(billerLimit.getPaymentProfileId());
				}
				else {
					tran.setMccPartnerProfileId(EMTSharedContainerProperties.convertToGlobalCollectMerchantId(billerLimit.getPaymentProfileId()));
				}
			} else if (tran.getEmgTranTypeCode().equalsIgnoreCase(TransactionType.MONEY_TRANSFER_SEND_CODE)) {
				//MBO-5058
				try {
					tran.setMccPartnerProfileId(merchant.merchantIdInfo(tran.getPartnerSiteId(), tran.getEmgTranTypeCode(), GC));
				} catch (Exception e) {
					throw new IllegalArgumentException("Merchant id code could not be retrieved for transaction = " + tran.getEmgTranId());
				}
			}
		//}
		return tran;
	}

	@Override
	public boolean isTransactionStatusValid(Transaction transaction) {
		if (TransactionStatus.APPROVED_CODE.equals(transaction.getTranStatCode())
				&& TransactionStatus.NOT_FUNDED_CODE.equals(transaction.getTranSubStatCode())){
				if(!transaction.isFundable()) {
					throw new IllegalStateException("transaction id = " + transaction.getEmgTranId()
							+ " is not in a fundable state");
				}
				return true;
		}
		return false;
	}	
}
