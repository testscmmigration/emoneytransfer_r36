package emgadm.services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;

import shared.mgo.dto.MGOAgentProfile;
import shared.mgo.dto.MGOProduct;
import shared.mgo.services.AgentConnectAccessor;

import com.moneygram.agentconnect1305.wsclient.CommitTransactionRequest;
import com.moneygram.agentconnect1305.wsclient.CommitTransactionResponse;
import com.moneygram.agentconnect1305.wsclient.Error;
import com.moneygram.common.util.StringUtility;
import com.moneygram.service.CreditCardPaymentService_v2.AuthorizationResponse;
import com.moneygram.service.CreditCardPaymentService_v2.CCPError;
import com.moneygram.service.CreditCardPaymentService_v2.PostTransactionResponse;
import com.moneygram.service.CreditCardPaymentService_v2.RefundTransactionResponse;
import com.moneygram.service.CreditCardPaymentService_v2.SetPaymentResponse;
import com.moneygram.service.CreditCardPaymentService_v2.StatusResponse;

import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.dataaccessors.ManagerFactory;
import emgadm.dataaccessors.TransactionManager;
import emgadm.exceptions.CreditCardRefundException;
import emgadm.exceptions.InvalidRRNException;
import emgadm.exceptions.MGOGlobalAggException;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.model.GlAccount;
import emgadm.services.creditcardpayment.CreditCardPaymentProxy;
import emgadm.services.creditcardpayment.CreditCardPaymentProxyImpl;
import emgadm.services.processing.GlobalCollectAuthorize;
import emgadm.services.processing.UpdateTransactionCommand;
import emgadm.services.processing.VendorOperation;
import emgadm.sysmon.EventListConstant;
import emgadm.util.AgentConnectUtil;
import emgadm.util.StringHelper;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.AgentConnectException;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.ConsumerAccountType;
import emgshared.model.ConsumerCreditCardAccount;
import emgshared.model.DeliveryOption;
import emgshared.model.Transaction;
import emgshared.model.TransactionStatus;
import emgshared.services.CommentService;
import emgshared.services.ConsumerAccountService;
import emgshared.services.CreditCardServiceResponse;
import emgshared.services.CreditCardServiceResponseImpl;
import emgshared.util.Constants;
import eventmon.eventmonitor.EventMonitor;

public class TransactionServiceGlobalCollect extends TransactionServiceBase {

	private static final TransactionService INSTANCE = new TransactionServiceGlobalCollect();
	private static Logger logger = EMGSharedLogger.getLogger(TransactionServiceGlobalCollect.class);
	
	//MBO-1702 : CCPS V2 dont have Decision class
	private final String decision_reject = "reject";
	//ended

	public static TransactionService getInstance() {
		return INSTANCE;
	}

	private TransactionServiceGlobalCollect() {
		;
	}

	public void sendPersonToPerson(final int tranId, final String userId, final boolean useBackupAccount)
	throws TransactionAlreadyInProcessException, TransactionOwnershipException, InvalidRRNException, AgentConnectException {
		final TransactionManager tranManager = ManagerFactory.createTransactionManager();
		final Transaction tran = tranManager.getTransaction(tranId);
		UpdateTransactionCommand command1305 = new UpdateTransactionCommand() {
			public Object execute() {
				if (tran == null) {
					throw new IllegalArgumentException("unable to retrieve transaction with id = " + tranId);
				}
				tranManager.updateTransaction(tran, userId, null, null);
				if (!tran.isSendable()) {
					throw new IllegalStateException("transaction id = " + tranId + " is not in a sendable state.");
				}
				CommitTransactionRequest req = null;
				CommitTransactionResponse response = null;
				AgentConnectAccessor ac = null;
				Date before = new Date(System.currentTimeMillis());
				try {
					//6101
					/*MGOAgentProfileService maps = MGOServiceFactory.getInstance().getMGOAgentProfileService();
					String agtid = String.valueOf(tran.getSndAgentId());
					MGOAgentProfile mgoAgentProfile = maps.getMGOProfileByAgtId(agtid, tran.getEmgTranTypeCode());*/
					MGOAgentProfile mgoAgentProfile = new MGOAgentProfile();
					mgoAgentProfile.setAgentId(tran.getAgentId());
					mgoAgentProfile.setAgentSequence(tran.getAgentSeqNo());
					mgoAgentProfile.setProfileId(tran.getAgentUnitProfileId()!=null ? Integer.valueOf(tran.getAgentUnitProfileId()) : 0);
					mgoAgentProfile.setToken(tran.getAgentToken());
					MGOProduct mgoProduct = new MGOProduct();
					mgoProduct.setProductName(tran.getEmgTranTypeCode());
					mgoAgentProfile.setMgoProduct(mgoProduct);
					//6101 ended
					//Removed RRN check as part of MBO-5147
					long beginTime = System.currentTimeMillis();
					try {
						ac = new AgentConnectAccessor(mgoAgentProfile);
						req = buildCommitTransactionRequest(tran);
						try {
							EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("EMT Agent Connect v.13.05 ...START...\n\n");
							com.moneygram.agentconnect1305.wsclient.Error error=new Error();
							response = ac.commitTransaction(req);
							EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("EMT Agent Connect v.13.05 ...END...\n\n");
						} catch (Exception e2) {
							EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("EMT Agent Connect v.13.05 ...ERROR...\n\n");
							acknowledgementFailEmail(tranId, response==null?"":response.getReferenceNumber(), e2, userId);
							AgentConnectUtil acUtil=new AgentConnectUtil();
							if(acUtil.hasGlobalAggregateError(e2)){
								throw new MGOGlobalAggException("Global Aggregate error",e2);
							} else {
								throw new AgentConnectException(e2);
							}

						}
					} catch (MGOGlobalAggException e0) {
					    throw e0;
					} catch (Exception e1) {
						throw new AgentConnectException(e1);
					} finally {
						EventMonitor.getInstance().recordEvent(EventListConstant.P2P_AC_SEND, beginTime, System.currentTimeMillis());
					}
					String referenceNumber = response.getReferenceNumber();
					tran.setLgcyRefNbr(referenceNumber);
					tran.setSndTranDate(new Date());
					tran.setTranAvailabilityDate(response.getExpectedDateOfDelivery());

					//S29a Add three minute phone number and three minute pin number
					if(tran.getDlvrOptnId()==DeliveryOption.WILL_CALL_ID ||tran.getDlvrOptnId()==DeliveryOption.ONLY_AT_ID)
					{

					tran.setThreeMinuteFreePhoneNumber(response.getTollFreePhoneNumber());
					tran.setThreeMinuteFreePinNumber(response.getFreePhoneCallPIN());
					}
					//MGO-12802 changes starts
					
					/*// set the actual send fee amount, total amount
					if (!"USA".equalsIgnoreCase(tran.getRcvISOCntryCode())) {
					    BigDecimal exRate = null;
						exRate = getNewExchangeRate(
						        ac,
						        tran.getRcvISOCntryCode(),
						        tran.getSndISOCntryCode(),
						        tran.getDlvrOptnId(),
						        tran.getRcvISOCrncyId(),
						        tran.getRcvPayoutISOCrncyId(),
						        null,
						        tran.getLoyaltyPgmMembershipId());
						// set to new foreign exchange rate
						if (exRate != null) {
							tran.setSndFxCnsmrRate(exRate);
							if (!tran.getRcvISOCrncyId()
							        .equals(tran.getRcvPayoutISOCrncyId())) {
							    tran.setRcvISOCrncyId(tran.getRcvPayoutISOCrncyId());
							}
						}
					}*/
					
					//MGO-12802 changes ends
					String refNbr = response.getPartnerConfirmationNumber();
					if (!StringHelper.isNullOrEmpty(refNbr)) {
						tran.setRcvAgentRefNbr(refNbr);
					}
 					ConsumerAccountType accountType = useBackupAccount ?
 							ConsumerAccountType.getInstance(tran.getSndBkupAcctTypeCode()) :
 							ConsumerAccountType.getInstance(tran.getSndAcctTypeCode());
					if (!useBackupAccount) {
						tran.setTranStatCode(TransactionStatus.SENT_CODE);
						tran.setSndTranDate(new Date());
						tranManager.updateTransaction(tran, userId, null, null);
					}
					try {
						sendPersonToPersonSuccessEmail(tran, accountType);
					} catch (Exception e) {
						// Email failed to get sent.
						// Not a serious error, log and move on.
						EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Failed to send Transaction Email:", e);
					}
				} catch (MGOGlobalAggException e0) {
				    addMGOGlobalAggErrorToTranComment(e0, tranId, userId);
				} catch (Throwable t) {
                    addAgentConnectErrorToTranComment(t, tranId, userId);
					String msg = "In Program: TransactionServiceImpl\n" + "In Method: sendPersonToPerson()\n" + "AC call started at: " + before + "\n" + "AC call aborted at: " + new Date(System.currentTimeMillis()) + "\n" + "AC Method called: moneyGramSend() with a transaction id of " + tran.getEmgTranId() + " and the request object is as follows:\n\n" + req;
					EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Send Person To Person failed.\n\n" + msg, t);
					sendAgentConnectFailMail(msg, t, userId);
					// tran.setTranStatCode(TransactionStatus.ERROR_CODE);
					// tranManager.setTransactionStatus(
					// tranId,
					// tran.getTranStatCode(),
					// tran.getTranSubStatCode(),
					// userId);
					return t;
				}
				return null;
			}
		};
//		Object obj = updateTransaction(tranId, userId, !isMgiTranId(tran)?command76:command1305);
		Object obj = updateTransaction(tranId, userId, command1305);
		if (obj != null) {
			throw new AgentConnectException((Throwable) obj);
		}
	}

	public void fundPersonToPerson(final int tranId, final String authRequestId,
	        final String userId, final boolean useBackupAccount,
	        final String contextRoot, final long effortId)
	throws TransactionAlreadyInProcessException, TransactionOwnershipException {

		Validate.notNull(authRequestId, "authRequestId is a required parameter and must not be null.");

		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				final Transaction tran = tranManager.getTransaction(tranId);
				if (tran == null) {
					throw new IllegalArgumentException("unable to retrieve transaction with id = " + tranId);
				}
				if (!tran.isFundable()) {
					throw new IllegalStateException("transaction id = " + tranId + " is not in a fundable state");
				}
				final int tranId = tran.getEmgTranId();

				final ConsumerAccountType primaryAccountType = ConsumerAccountType.getInstance(tran.getSndAcctTypeCode());
				final int creditCardAccountId = (primaryAccountType.isCardAccount() ?
						tran.getSndCustAcctId() :
						tran.getSndCustBkupAcctId());

				final double amount = tran.getSndTotAmt().doubleValue();
				long beginTime = System.currentTimeMillis();
				String currency = tran.getSndISOCrncyId();

				
                CreditCardServiceResponse creditCardResponse = null;
                String reasonCode = null;
                
				String reasonCode1 = null;
				String reasonCode2 = null;
				
				String postTxDecision1 = null;
				String postTxDecision2 = null;
				String postTxDecision = null;
				Integer statusResponseId1 = null;
				Integer statusResponseId2 = null;
				Integer statusResponseId = null;
			    PostTransactionResponse postTransactionResponse1 = null;
                PostTransactionResponse postTransactionResponse2 = null;
				boolean isMaestroTx = primaryAccountType.isDebitCardAccount()
				        && primaryAccountType.isMaestroDebitCardAccount();

				try {

				    //changes start for 1702
					//deleted Maestro specific code as per 1702
					System.out.println("Tran App Version is " + tran.getTranAppVersionNumber());
				    if(tran.getTranAppVersionNumber() < 2.00) {

                        // DO_SETPAYMENT response
                        PostTransactionResponse postTransactionResponse = null;
                        
                        //MBO-1704
                        long orderId = tranId;
						String strOrderId = tran.getTCProviderTransactionNumber();
	            		System.out.println("Order Id for Txn is " + strOrderId);
				    	if(null!=strOrderId && !"".equals(strOrderId.trim())) {
				    	  orderId = Long.valueOf(strOrderId);
				    	}
				    	else
				    		orderId = -1; //PostTrasnaction has logic that if orderId=-1, orderId=tranId;
				    	logger.info("Order Id being passed in postTrasnaction for " + tranId + " is " + orderId);
				    	
				    	/*FinishPaymentResponse finishPaymentResponse = CreditCardPaymentProxyImpl.getInstance().doFinishPayment
				    	(tran.getMccPartnerProfileId(), 
								orderId, tran.getPartnerSiteId(), Long.valueOf(1));
				    	logger.info("Going to call finish payment before Post Txn as it is mandatory");
				    	if(finishPaymentResponse!=null && finishPaymentResponse.getDecision()!=null
				    			&&  finishPaymentResponse.getDecision().getValue()!=null)
				    		logger.info("Finishpayment before Post Txn is done with Decision value as " + 
				    				finishPaymentResponse.getDecision().getValue());*/
                        //ended
                        postTransactionResponse = 
                            CreditCardPaymentProxyImpl.getInstance()
                                    .postTransaction(creditCardAccountId, 
                                            amount, currency, userId, 
                                            tran.getMccPartnerProfileId(), 
                                            effortId, tranId,orderId);

                        if (postTransactionResponse.getError() != null) {
                            reasonCode = 
                                postTransactionResponse.getError()
                                        .getErrorCode();
                        }

                        creditCardResponse = 
                            new CreditCardServiceResponseImpl(
                                    postTransactionResponse.getDecision()
                                            .getValue(), reasonCode);
                        
                        creditCardResponse.setProcessorFormattedRespText(
                                postTransactionResponse.getHeader()
                                    .getClientHeader()
                                    .getClientSessionID());
                        
                        postTxDecision = postTransactionResponse.getDecision()
                        .getValue().toUpperCase();
                    
                    /*    StatusResponse statusResponse = 
                            CreditCardPaymentProxyImpl.getInstance()
                                    .status(null, creditCardAccountId, effortId,
                                            tranId, tran.getMccPartnerProfileId(),
                                            currency);*/
                        
				    } //if block ended for 1.x txn logic
				    else if(tran.getTranAppVersionNumber()>=2.00) {
				    	logger.info("Inside .NXT Txn loop for fundPersonToPerson method");
				    	SetPaymentResponse response  = null;
				    	String strOrderId = tran.getTCProviderTransactionNumber();
				    	logger.info("Order Id to be passed in Set Payment response is " + strOrderId);
				    	logger.info("Merchant id is " + ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTML(tran.getMccPartnerProfileId())));
				    	logger.info("Credit card account id is " + creditCardAccountId);
				    	if(null!=strOrderId && !"".equals(strOrderId.trim())) {
				    		response = CreditCardPaymentProxyImpl.getInstance()
                                .doSetPayment(tran.getMccPartnerProfileId(),currency, amount, 
                                		Long.valueOf(strOrderId), effortId,userId, creditCardAccountId);
				    		
				    		if (response.getError() != null) {
	                            reasonCode = 
	                            	response.getError()
	                                        .getErrorCode();
	                        }

	                        creditCardResponse = 
	                            new CreditCardServiceResponseImpl(
	                            		response.getDecision()
	                                            .getValue(), reasonCode);
	                     // to be commented
							logger.debug("comment to be inserted from setpayment is "
											+ response
													.getFormattedResponseText());
							//MBO-4001 from client header comment is moved to formattedResponseText
	                        creditCardResponse.setProcessorFormattedRespText(
	                        		response.getFormattedResponseText());	
	                     // MBO-4001
				    	}
				    	
				    	//end 1702
				    }

				} catch (ProxyException e1) {
					logger.error(e1.getMessage(),e1);
				} catch (Exception e1) {
					logger.error(e1.getMessage(),e1);
				}

				CommentService commentService =
				    emgshared.services.ServiceFactory.getInstance().getCommentService();
				EventMonitor eventMonitor = EventMonitor.getInstance();

				//code changes for 1702
				
                    // Log post transaction (SET_PAYMENT)
                    eventMonitor.recordEvent(
                            EventListConstant.GLOBAL_COLLECT_POST,
                            beginTime,System.currentTimeMillis());

                    commentService.insertTransactionComment(tran.getEmgTranId(),
                            userId,
                            creditCardResponse.getProcessorFormattedRespText());
                    
                    if (creditCardResponse != null
                            && creditCardResponse.isAccepted()) {
                        tran.setTranSeqNumber(effortId);
                        tran.setTranSubStatCode(TransactionStatus.CC_SALE_CODE);
                        tran.setTCProviderCode(Constants.PYMT_PROVIDER_CODE_GLOBAL_COLLECT);
                        tranManager.updateTransaction(tran, userId, null, null);
                    } else {

                        commentService.insertCreditCardResponseComment(creditCardResponse, Integer.valueOf(tran.getSndCustAcctId()),
                                Integer.valueOf(tranId), resourceFile, userId);

                        if (creditCardResponse != null
                                && !creditCardResponse.isReTriable()) {
                            try {
                                sendCreditCardFailEmail(tran);
                            } catch (Exception e) {
                                // Email failed to get sent.
                                // Not a serious error, log and move on.
                                EMGSharedLogger.getLogger(this.getClass().getName().toString())
                                        .error("Failed to send Transaction Email:",e);
                            }
                            tran.setTranSubStatCode(TransactionStatus.CC_FAILED_CODE);
                            tranManager.updateTransaction(tran, userId,authRequestId, null);
                            tran.setTranStatCode(TransactionStatus.ERROR_CODE);
                            tranManager.updateTransaction(tran, userId, null, null);
                        } else if (creditCardResponse != null
                                && creditCardResponse.isReTriable()) {
                            // add admin message
                            String[] params = {
                                    String.valueOf(tran.getEmgTranId()),
                                    creditCardResponse.getReason(),
                                    getTransactionUrl(contextRoot) };
                            sendAdminMessage(
                                    tran.getCsrPrcsUserid(),
                                    getMessageResource(
                                            "error.cc.capture.fail.notice", params));
                        }
                    }

                
				return null;
			}
		};
		updateTransaction(tranId, userId, command);
	}

	public ArrayList processMoneyGramTransaction(int tranId,
			String callerLoginId, String tranStat, String fundStat,
			String contextRoot) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException, AgentConnectException,
			InvalidRRNException, DataSourceException, SQLException {
		TransactionManager tm = ManagerFactory.createTransactionManager();
		Transaction tran = tm.getTransaction(tranId);
		ArrayList errors = new ArrayList<String>();

		
		
		/*
         * Identify if we are dealing with a Maestro transaction
         */
        int accountId = tran.getSndCustAcctId();
        emgshared.services.ServiceFactory sharedServiceFactory =
            emgshared.services.ServiceFactory.getInstance();
        ConsumerAccountService accountService =
            sharedServiceFactory.getConsumerAccountService();
        ConsumerCreditCardAccount account = null;
        ConsumerAccountType cat = null;

        try {
            account = accountService.getCreditCardAccount(accountId,
                    callerLoginId);
        } catch (DataSourceException e1) {
            // TODO Handle Correct Exception
            e1.printStackTrace();
        }
        
        try {
            cat = account.getAccountType();
            if (cat.isMaestroDebitCardAccount()) {
                tran.setTranSubStatCode(TransactionStatus.CC_AUTH_CODE);
                tran.setTCInternalTransactionNumber(String.valueOf(tranId));
                tran = tranManager.setMerchantId(tran);
                tranManager.updateTransaction(tran, callerLoginId);
            } else {
            	//code added for 1702
            	if (TransactionStatus.APPROVED_CODE.equals(tranStat) && TransactionStatus.NOT_FUNDED_CODE.equals(fundStat)) {
            		System.out.println("Tran App Version is " + tran.getTranAppVersionNumber());
            		VendorOperation globalCollectAuthorize =
                    new GlobalCollectAuthorize();
            		tran = globalCollectAuthorize.execute(tranId, callerLoginId,contextRoot);
            		tm.updateTransaction(tran, callerLoginId);
            	}
            	
            }
        } catch (EMGRuntimeException e) {
        	logger.error(e.getMessage(),e);
        } catch (Exception e2) {
        	logger.error(e2.getMessage(),e2);
        }

		if (TransactionStatus.APPROVED_CODE.equals(tran.getTranStatCode())
				&& TransactionStatus.CC_AUTH_CODE.equals(tran
						.getTranSubStatCode())) {
			try {
				sendPersonToPerson(tranId, callerLoginId, false);
				tran = tm.getTransaction(tranId);
			} catch (AgentConnectException e) {
				// add admin message
				String[] params = { String.valueOf(tran.getEmgTranId()),
						tran.getLgcyRefNbr(), getTransactionUrl(contextRoot) };
				sendAdminMessage(
						tran.getCsrPrcsUserid(),
						getMessageResource(
								"error.person.to.person.send.fail.notice",
								params));
				errors.add("error.agent.connect.failed");
				//MBO-1920 call a stored proc to update the transaction with last update timestamp and cs_prcs_userid as null
				tran.setCsrPrcsUserid(null);
				tm.updateTransaction(tran, callerLoginId);
				
				return errors;
			}
		}

		// Get funding (CC Capture or ACH Waiting) with status of SEN/CCA
		if (TransactionStatus.SENT_CODE.equals(tran.getTranStatCode())
				&& TransactionStatus.CC_AUTH_CODE.equals(tran
						.getTranSubStatCode())) {
			// find conf. code from tran action table with reason code of 28
			// FIXME(MGOUK-TxnProcessing) - GC doesn't have auth confcode - what
			// should we pass to log to DB for this?
			// - We shouldn't need to do the lookup below - maybe use 'tranid'
			// or the orderid+effortid?
			String ccAuthConfReasonCode = "28";
			String AuthConfCode = tm.getCCAuthConfId(tranId,
					ccAuthConfReasonCode);
			Long effortId=Long.valueOf(1);
			fundPersonToPerson(tranId, AuthConfCode, callerLoginId, false,
					contextRoot, effortId.longValue());
		}
		return null; // all is well.
	}

	public void doCreditCardReversal(final int tranId, final String userId,
			final String contextRoot)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {

		UpdateTransactionCommand command = new UpdateTransactionCommand() {

			public Object execute() {

				final Transaction tran = tranManager.getTransaction(tranId);
				long effortId =tran.getTranSeqNumber();
								
//				if(tran.getSndAcctTypeCode().equals(ConsumerAccountType.DEBIT_CARD_MAESTRO.getCode())){
//					effortId = Long.valueOf(1);
//				}
				final double amount = tran.getSndTotAmt().doubleValue();

				if (tran == null) {
					throw new IllegalArgumentException(
							"unable to retrieve transaction with id = "
									+ tranId);
				}

				String RequestId = null;
				long beginTime = System.currentTimeMillis();

				try {

					final ConsumerAccountType primaryAccountType = ConsumerAccountType
							.getInstance(tran.getSndAcctTypeCode());
					final long creditCardAccountId = (primaryAccountType
							.isCardAccount() ? tran.getSndCustAcctId() : tran
							.getSndCustBkupAcctId());
					String currency = tran.getSndISOCrncyId();
					boolean isMaestroTx = primaryAccountType.isDebitCardAccount()&& primaryAccountType.isMaestroDebitCardAccount();
					boolean isUsingTranIdAsOrderId = true; 
					
					//MBO-1704
                    long orderId = tranId;
					String strOrderId = tran.getTCProviderTransactionNumber();
            		System.out.println("Order Id for Txn is " + strOrderId);
			    	if(null!=strOrderId && !"".equals(strOrderId.trim())) {
			    	  orderId = Long.valueOf(strOrderId);
			    	}
			    	else
			    		orderId = tranId;
			    	
			    	//ended 1704
			    	
					CreditCardServiceResponse serviceResponse = null;
					if(isMaestroTx || isUsingTranIdAsOrderId){
						serviceResponse = executeRefund(RequestId, amount, creditCardAccountId, tran.getMccPartnerProfileId(), effortId, tranId,null,true,null,orderId);
					}
					else{
						serviceResponse = executeRefund(RequestId, amount, creditCardAccountId, tran.getMccPartnerProfileId(), effortId, tranId,null,false,null,orderId);
					}
					

					EventMonitor.getInstance().recordEvent(
							EventListConstant.GLOBAL_COLLECT_CREDIT_REVERSAL,
							beginTime, System.currentTimeMillis());

					if (serviceResponse != null && serviceResponse.isAccepted()) {

			            String requestId = serviceResponse.getIdentifier();
			            //MBO-2813 When the sub status is EWL we need to change it to WOL when the API call is successful
						if (TransactionStatus.EXCEPTION_WRITE_OFF_LOSS_CODE
								.equals(tran.getTranSubStatCode())) {
							tran.setTranSubStatCode(TransactionStatus.WRITE_OFF_LOSS_CODE);
							tranManager.updateTransaction(tran, userId,
									StringUtils.trimToNull(requestId), null);
						} else {
							tran.setTranSubStatCode(TransactionStatus.CC_CANCEL_CODE);
						
			            Integer gcTxnStatus = null;
			            
			            try {
			              gcTxnStatus = getGCTxnStatus(requestId, creditCardAccountId, tran.getMccPartnerProfileId(),
			                tranId, currency);
			            } catch (Exception e){
			              //continue on if exception
			              EMGSharedLogger.getLogger(
			              this.getClass().getName().toString())
			              .error("Failed to get GC Status during refund for tranId " + tranId, e);
			            }
			            
			            if (gcTxnStatus != null &&
			              gcTxnStatus < PAYMENT_STATUS_PROCESSED) { 
			              //if GC hasn't posted txn to customer yet, move to 'CCV' instead of 'CCC'
			              tran.setTranSubStatCode(TransactionStatus.CC_VOID_CODE);
			            }
			            tranManager.updateTransaction(tran, userId,
			                StringUtils.trimToNull(requestId), null);
						}

					} else {

						if (serviceResponse != null
								&& !serviceResponse.isReTriable()
								&& !(TransactionStatus.EXCEPTION_WRITE_OFF_LOSS_CODE
										.equals(tran.getTranSubStatCode()))) {

							try {
								sendCreditCardFailEmail(tran);
							} catch (Exception e) {
								// Email failed to get sent.
								// Not a serious error, log and move on.
								EMGSharedLogger
										.getLogger(
												this.getClass().getName()
														.toString())
										.error("Failed to send Transaction Email:",
												e);
							}

							/*tran.setTranSubStatCode(TransactionStatus.CC_FAILED_CODE);
							tranManager.updateTransaction(tran, userId,
									RequestId, null);
							tran.setTranStatCode(TransactionStatus.ERROR_CODE);
							tranManager.updateTransaction(tran, userId, null,
									null);*/

						} else if (serviceResponse != null
								&& serviceResponse.isReTriable()) {
							// add admin message
							String[] params = {
									String.valueOf(tran.getEmgTranId()),
									serviceResponse.getReason(),
									getTransactionUrl(contextRoot) };

							sendAdminMessage(
									tran.getCsrPrcsUserid(),
									getMessageResource(
											"error.cc.reversal.fail.notice",
											params));
						}
					}
					return null;
				} catch (Exception e) {
					throw new EMGRuntimeException(e);
				}
			}
		};
		updateTransaction(tranId, userId, command);
	}

	private CreditCardServiceResponse executeRefund(String captureRequestId,
			double amount, long accountId, String merchantId, long effortId,
			int tranId, String currency, boolean useTranIdAsOrderID, String userId, long orderId) throws Exception {
		RefundTransactionResponse refundResponse = null;
		if (useTranIdAsOrderID) {			
			refundResponse = CreditCardPaymentProxyImpl.getInstance().refundTransaction(captureRequestId, amount,currency, accountId, merchantId, effortId, tranId,orderId);
		}
		else {
			refundResponse = CreditCardPaymentProxyImpl.getInstance().refundTransaction(captureRequestId, amount,currency, accountId, merchantId, effortId, tranId);
		}
		

		String decision = null;
		String reasonCode = null;

		if (refundResponse == null) {
			decision = decision_reject;
		} else if (refundResponse.getErrors() != null
				&& refundResponse.getErrors().length > 0) {
			reasonCode = refundResponse.getErrors()[0].getReasonCode();
			decision = decision_reject;
		} else {
			decision = refundResponse.getDecision().getValue();
		}

		CreditCardServiceResponse response = new CreditCardServiceResponseImpl(
				decision, reasonCode);
		StringBuilder buf = new StringBuilder();
		buf.append(EventListConstant.GC_REFUND + "|");
		buf.append(refundResponse.getDecision().getValue().toUpperCase());

		if (refundResponse.getError() != null) {
			buf.append("|MGICode=");
			buf.append(refundResponse.getError().getErrorCode());
			buf.append("|ErrorDescr=");
			buf.append(refundResponse.getError().getErrorMessage());
		} else if (refundResponse.getErrors() != null
				&& refundResponse.getErrors().length > 0) {
			CCPError ccpErrors[] = refundResponse.getErrors();
			for (int i = 0; i < ccpErrors.length; i++) {
				buf.append("|Error");
				buf.append(i + 1);
				buf.append("Code=");
				buf.append(ccpErrors[0].getReasonCode());
			}
			buf.append("|Error1Desc=");
			buf.append(ccpErrors[0].getErrorMessage());
		}

		if (buf.length() > EMoneyGramAdmApplicationConstants.COMMENT_DB_COLUMN_MAX_SIZE) {
			buf.delete(
					EMoneyGramAdmApplicationConstants.COMMENT_DB_COLUMN_MAX_SIZE,
					buf.length());
		}
		response.setProcessorFormattedRespText(buf.toString());
		CommentService commentService = emgshared.services.ServiceFactory
				.getInstance().getCommentService();
		commentService.insertTransactionComment(tranId, userId,
				response.getProcessorFormattedRespText());

		if (response.isRejected() || response.isError()) {
			StringBuilder errorText = new StringBuilder(128);
			errorText.append("Credit card credit failed.");
			errorText.append("emgTrackingValue = ");
			errorText.append(accountId);
			errorText.append(".  CC request id = ");
			errorText.append(response.getIdentifier());
			errorText.append(".  CC decision = ");
			errorText.append(decision);
			errorText.append(".  CC reason code = ");
			errorText.append(response.getReason());
			errorText.append(".");
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.error(errorText.toString());
		}
		return response;
	}

	public void doCreditCardPartialRefund(final int tranId,
			final double refundAmount, final String userId,
			final String contextRoot)
			throws TransactionAlreadyInProcessException,
			TransactionOwnershipException, CreditCardRefundException {

		UpdateTransactionCommand command = new UpdateTransactionCommand() {

			public Object execute() {

				final Transaction tran = tranManager.getTransaction(tranId);
				final long effortId = tran.getTranSeqNumber();
				
				if (tran == null) {
					throw new IllegalArgumentException("unable to retrieve transaction with id = "+ tranId);
				}
				
				//MBO-1704
                long orderId = tranId;
				String strOrderId = tran.getTCProviderTransactionNumber();
        		System.out.println("Order Id for Txn is " + strOrderId);
		    	if(null!=strOrderId && !"".equals(strOrderId.trim())) {
		    	  orderId = Long.valueOf(strOrderId);
		    	}
		    	else		    	 
		    		orderId = tranId;
		    	//ended 1704

				String RequestId = null;
				StatusResponse captureResponse = null;
				long beginTime = System.currentTimeMillis();

				try {

					final ConsumerAccountType primaryAccountType = ConsumerAccountType
																			.getInstance(tran.getSndAcctTypeCode());

					final long creditCardAccountId = (primaryAccountType.isCardAccount() ?
														tran.getSndCustAcctId() : tran.getSndCustBkupAcctId());

					boolean isMaestroTx = primaryAccountType.isDebitCardAccount()
												&& primaryAccountType.isMaestroDebitCardAccount();
					
					boolean isUsingTranIdAsOrderId = (tran.getPartnerSiteId().equals(EMoneyGramAdmApplicationConstants.MGODE_PARTNER_SITE_ID) ||
							tran.getPartnerSiteId().equals(EMoneyGramAdmApplicationConstants.MGOUK_PARTNER_SITE_ID));
					
					
					String currency = tran.getSndISOCrncyId();

					CreditCardPaymentProxy creditCardProxy = CreditCardPaymentProxyImpl.getInstance();


					if(isMaestroTx || isUsingTranIdAsOrderId){
						captureResponse = creditCardProxy.status(RequestId,creditCardAccountId, effortId, tranId,
																	tran.getMccPartnerProfileId(), currency, orderId);
					}
					else{
						captureResponse = creditCardProxy.status(RequestId, creditCardAccountId, effortId, tranId,
																			tran.getMccPartnerProfileId(), currency);
					}

					EventMonitor.getInstance().recordEvent( EventListConstant.GLOBAL_COLLECT_CREDIT_REVERSAL,
																			beginTime, System.currentTimeMillis());

					if (captureResponse != null) {

						String reasonCode = null;
						String errorDescription = null;

						if (captureResponse.getError() != null) {
							reasonCode = captureResponse.getError().getErrorCode();
							errorDescription = captureResponse.getError().getErrorMessage();
						}

						CreditCardServiceResponse serviceResponse = new CreditCardServiceResponseImpl(
								captureResponse.getDecision().getValue(),reasonCode);

						if (serviceResponse.isAccepted()
								&& captureResponse.getGlobalCollect().getStatusId() >= PAYMENT_STATUS_PROCESSED) {

							RefundTransactionResponse refundResponse = null;
							if(isMaestroTx || isUsingTranIdAsOrderId ){
								 refundResponse = creditCardProxy.refundTransaction(RequestId,refundAmount,currency,
										creditCardAccountId,tran.getMccPartnerProfileId(),effortId, tranId, orderId);
							}
							else{
								 refundResponse = creditCardProxy.refundTransaction(RequestId,refundAmount,currency,
										 creditCardAccountId,tran.getMccPartnerProfileId(),effortId, tranId);
							}


							if (refundResponse.getErrors() != null && refundResponse.getErrors().length > 0) {
								reasonCode = refundResponse.getErrors()[0].getReasonCode();
								errorDescription = refundResponse.getErrors()[0].getErrorMessage();
							}

							if (!refundResponse.getDecision().getValue().equalsIgnoreCase(DECISION_ACCEPT)) {
								throw new CreditCardRefundException(reasonCode,errorDescription);
							}

							return null;
						} else {

							if (captureResponse.getGlobalCollect()
									.getStatusId() < PAYMENT_STATUS_PROCESSED) {
								if(StringUtility.isNullOrEmpty(errorDescription)){
									errorDescription = "Transaction has not been processed yet by Global Collect";
								}

								String[] params = {
										String.valueOf(tran.getEmgTranId()),
										reasonCode, errorDescription };
								sendAdminMessage(
										tran.getCsrPrcsUserid(),
										getMessageResource(
												"error.partial.refund.fail.notice",
												params));
								CommentService commentService = emgshared.services.ServiceFactory.getInstance().getCommentService();
								commentService.insertGCCardResponseComment(serviceResponse, Integer.valueOf(tran.getSndCustAcctId()), 
										Integer.valueOf(tranId), resourceFile, userId);
							}
							throw new CreditCardRefundException(reasonCode,
									errorDescription);
						}
					}
				} catch (CreditCardRefundException e) {
					throw new EMGRuntimeException(e);
				} catch (Exception e) {
					throw new EMGRuntimeException(e);
				}

				return null;
			}
		};

		updateTransaction(tranId, userId, command);
	}

	public List<GlAccount> getGLAccounts(String userId) throws Exception {
		TransactionManager transactionManager = ManagerFactory
				.createTransactionManager();
		List<GlAccount> glAccounts = new ArrayList<GlAccount>();

		for (GlAccount ga : transactionManager.getGLAccounts(userId)) {
			if (ga.getGlAccountId() == EMoneyGramAdmApplicationConstants.GL_ACCOUNT_CODE_FEE_REVENUE
					|| ga.getGlAccountId() == EMoneyGramAdmApplicationConstants.GL_ACCOUNT_CODE_AGENT_AR_AP
					|| ga.getGlAccountId() == EMoneyGramAdmApplicationConstants.GL_ACCOUNT_CODE_LOSS_RESERVE
					|| ga.getGlAccountId() == EMoneyGramAdmApplicationConstants.GL_ACCOUNT_CODE_GOODWILL) {
				glAccounts.add(ga);
			}
		}
		return glAccounts;
	}

	private CreditCardServiceResponse executeAuthorization(int accountId,
			double amount, long internalTranId, String callerLoginId,
			String merchantId, int tranId) throws Exception {

		final Transaction tran = tranManager.getTransaction(tranId);
		tran.setTranSeqNumber(0L);
		tran.setMccPartnerProfileId(merchantId);
		AuthorizationResponse authorizationResponse = CreditCardPaymentProxyImpl
				.getInstance().authorize(tran, callerLoginId);

		String requestId = null;
		String decision = null;
		String reasonCode = null;
		String processorTraceNbr = null;
		String processorRespTxt = null;
		long effortId = 0L;

		if (authorizationResponse == null) {
			decision = decision_reject;
		} else if (authorizationResponse.getErrors() != null
				&& authorizationResponse.getErrors().length > 0) {
			reasonCode = authorizationResponse.getErrors()[0].getReasonCode();
			processorRespTxt = authorizationResponse.getHeader()
					.getClientHeader().getClientSessionID();
			decision = decision_reject;
		} else {
			decision = authorizationResponse.getDecision().getValue();
			requestId = authorizationResponse.getVendorRequestId();
			processorRespTxt = authorizationResponse.getHeader()
					.getClientHeader().getClientSessionID();
			if (authorizationResponse.getGlobalCollect() != null
					&& authorizationResponse.getGlobalCollect().getGcResponse() != null
					&& authorizationResponse.getGlobalCollect().getGcResponse()
							.getEffortId() != null) {
				effortId = authorizationResponse.getGlobalCollect()
						.getGcResponse().getEffortId();
			}
			if (authorizationResponse.getGlobalCollect() != null
					&& authorizationResponse.getGlobalCollect().getGcResponse() != null) {
				processorTraceNbr = authorizationResponse.getGlobalCollect()
						.getGcResponse().getPaymentReference();
			}
		}

		if (decision.equalsIgnoreCase(decision_reject)
				|| (authorizationResponse.getErrors() != null && authorizationResponse
						.getErrors().length > 0)) {
			StringBuilder errorText = new StringBuilder(128);
			errorText.append("Credit card authorization failed.");
			errorText.append("emgTrackingValue = ");
			errorText.append(internalTranId);
			errorText.append(".  Request id = ");
			errorText.append(requestId);
			errorText.append(".  Decision = ");
			errorText.append(decision);
			errorText.append(".  Reason code = ");
			errorText.append(reasonCode);
			errorText.append(".");
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.error(errorText.toString());
		}

		CreditCardServiceResponse response = new CreditCardServiceResponseImpl(
				requestId, decision, reasonCode);
		response.setProcessorFormattedRespText(processorRespTxt);
		response.setProcessorTraceNbr(processorTraceNbr);
		response.setEffortId(effortId);

		return response;
	}

	protected void fundExpressPayment(final int tranId, final String authRequestId, final String userId, final String contextRoot)
			throws TransactionAlreadyInProcessException, TransactionOwnershipException {
		Validate.notNull(authRequestId, "uthRequestId is a required parameter and must not be null.");
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				final Transaction tran = tranManager.getTransaction(tranId);
				if (tran == null) {
					throw new IllegalArgumentException("Unable to retrieve transaction with id = " + tranId);
				}
				if (!tran.isFundable()) {
					throw new IllegalStateException("transaction id = " + tranId + " is not in a fundable state");
				}
				final int tranId = tran.getEmgTranId();

				ConsumerAccountType accountType = ConsumerAccountType.getInstance(tran.getSndAcctTypeCode());
				if (accountType.isBankAccount()) {
					tran.setTranSubStatCode(TransactionStatus.ACH_WAITING_CODE);
					tranManager.updateTransaction(tran, userId);
				} else if (accountType.isCardAccount()) {
					final double amount = tran.getSndTotAmt().doubleValue();
					long beginTime = System.currentTimeMillis();
					CreditCardServiceResponse creditCardResponse = null;

					final ConsumerAccountType primaryAccountType = ConsumerAccountType.getInstance(tran.getSndAcctTypeCode());
					final int creditCardAccountId = (primaryAccountType.isCardAccount() ?
							tran.getSndCustAcctId() :
							tran.getSndCustBkupAcctId());
					String currency = tran.getSndISOCrncyId();
					//long effortId = tran.getTranSeqNumber();
					long effortId = 1;
					try {
						PostTransactionResponse postTransactionResponse = CreditCardPaymentProxyImpl.getInstance().postTransaction(creditCardAccountId, amount, currency, userId, tran.getMccPartnerProfileId(), effortId, tranId);
						String reasonCode = null;

						if (postTransactionResponse.getError() != null) {
							reasonCode = postTransactionResponse.getError().getErrorCode();
						}
						creditCardResponse = new CreditCardServiceResponseImpl(postTransactionResponse.getDecision().getValue(), reasonCode);
						creditCardResponse.setProcessorFormattedRespText(postTransactionResponse.getHeader().getClientHeader().getClientSessionID());
					} catch (Exception e) {
						logger.error(e.getMessage(),e);
					}

					EventMonitor.getInstance().recordEvent(EventListConstant.GLOBAL_COLLECT_POST, beginTime, System.currentTimeMillis());
					if (creditCardResponse != null && creditCardResponse.isAccepted()) {
						String requestId = creditCardResponse.getIdentifier();
						tran.setTranSubStatCode(TransactionStatus.CC_SALE_CODE);
						tran.setTCProviderCode(Constants.PYMT_PROVIDER_CODE_GLOBAL_COLLECT);
						tranManager.updateTransaction(tran, userId, StringUtils.trimToNull(requestId), null);
					} else {
						CommentService commentService = emgshared.services.ServiceFactory.getInstance().getCommentService();
						commentService.insertCreditCardResponseComment(creditCardResponse, Integer.valueOf(tran.getSndCustAcctId()), Integer.valueOf(tranId), resourceFile, userId);
						if (creditCardResponse != null && !creditCardResponse.isReTriable()) {
							try {
								sendCreditCardFailEmail(tran);
							} catch (Exception e) {
								// Email failed to get sent.
								// Not a serious error, log and move on.
								EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Failed to send Transaction Email:", e);
							}
							tran.setTranSubStatCode(TransactionStatus.CC_FAILED_CODE);
							tranManager.updateTransaction(tran, userId, authRequestId, null);
							tran.setTranStatCode(TransactionStatus.ERROR_CODE);
							tranManager.updateTransaction(tran, userId, null, null);
						} else if (creditCardResponse != null && creditCardResponse.isReTriable()) {
							// add admin message
							String[] params = {String.valueOf(tran.getEmgTranId()), creditCardResponse.getReason(), getTransactionUrl(contextRoot)};
							sendAdminMessage(tran.getCsrPrcsUserid(), getMessageResource("error.cc.capture.fail.notice", params));
						}
					}
				}
				return null;
			}
		};
		updateTransaction(tranId, userId, command);
	}

	public ArrayList processExpressPayTransaction(int tranId, String callerLoginId, String tranStat, String fundStat,
			String contextRoot) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException, AgentConnectException {
				ArrayList<String> esl = new ArrayList<String>();
				TransactionManager tm = ManagerFactory.createTransactionManager();
				Transaction tran = ManagerFactory.createTransactionManager().getTransaction(tranId);

				try {
					VendorOperation globalCollectAuthorize = new GlobalCollectAuthorize();
					tran = globalCollectAuthorize.execute(tranId, callerLoginId,
							contextRoot);
				} catch (EMGRuntimeException e) {
					e.printStackTrace();
				}

				if (tran.getTranStatCode().equals(TransactionStatus.APPROVED_CODE)
						&& tran.getTranSubStatCode().equals(TransactionStatus.CC_AUTH_CODE) && tran.isSendable()) {
					try {
						sendExpressPayment(tranId, callerLoginId, contextRoot);
						tran = tm.getTransaction(tranId);
					} catch (AgentConnectException e) {
						
						esl.add("error.agent.connect.failed");
						tran.setCsrPrcsUserid(null);
						tm.updateTransaction(tran, callerLoginId);
						return esl;
					}
				}
				// Get funding (CC Capture or ACH Waiting) with status of SEN/CCA
				if (TransactionStatus.SENT_CODE.equals(tran.getTranStatCode()) && TransactionStatus.CC_AUTH_CODE.equals(tran.getTranSubStatCode())) {
					// find conf. code from tran action table with reason code of 28
					String ccAuthConfReasonCode = "28";
					String AuthConfCode;
					try {
						AuthConfCode = tm.getCCAuthConfId(tranId, ccAuthConfReasonCode);
						fundExpressPayment(tranId, AuthConfCode, callerLoginId, contextRoot);
					} catch (Exception e) {
						esl.add(new String("Failed CC Auth Retrieval"));
					}
				}
				return null;
			}

	public CreditCardServiceResponse executeCreditCardAuth(int tranId, String userId,
			String contextRoot) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		// TODO Global Collect does not need this method, functionality was pulled out to VendorOperation Object

		return null;
	}
	
	private Integer getGCTxnStatus(String requestId, long accountId,
			String merchantId, int tranId, String currency) throws Exception {

		try {

			//MBO-1704
			final Transaction tran = tranManager.getTransaction(tranId);
			final long effortId = tran.getTranSeqNumber();
			
			if (tran == null) {
				throw new IllegalArgumentException("unable to retrieve transaction with id = "+ tranId);
			}
			
			//MBO-1704
            long orderId = tranId;
			String strOrderId = tran.getTCProviderTransactionNumber();
    		System.out.println("Order Id for Txn is " + strOrderId);
	    	if(null!=strOrderId && !"".equals(strOrderId.trim())) {
	    	  orderId = Long.valueOf(strOrderId);
	    	}
	    	else		    	 
	    		orderId = tranId;
	    	
			StatusResponse statusResponse = CreditCardPaymentProxyImpl
					.getInstance().status(requestId, accountId, 1, tranId,
							merchantId, currency, orderId);
	    	//ended 1704

			if (statusResponse == null || statusResponse.getError() != null
					|| statusResponse.getGlobalCollect() == null
					|| statusResponse.getGlobalCollect().getStatusId() == null) {
				return null; // indicates error
			}

			return statusResponse.getGlobalCollect().getStatusId();

		} catch (Exception e) {
			throw new EMGRuntimeException(e);
		}
	}


}
