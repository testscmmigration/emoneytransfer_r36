/**
 * ResponseArrayRecordIP.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package emgadm.services.iplookup.melissadata.client;

public class IP  implements java.io.Serializable {
    private java.lang.String latitude;

    private java.lang.String longitude;

    private java.lang.String zip;

    private java.lang.String region;

    private emgadm.services.iplookup.melissadata.client.ISP ISP;

    private emgadm.services.iplookup.melissadata.client.Domain domain;

    private emgadm.services.iplookup.melissadata.client.City city;

    private emgadm.services.iplookup.melissadata.client.Country country;

    public IP() {
    }

    public IP(
           java.lang.String latitude,
           java.lang.String longitude,
           java.lang.String zip,
           java.lang.String region,
           emgadm.services.iplookup.melissadata.client.ISP ISP,
           emgadm.services.iplookup.melissadata.client.Domain domain,
           emgadm.services.iplookup.melissadata.client.City city,
           emgadm.services.iplookup.melissadata.client.Country country) {
           this.latitude = latitude;
           this.longitude = longitude;
           this.zip = zip;
           this.region = region;
           this.ISP = ISP;
           this.domain = domain;
           this.city = city;
           this.country = country;
    }


    /**
     * Gets the latitude value for this ResponseArrayRecordIP.
     * 
     * @return latitude
     */
    public java.lang.String getLatitude() {
        return latitude;
    }


    /**
     * Sets the latitude value for this ResponseArrayRecordIP.
     * 
     * @param latitude
     */
    public void setLatitude(java.lang.String latitude) {
        this.latitude = latitude;
    }


    /**
     * Gets the longitude value for this ResponseArrayRecordIP.
     * 
     * @return longitude
     */
    public java.lang.String getLongitude() {
        return longitude;
    }


    /**
     * Sets the longitude value for this ResponseArrayRecordIP.
     * 
     * @param longitude
     */
    public void setLongitude(java.lang.String longitude) {
        this.longitude = longitude;
    }


    /**
     * Gets the zip value for this ResponseArrayRecordIP.
     * 
     * @return zip
     */
    public java.lang.String getZip() {
        return zip;
    }


    /**
     * Sets the zip value for this ResponseArrayRecordIP.
     * 
     * @param zip
     */
    public void setZip(java.lang.String zip) {
        this.zip = zip;
    }


    /**
     * Gets the region value for this ResponseArrayRecordIP.
     * 
     * @return region
     */
    public java.lang.String getRegion() {
        return region;
    }


    /**
     * Sets the region value for this ResponseArrayRecordIP.
     * 
     * @param region
     */
    public void setRegion(java.lang.String region) {
        this.region = region;
    }


    /**
     * Gets the ISP value for this ResponseArrayRecordIP.
     * 
     * @return ISP
     */
    public emgadm.services.iplookup.melissadata.client.ISP getISP() {
        return ISP;
    }


    /**
     * Sets the ISP value for this ResponseArrayRecordIP.
     * 
     * @param ISP
     */
    public void setISP(emgadm.services.iplookup.melissadata.client.ISP ISP) {
        this.ISP = ISP;
    }


    /**
     * Gets the domain value for this ResponseArrayRecordIP.
     * 
     * @return domain
     */
    public emgadm.services.iplookup.melissadata.client.Domain getDomain() {
        return domain;
    }


    /**
     * Sets the domain value for this ResponseArrayRecordIP.
     * 
     * @param domain
     */
    public void setDomain(emgadm.services.iplookup.melissadata.client.Domain domain) {
        this.domain = domain;
    }


    /**
     * Gets the city value for this ResponseArrayRecordIP.
     * 
     * @return city
     */
    public emgadm.services.iplookup.melissadata.client.City getCity() {
        return city;
    }


    /**
     * Sets the city value for this ResponseArrayRecordIP.
     * 
     * @param city
     */
    public void setCity(emgadm.services.iplookup.melissadata.client.City city) {
        this.city = city;
    }


    /**
     * Gets the country value for this ResponseArrayRecordIP.
     * 
     * @return country
     */
    public emgadm.services.iplookup.melissadata.client.Country getCountry() {
        return country;
    }


    /**
     * Sets the country value for this ResponseArrayRecordIP.
     * 
     * @param country
     */
    public void setCountry(emgadm.services.iplookup.melissadata.client.Country country) {
        this.country = country;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof IP)) return false;
        IP other = (IP) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.latitude==null && other.getLatitude()==null) || 
             (this.latitude!=null &&
              this.latitude.equals(other.getLatitude()))) &&
            ((this.longitude==null && other.getLongitude()==null) || 
             (this.longitude!=null &&
              this.longitude.equals(other.getLongitude()))) &&
            ((this.zip==null && other.getZip()==null) || 
             (this.zip!=null &&
              this.zip.equals(other.getZip()))) &&
            ((this.region==null && other.getRegion()==null) || 
             (this.region!=null &&
              this.region.equals(other.getRegion()))) &&
            ((this.ISP==null && other.getISP()==null) || 
             (this.ISP!=null &&
              this.ISP.equals(other.getISP()))) &&
            ((this.domain==null && other.getDomain()==null) || 
             (this.domain!=null &&
              this.domain.equals(other.getDomain()))) &&
            ((this.city==null && other.getCity()==null) || 
             (this.city!=null &&
              this.city.equals(other.getCity()))) &&
            ((this.country==null && other.getCountry()==null) || 
             (this.country!=null &&
              this.country.equals(other.getCountry())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLatitude() != null) {
            _hashCode += getLatitude().hashCode();
        }
        if (getLongitude() != null) {
            _hashCode += getLongitude().hashCode();
        }
        if (getZip() != null) {
            _hashCode += getZip().hashCode();
        }
        if (getRegion() != null) {
            _hashCode += getRegion().hashCode();
        }
        if (getISP() != null) {
            _hashCode += getISP().hashCode();
        }
        if (getDomain() != null) {
            _hashCode += getDomain().hashCode();
        }
        if (getCity() != null) {
            _hashCode += getCity().hashCode();
        }
        if (getCountry() != null) {
            _hashCode += getCountry().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(IP.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", ">>ResponseArray>Record>IP"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latitude");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "Latitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longitude");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "Longitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zip");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "Zip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("region");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "Region"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ISP");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "ISP"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", ">>>ResponseArray>Record>IP>ISP"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("domain");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "Domain"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", ">>>ResponseArray>Record>IP>Domain"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("city");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "City"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", ">>>ResponseArray>Record>IP>City"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "Country"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", ">>>ResponseArray>Record>IP>Country"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
