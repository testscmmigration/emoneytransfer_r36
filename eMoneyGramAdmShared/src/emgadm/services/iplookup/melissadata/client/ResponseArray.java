/**
 * ResponseArray.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package emgadm.services.iplookup.melissadata.client;

public class ResponseArray  implements java.io.Serializable {
    private java.lang.String version;

    private java.lang.String transmissionReference;

    private java.lang.String results;

    private java.lang.String totalRecords;

    private emgadm.services.iplookup.melissadata.client.ResponseRecord[] record;

    public ResponseArray() {
    }

    public ResponseArray(
           java.lang.String version,
           java.lang.String transmissionReference,
           java.lang.String results,
           java.lang.String totalRecords,
           emgadm.services.iplookup.melissadata.client.ResponseRecord[] record) {
           this.version = version;
           this.transmissionReference = transmissionReference;
           this.results = results;
           this.totalRecords = totalRecords;
           this.record = record;
    }


    /**
     * Gets the version value for this ResponseArray.
     * 
     * @return version
     */
    public java.lang.String getVersion() {
        return version;
    }


    /**
     * Sets the version value for this ResponseArray.
     * 
     * @param version
     */
    public void setVersion(java.lang.String version) {
        this.version = version;
    }


    /**
     * Gets the transmissionReference value for this ResponseArray.
     * 
     * @return transmissionReference
     */
    public java.lang.String getTransmissionReference() {
        return transmissionReference;
    }


    /**
     * Sets the transmissionReference value for this ResponseArray.
     * 
     * @param transmissionReference
     */
    public void setTransmissionReference(java.lang.String transmissionReference) {
        this.transmissionReference = transmissionReference;
    }


    /**
     * Gets the results value for this ResponseArray.
     * 
     * @return results
     */
    public java.lang.String getResults() {
        return results;
    }


    /**
     * Sets the results value for this ResponseArray.
     * 
     * @param results
     */
    public void setResults(java.lang.String results) {
        this.results = results;
    }


    /**
     * Gets the totalRecords value for this ResponseArray.
     * 
     * @return totalRecords
     */
    public java.lang.String getTotalRecords() {
        return totalRecords;
    }


    /**
     * Sets the totalRecords value for this ResponseArray.
     * 
     * @param totalRecords
     */
    public void setTotalRecords(java.lang.String totalRecords) {
        this.totalRecords = totalRecords;
    }


    /**
     * Gets the record value for this ResponseArray.
     * 
     * @return record
     */
    public emgadm.services.iplookup.melissadata.client.ResponseRecord[] getRecord() {
        return record;
    }


    /**
     * Sets the record value for this ResponseArray.
     * 
     * @param record
     */
    public void setRecord(emgadm.services.iplookup.melissadata.client.ResponseRecord[] record) {
        this.record = record;
    }

    public emgadm.services.iplookup.melissadata.client.ResponseRecord getRecord(int i) {
        return this.record[i];
    }

    public void setRecord(int i, emgadm.services.iplookup.melissadata.client.ResponseRecord _value) {
        this.record[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ResponseArray)) return false;
        ResponseArray other = (ResponseArray) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.version==null && other.getVersion()==null) || 
             (this.version!=null &&
              this.version.equals(other.getVersion()))) &&
            ((this.transmissionReference==null && other.getTransmissionReference()==null) || 
             (this.transmissionReference!=null &&
              this.transmissionReference.equals(other.getTransmissionReference()))) &&
            ((this.results==null && other.getResults()==null) || 
             (this.results!=null &&
              this.results.equals(other.getResults()))) &&
            ((this.totalRecords==null && other.getTotalRecords()==null) || 
             (this.totalRecords!=null &&
              this.totalRecords.equals(other.getTotalRecords()))) &&
            ((this.record==null && other.getRecord()==null) || 
             (this.record!=null &&
              java.util.Arrays.equals(this.record, other.getRecord())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVersion() != null) {
            _hashCode += getVersion().hashCode();
        }
        if (getTransmissionReference() != null) {
            _hashCode += getTransmissionReference().hashCode();
        }
        if (getResults() != null) {
            _hashCode += getResults().hashCode();
        }
        if (getTotalRecords() != null) {
            _hashCode += getTotalRecords().hashCode();
        }
        if (getRecord() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRecord());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRecord(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ResponseArray.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "ResponseArray"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("version");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "Version"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transmissionReference");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "TransmissionReference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("results");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "Results"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalRecords");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "TotalRecords"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("record");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "Record"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", ">ResponseArray>Record"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
