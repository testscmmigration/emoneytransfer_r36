package emgadm.services.iplookup.melissadata.client;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.rmi.RemoteException;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.owasp.esapi.ESAPI;

import javax.xml.rpc.ServiceException;



import emgadm.property.EMTAdmContainerProperties;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.model.IPDetails;

public class IPLocatorService {

	static RequestArray request;
	static ResponseArray response;
	static IService mdWebService;
	static List<RequestRecord> reqArray;
	static Logger logger = EMGSharedLogger.getLogger(
			IPLocatorService.class);
	private IPLocatorService() {
	}

	
	private static JSONObject doIpLocator(boolean isIpLookuponScoring,
			String ipAddress) throws IPLocatorException, IOException {

		InputStream is = null;
		JSONObject responseJSON = null;
		try {
			String url = EMTAdmContainerProperties.getIpLookUpServiceURL()
					+ "?id=" + EMTAdmContainerProperties.getIpLookUpCustomerID()
					+ "&t=MoneyGram_IP_location" + "&ip=" + ipAddress;
			logger.info("End Point ::::" + ESAPI.encoder().decodeFromURL(ESAPI.encoder().encodeForURL(url)));
			is = new URL(url).openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is,
					Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			responseJSON = new JSONObject(jsonText);
			logger.info("-------Retrived data from Mellisa service successfully");
		} catch (MalformedURLException e) {
			throw new IPLocatorException(
					"Wrong URL to invoke IP location service", e);
		} catch (Exception e) {
			throw new IPLocatorException(e);
		} finally {
			if(is!=null){
				is.close();
			}
		}
		return responseJSON;
	}

	/**
	 * Obtains information about specified IP addesses
	 * @param ipAddresses the IP addresses to check
	 * @return a response array container with location information about each address received
	 * @throws IPLocatorException in case something fails during execution of the service
	 * @throws IOException 
	 */
	public static JSONObject ipLocate(boolean isIpLookuponScoring, String ipAddresses) throws IPLocatorException, IOException {
		ResponseArray respArray = new ResponseArray();
		return doIpLocator(isIpLookuponScoring, ipAddresses);
	}

	public static String getFormattedStringForIpLookup(String ipAddress, boolean isIpLookuponScoring ) {
		if (ipAddress == null) {
			return "Error: No IP recorder for transaction";
		}
		JSONArray records = null;
		JSONObject result = null;
		StringBuilder message = new StringBuilder();
		try {
			JSONObject response = IPLocatorService.ipLocate(isIpLookuponScoring, ipAddress);
			records = response.getJSONArray("Records");
			result = records.getJSONObject(0);	
			//ResponseRecord record = response.getRecord(0);
			message.append("IP Address: " + ipAddress);
			message.append(", Country: " + result.get("CountryName"));
			message.append(", Region: " + result.get("Region"));
			message.append(", City: " + result.get("City"));
			message.append(", Zip Code: " +  result.get("PostalCode"));
			message.append(", Provider: " + result.get("ISPName"));
	    } catch(Exception e) {
	    	message.append("N/A");
	    	// TODO: Log
	    }
	    return message.toString();
	}
	
	public static IPDetails getIPDetailsFromLookupService(String ipAddress,
			boolean isIpLookuponScoring) {
		
		
		
		logger.info("========== Inside getIPDetailsFromLookupService ======= ");
		logger.info("========== IP Address =  " + ESAPI.encoder().encodeForHTMLAttribute(ipAddress.trim())+"  ===============");

		IPDetails ipDetails = new IPDetails();
		JSONObject jsonResponse = null;		
		JSONArray records = null;
		try {
			jsonResponse = doIpLocator(isIpLookuponScoring, ipAddress);
			records = jsonResponse.getJSONArray("Records");
			 
		} catch (Exception iPLocatorException) {
			logger.error("MelissaException", iPLocatorException);
			ipDetails.setConsumerCity("System Error");
			ipDetails.setConsumerCountry("System Error");
			ipDetails.setConsumerDomain("System Error");
			ipDetails.setConsumerISP("System Error");
			ipDetails.setConsumerRegion("System Error");
			ipDetails.setConsumerZipCode("System Error");
			ipDetails.setCountryCode("System Error");
			return ipDetails;
		}

		if (jsonResponse != null && jsonResponse.length() != 0 && records.length() != 0) {
						
		    JSONObject result;
			try {
				result = records.getJSONObject(0);	
				String ispName = result.get("ISPName").toString();
				if (ispName.length() > 120) {
					ispName = ispName.substring(0, 120);
				}

				logger.info("JSON Response ======= " + ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTML(jsonResponse.toString())));

				logger.info("..................");
			logger.info("IpAddress : " + result.get("IPAddress"));
			logger.info("Country : "	+ result.get("CountryName"));			
			logger.info("City : " + result.get("City"));
			logger.info("Zip Code : " + result.get("PostalCode"));
			logger.info("Provider : " + result.get("ISPName"));
			logger.info("Country Code : "	+ result.get("CountryAbbreviation"));
			logger.info("..................");

			ipDetails.setConsumerISP(ispName);
			ipDetails.setConsumerDomain(result.get("DomainName").toString());
			ipDetails.setConsumerCountry(result.get("CountryName").toString());
			ipDetails.setConsumerCity(result.get("City").toString());
			ipDetails.setConsumerZipCode(result.get("PostalCode").toString());
			ipDetails.setCountryCode(result.get("CountryAbbreviation").toString());
			ipDetails.setConsumerRegion(result.get("Region").toString());
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				logger.error("Exception in Parsing JSON response", e);
			} 
		} else {
			ipDetails.setConsumerCity("System Error");
			ipDetails.setConsumerCountry("System Error");
			ipDetails.setConsumerDomain("System Error");
			ipDetails.setConsumerISP("System Error");
			ipDetails.setConsumerRegion("System Error");
			ipDetails.setConsumerZipCode("System Error");
			ipDetails.setCountryCode("System Error");
		}

		return ipDetails;
	}
	
	
	private static String readAll(Reader rd) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rd.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	  }

}

