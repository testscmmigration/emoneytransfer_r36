/**
 * RequestArray.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package emgadm.services.iplookup.melissadata.client;

public class RequestArray  implements java.io.Serializable {
    private java.lang.String transmissionReference;

    private java.lang.String customerID;

    private emgadm.services.iplookup.melissadata.client.RequestRecord[] record;

    public RequestArray() {
    }

    public RequestArray(
           java.lang.String transmissionReference,
           java.lang.String customerID,
           emgadm.services.iplookup.melissadata.client.RequestRecord[] record) {
           this.transmissionReference = transmissionReference;
           this.customerID = customerID;
           this.record = record;
    }


    /**
     * Gets the transmissionReference value for this RequestArray.
     * 
     * @return transmissionReference
     */
    public java.lang.String getTransmissionReference() {
        return transmissionReference;
    }


    /**
     * Sets the transmissionReference value for this RequestArray.
     * 
     * @param transmissionReference
     */
    public void setTransmissionReference(java.lang.String transmissionReference) {
        this.transmissionReference = transmissionReference;
    }


    /**
     * Gets the customerID value for this RequestArray.
     * 
     * @return customerID
     */
    public java.lang.String getCustomerID() {
        return customerID;
    }


    /**
     * Sets the customerID value for this RequestArray.
     * 
     * @param customerID
     */
    public void setCustomerID(java.lang.String customerID) {
        this.customerID = customerID;
    }


    /**
     * Gets the record value for this RequestArray.
     * 
     * @return record
     */
    public emgadm.services.iplookup.melissadata.client.RequestRecord[] getRecord() {
        return record;
    }


    /**
     * Sets the record value for this RequestArray.
     * 
     * @param record
     */
    public void setRecord(emgadm.services.iplookup.melissadata.client.RequestRecord[] record) {
        this.record = record;
    }

    public emgadm.services.iplookup.melissadata.client.RequestRecord getRecord(int i) {
        return this.record[i];
    }

    public void setRecord(int i, emgadm.services.iplookup.melissadata.client.RequestRecord _value) {
        this.record[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RequestArray)) return false;
        RequestArray other = (RequestArray) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.transmissionReference==null && other.getTransmissionReference()==null) || 
             (this.transmissionReference!=null &&
              this.transmissionReference.equals(other.getTransmissionReference()))) &&
            ((this.customerID==null && other.getCustomerID()==null) || 
             (this.customerID!=null &&
              this.customerID.equals(other.getCustomerID()))) &&
            ((this.record==null && other.getRecord()==null) || 
             (this.record!=null &&
              java.util.Arrays.equals(this.record, other.getRecord())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTransmissionReference() != null) {
            _hashCode += getTransmissionReference().hashCode();
        }
        if (getCustomerID() != null) {
            _hashCode += getCustomerID().hashCode();
        }
        if (getRecord() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRecord());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRecord(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RequestArray.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "RequestArray"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transmissionReference");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "TransmissionReference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerID");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "CustomerID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("record");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "Record"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", ">RequestArray>Record"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
