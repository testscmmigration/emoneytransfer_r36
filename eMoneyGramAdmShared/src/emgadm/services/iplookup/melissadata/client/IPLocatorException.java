package emgadm.services.iplookup.melissadata.client;

public class IPLocatorException extends Exception {
	public IPLocatorException(String msg) {
		super(msg);
	}

	public IPLocatorException(String msg, Throwable t) {
		super(msg, t);
	}

	public IPLocatorException(Throwable t) {
		super(t);
	}
}
