package emgadm.services;

import java.lang.reflect.InvocationTargetException;

import com.moneygram.mgo.service.consumerValidationV2_1.RiskIndicator;

import emgadm.model.BPSResultUnit;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.ConsumerProfile;
import emgshared.model.LexisNexisActivity;

public interface LexNexService {

	public void getPersonSearch(ConsumerProfile consumerProfile, int custId,
			String ipAddress) throws Exception;

	public java.util.LinkedHashMap<String, BPSResultUnit> getBPSSearchResults(
			Long searchId, String userId) throws Exception;

	public String getHighRiskIndList(RiskIndicator[] riskIndicatorArr)
			throws Exception;

	public String serializeResponseObject(Object responseObj,
			boolean removeNamespaces, boolean prettyPrint)
			throws SecurityException, IllegalArgumentException,
			NoSuchMethodException, IllegalAccessException,
			InvocationTargetException;

	public void insertLexisNexisActivityLog(ConsumerProfile consumerProfile,
			LexisNexisActivity activity) throws DataSourceException;

	public void getRelativeSearch(ConsumerProfile consumerProfile, int custId,
			String uniqueId, String ipAddress) throws Exception;
	//3514
	public emgadm.model.relativessearch.RelativeSearchResponse getRelativesSearchResults(
			Long searchId, String userId) throws Exception;
//3514
}
