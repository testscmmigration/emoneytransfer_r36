package emgadm.services;

import java.net.URL;




import com.moneygram.common.service.ClientHeader;  
import com.moneygram.common.service.Header;
import com.moneygram.common.service.ProcessingInstruction;
import com.moneygram.mgo.service.consumer_v4_0_7_2.GetConsumerIDApprovalStatusRequest;
import com.moneygram.mgo.service.consumer_v4_0_7_2.GetConsumerIDApprovalStatusResponse;
import com.moneygram.mgo.service.consumer_v4_0_7_2.GetConsumerIDImageRequest;
import com.moneygram.mgo.service.consumer_v4_0_7_2.GetConsumerIDImageResponse;
import com.moneygram.mgo.service.consumer_v4_0_7_2.GetConsumerIDUploadDynamicDataRequest;
import com.moneygram.mgo.service.consumer_v4_0_7_2.GetConsumerIDUploadDynamicDataResponse;
import com.moneygram.mgo.service.consumer_v4_0_7_2.GetConsumerProfileRequest;
import com.moneygram.mgo.service.consumer_v4_0_7_2.GetConsumerProfileResponse;
import com.moneygram.mgo.service.consumer_v4_0_7_2.HasImageUploadedRequest;
import com.moneygram.mgo.service.consumer_v4_0_7_2.HasImageUploadedResponse;
import com.moneygram.mgo.service.consumer_v4_0_7_2.MGOConsumerServicePortType_v4_0_7_2;
import com.moneygram.mgo.service.consumer_v4_0_7_2.MGOConsumerService_v4_0_7_2SoapBindingStub;
import com.moneygram.mgo.service.consumer_v4_0_7_2.ProfilePart;
import com.moneygram.mgo.service.consumer_v4_0_7_2.UpdateConsumerProfileRequest;
import com.moneygram.mgo.service.consumer_v4_0_7_2.UpdateConsumerProfileResponse;
import emgadm.property.EMTAdmContainerProperties;

public class ConsumerServiceProxyImpl implements ConsumerServiceProxy {

	private static final ConsumerServiceProxyImpl instance = new ConsumerServiceProxyImpl();
	private static final String GET_CONSUMER_ID_APPROVAL_STATUS_ACTION = "getConsumerIDApprovalStatus_v4_0_7_2";
	private static final String GET_CONSUMER_ID_IMAGE_ACTION= "getConsumerIDImage_v4_0_7_2";
	private static final String UPDATE_CONSUMER_PROFILE_ACTION = "updateConsumerProfile_v4_0_7_2";
	private static final String HAS_UPLOADED_IMAGE_ACTION = "hasUploadedImage_v4_0_7_2";  
	private static final String GET_CONSUMER_ID_UPLOAD_DATA_ACTION = "getConsumerIDUploadDynamicData_v4_0_7_2";
	private static final String GET_CONSUMER_PROFILEID_ACTION = "getConsumerProfile_v4_0_7_2";
	
	private static final String PHOTOID_PROFILE_PART = "PersonalAndPhotoID";
	
	private ConsumerServiceProxyImpl()
	{
	}

	public static final ConsumerServiceProxyImpl getInstance()
	{
		return instance;
	}
	
	public GetConsumerIDApprovalStatusResponse getConsumerIDApprovalStatusResponse(
		long customerId, String partnerSiteId) throws Exception {
		
		//String url = "http://q3wsintsvcs.qacorp.moneygram.com/MGOService/services/MGOConsumerService_v3?wsdl";
		String url = EMTAdmContainerProperties.getConsumerServiceUrl();
			
		URL consumerServiceURL = new URL(url);
		
		MGOConsumerServicePortType_v4_0_7_2 consumerService = 
			new MGOConsumerService_v4_0_7_2SoapBindingStub(consumerServiceURL, null);
		
		GetConsumerIDApprovalStatusRequest idApprovalStatusRequest;
		idApprovalStatusRequest = new GetConsumerIDApprovalStatusRequest();
		idApprovalStatusRequest.setCustId(customerId);
		
		//FIXME For now these are set to 0, do we need this?  Since the external id is 
		// available above via form.setIdNumber(id);
		idApprovalStatusRequest.setEncryptExtnlId("");
		idApprovalStatusRequest.setMaskExtnlId("");
		
		Header header = new Header();
		ProcessingInstruction pi = new ProcessingInstruction();
		pi.setAction(GET_CONSUMER_ID_APPROVAL_STATUS_ACTION);
		
		pi.setReadOnlyFlag(false);
		pi.setPartnerSiteId(partnerSiteId);
		
		header.setProcessingInstruction(pi);
		ClientHeader ch = new ClientHeader();
		ch.setClientRequestID(String.valueOf(customerId) + 
				String.valueOf(new java.util.Date().getTime()));
		header.setClientHeader(ch);
		idApprovalStatusRequest.setHeader(header);
		
		GetConsumerIDApprovalStatusResponse idApprovalStatusResponse;
		idApprovalStatusResponse = consumerService.getConsumerIDApprovalStatus(idApprovalStatusRequest);
		
		return idApprovalStatusResponse;
		
	}

	public GetConsumerIDImageResponse getConsumerIDImageResponse(
			long customerId, String partnerSiteId) throws Exception {
		
		//String url = "http://q3wsintsvcs.qacorp.moneygram.com/MGOService/services/MGOConsumerService_v3?wsdl";
		String url = EMTAdmContainerProperties.getConsumerServiceUrl();
    	
		URL consumerServiceURL = new URL(url);

		MGOConsumerServicePortType_v4_0_7_2 consumerService;
		consumerService = new MGOConsumerService_v4_0_7_2SoapBindingStub(consumerServiceURL, null);
		
		// Obtain id image
		GetConsumerIDImageRequest idImageRequest = new GetConsumerIDImageRequest();
		
		//FIXME should consumerId type be changed to long
		idImageRequest.setCustomerExternalId(customerId);
		
		Header header = new Header();
		ProcessingInstruction pi = new ProcessingInstruction();
		pi.setAction(GET_CONSUMER_ID_IMAGE_ACTION);
		pi.setPartnerSiteId(partnerSiteId);
		pi.setReadOnlyFlag(false);
		
		header.setProcessingInstruction(pi);
		ClientHeader ch = new ClientHeader();
		ch.setClientRequestID(String.valueOf(customerId) + 
				"-" + String.valueOf(new java.util.Date().getTime()));
		header.setClientHeader(ch);
		idImageRequest.setHeader(header);
	
		GetConsumerIDImageResponse idImageResponse = 
			consumerService.getConsumerIDImage(idImageRequest);
		
		return idImageResponse;
	}
	public UpdateConsumerProfileResponse updateConsumerProfile(
			UpdateConsumerProfileRequest updateConsumerProfileRequest,
			String partnerSiteId) throws Exception {
		// Added new method for MGO-9024
		Header header = new Header();
		
		ProcessingInstruction processingInstruction = new ProcessingInstruction();
		processingInstruction.setAction(UPDATE_CONSUMER_PROFILE_ACTION);
		processingInstruction.setReadOnlyFlag(false);

		header.setProcessingInstruction(processingInstruction);

		updateConsumerProfileRequest.setHeader(header);

		UpdateConsumerProfileResponse updateConsumerProfileResponse =new MGOConsumerService_v4_0_7_2SoapBindingStub(
				new URL(EMTAdmContainerProperties.getConsumerServiceUrl()),
				null).update(updateConsumerProfileRequest);
		
		return updateConsumerProfileResponse;
	}

	public boolean hasUploadedImage(String customerId, String partnerSiteId)
			throws Exception {
		String url = EMTAdmContainerProperties.getConsumerServiceUrl();
		URL consumerServiceURL = new URL(url);
		MGOConsumerServicePortType_v4_0_7_2 consumerService;
		consumerService = new MGOConsumerService_v4_0_7_2SoapBindingStub(
				consumerServiceURL, null);
		HasImageUploadedRequest imgUploadedReq = new HasImageUploadedRequest();
		Header header = new Header();
		ProcessingInstruction pi = new ProcessingInstruction();
		pi.setAction(HAS_UPLOADED_IMAGE_ACTION);
		pi.setPartnerSiteId(partnerSiteId);
		pi.setReadOnlyFlag(false);

		header.setProcessingInstruction(pi);
		ClientHeader ch = new ClientHeader();
		ch.setClientRequestID(String.valueOf(customerId) + "-"
				+ String.valueOf(new java.util.Date().getTime()));
		header.setClientHeader(ch);
		imgUploadedReq.setHeader(header);
		imgUploadedReq.setCustId(customerId);
		HasImageUploadedResponse imgUplodResponse = consumerService
				.hasUploadedImage(imgUploadedReq);
		return imgUplodResponse != null ? imgUplodResponse.isIsImgUploaded()
				: Boolean.FALSE;
	}
	
	public GetConsumerProfileResponse getConsumerProfile(
			Long consumerProfileId,String sourceSiteId) throws Exception {
		// TODO Auto-generated method stub
		
		Header header = new Header();
		
		ProcessingInstruction processingInstruction = new ProcessingInstruction();
	
		processingInstruction.setAction(GET_CONSUMER_PROFILEID_ACTION);   
		processingInstruction.setReadOnlyFlag(false);
		processingInstruction.setPartnerSiteId(sourceSiteId);

		header.setProcessingInstruction(processingInstruction);
		
		GetConsumerProfileRequest getConsumerProfileRequest = new GetConsumerProfileRequest();

		getConsumerProfileRequest.setHeader(header);
		getConsumerProfileRequest.setConsumerId(consumerProfileId);
		
		ProfilePart[] profilePart = new ProfilePart[]{
        		ProfilePart.Accounts, ProfilePart.Address , ProfilePart.Contact,
        		ProfilePart.PersonalInfo, ProfilePart.IncludeGuestProfiles , ProfilePart.IncludeGuestPaymentAccounts,
        		ProfilePart.Internal, ProfilePart.PersonalAndPhotoID , ProfilePart.ProfileEvents, ProfilePart.LoyaltyInfo};
		
		getConsumerProfileRequest.setResponseFilter(profilePart);

		GetConsumerProfileResponse getConsumerProfileResponse =new MGOConsumerService_v4_0_7_2SoapBindingStub(
				new URL(EMTAdmContainerProperties.getConsumerServiceUrl()),
				null).get(getConsumerProfileRequest);
		
		return getConsumerProfileResponse;
	}

	public GetConsumerIDUploadDynamicDataResponse getConsumerIDUploadData(
			String mgiIdentDocUploadRefId) throws Exception {
		// TODO Auto-generated method stub
		
		Header header = new Header();
		
		ProcessingInstruction processingInstruction = new ProcessingInstruction();
	
		processingInstruction.setAction(GET_CONSUMER_ID_UPLOAD_DATA_ACTION);
		processingInstruction.setReadOnlyFlag(false);

		header.setProcessingInstruction(processingInstruction);
		
		GetConsumerIDUploadDynamicDataRequest getConsumerIDUploadDynamicDataRequest = new GetConsumerIDUploadDynamicDataRequest();

		getConsumerIDUploadDynamicDataRequest.setHeader(header);
		getConsumerIDUploadDynamicDataRequest.setMGIIdentDocUploadRefId(mgiIdentDocUploadRefId);

		GetConsumerIDUploadDynamicDataResponse getConsumerIDUploadDynamicDataResponse =new MGOConsumerService_v4_0_7_2SoapBindingStub(
				new URL(EMTAdmContainerProperties.getConsumerServiceUrl()),
				null).getConsumerIDUploadDynamicData(getConsumerIDUploadDynamicDataRequest);
		
		return getConsumerIDUploadDynamicDataResponse;
	}
}
