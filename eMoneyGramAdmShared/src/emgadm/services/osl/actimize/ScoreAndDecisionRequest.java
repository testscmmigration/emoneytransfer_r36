package emgadm.services.osl.actimize;

public class ScoreAndDecisionRequest {

	private String transactionId;
	private String sourceApplication;
	private boolean otpInSession;
	private boolean idUploadedInSession;

	
	public ScoreAndDecisionRequest() {
		this.sourceApplication = "EMT";
		this.otpInSession = Boolean.FALSE;
		this.idUploadedInSession = Boolean.FALSE;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getSourceApplication() {
		return sourceApplication;
	}

	public void setSourceApplication(String sourceApplication) {
		this.sourceApplication = sourceApplication;
	}

	public boolean isOtpInSession() {
		return otpInSession;
	}

	public void setOtpInSession(boolean otpInSession) {
		this.otpInSession = otpInSession;
	}

	public boolean isIdUploadedInSession() {
		return idUploadedInSession;
	}

	public void setIdUploadedInSession(boolean idUploadedInSession) {
		this.idUploadedInSession = idUploadedInSession;
	}

}
