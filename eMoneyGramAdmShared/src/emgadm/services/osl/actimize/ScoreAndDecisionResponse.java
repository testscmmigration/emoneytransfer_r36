package emgadm.services.osl.actimize;

import emgadm.services.osl.ucpcreate.ServiceErrorOSL;

public class ScoreAndDecisionResponse {
	private int status;
	private String decision;
	private String score;
	private ServiceErrorOSL error;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDecision() {
		return decision;
	}

	public void setDecision(String decision) {
		this.decision = decision;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public ServiceErrorOSL getError() {
		return error;
	}

	public void setError(ServiceErrorOSL error) {
		this.error = error;
	}

}
