package emgadm.services.osl;

import emgadm.services.osl.actimize.ScoreAndDecisionRequest;
import emgadm.services.osl.actimize.ScoreAndDecisionResponse;
import emgadm.services.osl.ucpcreate.CreateOrUpdateUCPResponseOSL;
import emgshared.model.ConsumerProfile;

/**
 * This is the proxy interface to connect with OSL service. UCP-101
 * 
 */
public interface OSLProxy {

	/**
	 * This method converts the ConsumeProfileOSL into JSON request and posts it
	 * to OSL api 'ucpCreate', to Create or Update MDM profile.
	 * 
	 * @param consumer
	 * @return CreateOrUpdateUCPResponseOSL
	 * @throws ProxyException
	 */
	public CreateOrUpdateUCPResponseOSL createOrUpdateUCPProfile(
			ConsumerProfile consumer, String newPlusNumber) throws OSLException;
	
	/**
	 * THis mehod to call OSL actimize score and decision with source application EMT
	 * @param scoreAndDecisionRequest
	 * @return
	 * @throws OSLException
	 */
	public ScoreAndDecisionResponse getActimizeScoreAndDecision(
			ScoreAndDecisionRequest scoreAndDecisionRequest)
			throws OSLException;
}
