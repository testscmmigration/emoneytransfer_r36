package emgadm.services.osl.ucpcreate;

public class BankAccountOSL {
	private String accountNumber;
	private String encryptedAccountNumber;
	private String routingNumber;
	private ConsumerIdentificationOSL identification;
	private boolean blocked;
	private boolean abaBlocked;

	public String getEncryptedAccountNumber() {
		return encryptedAccountNumber;
	}

	public void setEncryptedAccountNumber(String encryptedAccountNumber) {
		this.encryptedAccountNumber = encryptedAccountNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getRoutingNumber() {
		return routingNumber;
	}

	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}

	public ConsumerIdentificationOSL getIdentification() {
		return identification;
	}

	public void setIdentification(ConsumerIdentificationOSL identification) {
		this.identification = identification;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public boolean isAbaBlocked() {
		return abaBlocked;
	}

	public void setAbaBlocked(boolean abaBlocked) {
		this.abaBlocked = abaBlocked;
	}

}
