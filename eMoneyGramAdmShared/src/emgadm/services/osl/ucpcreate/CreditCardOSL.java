package emgadm.services.osl.ucpcreate;

import java.io.Serializable;

public class CreditCardOSL implements Serializable {
	private String cardNumber;
	// private LocalDate expirationDate;
	private String expirationDate;
	private String ccv;
	// private CreditCardProvider creditCardProvider;
	private String creditCardProvider;
	private boolean binBlocked;
	private boolean blocked;
	// private CardType cardType;
	private String cardType;
	private String binNumber;

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getCcv() {
		return ccv;
	}

	public void setCcv(String ccv) {
		this.ccv = ccv;
	}

	public String getCreditCardProvider() {
		return creditCardProvider;
	}

	public void setCreditCardProvider(String creditCardProvider) {
		this.creditCardProvider = creditCardProvider;
	}

	public boolean isBinBlocked() {
		return binBlocked;
	}

	public void setBinBlocked(boolean binBlocked) {
		this.binBlocked = binBlocked;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getBinNumber() {
		return binNumber;
	}

	public void setBinNumber(String binNumber) {
		this.binNumber = binNumber;
	}
}