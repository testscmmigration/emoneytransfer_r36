package emgadm.services.osl.ucpcreate;

import java.io.Serializable;

public class ConsumerPersonalInfoOSL implements Serializable {
	private String firstName;
	private String lastName;
	private String middleName;
	private String secondLastName;
	// private LocalDate dateOfBirth;
	private String dateOfBirth;
	private String senderBirthCountry;
	private String ssn;
	private ConsumerIdentificationOSL simplePhotoId;
	private ConsumerIdentificationOSL fullPhotoId;
	private Boolean identificationRequired;
	// MBO-5720
	private String residencyStatus;

	public ConsumerPersonalInfoOSL() {
		// for ModelMapper
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getSecondLastName() {
		return secondLastName;
	}

	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getSenderBirthCountry() {
		return senderBirthCountry;
	}

	public void setSenderBirthCountry(String senderBirthCountry) {
		this.senderBirthCountry = senderBirthCountry;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public ConsumerIdentificationOSL getSimplePhotoId() {
		return simplePhotoId;
	}

	public void setSimplePhotoId(ConsumerIdentificationOSL simplePhotoId) {
		this.simplePhotoId = simplePhotoId;
	}

	public ConsumerIdentificationOSL getFullPhotoId() {
		return fullPhotoId;
	}

	public void setFullPhotoId(ConsumerIdentificationOSL fullPhotoId) {
		this.fullPhotoId = fullPhotoId;
	}

	public Boolean getIdentificationRequired() {
		return identificationRequired;
	}

	public void setIdentificationRequired(Boolean identificationRequired) {
		this.identificationRequired = identificationRequired;
	}

	public String getResidencyStatus() {
		return residencyStatus;
	}

	public void setResidencyStatus(String residencyStatus) {
		this.residencyStatus = residencyStatus;
	}
}
