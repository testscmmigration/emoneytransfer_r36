package emgadm.services.osl.ucpcreate;

import java.io.Serializable;
import java.util.Locale;

public class ContactInfoOSL implements Serializable {
	private String primaryPhone;
	private PhoneTypeOSL primaryPhoneType;
	

	private String email;
	private Boolean promoEmail;
	//private Locale communicationLanguage;
	private String communicationLanguage;
	private Boolean operationalSMSOptIn;
	private Boolean promoSMS;
	private String phoneCountryCode;
	private Integer phoneCountryDialingCode;

	public ContactInfoOSL() {
		// for ModelMapper
	}

	public String getPrimaryPhone() {
		return primaryPhone;
	}

	public PhoneTypeOSL getPrimaryPhoneType() {
		return primaryPhoneType;
	}

	public void setPrimaryPhoneType(PhoneTypeOSL primaryPhoneType) {
		this.primaryPhoneType = primaryPhoneType;
	}

	public String getEmail() {
		return email;
	}

	public Boolean getPromoEmail() {
		return promoEmail;
	}

	public void setPrimaryPhone(String primaryPhone) {
		this.primaryPhone = primaryPhone;
	}

	

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPromoEmail(Boolean promoEmail) {
		this.promoEmail = promoEmail;
	}

	public String getCommunicationLanguage() {
		return communicationLanguage;
	}

	public void setCommunicationLanguage(String communicationLanguage) {
		this.communicationLanguage = communicationLanguage;
	}

	public Boolean getOperationalSMSOptIn() {
		return operationalSMSOptIn;
	}

	public void setOperationalSMSOptIn(Boolean operationalSMSOptIn) {
		this.operationalSMSOptIn = operationalSMSOptIn;
	}

	public Boolean getPromoSMS() {
		return promoSMS;
	}

	public void setPromoSMS(Boolean promoSMS) {
		this.promoSMS = promoSMS;
	}

	public String getPhoneCountryCode() {
		return phoneCountryCode;
	}

	public void setPhoneCountryCode(String phoneCountryCode) {
		this.phoneCountryCode = phoneCountryCode;
	}

	public Integer getPhoneCountryDialingCode() {
		return phoneCountryDialingCode;
	}

	public void setPhoneCountryDialingCode(Integer phoneCountryDialingCode) {
		this.phoneCountryDialingCode = phoneCountryDialingCode;
	}
	 

}


