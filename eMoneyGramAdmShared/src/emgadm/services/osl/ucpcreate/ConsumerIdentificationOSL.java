package emgadm.services.osl.ucpcreate;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

public class ConsumerIdentificationOSL implements Serializable {
	private PhotoIdOSL senderPhotoIdType;
	//private String senderPhotoIdType;
	private String senderPhotoId;
	private String senderPhotoIdEncrypted;
	private String senderPhotoIdState;
	private String senderPhotoIdCountry;
	// private LocalDate senderPhotoIdExpiration;
	private String senderPhotoIdExpiration;
	private Boolean identificationRequired;
	private String senderPhotoId2;
	private String senderPhotoId3;

	public PhotoIdOSL getSenderPhotoIdType() {
		return senderPhotoIdType;
	}

	public void setSenderPhotoIdType(PhotoIdOSL senderPhotoIdType) {
		this.senderPhotoIdType = senderPhotoIdType;
	}

	public String getSenderPhotoId() {
		return senderPhotoId;
	}

	public void setSenderPhotoId(String senderPhotoId) {
		this.senderPhotoId = senderPhotoId;
	}

	public String getSenderPhotoIdEncrypted() {
		return senderPhotoIdEncrypted;
	}

	public void setSenderPhotoIdEncrypted(String senderPhotoIdEncrypted) {
		this.senderPhotoIdEncrypted = senderPhotoIdEncrypted;
	}

	public String getSenderPhotoIdState() {
		return senderPhotoIdState;
	}

	public void setSenderPhotoIdState(String senderPhotoIdState) {
		this.senderPhotoIdState = senderPhotoIdState;
	}

	public String getSenderPhotoIdCountry() {
		return senderPhotoIdCountry;
	}

	public void setSenderPhotoIdCountry(String senderPhotoIdCountry) {
		this.senderPhotoIdCountry = senderPhotoIdCountry;
	}

	public String getSenderPhotoIdExpiration() {
		return senderPhotoIdExpiration;
	}

	public void setSenderPhotoIdExpiration(String senderPhotoIdExpiration) {
		this.senderPhotoIdExpiration = senderPhotoIdExpiration;
	}

	public String getSenderPhotoId2() {
		return senderPhotoId2;
	}

	public void setSenderPhotoId2(String senderPhotoId2) {
		this.senderPhotoId2 = senderPhotoId2;
	}

	public String getSenderPhotoId3() {
		return senderPhotoId3;
	}

	public void setSenderPhotoId3(String senderPhotoId3) {
		this.senderPhotoId3 = senderPhotoId3;
	}

	public Boolean getIdentificationRequired() {
		return identificationRequired;
	}

	public void setIdentificationRequired(Boolean identificationRequired) {
		this.identificationRequired = identificationRequired;
	}

	@Override
	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
