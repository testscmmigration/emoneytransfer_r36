package emgadm.services.osl.ucpcreate;

import java.util.ArrayList;
import java.util.List;

public class ConsumerProfileOSL {
	private ConsumerPersonalInfoOSL personalInfo;
	private ContactInfoOSL contactInfo;
	private List<ConsumerAddressInfoOSL> addressInfos;
	private List<ConsumerAccountInfoOSL> accountInfos;
	private String ipAddress;
	private Long profileId;
	private Boolean guestProfile;
	private Boolean allowRegistration;
	private String passwordHash;
	private String globalUniqueId;
	private Long occupationCode;
	//private SecurityChallengeStatus securityChallengeStatus;
	private String securityChallengeStatus;
	private String version;
	private Boolean blocked;
	private String username;
	private Long globalConsumerProfileId;
//	private ProfileStatus status;
//	private ProfileSubStatus subStatus;
	private String status;
	private String subStatus;
	private Boolean plusOnly;
//	private SourceSite sourceSite;
	private String sourceSite;
	private Boolean rsaDeny;
	//private LocalDate lastAuthenticateDate;
	private String lastAuthenticateDate;
	private Boolean premier;
	private String plusNumber;
	private Boolean kbaStatusUpdated;	
	private String sourceApplication;

	public ConsumerProfileOSL() {

	}

	public ConsumerPersonalInfoOSL getPersonalInfo() {
		return personalInfo;
	}

	public void setPersonalInfo(ConsumerPersonalInfoOSL personalInfo) {
		this.personalInfo = personalInfo;
	}

	public ContactInfoOSL getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ContactInfoOSL contactInfo) {
		this.contactInfo = contactInfo;
	}

	public List<ConsumerAddressInfoOSL> getAddressInfos() {
		if (this.addressInfos == null) {
			this.addressInfos = new ArrayList<ConsumerAddressInfoOSL>();
		}
		return addressInfos;
	}

	public void setAddressInfos(List<ConsumerAddressInfoOSL> addressInfos) {
		this.addressInfos = addressInfos;
	}

	public List<ConsumerAccountInfoOSL> getAccountInfos() {
		return accountInfos;
	}

	public void setAccountInfos(List<ConsumerAccountInfoOSL> accountInfos) {
		this.accountInfos = accountInfos;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public Boolean getGuestProfile() {
		return guestProfile;
	}

	public void setGuestProfile(Boolean guestProfile) {
		this.guestProfile = guestProfile;
	}

	public Boolean getAllowRegistration() {
		return allowRegistration;
	}

	public void setAllowRegistration(Boolean allowRegistration) {
		this.allowRegistration = allowRegistration;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public String getGlobalUniqueId() {
		return globalUniqueId;
	}

	public void setGlobalUniqueId(String globalUniqueId) {
		this.globalUniqueId = globalUniqueId;
	}

	public Long getOccupationCode() {
		return occupationCode;
	}

	public void setOccupationCode(Long occupationCode) {
		this.occupationCode = occupationCode;
	}

	public String getSecurityChallengeStatus() {
		return securityChallengeStatus;
	}

	public void setSecurityChallengeStatus(
			String securityChallengeStatus) {
		this.securityChallengeStatus = securityChallengeStatus;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Boolean getBlocked() {
		return blocked;
	}

	public void setBlocked(Boolean blocked) {
		this.blocked = blocked;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getGlobalConsumerProfileId() {
		return globalConsumerProfileId;
	}

	public void setGlobalConsumerProfileId(Long globalConsumerProfileId) {
		this.globalConsumerProfileId = globalConsumerProfileId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public Boolean getPlusOnly() {
		return plusOnly;
	}

	public void setPlusOnly(Boolean plusOnly) {
		this.plusOnly = plusOnly;
	}

	public String getSourceSite() {
		return sourceSite;
	}

	public void setSourceSite(String sourceSite) {
		this.sourceSite = sourceSite;
	}

	public void setRsaDeny(Boolean rsaDeny) {
		this.rsaDeny = rsaDeny;
	}

	public Boolean getRsaDeny() {
		return rsaDeny;
	}

	public String getLastAuthenticateDate() {
		return lastAuthenticateDate;
	}

	public void setLastAuthenticateDate(String lastAuthenticateDate) {
		this.lastAuthenticateDate = lastAuthenticateDate;
	}

	public Boolean getPremier() {
		return premier;
	}

	public void setPremier(Boolean premier) {
		this.premier = premier;
	}

	public String getPlusNumber() {
		return plusNumber;
	}

	public void setPlusNumber(String plusNumber) {
		this.plusNumber = plusNumber;
	}

	public void setKbaStatusUpdated(Boolean kbaStatusUpdated) {
		this.kbaStatusUpdated = kbaStatusUpdated;
	}

	public Boolean getKbaStatusUpdated() {
		return kbaStatusUpdated;
	}

	public String getSourceApplication() {
		return sourceApplication;
	}

	public void setSourceApplication(String sourceApplication) {
		this.sourceApplication = sourceApplication;
	}
	
	
}
