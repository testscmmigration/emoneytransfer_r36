package emgadm.services.osl.ucpcreate;

import java.io.Serializable;

public class ConsumerAccountInfoOSL implements Serializable {
	private CreditCardOSL creditCard;
	private BankAccountOSL bankAccount;
	private Long accountId;
	private Boolean guest;
	private Boolean authorized;
	// private AccountStatusType accountStatus;
	private String accountStatus;

	private ConsumerAccountInfoOSL() {
	}

	public ConsumerAccountInfoOSL(CreditCardOSL creditCard, BankAccountOSL bankAccount,
			Long accountId) {
		this.creditCard = creditCard;
		this.bankAccount = bankAccount;
		this.accountId = accountId;
	}

	public CreditCardOSL getCreditCard() {
		return creditCard;
	}

	public BankAccountOSL getBankAccount() {
		return bankAccount;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Boolean isGuest() {
		return guest;
	}

	public void setGuest(Boolean guest) {
		this.guest = guest;
	}

	public Boolean getAuthorized() {
		return authorized;
	}

	public void setAuthorized(Boolean authorized) {
		this.authorized = authorized;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
}