package emgadm.services.osl.ucpcreate;

import emgadm.services.osl.*;
import emgshared.model.ConsumerProfile;

/**
 * This is the proxy interface to connect with OSL service. UCP-101
 * 
 */
public interface OSLProxy {

	/**
	 * This method converts the ConsumeProfileOSL into JSON request and posts it
	 * to OSL api 'ucpCreate', to Create or Update MDM profile.
	 * 
	 * @param consumer
	 * @return CreateOrUpdateUCPResponseOSL
	 * @throws ProxyException
	 */
	public CreateOrUpdateUCPResponseOSL createOrUpdateUCPProfile(
			ConsumerProfile consumer) throws OSLException;
}
