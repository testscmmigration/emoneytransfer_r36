package emgadm.services.osl.ucpcreate;

public class ConsumerProfileIDInfoOSL {

	private String consumerProfileID;
	private String consumerProfileIDType;
	// @JsonFormat(shape = JsonFormat.Shape.STRING, pattern =
	// "yyyy-MM-dd'T'HH:mm")
	// private LocalDateTime consumerProfileIDCreateDate;
	private String consumerProfileIDCreateDate;

	public ConsumerProfileIDInfoOSL() {

	}

	public ConsumerProfileIDInfoOSL(String consumerProfileID,
			String consumerProfileIDType, String consumerProfileIDCreateDate) {
		this.consumerProfileID = consumerProfileID;
		this.consumerProfileIDType = consumerProfileIDType;
		this.consumerProfileIDCreateDate = consumerProfileIDCreateDate;
	}

	public String getConsumerProfileID() {
		return consumerProfileID;
	}

	public void setConsumerProfileID(String consumerProfileID) {
		this.consumerProfileID = consumerProfileID;
	}

	public String getConsumerProfileIDType() {
		return consumerProfileIDType;
	}

	public void setConsumerProfileIDType(String consumerProfileIDType) {
		this.consumerProfileIDType = consumerProfileIDType;
	}

	public String getConsumerProfileIDCreateDate() {
		return consumerProfileIDCreateDate;
	}

	public void setConsumerProfileIDCreateDate(
			String consumerProfileIDCreateDate) {
		this.consumerProfileIDCreateDate = consumerProfileIDCreateDate;
	}

}
