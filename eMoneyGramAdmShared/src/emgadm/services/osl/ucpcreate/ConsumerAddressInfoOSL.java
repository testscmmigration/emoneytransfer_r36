package emgadm.services.osl.ucpcreate;

import java.io.Serializable;

public class ConsumerAddressInfoOSL implements Serializable {
	private Long addressId;
	private String line1;
	private String line2;
	private String line3;	
	private String city;
	private String state;
	private String postalCode;
	// private CountryCode country;
	private String country;
	private Boolean useAddressFromPostalLookup;
	private Boolean homeAddress;
	private String street;
	private String subStreet;
	private String company;
	private String department;
	private String subBuilding;
	private String subCity;
	private String poBox;
	private String region;
	private String premise;
	private Boolean activeFlow;
	private String formattedAddress;

	public ConsumerAddressInfoOSL() {
		// for ModelMapper
	}

	public Long getAddressId() {
		return addressId;
	}

	public String getLine1() {
		return line1;
	}

	public String getLine2() {
		return line2;
	}

	public String getLine3() {
		return line3;
	}

	

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public void setLine3(String line3) {
		this.line3 = line3;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Boolean getUseAddressFromPostalLookup() {
		return useAddressFromPostalLookup;
	}

	public void setUseAddressFromPostalLookup(Boolean useAddressFromPostalLookup) {
		this.useAddressFromPostalLookup = useAddressFromPostalLookup;
	}

	public Boolean getHomeAddress() {
		return homeAddress;
	}

	public void setHomeAddress(Boolean homeAddress) {
		this.homeAddress = homeAddress;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getSubStreet() {
		return subStreet;
	}

	public void setSubStreet(String subStreet) {
		this.subStreet = subStreet;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getSubBuilding() {
		return subBuilding;
	}

	public void setSubBuilding(String subBuilding) {
		this.subBuilding = subBuilding;
	}

	public String getSubCity() {
		return subCity;
	}

	public void setSubCity(String subCity) {
		this.subCity = subCity;
	}

	public String getPoBox() {
		return poBox;
	}

	public void setPoBox(String poBox) {
		this.poBox = poBox;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getPremise() {
		return premise;
	}

	public void setPremise(String premise) {
		this.premise = premise;
	}

	public Boolean getActiveFlow() {
		return activeFlow;
	}

	public void setActiveFlow(Boolean activeFlow) {
		this.activeFlow = activeFlow;
	}

	public String getFormattedAddress() {
		return formattedAddress;
	}

	public void setFormattedAddress(String formattedAddress) {
		this.formattedAddress = formattedAddress;
	}
}