package emgadm.services.osl.ucpcreate;

import java.util.List;

public class CreateOrUpdateUCPResponseOSL {

	private Integer status;
	private boolean doCheckIn;
	// @JsonFormat(shape = JsonFormat.Shape.STRING, pattern =
	// "yyyy-MM-dd'T'HH:mm")
	// private LocalDateTime timeStamp;
	private String timeStamp;
	private String flags;
	private String mgiSessionID;
	private String GAFVersionNumber;
	private String customerServiceMessage;
	private String consumerProfileID;
	private List<ConsumerProfileIDInfoOSL> consumerProfileIDs;
	private ServiceErrorOSL error;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public boolean isDoCheckIn() {
		return doCheckIn;
	}

	public void setDoCheckIn(boolean doCheckIn) {
		this.doCheckIn = doCheckIn;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getFlags() {
		return flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public String getMgiSessionID() {
		return mgiSessionID;
	}

	public void setMgiSessionID(String mgiSessionID) {
		this.mgiSessionID = mgiSessionID;
	}

	public String getGAFVersionNumber() {
		return GAFVersionNumber;
	}

	public void setGAFVersionNumber(String gAFVersionNumber) {
		GAFVersionNumber = gAFVersionNumber;
	}

	public String getCustomerServiceMessage() {
		return customerServiceMessage;
	}

	public void setCustomerServiceMessage(String customerServiceMessage) {
		this.customerServiceMessage = customerServiceMessage;
	}

	public String getConsumerProfileID() {
		return consumerProfileID;
	}

	public void setConsumerProfileID(String consumerProfileID) {
		this.consumerProfileID = consumerProfileID;
	}

	public List<ConsumerProfileIDInfoOSL> getConsumerProfileIDs() {
		return consumerProfileIDs;
	}

	public void setConsumerProfileIDs(
			List<ConsumerProfileIDInfoOSL> consumerProfileIDs) {
		this.consumerProfileIDs = consumerProfileIDs;
	}

	public ServiceErrorOSL responseEntity() {
		return error;
	}

	public void setError(ServiceErrorOSL error) {
		this.error = error;
	}

}
