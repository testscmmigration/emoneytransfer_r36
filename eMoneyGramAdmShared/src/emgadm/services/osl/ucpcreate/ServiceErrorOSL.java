package emgadm.services.osl.ucpcreate;

/**
 * This object is the part of Service Response VO, that takes the failure info.
 * MBO-5372
 *
 */
public class ServiceErrorOSL {
	private String errorCode;
	private String property;
	private String message;
	private String developerMessage;
	private String moreInfo;
	
	public ServiceErrorOSL(){
		
	}
	
	public ServiceErrorOSL(String errorCode, String property, String message, String developerMessage,
			String moreInfo) {
		this.errorCode = errorCode;
		this.property = property;
		this.message = message;
		this.developerMessage = developerMessage;
		this.moreInfo = moreInfo;
	}
	
	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}
	
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	/**
	 * @return the property
	 */
	public String getProperty() {
		return property;
	}
	
	/**
	 * @param property the property to set
	 */
	public void setProperty(String property) {
		this.property = property;
	}
	
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	/**
	 * @return the developerMessage
	 */
	public String getDeveloperMessage() {
		return developerMessage;
	}
	
	/**
	 * @param developerMessage the developerMessage to set
	 */
	public void setDeveloperMessage(String developerMessage) {
		this.developerMessage = developerMessage;
	}
	
	/**
	 * @return the moreInfo
	 */
	public String getMoreInfo() {
		return moreInfo;
	}
	
	/**
	 * @param moreInfo the moreInfo to set
	 */
	public void setMoreInfo(String moreInfo) {
		this.moreInfo = moreInfo;
	}
	
	@Override
	public String toString() {
		StringBuilder serviceError = new StringBuilder(1000);
		serviceError.append(" ErrorCode = ").append(this.getErrorCode())
				.append(", ");
		serviceError.append("Property = ").append(this.getProperty())
				.append(", ");
		serviceError.append("Message = ").append(this.getMessage())
				.append(", ");
		serviceError.append("Detail = ").append(this.getDeveloperMessage());

		return serviceError.toString();
	}
	

}
