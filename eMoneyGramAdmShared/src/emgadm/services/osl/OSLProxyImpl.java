package emgadm.services.osl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.owasp.esapi.ESAPI;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.moneygram.mgo.service.consumer_v4_0_7_2.Consumer;

import emgadm.model.Notification;
import emgadm.property.EMTAdmContainerProperties;
import emgadm.services.osl.actimize.ScoreAndDecisionRequest;
import emgadm.services.osl.actimize.ScoreAndDecisionResponse;
import emgadm.services.osl.ucpcreate.ConsumerAddressInfoOSL;
import emgadm.services.osl.ucpcreate.ConsumerIdentificationOSL;
import emgadm.services.osl.ucpcreate.ConsumerPersonalInfoOSL;
import emgadm.services.osl.ucpcreate.ConsumerProfileOSL;
import emgadm.services.osl.ucpcreate.ContactInfoOSL;
import emgadm.services.osl.ucpcreate.CreateOrUpdateUCPResponseOSL;
import emgadm.services.osl.ucpcreate.PhoneTypeOSL;
import emgadm.services.osl.ucpcreate.PhotoIdOSL;
import emgadm.services.oslupdate.OSLUpdateAddress;
import emgadm.services.oslupdate.UpdateAddressUtil;
import emgadm.services.oslupdate.UpdateOSLAddressResponse;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.model.ConsumerProfile;

public class OSLProxyImpl implements OSLProxy {

	private static final Logger LOGGER = EMGSharedLogger
			.getLogger("OSLProxyImpl");

	private static RestTemplate restTemplate = new RestTemplate();
	private static final String UCP_CREATE_SERVICE_METHOD = "consumers/ucpcreate";

	private static final String DATE_FORMAT_NOW = "yyyy-MM-dd";
	private static SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
	private static final OSLProxy instance = new OSLProxyImpl();
	private static final String CONTENT_TYPE = "Content-Type";
	private static final String APPLICATION_JSON_UTF8 = "application/json; charset=utf-8";
	private static final String UTF8 = "UTF-8";

	private static Map<String, PhotoIdOSL> photoIdMap = new HashMap<String, PhotoIdOSL>();
	static {
		photoIdMap.put("DRLC", PhotoIdOSL.DRIVERS_LICENSE);
		photoIdMap.put("SP", PhotoIdOSL.PASSPORT);
		photoIdMap.put("ECDL", PhotoIdOSL.DRIVERS_LICENSE);
		photoIdMap.put("EUID", PhotoIdOSL.GOVERNMENT_ID);
		photoIdMap.put("IPSP", PhotoIdOSL.PASSPORT);
		photoIdMap.put("ASYLUM_ID", PhotoIdOSL.ASYLUM_SEEKER_TEMP_ID);
		photoIdMap.put("PAS", PhotoIdOSL.PASSPORT);
		photoIdMap.put("SMART_CARD_ID", PhotoIdOSL.ID_SMART_CARD);
		photoIdMap.put("GREEN_BARCODE_ID", PhotoIdOSL.GREEN_BARCODED_ID);
		photoIdMap.put("REFUGEE_ID", PhotoIdOSL.REFUGEE_ID);
		photoIdMap.put("TEMP_RESIDENT_ID", PhotoIdOSL.TEMP_RESIDENT_PERMIT);
	};

	public static final OSLProxy getInstance() {
		return instance;
	}

	public static void sendNotification(Notification notificationBean)
			throws OSLException {

		DefaultHttpClient httpClient = null;

		try {
			httpClient = new DefaultHttpClient();
			String oslUrl = EMTAdmContainerProperties.getOslUrl();
			HttpPost postRequest = new HttpPost(oslUrl);
			JSONObject ob = new JSONObject(notificationBean);
			LOGGER.debug("OSL : JSON Objects :: " + ob.toString());
			StringEntity input = new StringEntity(ob.toString());
			input.setContentType("application/json");
			postRequest.setEntity(input);
			HttpResponse response = httpClient.execute(postRequest);
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));
			String output;
			LOGGER.debug("OSL : Output from Server :: \n"
					+ notificationBean.getPhoneNumber());
			while ((output = br.readLine()) != null) {
				LOGGER.debug("OSL : Output from Server :: \n"
						+ ESAPI.encoder().decodeForHTML(
								(ESAPI.encoder().encodeForHTML(output))));
			}

		} catch (MalformedURLException e) {
			throw new OSLException("Wrong URL to invoke OSL service", e);

		} catch (IOException e) {
			throw new OSLException(e);

		} finally {
			httpClient.getConnectionManager().shutdown();
		}
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.moneygram.mgo.service.consumer_v4_0_6.osl.proxy.OSLProxy#
	 * createOrUpdateUCPProfile
	 * (com.moneygram.mgo.service.consumer_v4_0_6.osl.proxy.ConsumerProfileOSL)
	 */
	public CreateOrUpdateUCPResponseOSL createOrUpdateUCPProfile(
			ConsumerProfile consumerProfile, String newPlusNumber) throws OSLException {
		consumerProfile.setLoyaltyPgmMembershipId(newPlusNumber);
		ConsumerProfileOSL consumerProfileOSL = transformToOSLConsumer(
				consumerProfile);
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		//UCP-824
	     headers.set(CONTENT_TYPE,APPLICATION_JSON_UTF8);

		try {
			String jsonToSend = (new ObjectMapper())
					.writeValueAsString(consumerProfileOSL);
			LOGGER.debug("OSL : JSON Objects :: " + jsonToSend);

			HttpEntity<String> requestEntity = new HttpEntity<String>(
					jsonToSend, headers);
			String oslUrl = EMTAdmContainerProperties.getOslUcpCreateEndPoint()
					+ UCP_CREATE_SERVICE_METHOD;
			ResponseEntity<String> responseEntity = restTemplate.exchange(
					oslUrl, HttpMethod.POST, requestEntity, String.class);
			return (new ObjectMapper()).readValue(
					responseEntity.getBody(),
					CreateOrUpdateUCPResponseOSL.class);			

		} catch (JsonProcessingException e) {
			LOGGER.error("Exception = ", e);
			throw new OSLException("Error occurred while processing json: ", e);

		} catch (MalformedURLException e) {
			LOGGER.error("Exception = ", e);
			throw new OSLException("Wrong URL to invoke OSL service", e);

		} catch (IOException e) {
			LOGGER.error("Exception = ", e);
			throw new OSLException(e);

		}
	}

	/*
	 * This method transforms the MGOServices Consumer object into OSL Consumer,
	 * to construct the json request to OSL.
	 */
	private ConsumerProfileOSL transformToOSLConsumer(
			ConsumerProfile consumerProfile) {
		LOGGER.debug("Inside transformToOSLConsumer() for ConsumerId = "
				+ consumerProfile.getId());

		ConsumerProfileOSL oslConsumer = new ConsumerProfileOSL();

		// Build Account Info details
		// oslConsumer.setAccountInfos(this.buildAccountInfoOSL(consumerProfile));

		// Build Address Info details
		oslConsumer.setAddressInfos(this.buildAddressInfoOSL(consumerProfile));
		oslConsumer.setPlusNumber(consumerProfile.getLoyaltyPgmMembershipId());
		// oslConsumer.setBlocked(consumerProfile.is);
		oslConsumer.setGlobalUniqueId(consumerProfile.getGuid());

		oslConsumer.setGuestProfile("Guest".equals(consumerProfile
				.getProfileType()) ? true : false);

		oslConsumer.setStatus(consumerProfile.getStatusCode());
		oslConsumer.setSubStatus(consumerProfile.getSubStatusCode());

		// Build Contact Info details
		oslConsumer.setContactInfo(this.buildContactInfoOSL(consumerProfile));

		oslConsumer.setIpAddress(consumerProfile.getCreateIpAddrId());
		
		oslConsumer.setPlusOnly(false);

		// Build Personal Info details
		oslConsumer.setPersonalInfo(this.buildPersonalInfoOSL(consumerProfile));

		oslConsumer.setProfileId(Long.valueOf(consumerProfile.getId()));

		oslConsumer.setSourceSite(consumerProfile.getPartnerSiteId());
		oslConsumer.setUsername(consumerProfile.getUserId());
		
		oslConsumer.setSourceApplication("EMT");
		return oslConsumer;
	}

	private PhoneTypeOSL getPhoneNumberTypeForOSLUpdate(
			String phoneNumberTypeInput) {
		if (ConsumerProfile.MOBILE_PHONE_TYPE
				.equalsIgnoreCase(phoneNumberTypeInput)) {
			return PhoneTypeOSL.MOBILE;
		} else if (ConsumerProfile.HOME_PHONE_TYPE
				.equalsIgnoreCase(phoneNumberTypeInput)) {
			return PhoneTypeOSL.HOME;
		} else {
			return PhoneTypeOSL.OTHER;
		}
	}

	/*
	 * This method builds Address Info
	 */
	private List<ConsumerAddressInfoOSL> buildAddressInfoOSL(
			ConsumerProfile consumerProfile) {
		LOGGER.debug("Inside buildAddressInfoOSL() for ConsumerId = "
				+ consumerProfile.getId());
		List<ConsumerAddressInfoOSL> addressInfos = new ArrayList<ConsumerAddressInfoOSL>();

		if (null != consumerProfile.getAddress()) {
			ConsumerAddressInfoOSL addressInfo = new ConsumerAddressInfoOSL();

			addressInfo.setAddressId(Long.valueOf(consumerProfile.getAddress()
					.getAddressId()));
			addressInfo
					.setLine1(consumerProfile.getAddress().getAddressLine1());
			addressInfo
					.setLine2(consumerProfile.getAddress().getAddressLine2());
			addressInfo
					.setLine3(consumerProfile.getAddress().getAddressLine3());
			addressInfo.setCity(consumerProfile.getAddress().getCity());
			addressInfo.setCountry(consumerProfile.getAddress()
					.getIsoCountryCode());
			addressInfo.setState(consumerProfile.getAddress().getState());
			addressInfo.setPostalCode(consumerProfile.getAddress()
					.getPostalCode());

			addressInfos.add(addressInfo);
		}

		return addressInfos;
	}

	/*
	 * This method builds Contact Info
	 */
	private ContactInfoOSL buildContactInfoOSL(ConsumerProfile consumerProfile) {
		LOGGER.debug("Inside buildContactInfoOSL() for ConsumerId = "
				+ consumerProfile.getId());
		ContactInfoOSL contactInfo = new ContactInfoOSL();

		contactInfo.setCommunicationLanguage(consumerProfile
				.getPreferedLanguage());
		contactInfo.setEmail(consumerProfile.getUserId());
		contactInfo.setOperationalSMSOptIn(Boolean.valueOf(consumerProfile
				.getNotificationSMSOptInFlag()));
		contactInfo.setPhoneCountryCode(consumerProfile.getPhoneCountryCode());
		LOGGER.debug("checking phone dialling code :"
				+ consumerProfile.getPhoneDialingCode());
		try {
			contactInfo.setPhoneCountryDialingCode(Integer
					.valueOf(consumerProfile.getPhoneDialingCode()));
		} catch (NumberFormatException diallingCodeFormatException) {
			// TODO Auto-generated catch block
			LOGGER.error("improper format of dialling code ",
					diallingCodeFormatException);
		}
		contactInfo.setPrimaryPhone(consumerProfile.getPhoneNumber());
		contactInfo
				.setPrimaryPhoneType(getPhoneNumberTypeForOSLUpdate(consumerProfile
						.getPhoneNumberType()));
		contactInfo.setPromoEmail(consumerProfile.isAcceptPromotionalEmail());
		// contactInfo.setPromoSMS();

		return contactInfo;
	}

	/*
	 * This method builds Personal Info.
	 */
	private ConsumerPersonalInfoOSL buildPersonalInfoOSL(
			ConsumerProfile consumerProfile) {
		LOGGER.debug("Inside buildPersonalInfoOSL() for ConsumerId = "
				+ consumerProfile.getId());
		ConsumerPersonalInfoOSL personalInfo = new ConsumerPersonalInfoOSL();

		String stringDOB = sdf.format(consumerProfile.getBirthdate());
		personalInfo.setDateOfBirth(stringDOB);

		personalInfo.setFirstName(consumerProfile.getFirstName());
		personalInfo.setLastName(consumerProfile.getLastName());
		personalInfo.setMiddleName(consumerProfile.getMiddleName());
		personalInfo.setResidencyStatus(consumerProfile.getResidentStatus());
		personalInfo.setSecondLastName(consumerProfile.getSecondLastName());
		personalInfo.setSenderBirthCountry(consumerProfile.getCountryOfBirth());
		personalInfo.setSsn(consumerProfile.getSsnLast4());

		ConsumerIdentificationOSL identification = new ConsumerIdentificationOSL();

		identification.setSenderPhotoId(consumerProfile.getIdExtnlId());
		identification.setSenderPhotoIdCountry(consumerProfile
				.getIdIssueCountry());
		if (null != consumerProfile.getIdExpirationDate()) {
			String stringIDExpDate = sdf.format(consumerProfile
					.getIdExpirationDate());
			identification.setSenderPhotoIdExpiration(stringIDExpDate);
		}

		identification.setSenderPhotoIdType(this.getPhotoIdOSL(consumerProfile
				.getIdType()));

		personalInfo.setFullPhotoId(identification);

		return personalInfo;
	}

	private PhotoIdOSL getPhotoIdOSL(String externalIDType) {
		return photoIdMap.get(externalIDType);
	}
	
	public UpdateOSLAddressResponse updateAddressthroOSL(Consumer consumer)
	throws OSLException {
OSLUpdateAddress oslAddress = UpdateAddressUtil
		.transformToOSLAddress(consumer);
DefaultHttpClient httpClient = null;
UpdateOSLAddressResponse addressResponse = new UpdateOSLAddressResponse();
try {
	httpClient = new DefaultHttpClient();
	String oslUrl = EMTAdmContainerProperties.getOslUcpCreateEndPoint();
	String oslAddressURL = oslUrl + "consumers/address";
	HttpPut putRequest = new HttpPut(oslAddressURL);
	JSONObject ob = new JSONObject(oslAddress);
	LOGGER.debug("OSL : JSON Objects :: " + ob.toString());
	//UCP-824
	StringEntity input = new StringEntity(ob.toString(),UTF8);
	input.setContentType(APPLICATION_JSON_UTF8);
	putRequest.setEntity(input);
	HttpResponse response = httpClient.execute(putRequest);

	Gson gson = new GsonBuilder().create();

	String responseJSONStr = EntityUtils.toString(response.getEntity());
	addressResponse = gson.fromJson(responseJSONStr,
			UpdateOSLAddressResponse.class);
} catch (MalformedURLException e) {
	throw new OSLException(
			"Wrong URL to invoke OSL service while trying to update Address",
			e);

} catch (IOException e) {
	LOGGER.error(
			"IOException while trying to update address through OSL", e);
	throw new OSLException(e);

} finally {
	httpClient.getConnectionManager().shutdown();
}

return addressResponse;
}
	/**
	 * Method to call OSL getScoreAndDeciosion
	 */
	public ScoreAndDecisionResponse getActimizeScoreAndDecision(ScoreAndDecisionRequest requestBean)
			throws OSLException {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		ScoreAndDecisionResponse response = new ScoreAndDecisionResponse();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		try {

			HttpPost postRequest = new HttpPost(
					EMTAdmContainerProperties.getOslUcpCreateEndPoint().concat("transactions/scoreAndDecision"));
			String request = gson.toJson(requestBean);
			LOGGER.debug("OSL : Actimize JSON Request :: "+request);
			StringEntity input = new StringEntity(request);
			input.setContentType("application/json");
			postRequest.setEntity(input);
			HttpResponse clientResponse = httpClient.execute(postRequest);
			String responseJSONStr = EntityUtils.toString(clientResponse.getEntity());
			response = gson.fromJson(responseJSONStr,
					ScoreAndDecisionResponse.class);
			LOGGER.debug("Actimize Response for tran id "+requestBean.getTransactionId()+" :: "+toPrettyFormat(responseJSONStr));
		} catch (MalformedURLException e) {
			LOGGER.error("MalformedURLException in getCountryCodeInfo for tranid "+ requestBean.getTransactionId(), e);
			throw new OSLException("Wrong URL to invoke OSL service", e);

		} catch (IOException e) {
			LOGGER.error("IOException in actimize score for tranid "+ requestBean.getTransactionId(), e);
			throw new OSLException(e);

		} catch (Exception e) {
			LOGGER.error("JSONException in Actimize for tranid "+ requestBean.getTransactionId(), e);
			// TODO Auto-generated catch block
			throw new OSLException(e);
		} finally {
			httpClient.getConnectionManager().shutdown();
		}
		return response;
	}
	/**
	 * Method to do pretty print for jsonstring
	 * @param jsonString
	 * @return
	 */
	public static String toPrettyFormat(String jsonString) {
		JsonParser parser = new JsonParser();
		JsonObject json = parser.parse(jsonString).getAsJsonObject();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String prettyJson = gson.toJson(json);
		return prettyJson;
	}

}
