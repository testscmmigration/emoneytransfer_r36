package emgadm.services.oslupdate;

public class UpdateOSLAddressResponse {

	private Long addressId;
	 private ServiceErrorVO errorVO;
	 private int status;
	public Long getAddressId() {
		return addressId;
	}
	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}
	public ServiceErrorVO getErrorVO() {
		return errorVO;
	}
	public void setErrorVO(ServiceErrorVO errorVO) {
		this.errorVO = errorVO;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	 
	 
}
