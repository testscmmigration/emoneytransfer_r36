package emgadm.services.oslupdate;

import org.apache.log4j.Logger;

import com.moneygram.mgo.service.consumer_v4_0_7_2.Consumer;
import emgshared.dataaccessors.EMGSharedLogger;

public class UpdateAddressUtil {
	private static final Logger LOGGER = EMGSharedLogger
			.getLogger("UpdateAddressUtil");
	private static final String SOURCE_APPLICATION = "EMT";
	
	private UpdateAddressUtil(){
		
	}

	public static OSLUpdateAddress transformToOSLAddress(Consumer consumer) {
		OSLUpdateAddress updateAddressObj = new OSLUpdateAddress();
		updateAddressObj.setProfileId(consumer.getConsumerId());
		updateAddressObj.setSourceSite(consumer.getSourceSite());
		updateAddressObj.setSourceApplication(SOURCE_APPLICATION);
		updateAddressObj.setAddressInfo(buildAddressInfoOSL(consumer));
		return updateAddressObj;

	}

	public static ConsumerAddressInfoOSL buildAddressInfoOSL(Consumer consumer) {
		LOGGER.debug("Inside buildAddressInfoOSL() for ConsumerId = "
				+ consumer.getConsumerId());
		ConsumerAddressInfoOSL addressInfo = new ConsumerAddressInfoOSL();
		if (null != consumer.getProfileAddress()) {

			addressInfo.setAddressId(consumer.getProfileAddress()
					.getAddressId());
			addressInfo.setAddressLine1(consumer.getProfileAddress().getLine1());
			addressInfo.setAddressLine2(consumer.getProfileAddress().getLine2());
			addressInfo.setAddressLine3(consumer.getProfileAddress().getLine3());			
			addressInfo.setCity(consumer.getProfileAddress().getCity());
			addressInfo.setCountry(consumer.getProfileAddress().getCountry());
			addressInfo.setState(consumer.getProfileAddress().getState());
			addressInfo
					.setPostalCode(consumer.getProfileAddress().getZipCode());
			addressInfo.setAddressId(consumer.getProfileAddress()
					.getAddressId());
			addressInfo.setCountry(consumer.getProfileAddress().getCountry());
			addressInfo.setActiveFlow(true);

		}

		return addressInfo;
	}
}
