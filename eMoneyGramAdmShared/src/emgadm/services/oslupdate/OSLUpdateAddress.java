package emgadm.services.oslupdate;

public class OSLUpdateAddress {

	private Long profileId;
	private String sourceSite;
	private String sourceApplication;
	private ConsumerAddressInfoOSL addressInfo;

	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public String getSourceSite() {
		return sourceSite;
	}

	public void setSourceSite(String sourceSite) {
		this.sourceSite = sourceSite;
	}

	public ConsumerAddressInfoOSL getAddressInfo() {
		return addressInfo;
	}

	public void setAddressInfo(ConsumerAddressInfoOSL addressInfo) {
		this.addressInfo = addressInfo;
	}

	public String getSourceApplication() {
		return sourceApplication;
	}

	public void setSourceApplication(String sourceApplication) {
		this.sourceApplication = sourceApplication;
	}
	
	

}
