/*
 * Created on Feb 4, 2005
 *
 */
package emgadm.services;

import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.ConsumerCreditCardAccount;
import emgshared.model.ConsumerProfile;
import emgshared.model.CreditCard;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.ConsumerAccountService;
import emgshared.services.ConsumerProfileService;
import emgshared.services.CreditCardService;
import emgshared.services.CreditCardServiceResponse;
import emgshared.services.PCIService;
import emgshared.services.ServiceFactory;

/**
 * @author A131
 * GRS due to needless pain in modifying these classes/interfaces to support
 * VerifiedByVisa, refactored away the excess interface and it's inherited
 * delegating implementations, several of which are unused.
 * This class is now a simple helper and shouldn't present any problem to
 * any future implementer of additional service vendors.
 * The only method implemented is the only non-pure-delegator that is actually
 * being used currently ( Release 5.2 build 10 )
 *
 */
public class SecuredCreditCardService
{
	private static EMTSharedDynProperties dynProps = new EMTSharedDynProperties();

	private static final SecuredCreditCardService instance = new SecuredCreditCardService();

	private static final CreditCardService creditCardService = ServiceFactory.getInstance().getCreditCardService();
	private static final PCIService pciService = ServiceFactory.getInstance().getPCIService();

	private SecuredCreditCardService() {}

	public static final SecuredCreditCardService getInstance()
	{
		return instance;
	}

	/**
	 * Reserve funds in a consumer's account.
	 *
	 * @param accountId
	 * @param amount
	 * @param emgTrackingValue
	 * @param callerUserId
	 * @param vbvTicket - optional info for VerifiedByVisa. Will process as before if this parameter is null.
	 * @return
	 * @see emgshared.services.CreditCardService#executeAuthorization(emgshared.model.ConsumerProfile, emgshared.model.CreditCard, emgshared.model.ConsumerAddress, double, boolean, java.lang.String)
	 */

	public CreditCardServiceResponse executeAuthorization(
		int accountId,
		double amount,
		String emgTrackingValue,
		String callerLoginId,
		String merchantId,
		String partnerSiteId)
	{
		ServiceFactory sharedServiceFactory = ServiceFactory.getInstance();
		ConsumerProfileService profileService =	sharedServiceFactory.getConsumerProfileService();
		ConsumerAccountService accountService =	sharedServiceFactory.getConsumerAccountService();

		try
		{
			ConsumerCreditCardAccount account =	accountService.getCreditCardAccount(accountId, callerLoginId);
			ConsumerProfile consumerProfile =
				profileService.getConsumerProfile(
					Integer.valueOf(account.getConsumerId()), callerLoginId, null);

			CreditCard creditCard = getCreditCard(account, callerLoginId);
			return creditCardService.executeAuthorization(
				consumerProfile,
				creditCard,
				account.getBillingAddress(),
				amount,
				dynProps.isCcAVSTrans(),
				emgTrackingValue,
				merchantId);
		} catch (Exception e) {
			throw new EMGRuntimeException(e);
		}
	}

	public CreditCard getCreditCard(ConsumerCreditCardAccount account, String callerLoginId)
		throws Exception
	{
		String decryptedAccountNumber =	pciService.retrieveCardNumber(account.getId(), true);

		return new CreditCard(
			decryptedAccountNumber,
			null,
			account.getExpireMonth(),
			account.getExpireYear());
	}

}
