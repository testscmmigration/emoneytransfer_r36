/*
 * Created on Jan 25, 2005
 *
 */
package emgadm.services;

import emgshared.property.EMTSharedContainerProperties;
import emgshared.util.MailHelper;
/**
 * @author T349
 *
 */
class MailServiceImpl implements MailService
{
	private static final MailService instance = new MailServiceImpl();

	private MailServiceImpl()
	{
	}

	public static final MailService getInstance()
	{
		return instance;
	}

	public boolean sendTransactionMail(
		String emailTo,
		String subject,
		String body)
	{
		return MailHelper.sendMail(
				EMTSharedContainerProperties.getMailHost(),
				EMTSharedContainerProperties.getMailFrom(),
			emailTo,
			subject,
			body);
	}
}