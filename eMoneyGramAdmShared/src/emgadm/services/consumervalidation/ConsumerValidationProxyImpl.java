package emgadm.services.consumervalidation;

import java.net.URL;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.moneygram.mgo.service.consumerValidationV2_1.Address;
import com.moneygram.mgo.service.consumerValidationV2_1.Consumer;
import com.moneygram.mgo.service.consumerValidationV2_1.GetKBAQuestionsRequest;
import com.moneygram.mgo.service.consumerValidationV2_1.GetKBAQuestionsResponse;
import com.moneygram.mgo.service.consumerValidationV2_1.MGOConsumerValidationPortTypeV2_1;
import com.moneygram.mgo.service.consumerValidationV2_1.MGOConsumerValidationSoapBindingV2_1Stub;
import com.moneygram.mgo.service.consumerValidationV2_1.MultipleChoiceAnswer;
import com.moneygram.mgo.service.consumerValidationV2_1.MultipleChoiceAnswers;
import com.moneygram.mgo.service.consumerValidationV2_1.PersonSearchRequest;
import com.moneygram.mgo.service.consumerValidationV2_1.PersonSearchResponse;
import com.moneygram.mgo.service.consumerValidationV2_1.Phone;
import com.moneygram.mgo.service.consumerValidationV2_1.PhoneType;
import com.moneygram.mgo.service.consumerValidationV2_1.RelativeSearchRequest;
import com.moneygram.mgo.service.consumerValidationV2_1.RelativeSearchResponse;
import com.moneygram.mgo.service.consumerValidationV2_1.SubmitKBAAnswersRequest;
import com.moneygram.mgo.service.consumerValidationV2_1.SubmitKBAAnswersResponse;

import emgadm.property.EMTAdmContainerProperties;
import emgadm.services.ProxyException;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.model.ConsumerAddress;
import emgshared.model.ConsumerProfile;

public class ConsumerValidationProxyImpl implements ConsumerValidationProxy {

	private static final Logger log = EMGSharedLogger.getLogger(
			ConsumerValidationProxyImpl.class);	
	private static final int DEFAULT_TIMEOUT = 15000;		
	private static final ConsumerValidationProxy instance = new ConsumerValidationProxyImpl();	
	
	private MGOConsumerValidationSoapBindingV2_1Stub consumerValidationClient = null;

	private ConsumerValidationProxyImpl() {
	}
	
	public static final ConsumerValidationProxy getInstance() {
		return instance;
	}

	@SuppressWarnings("unused")
	private MGOConsumerValidationPortTypeV2_1 getConsumerValidationClient()
			throws ProxyException {
		if (consumerValidationClient != null) {
			return consumerValidationClient;
		} else {
			synchronized (ConsumerValidationProxyImpl.class) {
				try {
					URL url = new URL(EMTAdmContainerProperties
							.getMGOConsumerValidationServiceUrl());
					String to = EMTAdmContainerProperties
							.getMGOConsumerValidationServiceTimeout();
					int timeout = DEFAULT_TIMEOUT;
					if (StringUtils.isNumeric(to)) {
						timeout = Integer.parseInt(to);
					} else {
						EMGSharedLogger.getLogger(
								this.getClass().getName().toString()).warn(
								"Invalid timeout value for MGO Consumer Validation Service: "
										+ to + ". Using default value of "
										+ timeout);
					}
					consumerValidationClient = new MGOConsumerValidationSoapBindingV2_1Stub(url, null);
					return consumerValidationClient;
				} catch (Exception e) {
					log.error("Failed to create ConsumerValidationClient.", e);
					throw new ProxyException(
							"Failed to create ConsumerValidationClient:"
									+ e.getMessage());
				}
			}
		}
	}

	/**
	 * This method gets the KBA Questions for the given consumer detail.
	 */
	public GetKBAQuestionsResponse getKBAQuestions(
			ConsumerProfile consumerProfile) throws Exception {

		GetKBAQuestionsResponse response = null;

		GetKBAQuestionsRequest request = new GetKBAQuestionsRequest();

		Consumer consumer = new Consumer();
		consumer.setCountryOfBirth(consumerProfile.getIsoCountryCode());
		consumer.setCustId(Long.valueOf(consumerProfile.getId()));
		consumer.setDateOfBirth(consumerProfile.getBirthdate());
		consumer.setFirstName(consumerProfile.getFirstName());
		consumer.setGender(consumerProfile.getGender());
		consumer.setIpAddress(consumerProfile.getCreateIpAddrId());
		consumer.setLastName(consumerProfile.getLastName());
		consumer.setMiddleName(consumerProfile.getMiddleName());
		consumer.setSsn(consumerProfile.getSsnLast4());
		consumer.setCountryOfBirth("USA");
		request.setConsumer(consumer);
		request.setSourceSite(consumerProfile.getPartnerSiteId());
	

		ConsumerAddress consumerAddress = consumerProfile.getAddress();
		if (null != consumerAddress) {
			Address address = new Address();
			address.setAddressLine1(consumerAddress.getAddressLine1());
			address.setAddressLine2(consumerAddress.getAddressLine2());			
			address.setCity(consumerAddress.getCity());
			address.setThreeCharISOCountry(consumerAddress.getIsoCountryCode());
			address.setPostalCode(consumerAddress.getPostalCode());

			request.setAddress(address);
		}

		Phone phone = new Phone();
		phone.setNumber(consumerProfile.getPhoneNumber());
        phone.setType(PhoneType.mobile);
		request.setPhone(phone);
		
		

		try {
			response = getConsumerValidationClient().getKBAQuestions(request);
			log.info("Done GetKBAQuestions for the consumer = "
					+ consumer.getCustId());
		} catch (Exception e) {
			log.error(
					"GetKBAQuestions failed for the consumer = "
							+ consumer.getCustId(), e);
			throw e;
		}

		return response;

	}

	/**
	 * This method submits the Answers for KBA Questions.
	 */
	public SubmitKBAAnswersResponse submitKBAAnswers(
			ConsumerProfile consumerProfile, String eventId,
			Long questionSetId, HashMap<String, Long> questionAnswerPairs)
			throws Exception {

		SubmitKBAAnswersResponse response = null;

		SubmitKBAAnswersRequest request = new SubmitKBAAnswersRequest();

		request.setVendorEventId(eventId);
		Consumer consumer = new Consumer();
		consumer.setCountryOfBirth(consumerProfile.getIsoCountryCode());
		consumer.setCustId(Long.valueOf(consumerProfile.getId()));
		consumer.setDateOfBirth(consumerProfile.getBirthdate());
		consumer.setFirstName(consumerProfile.getFirstName());
		consumer.setGender(consumerProfile.getGender());
		consumer.setIpAddress(consumerProfile.getCreateIpAddrId());
		consumer.setLastName(consumerProfile.getLastName());
		consumer.setMiddleName(consumerProfile.getMiddleName());
		consumer.setSsn(consumerProfile.getSsnLast4());
		consumer.setCountryOfBirth("USA");
		request.setConsumer(consumer);

		int size = questionAnswerPairs.size();
		MultipleChoiceAnswer[] multipleChoiceAnswers = new MultipleChoiceAnswer[size];
		MultipleChoiceAnswers answersList = new MultipleChoiceAnswers();

		int index = 0;
		for (String key : questionAnswerPairs.keySet()) {
			MultipleChoiceAnswer multipleChoiceAnswer = new MultipleChoiceAnswer();
			multipleChoiceAnswer.setQuestionId(key);
			multipleChoiceAnswer.setAnswerId(questionAnswerPairs.get(key));

			multipleChoiceAnswers[index] = multipleChoiceAnswer;
			index++;
		}
		answersList.setItem(multipleChoiceAnswers);
		answersList.setQuestionsSetId(questionSetId);

		request.setAnswers(answersList);
		
		try {
			response = getConsumerValidationClient().submitKBAAnswers(request);
			log.info("Done SubmitKBAAnswers for the consumer = "
					+ consumerProfile.getId());
		} catch (Exception e) {
			log.error(
					"SubmitKBAAnswers failed for the consumer = "
							+ consumerProfile.getId(), e);
			throw e;
		}

		return response;
	}

	/**
	 * This method does the Person Search with the given consumer detail.
	 */
	public PersonSearchResponse personSearch(ConsumerProfile consumerProfile,
			String ipaddress) throws Exception {
		// Response
		PersonSearchResponse response = null;

		// Build the PersonSearchRequest
		PersonSearchRequest request = new PersonSearchRequest();

		Consumer consumer = new Consumer();
		consumer.setCountryOfBirth(consumerProfile.getIsoCountryCode());
		consumer.setCustId(Long.valueOf(consumerProfile.getId()));
		consumer.setDateOfBirth(consumerProfile.getBirthdate());
		consumer.setFirstName(consumerProfile.getFirstName());
		consumer.setGender(consumerProfile.getGender());
		consumer.setIpAddress(ipaddress);
		consumer.setLastName(consumerProfile.getLastName());
		consumer.setMiddleName(consumerProfile.getMiddleName());
		consumer.setSsn(consumerProfile.getSsnLast4());

		request.setConsumer(consumer);		
		consumerProfile.getAddress().getAddressId();
		consumerProfile.getAddress().getAddressLine1();
		consumerProfile.getAddress().getAddressLine2();
		consumerProfile.getAddress().getAddressLine3();
		consumerProfile.getAddress().getCity();

		ConsumerAddress consumerAddress = consumerProfile.getAddress();
		if (null != consumerAddress) {
			Address address = new Address();
			address.setAddressLine1(consumerAddress.getAddressLine1());
			address.setAddressLine2(consumerAddress.getAddressLine2());			
			address.setCity(consumerAddress.getCity());
			address.setThreeCharISOCountry(consumerAddress.getIsoCountryCode());
			address.setPostalCode(consumerAddress.getPostalCode());

			request.setAddress(address);
		}

		Phone phone = new Phone();
		phone.setNumber(consumerProfile.getPhoneNumber());

		request.setPhone(phone);

		try {
			response = getConsumerValidationClient().personSearch(request);
			log.info("Done PersonSearch for the consumer = "
					+ consumer.getCustId());

		} catch (Exception e) {
			log.error(
					"PersonSearch failed for the consumer = "
							+ consumer.getCustId(), e);
			throw e;
		}

		// Return
		return response;
	}
	
	/**
	 * This method does the Relative Search with the given relative detail.
	 */
	public RelativeSearchResponse relativeSearch(
			ConsumerProfile consumerProfile,String uniqueId, String ipaddress) throws Exception {
		// Response
		RelativeSearchResponse response = null;

		// Build the RelativeSearchRequest
		RelativeSearchRequest request = new RelativeSearchRequest();

		Consumer consumer = new Consumer();
		consumer.setCountryOfBirth(consumerProfile.getIsoCountryCode());
		consumer.setCustId(Long.valueOf(consumerProfile.getId()));
		consumer.setDateOfBirth(consumerProfile.getBirthdate());
		consumer.setFirstName(consumerProfile.getFirstName());
		consumer.setGender(consumerProfile.getGender());
		consumer.setIpAddress(ipaddress);
		consumer.setLastName(consumerProfile.getLastName());
		consumer.setMiddleName(consumerProfile.getMiddleName());
		consumer.setSsn(consumerProfile.getSsnLast4());

		request.setConsumer(consumer);

		ConsumerAddress consumerAddress = consumerProfile.getAddress();
		if (null != consumerAddress) {
			Address address = new Address();
			address.setAddressLine1(consumerAddress.getAddressLine1());
			address.setAddressLine2(consumerAddress.getAddressLine2());			
			address.setCity(consumerAddress.getCity());
			address.setThreeCharISOCountry(consumerAddress.getIsoCountryCode());
			address.setPostalCode(consumerAddress.getPostalCode());

			request.setAddress(address);
		}

		Phone phone = new Phone();
		phone.setNumber(consumerProfile.getPhoneNumber());

		request.setPhone(phone);
        request.setUniqueId(uniqueId);
		try {
			response = getConsumerValidationClient().relativeSearch(request);
			log.info("Done RelativeSearch for the consumer = "
					+ consumer.getCustId());

		} catch (Exception e) {
			log.error(
					"RelativeSearch failed for the consumer = "
							+ consumer.getCustId(), e);
			throw e;
		}

		// Return
		return response;
	}

}
