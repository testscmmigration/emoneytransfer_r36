package emgadm.services.consumervalidation;

import java.util.HashMap;

import com.moneygram.mgo.service.consumerValidationV2_1.GetKBAQuestionsResponse;
import com.moneygram.mgo.service.consumerValidationV2_1.PersonSearchResponse;
import com.moneygram.mgo.service.consumerValidationV2_1.RelativeSearchResponse;
import com.moneygram.mgo.service.consumerValidationV2_1.SubmitKBAAnswersResponse;

import emgshared.model.ConsumerProfile;

public interface ConsumerValidationProxy {
	public GetKBAQuestionsResponse getKBAQuestions(
			ConsumerProfile consumerProfile) throws Exception;

	public SubmitKBAAnswersResponse submitKBAAnswers(
			ConsumerProfile consumerProfile, String eventId,
			Long questionSetId, HashMap<String, Long> questionAnswerPairs)
			throws Exception;

	public PersonSearchResponse personSearch(ConsumerProfile consumerProfile,
			String ipaddress) throws Exception;
	
	public RelativeSearchResponse relativeSearch(ConsumerProfile consumerProfile,
			String uniqueId,String ipaddress) throws Exception;
	
}
