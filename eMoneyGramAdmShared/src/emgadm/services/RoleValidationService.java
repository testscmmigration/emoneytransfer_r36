package emgadm.services;

public interface RoleValidationService {

	public boolean checkUserAuthorize(String userID)throws Exception;
}
