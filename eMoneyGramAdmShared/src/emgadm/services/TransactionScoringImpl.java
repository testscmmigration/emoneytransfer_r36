package emgadm.services;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import emgadm.dataaccessors.ManagerFactory;
import emgadm.dataaccessors.TransactionManager;
import emgadm.property.EMTAdmAppProperties;
import emgadm.sysmon.EventListConstant;
import emgadm.util.AddressHelper;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerStatus;
import emgshared.model.EMTTransactionType;
import emgshared.model.ScoreCategory;
import emgshared.model.ScoreConfiguration;
import emgshared.model.ScoreValue;
import emgshared.model.ScoringSingleton;
import emgshared.model.Transaction;
import emgshared.model.TransactionScore;
import emgshared.model.TransactionScoreCategory;
import emgshared.model.TransactionScoreResult;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ScoringService;
import emgshared.util.PhoneNumberHelper;
import eventmon.eventmonitor.EventMonitor;

public class TransactionScoringImpl implements TransactionScoring {

	private static final TransactionScoring INSTANCE = new TransactionScoringImpl();

	protected static final TransactionManager tranManager = ManagerFactory.createTransactionManager();
	protected static final ConsumerProfileService profileService = emgshared.services.ServiceFactory.getInstance()
	.getConsumerProfileService();

	public static TransactionScoring getInstance() {
		return INSTANCE;
	}

	public Map getTransactionScore(String userId, int tranId) throws DataSourceException, SQLException {

		Map categoryResults = getTransScoreValues(userId, tranId);

		Transaction tran = (Transaction) categoryResults.get(new String("Tran"));
		EMTTransactionType tranType = new EMTTransactionType();
		tranType.setPartnerSiteId(tran.getPartnerSiteId());
		tranType.setEmgTranTypeCode(tran.getEmgTranTypeCode());
		tranType.setEmgTranTypeDesc(tran.getEmgTranTypeDesc());
		tranType.setEmgTranTypeCodeLabel(tranType.getEmgTranTypeCode());
		tranType.setEmgTranTypeDescLabel(tranType.getEmgTranTypeDesc());

		ScoreConfiguration scoreConfiguration = ScoringSingleton.getInstance().getScoringConfiguration(tranType);

		return getTransactionScore(userId,tranId,scoreConfiguration,categoryResults);
	}

	public Map getTransactionScore(String userId, int tranId, ScoreConfiguration scoreConfiguration, Map categoryResults)
	throws DataSourceException, SQLException {

		int finalScore = 0;
		int rawScore = -999;
		int weightedScore = -999;
		double dTotalWeight = 0.0;
		String sysRecommendation = null;
		TransactionScoreResult transactionScoreResult = new TransactionScoreResult();

		HashMap<String,Object> tscs = new HashMap<String,Object>();
		emgshared.services.ServiceFactory sf = emgshared.services.ServiceFactory.getInstance();
		ScoringService ss = sf.getScoringService();

		for (Iterator it = scoreConfiguration.getScoreCategories().values().iterator(); it.hasNext();) {
			rawScore = -999;
			weightedScore = -999;
			ScoreCategory sc = (ScoreCategory) it.next();

			if (sc.isIncluded()) {
				Object checkObj =  categoryResults.get(sc.getScoreCategoryName());
				String checkValue = null;
				dTotalWeight = scoreConfiguration.getGetTotalWeight();

				if(checkObj instanceof ConsumerStatus) {
					checkValue = ((ConsumerStatus) checkObj).getSubStatusCode();
				}
				else {
					checkValue = String.valueOf(checkObj);
				}

				for (ScoreValue scoreValue : sc.getScoreValues()) {
					TransactionScoreCategory tsc = sc.computeTransactionScoreCategory(scoreValue, checkValue, dTotalWeight);

					if(tsc != null) {
						tsc.setEmgTranId(tranId);
						tsc.setScoreCnfgId(scoreConfiguration.getConfigurationId());
						rawScore = tsc.getRawScoreNbr();
						weightedScore = tsc.getWgtScoreNbr();

						tscs.put(sc.getScoreCategoryName(), tsc);
					}
				}

				if (weightedScore < 0) {

					if (rawScore < 0) {
						rawScore = 1000;
					}
					weightedScore = (int) Math.round((rawScore * sc.getWeight()) / scoreConfiguration.getGetTotalWeight());

					TransactionScoreCategory tsc = new TransactionScoreCategory();
					tsc.setEmgTranId(tranId);
					tsc.setScoreCatName(sc.getScoreCategoryName());
					tsc.setScoreCnfgId(scoreConfiguration.getConfigurationId());
					tsc.setScoreCatCode(sc.getScoreCategoryCode());
					tsc.setRawScoreNbr(rawScore);
					tsc.setWgtScoreNbr(weightedScore);
					tsc.setScoreQueryRsltData(checkValue);

					tscs.put(sc.getScoreCategoryName(), tsc);
				}

				finalScore = finalScore + weightedScore;
			}
		}

		transactionScoreResult.setScore(finalScore);

		// System recommended Action
		if (finalScore <= scoreConfiguration.getAcceptThreshold()) {
			transactionScoreResult.setResultCode("APRV");
			sysRecommendation = "APRV";
		}
		else if (finalScore > scoreConfiguration.getAcceptThreshold() && finalScore <= scoreConfiguration.getRejectThreshold()) {
			transactionScoreResult.setResultCode("REVW");
			sysRecommendation = "REVW";
		}
		else if (finalScore > scoreConfiguration.getRejectThreshold()) {
			transactionScoreResult.setResultCode("REJ");
			sysRecommendation = "REJ";
		}

		// save computed score to db
		if(tranId != 0) {
			TransactionScore ts = new TransactionScore();
			ts.setEmgTranId(tranId);
			ts.setScoreCnfgId(scoreConfiguration.getConfigurationId());
			ts.setTranScoreNbr(finalScore);
			ts.setSysAutoRsltCode(sysRecommendation);
			ss.setEmgTranScore(null, ts);

			Iterator it = tscs.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pairs = (Map.Entry)it.next();
				ss.setEmgTranScoreCategory(null, (TransactionScoreCategory) pairs.getValue());
				it.remove();
			}
		}

		tscs.put("Transaction Score Result", transactionScoreResult);
		return tscs;
	}

	private Map getTransScoreValues(String userId, int tranId) {
		long beginTime = System.currentTimeMillis();
		// get transaction bean
		Transaction tran = tranManager.getTransaction(tranId);
		if (tran == null) {
			throw new EMGRuntimeException("Transaction " + tranId + " not found");
		}
		// get consumer profile
		ConsumerProfile profile = null;
		try {
			profile = profileService.getConsumerProfile(Integer.valueOf(tran.getCustId()), tran.getSndCustLogonId(), "");
		} catch (DataSourceException e) {
		}
		if (profile == null) {
			throw new EMGRuntimeException("Consumer " + tran.getCustId() + " not found");
		}
		Map<String,Object> catMap = new HashMap<String,Object>();
		catMap.put("Tran", tran);
		// get call score categories
		emgshared.services.ServiceFactory sf = emgshared.services.ServiceFactory.getInstance();
		ScoringService ss = sf.getScoringService();
		EMTTransactionType tranType = new EMTTransactionType();
		tranType.setPartnerSiteId(tran.getPartnerSiteId());
		tranType.setEmgTranTypeCode(tran.getEmgTranTypeCode());
		tranType.setEmgTranTypeCodeLabel(tran.getEmgTranTypeCode());
		Collection categories = ScoringSingleton.getInstance().getScoringConfiguration(tranType).getScoreCategories().values();
		Iterator iter = categories.iterator();
		while (iter.hasNext()) {
			ScoreCategory sc = (ScoreCategory) iter.next();
			String name = sc.getScoreCategoryName();
			if (ScoreCategory.cat1.equalsIgnoreCase(name)) {
				catMap.put(ScoreCategory.cat1, new Boolean(ss.isPasswordChangedWithin24Hours(userId, tran.getCustId())));
			} else if (ScoreCategory.cat2.equalsIgnoreCase(name)) {
				catMap.put(ScoreCategory.cat2, profile.getStatus());
			} else if (ScoreCategory.cat3.equalsIgnoreCase(name)) {
				catMap.put(ScoreCategory.cat3, Integer.valueOf(ss.getBadTranCount(userId, tran.getCustId())));
			} else if (ScoreCategory.cat4.equalsIgnoreCase(name)) {
				catMap.put(ScoreCategory.cat4, Integer.valueOf(ss.getDaysLastTransaction(userId, tran.getCustId(), tranId)));
			} else if (ScoreCategory.cat5.equalsIgnoreCase(name)) {
				catMap.put(ScoreCategory.cat5, new Boolean(ss.isBlockedEmailMatch(userId, tranId)));
			} else if (ScoreCategory.cat6.equalsIgnoreCase(name)) {
				catMap.put(ScoreCategory.cat6, new Boolean(ss.isMaxSendLimitExceeded(userId, tranId)));
			} else if (ScoreCategory.cat7.equalsIgnoreCase(name)) {
				catMap.put(ScoreCategory.cat7, new Boolean(ss.isReceiveStateMatch(userId, tranId)));
			} else if (ScoreCategory.cat8.equalsIgnoreCase(name)) {
				catMap.put(ScoreCategory.cat8, new Boolean(ss.isSeniorCitizenMatch(userId, tranId)));
			} else if (ScoreCategory.cat9.equalsIgnoreCase(name)) {
				String state = "";
				String zipCode = "";
				try {
					state = profile.getState();
					zipCode = profile.getAddress().getPostalCode();
				} catch (Exception ex) {
					System.out.println(ex.toString());
				}
				catMap.put(ScoreCategory.cat9, new Boolean(ss.isZipStateMatch(userId, "USA", zipCode, state)));
			} else if (ScoreCategory.cat10.equalsIgnoreCase(name)) {
				String state = "";
				String areaCode = "";
				String areaCodeAlternate = "";
				try {
					state = profile.getState();
					areaCode = PhoneNumberHelper.areaCode(profile.getPhoneNumber());
					areaCodeAlternate = PhoneNumberHelper.areaCode(profile.getPhoneNumberAlternate());
				} catch (Exception ex) {
					System.out.println(ex.toString());
				}
				catMap.put(ScoreCategory.cat10, new Boolean(ss.isAreaStateMatch(userId, "USA", areaCode, state)
						|| ss.isAreaStateMatch(userId, "USA", areaCodeAlternate, state)));
			} else if (ScoreCategory.cat11.equalsIgnoreCase(name)) {
				String negativeAddress = null;
				try {
					negativeAddress = AddressHelper.negativeAddressFormat(profile.getAddressLine1(), profile.getPostalCode(), profile
							.getState());
				} catch (Exception ex) {
					System.out.println(ex.toString());
				}
				catMap.put(ScoreCategory.cat11, new Boolean(ss.isNegativeAddressMatch(userId, negativeAddress)));
			} else if (ScoreCategory.cat12.equalsIgnoreCase(name)) {
				Collection emails = profile.getEmails();
				catMap.put(ScoreCategory.cat12, new Boolean(ss.isEmailKeywordMatch(userId, emails)));
			} else if (ScoreCategory.cat13.equalsIgnoreCase(name)) {
				if (tran.getTranRiskScore()==null)
					catMap.put(ScoreCategory.cat13, Integer.valueOf(EMTAdmAppProperties.getTransMonitorDefaultScore()));
				else
					catMap.put(ScoreCategory.cat13, tran.getTranRiskScore());
			}
			else if (ScoreCategory.cat14.equalsIgnoreCase(name)) {
				catMap.put(ScoreCategory.cat14, tran.getSndFaceAmt().intValue());
			}
			else if (ScoreCategory.cat15.equalsIgnoreCase(name)) {
				catMap.put(ScoreCategory.cat15,  tran.getSndTotAmt().intValue());
			} else {
				throw new EMGRuntimeException("Unimplemented Scoring Category");
			}
		}
		EventMonitor.getInstance().recordEvent(EventListConstant.GET_TRAN_RAW_VALUES, beginTime, System.currentTimeMillis());
		return catMap;
	}

}
