/*
 * Created on Mar 24, 2004
 *
 */
package emgadm.exceptions;

/**
 * @author A131
 *
 */
public class InvalidRangeException extends MGIBaseException
{
	public InvalidRangeException()
	{
		super();
	}

	public InvalidRangeException(String s)
	{
		super(s);
	}
}
