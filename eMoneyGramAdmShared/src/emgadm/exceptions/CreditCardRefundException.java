package emgadm.exceptions;

import emgshared.exceptions.EMGBaseException;


public class CreditCardRefundException extends EMGBaseException
{
	private final String errorDescription;
	private final String errorCode;

	public CreditCardRefundException() {
		super();
		this.errorCode = null;
		this.errorDescription = null;
	}

	public CreditCardRefundException(String message) {
		super(message);
		this.errorCode = null;
		this.errorDescription = null;
	}

	public CreditCardRefundException(String message, String errorCode) {
		super(message);
		this.errorCode = errorCode;
		this.errorDescription = null;
	}

	public CreditCardRefundException(String message, String errorCode, String errorDescription) {
		super(message);
		this.errorCode = errorCode;
		this.errorDescription = errorDescription;
	}

	public String getDescription() {
		return errorDescription;
	}

	public String getCode() {
		return errorCode;
	}

}