/*
 * Created on Apr 7, 2004
 *
 */
package emgadm.exceptions;

/**
 * @author Greg
 *
 */
public class UserNotFoundException extends MGIBaseException {

	/**
	 * 
	 */
	public UserNotFoundException() {
		super();
	}

	/**
	 * @param s
	 */
	public UserNotFoundException(String s) {
		super(s);
	}

}
