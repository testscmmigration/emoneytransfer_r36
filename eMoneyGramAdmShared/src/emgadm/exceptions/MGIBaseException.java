/*
 * Created on Mar 18, 2004
 *
 */
package emgadm.exceptions;

/**
 * @author A131
 *
 */
public class MGIBaseException extends Exception
{

	/**
	 * 
	 */
	public MGIBaseException()
	{
		super();
	}

	/**
	 * @param s
	 */
	public MGIBaseException(String s)
	{
		super(s);
	}
	/**
	 * @param t root cause
	 */
	public MGIBaseException(Throwable t) {
		super(t);
	}

	/**
	 * @param s message
	 * @param t root cause
	 */
	public MGIBaseException(String s, Throwable t) {
		super(s, t);
	}
}
