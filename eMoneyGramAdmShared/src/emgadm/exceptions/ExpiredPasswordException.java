package emgadm.exceptions;

public class ExpiredPasswordException extends MGIBaseException {

	public ExpiredPasswordException() {
		super();
	}

	public ExpiredPasswordException(String s) {
		super(s);
	}

}
