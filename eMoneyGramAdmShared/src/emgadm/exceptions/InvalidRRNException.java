package emgadm.exceptions;

/**
 * @author David Hadley
 *
 */
public class InvalidRRNException extends MGIBaseException
{
	/**
	 * Constructor
	 */
	public InvalidRRNException()
	{
		super();
		this.rrn = null;
	}

	/**
	 * Constructor
	 * @param arg0
	 */
	public InvalidRRNException(String arg0)
	{
		super(arg0);
		this.rrn = null;
	}
	
	public InvalidRRNException(String arg0, String rrn)
	{
		super(arg0);
		this.rrn = rrn;
	}
	
	public String getRRN() {
		return this.rrn;
	}
	
	private String rrn;
}
