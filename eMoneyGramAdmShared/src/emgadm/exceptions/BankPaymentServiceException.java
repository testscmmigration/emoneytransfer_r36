package emgadm.exceptions;

public class BankPaymentServiceException extends MGIBaseException {

	public BankPaymentServiceException() {
		super();
	}

	public BankPaymentServiceException(String s) {
		super(s);
	}

}
