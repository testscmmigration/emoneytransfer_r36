/*
 * Created on Apr 7, 2004
 *
 */
package emgadm.exceptions;

/**
 * @author Greg
 *
 */
public class UserAuthenticationException extends MGIBaseException {

	/**
	 * 
	 */
	public UserAuthenticationException() {
		super();
	}

	/**
	 * @param s
	 */
	public UserAuthenticationException(String s) {
		super(s);
	}

}
