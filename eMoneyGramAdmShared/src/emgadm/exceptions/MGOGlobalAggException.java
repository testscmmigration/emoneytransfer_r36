package emgadm.exceptions;

import java.math.BigInteger;


public class MGOGlobalAggException extends Exception{

	private BigInteger errorCode;
	private BigInteger subErrorCode;
	String errorString;
	String detailString;
	
	public MGOGlobalAggException(String message, Throwable cause) {
        super(message, cause);
        if(cause != null){
            //validate error for AC1211
            if(cause instanceof com.moneygram.agentconnect1305.wsclient.Error) {
                com.moneygram.agentconnect1305.wsclient.Error error = (com.moneygram.agentconnect1305.wsclient.Error) cause;
                if (error.getErrorCode() != null) {
                    this.errorCode = error.getErrorCode();
                    this.subErrorCode = (error.getSubErrorCode() == null ? new BigInteger("0") : error.getSubErrorCode());
                    this.errorString = error.getErrorString();
                    this.detailString = error.getDetailString();
                }
            }
        }
    }
	
	
	public MGOGlobalAggException() {
		super();
	}

	public MGOGlobalAggException(String message) {
		super(message);
	}

	public MGOGlobalAggException(Throwable arg0) {
		super(arg0);
	}

	public BigInteger getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(BigInteger errorCode) {
		this.errorCode = errorCode;
	}

	public BigInteger getSubErrorCode() {
		return subErrorCode;
	}

	public void setSubErrorCode(BigInteger subErrorCode) {
		this.subErrorCode = subErrorCode;
	}

	public String getErrorString() {
		return errorString;
	}

	public void setErrorString(String errorString) {
		this.errorString = errorString;
	}

	public String getDetailString() {
		return detailString;
	}

	public void setDetailString(String detailString) {
		this.detailString = detailString;
	}
}
