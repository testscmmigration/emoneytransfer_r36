package emgadm.exceptions;

/**
 * @author David Hadley
 *
 */
public class InvalidUserException extends MGIBaseException
{
	/**
	 * Constructor
	 */
	public InvalidUserException()
	{
		super();
	}

	/**
	 * Constructor
	 * @param arg0
	 */
	public InvalidUserException(String arg0)
	{
		super(arg0);
	}
	/**
	 * Constructor
	 * @param t root cause
	 */
	public InvalidUserException(Throwable t) {
		super(t);
	}

	/**
	 * Constructor
	 * @param s message
	 * @param t root cause
	 */
	public InvalidUserException(String s, Throwable t) {
		super(s, t);
	}
}
