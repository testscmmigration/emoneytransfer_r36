package emgadm.exceptions;

public class TooManyResultException extends MGIBaseException{

	public TooManyResultException() {
		super();
	}

	public TooManyResultException(String s) {
		super(s);
	}
}
