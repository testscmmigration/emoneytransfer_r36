/*
 * Created on Jan 18, 2005
 *
 */
package emgadm.view;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import emgshared.model.AccountStatus;
import emgshared.model.ConsumerCreditCardAccount;
import emgshared.model.TaintIndicatorType;

/**
 * This view object is intended to be used by
 * the presentation layer.  It is similar to
 * a struts form object, and is necessary when
 * dealing with a collection of items.
 * 
 * @author A131
 *
 */
public class ConsumerCreditCardAccountView
{
	private String accountId;
	private String accountNumberMask;
	private String cardType;
	private String expirationMonth;
	private String expirationYear;
	private String issuer;
	private String statusCode;
	private String subStatusCode;
	private String accountTypeCode;
	private String accountTaintText;
	private String creditCardBinTaintText;
	private String creditOrDebitCode;
	private List cmntList;

	public ConsumerCreditCardAccountView()
	{
	}

	/**
	 * @param account
	 */
	public ConsumerCreditCardAccountView(ConsumerCreditCardAccount account)
	{
		this.accountId = String.valueOf(account.getId());
		this.accountNumberMask = account.getAccountNumberBin() + "******" + account.getAccountNumberMask();
		this.cardType = account.getTypeDescription();
		this.expirationMonth = String.valueOf(account.getExpireMonth());
		this.expirationYear = String.valueOf(account.getExpireYear());
		this.issuer = account.getTypeDescription();
		this.statusCode = account.getStatusCode();
		this.subStatusCode = account.getSubStatusCode();
		this.accountTypeCode = account.getType().getCode();
		
		if (account.isAccountNumberBlocked()) {
			this.accountTaintText = TaintIndicatorType.BLOCKED_TEXT;
		} else if (account.isAccountNumberOverridden()) {
			this.accountTaintText = TaintIndicatorType.OVERRIDE_TEXT;
		} else if (account.isAccountNumberNotBlocked()) {
			this.accountTaintText = TaintIndicatorType.NOT_BLOCKED_TEXT;
		} else {
			this.accountTaintText = "Unknown";
		}
			
		if (account.isCreditCardBinBlocked()) {
			this.creditCardBinTaintText = TaintIndicatorType.BLOCKED_TEXT;
		} else if (account.isCreditCardBinOverridden()) {
			this.creditCardBinTaintText = TaintIndicatorType.OVERRIDE_TEXT;
		} else if (account.isCreditCardBinNotBlocked()) {
			this.creditCardBinTaintText = TaintIndicatorType.NOT_BLOCKED_TEXT;
		} else {
			this.creditCardBinTaintText = "Unknown";
		}

		if (account.getCreditOrDebitCode().equalsIgnoreCase("U")){
			this.creditOrDebitCode = "Unknown";
		}else if (account.getCreditOrDebitCode().equalsIgnoreCase("C")){
			this.creditOrDebitCode = "Credit";
		}else if (account.getCreditOrDebitCode().equalsIgnoreCase("D")){
			this.creditOrDebitCode = "Debit";
		}else if (account.getCreditOrDebitCode().equalsIgnoreCase("P")){
			this.creditOrDebitCode = "Prepaid";
		}else if (account.getCreditOrDebitCode().equalsIgnoreCase("H")){
			this.creditOrDebitCode = "Charge";
		}else if (account.getCreditOrDebitCode().equalsIgnoreCase("R")){
			this.creditOrDebitCode = "Deferred Debit";
		}
					
		
		this.cmntList = account.getCmntList();
	}

	public String getAccountNumberMask()
	{
		return accountNumberMask;
	}

	public String getCardType()
	{
		return cardType;
	}

	public void setAccountNumberMask(String string)
	{
		accountNumberMask = string;
	}

	public void setCardType(String string)
	{
		cardType = string;
	}

	public String getAccountId()
	{
		return accountId;
	}

	public String getExpirationMonth()
	{
		return expirationMonth;
	}

	public String getExpirationYear()
	{
		return expirationYear;
	}

	public void setExpirationMonth(String string)
	{
		expirationMonth = string;
	}

	public void setExpirationYear(String string)
	{
		expirationYear = string;
	}

	public String getIssuer()
	{
		return issuer;
	}

	public void setIssuer(String string)
	{
		issuer = string;
	}

	public String getStatusCode()
	{
		return statusCode;
	}

	public String getSubStatusCode()
	{
		return subStatusCode;
	}

	public void setStatusCode(String string)
	{
		statusCode = string;
	}

	public void setSubStatusCode(String string)
	{
		subStatusCode = string;
	}

	public String getCombinedStatusCode()
	{
		return this.statusCode + AccountStatus.DELIMITER + this.subStatusCode;
	}

	public void setCombinedStatusCode(String combinedStatusCode)
	{
		String[] codes =
			StringUtils.split(combinedStatusCode, AccountStatus.DELIMITER);
		this.statusCode = (codes.length > 0 ? codes[0] : null);
		this.subStatusCode = (codes.length > 1 ? codes[1] : null);
	}

	public String getAccountTypeCode()
	{
		return accountTypeCode;
	}

	public void setAccountTypeCode(String string)
	{
		accountTypeCode = string;
	}
	/**
	 * @return
	 */
	public String getAccountTaintText() {
		return accountTaintText;
	}

	/**
	 * @param string
	 */
	public void setAccountTaintText(String string) {
		accountTaintText = string;
	}

	/**
	 * @return
	 */
	public String getCreditCardBinTaintText() {
		return creditCardBinTaintText;
	}

	/**
	 * @param string
	 */
	public void setCreditCardBinTaintText(String string) {
		creditCardBinTaintText = string;
	}

	/**
	 * @return
	 */
	public List getCmntList()
	{
		return cmntList;
	}

	/**
	 * @param list
	 */
	public void setCmntList(List list)
	{
		cmntList = list;
	}

	/**
	 * @return
	 */
	public String getCreditOrDebitCode()
	{
		return creditOrDebitCode;
	}

	/**
	 * @param string
	 */
	public void setCreditOrDebitCode(String string)
	{
		creditOrDebitCode = string;
	}

}
