/*
 * Created on Feb 7, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

import java.util.Date;
import java.util.TimeZone;

/**
 * @author A119 Holds info relating to MGO partner sites
 */
public class PartnerSite implements Comparable<PartnerSite> {

	private String partnetSideIdCode;
	private String partnerSiteId;
	private String partnerSiteDesc;
	private String partnerSiteNarrDesc;
	private String partnerSiteDipslayOrder;
	private String partnerSiteCurrency;
	private String country;
	private String countryAbbr;
	private TimeZone timeZone;
	private String sourceSiteName;
	private String defaultLanguage;
	private String iso3CharCountryCode;
	private Date dateFormat;
	private String timeZoneAbbr;
	private String partner;
	private String defaultPhoneDailingCode;
	private String iso2CharCountryCode;
	private String stagingFlag;	
	

	public String getPartnerSiteCurrency() {
		return partnerSiteCurrency;
	}

	public void setPartnerSiteCurrency(String partnerSiteCurrency) {
		this.partnerSiteCurrency = partnerSiteCurrency;
	}

	public String getPartnetSideIdCode() {
		return partnetSideIdCode;
	}

	public void setPartnetSideIdCode(String partnetSideIdCode) {
		this.partnetSideIdCode = partnetSideIdCode;
	}

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}

	public String getPartnerSiteDesc() {
		return partnerSiteDesc;
	}

	public void setPartnerSiteDesc(String partnerSiteDesc) {
		this.partnerSiteDesc = partnerSiteDesc;
	}

	public String getPartnerSiteNarrDesc() {
		return partnerSiteNarrDesc;
	}

	public void setPartnerSiteNarrDesc(String partnerSiteNarrDesc) {
		this.partnerSiteNarrDesc = partnerSiteNarrDesc;
	}

	public String getPartnerSiteDipslayOrder() {
		return partnerSiteDipslayOrder;
	}

	public void setPartnerSiteDipslayOrder(String partnerSiteDipslayOrder) {
		this.partnerSiteDipslayOrder = partnerSiteDipslayOrder;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountry() {
		return country;
	}

	public void setCountryAbbr(String countryAbbr) {
		this.countryAbbr = countryAbbr;
	}

	public String getCountryAbbr() {
		return countryAbbr;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	public TimeZone getTimeZone() {
		return timeZone;
	}
	public String getSourceSiteName() {
		return sourceSiteName;
	}
	public void setSourceSiteName(String sourceSiteName) {
		this.sourceSiteName = sourceSiteName;
	}
	public String getDefaultLanguage() {
		return defaultLanguage;
	}
	public void setDefaultLanguage(String defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}
	public String getIso3CharCountryCode() {
		return iso3CharCountryCode;
	}
	public void setIso3CharCountryCode(String iso3CharCountryCode) {
		this.iso3CharCountryCode = iso3CharCountryCode;
	}
	public Date getDateFormat() {
		return dateFormat;
	}
	public void setDateFormat(Date dateFormat) {
		this.dateFormat = dateFormat;
	}
	public String getTimeZoneAbbr() {
		return timeZoneAbbr;
	}
	public void setTimeZoneAbbr(String timeZoneAbbr) {
		this.timeZoneAbbr = timeZoneAbbr;
	}
	public String getPartner() {
		return partner;
	}
	public void setPartner(String partner) {
		this.partner = partner;
	}
	public String getDefaultPhoneDailingCode() {
		return defaultPhoneDailingCode;
	}
	public void setDefaultPhoneDailingCode(String defaultPhoneDailingCode) {
		this.defaultPhoneDailingCode = defaultPhoneDailingCode;
	}
	public String getIso2CharCountryCode() {
		return iso2CharCountryCode;
	}
	public void setIso2CharCountryCode(String iso2CharCountryCode) {
		this.iso2CharCountryCode = iso2CharCountryCode;
	}
	public String getStagingFlag() {
		return stagingFlag;
	}
	public void setStagingFlag(String stagingFlag) {
		this.stagingFlag = stagingFlag;
	}

	// mbo-4766 alphabetical order
	
	public int compareTo(PartnerSite partnerSite) {
		if (this.partnerSiteId != null && partnerSite.partnerSiteId != null) {
			return this.partnerSiteId
					.compareToIgnoreCase(partnerSite.partnerSiteId);
		}
		return 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((partnerSiteId == null) ? 0 : partnerSiteId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PartnerSite other = (PartnerSite) obj;
		if (partnerSiteId == null) {
			if (other.partnerSiteId != null) {
				return false;
			}
		} else if (!partnerSiteId.equals(other.partnerSiteId)) {
			return false;
		}
		return true;
	}

}
