package emgadm.model;

import java.util.ArrayList;
import java.util.List;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class Person {
	@XStreamImplicit(itemFieldName="phone")
	private ArrayList<BPSPhoneDetails> phoneDetails;
	private String verified;
	private String firstName;
	private String middleName;
	private String lastName;
	private String age;
	private String dob;
	private String deceased;
	private String prefix;
	private String uniqueId;
	private String dateFirstSeen;
	private String dateLastSeen;
	private String gender;
	private BPSAddress address;
	private SSNinfo ssnInfo;

	public ArrayList<BPSPhoneDetails> getPhoneDetails() {
		return phoneDetails;
	}

	public void setPhoneDetails(ArrayList<BPSPhoneDetails> phoneDetails) {
		this.phoneDetails = phoneDetails;
	}

	public String getVerified() {
		return verified;
	}

	public void setVerified(String verified) {
		this.verified = verified;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getDeceased() {
		return deceased;
	}

	public void setDeceased(String deceased) {
		this.deceased = deceased;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getDateFirstSeen() {
		return dateFirstSeen;
	}

	public void setDateFirstSeen(String dateFirstSeen) {
		this.dateFirstSeen = dateFirstSeen;
	}

	public String getDateLastSeen() {
		return dateLastSeen;
	}

	public void setDateLastSeen(String dateLastSeen) {
		this.dateLastSeen = dateLastSeen;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public BPSAddress getAddress() {
		return address;
	}

	public void setAddress(BPSAddress address) {
		this.address = address;
	}

	public SSNinfo getSsnInfo() {
		return ssnInfo;
	}

	public void setSsnInfo(SSNinfo ssnInfo) {
		this.ssnInfo = ssnInfo;
	}

}
