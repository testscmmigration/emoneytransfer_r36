package emgadm.model;

public class SSNinfo {

	private String ssnNumber;
 	private String issuedLocation;
 	public String getSsnNumber() {
		return ssnNumber;
	}
	public void setSsnNumber(String ssnNumber) {
		this.ssnNumber = ssnNumber;
	}
	public String getIssuedLocation() {
		return issuedLocation;
	}
	public void setIssuedLocation(String issuedLocation) {
		this.issuedLocation =  issuedLocation;
	}
	public String getIssuedStartDate() {
		return issuedStartDate;
	}
	public void setIssuedStartDate(String issuedStartDate) {
		this.issuedStartDate = issuedStartDate;
	}
	public String getIssuedEndDate() {
		return issuedEndDate;
	}
	public void setIssuedEndDate(String issuedEndDate) {
		this.issuedEndDate = issuedEndDate;
	}

	private String issuedStartDate;
 	private String issuedEndDate;
 	
	protected static String nullCheck(String ssnAttribute){
 		return ssnAttribute != null ? ssnAttribute : "";
 	}
	//3514 display purpose
	public SSNinfo(String ssnNumber, String issuedLocation,
			String issuedStartDate, String issuedEndDate) {
		super();
		this.ssnNumber = nullCheck(ssnNumber);;
		this.issuedLocation = nullCheck(issuedLocation);
		this.issuedStartDate = nullCheck(issuedStartDate);
		this.issuedEndDate = nullCheck(issuedEndDate);
	}
	//3514 display purpose
 	public SSNinfo(SSNinfo ssnInfo){
 		this.ssnNumber = nullCheck(ssnInfo.getSsnNumber());
 		//this.ssnIndicator = nullCheck(ssnInfo.getSsnIndicator());
 		this.issuedStartDate = nullCheck(ssnInfo.getIssuedStartDate());
 		this.issuedEndDate = nullCheck(ssnInfo.getIssuedEndDate());
 		this.issuedLocation = nullCheck(ssnInfo.getIssuedLocation());
 		//this.validSSN = nullCheck(ssnInfo.getValidSSN());
 		//this.ssnValidityIndicator= nullCheck(ssnInfo.getSsnValidityIndicator());
 	}
	
	
}
