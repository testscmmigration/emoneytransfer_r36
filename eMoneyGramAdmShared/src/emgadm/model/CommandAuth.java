package emgadm.model;

public class CommandAuth {
	private boolean editCntryExcp;
	private boolean editAgentLimit;
	private boolean addIPBlockedList;
	private boolean saveIPBlockedList;	
	private boolean addSSNBlockedList;
	private boolean saveSSNBlockedList;	
	private boolean addCCBlockedList;	
	private boolean saveCCBlockedList;	
	private boolean addBinBlockedList;	
	private boolean saveBinBlockedList;	
	private boolean addBankBlockedList;	
	private boolean saveBankBlockedList;	
	private boolean addEmailBlockedList;	
	private boolean saveEmailBlockedList;	
	private boolean editAdminUserProfile;	
	private boolean resetAdminUserPassword;	
	private boolean addNegativeTermList;
	private boolean saveNegativeTermList;
	
	/**
	 * @return Returns the editAdminUserProfile.
	 */
	public boolean isEditAdminUserProfile() {
		return editAdminUserProfile;
	}
	/**
	 * @param editAdminUserProfile The editAdminUserProfile to set.
	 */
	public void setEditAdminUserProfile(boolean b) {
		editAdminUserProfile = b;
	}
	/**
	 * @return Returns the resetAdminUserPassword.
	 */
	public boolean isResetAdminUserPassword() {
		return resetAdminUserPassword;
	}
	/**
	 * @param resetAdminUserPassword The resetAdminUserPassword to set.
	 */
	public void setResetAdminUserPassword(boolean b) {
		resetAdminUserPassword = b;
	}
	public boolean isEditCntryExcp() {
		return editCntryExcp;
	}

	public void setEditCntryExcp(boolean b) {
		editCntryExcp = b;
	}

	public boolean isEditAgentLimit() {
		return editAgentLimit;
	}

	public void setEditAgentLimit(boolean b) {
		editAgentLimit = b;
	}
	
	public boolean isAddIPBlockedList() {
		return addIPBlockedList;
	}
		
	public void setAddIPBlockedList(boolean b) {
		addIPBlockedList = b;
	}	

	public boolean isAddSSNBlockedList() {
		return addSSNBlockedList;
	}
		
	public void setAddSSNBlockedList(boolean b) {
		addSSNBlockedList = b;
	}	

	public boolean isAddCCBlockedList() {
		return addCCBlockedList;
	}
		
	public void setAddCCBlockedList(boolean b) {
		addCCBlockedList = b;
	}
	public boolean isSaveIPBlockedList() {
		return saveIPBlockedList;
	}
		
	public void setSaveIPBlockedList(boolean b) {
		saveIPBlockedList = b;
	}

	public boolean isSaveSSNBlockedList() {
		return saveSSNBlockedList;
	}
		
	public void setSaveSSNBlockedList(boolean b) {
		saveSSNBlockedList = b;
	}
	
	public void setSaveCCBlockedList(boolean b) {
		saveCCBlockedList = b;
	}
	public boolean isSaveCCBlockedList() {
		return saveCCBlockedList;
	}

	public void setAddBinBlockedList(boolean b) {
		addBinBlockedList = b;
	}
	public boolean isAddBinBlockedList() {
		return addBinBlockedList;
	}

	public void setSaveBinBlockedList(boolean b) {
		saveBinBlockedList = b;
	}
	public boolean isSaveBinBlockedList() {
		return saveBinBlockedList;
	}

	public void setAddBankBlockedList(boolean b) {
		addBankBlockedList = b;
	}
	public boolean isAddBankBlockedList() {
		return addBankBlockedList;
	}

	public void setSaveBankBlockedList(boolean b) {
		saveBankBlockedList = b;
	}
	public boolean isSaveBankBlockedList() {
		return saveBankBlockedList;
	}
	

	/**
	 * @return
	 */
	public boolean isAddEmailBlockedList() {
		return addEmailBlockedList;
	}

	/**
	 * @return
	 */
	public boolean isSaveEmailBlockedList() {
		return saveEmailBlockedList;
	}

	/**
	 * @param b
	 */
	public void setAddEmailBlockedList(boolean b) {
		addEmailBlockedList = b;
	}

	/**
	 * @param b
	 */
	public void setSaveEmailBlockedList(boolean b) {
		saveEmailBlockedList = b;
	}

	/**
	 * @return
	 */
	public boolean isAddNegativeTermList() {
		return addNegativeTermList;
	}

	/**
	 * @return
	 */
	public boolean isSaveNegativeTermList() {
		return saveNegativeTermList;
	}

	/**
	 * @param b
	 */
	public void setAddNegativeTermList(boolean b) {
		addNegativeTermList = b;
	}

	/**
	 * @param b
	 */
	public void setSaveNegativeTermList(boolean b) {
		saveNegativeTermList = b;
	}

}
