package emgadm.model;

import emgshared.model.Transaction;

public class BatchTranView implements Comparable
{
	private int id;
	private String owner;
	private String status;
	private String subStatus;
	private String sndName;
	private String rcvName;
	private String rcvCntryCd;
	private String faceAmt;
	private String totalAmt;
	private String createDate;
	private int transScore;
	private String rsltCode;
	private String msg;
	private Transaction tran;

	public BatchTranView(
		int id,
		String owner,
		String sndName,
		String rcvName,
		String rcvCntryCd,
		String faceAmt,
		String totalAmt,
		String createDate,
		int transScore,
		String rsltCode)
	{
		this.id = id;
		this.owner = owner;
		this.sndName = sndName;
		this.rcvName = rcvName;
		this.rcvCntryCd = rcvCntryCd;
		this.faceAmt = faceAmt;
		this.totalAmt = totalAmt;
		this.createDate = createDate;
		this.transScore = transScore;
		this.rsltCode = rsltCode;
	}

	public int getId()
	{
		return id;
	}

	public String getOwner()
	{
		return owner;
	}

	public void setOwner(String s)
	{
		owner = s;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String s)
	{
		status = s;
	}

	public String getSubStatus()
	{
		return subStatus;
	}

	public void setSubStatus(String s)
	{
		subStatus = s;
	}

	public String getSndName()
	{
		return sndName;
	}

	public String getRcvName()
	{
		return rcvName;
	}

	public String getRcvCntryCd()
	{
		return rcvCntryCd;
	}

	public String getFaceAmt()
	{
		return faceAmt;
	}

	public String getTotalAmt()
	{
		return totalAmt;
	}

	public String getCreateDate()
	{
		return createDate;
	}

	public int getTransScore()
	{
		return transScore;
	}

	public String getRsltCode()
	{
		return rsltCode;
	}

	public String getMsg()
	{
		return msg;
	}

	public void setMsg(String string)
	{
		msg = string;
	}

	public Transaction getTran()
	{
		return tran;
	}

	public void setTran(Transaction transaction)
	{
		tran = transaction;
	}

	public int compareTo(Object o)
	{
		BatchTranView view = (BatchTranView) o;
		return this.id - view.id;
	}
}