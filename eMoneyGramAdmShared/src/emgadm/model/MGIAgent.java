/*
 * Created on July 14, 2005
 * 
 * T004
 * 
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

public class MGIAgent {

	private int agentId;
	private String agentDesc;


	public MGIAgent(int agentId, String agentDesc) {
		this.agentId = agentId;
		this.agentDesc = agentDesc;
	}

	/**
	 * @return
	 */
	public String getAgentDesc() {
		return agentDesc;
	}

	/**
	 * @return
	 */
	public int getAgentId() {
		return agentId;
	}

	/**
	 * @param string
	 */
	public void setAgentDesc(String string) {
		agentDesc = string;
	}

	/**
	 * @param i
	 */
	public void setAgentId(int i) {
		agentId = i;
	}

}

