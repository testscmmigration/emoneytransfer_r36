/*
 * Created on Aug 3, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

import java.util.Date;

/**
 * @author T004
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ChargebackTransactionHeader implements Comparable {

	public static String sortBy = "tranId";
	public static int sortSeq = 1;
	
	private int tranId;
	private String chargebackStatusCode;
	private String chargebackStatusDesc;
	private Date chargebackStatusDate;
	private Date chargebackTrackingDate;
	private String genderCode;
	private String customerContactMethodCode;
	private String emailConsistentFlag;
	private String phoneNumberVerifiedFlag;
	private String customerVerbalContactFlag;

	private String unSolicitPhoneCallFlag;
	private String customerProfileValidationFlag;
	private int senderReceiverDistance;
	private int linkedCustomerProfiles;
	private Date createDate;
	private String createUserId;
	private Date updateDate;
	private String updateUserId;
	
	
	/**
	 * @return Returns the chargebackStatusCode.
	 */
	public String getChargebackStatusCode() {
		return chargebackStatusCode;
	}
	/**
	 * @param chargebackStatusCode The chargebackStatusCode to set.
	 */
	public void setChargebackStatusCode(String chargebackStatusCode) {
		this.chargebackStatusCode = chargebackStatusCode;
	}
	/**
	 * @return Returns the chargebackStatusDate.
	 */
	public Date getChargebackStatusDate() {
		return chargebackStatusDate;
	}
	/**
	 * @param chargebackStatusDate The chargebackStatusDate to set.
	 */
	public void setChargebackStatusDate(Date chargebackStatusDate) {
		this.chargebackStatusDate = chargebackStatusDate;
	}
	/**
	 * @return Returns the chargebackTrackingDate.
	 */
	public Date getChargebackTrackingDate() {
		return chargebackTrackingDate;
	}
	/**
	 * @param chargebackTrackingDate The chargebackTrackingDate to set.
	 */
	public void setChargebackTrackingDate(Date chargebackTrackingDate) {
		this.chargebackTrackingDate = chargebackTrackingDate;
	}
	/**
	 * @return Returns the createDate.
	 */
	public Date getCreateDate() {
		return createDate;
	}
	/**
	 * @param createDate The createDate to set.
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * @return Returns the createUserId.
	 */
	public String getCreateUserId() {
		return createUserId;
	}
	/**
	 * @param createUserId The createUserId to set.
	 */
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	/**
	 * @return Returns the customerContactMethodCode.
	 */
	public String getCustomerContactMethodCode() {
		return customerContactMethodCode;
	}
	/**
	 * @param customerContactMethodCode The customerContactMethodCode to set.
	 */
	public void setCustomerContactMethodCode(String customerContactMethodCode) {
		this.customerContactMethodCode = customerContactMethodCode;
	}
	/**
	 * @return Returns the customerProfileValidationFlag.
	 */
	public String getCustomerProfileValidationFlag() {
		return customerProfileValidationFlag;
	}
	/**
	 * @param customerProfileValidationFlag The customerProfileValidationFlag to set.
	 */
	public void setCustomerProfileValidationFlag(
			String customerProfileValidationFlag) {
		this.customerProfileValidationFlag = customerProfileValidationFlag;
	}
	/**
	 * @return Returns the customerVerbalContactFlag.
	 */
	public String getCustomerVerbalContactFlag() {
		return customerVerbalContactFlag;
	}
	/**
	 * @param customerVerbalContactFlag The customerVerbalContactFlag to set.
	 */
	public void setCustomerVerbalContactFlag(String customerVerbalContactFlag) {
		this.customerVerbalContactFlag = customerVerbalContactFlag;
	}
	/**
	 * @return Returns the emailConsistentFlag.
	 */
	public String getEmailConsistentFlag() {
		return emailConsistentFlag;
	}
	/**
	 * @param emailConsistentFlag The emailConsistentFlag to set.
	 */
	public void setEmailConsistentFlag(String emailConsistentFlag) {
		this.emailConsistentFlag = emailConsistentFlag;
	}
	/**
	 * @return Returns the genderCode.
	 */
	public String getGenderCode() {
		return genderCode;
	}
	/**
	 * @param genderCode The genderCode to set.
	 */
	public void setGenderCode(String genderCode) {
		this.genderCode = genderCode;
	}
	/**
	 * @return Returns the linkedCustomerProfiles.
	 */
	public int getLinkedCustomerProfiles() {
		return linkedCustomerProfiles;
	}
	/**
	 * @param linkedCustomerProfiles The linkedCustomerProfiles to set.
	 */
	public void setLinkedCustomerProfiles(int linkedCustomerProfiles) {
		this.linkedCustomerProfiles = linkedCustomerProfiles;
	}
	/**
	 * @return Returns the phoneNumberVerifiedFlag.
	 */
	public String getPhoneNumberVerifiedFlag() {
		return phoneNumberVerifiedFlag;
	}
	/**
	 * @param phoneNumberVerifiedFlag The phoneNumberVerifiedFlag to set.
	 */
	public void setPhoneNumberVerifiedFlag(String phoneNumberVerifiedFlag) {
		this.phoneNumberVerifiedFlag = phoneNumberVerifiedFlag;
	}
	/**
	 * @return Returns the senderReceiverDistance.
	 */
	public int getSenderReceiverDistance() {
		return senderReceiverDistance;
	}
	/**
	 * @param senderReceiverDistance The senderReceiverDistance to set.
	 */
	public void setSenderReceiverDistance(int senderReceiverDistance) {
		this.senderReceiverDistance = senderReceiverDistance;
	}
	/**
	 * @return Returns the tranId.
	 */
	public int getTranId() {
		return tranId;
	}
	/**
	 * @param tranId The tranId to set.
	 */
	public void setTranId(int tranId) {
		this.tranId = tranId;
	}
	/**
	 * @return Returns the unSolicitPhoneCallFlag.
	 */
	public String getUnSolicitPhoneCallFlag() {
		return unSolicitPhoneCallFlag;
	}
	/**
	 * @param unSolicitPhoneCallFlag The unSolicitPhoneCallFlag to set.
	 */
	public void setUnSolicitPhoneCallFlag(String unSolicitPhoneCallFlag) {
		this.unSolicitPhoneCallFlag = unSolicitPhoneCallFlag;
	}
	/**
	 * @return Returns the updateDate.
	 */
	public Date getUpdateDate() {
		return updateDate;
	}
	/**
	 * @param updateDate The updateDate to set.
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	/**
	 * @return Returns the updateUserId.
	 */
	public String getUpdateUserId() {
		return updateUserId;
	}
	/**
	 * @param updateUserId The updateUserId to set.
	 */
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	/**
	 * @return Returns the chargebackStatusDesc.
	 */
	public String getChargebackStatusDesc() {
		return chargebackStatusDesc;
	}
	/**
	 * @param chargebackStatusDesc The chargebackStatusDesc to set.
	 */
	public void setChargebackStatusDesc(String chargebackStatusDesc) {
		this.chargebackStatusDesc = chargebackStatusDesc;
	}
	
	public long getStatusAgeInDays()
	{
	    long returnValue = System.currentTimeMillis() - getChargebackStatusDate().getTime();
	    returnValue = returnValue / 1000 / 60 / 60 /24;
	    return returnValue;
	}
	
	public int compareTo(Object o)
	{
		ChargebackTransactionHeader header = (ChargebackTransactionHeader) o;

		if (ChargebackTransactionHeader.sortBy == null)
		{
			ChargebackTransactionHeader.sortBy = "transId";
		}

		if (ChargebackTransactionHeader.sortBy.equalsIgnoreCase("tranId"))
		{
			return (tranId - header.tranId) * sortSeq;
		}
		

		if (ChargebackTransactionHeader.sortBy.equalsIgnoreCase("chargebackStatusDesc"))
		{
			return (chargebackStatusDesc.compareToIgnoreCase(header.chargebackStatusDesc))
				* sortSeq;
		}
		
		if (ChargebackTransactionHeader.sortBy.equalsIgnoreCase("chargebackStatusDate"))
		{
			return (chargebackStatusDate.compareTo(header.chargebackStatusDate))
				* sortSeq;
		}		

		if (ChargebackTransactionHeader.sortBy.equalsIgnoreCase("chargebackTrackingDate"))
		{
			if (chargebackTrackingDate == null)
				return -1;
			
			if (header.chargebackTrackingDate == null)
				return 1;
			
			return (chargebackTrackingDate.compareTo(header.chargebackTrackingDate))
				* sortSeq;
		}		

		if (ChargebackTransactionHeader.sortBy.equalsIgnoreCase("createDate"))
		{
			return (createDate.compareTo(header.createDate))
				* sortSeq;
		}		

		
				
		if (ChargebackTransactionHeader.sortBy.equalsIgnoreCase("createUserId"))
		{
			return (createUserId.compareToIgnoreCase(header.createUserId))
				* sortSeq;
		}

		return 0;
	}	
}
