/*
 * Created on Feb 20, 2004
 *
 */
package emgadm.model;

import java.util.Date;
import java.util.TimeZone;

import emgshared.util.I18NHelper;

/**
 * @author A131
 *
 */
public final class BusinessAdminSettings
{
	private final String billerMainOfficeId;
	private final BusinessWeekType businessWeek;
	private final Date endOfDayTime;
	private final TimeZone timeZone;

	/**
	 *	Constructor 
	 */
	public BusinessAdminSettings(
		String billerMainOfficeId,
		BusinessWeekType businessWeek,
		Date endOfDayTime,
		TimeZone timeZone)
	{
		this.billerMainOfficeId = billerMainOfficeId;
		this.businessWeek = businessWeek;
		this.endOfDayTime =
			endOfDayTime == null ? null : (Date) endOfDayTime.clone();
		this.timeZone = timeZone == null ? null : (TimeZone) timeZone.clone();
		verifyState();
	}

	/**
	 * Creates a BusinessAdminSettings object for the given identifier
	 * by copying the settings from this object.
	 * 
	 * @param billerMainOfficeId	The desired biller main office identifier
	 * @return	BusinessAdminSettings
	 */
	public BusinessAdminSettings mimic(String billerMainOfficeId)
	{
		return new BusinessAdminSettings(
			billerMainOfficeId,
			this.getBusinessWeek(),
			this.endOfDayTime,
			this.timeZone);
	}

	/**
	 * @return
	 */
	public String getBillerMainOfficeId()
	{
		return billerMainOfficeId;
	}

	/**
	 * @return
	 */
	public BusinessWeekType getBusinessWeek()
	{
		return businessWeek;
	}

	/**
	 * @return
	 */
	public Date getEndOfDayTime()
	{
		return (Date) endOfDayTime.clone();
	}

	/**
	 * @return
	 */
	public TimeZone getTimeZone()
	{
		return (TimeZone) timeZone.clone();
	}

	private void verifyState()
	{
		if (this.billerMainOfficeId == null)
		{
			String errorMessage = I18NHelper.getFormattedMessage("exception.required.param", new Object[] { "billerMainOffice" }); //$NON-NLS-1$ //$NON-NLS-2$
			throw new NullPointerException(errorMessage);
		}

		if (this.businessWeek == null)
		{
			String errorMessage = I18NHelper.getFormattedMessage("exception.required.param", new Object[] { "businessWeek" }); //$NON-NLS-1$ //$NON-NLS-2$
			throw new NullPointerException(errorMessage);
		}

		if (this.endOfDayTime == null)
		{
			String errorMessage = I18NHelper.getFormattedMessage("exception.required.param", new Object[] { "endOfDayTime" }); //$NON-NLS-1$ //$NON-NLS-2$
			throw new NullPointerException(errorMessage);
		}

		if (this.timeZone == null)
		{
			String errorMessage = I18NHelper.getFormattedMessage("exception.required.param", new Object[] { "timeZone" }); //$NON-NLS-1$ //$NON-NLS-2$
			throw new NullPointerException(errorMessage);
		}
	}

}
