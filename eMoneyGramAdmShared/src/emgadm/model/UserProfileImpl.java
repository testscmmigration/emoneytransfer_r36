package emgadm.model;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import org.apache.struts.util.LabelValueBean;

import emgadm.dataaccessors.ManagerFactory;
import emgadm.util.DateHelper;
import emgadm.util.StringHelper;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;

public class UserProfileImpl implements UserProfile, Serializable {

	private String firstName;
	private String lastName;
	private boolean extendedReportAccess;
	private List<Role> roles;
	private String primaryAgent;
	private String uid;
	private Phone fax;
	private String title;
	private Phone phone;
	private String emailAddress;
	private Address address;
	private String password;
	private TimeZone timeZone;
	private String guid;
	private boolean internalUser;
	private boolean demoUser;
	private String lastAccessTimeStamp;
	private Calendar lastAccess;

	public UserProfileImpl() {
	}

	public UserProfileImpl(String userId) {
		uid = userId.toUpperCase();
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName.toUpperCase();
	}

	public void setLastName(String lastName) {
		this.lastName = lastName.toUpperCase();
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUID() {
		return uid == null ? "" : uid; //$NON-NLS-1$
	}

	public void setUID(String uid) {
		this.uid = uid;
	}

	/**
	 * A user is internal if the attribute "internal-user" is true
	 */
	public boolean isInternalUser() {
		return internalUser;
	}

	public boolean isExternalUser() {
		return !isInternalUser();
	}

	public String getFirstName() {
		return firstName == null ? "" : firstName; //$NON-NLS-1$
	}

	public String getLastName() {
		return lastName == null ? "" : lastName; //$NON-NLS-1$
	}

	public Phone getFaxNumber() {
		if (fax == null) {
			fax = new Telephone(""); //$NON-NLS-1$
		}
		return fax;
	}

	public String getTitle() {
		return title == null ? "" : title; //$NON-NLS-1$
	}

	public Phone getPhoneNumber() {
		if (phone == null) {
			phone = new Telephone(""); //$NON-NLS-1$
		}
		return phone;
	}

	public String getEmailAddress() {
		return emailAddress == null ? "" : emailAddress; //$NON-NLS-1$
	}

	public Address getAddress() {
		if (this.address == null) {
			Address address = new Address();
			address.setAddressLine1(""); //$NON-NLS-1$
			address.setAddressLine2(""); //$NON-NLS-1$
			address.setCity(""); //$NON-NLS-1$
			String state = ""; //$NON-NLS-1$

			/*
			 * HACK!!! todo
			 */
			Country country = new Country();
			country.setCountryName("United Sates of America"); //$NON-NLS-1$
			country.setISOCountryCode("US"); //$NON-NLS-1$
			State stateObj = new State();
			stateObj.setAbbreviatedName(state.toString());
			stateObj.setName(state.toString());
			stateObj.setCountry(country);
			address.setState(stateObj);

			address.setPostalCode(""); //$NON-NLS-1$
			this.address = address;
		}
		return this.address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setPhoneNumber(Phone phone) {
		this.phone = phone;
	}

	public void setFaxNumber(Phone fax) {
		this.fax = fax;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @param string
	 */
	public void setTimeZone(String timeZoneId) {
		setTimeZone(timeZoneId == null ? null : TimeZone
				.getTimeZone(timeZoneId));
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone == null ? null : (TimeZone) timeZone.clone();
	}

	/**
	 * @return
	 */

	/**
	 * @return
	 */
	public String getPrimaryAgentId() {
		return primaryAgent;
	}

	/**
	 * @return
	 */

	/**
	 * @param primaryAgent
	 */
	public void setPrimaryAgentId(String primaryAgent) {
		this.primaryAgent = primaryAgent;
	}

	/**
	 * @return
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * @return
	 */
	public boolean getInternalUser() {
		return internalUser;
	}

	/**
	 * @return
	 */
	public Calendar getLastAccess() {
		return lastAccess;
	}

	/**
	 * @return
	 */
	public String getLastAccessedTimeStamp() {
		return lastAccessTimeStamp;
	}

	/**
	 * @param guid
	 */
	public void setGuid(String guid) {
		this.guid = guid;
	}

	/**
	 * @param internalUser
	 */
	public void setInternalUser(boolean internalUser) {
		this.internalUser = internalUser;
	}

	/**
	 * @param string
	 */
	public void setLastAccessedTimeStamp(String string) throws ParseException {
		Calendar lastAccess = null;
		if (!StringHelper.isNullOrEmpty(string)) {
			lastAccess = new GregorianCalendar();
			lastAccess.setTime(DateHelper.getDateTimeFormatter().parse(string));
		}
		this.setLastAccess(lastAccess);
	}

	/**
	 * @param string
	 */
	public void setLastAccess(Calendar cal) {
		this.lastAccess = cal;
		this.lastAccessTimeStamp = (lastAccess == null ? null : DateHelper
				.getDateTimeFormatter().format(lastAccess.getTime()));
	}

	/**
	 * @return
	 */
	public List<Role> getRoles() {
		return roles;
	}

	/**
	 * @return
	 */
	public String getRoleDescriptions() {
		StringBuilder descriptions = new StringBuilder();
		boolean first = true;
		for (Role role : roles) {
			if (first) {
				first = false;
			} else {
				descriptions.append(", ");
			}
			descriptions.append(role.getDescription());
		}
		return descriptions.toString();
	}

	/**
	 * @param roles
	 */
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public boolean isActive() {
		return true; // MAS
	}

	/**
	 * @return
	 */
	public boolean isExtendedReportAccess() {
		return extendedReportAccess;
	}

	/**
	 * @param b
	 */
	public void setExtendedReportAccess(boolean extendedReportAccess) {
		this.extendedReportAccess = extendedReportAccess;
	}

	public boolean isPrimaryAgent(Agent agent) {
		boolean result = false;
		if (agent != null) {
			result = agent.getAgentId().equalsIgnoreCase(
					this.getPrimaryAgentId());
		}
		return (result);
	}

	public boolean isDemoUser() {
		return demoUser;
	}

	public void setDemoUser(boolean isDemoUser) {
		this.demoUser = isDemoUser;
	}

	/**
	 * @return
	 */
	public TimeZone getTimeZone() {
		return timeZone;
	}

	public int compareTo(UserProfile up) {
		return this.uid.compareTo(up.getUID());
	}

	public boolean hasPermission(String command) {
		for (Role role : roles) {
			if (role.hasPermission(command)) {
				return true;
			}
		}
		return false;
	}

	public List<LabelValueBean> getProgramsWithPermissions()
			throws DataSourceException {
		List<LabelValueBean> programNames = new ArrayList<LabelValueBean>();
		java.util.Collection<PartnerSite> partnerSiteIds;
		try {
			partnerSiteIds = ManagerFactory.createTransactionManager()
					.getPartnerSiteIds("B");
		} catch (DataSourceException e) {
			throw e;
		} catch (SQLException e) {
			throw new EMGRuntimeException(e);
		}

		/***********************************************************************
		 * vf69
		 * 
		 * Functionality not implemented in Sprint 28, left for future
		 * implementation
		 * 
		 * getProgramsWithPermissions method returns a List of LabelValueBeans
		 * which details the partner site id for which a certain user has
		 * permission to access. This partner sites are determined from the role
		 * prefix (ex: MGO:Manager, MGOUK:Manager ), "All" is appended to the
		 * list if user is able to view all current partner sites.
		 ************************************************************************/
		// MBO-6156
		programNames.add(new LabelValueBean("ALL", "ALL"));
		// fix so that this method can be called in sprint 28 - vf69

		for (PartnerSite programNametemp : partnerSiteIds) {
			String programName = programNametemp.getPartnerSiteId();

			programNames.add(new LabelValueBean(programName, programName));

		}
		return programNames;

	}

}
