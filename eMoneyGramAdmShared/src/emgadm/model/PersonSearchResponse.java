package emgadm.model;

public class PersonSearchResponse {
	public Persons getPersons() {
		return persons;
	}
	public void setPersons(Persons persons) {
		this.persons = persons;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = "null".equals(transactionId) ? "" : transactionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = "null".equals(status) ? "" : status;
	}
	private Persons persons;
	private String transactionId;
	private String status;
}
