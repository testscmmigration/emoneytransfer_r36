package emgadm.model;

import java.util.Date;

import emgshared.exceptions.EMGRuntimeException;
import emgshared.exceptions.ParseDateException;
import emgshared.services.CreditCardServiceResponseImpl;
import emgshared.util.DateFormatter;

public class TranComment implements Comparable
{
	private int tranId;
	private String tranCmntReasCode;
	private String cmntReasDesc;
	private String cmntText;
	private String cmntFormattedText;
	private String createUserId;
	private String createDate;
	private Date date;

	private DateFormatter df = new DateFormatter("dd/MMM/yyyy hh:mm a", true);

	public int getTranId()
	{
		return tranId;
	}

	public void setTranId(int i)
	{
		tranId = i;
	}

	public String getTranCmntReasCode()
	{
		return tranCmntReasCode == null ? "" : tranCmntReasCode;
	}

	public void setTranCmntReasCode(String string)
	{
		tranCmntReasCode = string;
	}

	public String getCmntReasDesc()
	{
		return cmntReasDesc == null ? "" : cmntReasDesc;
	}

	public void setCmntReasDesc(String string)
	{
		cmntReasDesc = string;
	}

	public String getCmntText()
	{
		return cmntText == null ? "" : cmntText;
	}

	public void setCmntText(String string)
	{
		cmntText = string;
		setCmntFormattedText(string);
		if(string.lastIndexOf(CreditCardServiceResponseImpl.ACCEPTED)!=0){
			setCmntFormattedText(cmntFormattedText.replaceAll(CreditCardServiceResponseImpl.ACCEPTED, "<b>"+CreditCardServiceResponseImpl.ACCEPTED+"</b>"));
		} else if(string.lastIndexOf(CreditCardServiceResponseImpl.REJECTED)!=0){
			setCmntFormattedText(cmntFormattedText.replaceAll(CreditCardServiceResponseImpl.REJECTED, "<b>"+CreditCardServiceResponseImpl.REJECTED+"</b>"));
		}
	}

	public String getCreateUserId()
	{
		return createUserId == null ? "" : createUserId;
	}

	public void setCreateUserId(String string)
	{
		createUserId = string;
	}

	public String getCreateDate()
	{
		return createDate == null ? "" : createDate;
	}

	public void setCreateDate(String string)
	{
		createDate = string;
	}

	public int compareTo(Object obj)
	{
		TranComment tc = (TranComment) obj;
		try
		{
			return df.parse(createDate).after(df.parse(tc.createDate)) ? -1 : 1;
		} catch (ParseDateException e)
		{
			throw new EMGRuntimeException(e);
		}
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getDate() {
		return date;
	}

	public String getCmntFormattedText() {
		return cmntFormattedText;
	}

	public void setCmntFormattedText(String cmntFormattedText) {
		this.cmntFormattedText = cmntFormattedText;
	}
}
