/*
 * Created on June 15, 2005
 *
 */
package emgadm.model;

import emgshared.util.I18NHelper;

/**
 * @author T004
 *
 */
public class AchReturnType
{

	public static final String ACH_RETURN_CODE = "ACH";
	public static final String MICRO_TRANSACTION_CODE = "MICR";

	public static final AchReturnType ACH_RETURN =
		new AchReturnType(ACH_RETURN_CODE, "ACH Returns");
	public static final AchReturnType MICRO_TRANSACTION =
		new AchReturnType(MICRO_TRANSACTION_CODE, "Micro Deposits");

	public static final AchReturnType[] ALL_TYPES = {
		ACH_RETURN, MICRO_TRANSACTION
	};
	
	private final String typeCode;
	private final String typeDescription;

	private AchReturnType(String type, String typeDescription)
	{
		this.typeCode = type;
		this.typeDescription = typeDescription;
	}

	public static AchReturnType getInstance(String typeCode)
	{
		AchReturnType type;
		if (ACH_RETURN_CODE.equalsIgnoreCase(typeCode))
		{
			type = ACH_RETURN;
		} else if (MICRO_TRANSACTION_CODE.equalsIgnoreCase(typeCode))
		{
			type = MICRO_TRANSACTION;
		} else
		{
			String errorMessage =
				I18NHelper.getFormattedMessage(
					"exception.invalid.param.value",
					new Object[] { typeCode, "type" });
			throw new IllegalArgumentException(errorMessage);
		}
		return (type);
	}

	public String getStatusCode()
	{
		return typeCode;
	}

	public boolean isACHTransaction()
	{
		return ACH_RETURN_CODE.equalsIgnoreCase(this.typeCode);
	}
	public boolean isMicroTransaction()
	{
		return MICRO_TRANSACTION_CODE.equalsIgnoreCase(this.typeCode);
	}

	public String toString()
	{
		return typeCode;
	}

	public String getStatusDescription() {
		return typeDescription;
	}

}