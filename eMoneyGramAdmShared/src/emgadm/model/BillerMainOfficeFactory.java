/*
 * Created on Mar 1, 2004
 *
 */
package emgadm.model;

import emgshared.exceptions.DataSourceException;

/**
 * @author A131
 *
 */
public class BillerMainOfficeFactory
{
	private BillerMainOfficeFactory()
	{
	}

	public static BillerMainOffice createBillerMainOffice(
		String guid,
		BusinessAdminSettings businessAdminSettings,
		SecurityStandards securityStandards)
		throws DataSourceException
	{
		return new BillerMainOfficeImpl(
			guid,
			businessAdminSettings,
			securityStandards);
	}

	public static BillerMainOffice createBillerMainOffice(
		String guid,
		BusinessAdminSettings businessAdminSettings,
		SecurityStandards securityStandards,
		BillerHierarchy billerHierarchy)
		throws DataSourceException
	{
		return new BillerMainOfficeImpl(
			guid,
			businessAdminSettings,
			securityStandards,
			billerHierarchy);
	}
}
