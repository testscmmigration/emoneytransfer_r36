/*
 * Created on Feb 19, 2004
 *
 */
package emgadm.model;

import emgshared.util.I18NHelper;

/**
 * @author A131
 *
 */
public final class SecurityStandards
{
	private final String billerMainOfficeId;
	private final int minimumPasswordLength;
	private final int minimumAlphaCharsInPassword;
	private final int minimumNumericCharsInPassword;
	private final int passwordResetDuration;
	private final int maximumLoginInactivity;

	public SecurityStandards(
		String billerMainOfficeId,
		int minimumPasswordLength,
		int minimumAlphaCharsInPassword,
		int minimumNumericCharsInPassword,
		int passwordResetDuration,
		int maximumLoginInactivity)
	{
		if (billerMainOfficeId == null)
		{
			String errorMessage = I18NHelper.getFormattedMessage("exception.required.param", new Object[] { "billerMainOfficeId" }); //$NON-NLS-1$ //$NON-NLS-2$
			throw new NullPointerException(errorMessage);
		}
		this.billerMainOfficeId = billerMainOfficeId;
		this.minimumPasswordLength = minimumPasswordLength;
		this.minimumAlphaCharsInPassword = minimumAlphaCharsInPassword;
		this.minimumNumericCharsInPassword = minimumNumericCharsInPassword;
		this.passwordResetDuration = passwordResetDuration;
		this.maximumLoginInactivity = maximumLoginInactivity;
	}

	/**
	 * Creates a SecurityStandards object for the given identifier
	 * by copying the settings from the given source.
	 * 
	 * @param billerMainOfficeId	The desired biller main office identifier
	 * @return	SecurityStandards
	 */
	public SecurityStandards mimic(String billerMainOfficeId)
	{
		return new SecurityStandards(
			billerMainOfficeId,
			this.minimumPasswordLength,
			this.minimumAlphaCharsInPassword,
			this.minimumNumericCharsInPassword,
			this.passwordResetDuration,
			this.maximumLoginInactivity);
	}

	/**
	 * @return
	 */
	public String getBillerMainOfficeId()
	{
		return billerMainOfficeId;
	}

	/**
	 * @return
	 */
	public int getMaximumLoginInactivity()
	{
		return maximumLoginInactivity;
	}

	/**
	 * @return
	 */
	public int getMinimumAlphaCharsInPassword()
	{
		return minimumAlphaCharsInPassword;
	}

	/**
	 * @return
	 */
	public int getMinimumNumericCharsInPassword()
	{
		return minimumNumericCharsInPassword;
	}

	/**
	 * @return
	 */
	public int getMinimumPasswordLength()
	{
		return minimumPasswordLength;
	}

	/**
	 * @return
	 */
	public int getPasswordResetDuration()
	{
		return passwordResetDuration;
	}

}
