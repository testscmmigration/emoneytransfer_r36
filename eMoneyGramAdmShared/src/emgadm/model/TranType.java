/*
 * Created on Feb 7, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

/**
 * @author T008
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TranType {
	private String emgTranTypeCode;
	private String emgTranTypeDesc;
	private String scoreFlag;
	private String autoApproveFlag;
	private String partnerSiteId;
	
	/**
	 * @return
	 */
	public String getEmgTranTypeCode() {
		return emgTranTypeCode == null ? "" : emgTranTypeCode;
	}

	/**
	 * @param string
	 */
	public void setEmgTranTypeCode(String string) {
		emgTranTypeCode = string;
	}

	/**
	 * @return
	 */
	public String getEmgTranTypeDesc() {
		return emgTranTypeDesc == null ? "" : emgTranTypeDesc;
	}

	/**
	 * @param string
	 */
	public void setEmgTranTypeDesc(String string) {
		emgTranTypeDesc = string;
	}

	/**
	 * @return
	 */
	public String getAutoApproveFlag()
	{
		return autoApproveFlag;
	}


	/**
	 * @param string
	 */
	public void setAutoApproveFlag(String string)
	{
		autoApproveFlag = string;
	}


	/**
	 * @return
	 */
	public String getScoreFlag()
	{
		return scoreFlag;
	}

	/**
	 * @param string
	 */
	public void setScoreFlag(String string)
	{
		scoreFlag = string;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	@Override
	public String toString() {
		return getEmgTranTypeCode();
	}

}
