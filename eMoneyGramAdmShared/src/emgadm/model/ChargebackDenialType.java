/*
 * Created on Aug 2, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

/**
 * @author T004
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ChargebackDenialType {
	private String chargebackDenialTypeCode;
	private String chargebackDenialTypeDesc;
	
	/**
	 * @return Returns the chargebackDenialTypeCode.
	 */
	public String getChargebackDenialTypeCode() {
		return chargebackDenialTypeCode;
	}
	/**
	 * @param chargebackDenialTypeCode The chargebackDenialTypeCode to set.
	 */
	public void setChargebackDenialTypeCode(String chargebackDenialTypeCode) {
		this.chargebackDenialTypeCode = chargebackDenialTypeCode;
	}
	/**
	 * @return Returns the chargebackDenialTypeDesc.
	 */
	public String getChargebackDenialTypeDesc() {
		return chargebackDenialTypeDesc;
	}
	/**
	 * @param chargebackDenialTypeDesc The chargebackDenialTypeDesc to set.
	 */
	public void setChargebackDenialTypeDesc(String chargebackDenialTypeDesc) {
		this.chargebackDenialTypeDesc = chargebackDenialTypeDesc;
	}
}
