package emgadm.model;

public class BasicPersonDetails {
	private String firstName;
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getDeceased() {
		return deceased;
	}

	public void setDeceased(String deceased) {
		this.deceased = deceased;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	private String gender;
	private String middleName;
	private String lastName;
	private String age;
	private String dob;
	private String deceased;
	private String prefix;
	
	// written for display purpose  in pop up
	public BasicPersonDetails(String firstName, String gender,
			String middleName, String lastName, String age, String dob,
			String deceased, String prefix) {
		super();
		this.firstName = checkNull(firstName);
		this.gender = checkNull(gender);
		this.middleName = checkNull(middleName);
		this.lastName = checkNull(lastName);
		this.age = checkNull(age);
		this.dob = checkNull(dob);
		this.deceased = checkNull(deceased);
		this.prefix = checkNull(prefix);
	}
	
	protected String checkNull(String personAttribute){
		return personAttribute != null ? personAttribute : "";
	}
	
}
