package emgadm.model;

import java.io.*;


/**
 * @version 	1.0
 * @author David Hadley
 */
public class Address implements Serializable{
	private String _addressLine1;
	private String _addressLine2;
	private String _addressLine3;
	private String _city;
	private State _state;
	private String _postalCode;
	private Country _country;
	private String _attentionTo;

	public String getAddressLine1() {
		return _addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		_addressLine1 = addressLine1.toUpperCase();
	}

	public String getAddressLine2() {
		return _addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		_addressLine2 = addressLine2.toUpperCase();
	}

	public String getAddressLine3() {
		return _addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		_addressLine3 = addressLine3.toUpperCase();
	}

	public String getCity() {
		return _city;
	}

	public void setCity(String city) {
		_city = city.toUpperCase();
	}

	public Country getCountry() {
		return _country;
	}

	public void setCountry(Country country) {
		_country = country;
	}

	public String getPostalCode() {
		return _postalCode;
	}

	public void setPostalCode(String postalCode) {
		_postalCode = postalCode;
	}

	public State getState() {
		return _state;
	}

	public void setState(State state) {
		_state = state;
	}

	/**
	 * Returns the attentionTo.
	 * @return String
	 */
	public String getAttentionTo() {
		return _attentionTo;
	}

	/**
	 * Sets the attentionTo.
	 * @param attentionTo The attentionTo to set
	 */
	public void setAttentionTo(String attentionTo) {
		_attentionTo = attentionTo.toUpperCase();
	}

}
