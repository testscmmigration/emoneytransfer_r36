package emgadm.model;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import emgshared.exceptions.DataSourceException;
import emgshared.util.I18NHelper;

class BillerMainOfficeImpl implements BillerMainOffice
{
	private String guid;
	private BusinessAdminSettings businessAdminSettings;
	private SecurityStandards securityStandards;
	private BillerHierarchy billerHierarchy;
	private Calendar inactivityThreshold = Calendar.getInstance();
	private Calendar passwordChangeThreshold = Calendar.getInstance();

	BillerMainOfficeImpl(
		String guid,
		BusinessAdminSettings businessAdminSettings,
		SecurityStandards securityStandards)
		throws DataSourceException
	{
		this(guid, businessAdminSettings, securityStandards, null);
	}

	BillerMainOfficeImpl(
		String guid,
		BusinessAdminSettings businessAdminSettings,
		SecurityStandards securityStandards,
		BillerHierarchy billerHierarchy)
		throws DataSourceException
	{
		this.guid = guid;
		this.businessAdminSettings = businessAdminSettings;
		this.securityStandards = securityStandards;
		this.billerHierarchy =
			(billerHierarchy == null
				? new BillerHierarchy(guid)
				: billerHierarchy);
		verifyState();
		this.inactivityThreshold.add(
			Calendar.DAY_OF_MONTH,
			-1 * this.getMaximumLoginInactivity());
		this.passwordChangeThreshold.add(
			Calendar.DAY_OF_MONTH,
			-1 * this.getPasswordResetDuration());
	}

	private void verifyState()
	{
		if (this.guid == null)
		{
			String errorMessage = I18NHelper.getFormattedMessage("exception.required.param", new Object[] { "guid" }); //$NON-NLS-1$ //$NON-NLS-2$
			throw new NullPointerException(errorMessage);
		}

		if (this.businessAdminSettings == null)
		{
			String errorMessage = I18NHelper.getFormattedMessage("exception.required.param", new Object[] { "businessAdminSettings" }); //$NON-NLS-1$ //$NON-NLS-2$
			throw new NullPointerException(errorMessage);
		}

		if (this.securityStandards == null)
		{
			String errorMessage = I18NHelper.getFormattedMessage("exception.required.param", new Object[] { "securityStandards" }); //$NON-NLS-1$ //$NON-NLS-2$
			throw new NullPointerException(errorMessage);
		}

		if (false
			== guid.equalsIgnoreCase(
				businessAdminSettings.getBillerMainOfficeId()))
		{
			throw new IllegalStateException(I18NHelper.getFormattedMessage("exception.inconsistent.data", new Object[] { "Biller main office", "business admin settings" })); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}

		if (false
			== guid.equalsIgnoreCase(securityStandards.getBillerMainOfficeId()))
		{
			throw new IllegalStateException(I18NHelper.getFormattedMessage("exception.inconsistent.data", new Object[] { "Biller main office", "security standards" })); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}

		if (this.billerHierarchy.getMainOfficeId() != null)
		{
			if (false
				== guid.equalsIgnoreCase(
					businessAdminSettings.getBillerMainOfficeId()))
			{
				throw new IllegalStateException(I18NHelper.getFormattedMessage("exception.inconsistent.data", new Object[] { "Biller main office", "biller hierarchy" })); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
		}
	}

	/**
	 * @return
	 */
	public String getGuid()
	{
		return guid;
	}

	/**
	 * Creates a BillerMainOffice object for the given identifier
	 * by copying the security standards and business admin
	 * settings from this object, and by creating a new
	 * biller hierarchy based on the desired guid.
	 * 
	 * @param newGuid	The desired guid
	 * @return	BillerMainOffice
	 */
	public BillerMainOffice mimic(String newGuid) throws DataSourceException
	{
		return (
			BillerMainOfficeFactory.createBillerMainOffice(
				newGuid,
				this.businessAdminSettings.mimic(newGuid),
				this.securityStandards.mimic(newGuid)));
	}
	/**
	 * @return
	 */
	public BusinessWeekType getBusinessWeek()
	{
		return businessAdminSettings.getBusinessWeek();
	}

	/**
	 * @return
	 */
	public Date getEndOfDayTime()
	{
		return businessAdminSettings.getEndOfDayTime();
	}

	/**
	 * @return
	 */
	public TimeZone getTimeZone()
	{
		return businessAdminSettings.getTimeZone();
	}

	/**
	 * @return
	 */
	public int getMaximumLoginInactivity()
	{
		return securityStandards.getMaximumLoginInactivity();
	}

	/**
	 * @return
	 */
	public int getMinimumAlphaCharsInPassword()
	{
		return securityStandards.getMinimumAlphaCharsInPassword();
	}

	/**
	 * @return
	 */
	public int getMinimumNumericCharsInPassword()
	{
		return securityStandards.getMinimumNumericCharsInPassword();
	}

	/**
	 * @return
	 */
	public int getMinimumPasswordLength()
	{
		return securityStandards.getMinimumPasswordLength();
	}

	/**
	 * @return
	 */
	public int getPasswordResetDuration()
	{
		return securityStandards.getPasswordResetDuration();
	}

	/**
	 * @return
	 */
	public BillerHierarchy getBillerHierarchy()
	{
		return billerHierarchy;
	}

	/**
	 * @return
	 */
	public String getName()
	{
		return billerHierarchy.getMainOfficeName();
	}
	/**
	 * @return
	 */
	public BusinessAdminSettings getBusinessAdminSettings()
	{
		return businessAdminSettings;
	}

	/**
	 * @return
	 */
	public SecurityStandards getSecurityStandards()
	{
		return securityStandards;
	}

	public boolean isPasswordValid(String password)
	{
		password = password.trim();
		int alpha = 0;
		int num = 0;

		if (password.length() < securityStandards.getMinimumPasswordLength())
		{
			return false;
		}
		for (int i = 0; i < password.length(); i++)
		{
			if (Character.isDigit(password.charAt(i)))
			{
				num++;
			} else if (Character.isLetter(password.charAt(i)))
			{
				alpha++;
			}
		}
		if (alpha >= securityStandards.getMinimumAlphaCharsInPassword()
			&& num >= securityStandards.getMinimumNumericCharsInPassword())
		{
			return true;
		}
		return false;
	}

	public boolean isUserActive(Calendar lastActivityDate)
	{
		if (lastActivityDate == null)
		{
			return true; //New User
		}
		return this.inactivityThreshold.before(lastActivityDate);
	}

	public boolean isPasswordExpired(Calendar lastChangeDate)
	{
		return this.passwordChangeThreshold.after(lastChangeDate);
	}

	public long getStartOfBusinessDayInMillis(long referencePointInMillis)
	{
		return getBusinessDayInMillis(referencePointInMillis, true) + 1000;
	}

	public long getEndOfBusinessDayInMillis(long referencePointInMillis)
	{
		return getBusinessDayInMillis(referencePointInMillis, false);
	}

	private long getBusinessDayInMillis(
		long referencePointInMillis,
		boolean findStart)
	{
		Date referencePointDate = new Date(referencePointInMillis);
		TimeZone mainOfficeTimeZone = this.getTimeZone();

		Calendar referencePoint = Calendar.getInstance(mainOfficeTimeZone);
		referencePoint.setTime(referencePointDate);

		Calendar endOfDayCalendar = Calendar.getInstance(mainOfficeTimeZone);
		endOfDayCalendar.setTime(this.getEndOfDayTime());

		Calendar result = Calendar.getInstance(mainOfficeTimeZone);
		result.setTime(referencePointDate);
		result.set(
			Calendar.HOUR_OF_DAY,
			endOfDayCalendar.get(Calendar.HOUR_OF_DAY));
		result.set(Calendar.MINUTE, endOfDayCalendar.get(Calendar.MINUTE));
		result.set(Calendar.SECOND, endOfDayCalendar.get(Calendar.SECOND));
		result.set(Calendar.MILLISECOND, 0);

		long referenceTimeOfDay =
			referencePoint.get(Calendar.HOUR_OF_DAY) * 10000
				+ referencePoint.get(Calendar.MINUTE) * 100
				+ referencePoint.get(Calendar.SECOND);
		long endOfBusinessDayTime =
			endOfDayCalendar.get(Calendar.HOUR_OF_DAY) * 10000
				+ endOfDayCalendar.get(Calendar.MINUTE) * 100
				+ endOfDayCalendar.get(Calendar.SECOND);

		if (findStart)
		{
			if (referenceTimeOfDay <= endOfBusinessDayTime)
			{
				result.add(Calendar.DATE, -1);
			}
		} else
		{
			if (referenceTimeOfDay > endOfBusinessDayTime)
			{
				result.add(Calendar.DATE, 1);
			}
		}

		while (!this.getBusinessWeek().isBusinessDay(result))
		{
			result.add(Calendar.DATE, (findStart ? -1 : 1));
		}

		return (result.getTime().getTime());
	}

	public long getPreviousBusinessDayInMillis(long referencePointInMillis)
	{
		Calendar result = Calendar.getInstance(this.getTimeZone());
		result.setTime(
			new Date(
				this.getStartOfBusinessDayInMillis(referencePointInMillis)));
		result.add(Calendar.DATE, -1);
		while (!this.getBusinessWeek().isBusinessDay(result))
		{
			result.add(Calendar.DATE, -1);
		}
		return result.getTime().getTime();
	}
}
