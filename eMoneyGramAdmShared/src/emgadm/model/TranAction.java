package emgadm.model;

import java.math.BigDecimal;
import java.util.Date;

public class TranAction implements Comparable
{
	private int tranActionId;
	private int tranId;
	private String tranReasCode;
	private Date tranActionDate;
	private String tranConfId;
	private BigDecimal tranPostAmt;
	private String tranStatCode;
	private String tranSubStatCode;
	private String tranOldStatCode;
	private String tranOldSubStatCode;
	private String tranReasDesc;
	private int drGLAcctId;
	private int crGLAcctId;
	private String drGLAcctDesc;
	private String crGLAcctDesc;
	private String tranCustLastName;
	private String tranCustFrstName;
	private String tranCustLogonId;
	private String tranCreateUserid;
	private String subReasonCode;
	private String tranReasTypeCode;
	private String tranSubReasDesc;
	public int getTranActionId()
	{
		return tranActionId;
	}

	public void setTranActionId(int i)
	{
		tranActionId = i;
	}

	public int getTranId()
	{
		return tranId;
	}

	public void setTranId(int i)
	{
		tranId = i;
	}

	public String getTranReasCode()
	{
		return tranReasCode;
	}

	public void setTranReasCode(String string)
	{
		tranReasCode = string;
	}

	public Date getTranActionDate()
	{
		return tranActionDate;
	}

	public void setTranActionDate(Date date)
	{
		tranActionDate = date;
	}

	public String getTranConfId()
	{
		return tranConfId;
	}

	public void setTranConfId(String string)
	{
		tranConfId = string;
	}

	public BigDecimal getTranPostAmt()
	{
		return tranPostAmt;
	}

	public void setTranPostAmt(BigDecimal decimal)
	{
		tranPostAmt = decimal;
	}

	public String getTranStatCode()
	{
		return tranStatCode;
	}

	public void setTranStatCode(String string)
	{
		tranStatCode = string;
	}

	public String getTranSubStatCode()
	{
		return tranSubStatCode;
	}

	public void setTranSubStatCode(String string)
	{
		tranSubStatCode = string;
	}

	public String getTranOldStatCode()
	{
		return tranOldStatCode;
	}

	public void setTranOldStatCode(String string)
	{
		tranOldStatCode = string;
	}

	public String getTranOldSubStatCode()
	{
		return tranOldSubStatCode;
	}

	public void setTranOldSubStatCode(String string)
	{
		tranOldSubStatCode = string;
	}

	public String getTranReasDesc()
	{
		return tranReasDesc;
	}

	public void setTranReasDesc(String string)
	{
		tranReasDesc = string;
	}

	public int getDrGLAcctId()
	{
		return drGLAcctId;
	}

	public void setDrGLAcctId(int i)
	{
		drGLAcctId = i;
	}

	public int getCrGLAcctId()
	{
		return crGLAcctId;
	}

	public void setCrGLAcctId(int i)
	{
		crGLAcctId = i;
	}

	public String getTranCustLastName()
	{
		return tranCustLastName;
	}

	public void setTranCustLastName(String string)
	{
		tranCustLastName = string;
	}

	public String getTranCustFrstName()
	{
		return tranCustFrstName;
	}

	public void setTranCustFrstName(String string)
	{
		tranCustFrstName = string;
	}

	public String getTranCustLogonId()
	{
		return tranCustLogonId;
	}

	public void setTranCustLogonId(String string)
	{
		tranCustLogonId = string;
	}

	public String getTranCreateUserid()
	{
		return tranCreateUserid;
	}

	public void setTranCreateUserid(String string)
	{
		tranCreateUserid = string;
	}

	public String getDrGLAcctDesc()
	{
		return drGLAcctDesc;
	}

	public void setDrGLAcctDesc(String string)
	{
		drGLAcctDesc = string;
	}

	public String getCrGLAcctDesc()
	{
		return crGLAcctDesc;
	}

	public void setCrGLAcctDesc(String string)
	{
		crGLAcctDesc = string;
	}

	public int compareTo(Object obj)
	{
		TranAction ta = (TranAction) obj;
		return this.tranActionDate.after(ta.tranActionDate) ? -1 : 1;
	}

    /**
     * @return Returns the subReasonCode.
     */
    public String getSubReasonCode() {
        return subReasonCode;
    }
    /**
     * @param subReasonCode The subReasonCode to set.
     */
    public void setSubReasonCode(String src) {
        this.subReasonCode = src;
    }
    /**
     * @return Returns the tranReasTypeCode.
     */
    public String getTranReasTypeCode() {
        return tranReasTypeCode;
    }
    /**
     * @param tranReasTypeCode The tranReasTypeCode to set.
     */
    public void setTranReasTypeCode(String trtc) {
        this.tranReasTypeCode = trtc;
    }
    /**
     * @return Returns the tranSubReasDesc.
     */
    public String getTranSubReasDesc() {
        return tranSubReasDesc;
    }
    /**
     * @param tranSubReasDesc The tranSubReasDesc to set.
     */
    public void setTranSubReasDesc(String tsrd) {
        this.tranSubReasDesc = tsrd;
    }
}
