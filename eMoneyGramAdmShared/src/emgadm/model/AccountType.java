/*
 * Created on Feb 7, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

/**
 * @author T004
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AccountType {
	private String acctTypeCode;
	private String acctTypeDesc;
	
	/**
	 * @return
	 */
	public String getAcctTypeCode() {
		return acctTypeCode == null ? "" : acctTypeCode;
	}

	/**
	 * @param string
	 */
	public void setAcctTypeCode(String string) {
		acctTypeCode = string;
	}

	/**
	 * @return
	 */
	public String getAcctTypeDesc() {
		return acctTypeDesc == null ? "" : acctTypeDesc;
	}

	/**
	 * @param string
	 */
	public void setAcctTypeDesc(String string) {
		acctTypeDesc = string;
	}

}
