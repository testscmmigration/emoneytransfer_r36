package emgadm.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import org.apache.log4j.Logger;

import emgshared.dataaccessors.EMGSharedLogger;




public class PersonComparator implements Comparator<Person> {
	private static Logger logger = EMGSharedLogger.getLogger(
			PersonComparator.class);
	
	public int compare(Person personOne, Person personTwo) {
		Date personOneLastSeenDate = null;
		Date personTwoLastSeenDate = null;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/yyyy");
		try {
			if (personOne.getDateLastSeen() != null
					&& !"".equals(personOne.getDateLastSeen())) {
				//MBO-11528 - To reformat existing date with null month (null/yyyy) to only year (yyyy) to handle exception
				String lastSeenOne = personOne.getDateLastSeen();
				if(lastSeenOne.contains("null/"))
					lastSeenOne = lastSeenOne.substring(lastSeenOne.indexOf("/")+1);
				if(!lastSeenOne.contains("/"))
					simpleDateFormat = new SimpleDateFormat("yyyy");
				personOneLastSeenDate = simpleDateFormat.parse(lastSeenOne);
			}
			if (personTwo.getDateLastSeen() != null
					&& !"".equals(personTwo.getDateLastSeen())) {
				//MBO-11528 - To reformat existing date with null month (null/yyyy) to only year (yyyy)to handle exception
				String lastSeenTwo = personTwo.getDateLastSeen();
				if(lastSeenTwo.contains("null/"))
					lastSeenTwo = lastSeenTwo.substring(lastSeenTwo.indexOf("/")+1);
				if(!lastSeenTwo.contains("/"))
					simpleDateFormat = new SimpleDateFormat("yyyy");
				personTwoLastSeenDate = simpleDateFormat.parse(lastSeenTwo);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			logger.error("Excetipon during last seen date parsing "+e.getMessage());
		}
		if (personOneLastSeenDate == null && null == personTwoLastSeenDate) {
			return 0;
		} else if (personOneLastSeenDate != null
				&& null == personTwoLastSeenDate) {
			return 1;
		} else if (personOneLastSeenDate == null
				&& null != personTwoLastSeenDate) {
			return -1;
		} else {
			return personOneLastSeenDate.compareTo(personTwoLastSeenDate);
		}
	}

}
