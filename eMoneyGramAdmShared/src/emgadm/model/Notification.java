package emgadm.model;

public class Notification {

	private String sourceSite;
	
	private String notificationType;
	
	private String languagePreference;
	
	private String notificationMethod;
	
	private String phoneNumber;
	
	private String countryDialingCode;
	
	private String securityCode;
	
	private String tranRefNum;
	
	private String generalPhoneNumber;
	
	private String tranId;
	
	private String consumerId;
	
	private String emailId;
	
	private String firstName;
	
	private String lastName;
	

	public String getTranId() {
		return tranId;
	}

	public void setTranId(String tranId) {
		this.tranId = tranId;
	}

	public String getGeneralPhoneNumber() {
		return generalPhoneNumber;
	}

	public void setGeneralPhoneNumber(String generalPhoneNumber) {
		this.generalPhoneNumber = generalPhoneNumber;
	}

	public String getTranRefNum() {
		return tranRefNum;
	}

	public void setTranRefNum(String tranRefNum) {
		this.tranRefNum = tranRefNum;
	}

	public String getSourceSite() {
		return sourceSite;
	}

	public void setSourceSite(String sourceSite) {
		this.sourceSite = sourceSite;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public String getLanguagePreference() {
		return languagePreference;
	}

	public void setLanguagePreference(String languagePreference) {
		this.languagePreference = languagePreference;
	}

	public String getNotificationMethod() {
		return notificationMethod;
	}

	public void setNotificationMethod(String notificationMethod) {
		this.notificationMethod = notificationMethod;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCountryDialingCode() {
		return countryDialingCode;
	}

	public void setCountryDialingCode(String countryDialingCode) {
		this.countryDialingCode = countryDialingCode;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public String getConsumerId() {
		return consumerId;
	}

	public void setConsumerId(String consumerId) {
		this.consumerId = consumerId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
