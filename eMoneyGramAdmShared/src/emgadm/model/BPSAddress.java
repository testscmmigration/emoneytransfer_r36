package emgadm.model;

import java.util.List;

public class BPSAddress {
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	private String postalCode;
	private String subCity;
	private String firstSeenDate;
	private java.util.ArrayList<BPSPhoneDetails> bpsPhoneDetails;
	private String lastSeenDate;

	protected static String nullCheck(String addressAttribute) {
		return addressAttribute != null ? addressAttribute : "";
	}

	public BPSAddress(BPSAddress bpsAddress) {
		this.addressLine1 = nullCheck(bpsAddress.getAddressLine1());
		this.addressLine2 = nullCheck(bpsAddress.getAddressLine2());
		this.city = nullCheck(bpsAddress.getCity());
		this.state = nullCheck(bpsAddress.getState());
		this.postalCode = nullCheck(bpsAddress.getPostalCode());
		this.subCity = nullCheck(bpsAddress.getSubCity());
		this.firstSeenDate = nullCheck(bpsAddress.getFirstSeenDate());
		this.lastSeenDate = nullCheck(bpsAddress.getLastSeenDate());
		this.bpsPhoneDetails = bpsAddress.getBpsPhoneDetails() != null ? bpsAddress.getBpsPhoneDetails() : null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((addressLine1 == null) ? 0 : addressLine1.hashCode());
		result = prime * result
				+ ((addressLine2 == null) ? 0 : addressLine2.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result
				+ ((firstSeenDate == null) ? 0 : firstSeenDate.hashCode());
		result = prime * result
				+ ((lastSeenDate == null) ? 0 : lastSeenDate.hashCode());
		result = prime * result
				+ ((postalCode == null) ? 0 : postalCode.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((subCity == null) ? 0 : subCity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BPSAddress other = (BPSAddress) obj;
		if (addressLine1 == null) {
			if (other.addressLine1 != null)
				return false;
		} else if (!addressLine1.equals(other.addressLine1))
			return false;
		if (addressLine2 == null) {
			if (other.addressLine2 != null)
				return false;
		} else if (!addressLine2.equals(other.addressLine2))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (firstSeenDate == null) {
			if (other.firstSeenDate != null)
				return false;
		} else if (!firstSeenDate.equals(other.firstSeenDate))
			return false;
		if (lastSeenDate == null) {
			if (other.lastSeenDate != null)
				return false;
		} else if (!lastSeenDate.equals(other.lastSeenDate))
			return false;
		if (postalCode == null) {
			if (other.postalCode != null)
				return false;
		} else if (!postalCode.equals(other.postalCode))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (subCity == null) {
			if (other.subCity != null)
				return false;
		} else if (!subCity.equals(other.subCity))
			return false;
		return true;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getSubCity() {
		return subCity;
	}

	public void setSubCity(String subCity) {
		this.subCity = subCity;
	}

	public String getFirstSeenDate() {
		return firstSeenDate;
	}

	public void setFirstSeenDate(String firstSeenDate) {
		this.firstSeenDate = firstSeenDate;
	}

	public java.util.ArrayList<BPSPhoneDetails> getBpsPhoneDetails() {
		return bpsPhoneDetails;
	}

	public void setBpsPhoneDetails(java.util.ArrayList<BPSPhoneDetails> bpsPhoneDetails) {
		this.bpsPhoneDetails = bpsPhoneDetails;
	}

	public String getLastSeenDate() {
		return lastSeenDate;
	}

	public void setLastSeenDate(String lastSeenDate) {
		this.lastSeenDate = lastSeenDate;
	}

}
