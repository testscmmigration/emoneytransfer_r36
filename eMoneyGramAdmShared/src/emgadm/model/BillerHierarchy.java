/*
 * Created on Nov 20, 2003
 */
package emgadm.model;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import emgadm.dataaccessors.BillerMainOfficeManager;
import emgadm.dataaccessors.ManagerFactory;
import emgadm.util.StringHelper;
import emgshared.exceptions.DataSourceException;
/**
 * @author A119
 */
public class BillerHierarchy
{
	private Agent _mainOffice;
	private List _subDivisionList;
	private List _locationList;

	/** This constuctor is used to create a biller hierarchy for a User Profile to be used 
	 *  throughout the users login session.  
	 */

	public BillerHierarchy(String primaryUserLocation)
		throws DataSourceException
	{
		BillerMainOfficeManager bmoManager =
			ManagerFactory.createBillerMainOfficeManager();
//		this._mainOffice = bmoManager.getMainOffice(primaryUserLocation);
//		this.setSubDivisionList(
//			bmoManager.getDivisions(this.getMainOfficeId()));
		this.setLocationList(bmoManager.getLocations(this.getMainOfficeId()));
	}

	/**
	 *  This constuctor was implemented to support demo functionality. 
	 */

	public BillerHierarchy(Agent mainOffice, List subDivisions, List locations)
	{
		this._mainOffice = mainOffice;
		this.setSubDivisionList(subDivisions);
		this.setLocationList(locations);
	}

	/**
	*  This method can be used to get an Agent object by just knowing the agent ID.
	*/

	public Agent getAgentByAgentID(String agentID)
	{
		Agent returnAgent = null;
		List agentList = this.getLocationList();
		if (agentList != null)
		{
			for (Iterator i = agentList.iterator(); i.hasNext();)
			{
				Agent agent = (Agent) i.next();
				if (agent.getAgentId().equals(agentID))
				{
					returnAgent = agent;
					break;
				}
			}
		}
		return returnAgent;
	}

	/**
	*  This method can be used to get a subdivision Agent object
	*  by just knowing the agent ID.
	*/

	public Agent getSubDivisionByAgentID(String agentID)
	{
		Agent returnAgent = null;
		List agentList = this.getSubDivisionList();
		if (agentList != null)
		{
			for (Iterator i = agentList.iterator(); i.hasNext();)
			{
				Agent agent = (Agent) i.next();
				if (agent.getAgentId().equals(agentID))
				{
					returnAgent = agent;
					break;
				}
			}
		}
		return returnAgent;
	}

	/**
	 * @return
	 */
	public int getLocationCount()
	{
		return _locationList == null ? 0 : _locationList.size();
	}

	/**
	 * @return
	 */
	public List getLocationList()
	{
		return _locationList == null
			? new ArrayList(0)
			: Collections.unmodifiableList(_locationList);
	}

	/**
	 * @return
	 */
	public int getSubDivisionCount()
	{
		return _subDivisionList == null ? 0 : _subDivisionList.size();
	}

	/**
	 * @return
	 */
	public List getSubDivisionList()
	{
		return _subDivisionList == null
			? new ArrayList(0)
			: Collections.unmodifiableList(_subDivisionList);
	}

	/**
	 * @param list
	 */
	private void setLocationList(List list)
	{
		_locationList = list;
	}

	/**
	 * @param list
	 */
	private void setSubDivisionList(List list)
	{
		_subDivisionList = list;
	}

	/**
	 * @return
	 */

	public String getMainOfficeId()
	{
		return _mainOffice == null ? "" : _mainOffice.getAgentId(); //$NON-NLS-1$
	}

	public String getMainOfficeName()
	{
		return _mainOffice == null ? "" : _mainOffice.getAgentName(); //$NON-NLS-1$
	}

	/**
	 * Returns true if the given agent or any ancestor
	 * is closed.  Otherwise, false.
	 * 
	 * @param agentId
	 * @return	true if the given agent or any ancestor
	 * 			is closed.  Otherwise, false.
	 */
	public boolean isHierarchyClosed(String agentId)
	{
		boolean closed = false;

		if (!StringHelper.isNullOrEmpty(agentId))
		{
			Agent agent = findAgentInHierarchy(agentId);
			if (agent != null)
			{
				closed = agent.isClosed();
				String parentId = agent.getAgentParentID();
				if (!closed
					&& !agentId.equalsIgnoreCase(parentId))
				{
					closed = isHierarchyClosed(parentId);
				}
			}
		}

		return (closed);
	}

	public Agent findAgentInHierarchy(String agentId)
	{
		Agent agent = null;

		if (agentId != null)
		{
			if (agentId.equals(this.getMainOfficeId()))
			{
				agent = _mainOffice;
			}

			if (agent == null)
			{
				agent = getSubDivisionByAgentID(agentId);
			}

			if (agent == null)
			{
				agent = getAgentByAgentID(agentId);
			}
		}

		return (agent);
	}
	/**
	 * @return
	 */
	public Agent getMainOffice()
	{
		return _mainOffice;
	}

	public boolean isDescendant(String ancestorAgentId, Agent agent)
	{
		boolean descendant = false;
		if (ancestorAgentId != null)
		{
			for (;
				descendant == false
					&& agent != null
					&& agent.getAgentParentID() != null
					&& !agent.getAgentParentID().equalsIgnoreCase(
						agent.getAgentId());
				agent = findAgentInHierarchy(agent.getAgentParentID()))
			{
				descendant =
					agent.getAgentParentID().equalsIgnoreCase(ancestorAgentId);
			}
		}
		return (descendant);
	}
}
