/*
 * Created on Feb 7, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

/**
 * @author T008
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TranStatus {
	private String tranStatCode;
	private String tranStatDesc;
	
	/**
	 * @return
	 */
	public String getTranStatCode() {
		return tranStatCode == null ? "" : tranStatCode;
	}

	/**
	 * @param string
	 */
	public void setTranStatCode(String string) {
		tranStatCode = string;
	}

	/**
	 * @return
	 */
	public String getTranStatDesc() {
		return tranStatDesc == null ? "" : tranStatDesc;
	}

	/**
	 * @param string
	 */
	public void setTranStatDesc(String string) {
		tranStatDesc = string;
	}

}
