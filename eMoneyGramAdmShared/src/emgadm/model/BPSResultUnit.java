package emgadm.model;

import java.util.LinkedHashSet;
import java.util.LinkedList;

public class BPSResultUnit {
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public LinkedList<BPSAddress> getAddressList() {
		return addressList;
	}
	public void setAddressList(LinkedList<BPSAddress> addressList) {
		this.addressList = addressList;
	}
	
	public SSNinfo getSsnInfo() {
		return ssnInfo;
	}
	public void setSsnInfo(SSNinfo ssnInfo) {
		this.ssnInfo = ssnInfo;
	}
	public BPSResultUnit(String uniqueId, LinkedList<BPSAddress> addressList,
			LinkedList<BasicPersonDetails> personList, SSNinfo ssnInfo) {
		super();
		this.uniqueId = nullcheck(uniqueId);
		this.addressList = addressList;
		this.personList = personList;
		this.ssnInfo = ssnInfo;
	}
	protected static String nullcheck(String resultUnit){
		return resultUnit == null ? "" : resultUnit;
	}
	
	public BPSResultUnit(){
		super();
	}
	private String uniqueId;
	private LinkedList<BPSAddress> addressList;
	private LinkedList<BasicPersonDetails> personList;
	private LinkedHashSet<BasicPersonDetailsAcc> accessoryDetailsSet; 
	public LinkedHashSet<BasicPersonDetailsAcc> getAccessoryDetailsSet() {
		return accessoryDetailsSet;
	}
	public void setAccessoryDetailsSet(
			LinkedHashSet<BasicPersonDetailsAcc> accessoryDetailsSet) {
		this.accessoryDetailsSet = accessoryDetailsSet;
	}
	public void setPersonList(LinkedList<BasicPersonDetails> personList) {
		this.personList = personList;
	}
	public LinkedList<BasicPersonDetails> getPersonList() {
		return personList;
	}
	
	private SSNinfo ssnInfo;
	private java.util.LinkedHashSet<String> personNameSet;
	
	public java.util.LinkedHashSet<String> getPersonNameSet() {
		return personNameSet;
	}
	public void setPersonNameSet(java.util.LinkedHashSet<String> personNameSet) {
		this.personNameSet = personNameSet;
	}
	
	
}
