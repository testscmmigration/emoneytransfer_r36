package emgadm.model;


public enum KBAResultType {
    PASSED("passed"), QUESTIONS("questions"), FAILED("failed"), ERROR("error");

    private String value;

    KBAResultType(String value) {
        this.value = value;

    }

    public String getValue() {
        return this.value;
    }

}
