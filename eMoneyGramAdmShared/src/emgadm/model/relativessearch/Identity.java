package emgadm.model.relativessearch;

public class Identity {
	private String prefix;
	private String firstName;
	private String lastName;
	private String middleName;
	private String gender;
	private String dob;
	private String dod;
	private String age;
	private String deceased;
	private String deathVerificationCode;
	
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = checkNull(prefix);
	}
	
	public String getDod() {
		return dod;
	}
	public void setDod(String dod) {
		this.dod = checkNull(dod);
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = checkNull(age);
	}
	public String getDeceased() {
		return deceased;
	}
	public void setDeceased(String deceased) {
		this.deceased = checkNull(deceased);
	}
	public String getDeathVerificationCode() {
		return deathVerificationCode;
	}
	public void setDeathVerificationCode(String deathVerificationCode) {
		this.deathVerificationCode = checkNull(deathVerificationCode);
	}
	private emgadm.model.SSNinfo ssnInfo;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = checkNull(firstName);
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = checkNull(lastName);
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = checkNull(middleName);
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = checkNull(gender);
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = checkNull(dob);
	}
	public emgadm.model.SSNinfo getSsnInfo() {
		return ssnInfo;
	}
	public void setSsnInfo(emgadm.model.SSNinfo ssnInfo) {
		this.ssnInfo = ssnInfo;
	}
	public static String checkNull(String attribute){
		return attribute == null? "" : attribute;		
	}
}
