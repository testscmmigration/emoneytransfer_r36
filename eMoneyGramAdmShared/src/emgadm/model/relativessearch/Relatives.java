package emgadm.model.relativessearch;

import java.util.LinkedList;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class Relatives {
	@XStreamImplicit(itemFieldName="Relative")
	private LinkedList<emgadm.model.relativessearch.Relative> relativesList;

	public LinkedList<emgadm.model.relativessearch.Relative> getRelativesList() {
		return relativesList;
	}

	public void setRelativesList(LinkedList<emgadm.model.relativessearch.Relative> relativesList) {
		this.relativesList = relativesList;
		System.out.println("helllllo");
	}
}
