package emgadm.model.relativessearch;

public class BaseAddress {
	private String dateFirstSeen;
	private String dateLastSeen;
	private String shared;
	private emgadm.model.relativessearch.Address address;
	public String getDateFirstSeen() {
		return dateFirstSeen;
	}
	public void setDateFirstSeen(String dateFirstSeen) {
		this.dateFirstSeen = dateFirstSeen;
	}
	public String getDateLastSeen() {
		return dateLastSeen;
	}
	public void setDateLastSeen(String dateLastSeen) {
		this.dateLastSeen = dateLastSeen;
	}
	public String getShared() {
		return shared;
	}
	public void setShared(String shared) {
		this.shared = shared;
	}
	public emgadm.model.relativessearch.Address getAddress() {
		return address;
	}
	public void setAddress(emgadm.model.relativessearch.Address address) {
		this.address = address;
	}
	
}
