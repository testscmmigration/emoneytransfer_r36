package emgadm.model.relativessearch;

public class Address {
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String subCity;
	private String state;
	private String postalCode;
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = checkNull(addressLine1);
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = checkNull(addressLine2);
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = checkNull(city);
	}
	public String getSubCity() {
		return subCity;
	}
	public void setSubCity(String subCity) {
		this.subCity = checkNull(subCity);
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = checkNull(state);
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = checkNull(postalCode);
	}
	public static String checkNull(String attribute){
		return attribute != null ? attribute : "";
	}
	
	private String formattedAddress;
	public String getFormattedAddress() {
		return formattedAddress;
	}
	public void setFormattedAddress(String formattedAddress) {
		this.formattedAddress = formattedAddress;
	}
	
	/**
	 * to format the address in accordance to the display
	 * 
	 */
	public void formatAddress(){
		StringBuilder sb = new StringBuilder();
		final String space = "\u00A0";
		sb.append(this.addressLine1 != null ?  this.addressLine1 : "");	
		sb.append(this.addressLine2 != null ? (space + this.addressLine2) : "");	
		sb.append(this.subCity != null ? (space + this.subCity) : "");	
		sb.append(this.city != null ? (space + this.city) : "");
		sb.append(",");		
		sb.append(this.state != null ? (space + this.state) : "");
		sb.append(this.postalCode != null ? (space + this.postalCode) : "");
		this.formattedAddress = sb.toString();
	}
}
