package emgadm.model.relativessearch;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class AssociateAddresses {
	@XStreamImplicit(itemFieldName="AssociateAddress")
	private java.util.LinkedList<BaseAddress> associateAddressList;

	public java.util.LinkedList<BaseAddress> getAssociateAddressList() {
		return associateAddressList;
	}

	public void setAssociateAddressList(
			java.util.LinkedList<BaseAddress> associateAddressList) {
		this.associateAddressList = associateAddressList;
	}

	
}
