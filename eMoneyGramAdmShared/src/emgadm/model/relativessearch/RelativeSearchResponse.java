package emgadm.model.relativessearch;


public class RelativeSearchResponse {
	
	private emgadm.model.relativessearch.Relatives relatives;
	private String transactionId;
	private String status;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public emgadm.model.relativessearch.Relatives getRelatives() {
		return relatives;
	}

	public void setRelatives(emgadm.model.relativessearch.Relatives relatives) {
		this.relatives = relatives;
	}
	
	private emgadm.model.relativessearch.Associates associates;

	public emgadm.model.relativessearch.Associates getAssociates() {
		return associates;
	}

	public void setAssociates(
			emgadm.model.relativessearch.Associates associates) {
		this.associates = associates;
	}
	
}
