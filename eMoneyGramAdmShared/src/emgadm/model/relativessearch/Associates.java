package emgadm.model.relativessearch;

import java.util.LinkedList;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class Associates {
	@XStreamImplicit(itemFieldName="Associate")
	private LinkedList<emgadm.model.relativessearch.Associate> associateList;

	public LinkedList<emgadm.model.relativessearch.Associate> getAssociateList() {
		return associateList;
	}

	public void setAssociateList(LinkedList<emgadm.model.relativessearch.Associate> associateList) {
		this.associateList = associateList;
	}
}
