package emgadm.model.relativessearch;

public class Associate {
	private emgadm.model.relativessearch.Identity associateIdentity;
	private emgadm.model.relativessearch.AssociateAddresses associateAddresses;
	private String uniqueId;
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId != null && !("null".equals(uniqueId)) ? uniqueId : "";
	}
	public emgadm.model.relativessearch.Identity getAssociateIdentity() {
		return associateIdentity;
	}
	public void setAssociateIdentity(
			emgadm.model.relativessearch.Identity associateIdentity) {
		this.associateIdentity = associateIdentity;
	}
	public emgadm.model.relativessearch.AssociateAddresses getAssociateAddresses() {
		return associateAddresses;
	}
	public void setAssociateAddresses(
			emgadm.model.relativessearch.AssociateAddresses associateAddresses) {
		this.associateAddresses = associateAddresses;
	}
}
