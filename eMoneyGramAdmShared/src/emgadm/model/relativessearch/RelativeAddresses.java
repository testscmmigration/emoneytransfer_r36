package emgadm.model.relativessearch;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class RelativeAddresses {
	@XStreamImplicit(itemFieldName="RelativeAddress")
	private java.util.LinkedList<BaseAddress> relativeAddressList;

	public java.util.LinkedList<BaseAddress> getRelativeAddressList() {
		return relativeAddressList;
	}

	public void setRelativeAddressList(
			java.util.LinkedList<BaseAddress> relativeAddressList) {
		this.relativeAddressList = relativeAddressList;
	}

}
