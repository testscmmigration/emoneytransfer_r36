package emgadm.model.relativessearch;

public class Relative {
	private String uniqueId;
	private int depth;
	
	public int getDepth() {
		return depth;
	}
	public void setDepth(int depth) {
		this.depth = depth;
	}
	private emgadm.model.relativessearch.Identity relativeIdentity;
	private emgadm.model.relativessearch.RelativeAddresses relativeAddresses;
	private emgadm.model.relativessearch.Relatives relatives;
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId == null ? "" : uniqueId;
	}
	public emgadm.model.relativessearch.Identity getRelativeIdentity() {
		return relativeIdentity;
	}
	public void setRelativeIdentity(
			emgadm.model.relativessearch.Identity relativeIdentity) {
		this.relativeIdentity = relativeIdentity;
	}
	public emgadm.model.relativessearch.RelativeAddresses getRelativeAddresses() {
		return relativeAddresses;
	}
	public void setRelativeAddresses(
			emgadm.model.relativessearch.RelativeAddresses relativeAddresses) {
		this.relativeAddresses = relativeAddresses;
	}
	public emgadm.model.relativessearch.Relatives getRelatives() {
		return relatives;
	}
	public void setRelatives(emgadm.model.relativessearch.Relatives relatives) {
		this.relatives = relatives;
	}
}
