package emgadm.model;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.ParseDateException;
import emgshared.util.DateFormatter;

public class Trans implements Comparable {
	public static String sortBy = "transType";
	public static int sortSeq = 1;

	private String transUserId; // column 1.a
	private String transCreateDate; // column 1.b
	private String transCommand; // column 1.c
	private String transId; // column 2
	private String transSender; // column 3
	private String transCustId; // column 3a
	private String transSenderName; // column 3x
	private String transReceiver; // column 4
	private String transCntryCd; // column 4a
	private String transStatus; // column 5.1
	private String transSubStatus; // column 5.2
	private String transType; // column 6
	private String transAmount; // column 7
	private String transStatusDate; // column 8
	private int transScores; // column 9.1
	private String sysAutoRsltCode; // column 9.2
	private String esSendable;
	private String internetPurchase;
	private String tranRvwPrcsCode;
	private String partnerSiteId;
	private String clientTraceId;
	private String processorTraceId;
	private String sndISOCrncyId;

	public String getTransUserId() {
		return transUserId;
	}

	public void setTransUserId(String string) {
		transUserId = string;
	}

	public String getTransCreateDate() {
		return transCreateDate;
	}

	public void setTransCreateDate(String string) {
		transCreateDate = string;
	}

	public String getTransCommand() {
		return transCommand;
	}

	public void setTransCommand(String string) {
		transCommand = string;
	}

	public String getTransId() {
		return transId;
	}

	public void setTransId(String string) {
		transId = string;
	}

	public String getTransSender() {
		return transSender;
	}

	public void setTransSender(String string) {
		transSender = string;
	}

	public String getTransCustId() {
		return transCustId == null ? "" : transCustId;
	}

	public void setTransCustId(String string) {
		transCustId = string;
	}

	public String getTransReceiver() {
		return transReceiver;
	}

	public void setTransReceiver(String string) {
		transReceiver = string;
	}

	public String getTransCntryCd() {
		return transCntryCd;
	}

	public void setTransCntryCd(String string) {
		transCntryCd = string;
	}

	public String getTransStatus() {
		return transStatus;
	}

	public void setTransStatus(String string) {
		transStatus = string;
	}

	public String getTransSubStatus() {
		return transSubStatus;
	}

	public void setTransSubStatus(String string) {
		transSubStatus = string;
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String string) {
		transType = string;
	}

	public String getTransAmount() {
		return transAmount;
	}

	public void setTransAmount(String string) {
		transAmount = string;
	}

	public String getTransStatusDate() {
		return transStatusDate;
	}

	public void setTransStatusDate(String string) {
		transStatusDate = string;
	}

	public int getTransScores() {
		return transScores;
	}

	public void setTransScores(int i) {
		transScores = i;
	}

	public String getSysAutoRsltCode() {
		return sysAutoRsltCode == null ? "" : sysAutoRsltCode;
	}

	public void setSysAutoRsltCode(String string) {
		sysAutoRsltCode = string;
	}

	public String getTransSenderName() {
		return transSenderName;
	}

	public void setTransSenderName(String string) {
		transSenderName = string;
	}

	public String getEsSendable() {
		return esSendable;
	}

	public void setEsSendable(String s) {
		esSendable = s;
	}

	public String getInternetPurchase() {
		return internetPurchase;
	}

	public void setInternetPurchase(String string) {
		internetPurchase = string;
	}

	public String getTranRvwPrcsCode() {
		return tranRvwPrcsCode;
	}

	public void setTranRvwPrcsCode(String rvwPrcsCode) {
		this.tranRvwPrcsCode = rvwPrcsCode;
	}

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}

	public String getClientTraceId() {
		return clientTraceId;
	}

	public void setClientTraceId(String clientTraceId) {
		this.clientTraceId = clientTraceId;
	}

	public String getProcessorTraceId() {
		return processorTraceId;
	}

	public void setProcessorTraceId(String processorTraceId) {
		this.processorTraceId = processorTraceId;
	}

	public int compareTo(Object o) {
		Trans t = (Trans) o;

		if (Trans.sortBy == null) {
			Trans.sortBy = "transId";
		}

		if (Trans.sortBy.equalsIgnoreCase("transUserId")) {
			return (transUserId.compareToIgnoreCase(t.transUserId)) * sortSeq;
		}

		if (Trans.sortBy.equalsIgnoreCase("transId")) {
			return (Integer.parseInt(transId) - Integer.parseInt(t.transId))
					* sortSeq;
		}

		if (Trans.sortBy.equalsIgnoreCase("transSenderName")) {
			return (transSenderName.compareToIgnoreCase(t.transSenderName))
					* sortSeq;
		}

		if (Trans.sortBy.equalsIgnoreCase("transReceiver")) {
			return (transReceiver.compareToIgnoreCase(t.transReceiver))
					* sortSeq;
		}

		if (Trans.sortBy.equalsIgnoreCase("transCntryCd")) {
			return (transCntryCd.compareToIgnoreCase(t.transCntryCd)) * sortSeq;
		}

		if (Trans.sortBy.equalsIgnoreCase("transStatus")) {
			return (transStatus.compareToIgnoreCase(t.transStatus)) * sortSeq;
		}

		if (Trans.sortBy.equalsIgnoreCase("transSubStatus")) {
			return (transSubStatus.compareToIgnoreCase(t.transSubStatus))
					* sortSeq;
		}

		DateFormatter dfmt = new DateFormatter("dd/MMM/yyyy hh:mm a", true);
		if (Trans.sortBy.equalsIgnoreCase("transStatusDate")) {
			try {
				return (dfmt.parse(transStatusDate).after(
						dfmt.parse(t.transStatusDate)) ? 1 : -1)
						* sortSeq;
			} catch (ParseDateException e) {
				EMGSharedLogger.getLogger(this.getClass()).error(
						e.getMessage(), e);
			}
		}

		if (Trans.sortBy.equalsIgnoreCase("transCreateDate")) {
			try {
				return (dfmt.parse(transCreateDate).after(
						dfmt.parse(t.transCreateDate)) ? 1 : -1)
						* sortSeq;
			} catch (ParseDateException e) {
				EMGSharedLogger.getLogger(this.getClass()).error(
						e.getMessage(), e);
			}
		}

		if (Trans.sortBy.equalsIgnoreCase("transScores")) {
			return (transScores - t.transScores) * sortSeq;
		}

		if (Trans.sortBy.equalsIgnoreCase("transType")) {
			return (transType.compareToIgnoreCase(t.transType)) * sortSeq;
		}

		if (Trans.sortBy.equalsIgnoreCase("transAmount")) {
			double n1 = 0;
			double n2 = 0;

			NumberFormat df = new DecimalFormat("#,##0.00");
			try {
				n1 = df.parse(transAmount).doubleValue();
				n2 = df.parse(t.transAmount).doubleValue();
			} catch (ParseException e) {
				EMGSharedLogger.getLogger(this.getClass()).error(
						e.getMessage(), e);
			}

			return (n1 >= n2 ? 1 : -1) * sortSeq;
		}

		return 0;
	}

	public void setSndISOCrncyId(String sndISOCrncyId) {
		this.sndISOCrncyId = sndISOCrncyId;
	}

	public String getSndISOCrncyId() {
		return sndISOCrncyId;
	}

}
