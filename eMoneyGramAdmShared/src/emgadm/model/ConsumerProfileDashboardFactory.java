package emgadm.model;

import emgshared.util.Constants;

public class ConsumerProfileDashboardFactory {

	public static ConsumerProfileDashboard create(String parentSiteId) {
		if (Constants.MGO_PARTNER_SITE_ID.equals(parentSiteId)
				|| Constants.WAP_PARTNER_SITE_ID.equals(parentSiteId)) {
			return new USConsumerProfileDashboard();
		} else if (Constants.MGOUK_PARTNER_SITE_ID.equals(parentSiteId)) {
			return new UKConsumerProfileDashboard();
		} else if (Constants.MGODE_PARTNER_SITE_ID.equals(parentSiteId)) {
			return new DEConsumerProfileDashboard();
		} else if (Constants.MGOZAF_PARTNER_SITE_ID.equals(parentSiteId)) {
            return new DEConsumerProfileDashboard();
        } else {
        	//return default dashboard which is US Dashboard-MBO 6717
			return new USConsumerProfileDashboard();
		}
	}

}
