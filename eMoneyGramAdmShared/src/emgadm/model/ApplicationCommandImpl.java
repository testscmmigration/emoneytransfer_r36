package emgadm.model;

import java.io.Serializable;
import java.util.Comparator;

import emgadm.util.StringHelper;

class ApplicationCommandImpl implements ApplicationCommand, Serializable {
	private String displayName;
	private String menuGroupName;
	private String id;
	private String description;
	private int menuLevel;
	private boolean allowInternal;
	private boolean isAuthorizationRequired;
	private boolean isMenuItem;

	private static final Comparator<ApplicationCommand> menuDisplayNameComparator = new Comparator<ApplicationCommand>() {
		public int compare(ApplicationCommand c1, ApplicationCommand c2) {
			int comparison = 0;
			if (c1 == null) {
				comparison = (c2 == null ? 0 : 1);
			} else if (c2 == null) {
				comparison = -1;
			} else {
				if (c1.isMenuItem() != c2.isMenuItem()) {
					comparison = (c1.isMenuItem() ? -1 : 1);
				} else {
					comparison = StringHelper.compareToIgnoreCase(c1.getMenuGroupName(), c2
							.getMenuGroupName());
					if (comparison == 0) {
						comparison = StringHelper.compareToIgnoreCase(c1.getDescription(), c2
								.getDescription());
					}
				}
			}
			return comparison;
		}
	};

	ApplicationCommandImpl() {
	}

	public String getDisplayName() {
		return displayName;
	}

	public int getMenuLevel() {
		return menuLevel;
	}

	public String getMenuGroupName() {
		return menuGroupName;
	}

	public String getId() {
		return id;
	}

	public boolean isMenuItem() {
		return isMenuItem;
	}

	public boolean isAllowInternal() {
		return allowInternal;
	}

	public boolean isAuthorizationRequired() {
		return isAuthorizationRequired;
	}

	public void setAllowInternal(String b) {
		allowInternal = Boolean.valueOf(b).booleanValue();
	}

	public void setAuthorizationRequired(String b) {
		isAuthorizationRequired = Boolean.valueOf(b).booleanValue();
	}

	public void setMenuLevel(String i) {
		if (i.trim().length() == 0) {
			menuLevel = 0;
		} else {
			menuLevel = Integer.parseInt(i);
		}
	}

	public void setMenuGroupName(String string) {
		menuGroupName = string;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setMenuItem(boolean isMenuItem) {
		this.isMenuItem = isMenuItem;
	}

	public void setDisplayName(String string) {
		displayName = string;
	}

	/**
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param string
	 */
	public void setDescription(String string) {
		description = string;
	}

	public Comparator getCommandSorterByMenuAndDisplayName() {
		return menuDisplayNameComparator;
	}
}
