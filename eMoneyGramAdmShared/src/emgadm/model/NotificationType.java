package emgadm.model;

public enum NotificationType {
	Transfer_Sent_Cash_PickUp,
	Transfer_Sent_Account_Deposit,
	Transfer_Sent_Home_Delivery,
	Transaction_Declined,
	Express_Payment_Processing,
	Card_Declined,Receiver_Cash_PickUp,
	Receiver_Account_Deposit,
	Receiver_Home_Delivery;
}
