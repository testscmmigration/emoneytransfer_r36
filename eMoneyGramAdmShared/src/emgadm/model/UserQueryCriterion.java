package emgadm.model;

import emgadm.constants.EMoneyGramAdmLDAPKeys;
import emgadm.util.StringHelper;
/**
 * Title:
 * Description:
 * Company:
 * @author
 * @version 1.0
 */
public class UserQueryCriterion extends QueryCriterion {

	public static final char DATA_SOURCE_WILDCARD = '*';
	private String _userID;
	private String _firstName;
	private String _lastName;
	private String _webStatus;
	private Boolean _internalUser;
	private String _roleId;

	public UserQueryCriterion() {
	}

	public UserQueryCriterion(String userID, String firstName, String lastName, String webStatus, String roleId, boolean internal) throws Exception {
		this.setFirstName(firstName);
		this.setInternalUser(new Boolean(internal));
		this.setLastName(lastName);
		this.setUserID(userID);
		this.setWebStatus(webStatus);
		this.setRoleId(roleId);
	}

	public UserQueryCriterion(String uid) {

		this.setUserID(uid);
	}

	public String getLDAPFilter() {
		StringBuilder query = new StringBuilder(256);

		int numberQueryFields = 0;
		if (!StringHelper.isNullOrEmpty(this.getUserID())) {
			numberQueryFields++;
			query.append("(" + EMoneyGramAdmLDAPKeys.USER_ID + "=" + this.getUserID() + "*)"); //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$
		}
		if (!StringHelper.isNullOrEmpty(this.getFirstName())) {
			numberQueryFields++;
			query.append("(" + EMoneyGramAdmLDAPKeys.USER_FIRST_NAME + "=" + this.getFirstName() + "*)"); //$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$ //$NON-NLS-4$
		}
		if (!StringHelper.isNullOrEmpty(this.getLastName())) {
			numberQueryFields++;
			query.append("(" + EMoneyGramAdmLDAPKeys.USER_LAST_NAME + "=" + this.getLastName() + "*)"); //$NON-NLS-1$ //$NON-NLS-2$//$NON-NLS-3$ //$NON-NLS-4$
		}
		if (!StringHelper.isNullOrEmpty(this.getRoleId()) && !(this.getRoleId().equals("*"))) {
			numberQueryFields++;
			query.append("(" + EMoneyGramAdmLDAPKeys.USER_ROLES + "=" + EMoneyGramAdmLDAPKeys.CN + "=" + this.getRoleId() + "," + EMoneyGramAdmLDAPKeys.USER_ROLE_DN_SUFFIX + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		} else {
			numberQueryFields++;
			query.append("(" + EMoneyGramAdmLDAPKeys.USER_ROLES + "=" + EMoneyGramAdmLDAPKeys.EMT_ADMIN_USERS_GROUP_DN + ")"); //$NON-NLS-1$ 
		}
		StringBuilder begin = new StringBuilder(100);
		StringBuilder end = new StringBuilder(100);
		for (int i = 1; i < numberQueryFields; i++) {
			begin.append("(&"); //$NON-NLS-1$
			end.append(")"); //$NON-NLS-1$
		}
		return begin.toString() + query.toString() + end.toString();
	}

	/**
	 * @return
	 */
	public String getFirstName() {
		return _firstName;
	}

	/**
	 * @return
	 */
	public Boolean getInternalUser() {
		return _internalUser;
	}

	/**
	 * @return
	 */
	public String getLastName() {
		return _lastName;
	}

	/**
	 * @return
	 */
	public String getUserID() {
		return _userID;
	}

	/**
	 * @return
	 */
	public String getWebStatus() {
		return _webStatus;
	}

	/**
	 * @param string
	 */
	public final void setFirstName(String string) {
		string = StringHelper.trim(string);
		validateWildcard(string, DATA_SOURCE_WILDCARD);
		_firstName = string;
	}

	/**
	 * @param boolean1
	 */
	public final void setInternalUser(Boolean boolean1) {
		_internalUser = boolean1;
	}

	/**
	 * @param string
	 */
	public final void setLastName(String string) {
		string = StringHelper.trim(string);
		validateWildcard(string, DATA_SOURCE_WILDCARD);
		_lastName = string;
	}

	/**
	 * @param string
	 */
	public final void setUserID(String string) {
		_userID = string;
	}

	/**
	 * @param string
	 */
	public final void setWebStatus(String status) {
//		if (status == null)
//			_webStatus = null;
//		else if (status.equalsIgnoreCase(EMoneyGramAdmLDAPKeys.USER_IS_ACTIVE))
//			_webStatus = EMoneyGramAdmLDAPKeys.USER_IS_ACTIVE;
//		else if (status.equalsIgnoreCase(EMoneyGramAdmLDAPKeys.USER_IS_INACTIVE))
//			_webStatus = EMoneyGramAdmLDAPKeys.USER_IS_INACTIVE;
//
	}
	/**
	 * @return
	 */
	public String getRoleId() {
		return _roleId;
	}

	/**
	 * @param string
	 */
	public final void setRoleId(String string) {
		_roleId = string;
	}
}
