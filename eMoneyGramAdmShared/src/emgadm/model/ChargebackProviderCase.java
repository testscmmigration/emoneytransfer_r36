/*
 * Created on Aug 3, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

import java.util.Date;

/**
 * @author T004
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ChargebackProviderCase {
	private int chgbkCaseId;
	private int tranId;
	private String chargebackDenialCode;
	private String chargebackDenialDesc;
	private String chargebackProviderCaseId;
	private Date chargebackOriginalStatementDate;
	private Date createDate;
	private String createUserId;
	private Date updateDate;
	private String updateUserId;
	/**
	 * @return Returns the chargebackDenialCode.
	 */
	public String getChargebackDenialCode() {
		return chargebackDenialCode;
	}
	/**
	 * @param chargebackDenialCode The chargebackDenialCode to set.
	 */
	public void setChargebackDenialCode(String chargebackDenialCode) {
		this.chargebackDenialCode = chargebackDenialCode;
	}
	/**
	 * @return Returns the chargebackDenialDesc.
	 */
	public String getChargebackDenialDesc() {
		return chargebackDenialDesc;
	}
	/**
	 * @param chargebackDenialDesc The chargebackDenialDesc to set.
	 */
	public void setChargebackDenialDesc(String chargebackDenialDesc) {
		this.chargebackDenialDesc = chargebackDenialDesc;
	}
	/**
	 * @return Returns the chargebackProviderCaseId.
	 */
	public String getChargebackProviderCaseId() {
		return chargebackProviderCaseId;
	}
	/**
	 * @param chargebackProviderCaseId The chargebackProviderCaseId to set.
	 */
	public void setChargebackProviderCaseId(String chargebackProviderCaseId) {
		this.chargebackProviderCaseId = chargebackProviderCaseId;
	}
	/**
	 * @return Returns the chgbkCaseId.
	 */
	public int getChgbkCaseId() {
		return chgbkCaseId;
	}
	/**
	 * @param chgbkCaseId The chgbkCaseId to set.
	 */
	public void setChgbkCaseId(int chgbkCaseId) {
		this.chgbkCaseId = chgbkCaseId;
	}
	/**
	 * @return Returns the createUserId.
	 */
	public String getCreateUserId() {
		return createUserId;
	}
	/**
	 * @param createUserId The createUserId to set.
	 */
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	/**
	 * @return Returns the tranId.
	 */
	public int getTranId() {
		return tranId;
	}
	/**
	 * @param tranId The tranId to set.
	 */
	public void setTranId(int tranId) {
		this.tranId = tranId;
	}
	/**
	 * @return Returns the chargebackOriginalStatementDate.
	 */
	public Date getChargebackOriginalStatementDate() {
		return chargebackOriginalStatementDate;
	}
	/**
	 * @param chargebackOriginalStatementDate The chargebackOriginalStatementDate to set.
	 */
	public void setChargebackOriginalStatementDate(
			Date chargebackOriginalStatementDate) {
		this.chargebackOriginalStatementDate = chargebackOriginalStatementDate;
	}
	/**
	 * @return Returns the createDate.
	 */
	public Date getCreateDate() {
		return createDate;
	}
	/**
	 * @param createDate The createDate to set.
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * @return Returns the updateDate.
	 */
	public Date getUpdateDate() {
		return updateDate;
	}
	/**
	 * @param updateDate The updateDate to set.
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	/**
	 * @return Returns the updateUserId.
	 */
	public String getUpdateUserId() {
		return updateUserId;
	}
	/**
	 * @param updateUserId The updateUserId to set.
	 */
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
}
