package emgadm.model;

import java.math.BigDecimal;

import emgshared.exceptions.ParseDateException;
import emgshared.util.DateFormatter;

/**
 * Search criteria for ACHReturn entries
 *
 */
public class AchReturnSearchRequest {
	
		DateFormatter df1 = new DateFormatter("MM/dd/yyyy", true);
		DateFormatter df2 = new DateFormatter("dd/MMM/yyyy", true);

		private String achTranType;
		private String achTranStatusCode;
		private String achEffDateBegin;
		private String achEffDateEnd;
		private int achFileCntlSeqNbr;
		private String achIndivId;
		private BigDecimal achCreditAmt;
		private BigDecimal achDebitAmt;
		private String achRetReasDesc;


	public BigDecimal getAchCreditAmt() {
		return achCreditAmt;
	}
	public void setAchCreditAmt(BigDecimal achCreditAmt) {
		this.achCreditAmt = achCreditAmt;
	}
	public BigDecimal getAchDebitAmt() {
		return achDebitAmt;
	}
	public void setAchDebitAmt(BigDecimal achDebitAmt) {
		this.achDebitAmt = achDebitAmt;
	}
	public int getAchFileCntlSeqNbr() {
		return achFileCntlSeqNbr;
	}
	public void setAchFileCntlSeqNbr(int achFileCntlSeqNbr) {
		this.achFileCntlSeqNbr = achFileCntlSeqNbr;
	}
	public String getAchIndivId() {
		return achIndivId;
	}
	public void setAchIndivId(String achIndivId) {
		this.achIndivId = achIndivId;
	}
	public String getAchRetReasDesc() {
		return achRetReasDesc;
	}
	public void setAchRetReasDesc(String achRetReasDesc) {
		this.achRetReasDesc = achRetReasDesc;
	}
	public String getAchEffDateBegin() {
		return achEffDateBegin;
	}
	public void setAchEffDateBegin(String achEffDateBegin) {
		try
		{
			this.achEffDateBegin = df1.format(df2.parse(achEffDateBegin));
		}
		catch (ParseDateException e)
		{
			this.achEffDateBegin = achEffDateBegin;
		}
	}
	public String getAchEffDateEnd() {
		return achEffDateEnd;
	}
	public void setAchEffDateEnd(String achEffDateEnd) {
		try
		{
			this.achEffDateEnd = df1.format(df2.parse(achEffDateEnd));
		}
		catch (ParseDateException e)
		{
			this.achEffDateEnd = achEffDateEnd;
		}
	}
	public String getAchTranStatusCode() {
		return achTranStatusCode;
	}
	public void setAchTranStatusCode(String achTranStatusCode) {
		this.achTranStatusCode = achTranStatusCode;
	}
	public String getAchTranType() {
		return achTranType;
	}
	public void setAchTranType(String achTranType) {
		this.achTranType = achTranType;
	}

}
