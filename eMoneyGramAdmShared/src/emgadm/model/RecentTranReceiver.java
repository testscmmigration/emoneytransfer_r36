package emgadm.model;

import java.util.ArrayList;
import java.util.List;

public class RecentTranReceiver implements Comparable
{
	private String frstName;
	private String lastName;
	private List tranList = new ArrayList();

	public RecentTranReceiver(String frstName, String lastName, List list)
	{
		this.frstName = frstName;
		this.lastName = lastName;
		this.tranList = list;
	}
	
	public String getFrstName()
	{
		return frstName;
	}

	public void setFrstName(String string)
	{
		frstName = string;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String string)
	{
		lastName = string;
	}

	public String getName()
	{
		return frstName + " " + lastName;
	}

	public List getTranList()
	{
		return tranList;
	}

	public void setTranList(List list)
	{
		tranList = list;
	}

	public int getTranCnt()
	{
		return tranList.size();
	}

	public int compareTo(Object obj)
	{
		RecentTranReceiver rtr = (RecentTranReceiver) obj;
		return this.getTranCnt() - rtr.getTranCnt();
	}
}
