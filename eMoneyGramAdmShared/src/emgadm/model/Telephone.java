package emgadm.model;


public class Telephone extends Phone {

	private String _phoneNumber = null;

	public Telephone(){
	}

	protected static String stripNonDigits(String phoneNumber){
		if(phoneNumber==null){
			return null;
		}
		StringBuilder pureNumber = new StringBuilder();
		char formatedNumber[] = phoneNumber.toCharArray();
		for (int i = 0; i < formatedNumber.length; i++) {
			if (Character.isDigit(formatedNumber[i])) {
				pureNumber.append(formatedNumber[i]);
			}
		}
		return pureNumber.toString();		
	}

	public Telephone(String phoneNumber) {
		setPhoneNumber(phoneNumber);
	}
	
	public String getPhoneNumber(){
		return _phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber){
		if(phoneNumber==null){
			_phoneNumber = phoneNumber;
		}else{
			_phoneNumber = stripNonDigits(phoneNumber);
		}
	}
	
	public String getUSFormattedPhoneNumber(){
		StringBuilder formattedPhoneNumber = new StringBuilder();
		if(_phoneNumber==null){
			return ""; //$NON-NLS-1$
		}
		if(_phoneNumber.equals("") || _phoneNumber.length()<10){ //$NON-NLS-1$
			return _phoneNumber;
		}
		formattedPhoneNumber.append("("); //$NON-NLS-1$
		formattedPhoneNumber.append(_phoneNumber.substring(0,3));
		formattedPhoneNumber.append(") ");		 //$NON-NLS-1$
		formattedPhoneNumber.append(_phoneNumber.substring(3,6));
		formattedPhoneNumber.append("-");				 //$NON-NLS-1$
		formattedPhoneNumber.append(_phoneNumber.substring(6,10));
		return formattedPhoneNumber.toString();		
	}

	
}
