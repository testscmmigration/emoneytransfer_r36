package emgadm.model;

import java.util.List;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class Persons {
	
	@XStreamImplicit(itemFieldName="Person")
	private List<Person> personList;

	public List<Person> getPersonList() {
		return personList;
	}

	public void setPersonList(List<Person> personList) {
		this.personList = personList;
	}
}
