package emgadm.model;

public class KBAAnswer {
    private String KBAAnswerCode;
    private String KBAAnswerDescription;
    
	public String getKBAAnswerCode() {
		return KBAAnswerCode;
	}
	public void setKBAAnswerCode(String kBAAnswerCode) {
		KBAAnswerCode = kBAAnswerCode;
	}
	public String getKBAAnswerDescription() {
		return KBAAnswerDescription;
	}
	public void setKBAAnswerDescription(String kBAAnswerDescription) {
		KBAAnswerDescription = kBAAnswerDescription;
	}


}
