package emgadm.model;

import java.util.List;
import java.util.TimeZone;

import org.apache.struts.util.LabelValueBean;

import emgshared.exceptions.DataSourceException;

public interface UserProfile
		extends Comparable<UserProfile> {
	public void setUID(String uid);
	public void setFirstName(String firstName);
	public void setLastName(String lastName);
	public void setEmailAddress(String emailAddress);
	public String getUID();
	public String getFirstName();
	public String getLastName();
	public String getEmailAddress();
	public List<Role> getRoles();
	public void setRoles(List<Role> roles);
	public TimeZone getTimeZone();
	public String getGuid();
	public void setGuid(String string);
	public void setInternalUser(boolean boolean1);
	public boolean isInternalUser();
	public boolean hasPermission(String command);
	public List<LabelValueBean> getProgramsWithPermissions() throws DataSourceException;
}
