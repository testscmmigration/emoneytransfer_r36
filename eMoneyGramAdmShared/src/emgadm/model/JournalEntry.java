package emgadm.model;

import java.io.Serializable;

import java.util.Date;

/**
 * @author Jawahar Varadhan
 */
public class JournalEntry implements Serializable, Comparable
{
	public static String sortBy = "entryDate";
	
	private int tranActionId;
	private Date entryDate;
	private String userId;
	private String senderFirstName;
	private String senderLastName;	
	private int debitAccountId;
	private String debitAccountDesc;
	private int creditAccountId;
	private String creditAccountDesc;
	private int tranId;
	private String oldTranStatCode;
	private String oldTranSubStatCode;
	private String tranStatCode;
	private String tranSubStatCode;
	private String tranReasonDescription;	
	private double postAmount;
	private String subReasonCode;
	private String tranReasTypeCode;
	private String tranSubReasDesc;

	
	public static void setSortBaseAsEntryDate(String sortByBase){
		JournalEntry.sortBy = sortByBase;
	}

	public JournalEntry()
	{}

	/**
	 * @return
	 */
	public Date getEntryDate() {
		return entryDate;
	}

	/**
	 * @return
	 */
	public String getOldTranStatCode() {
		return oldTranStatCode;
	}

	/**
	 * @return
	 */
	public String getOldTranSubStatCode() {
		return oldTranSubStatCode;
	}

	/**
	 * @return
	 */
	public String getSenderFirstName() {
		return senderFirstName;
	}

	/**
	 * @return
	 */
	public String getSenderLastName() {
		return senderLastName;
	}


	/**
	 * @return
	 */
	public String getTranStatCode() {
		return tranStatCode;
	}

	/**
	 * @return
	 */
	public String getTranSubStatCode() {
		return tranSubStatCode;
	}

	/**
	 * @return
	 */
	public String getUserId() {
		return userId;
	}


	/**
	 * @param date
	 */
	public void setEntryDate(Date date) {
		entryDate = date;
	}

	/**
	 * @param string
	 */
	public void setOldTranStatCode(String string) {
		oldTranStatCode = string;
	}

	/**
	 * @param string
	 */
	public void setOldTranSubStatCode(String string) {
		oldTranSubStatCode = string;
	}

	/**
	 * @param string
	 */
	public void setSenderFirstName(String string) {
		senderFirstName = string;
	}

	/**
	 * @param string
	 */
	public void setSenderLastName(String string) {
		senderLastName = string;
	}


	/**
	 * @param string
	 */
	public void setTranStatCode(String string) {
		tranStatCode = string;
	}

	/**
	 * @param string
	 */
	public void setTranSubStatCode(String string) {
		tranSubStatCode = string;
	}

	/**
	 * @param string
	 */
	public void setUserId(String string) {
		userId = string;
	}

	/**
	 * @return
	 */
	public int getTranId() {
		return tranId;
	}

	/**
	 * @param i
	 */
	public void setTranId(int i) {
		tranId = i;
	}

	/**
	 * @return
	 */
	public String getTranReasonDescription() {
		return tranReasonDescription;
	}

	/**
	 * @param string
	 */
	public void setTranReasonDescription(String string) {
		tranReasonDescription = string;
	}

	/**
	 * @return
	 */
	public int getTranActionId() {
		return tranActionId;
	}

	/**
	 * @param i
	 */
	public void setTranActionId(int i) {
		tranActionId = i;
	}


	/**
	 * @return
	 */
	public double getPostAmount() {
		return postAmount;
	}


	/**
	 * @param d
	 */
	public void setPostAmount(double d) {
		postAmount = d;
	}

	/**
	 * @return
	 */
	public int getCreditAccountId() {
		return creditAccountId;
	}

	/**
	 * @return
	 */
	public int getDebitAccountId() {
		return debitAccountId;
	}

	/**
	 * @param i
	 */
	public void setCreditAccountId(int i) {
		creditAccountId = i;
	}

	/**
	 * @param i
	 */
	public void setDebitAccountId(int i) {
		debitAccountId = i;
	}

	/**
	 * @return
	 */
	public String getDebitAccountDesc() {
		return debitAccountDesc == null ? "" : debitAccountDesc;
	}

	/**
	 * @param i
	 */
	public void setDebitAccountDesc(String s) {
		debitAccountDesc = s;
	}

	/**
	 * @return
	 */
	public String getCreditAccountDesc() {
		return creditAccountDesc == null ? "" : creditAccountDesc;
	}

	/**
	 * @param i
	 */
	public void setCreditAccountDesc(String s) {
		creditAccountDesc = s;
	}

	public int compareTo(Object o) {
		JournalEntry je = (JournalEntry) o;
		
		if (JournalEntry.sortBy.equalsIgnoreCase("entryDate")) {
			return (entryDate.compareTo(je.entryDate));
		}

		if (JournalEntry.sortBy.equalsIgnoreCase("userId")) {
			return (userId.compareToIgnoreCase(je.userId));
		}

		if (JournalEntry.sortBy.equalsIgnoreCase("senderFirstName")) {
			return ((senderFirstName + " " + senderLastName).compareToIgnoreCase(je.senderFirstName + " " + je.senderLastName));
		}

		if (JournalEntry.sortBy.equalsIgnoreCase("senderLastName")) {
			return (senderLastName.compareToIgnoreCase(je.senderLastName));
		}
				
		if (JournalEntry.sortBy.equalsIgnoreCase("tranId")) {
			return (tranId - je.tranId);
		}
				
		if (JournalEntry.sortBy.equalsIgnoreCase("tranReasonDescription")) {
			return (tranReasonDescription.compareToIgnoreCase(je.tranReasonDescription));
		}
		
		if (JournalEntry.sortBy.equalsIgnoreCase("postAmount")) {

			return (postAmount >= je.postAmount ? 1 : -1);
		}		
				
		return 0;
	}

    /**
     * @return Returns the subReasonCode.
     */
    public String getSubReasonCode() {
        return subReasonCode == null ? "" : subReasonCode;
    }
    /**
     * @param subReasonCode The subReasonCode to set.
     */
    public void setSubReasonCode(String src) {
        this.subReasonCode = src;
    }
    /**
     * @return Returns the tranReasTypeCode.
     */
    public String getTranReasTypeCode() {
        return tranReasTypeCode;
    }
    /**
     * @param tranReasTypeCode The tranReasTypeCode to set.
     */
    public void setTranReasTypeCode(String trtc) {
        this.tranReasTypeCode = trtc;
    }
    /**
     * @return Returns the tranSubReasDesc.
     */
    public String getTranSubReasDesc() {
        return tranSubReasDesc;
    }
    /**
     * @param tranSubReasDesc The tranSubReasDesc to set.
     */
    public void setTranSubReasDesc(String tsrd) {
        this.tranSubReasDesc = tsrd;
    }
}
