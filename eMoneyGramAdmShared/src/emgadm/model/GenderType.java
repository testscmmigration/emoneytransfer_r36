/*
 * Created on Aug 2, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

/**
 * @author T004
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class GenderType {
	private String genderCode;
	private String genderDesc;
	
	/**
	 * @return Returns the genderCode.
	 */
	public String getGenderCode() {
		return genderCode;
	}
	/**
	 * @param genderCode The genderCode to set.
	 */
	public void setGenderCode(String genderCode) {
		this.genderCode = genderCode;
	}
	/**
	 * @return Returns the genderDesc.
	 */
	public String getGenderDesc() {
		return genderDesc;
	}
	/**
	 * @param genderDesc The genderDesc to set.
	 */
	public void setGenderDesc(String genderDesc) {
		this.genderDesc = genderDesc;
	}
}
