/*
 * Created on Mar 2, 2004
 *
 */
package emgadm.model;


/**
 * @author A131
 *
 */
public class UserProfileFactory
{
	private UserProfileFactory(){}

	public static UserProfile createUserProfile(String userId)
	{
		return new UserProfileImpl(userId);
	}
	
	public static UserProfile createNewUserProfile()
	{
		return new UserProfileImpl();
	}
}
