/*
 * Created on Jul 06, 2006
 *
 */
package emgadm.model;

import java.util.Date;

public class AchReturn  implements Comparable
{
	
	public static String sortBy = "achReturnTranId";
	public static int sortSeq = 1;
	
	private int achReturnTranId;
	private int fileControlSequenceNumber;
	private String returnStatCode;
	private String returnStatDesc;
	private Date asOfDate;
	private String fileId;
	private String companyId;
	private String settlementBank;
	private String settlementAccount;
	private String returnTypeCode;
	private String returnTypeDesc;
	private double creditAmount;
	private double debitAmount;
	private Integer tranId; 
	private Integer microTranId; 
	private String individualId;
	private String individualName;
	private Date effectiveDate;
	private String descriptiveDate;
	private String receivingRDFI;
	private String accountNo;
	private String accountHolderName;
	private String bankNo;
	private String returnTraceNo;
	private String accountType;
	private String returnReasonCode;
	private String returnReasonDesc;
	private String originalTraceNo;
	private String searchResult;
	private Date lastUpdateDate;
	private String lastUpdateUserId;
	
	/**
	 * @return Returns the fileControlSequenceNumber.
	 */
	public int getFileControlSequenceNumber() {
		return fileControlSequenceNumber;
	}
	/**
	 * @param fileControlSequenceNumber The fileControlSequenceNumber to set.
	 */
	public void setFileControlSequenceNumber(int fileControlSequenceNumber) {
		this.fileControlSequenceNumber = fileControlSequenceNumber;
	}
	/**
	 * @return Returns the returnStatCode.
	 */
	public String getReturnStatCode() {
		return returnStatCode;
	}
	/**
	 * @param returnStatCode The returnStatCode to set.
	 */
	public void setReturnStatCode(String returnStatCode) {
		this.returnStatCode = returnStatCode;
	}
	/**
	 * @return Returns the achReturnTranId.
	 */
	public int getAchReturnTranId() {
		return achReturnTranId;
	}
	/**
	 * @param achReturnTranId The achReturnTranId to set.
	 */
	public void setAchReturnTranId(int achReturnTranId) {
		this.achReturnTranId = achReturnTranId;
	}
	/**
	 * @return Returns the accountNo.
	 */
	public String getAccountNo() {
		return accountNo;
	}
	/**
	 * @param accountNo The accountNo to set.
	 */
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	/**
	 * @return Returns the accountType.
	 */
	public String getAccountType() {
		return accountType;
	}
	/**
	 * @param accountType The accountType to set.
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	/**
	 * @return Returns the asOfDate.
	 */
	public Date getAsOfDate() {
		return asOfDate;
	}
	/**
	 * @param asOfDate The asOfDate to set.
	 */
	public void setAsOfDate(Date asOfDate) {
		this.asOfDate = asOfDate;
	}
	/**
	 * @return Returns the companyId.
	 */
	public String getCompanyId() {
		return companyId;
	}
	/**
	 * @param companyId The companyId to set.
	 */
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	/**
	 * @return Returns the creditAmount.
	 */
	public double getCreditAmount() {
		return creditAmount;
	}
	/**
	 * @param creditAmount The creditAmount to set.
	 */
	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}
	/**
	 * @return Returns the debitAmount.
	 */
	public double getDebitAmount() {
		return debitAmount;
	}
	/**
	 * @param debitAmount The debitAmount to set.
	 */
	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}
	/**
	 * @return Returns the descriptiveDate.
	 */
	public String getDescriptiveDate() {
		return descriptiveDate;
	}
	/**
	 * @param descriptiveDate The descriptiveDate to set.
	 */
	public void setDescriptiveDate(String descriptiveDate) {
		this.descriptiveDate = descriptiveDate;
	}
	/**
	 * @return Returns the effectiveDate.
	 */
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	/**
	 * @param effectiveDate The effectiveDate to set.
	 */
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	/**
	 * @return Returns the fileId.
	 */
	public String getFileId() {
		return fileId;
	}
	/**
	 * @param fileId The fileId to set.
	 */
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	/**
	 * @return Returns the individualName.
	 */
	public String getIndividualName() {
		return individualName;
	}
	/**
	 * @param individualName The individualName to set.
	 */
	public void setIndividualName(String individualName) {
		this.individualName = individualName;
	}
	/**
	 * @return Returns the microTranId.
	 */
	public Integer getMicroTranId() {
		return microTranId;
	}
	/**
	 * @param microTranId The microTranId to set.
	 */
	public void setMicroTranId(Integer microTranId) {
		this.microTranId = microTranId;
	}
	/**
	 * @return Returns the originalTraceNo.
	 */
	public String getOriginalTraceNo() {
		return originalTraceNo;
	}
	/**
	 * @param originalTraceNo The originalTraceNo to set.
	 */
	public void setOriginalTraceNo(String originalTraceNo) {
		this.originalTraceNo = originalTraceNo;
	}
	/**
	 * @return Returns the receivingRDFI.
	 */
	public String getReceivingRDFI() {
		return receivingRDFI;
	}
	/**
	 * @param receivingRDFI The receivingRDFI to set.
	 */
	public void setReceivingRDFI(String receivingRDFI) {
		this.receivingRDFI = receivingRDFI;
	}
	/**
	 * @return Returns the returnReasonCode.
	 */
	public String getReturnReasonCode() {
		return returnReasonCode;
	}
	/**
	 * @param returnReasonCode The returnReasonCode to set.
	 */
	public void setReturnReasonCode(String returnReasonCode) {
		this.returnReasonCode = returnReasonCode;
	}
	/**
	 * @return Returns the returnReasonDesc.
	 */
	public String getReturnReasonDesc() {
		return returnReasonDesc;
	}
	/**
	 * @param returnReasonDesc The returnReasonDesc to set.
	 */
	public void setReturnReasonDesc(String returnReasonDesc) {
		this.returnReasonDesc = returnReasonDesc;
	}
	/**
	 * @return Returns the returnTraceNo.
	 */
	public String getReturnTraceNo() {
		return returnTraceNo;
	}
	/**
	 * @param returnTraceNo The returnTraceNo to set.
	 */
	public void setReturnTraceNo(String returnTraceNo) {
		this.returnTraceNo = returnTraceNo;
	}
	/**
	 * @return Returns the returnTypeCode.
	 */
	public String getReturnTypeCode() {
		return returnTypeCode;
	}
	/**
	 * @param returnTypeCode The returnTypeCode to set.
	 */
	public void setReturnTypeCode(String returnTypeCode) {
		this.returnTypeCode = returnTypeCode;
	}
	/**
	 * @return Returns the returnTypeDesc.
	 */
	public String getReturnTypeDesc() {
		return returnTypeDesc;
	}
	/**
	 * @param returnTypeDesc The returnTypeDesc to set.
	 */
	public void setReturnTypeDesc(String returnTypeDesc) {
		this.returnTypeDesc = returnTypeDesc;
	}
	/**
	 * @return Returns the searchResult.
	 */
	public String getSearchResult() {
		return searchResult;
	}
	/**
	 * @param searchResult The searchResult to set.
	 */
	public void setSearchResult(String searchResult) {
		this.searchResult = searchResult;
	}
	/**
	 * @return Returns the settlementAccount.
	 */
	public String getSettlementAccount() {
		return settlementAccount;
	}
	/**
	 * @param settlementAccount The settlementAccount to set.
	 */
	public void setSettlementAccount(String settlementAccount) {
		this.settlementAccount = settlementAccount;
	}
	/**
	 * @return Returns the settlementBank.
	 */
	public String getSettlementBank() {
		return settlementBank;
	}
	/**
	 * @param settlementBank The settlementBank to set.
	 */
	public void setSettlementBank(String settlementBank) {
		this.settlementBank = settlementBank;
	}
	/**
	 * @return Returns the tranId.
	 */
	public Integer getTranId() {
		return tranId;
	}
	/**
	 * @param tranId The tranId to set.
	 */
	public void setTranId(Integer tranId) {
		this.tranId = tranId;
	}
	
	/**
	 * @return Returns the returnStatDesc.
	 */
	public String getReturnStatDesc() {
		return returnStatDesc;
	}
	/**
	 * @param returnStatDesc The returnStatDesc to set.
	 */
	public void setReturnStatDesc(String returnStatDesc) {
		this.returnStatDesc = returnStatDesc;
	}

	/**
	 * @return Returns the lastUpdateDate.
	 */
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	/**
	 * @param lastUpdateDate The lastUpdateDate to set.
	 */
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	/**
	 * @return Returns the lastUpdateUserId.
	 */
	public String getLastUpdateUserId() {
		return lastUpdateUserId;
	}
	/**
	 * @param lastUpdateUserId The lastUpdateUserId to set.
	 */
	public void setLastUpdateUserId(String lastUpdateUserId) {
		this.lastUpdateUserId = lastUpdateUserId;
	}
	/**
	 * @return Returns the accountHolderName.
	 */
	public String getAccountHolderName() {
		return accountHolderName;
	}
	/**
	 * @param accountHolderName The accountHolderName to set.
	 */
	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}
	/**
	 * @return Returns the bankNo.
	 */
	public String getBankNo() {
		return bankNo;
	}
	/**
	 * @param bankNo The bankNo to set.
	 */
	public void setBankNo(String bankNo) {
		this.bankNo = bankNo;
	}
	
	public int compareTo(Object o)
	{
		AchReturn achReturn = (AchReturn) o;

		if (AchReturn.sortBy == null)
		{
			AchReturn.sortBy = "achReturnTranId";
		}

	
		if (AchReturn.sortBy.equalsIgnoreCase("tranId"))
		{
			if (tranId == null)
				return -1;
			
			if (achReturn.tranId == null)
				return 1;
			
			return (tranId.intValue() - achReturn.tranId.intValue()) * sortSeq;
		}	
		
		if (AchReturn.sortBy.equalsIgnoreCase("microTranId"))
		{
			if (microTranId == null)
				return -1;
			
			if (achReturn.microTranId == null)
				return 1;
			
			return (microTranId.intValue() - achReturn.microTranId.intValue()) * sortSeq;
		}		
		
		if (AchReturn.sortBy.equalsIgnoreCase("creditAmount"))
		{
			return ((int)creditAmount - (int)achReturn.creditAmount) * sortSeq;
		}			
		
		if (AchReturn.sortBy.equalsIgnoreCase("debitAmount"))
		{
			return ((int)debitAmount - (int)achReturn.debitAmount) * sortSeq;
		}			

		
		if (AchReturn.sortBy.equalsIgnoreCase("returnStatDesc"))
		{
			return (returnStatDesc.compareToIgnoreCase(achReturn.returnStatDesc)) * sortSeq;
		}

		if (AchReturn.sortBy.equalsIgnoreCase("returnTypeDesc"))
		{
			return (returnTypeDesc.compareToIgnoreCase(achReturn.returnTypeDesc)) * sortSeq;
		}

		if (AchReturn.sortBy.equalsIgnoreCase("returnReasonDesc"))
		{
			return (returnReasonDesc.compareToIgnoreCase(achReturn.returnReasonDesc)) * sortSeq;
		}
		

		if (AchReturn.sortBy.equalsIgnoreCase("effectiveDate"))
		{
			return (effectiveDate.compareTo(achReturn.effectiveDate))
				* sortSeq;
		}
		

		if (AchReturn.sortBy.equalsIgnoreCase("individualId"))
		{
			return (individualId.compareToIgnoreCase(achReturn.individualId))
				* sortSeq;
		}


		return 0;
	}		
	
	/**
	 * @return Returns the individualId.
	 */
	public String getIndividualId() {
		return individualId;
	}
	/**
	 * @param individualId The individualId to set.
	 */
	public void setIndividualId(String individualId) {
		this.individualId = individualId;
	}
}
