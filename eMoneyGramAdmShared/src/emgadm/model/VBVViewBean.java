package emgadm.model;

public class VBVViewBean
{
	private int tranId;
	private String csTranId;
	private String authCmrcCode;
	private String eciRawCode;
	private String vbvTranId;
	private String proofXmlText;
	private String csRqstId;
	private String createDate;

	public int getTranId()
	{
		return tranId;
	}

	public void setTranId(int i)
	{
		tranId = i;
	}

	public String getCsTranId()
	{
		return csTranId;
	}

	public void setCsTranId(String string)
	{
		csTranId = string;
	}

	public String getAuthCmrcCode()
	{
		return authCmrcCode;
	}

	public void setAuthCmrcCode(String string)
	{
		authCmrcCode = string;
	}

	public String getEciRawCode()
	{
		return eciRawCode;
	}

	public void setEciRawCode(String string)
	{
		eciRawCode = string;
	}

	public String getVbvTranId()
	{
		return vbvTranId;
	}

	public void setVbvTranId(String string)
	{
		vbvTranId = string;
	}

	public String getProofXmlText()
	{
		return proofXmlText;
	}

	public void setProofXmlText(String string)
	{
		proofXmlText = string;
	}

	public String getCsRqstId()
	{
		return csRqstId;
	}

	public void setCsRqstId(String string)
	{
		csRqstId = string;
	}

	public String getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(String string)
	{
		createDate = string;
	}
}
