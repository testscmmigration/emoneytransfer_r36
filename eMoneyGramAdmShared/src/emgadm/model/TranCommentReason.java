/*
 * Created on Feb 3, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

/**
 * @author T008
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TranCommentReason {
	private String emgTranCmntReasCode;
	private String cmntReasDesc;
	
	/**
	 * @return
	 */
	public String getEmgTranCmntReasCode() {
		return emgTranCmntReasCode == null ? "" : emgTranCmntReasCode;
	}

	/**
	 * @param string
	 */
	public void setEmgTranCmntReasCode(String string) {
		emgTranCmntReasCode = string;
	}

	/**
	 * @return
	 */
	public String getCmntReasDesc() {
		return cmntReasDesc == null ? "" : cmntReasDesc;
	}

	/**
	 * @param string
	 */
	public void setCmntReasDesc(String string) {
		cmntReasDesc = string;
	}

}
