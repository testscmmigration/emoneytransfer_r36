package emgadm.model;

import java.util.Date;

public class DeadTransaction {
	private int tranId;
	private int custId;
	private String emailAddr;
	private Date tranDate;
	// MBO-450 changes starts
	private long applicationVersionNumber;

	// MBO-450 changes ends

	public int getTranId() {
		return tranId;
	}

	public void setTranId(int i) {
		tranId = i;
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int i) {
		custId = i;
	}

	public String getEmailAddr() {
		return emailAddr;
	}

	public void setEmailAddr(String string) {
		emailAddr = string;
	}

	public Date getTranDate() {
		return tranDate;
	}

	public void setTranDate(Date date) {
		tranDate = date;
	}

	// MBO-450 changes starts

	public long getApplicationVersionNumber() {
		return applicationVersionNumber;
	}

	public void setApplicationVersionNumber(long applicationVersionNumber) {
		this.applicationVersionNumber = applicationVersionNumber;
	}

	// MBO-450 changes ends
}
