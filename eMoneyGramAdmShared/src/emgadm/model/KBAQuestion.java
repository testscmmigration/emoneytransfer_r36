package emgadm.model;

import java.util.ArrayList;
import java.util.List;

public class KBAQuestion {
	
	 private static final long serialVersionUID = 1L;
	    private String KBAQuestionCode;
	    private String KBAQuestionDescription;
	    private List<KBAAnswer> kbaAnswers = new ArrayList<KBAAnswer>();
	    private String KBAAnswerCode;
		public String getKBAQuestionCode() {
			return KBAQuestionCode;
		}
		public void setKBAQuestionCode(String kBAQuestionCode) {
			KBAQuestionCode = kBAQuestionCode;
		}
		public String getKBAQuestionDescription() {
			return KBAQuestionDescription;
		}
		public void setKBAQuestionDescription(String kBAQuestionDescription) {
			KBAQuestionDescription = kBAQuestionDescription;
		}
		public List<KBAAnswer> getKbaAnswers() {
			return kbaAnswers;
		}
		public void setKbaAnswers(List<KBAAnswer> kbaAnswers) {
			this.kbaAnswers = kbaAnswers;
		}
		public String getKBAAnswerCode() {
			return KBAAnswerCode;
		}
		public void setKBAAnswerCode(String kBAAnswerCode) {
			KBAAnswerCode = kBAAnswerCode;
		}
	    


}
