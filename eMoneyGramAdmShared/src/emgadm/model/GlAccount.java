/*
 * Created on May 11, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

public class GlAccount {

	private int glAccountId;
	private String glAccountDesc;

	public int getGlAccountId() {
		return glAccountId;
	}

	public void setGlAccountId(int i) {
		glAccountId = i;
	}

	public String getGlAccountDesc() {
		return glAccountDesc == null ? "" : glAccountDesc;
	}

	public void setGlAccountDesc(String string) {
		glAccountDesc = string;
	}
}

