/*
 * Created on Mar 1, 2004
 *
 */
package emgadm.model;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import emgshared.exceptions.DataSourceException;

/**
 * @author A131
 *
 */
/**
 * @author A131
 *
 */
public interface BillerMainOffice
{
	/**
	 * @return
	 */
	public abstract String getGuid();
	/**
	 * @return
	 */
	public abstract BusinessWeekType getBusinessWeek();
	/**
	 * @return
	 */
	public abstract Date getEndOfDayTime();
	/**
	 * @return
	 */
	public abstract TimeZone getTimeZone();
	/**
	 * @return
	 */
	public abstract int getMaximumLoginInactivity();
	/**
	 * @return
	 */
	public abstract int getMinimumAlphaCharsInPassword();
	/**
	 * @return
	 */
	public abstract int getMinimumNumericCharsInPassword();
	/**
	 * @return
	 */
	public abstract int getMinimumPasswordLength();
	/**
	 * @return
	 */
	public abstract int getPasswordResetDuration();
	/**
	 * @return
	 */
	public abstract BillerHierarchy getBillerHierarchy();
	/**
	 * @return
	 */
	public abstract String getName();
	/**
	 * @return
	 */
	public abstract BusinessAdminSettings getBusinessAdminSettings();
	/**
	 * @return
	 */
	public abstract SecurityStandards getSecurityStandards();
	/**
	 * Creates a BillerMainOffice object for the given identifier
	 * by copying the settings from this object.
	 * 
	 * @param newGuid	The desired guid
	 * @return	BillerMainOffice
	 */
	public abstract BillerMainOffice mimic(String newGuid)
		throws DataSourceException;

	public abstract boolean isPasswordValid(String password);

	public abstract boolean isUserActive(Calendar lastActivityDate);

	public abstract boolean isPasswordExpired(Calendar lastChangeDate);

	/**
	 * This method returns the start of the business day which 
	 * includes the given referencePointInMillis.
	 * 
	 * @param referencePointInMillis
	 * @return	Time in milliseconds 
	 */

	public abstract long getEndOfBusinessDayInMillis(long referencePointInMillis);

	/**
	 * This method returns the end of the business day which 
	 * includes the given referencePointInMillis.
	 * 
	 * @param referencePointInMillis
	 * @return	Time in milliseconds 
	 */
	public abstract long getStartOfBusinessDayInMillis(long referencePointInMillis);
	public abstract long getPreviousBusinessDayInMillis(long referencePointInMillis);
}