/*
 * Created on Mar 22, 2004
 *
 */
package emgadm.model;

import java.util.Calendar;

import emgshared.util.I18NHelper;

/**
 * @author A131
 *
 */
class BusinessWeekTypeMonSun extends BusinessWeekType
{
	BusinessWeekTypeMonSun()
	{
		super(MON_SUN_TAG);
	}

	public boolean isBusinessDay(Calendar cal)
	{
		if (cal == null)
		{
			String errorMessage = I18NHelper.getFormattedMessage("exception.required.param", new Object[] { "cal" }); //$NON-NLS-1$ //$NON-NLS-2$
			throw new NullPointerException(errorMessage);
		}
		return (true);
	}
}
