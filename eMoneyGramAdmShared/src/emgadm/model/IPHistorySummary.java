/*
 * Created on May 25, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

/**
 * @author T008
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class IPHistorySummary {
	private String ipAddress;
	private String ipAddressTaintText;
	private int loginCount;
	private String firstLoginDate;
	private String lastLoginDate;
	private String country;
	private String state;
	private String city;
	private String ISP; 
	private String zipCode;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getISP() {
		return ISP;
	}

	public void setISP(String isp) {
		ISP = isp;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	/**
	 * @return
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param string
	 */
	public void setIpAddress(String string) {
		ipAddress = string;
	}

	/**
	 * @return
	 */
	public int getLoginCount() {
		return loginCount;
	}

	/**
	 * @param i
	 */
	public void setLoginCount(int i) {
		loginCount = i;
	}

	/**
	 * @return
	 */
	public String getFirstLoginDate() {
		return firstLoginDate;
	}

	/**
	 * @param string
	 */
	public void setFirstLoginDate(String string) {
		firstLoginDate = string;
	}

	/**
	 * @return
	 */
	public String getLastLoginDate() {
		return lastLoginDate;
	}

	/**
	 * @param string
	 */
	public void setLastLoginDate(String string) {
		lastLoginDate = string;
	}

	/**
	 * @return
	 */
	public String getIpAddressTaintText() {
		return ipAddressTaintText;
	}

	/**
	 * @param string
	 */
	public void setIpAddressTaintText(String string) {
		ipAddressTaintText = string;
	}

}
