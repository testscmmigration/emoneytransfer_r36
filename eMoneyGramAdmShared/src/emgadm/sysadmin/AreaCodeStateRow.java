/*
 * Created on Feb 7, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.sysadmin;

import java.text.DecimalFormat;

import emgadm.dataaccessors.TransactionManager;
import emgshared.util.StringHelper;

/**
 * @author Jawahar Varadhan
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AreaCodeStateRow extends UploadFileRow{
	public String areaCode;
	public String stateCode;
	
	public void validate(){
		String currentRow = this.row;
		
		try{
			if (currentRow==null || currentRow.trim().equals("")){
				setError("Empty or Null row or column");
				setValid(false);
				return;
			}else{
				if (StringHelper.split(currentRow, ",").length != 2){
					if (!StringHelper.isNullOrEmpty(getError()))
						setError(getError() + ", " + "Insufficient/Invalid Data");
					else
						setError("Insufficient/Invalid Data");					
					setValid(false);
					return;
				}else{
					ParseAndLoad(currentRow);
/*
					if (getStateCode().length() != 2){
						if (!StringHelper.isNullOrEmpty(getError())){
							setError(getError() + ", " + "Invalid State Code");
						}else{
							setError("Invalid State Code");
						}
						setValid(false);
					}
*/					
					if (getAreaCode().length() != 3){
						if (!StringHelper.isNullOrEmpty(getError())){
							setError(getError() + ", " + "Invalid Area Code");
						}else{
							setError("Invalid Area Code");
						}
						setValid(false);
					}
				}
			}
		}catch(Exception e){
			if (!StringHelper.isNullOrEmpty(getError())){
				setError(getError() + ", " + e.toString());
			}else{
				setError(e.toString());				
			}
			setValid(false);				
		}

	}
	
	/**
	 * This Method writes each row into the database. Only the validated rows will get Uploaded
	 */
	public void upload(String userId, TransactionManager tm, int fileControlNumber){
		String countryCode = "USA";
		
//		String currentRow = this.row;
		
		try{
			if (this.isValid()){
				tm.insertPhoneAreaCode(userId, countryCode, this.areaCode, this.stateCode);
			}
		}catch(Exception e){
				setError(e.getMessage());
				setValid(false);				
		}

	}	
	private void ParseAndLoad(String currentRow) throws Exception{
		DecimalFormat df = new DecimalFormat("000");		
		String [] fields = StringHelper.split(currentRow, ",");

		setAreaCode(df.format(Integer.parseInt(fields[0])));
		setStateCode(fields[1].trim());
				
		return;		
	}
	

	

	/**
	 * @return Returns the areaCode.
	 */
	public String getAreaCode() {
		return areaCode;
	}
	/**
	 * @param areaCode The areaCode to set.
	 */
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	/**
	 * @return Returns the stateCode.
	 */
	public String getStateCode() {
		return stateCode;
	}
	/**
	 * @param stateCode The stateCode to set.
	 */
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
}
