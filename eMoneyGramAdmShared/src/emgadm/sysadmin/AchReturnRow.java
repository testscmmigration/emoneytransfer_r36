/*
 * Created on Feb 7, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.sysadmin;

import java.util.Iterator;
import java.util.List;

import emgadm.dataaccessors.ManagerFactory;
import emgadm.dataaccessors.TransactionManager;
import emgadm.model.AchReturn;
import emgshared.model.MicroDeposit;
import emgshared.services.MicroDepositService;
import emgshared.services.ServiceFactory;
import emgshared.util.DateFormatter;
import emgshared.util.StringHelper;

/**
 * @author Jawahar Varadhan
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AchReturnRow extends UploadFileRow{
	public AchReturn achReturn = new AchReturn();
	
	
	public void validate(){
		String currentRow = this.row;

		//Pre-process the row to remove comma within the double quotes
		StringBuilder buf = new StringBuilder();
		String [] quoteArray = StringHelper.split(currentRow, "\"");
		
		for (int i=1; i<quoteArray.length; i=i+2){
			quoteArray[i] = StringHelper.replaceAll(quoteArray[i], ",", "");			
		}
		
		for (int i=0; i<quoteArray.length; i++){
			buf.append(quoteArray[i]);
		}
		
		currentRow = buf.toString();
		this.row = currentRow;
		
		try{
			if (currentRow==null || currentRow.trim().equals("")){
				setError("Empty or Null row or column");
				setValid(false);
				return;
			}else{
				if (StringHelper.split(currentRow, ",").length != 24){
					setError("Invalid Data/Column");
					setValid(false);
					return;
				}else{
					ParseAndLoad(currentRow);
				}
			}
		}catch(Exception e){
				setError(e.toString());
				setValid(false);				
		}

	}
	
	/**
	 * This Method writes each row into the database. Only the validated rows will get Uploaded
	 */
	public void upload(String userId, TransactionManager tm, int fileControlSeqNumber){
//		String currentRow = this.row;
		try{
			if (this.isValid()){
				tm.insertAchReturn(userId, fileControlSeqNumber, achReturn);
			}
		}catch(Exception e){
				setError(e.getMessage());
				setValid(false);				
		}		
		
	}
	
	private void ParseAndLoad(String currentRow) throws Exception{
		ServiceFactory sf = ServiceFactory.getInstance();
		MicroDepositService ms = sf.getMicroDepositService();
		
		String [] fields = StringHelper.split(currentRow, ",");
		// Check if the date format needs to be: dd/MMM/yyyy
		// Would depend on the returned string format
		DateFormatter df = new DateFormatter("mm/dd/yyyy", true);
		
		achReturn.setAsOfDate(df.parse(fields[0]));
		achReturn.setFileId(fields[1]);
		achReturn.setCompanyId(fields[2]);
		achReturn.setSettlementBank(fields[4]);
		achReturn.setSettlementAccount(fields[5]);
		achReturn.setReturnTypeCode(fields[6]);
		achReturn.setReturnTypeDesc(fields[7]);
		achReturn.setCreditAmount(Double.parseDouble(fields[9]));
		achReturn.setDebitAmount(Double.parseDouble(fields[10]));
		achReturn.setIndividualId(fields[11]);

		//if this record has a "NOC" return type_desc (return_type_code == 40) then do not store 
		//anything in tranId or microTranId
		if (!achReturn.getReturnTypeCode().equals("40")){
			if (achReturn.getCreditAmount()==0.0){
				achReturn.setTranId(Integer.valueOf(fields[11].substring(0, fields[11].length()-5)));
			}else{
				achReturn.setMicroTranId(Integer.valueOf(fields[11].substring(0, fields[11].length()-5)));
			}
		}

		//Check to see if this Transaction exists in the database
		if (achReturn.getTranId() != null){
			if (ManagerFactory.createTransactionManager().getTransaction(achReturn.getTranId().intValue()) == null)
				error = "Transaction does not exist in the system";
		}
		
		
		//Check to see if this Micro Deposit Transaction exists in the database
		if (achReturn.getMicroTranId() != null){
			List mdTrans = ms.getMDTrans(null, achReturn.getMicroTranId(), null, null);
			Integer vldnId = null;
			
			for (Iterator iter = mdTrans.iterator(); iter.hasNext();){
				MicroDeposit micro = (MicroDeposit) iter.next();
				vldnId = Integer.valueOf(micro.getValidationId());
			}
			
			if (vldnId == null){
				error = "MD-Transaction does not exist in the system";
			}else{
				achReturn.setMicroTranId(vldnId);
			}
		}
		
		achReturn.setIndividualName(fields[12]);
		achReturn.setEffectiveDate(df.parse(fields[13]));
		achReturn.setDescriptiveDate(fields[14]);
		achReturn.setReceivingRDFI(fields[15]);
		achReturn.setAccountNo(fields[16]);
		achReturn.setReturnTraceNo(fields[18]);
		achReturn.setAccountType(fields[19]);
		achReturn.setReturnReasonCode(fields[20]);
		achReturn.setReturnReasonDesc(fields[21]);
		achReturn.setOriginalTraceNo(fields[22]);
		achReturn.setSearchResult(fields[23]);		
		
		return;		
	}
	
	/**
	 * @return Returns the achReturn.
	 */
	public AchReturn getAchReturn() {
		return achReturn;
	}
	/**
	 * @param achReturn The achReturn to set.
	 */
	public void setAchReturn(AchReturn achReturn) {
		this.achReturn = achReturn;
	}
}
	

