package emgadm.constants;

public class EMoneyGramAdmButtonCommands {

	// 1
	public static final String APPROVE_COMMAND = "approve";

	// 2
	public static final String DENY_COMMAND = "deny";

	// 3
	public static final String UNDO_APPROVE_COMMAND = "undoApprove";

	// 4
	public static final String UNDO_DENY_COMMAND = "undoDeny";

	// 5
	public static final String MOVE_TO_PENDING_COMMAND = "moveToPending";

	// 6
	public static final String PROCESS_COMMAND = "process";

	// 11, 12, 13
	public static final String RESUBMIT_ACH_COMMAND = "resubmitAch";

	// 21, 22, 23
	public static final String CHARGE_BACKUP_CREDIT_CARD_COMMAND =
		"chargeBackupCreditCard";

	// 30
	public static final String REC_ACH_AUTO_REF_COMMAND = "recAchAutoRef";

	// 31, 32
	public static final String REC_ACH_CXL_REF_COMMAND = "recAchCxlRef";

	// 38
	public static final String REC_ACH_REFUND_REQ_COMMAND = "recAchRefundReq";

	// 40
	public static final String REC_CC_AUTO_REF_COMMAND = "recCcAutoRef";

	// 41, 42
	public static final String REC_CC_CXL_REF_COMMAND = "recCcCxlRef";

	// 43
	public static final String RECORD_ACH_CHARGE_BACK_COMMAND =
		"recordAchChargeback";

	// 44
	public static final String RECORD_CC_CHARGE_BACK_COMMAND =
		"recordCcChargeback";

	// 48
	public static final String REC_CC_REFUND_REQ_COMMAND = "recCcRefundReq";

	// 51
	public static final String MAN_WRITE_OFF_LOSS_COMMAND = "manWriteOffLoss";

	// 52
	public static final String MAN_REFUND_FEE_COMMAND = "manRefundFee";

	// 53
	public static final String MAN_COLLECT_FEE_COMMAND = "manCollectFee";

	// 54
	public static final String MAN_RETURN_FUNDING_COMMAND = "manReturnFunding";

	// 55
	public static final String AUTO_WRITE_OFF_LOSS_COMMAND = "autoWriteOffLoss";

	// 56
	public static final String AUTO_CHARGE_BACK_AND_WRITE_OFF_LOSS_COMMAND =
		"autoChargebackAndWriteOffLoss";

	// 61
	public static final String RECORD_LOSS_ADJUSTMENT_COMMAND =
		"recordLossAdjustment";

	// 62
	public static final String RECORD_FEE_ADJUSTMENT_COMMAND =
		"recordFeeAdjustment";

	// 63
	public static final String RECORD_CONSUMER_REFUND_OR_REMITTANCE_ADJUSTMENT_COMMAND =
		"recordConsumerRefundOrRemittanceAdjustment";

	// 64
	public static final String RECORD_MG_ACTIVITY_ADJUSTMENT_COMMAND =
		"recordMgActivityAdjustment";

	// 71
	public static final String RECORD_MG_CANCEL_REFUND_COMMAND =
		"recordMgCancelRefund";

	// 81
	public static final String CANCEL_ACH_WAITING_COMMAND = "cancelAchWaiting";

	// 82
	public static final String RECOVERY_OF_LOSS_COMMAND = "recoveryOfLoss";

	// 84
	public static final String ES_MG_SEND_COMMAND = "esMGSend";

	// 85
	public static final String ES_MG_SEND_CANCEL_COMMAND = "esMGSendCancel";

	// 86
	public static final String RECORD_TRAN_ERROR_COMMAND = "recordTranError";

	// 87
	public static final String RELEASE_TRANSACTION_COMMAND = "releaseTransaction";

	// 88
	public static final String GEN_TRAN_SCORE_COMMAND = "genTranScore";

	// 89
	public static final String APPROVE_PROCESS_COMMAND = "approveProcess";

	//	160
	public static final String RETRIEVAL_REQUEST_COMMAND = "retrievalRequest";

	//	161
	public static final String CANCEL_BANK_RQST_REFUND_COMMAND = "cancelBankRqstRefund";
	//	162
	public static final String BANK_REQUEST_REFUND_COMMAND = "bankRqstRefund";
	//	163
	public static final String AUTO_CANCEL_BANK_RQST_REFUND_COMMAND = "autoCancelBankRqstRefund";
	//	164
	public static final String BANK_REPRESENT_REJ_DEBIT_COMMAND = "bankRepresentRejDebit";
	//	165
	public static final String BANK_REJECTED_DEBIT_COMMAND = "bankRejectedDebit";
	//	166
	public static final String BANK_RECORD_VOID_COMMAND = "bankRecordVoid";
	//	167
	public static final String BANK_ISSUE_REFUND_COMMAND = "bankIssueRefund";
	//	168
	public static final String BANK_WRITE_OFF_LOSS_COMMAND = "bankWriteOffLoss";
	//	169
	public static final String BANK_RECOVER_LOSS_COMMAND = "bankRecoverLoss";

	//	170
	public static final String CREDIT_CARD_PARTIAL_REFUND = "ccPartialRefund";
	
	//  171
	public static final String UNDO_DS_ERROR = "undoDSError";

	// 172
	public static final String CREDIT_CARD_MANUAL_REFUND = "ccManualRefund";
	// 173
	public static final String CREDIT_CARD_MANUAL_VOID = "ccManualVoid";   
	 //  174
	public static final String REGENERATE_EMAIL = "regenerateEmail";
	//  175
    public static final String AUTO_EXCEPTION_WOL_COMMAND = "autoExceptionWOL";
	//  176
	public static final String MANUAL_EXCEPTION_WOL_COMMAND = "manualExceptionWOL";
	//177
	public static final String CANCEL_TRANSACTION_COMMAND = "cancelTransaction";
	//178
	public static final String MANUAL_CANCEL_TRANSACTION_COMMAND = "manualCancelTransaction";
	//179
	public static final String MOVE_TO_REVIEW_COMMAND = "moveToReview";
	  

}
