package emgadm.constants;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author A113
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class EMoneyGramAdmApplicationConstants {

	public static final String EMONEYGRAM		    = "eMoneyGramAdmin";
	public static final String EMAILLINK			= "/login.do";
	public static final String EXPRESSPAYFORMFREE	= "EPFF";

	public static final String AUTOMATED_ADMIN_USER	= "System";

	public static final String EMPLOYEE_LIST_FILE_UPLOAD = "EMG_EMPLOYEE_LIST_FILE_UPLOAD";
	public static final String AREACODE_STATE_FILE_UPLOAD = "EMG_AREACODE_STATE_FILE_UPLOAD";
	public static final String ACH_RETURN_FILE_UPLOAD = "EMG_ACH_RETURN_FILE_UPLOAD";
	public static final String ACH_RETURN_CLOSED_STATUS = "CLS";
	public static final String NIF = "NIF";

	public static final String FROM_DATE_FORMAT = "yyyy-MM-dd";
	public static final String TO_DATE_FORMAT = "dd/MMM/yyyy";

	public static final String DENIAL_CODE  = "R003";
	public static final String PENDANT_CODE = "R009";

	public static final String COMMENT_CODE_OTH="OTH";
	public static final String COMMENT_CODE_AVS="AVS";
	// source table src_web_site
	public static final int MGO_PARTNER_SITE_CODE = 1;
	public static final int WAP_PARTNER_SITE_CODE = 2;
	public static final int MGOUK_PARTNER_SITE_CODE = 3;
	public static final Integer MGODE_PARTNER_SITE_CODE = 4;
	public static final int MGOESP_PARTNER_SITE_CODE = 8;
	public static final int MGOFRA_PARTNER_SITE_CODE = 9;

	public static final String MGO_PARTNER_SITE_ID = "MGO";
	public static final String MGOUK_PARTNER_SITE_ID = "MGOUK";
	public static final String MGODE_PARTNER_SITE_ID = "MGODE";
	/***
	 *  https://mgonline.atlassian.net/browse/PAN-926
	 *  Enable GB group display for France and Spain
	 */
	public static final String MGOESP_PARTNER_SITE_ID = "MGOESP";
	public static final String MGOFRA_PARTNER_SITE_ID = "MGOFRA";

	public static final String WAP_PARTNER_SITE_ID = "WAP";
	public static final String ALL_PARTNER_SITE_ID = "ALL";

	public static final String CURRENCY_US = "USD";
	public static final String CURRENCY_GB = "GBP";

	public static final int GL_ACCOUNT_CODE_FEE_REVENUE = 3;
	public static final int GL_ACCOUNT_CODE_AGENT_AR_AP = 5;
	public static final int GL_ACCOUNT_CODE_LOSS_RESERVE = 4;
	public static final int GL_ACCOUNT_CODE_GOODWILL = 11;

	public static final String PASSPORT_ID = "PAS";
	public static final String DRIVER_LICENSE_ID = "DRV";
	
	public static final int PYMT_PROVIDER_CODE_GLOBAL_COLLECT = 2;

	public static final String APPROVED_DOC_STATUS = "APP";
	public static final String PENDING_DOC_STATUS = "PEN";
	public static final String DENIED_DOC_STATUS = "DEN";
	public static final String SUBMITTED_DOC_STATUS = "SUB";
	public static final String REQUESTED_DOC_STATUS = "REQ";
	public static final String USER_SEARCH_PRIVILEGE = "internalUserSearch";
//	public static final String VIEW_GBGROUP_LOG = "ViewGBGroupLogDetails";
	
	public static Map<Integer, String> partnerSiteCodeToId;
	public static Map<String, Integer> partnerSiteIdToCode;
	public static Map<Integer, String> glAccountCodeToAction;
	public static Map<String, String> additionalIdMap;
	public static Map<String, String> docStatusMap;
	
	//Increase Send Limits
	public static final String photoIdTypeDriverLicense="DRV"; 
	public static final String photoIdTypePassport="PAS"; 
	public static final String photoIdTypeStateId="STA"; 
	public static final String photoIdTypeGovernmentId="GOV"; 
	public static final String photoIdTypeAlienId="ALN"; 
	public static final String photoIdState="Send Cust ID Issuer";
	
	public static Map<String,String> photoIdTypeToString;
	public static Map<String,String> photoIdStringToIdType;
	
	public static final String MAESTRO="Maestro";
	public static final int COMMENT_DB_COLUMN_MAX_SIZE = 255;
	public static final String TRANS_SEND_COUNTRY_US = "USA";
	public static final String TRANS_SEND_COUNTRY_UK = "GBR";
	public static final String TRANS_SEND_COUNTRY_DE = "DEU";
	

	// Should come from the DB
	static {

		Map<Integer, String> tempCodeToId = new HashMap<Integer, String>();
		tempCodeToId.put(MGO_PARTNER_SITE_CODE, MGO_PARTNER_SITE_ID);
		tempCodeToId.put(WAP_PARTNER_SITE_CODE, WAP_PARTNER_SITE_ID);
		tempCodeToId.put(MGOUK_PARTNER_SITE_CODE, MGOUK_PARTNER_SITE_ID);
		tempCodeToId.put(MGODE_PARTNER_SITE_CODE, MGODE_PARTNER_SITE_ID);
		partnerSiteCodeToId = Collections.unmodifiableMap(tempCodeToId);

		Map<String, Integer> tempIdToCode = new HashMap<String, Integer>();
		tempIdToCode.put(MGO_PARTNER_SITE_ID, MGO_PARTNER_SITE_CODE);
		tempIdToCode.put(WAP_PARTNER_SITE_ID, WAP_PARTNER_SITE_CODE);
		tempIdToCode.put(MGOUK_PARTNER_SITE_ID, MGOUK_PARTNER_SITE_CODE);
		tempIdToCode.put(MGODE_PARTNER_SITE_ID, MGODE_PARTNER_SITE_CODE);
		partnerSiteIdToCode = Collections.unmodifiableMap(tempIdToCode);

		Map<Integer, String> tempCodeToAction = new HashMap<Integer, String>();
		tempCodeToAction.put(GL_ACCOUNT_CODE_FEE_REVENUE, "Refund");
		tempCodeToAction.put(GL_ACCOUNT_CODE_AGENT_AR_AP, "Fee Refund");
		tempCodeToAction.put(GL_ACCOUNT_CODE_LOSS_RESERVE, "Loss");
		tempCodeToAction.put(GL_ACCOUNT_CODE_GOODWILL, "Goodwill");
		glAccountCodeToAction = Collections.unmodifiableMap(tempCodeToAction);

		Map<String, String> tempidMap = new HashMap<String, String>();
		tempidMap.put(PASSPORT_ID,"Passport");
		tempidMap.put(DRIVER_LICENSE_ID,"UK Driver's License");
		additionalIdMap = Collections.unmodifiableMap(tempidMap);

		Map<String, String> tempdocStatusMap = new HashMap<String, String>();
		tempdocStatusMap.put(REQUESTED_DOC_STATUS,"Requested");
		tempdocStatusMap.put(APPROVED_DOC_STATUS,"Approved");
		tempdocStatusMap.put(PENDING_DOC_STATUS,"Pending");
		tempdocStatusMap.put(DENIED_DOC_STATUS,"Denied");
		tempdocStatusMap.put(SUBMITTED_DOC_STATUS,"Submitted");
		docStatusMap = Collections.unmodifiableMap(tempdocStatusMap);
		
		Map<String, String> tempPhotoIdTypeToString= new HashMap<String, String>();
		tempPhotoIdTypeToString.put(photoIdTypeDriverLicense, "Drivers License");
		tempPhotoIdTypeToString.put(photoIdTypePassport, "Passport");
		tempPhotoIdTypeToString.put(photoIdTypeStateId, "State Id");
		tempPhotoIdTypeToString.put(photoIdTypeGovernmentId, "Government Id");
		tempPhotoIdTypeToString.put(photoIdTypeAlienId, "Alien Id");
		photoIdTypeToString = Collections.unmodifiableMap(tempPhotoIdTypeToString);
		
		Map<String, String> tempPhotoStringToIdType= new HashMap<String, String>();
		tempPhotoStringToIdType.put("Drivers License", photoIdTypeDriverLicense);
		tempPhotoStringToIdType.put("Passport", photoIdTypePassport);
		tempPhotoStringToIdType.put("State Id", photoIdTypeStateId);
		tempPhotoStringToIdType.put("Government Id", photoIdTypeGovernmentId);
		tempPhotoStringToIdType.put("Alien Id", photoIdTypeAlienId);
		photoIdStringToIdType = Collections.unmodifiableMap(tempPhotoStringToIdType);
	}
}

