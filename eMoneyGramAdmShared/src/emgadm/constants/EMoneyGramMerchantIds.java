package emgadm.constants;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import emgshared.model.SelectOption;
import emgshared.property.EMTSharedContainerProperties;


public class EMoneyGramMerchantIds {

	private static Collection<SelectOption> merchantList = null;
	private static Map<String,String> merchantMap= null;
	private static final String TELECOM = "Telecom";
	private static final String MORTGAGE = "Mortgage";
	private static final String AUTO = "Auto";
	private static final String OTHER = "Other";


	public static Map<String, String> getMerchantMap() {
		if (EMoneyGramMerchantIds.merchantMap == null) {
			EMoneyGramMerchantIds.initMerchants();
		}	
		return EMoneyGramMerchantIds.merchantMap;
	}


	public static Collection<SelectOption> getMerchantList() {
		if (EMoneyGramMerchantIds.merchantList == null){
			EMoneyGramMerchantIds.initMerchants();
		}	
		return EMoneyGramMerchantIds.merchantList;
	}

	private static void initMerchants() {
		EMoneyGramMerchantIds.merchantList = new ArrayList<SelectOption>();
   		EMoneyGramMerchantIds.merchantList.add(new SelectOption(EMoneyGramMerchantIds.TELECOM,EMTSharedContainerProperties.getMerchantIdTelecom()));
   		EMoneyGramMerchantIds.merchantList.add(new SelectOption(EMoneyGramMerchantIds.MORTGAGE,EMTSharedContainerProperties.getMerchantIdMortgage()));
   		EMoneyGramMerchantIds.merchantList.add(new SelectOption(EMoneyGramMerchantIds.AUTO, EMTSharedContainerProperties.getMerchantIdAuto()));
   		EMoneyGramMerchantIds.merchantList.add(new SelectOption(EMoneyGramMerchantIds.OTHER,EMTSharedContainerProperties.getMerchantIdOther()));


		EMoneyGramMerchantIds.merchantMap = new HashMap<String,String>();
   		EMoneyGramMerchantIds.merchantMap.put(EMTSharedContainerProperties.getMerchantIdTelecom(),EMoneyGramMerchantIds.TELECOM);
   		EMoneyGramMerchantIds.merchantMap.put(EMTSharedContainerProperties.getMerchantIdMortgage(),EMoneyGramMerchantIds.MORTGAGE);
   		EMoneyGramMerchantIds.merchantMap.put(EMTSharedContainerProperties.getMerchantIdAuto(),EMoneyGramMerchantIds.AUTO);
   		EMoneyGramMerchantIds.merchantMap.put(EMTSharedContainerProperties.getMerchantIdOther(),EMoneyGramMerchantIds.OTHER);


	}
}
