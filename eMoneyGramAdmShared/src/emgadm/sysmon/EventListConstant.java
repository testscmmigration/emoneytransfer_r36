package emgadm.sysmon;

public class EventListConstant
{
	public static final String CYBERSOURCE_CREDIT_AUTH =
		"CyberSource Credit Auth";
	public static final String EP_AC_SEND = "EP AC Send";
	public static final String CYBERSOURCE_CAPTURE = "CyberSource Capture";
	public static final String GLOBAL_COLLECT_POST = "Global Collect Post";
	public static final String GC_POST = "GCPOST";
	public static final String GC_AUTHORIZATION = "GCAUTHORIZATION ";
	public static final String P2P_AC_SEND = "P2P AC Send";
	public static final String ES_AC_SEND = "ES AC Send";
	public static final String CYBERSOURCE_CREDIT_REVERSAL =
		"CyberSource Credit Reversal";
	public static final String GLOBAL_COLLECT_CREDIT_REVERSAL =
		"Global Collect Credit Reversal";
	public static final String BANK_PAYMENT_SERVICE_POST = "Bank Payment Service Post";
	public static final String BANK_PAYMENT_SERVICE_ADJUSTMENT = "Bank Payment Service Adjustment";
	public static final String AC_SEND_REVERSAL = "AC Send Reversal";
	public static final String GET_TRAN_RAW_VALUES = "Get Tran Raw Values";
	public static final String EP_PROCESS = "EP Process";
	public static final String P2P_PROCESS = "P2P Process";
	public static final String ES_PROCESS = "ES Process";
	public static final String DS_PROCESS = "DS Process";
	public static final String AD_HOC_REPORT = "Ad-hoc Report";
	public static final String CONSUMER_SEARCH = "Consumer Search";
	public static final String SHOW_CONSUMER_PROFILE = "Show Consumer Profile";
	public static final String CREDIT_CARD_AUTH = "Credit Card Auth";
	public static final String CREDIT_CARD_REVERSAL =
		"Credit Card Reversal";
	public static final String GC_REFUND = "GCREFUND";

	private static final String[] EVENT_LIST =
		{
			AC_SEND_REVERSAL,
			AD_HOC_REPORT,
			CONSUMER_SEARCH,
			CYBERSOURCE_CAPTURE,
			CYBERSOURCE_CREDIT_AUTH,
			CYBERSOURCE_CREDIT_REVERSAL,
			CREDIT_CARD_AUTH,
		    BANK_PAYMENT_SERVICE_POST,
		    BANK_PAYMENT_SERVICE_ADJUSTMENT,
			EP_AC_SEND,
			EP_PROCESS,
			ES_AC_SEND,
			ES_PROCESS,
			GET_TRAN_RAW_VALUES,
			P2P_AC_SEND,
			P2P_PROCESS,
			DS_PROCESS,
			SHOW_CONSUMER_PROFILE };

	public static String[] getEVENT_LIST()
	{
		return EVENT_LIST;
	}
}
