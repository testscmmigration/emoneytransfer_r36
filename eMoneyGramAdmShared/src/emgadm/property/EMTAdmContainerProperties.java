package emgadm.property;

import emgshared.property.PropertyLoader;

public class EMTAdmContainerProperties
{
	private static boolean secureProtocolRequired = false;
	private static String securePort = null;
	private static String ldapServer = null;
	private static String ldapPort = null;
	private static String ldapMainUserTreeContext = null;
	private static String ldapUserContext = null;
	private static String ldapUserId = null;
	private static String ldapPassword = null;
	private static String ldapApplicationContext = null;
	private static String ldapInternalRoleApplicationContext = null;
	private static String privateKeyFile = null;
	private static String oslCountryDiallingCodeUrl = null;
	private static String oslUcpCreateEndPoint = null;
	private static String tpsEndPoint = null;
    private static int AC4APIVersion = 1;
    private static int AC4ClientVersion = 1;
    private static String AC4Address = null;
    private static String AC4NamespaceURI = null;
    private static String AC4LocalPart = null;
    private static String AC4WSDDServiceName = null;
   
    private static String bankPaymentServiceUrl = null;
    private static String bankPaymentServiceTimeout = null;
    private static String MCCPartnerIdOverride = null;
    private static String loopbackServer = null;
    private static String loopbackHost = null;
	private static String creditCardPaymentServiceUrl;
    private static String creditCardPaymentServiceTimeout = null;
    private static boolean useGCForProcessing;
    private static String consumerServiceUrl = null;
    private static String configServiceUrl = null;
    private static boolean useReceiptQueue;
    private static boolean performIPLookUpFlag;
    private static int ipLookUpTimeOutOnScoring;
    private static long ipLookUpTimeOutOnLookUpIPAddress;
    private static String ipLookUpServiceURL;
    private static String ipLookUpCustomerID;
    private static boolean startBackgroundTimer;
    private static boolean startApproveBankBackgroundTimer;
    private static int nbrBackgroundTimers;
    private static int nbrProcessApprovedBackgroundTimers;
  //MBO-047 story changes starts , adding container property for new service
    private static String mgoScoringServiceUrl= null;
    private static String mgoScoringServiceTimeout= null;
    private static String merchantIdDssend = null;
    //MBO-047 story changes ends
    //MBO-1926 Added property for re-generate email
    private static String regenerateDestEmailAddress;
    //MBO - 2158 emailAge service 
    private static String emailAgeStartDate=null;
    private static String emailAgeServiceTimeout= null;
    private static String emailAgeURL = null;
    private static String emailAgeAccountSID =null;
    private static String emailAgeAuthToken = null;
    //MBO-3510 Decryption Logic Change
    private static String decryptSyncStartDate = null;
    //MBO-1920 Reprocess transactions stuck in either APP/CCA or APP/BKS status
    private static String reprocessTransactionInterval;
    
    //MBO-3183
    private static String mgoConsumerValidationServiceUrl= null;
    private static String mgoConsumerValidationServiceTimeout= null;
    
    //MBO-5464
   
    private static String sendLimit = null;
   
    //MBO-5877
    private static boolean sendSMS = false; 
    
    //MBO-6412
    private static String generalPhoneNumber=null;
    
    //MBO-6789
    private static String oslUrl=null;
        
  //MBO-10564
	private static String dbActTimeout= null;

	public static String getOslUrl() {
		return oslUrl;
	}

	public static void setOslUrl(String oslUrl) {
		EMTAdmContainerProperties.oslUrl = oslUrl;
	}

	public static String getEmailAgeURL() {
		return emailAgeURL;
	}

	public static void setEmailAgeURL(String emailAgeURL) {
		EMTAdmContainerProperties.emailAgeURL = emailAgeURL;
	}

	public static String getEmailAgeAccountSID() {
		return emailAgeAccountSID;
	}

	public static void setEmailAgeAccountSID(String emailAgeAccountSID) {
		EMTAdmContainerProperties.emailAgeAccountSID = emailAgeAccountSID;
	}

	public static String getEmailAgeAuthToken() {
		return emailAgeAuthToken;
	}

	public static void setEmailAgeAuthToken(String emailAgeAuthToken) {
		EMTAdmContainerProperties.emailAgeAuthToken = emailAgeAuthToken;
	}

	public static String getEmailAgeServiceTimeout() {
		return emailAgeServiceTimeout;
	}

	public static void setEmailAgeServiceTimeout(String emailAgeServiceTimeout) {
		EMTAdmContainerProperties.emailAgeServiceTimeout = emailAgeServiceTimeout;
	}
 	public static String getEmailAgeStartDate() {
		return emailAgeStartDate;
	}

	public static void setEmailAgeStartDate(String emailAgeStartDate) {
		EMTAdmContainerProperties.emailAgeStartDate = emailAgeStartDate;
	}
	
	public static String getDecryptSyncStartDate() {
		return decryptSyncStartDate;
	}

	public static void setDecryptSyncStartDate(String decryptSyncStartDate) {
		EMTAdmContainerProperties.decryptSyncStartDate = decryptSyncStartDate;
	}

 
 	private EMTAdmContainerProperties() {}
	 
    public static void initialize(PropertyLoader pv )
	{
        secureProtocolRequired = pv.getBoolean("SecureProtocolRequired");
        securePort = pv.getString("SecurePort");
        ldapServer = pv.getString("LDAPServer");
		ldapPort =	pv.getString("LDAPPort");
		ldapMainUserTreeContext = pv.getString("LDAPMainUserTreeContext");
		ldapUserContext = pv.getString("LDAPUserContext");
		ldapUserId = pv.getString("LDAPUserID");
		ldapPassword =	pv.getString("LDAPPwd");
		ldapApplicationContext = pv.getString("LDAPApplicationContext");
		ldapInternalRoleApplicationContext = pv.getString("LDAPInternalRoleApplicationContext");
		privateKeyFile = pv.getString("emgadmPrivateKeyFile");		
        AC4APIVersion = pv.getInteger("AC4APIVersion");
        AC4ClientVersion = pv.getInteger("AC4ClientVersion");
        AC4Address = pv.getString("AC4Address");
        AC4NamespaceURI = pv.getString("AC4NamespaceURI");
        AC4LocalPart = pv.getString("AC4LocalPart");
        AC4WSDDServiceName = pv.getString("AC4WSDDServiceName");
		loopbackHost = pv.getString("LoopbackHost");
        loopbackServer = pv.getString("LoopbackServer");
        MCCPartnerIdOverride = pv.getString("mcc_partner_id_override");
        bankPaymentServiceUrl = pv.getString("BANKPAYMENT_SERVICE_URL");
        bankPaymentServiceTimeout = pv.getString("BANKPAYMENT_SERVICE_TIMEOUT");
        creditCardPaymentServiceUrl = pv.getString("CREDITCARDPAYMENT_SERVICE_URL");
        creditCardPaymentServiceTimeout = pv.getString("CREDITCARDPAYMENT_SERVICE_TIMEOUT");
        useGCForProcessing = "true".equalsIgnoreCase(pv.getString("USE_GC_For_Processing"));
        consumerServiceUrl = pv.getString("CONSUMER_SERVICE_URL");
        configServiceUrl = pv.getString("MGO_CONFIGURATION_SERVICE_URL");
        useReceiptQueue = "true".equalsIgnoreCase(pv.getString("USE_RECEIPT_QUEUE"));
		performIPLookUpFlag = "true".equalsIgnoreCase(pv
				.getString("PERFORM_IP_LOOKUP_FLAG"));
		ipLookUpTimeOutOnScoring = pv.getInteger("IP_LOOKUP_TIMEOUT_ON_SCORING");
		ipLookUpTimeOutOnLookUpIPAddress = pv.getInteger("IP_LOOKUP_TIMEOUT_ON_LookupIPAddress");
		ipLookUpServiceURL = pv.getString("IP_LOOKUP_SERVICE_URL");
		ipLookUpCustomerID = pv.getString("IP_LOOKUP_CUSTOMER_ID");
		startBackgroundTimer = "true".equalsIgnoreCase(pv
				.getString("startBackgroundTimer"));
		startApproveBankBackgroundTimer = "true".equalsIgnoreCase(pv
				.getString("startApproveBankBackgroundTimer"));
		nbrBackgroundTimers =pv.getInteger("nbrBackgroundTimers");
		nbrProcessApprovedBackgroundTimers = pv.getInteger("nbrProcessApprovedBackgroundTimers");
		//MBO-047 story changes starts , adding container property for new service
		mgoScoringServiceUrl =pv.getString("MGOSCORING_SERVICE_URL");
		mgoScoringServiceTimeout =pv.getString("MGOSCORING_SERVICE_TIMEOUT");
		//MBO-047 story changes ends
		merchantIdDssend = pv.getString("MerchantId_DSSEND");
		//MBO-1926 Added property for re-generating email
		regenerateDestEmailAddress = pv.getString("REGENERATE_DEST_EMAIL_ADDRESS");
		oslCountryDiallingCodeUrl = pv.getString("osl_countryDiallingCodeUrl");
	//	MBO - 2158 changes
		emailAgeStartDate = pv.getString("EmailAgeStartDate");
		emailAgeServiceTimeout = pv.getString("EMAILAGE_SERVICE_TIMEOUT");
		emailAgeURL =pv.getString("EMAILAGE_URL");
	    emailAgeAccountSID =pv.getString("EMAILAGE_ACCOUNT_SID");
	    emailAgeAuthToken =pv.getString("EMAILAGE_AUTH_TOKEN");
	    
	  //MBO-1920 Reprocess transactions stuck in either APP/CCA or APP/BKS status
	    reprocessTransactionInterval = pv.getString("REPROCESS_TRANSACTION_TIME_INTERVAL");
	    //MBO-3510 Fix
	    decryptSyncStartDate = pv.getString("Decrypt_Sync_Start_Date");
		
	    //MBO-3183
	    mgoConsumerValidationServiceUrl =pv.getString("MGOCVS_SERVICE_URL");
		mgoConsumerValidationServiceTimeout =pv.getString("MGOCVS_SERVICE_TIMEOUT");
		
		//MBO-5464
		sendLimit = pv.getString("SEND_LIMIT_ALLOWED");
		
		//MBO-5877
		sendSMS = pv.getBoolean("enableSms");
		
		//MBO-6412
		generalPhoneNumber=pv.getString("generalPhoneNumber");
		
		//MBO-6789
		oslUrl=pv.getString("OSL_URL");
		oslUcpCreateEndPoint = pv.getString("OSL_ENDPOINT");
		
		tpsEndPoint = pv.getString("TPS_ENDPOINT");
		
		//MBO-10564
		dbActTimeout = pv.getString("DB_ACT_TIMEOUT");
	}

	/*********************************************************************/
	/*    Security                                                       */
	/*********************************************************************/

	public static boolean isSecureProtocolRequired()
	{
		return secureProtocolRequired;
	}

	public static String getSecurePort()
	{
		return securePort;
	}

	/*********************************************************************/
	/*    LDAP Server                                                    */
	/*********************************************************************/

	

	public static String getLDAP_SERVER()
	{
		return ldapServer;
	}

	public static String getLDAP_PORT()
	{
		return ldapPort;
	}

	public static String getLDAP_MAIN_USER_TREE_CONTEXT()
	{
		return ldapMainUserTreeContext;
	}

	public static String getLDAP_USER_CONTEXT()
	{
		return ldapUserContext;
	}

	public static String getLDAP_USER_ID()
	{
		return ldapUserId;
	}

	public static String getLDAP_PASSWORD()
	{
		return ldapPassword;
	}

	public static String getLDAP_APPLICATION_CONTEXT()
	{
		return ldapApplicationContext;
	}

	public static String getLDAP_INTERNAL_ROLE_APPLICATION_CONTEXT()
	{
		return ldapInternalRoleApplicationContext;
	}

	/*********************************************************************/
	/*    Cryptography                                                   */
	/*********************************************************************/

	public static String getPrivateKeyFile()
	{
		return privateKeyFile;
	}

	/*********************************************************************/
	/*    Mail                                                           */
	/*********************************************************************/


	/*********************************************************************/
	/*    Agent Connect                                                  */
	/*********************************************************************/

	public static int getAC4APIVersion()
	{
		return AC4APIVersion;
	}

	public static int getAC4ClientVersion()
	{
		return AC4ClientVersion;
	}

	public static String getAC4Address()
	{
		return AC4Address;
	}

	public static String getAC4NamespaceURI()
	{
		return AC4NamespaceURI;
	}

	public static String getAC4LocalPart()
	{
		return AC4LocalPart;
	}

	public static String getAC4WSDDServiceName()
	{
		return AC4WSDDServiceName;
	}

	public static String getMCCPartnerIdOverride() {
		return MCCPartnerIdOverride;
	}

	public static String getBankPaymentServiceUrl() {
		return bankPaymentServiceUrl;
	}

	public static String getBankPaymentServiceTimeout() {
		return bankPaymentServiceTimeout;
	}

    /**
     * The Server Network Name for the background processing loopback http call.
     * Should be set to your Tomcat catalina connector ( localhost:8080 ) for dev workstations,
     * and to the Apache virtual server name ( usually the IP ) for Dev,Q/A, Prod.
     * The port can be appended if neccesary, but the Apache servers currently
     * are configured to pass port 80 through. The emg security filter does the
     * ssl ( port 443 ) redirect, not Apache.
     * used for the background processing loopback http request.
     * ( see emgadm.background.BackgroundProcessMaster.java )
     * @return
     */
    public static String getLoopbackServer()
    {
        return loopbackServer;
}
     /**
     * The Virtual Server Name for the background processing loopback http call.
     * For workstations ( which don't use virtual servers ) set to "NULL", which
     * will prevent the Host header from being modified. For Dev,Q/A and Prod should
     * be whatever virtual server name is assigned ( like emgadm.moneygram.com )
     * used for the background processing loopback http request.
     * ( see emgadm.background.BackgroundProcessMaster.java )
     * @param loopbackServer
     */

    public static String getLoopbackHost()
    {
        return loopbackHost;
    }

	public static String getCreditCardPaymentServiceUrl() {
		return creditCardPaymentServiceUrl;
	}

	public static String getCreditCardPaymentServiceTimeout() {
		return creditCardPaymentServiceTimeout;
	}

	public static boolean getUseGCForProcessing() {
		return useGCForProcessing;
	}

	public static String getConsumerServiceUrl()
	{
		return consumerServiceUrl;
	}
	
	public static String getConfigServiceUrl()
	{
		return configServiceUrl;
	}


	public static boolean isUseReceiptQueue() {
		return useReceiptQueue;
	}

	public static void setUseReceiptQueue(boolean useReceiptQueue) {
		EMTAdmContainerProperties.useReceiptQueue = useReceiptQueue;
	}

	public static boolean isPerformIPLookUpFlag() {
		return performIPLookUpFlag;
	}

	public static void setPerformIPLookUpFlag(boolean performIPLookUpFlag) {
		EMTAdmContainerProperties.performIPLookUpFlag = performIPLookUpFlag;
	}

	public static int getIpLookUpTimeOutOnScoring() {
		return ipLookUpTimeOutOnScoring;
	}

	public static void setIpLookUpTimeOutOnScoring(int ipLookUpTimeOutOnScoring) {
		EMTAdmContainerProperties.ipLookUpTimeOutOnScoring = ipLookUpTimeOutOnScoring;
	}

	public static long getIpLookUpTimeOutOnLookUpIPAddress() {
		return ipLookUpTimeOutOnLookUpIPAddress;
	}

	public static void setIpLookUpTimeOutOnLookUpIPAddress(
			long ipLookUpTimeOutOnLookUpIPAddress) {
		EMTAdmContainerProperties.ipLookUpTimeOutOnLookUpIPAddress = ipLookUpTimeOutOnLookUpIPAddress;
	}

	public static String getIpLookUpServiceURL() {
		return ipLookUpServiceURL;
	}

	public static void setIpLookUpServiceURL(String ipLookUpServiceURL) {
		EMTAdmContainerProperties.ipLookUpServiceURL = ipLookUpServiceURL;
	}

	
	public static String getIpLookUpCustomerID() {
		return ipLookUpCustomerID;
	}

	public static void setIpLookUpCustomerID(String ipLookUpCustomerID) {
		EMTAdmContainerProperties.ipLookUpCustomerID = ipLookUpCustomerID;
	}

	public static boolean isStartBackgroundTimer() {
		return startBackgroundTimer;
	}

	public static void setStartBackgroundTimer(boolean startBackgroundTimer) {
		EMTAdmContainerProperties.startBackgroundTimer = startBackgroundTimer;
	}

	public static boolean isStartApproveBankBackgroundTimer() {
		return startApproveBankBackgroundTimer;
	}

	public static void setStartApproveBankBackgroundTimer(
			boolean startApproveBankBackgroundTimer) {
		EMTAdmContainerProperties.startApproveBankBackgroundTimer = startApproveBankBackgroundTimer;
	}

	
	//MGO-11451 story changes Starts
	 public static int getNbrBackgroundTimers() {
		 return nbrBackgroundTimers;
	 }

	 public static int getNbrProcessApprovedBackgroundTimers(){
          return nbrProcessApprovedBackgroundTimers;
	 }
	//MGO-11451 story changes ends
	 
		//MBO-047 story changes starts 
		 public static String getMGOScoringServiceUrl() {
				return mgoScoringServiceUrl;
		 }

			public static String getMGOScoringServiceTimeout() {
				return mgoScoringServiceTimeout;
			}
			public static String getMerchantIdDssend() {
				return merchantIdDssend;
			}
			
			// For MBO-1926
			public static String getRegenerateDestEmailAddress() {
				return regenerateDestEmailAddress;
			}

			public static void setRegenerateDestEmailAddress(
					String regenerateDestEmailAddress) {
				EMTAdmContainerProperties.regenerateDestEmailAddress = regenerateDestEmailAddress;
			}	
			
     //MBO-1920 Reprocess transactions stuck in either APP/CCA or APP/BKS status			
	 public static String getReprocessTransactionInterval() {
			return reprocessTransactionInterval;
	 }

	 public static void setReprocessTransactionInterval(String reprocessTransactionInterval) {
			EMTAdmContainerProperties.reprocessTransactionInterval = reprocessTransactionInterval;
	 }
	 
	// MBO-3183
	public static String getMGOConsumerValidationServiceUrl() {
		return mgoConsumerValidationServiceUrl;
	}

	public static String getMGOConsumerValidationServiceTimeout() {
		return mgoConsumerValidationServiceTimeout;
	}


	public static void setSendLimit(String sendLimit) {
		EMTAdmContainerProperties.sendLimit = sendLimit;
	}

	public static String getSendLimit() {
		return sendLimit;
	}

	public static boolean isSendSMS() {
		return sendSMS;
	}

	public static void setSendSMS(boolean sendSMS) {
		EMTAdmContainerProperties.sendSMS = sendSMS;
	}

	
	public static String getGeneralPhoneNumber() {
		return generalPhoneNumber;
	}

	public static void setGeneralPhoneNumber(String generalPhoneNumber) {
		EMTAdmContainerProperties.generalPhoneNumber = generalPhoneNumber;
	}
	
	public static String getTpsEndPoint() {
		return tpsEndPoint;
	}

	public static void setTpsEndPoint(String tpsEndPoint) {
		EMTAdmContainerProperties.tpsEndPoint = tpsEndPoint;
	}

	public static void setOSLURLCountryDiallingCode(String oslCountryDiallingCodeUrl){
		EMTAdmContainerProperties.oslCountryDiallingCodeUrl = oslCountryDiallingCodeUrl;
	}
	
	public static String getOSLURLCountryDiallingCode(){
		return oslCountryDiallingCodeUrl;
	}
	public static String getOslUcpCreateEndPoint(){
		return oslUcpCreateEndPoint;
	}
	
	public static String getDbActTimeout() {
		return dbActTimeout;
	}

	public static void setDbActTimeout(String dbActTimeout) {
		EMTAdmContainerProperties.dbActTimeout = dbActTimeout;
	}
	
}