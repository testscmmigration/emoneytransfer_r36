package emgadm.property;

import emgshared.property.PropertyLoader;

public class EMTAdmAppProperties {
	private static int waitTimeForTakingOver = 10;
	private static String systemUserIdPrefix = "sys-";
	private static String roleIdMsgSendTo = null;
	private static String transMonitorDefaultScore = "1000";

	private EMTAdmAppProperties() {
	}

	public static void initialize(PropertyLoader pv) {		
		waitTimeForTakingOver = pv.getInteger("WaitTimeForTakingOver");
		systemUserIdPrefix = pv.getString("SystemUserIdPrefix");
		roleIdMsgSendTo = pv.getString("RoleIdMsgSendTo");
		transMonitorDefaultScore = pv.getString("TransMonitorDefaultScore");
	}

	public static int getWaitTimeForTakingOver() {
		return waitTimeForTakingOver;
	}

	public static String getSystemUserIdPrefix() {
		return systemUserIdPrefix;
	}

	public static String getRoleIdMsgSendTo() {
		return roleIdMsgSendTo;
	}

	public static String getTransMonitorDefaultScore() {
		return transMonitorDefaultScore;
	}

	public static void setTransMonitorDefaultScore(String transMonitorDefaultScore) {
		EMTAdmAppProperties.transMonitorDefaultScore = transMonitorDefaultScore;
	}
}