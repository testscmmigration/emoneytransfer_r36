package emgadm.agentconnect;

import static org.junit.Assert.assertNotNull;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import oracle.jdbc.pool.OracleConnectionPoolDataSource;

import org.junit.Before;
import org.junit.Test;
import org.apache.log4j.Logger;

import shared.mgo.dto.MGOAgentProfile;
import shared.mgo.dto.MGOProduct;
import shared.mgo.services.AgentConnectAccessor;
import shared.mgo.services.AgentConnectService;
import shared.mgo.services.MGOServiceFactory;

import com.moneygram.agentconnect1305.wsclient.DetailLookupResponse;
import com.moneygram.agentconnect1305.wsclient.ProfileResponse;


public class AgentConnectConnectionTest {
	
	private MGOAgentProfile mgoAgentProfile = null;
	private AgentConnectAccessor agentConnectAccessor = null;
	private AgentConnectService agentConnectService = null;
	protected static Logger log = EMGSharedLogger.getLogger(AgentConnectConnectionTest.class);
	
	Properties properties = null;
	
	
	// props for agent connect 7.6
	final public static String API_VER_PROP_KEY = "AC4APIVersion";
	final public static String CLIENT_VER_PROP_KEY = "AC4ClientVersion";
	final public static String ADDRESS_PROP_KEY = "AC4Address";
	final public static String NAMESPACE_URI_PROP_KEY = "AC4NamespaceURI";
	final public static String LOCAL_PART_PROP_KEY = "AC4LocalPart";
	final public static String SERVICE_NAME_PROP_KEY = "AC4WSDDServiceName";
	final public static String RETRY_COUNT = "AC4RetryCount";
	final public static String RETRY_WAIT = "AC4RetryWaitTime";
	final public static String TIME_OUT = "A4CTimeout";
	

	@Before
	public void setUp() throws Exception {
		try {
			initializeProperties();
			initializeAgentProfile();
			//initializeDBContext();
			
			agentConnectAccessor = new AgentConnectAccessor(properties,
					mgoAgentProfile);
			
			agentConnectAccessor = new AgentConnectAccessor(properties,
					mgoAgentProfile);
			
			agentConnectService = MGOServiceFactory.getInstance().getAgentConnectService();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new AssertionError("Exception thrown");
		}
	}

	@Test
	public void getCodeTableResponseTest()throws Exception {
		com.moneygram.agentconnect1305.wsclient.CodeTableResponse response = agentConnectAccessor.getCodeTableResponse();
		assertNotNull(response);
	}
	
	/*@Test
	public void getActiveCountryListTest()throws Exception {
		ArrayList list = (ArrayList)agentConnectService.getActiveCountryList();
		assertNotNull(list);
		assertTrue("list bigger than 0", list.size()>0);
	}*/
	
	/*@Test
	public void refreshExceptionCountryTest()throws Exception {
		agentConnectService.refreshExceptionCountry();
	}*/
	
	@Test
	public void refreshCodeTablesFromAgentConnectTest()throws Exception {
		agentConnectAccessor.refreshCodeTablesFromAgentConnect();
	}
	
	/*@Test
	public void feeLookupTest()throws Exception {
		FeeLookupResponse response = agentConnectAccessor.feeLookup(new BigDecimal(100), 
				"COL", ProductType.SEND, "COP", 
				null, DeliveryOption.WILL_CALL, mgoAgentProfile.getProfileId().toString());
		assertNotNull(response);
	}*/
	
	/*@Test
	public void feeLookupTestAllOptionsTest()throws Exception {
		FeeInfo[] feeInfo = agentConnectAccessor.feeLookupAllOptions(new BigDecimal(100), 
				"COL", ProductType.SEND, mgoAgentProfile.getAgentId(), 
				ProductVariant.PREPAY);
		assertNotNull(feeInfo);
		assertTrue(feeInfo.length>0);
	}*/
	
	@Test
	public void getProfileTest()throws Exception {
		ProfileResponse response = agentConnectAccessor.getProfile();
		assertNotNull(response);
	}
	
	/*@Test
	public void moneySendTest()throws Exception {
		BillPaymentSendRequest request = new BillPaymentSendRequest();
		request.setAmount(new BigDecimal(100));
		request.setReceiveCountry("COL");
		request.setReceiveCurrency("COP");
		request.setSendCurrency("EUR");
		request.setTimeStamp(Calendar.getInstance());
		//request.setDeliveryOption(DeliveryOption.WILL_CALL);
		request.setProductVariant(ProductVariant.PREPAY);
		request.setBillerAccountNumber("123");
		
		BillPaymentSendResponse response = agentConnectAccessor.expressPaySend(request);
		assertNotNull(response);
	}*/
	
	@Test
	public void detailLookupTest()throws Exception {
		//That id is only valid for D5
		DetailLookupResponse response = agentConnectAccessor.detailLookup("34352452");
		assertNotNull("That id is only valid for D5",response);
	}
	
	private void initializeProperties(){
		properties = new Properties();
		properties.put(API_VER_PROP_KEY, "1");
		properties.put(CLIENT_VER_PROP_KEY, "1");
		properties.put(ADDRESS_PROP_KEY, "http://d5ws.qa.moneygram.com/ac2/services/AgentConnect76");
		properties.put(NAMESPACE_URI_PROP_KEY, "http://d5ws.qa.moneygram.com/ac2/services/AgentConnect76");
		properties.put(LOCAL_PART_PROP_KEY, "AgentConnectService");
		properties.put(SERVICE_NAME_PROP_KEY, "AgentConnect");
		properties.put(RETRY_COUNT, "3");
		properties.put(RETRY_WAIT, "15000");
		properties.put(TIME_OUT, "30000");
		
	}
	
	private void initializeAgentProfile(){
		MGOProduct mgoProduct = new MGOProduct();
		mgoProduct.setEnabled(true);
		mgoProduct.setProductName("MGSEND");
		
		mgoAgentProfile = new MGOAgentProfile();
		mgoAgentProfile.setAgentId("43537899");
		mgoAgentProfile.setAgentSequence("01");
		mgoAgentProfile.setMGOAgentIdentifier("MGO");
		mgoAgentProfile.setMgoAgentProfileDescription("MGO Same Day Agent");
		mgoAgentProfile.setMgoProduct(mgoProduct);
		mgoAgentProfile.setProfileId(Integer.valueOf(100416));
		mgoAgentProfile.setToken("TEST");
	}
	
	private void initializeDBContext()throws Exception{
		System.setProperty(Context.INITIAL_CONTEXT_FACTORY,"org.apache.naming.java.javaURLContextFactory");
		System.setProperty(Context.URL_PKG_PREFIXES,"org.apache.naming");
		InitialContext ic = new InitialContext();

		ic.createSubcontext("java:");
		ic.createSubcontext("java:comp");
		ic.createSubcontext("java:comp/env");
		ic.createSubcontext("java:comp/env/jdbc");

		// Construct DataSource
		OracleConnectionPoolDataSource ds = new OracleConnectionPoolDataSource();
		ds.setURL("jdbc:oracle:thin:@dmnalx0490:1521:EMGD2");
		ds.setUser("objects");
		ds.setPassword("objects");

		ic.bind("java:comp/env/jdbc/eMoneyGramDB", ds);
	}
}
