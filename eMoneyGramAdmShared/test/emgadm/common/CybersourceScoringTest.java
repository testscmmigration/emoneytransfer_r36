package emgadm.common;

import java.util.Map;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.apache.log4j.Logger;


import emgadm.property.EMTAdmContainerProperties;
import emgshared.model.ConsumerAddress;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerStatus;

public class CybersourceScoringTest extends TestCase {
	protected static Logger log = EMGSharedLogger.getLogger(CybersourceScoringTest.class);
	String ipAddress = null;
	String email = null;
	
	@Before
	public void setUp() throws Exception {						
		email = "test22488@mail.com";//"andyd2mgo19@mailinator.com"; //Ex: a@yahoo.com
		String accountSID = "A8173D42EC894A21A54557D076A9D18F";
		String authToken = "AD2CB9CB666B4182B74285B1D0EC45D9";
		String requestBaseUrl ="https://sandbox.emailage.com/EmailAgeValidator/";
		ipAddress = "66.87.71.185";
		
		EMTAdmContainerProperties.setEmailAgeURL(requestBaseUrl);
		EMTAdmContainerProperties.setEmailAgeAccountSID(accountSID);
		EMTAdmContainerProperties.setEmailAgeAuthToken(authToken);
		EMTAdmContainerProperties.setEmailAgeServiceTimeout("5000");		
	}

	@Test
	public void testGetEmailAgeInfo() {
		CyberSourceScoring s = new CyberSourceScoring();
		ConsumerAddress address = new ConsumerAddress(1234,123);
		ConsumerProfile consumerProfile = new ConsumerProfile(123,null,ConsumerStatus.ACTIVE_VALIDATION_LEVEL_14,address,null,null,null,null,null);
		consumerProfile.setUserId(email);
		consumerProfile.setState("NY");
		consumerProfile.setIsoCountryCode("USA");
		consumerProfile.setPhoneNumber("7068501234");
		Map<String,Object> resultMap = s.getEmailAgeInfo(consumerProfile, ipAddress);
		String score = resultMap.get("score")!=null ? resultMap.get("score").toString() : null;
		System.out.println(score);		
		assertNotNull(score);
		//for getting error code
		String accountSID = "A8173D42EC894A21A54557D076A9D18";
		EMTAdmContainerProperties.setEmailAgeAccountSID(accountSID);
		String errorCode =resultMap.get("errorCode")!=null ? 
				resultMap.get("errorCode").toString() : null;
		System.out.println("Error code is " + errorCode);
		assertNotNull(errorCode);
		//generate time out Exception
		EMTAdmContainerProperties.setEmailAgeServiceTimeout("1"); //set timeout to 1
		s.getEmailAgeInfo(consumerProfile, ipAddress);//Excetion bloack will be called..
	}

}
