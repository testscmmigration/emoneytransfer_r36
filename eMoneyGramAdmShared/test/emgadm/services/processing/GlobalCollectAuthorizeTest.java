package emgadm.services.processing;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import oracle.jdbc.pool.OracleConnectionPoolDataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.legacy.PowerMockRunner;

import com.moneygram.common_v1.ClientHeader;
import com.moneygram.common_v1.Header;
import com.moneygram.service.CreditCardPaymentService_v2.AuthorizationResponse;
import com.moneygram.service.CreditCardPaymentService_v2.GlobalCollectAuthorizationResponse;
import com.moneygram.service.CreditCardPaymentService_v2.GlobalCollectRequest;
import com.moneygram.service.CreditCardPaymentService_v2.GlobalCollectResponse;

import emgadm.dataaccessors.TransactionManager;
import emgadm.services.creditcardpayment.CreditCardPaymentProxy;
import emgadm.services.creditcardpayment.CreditCardPaymentProxyImpl;
import emgshared.model.Transaction;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CreditCardPaymentProxyImpl.class)
public class GlobalCollectAuthorizeTest {

	private String contextRoot;
	private GlobalCollectAuthorize gcTest;


	@Before
	public void setupDataSource() throws Exception {

		try {
			contextRoot = "/eMoneyGramAdm";
			// Create initial context
			System.setProperty(Context.INITIAL_CONTEXT_FACTORY,"org.apache.naming.java.javaURLContextFactory");
			System.setProperty(Context.URL_PKG_PREFIXES,"org.apache.naming");
			InitialContext ic = new InitialContext();

			ic.createSubcontext("java:");
			ic.createSubcontext("java:comp");
			ic.createSubcontext("java:comp/env");
			ic.createSubcontext("java:comp/env/jdbc");

			// Construct DataSource
			OracleConnectionPoolDataSource ds = new OracleConnectionPoolDataSource();
			ds.setURL("jdbc:oracle:thin:@dmnalx0490:1521:EMGD");
			ds.setUser("objects");
			ds.setPassword("objects");

			ic.bind("java:comp/env/jdbc/eMoneyGramDB", ds);
		} catch (NamingException ex) {
			ex.printStackTrace();
			throw new AssertionError("NamingException thrown");
		}

	}

	@Before
	public void setupMockTransaction() {

		Transaction mockTransaction = Mockito.mock(Transaction.class);
		Mockito.when(mockTransaction.getTranStatCode()).thenReturn("APP");
		Mockito.when(mockTransaction.getTranSubStatCode()).thenReturn("NOF");
		Mockito.when(mockTransaction.isFundable()).thenReturn(true);
		Mockito.when(mockTransaction.getSndTotAmt()).thenReturn(new BigDecimal(10.0));
		Mockito.when(mockTransaction.getEmgTranTypeCode()).thenReturn("MGSEND");
		Mockito.when(mockTransaction.getSndCustAcctId()).thenReturn(11111111);
		Mockito.when(mockTransaction.getCsrPrcsUserid()).thenReturn("up30");
		TransactionManager mockTransactionManager = Mockito.mock(TransactionManager.class);
		Mockito.when(mockTransactionManager.getTransaction(99999999)).thenReturn(mockTransaction);


		gcTest = new GlobalCollectAuthorize();
		gcTest.transactionManager = mockTransactionManager;

	}

	@Before
	public void setupMockAuthorizeResponse() {
		PowerMockito.mockStatic(CreditCardPaymentProxyImpl.class);
		CreditCardPaymentProxy mockClient = Mockito.mock(CreditCardPaymentProxyImpl.class);
		AuthorizationResponse mockAuth = Mockito.mock(AuthorizationResponse.class);
		GlobalCollectAuthorizationResponse gcAuthResponse = new GlobalCollectAuthorizationResponse();
		GlobalCollectRequest gcRequest = new GlobalCollectRequest();
		GlobalCollectResponse gcResponse = new GlobalCollectResponse();
		gcRequest.setEffortId(Long.valueOf(2));
		gcResponse.setPaymentReference("0");
		//gcAuthResponse.setGcRequest(gcRequest);
		gcAuthResponse.setGcResponse(gcResponse);
		Header header = new Header();
		header.setClientHeader(new ClientHeader());
		Mockito.when(mockAuth.getHeader()).thenReturn(header);
		Mockito.when(mockAuth.getGlobalCollect()).thenReturn(gcAuthResponse);
		//Mockito.when(mockAuth.getDecision()).thenReturn(Decision.fromValue(Decision._accept));

		try {
			Mockito.when(CreditCardPaymentProxyImpl.getInstance()).thenReturn(mockClient);
			//Mockito.when(mockClient.authorize(11111111, 10.0, "GBP", "up30", null, 99999999)).thenReturn(mockAuth);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void authorizeReturnsNonNullTransaction() {

		try {
			Transaction tran = gcTest.execute(99999999, "up30", contextRoot);
			assertNotNull(tran);
		} catch (Exception e) {
			e.printStackTrace();
			throw new AssertionError("Exception thrown");
		}
	}

	public void otherTest() {

		try {
			Transaction tran = gcTest.execute(99999999, "up30", contextRoot);
			assertNotNull(tran);
		} catch (Exception e) {
			e.printStackTrace();
			throw new AssertionError("Exception thrown");
		}
	}
}
