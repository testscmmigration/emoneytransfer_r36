package emgadm.services;

import static org.junit.Assert.assertEquals;

import java.sql.SQLException;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import oracle.jdbc.pool.OracleConnectionPoolDataSource;

import org.junit.Before;
import org.junit.Test;

import emgshared.exceptions.DataSourceException;
import emgshared.model.TransactionScoreResult;

//@RunWith(PowerMockRunner.class)
//@PrepareForTest(OracleAccess.class)
public class TransactionScoringImplTest {

	@Before
	public void setupDataSource() throws Exception {

		try {
			// Create initial context
			System.setProperty(Context.INITIAL_CONTEXT_FACTORY,"org.apache.naming.java.javaURLContextFactory");
			System.setProperty(Context.URL_PKG_PREFIXES,"org.apache.naming");
			InitialContext ic = new InitialContext();

			ic.createSubcontext("java:");
			ic.createSubcontext("java:comp");
			ic.createSubcontext("java:comp/env");
			ic.createSubcontext("java:comp/env/jdbc");

			// Construct DataSource
			OracleConnectionPoolDataSource ds = new OracleConnectionPoolDataSource();
			ds.setURL("jdbc:oracle:thin:@dmnalx0490:1521:EMGD");
			ds.setUser("objects");
			ds.setPassword("objects");

			ic.bind("java:comp/env/jdbc/eMoneyGramDB", ds);
		} catch (NamingException ex) {
			ex.printStackTrace();
			throw new AssertionError("NamingException thrown");
		}

	}

	@Test
	public void getSpecificTransactionScoreTest() {
		//PowerMockito.mockStatic(OracleAccess.class);
		//Mockito.when(OracleAccess.getDataSource()).thenReturn((DataSource) "localhost");

		String userId = "up30";
		int tranId = 9557;
		TransactionScoringImpl tscore = new TransactionScoringImpl();
		try {
			Map results = tscore.getTransactionScore(userId,tranId);
			TransactionScoreResult transactionScoreResult =
				(TransactionScoreResult) results.get("Transaction Score Result");
			assertEquals(519,transactionScoreResult.getScore());

			// test here
		} catch (DataSourceException e) {
			e.printStackTrace();
			throw new AssertionError("DataSourceException thrown");
		} catch (SQLException e) {
			e.printStackTrace();
			throw new AssertionError("SQLException thrown");
		}
	}
}
