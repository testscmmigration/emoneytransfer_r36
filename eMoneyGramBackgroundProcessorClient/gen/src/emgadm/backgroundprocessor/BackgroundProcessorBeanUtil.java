package emgadm.backgroundprocessor;

public class BackgroundProcessorBeanUtil {

	private static Object lookupHome(java.util.Hashtable environment,
			String jndiName, Class narrowTo)
			throws javax.naming.NamingException {
		// Obtain initial context
		javax.naming.InitialContext initialContext = new javax.naming.InitialContext(
				environment);
		try {
			Object objRef = initialContext.lookup(jndiName);
			// only narrow if necessary
			if (java.rmi.Remote.class.isAssignableFrom(narrowTo))
				return javax.rmi.PortableRemoteObject.narrow(objRef, narrowTo);
			else
				return objRef;
		} finally {
			initialContext.close();
		}
	}

	public static emgadm.backgroundprocessor.BackgroundProcessorLocalHome getLocalHome()
			throws javax.naming.NamingException {

		return (emgadm.backgroundprocessor.BackgroundProcessorLocalHome) lookupHome(
				null,
				emgadm.backgroundprocessor.BackgroundProcessorLocalHome.COMP_NAME,
				emgadm.backgroundprocessor.BackgroundProcessorLocalHome.class);

	}

}