package emgadm.backgroundprocessor;

import javax.ejb.CreateException;

/**
 * Local Home interface for emgadm.backgroundprocessor.BackgroundProcessorBean bean
 */
public interface BackgroundProcessorLocalHome extends javax.ejb.EJBLocalHome {

	public static final String COMP_NAME = "java:comp/env/ejb/BackgroundProcessorBean";
	public static final String JNDI_NAME = "ejb/emgadm/backgroundprocessor/BackgroundProcessorLocalHome";

	/* Default create */
	public emgadm.backgroundprocessor.BackgroundProcessorLocal create()
			throws CreateException;

}