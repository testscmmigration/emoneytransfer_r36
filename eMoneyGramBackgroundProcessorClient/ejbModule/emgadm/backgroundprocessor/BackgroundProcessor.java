package emgadm.backgroundprocessor;



public interface BackgroundProcessor {
	void start(BackgroundProcessorConfig config);
	void startProcessApprovedBankTx(ProcessApprovedBankTxProcessorConfig config);
	void cancelAllTimers();
}
