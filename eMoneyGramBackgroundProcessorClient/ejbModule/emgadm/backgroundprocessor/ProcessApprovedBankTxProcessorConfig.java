package emgadm.backgroundprocessor;

import emgshared.property.EMTSharedDynProperties;

public class ProcessApprovedBankTxProcessorConfig implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2717814449394563294L;
	// Service
	private String loopbackServer;
	private String loopbackHost;
	private String contextName;

	// Constants
	public static final long DEFAULT_TIME_TO_PROCESS_AGAIN = 1000; // one second
	public static final long DEFAULT_TIME_TO_POLL_BACK_AFTER_IDLE = 300000; // 5 minutes
	public static final long DEFAULT_TIME_TO_START = 1000; // one second
	public static final long DEFAULT_TIME_TO_RETRY_WHEN_BUSY = 300000; // 5 minutes
	public static final long DEFAULT_TIME_TO_RETRY_AFTER_ERROR = 300000; // 5 minutes
	public static final long DEFAULT_MINIMUM_TIME_TO_PROCESS;
	public static final int DEFAULT_MAX_ERRORS = 500;
	
	static {
		long defaultMinimumTimeToProcess = 4 * 60;	// 4 hours
		try {
			EMTSharedDynProperties prop = new EMTSharedDynProperties();
			defaultMinimumTimeToProcess = prop.getDsSendDelay(); 
		} catch (Exception e) {
		}
		// Dynamic property value is expected in minutes
		DEFAULT_MINIMUM_TIME_TO_PROCESS = defaultMinimumTimeToProcess * 60 * 1000;
	}

	// Data
	private long timeToProcessAgain = DEFAULT_TIME_TO_PROCESS_AGAIN;
	private long timeToPollBackAfterIdle = DEFAULT_TIME_TO_POLL_BACK_AFTER_IDLE;
	private long timeToStart = DEFAULT_TIME_TO_START;
	private long timeToRetryWhenBusy = DEFAULT_TIME_TO_RETRY_WHEN_BUSY;
	private long timeToRetryAfterError = DEFAULT_TIME_TO_RETRY_AFTER_ERROR;
	private long minimumTimeToProcess = DEFAULT_MINIMUM_TIME_TO_PROCESS;

	private int maxErrors = DEFAULT_MAX_ERRORS;

	public void setTimeToStart(long timeToStart) {
		this.timeToStart = timeToStart;
	}

	public long getTimeToStart() {
		return timeToStart;
	}

	public void setTimeToProcessAgain(long timeToProcessAgain) {
		this.timeToProcessAgain = timeToProcessAgain;
	}

	public long getTimeToProcessAgain() {
		return timeToProcessAgain;
	}

	public int getMaxErrors() {
		return this.maxErrors;
	}

	public void setMaxErrors(int maxErrors) {
		this.maxErrors = maxErrors;
	}

	public String getContextName() {
		return contextName;
	}

	public void setContextName(String contextName) {
		this.contextName = contextName;
	}

	public String getLoopbackServer() {
		return loopbackServer;
	}

	public void setLoopbackServer(String loopbackServer) {
		this.loopbackServer = loopbackServer;
	}

	public String getLoopbackHost() {
		return loopbackHost;
	}

	public void setLoopbackHost(String loopbackHost) {
		this.loopbackHost = loopbackHost;
	}

	public long getTimeToPollBackAfterIdle() {
		return timeToPollBackAfterIdle;
	}

	public void setTimeToPollBackAfterIdle(long timeToPollBackAfterIdle) {
		this.timeToPollBackAfterIdle = timeToPollBackAfterIdle;
	}

	public long getTimeToRetryWhenBusy() {
		return timeToRetryWhenBusy;
	}

	public void setTimeToRetryWhenBusy(long timeToRetryWhenBusy) {
		this.timeToRetryWhenBusy = timeToRetryWhenBusy;
	}

	public long getTimeToRetryAfterError() {
		return timeToRetryAfterError;
	}

	public void setTimeToRetryAfterError(long timeToRetryAfterError) {
		this.timeToRetryAfterError = timeToRetryAfterError;
	}

	public long getMinimumTimeToProcess() {
		return minimumTimeToProcess;
	}

	public void setMinimumTimeToProcess(long minimumTimeToProcess) {
		this.minimumTimeToProcess = minimumTimeToProcess;
	}
}